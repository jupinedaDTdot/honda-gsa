﻿using Marine.DataAccess.Model;
using HondaCA.Entity;
using HondaCA.Entity.Model;

namespace Marine.Service.Model
{
	public class MarineTrimService
	{
		public EntityCollection<Trim> GetTrimWithBasePrice(string categoryURL, int targetID, string language)
		{
			MarineTrimMapper trimMapper = new MarineTrimMapper();
			EntityCollection<Trim> trims = trimMapper.SelectTrimWithBasePrice(categoryURL, targetID, language);
			// adjust transmission
			return trims;
		}
	}
}
