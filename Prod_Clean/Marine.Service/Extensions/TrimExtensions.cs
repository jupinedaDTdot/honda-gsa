﻿using HondaATV.Service.Model;
using HondaCA.Common;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using HondaCA.Service.Model;

namespace Marine.Service.Extensions
{
	public static class TrimExtensions
	{
		public static Trim GetPriceAdjustedTrim(this Trim trim, int targetId, Language language)
		{
			Trim result = (Trim)trim.Clone();
			Transmission transmission = result.GetTransmissionRepresentative(targetId, language);
			if (transmission == null)
			{
				return result;
			}
			result.MSRP = transmission.MSRP;
			result.FreightPDI = transmission.FreightPDI;
			result.PriceDiscountAmount = transmission.PriceDiscountAmount;
			result.PriceDiscountPercentage = transmission.PriceDiscountPercentage;
			return result;
		}

		public static bool HasDiscount(this Trim trim)
		{
			return trim.GetDiscount() > 0;
		}

		
		public static decimal GetDiscount(this Trim trim, bool quebec = false)
		{
			decimal MSRP;
			if (quebec)
			{
				MSRP = trim.MSRP + trim.FreightPDI;
			}
			else
			{
				MSRP = trim.MSRP;
			}
			// decimal MSRP = trim.MSRP + trim.FreightPDI;
			return PETrimService.getDiscountAmount(MSRP,
												 trim.PriceDiscountAmount,
												 trim.PriceDiscountPercentage);
		}

		public static decimal GetFinalPrice(this Trim trim, bool quebec = false)
		{
			decimal MSRP;
			if (quebec)
			{
				MSRP = trim.MSRP + trim.FreightPDI;
			}
			else
			{
				MSRP = trim.MSRP;
			}
			return trim.HasDiscount() ? PETrimService.getDiscountedPrice(MSRP, trim.PriceDiscountAmount, trim.PriceDiscountPercentage) : MSRP;
		}

		public static bool HasDiscount(this Transmission transmission)
		{
			return transmission.GetDiscount() > 0;
		}

		public static decimal GetFinalPrice(this Transmission transmission, bool quebec = false)
		{
			decimal MSRP;
			if (quebec)
			{
				MSRP = transmission.MSRP + transmission.FreightPDI;
			}
			else
			{
				MSRP = transmission.MSRP;
			}
			//decimal MSRP = transmission.MSRP + transmission.FreightPDI;
			return transmission.HasDiscount()
				   ? PETrimService.getDiscountedPrice(MSRP, transmission.PriceDiscountAmount,
													  transmission.PriceDiscountPercentage)
				   : MSRP;
		}

		public static decimal GetDiscount(this Transmission transmission, bool quebec = false)
		{
			decimal MSRP;
			if (quebec)
			{
				MSRP = transmission.MSRP + transmission.FreightPDI;
			}
			else
			{
				MSRP = transmission.MSRP;
			}

			//decimal MSRP = transmission.MSRP + transmission.FreightPDI;
			return PETrimService.getDiscountAmount(MSRP,
												 transmission.PriceDiscountAmount,
												 transmission.PriceDiscountPercentage);
		}

		public static Transmission GetTransmissionRepresentative(this Trim trim, int targetId, Language language)
		{
			if (trim.Transmissions == null)
			{
				TransmissionTypeService tts = new HondaCA.Service.Model.TransmissionTypeService();
				EntityCollection<Transmission> transmissions = tts.GetTransmissionTypesByTrimID(trim.TrimID,
																								targetId,
																								language.GetCultureStringValue());
				trim.Transmissions = transmissions;
			}

			// query for the lowest price
			Transmission result = null;
			if (trim.Transmissions != null)
			{
				foreach (var transmission in trim.Transmissions)
				{
					if (result == null || result.GetFinalPrice() > transmission.GetFinalPrice())
					{
						result = transmission;
					}
				}
			}
			return result;
		}
	}
}
