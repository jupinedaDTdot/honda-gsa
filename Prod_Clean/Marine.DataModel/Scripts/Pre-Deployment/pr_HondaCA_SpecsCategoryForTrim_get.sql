﻿
-- =============================================
--	Created 	Nikhil Patel
-- Updated By : Nimit Patel
-- Updated date: 2012-Apr-25
-- Description:	Get spect category by trim with display order of spect category.
-- =============================================
ALTER PROCEDURE [dbo].[pr_HondaCA_SpecsCategoryForTrim_get] (
	@Lang char(2),
	@TrimID int,
	@TargetID int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
	   sc.SC_Name AS SpecCategoryName,
	   (CASE @Lang WHEN 'en' THEN sc.SC_Label_EN  WHEN 'fr' THEN sc.SC_Label_FR END ) AS SpecCategoryDescription,
	   sc.SpecCategory_ID,
	   sc.SC_Display_Order
	FROM TrimSpec ts
	INNER JOIN SpecItem si ON si.SpecItem_ID = ts.SpecItem_ID
	INNER JOIN SpecCategory sc ON sc.SpecCategory_ID = si.SpecCategory_ID
	INNER JOIN TrimVisibility tv ON tv.Trim_ID = ts.Trim_ID
	WHERE ts.Trim_ID = @TrimID AND
		tv.Target_ID = @TargetID 
		Group BY  sc.SC_Name ,sc.SC_Label_EN ,sc.SC_Label_FR,sc.SpecCategory_ID,sc.SC_Display_Order
		Order by sc.SC_Display_Order
END