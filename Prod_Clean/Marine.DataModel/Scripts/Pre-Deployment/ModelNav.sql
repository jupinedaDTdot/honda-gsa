﻿-- =============================================
-- Script Template
-- =============================================

--marine.honda.ca
Declare @Application varchar(50)
SET @Application = 'marine.honda.ca'
--SELECT * FROM [dbo].[cmsMenuTemplate] WHERE [MenuName]='ModelNav_en' AND [Application] = @Application;
  
DELETE FROM [dbo].[cmsMenuTemplate] WHERE [MenuName]='ModelNav_en' AND [Application] = @Application;
INSERT INTO [dbo].[cmsMenuTemplate]( [MenuName], [MenuItemName], [MenuItemText], [MenuItemURL], [UrlPrefix], [DisplayOrder], [ParentMenuItemID], [Optional], [Application]) 
values (  'ModelNav_en', 'Overview', 'Overview', '', NULL, 1, NULL, 0, @Application )

INSERT INTO [dbo].[cmsMenuTemplate]( [MenuName], [MenuItemName], [MenuItemText], [MenuItemURL], [UrlPrefix], [DisplayOrder], [ParentMenuItemID], [Optional], [Application]) 
values ( 'ModelNav_en', 'Features', 'Features', '/features', NULL, 2, NULL, 0, @Application )

INSERT INTO [dbo].[cmsMenuTemplate]( [MenuName], [MenuItemName], [MenuItemText], [MenuItemURL], [UrlPrefix], [DisplayOrder], [ParentMenuItemID], [Optional], [Application]) 
values ( 'ModelNav_en', 'Specifications', 'Specifications', '/specifications', NULL, 3, NULL, 0, @Application )

INSERT INTO [dbo].[cmsMenuTemplate]( [MenuName], [MenuItemName], [MenuItemText], [MenuItemURL], [UrlPrefix], [DisplayOrder], [ParentMenuItemID], [Optional], [Application]) 
values ( 'ModelNav_en', 'Accessories', 'Parts & Accessories', '/parts-accessories', NULL, 4, NULL, 0, @Application )


--French
DELETE FROM [dbo].[cmsMenuTemplate] WHERE [MenuName]='ModelNav_fr' AND [Application] = @Application;
--------------------------------------------------------
INSERT INTO [dbo].[cmsMenuTemplate]( [MenuName], [MenuItemName], [MenuItemText], [MenuItemURL], [UrlPrefix], [DisplayOrder], [ParentMenuItemID], [Optional], [Application]) 
values (  'ModelNav_fr', 'Survol', 'Survol', '', NULL, 1, NULL, 0, @Application )

INSERT INTO [dbo].[cmsMenuTemplate]( [MenuName], [MenuItemName], [MenuItemText], [MenuItemURL], [UrlPrefix], [DisplayOrder], [ParentMenuItemID], [Optional], [Application]) 
values (  'ModelNav_fr', 'Caractéristiques', 'Caractéristiques', '/caracteristiques', NULL, 2, NULL, 0, @Application )

INSERT INTO [dbo].[cmsMenuTemplate]( [MenuName], [MenuItemName], [MenuItemText], [MenuItemURL], [UrlPrefix], [DisplayOrder], [ParentMenuItemID], [Optional], [Application]) 
values (  'ModelNav_fr', 'Spécifications', 'Spécifications', '/specifications', NULL, 3, NULL, 0, @Application )


INSERT INTO [dbo].[cmsMenuTemplate]( [MenuName], [MenuItemName], [MenuItemText], [MenuItemURL], [UrlPrefix], [DisplayOrder], [ParentMenuItemID], [Optional], [Application]) 
values (  'ModelNav_fr', 'Accessoires', 'Pièces ET accessoires', '/accessoires', NULL, 4, NULL, 0, @Application )



GO

