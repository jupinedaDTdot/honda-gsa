﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

           

UPDATE [Trim] SET T_URL_EN  =
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(T_Name_EN  COLLATE Latin1_General_CI_AI
,'a','a'),'b','b'),'c','c'),'d','d'),'e','e'),'f', 'f')
,'g','g'),'h','h'),'i','i'),'j','j'),'k','k'),'l', 'l')
,'m','m'),'n','n'),'o','o'),'p','p'),'q','q'),'r', 'r')
,'s','s'),'t','t'),'u','u'),'v','v'),'w','w'),'x', 'x')
,'y','y'),'z','z')
,'"','') ,'.','') ,'''','') ,'(','') ,')','') ,' & ','and')
,'&','_and_') ,'/','') ,' ','-')

--UPDATE [BaseModelFamily] SET BaseModelFamily_Name_FR = BaseModelFamily_Name_EN  WHERE ISNULL(BaseModelFamily_URL_FR,'')  = ''

UPDATE [Trim] SET T_URL_FR  =
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(T_Name_FR  COLLATE Latin1_General_CI_AI
,'a','a'),'b','b'),'c','c'),'d','d'),'e','e'),'f', 'f')
,'g','g'),'h','h'),'i','i'),'j','j'),'k','k'),'l', 'l')
,'m','m'),'n','n'),'o','o'),'p','p'),'q','q'),'r', 'r')
,'s','s'),'t','t'),'u','u'),'v','v'),'w','w'),'x', 'x')
,'y','y'),'z','z')
,'"','') ,'.','') ,'''','') ,'(','') ,')','') ,' & ','and')
,'&','_and_') ,'/','') ,' ','-')


UPDATE [Trim] SET T_URL_FR = T_URL_FR + '_fr' WHERE T_URL_FR = T_URL_EN

           
UPDATE [BaseModel] SET BM_URL_EN  =
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(BM_Name_EN  COLLATE Latin1_General_CI_AI
,'a','a'),'b','b'),'c','c'),'d','d'),'e','e'),'f', 'f')
,'g','g'),'h','h'),'i','i'),'j','j'),'k','k'),'l', 'l')
,'m','m'),'n','n'),'o','o'),'p','p'),'q','q'),'r', 'r')
,'s','s'),'t','t'),'u','u'),'v','v'),'w','w'),'x', 'x')
,'y','y'),'z','z')
,'"','') ,'.','') ,'''','') ,'(','') ,')','') ,' & ','and')
,'&','_and_') ,'/','') ,' ','-')

--UPDATE [BaseModelFamily] SET BaseModelFamily_Name_FR = BaseModelFamily_Name_EN  WHERE ISNULL(BaseModelFamily_URL_FR,'')  = ''

UPDATE [BaseModel] SET BM_URL_FR  =
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(BM_Name_FR  COLLATE Latin1_General_CI_AI
,'a','a'),'b','b'),'c','c'),'d','d'),'e','e'),'f', 'f')
,'g','g'),'h','h'),'i','i'),'j','j'),'k','k'),'l', 'l')
,'m','m'),'n','n'),'o','o'),'p','p'),'q','q'),'r', 'r')
,'s','s'),'t','t'),'u','u'),'v','v'),'w','w'),'x', 'x')
,'y','y'),'z','z')
,'"','') ,'.','') ,'''','') ,'(','') ,')','') ,' & ','and')
,'&','_and_') ,'/','') ,' ','-')


UPDATE [BaseModel] SET BM_URL_FR = BM_URL_FR + '_fr' WHERE BM_URL_FR = BM_URL_EN

           
           
           
UPDATE [BaseModelFamily] SET BaseModelFamily_URL_EN  =
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(BaseModelFamily_Name_EN  COLLATE Latin1_General_CI_AI
,'a','a'),'b','b'),'c','c'),'d','d'),'e','e'),'f', 'f')
,'g','g'),'h','h'),'i','i'),'j','j'),'k','k'),'l', 'l')
,'m','m'),'n','n'),'o','o'),'p','p'),'q','q'),'r', 'r')
,'s','s'),'t','t'),'u','u'),'v','v'),'w','w'),'x', 'x')
,'y','y'),'z','z')
,'"','') ,'.','') ,'''','') ,'(','') ,')','') ,' & ','and')
,'&','_and_') ,'/','') ,' ','-')

--UPDATE [BaseModelFamily] SET BaseModelFamily_Name_FR = BaseModelFamily_Name_EN  WHERE ISNULL(BaseModelFamily_URL_FR,'')  = ''

UPDATE [BaseModelFamily] SET BaseModelFamily_URL_FR  =
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(BaseModelFamily_Name_FR  COLLATE Latin1_General_CI_AI
,'a','a'),'b','b'),'c','c'),'d','d'),'e','e'),'f', 'f')
,'g','g'),'h','h'),'i','i'),'j','j'),'k','k'),'l', 'l')
,'m','m'),'n','n'),'o','o'),'p','p'),'q','q'),'r', 'r')
,'s','s'),'t','t'),'u','u'),'v','v'),'w','w'),'x', 'x')
,'y','y'),'z','z')
,'"','') ,'.','') ,'''','') ,'(','') ,')','') ,' & ','and')
,'&','_and_') ,'/','') ,' ','-')


UPDATE [BaseModelFamily] SET BaseModelFamily_URL_FR = BaseModelFamily_URL_FR + '_fr' WHERE BaseModelFamily_URL_FR = BaseModelFamily_URL_EN


UPDATE [BaseModelCategory] SET BMC_URL_EN  =
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(BMC_Name_EN  COLLATE Latin1_General_CI_AI
,'a','a'),'b','b'),'c','c'),'d','d'),'e','e'),'f', 'f')
,'g','g'),'h','h'),'i','i'),'j','j'),'k','k'),'l', 'l')
,'m','m'),'n','n'),'o','o'),'p','p'),'q','q'),'r', 'r')
,'s','s'),'t','t'),'u','u'),'v','v'),'w','w'),'x', 'x')
,'y','y'),'z','z')
,'"','') ,'.','') ,'''','') ,'(','') ,')','') ,' & ','and')
,'&','_and_') ,'/','') ,' ','-')

--UPDATE [BaseModelCategory] SET BMC_Name_FR = BMC_Name_EN  WHERE ISNULL(BMC_URL_FR,'')  = ''

UPDATE [BaseModelCategory] SET BMC_URL_FR  =
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(BMC_Name_FR  COLLATE Latin1_General_CI_AI
,'a','a'),'b','b'),'c','c'),'d','d'),'e','e'),'f', 'f')
,'g','g'),'h','h'),'i','i'),'j','j'),'k','k'),'l', 'l')
,'m','m'),'n','n'),'o','o'),'p','p'),'q','q'),'r', 'r')
,'s','s'),'t','t'),'u','u'),'v','v'),'w','w'),'x', 'x')
,'y','y'),'z','z')
,'"','') ,'.','') ,'''','') ,'(','') ,')','') ,' & ','and')
,'&','_and_') ,'/','') ,' ','-')


UPDATE [BaseModelCategory] SET BMC_URL_FR = BMC_URL_FR + '_fr' WHERE BMC_URL_FR = BMC_URL_EN




USE [MARV3]
GO
/****** Object:  StoredProcedure [dbo].[pr_HondaCA_ModelCategory_get]    Script Date: 03/02/2012 10:46:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =====================================================================================
-- Author:		Nimit Patel
-- Create date: 2 Mar 2012
-- Description:	gets trim data with base price. it means get single data for each trim.
-- ======================================================================================

ALTER PROCEDURE [dbo].[pr_HondaCA_Trim_WithBasePrice_get]
 @TargetID INT, @ModelFamilyName varchar(50)=null,@BaseModel_ID int =-1, @ModelFamilyUrl varchar(50)=null, @Lang CHAR (2),
 @BMCategoryURL varchar(50)=null
AS
BEGIN

	SELECT distinct	t.Trim_ID AS TrimID, t.ACode AS AutoDataACode, t.Export_key AS TrimExportKey, t.T_Display_Order AS TrimDisplayOrder,					CASE @Lang WHEN 'en' THEN t .T_Name_EN WHEN 'fr' THEN t .T_Name_FR END AS TrimName, 					CASE @Lang WHEN 'en' THEN t.T_URL_EN WHEN 'fr' THEN t.T_URL_FR END AS TrimUrl,   
                    CASE @Lang WHEN 'en' THEN t .T_Description_EN WHEN 'fr' THEN t .T_Description_FR END AS TrimDescription,   
                    CASE @Lang WHEN 'en' THEN t .Det_Highlights_EN WHEN 'fr' THEN t .Det_Highlights_FR END AS DETHighlights,   
                    CASE @Lang WHEN 'en' THEN t .Comp_Highlights_EN WHEN 'fr' THEN t .Comp_Highlights_FR END AS CompHighlights, 					CASE @Lang WHEN 'en' THEN t .Det_Highlights_Title_EN WHEN 'fr' THEN t .Det_Highlights_Title_FR END AS DetHighlightsTitle,   
                    CASE @Lang WHEN 'en' THEN t .Comp_Highlights_Title_EN WHEN 'fr' THEN t .Comp_Highlights_Title_FR END AS CompHighlightsTitle,   
                    m.ModelYear_Year AS ModelYearYear, ModelYear.Is_Visible_Flag AS IsVisibleFlag, bm.Export_Key AS BaseModelExportKey,
                    CASE @Lang WHEN 'en' THEN bm.BM_Name_EN WHEN 'fr' THEN bm.BM_Name_FR END AS BaseModelName,
                    ( SELECT BMC_NAME_EN FROM  BaseModelCategory BMC WHERE BMC.BaseModelCategory_ID= T.BaseModelCategory_ID ),
                    CASE @Lang WHEN 'en' THEN bmf.BaseModelFamily_URL_EN WHEN 'fr' THEN bmf.BaseModelFamily_URL_FR END AS ModelFamilyURL,
                    bm.BM_Display_Order,
                    t.BaseModelCategory_ID,
					BP.MSRP,
					@Lang AS Language
					       
	FROM    [BaseModel] bm 		
		INNER JOIN model m on m.BaseModel_ID= bm.BaseModel_ID
		INNER JOIN Trim t on t.Model_ID = m.Model_ID
		INNER JOIN TrimVisibility AS tv ON t.Trim_ID = tv.Trim_ID
		INNER JOIN ModelYear ON m.ModelYear_Year = ModelYear.ModelYear_Year				
		INNER JOIN FactoryOption ON t.Trim_ID = FactoryOption.Trim_ID   
	    INNER JOIN (select tm.trim_ID, bm.BaseModel_ID, min(FactoryOption.MSRP) AS MSRP from   Model AS m INNER JOIN  
							Trim AS tm ON m.Model_ID = tm.Model_ID INNER JOIN  
							TrimVisibility AS tv ON tm.Trim_ID = tv.Trim_ID INNER JOIN  
							BaseModel AS bm ON m.BaseModel_ID = bm.BaseModel_ID INNER JOIN 
							FactoryOption ON tm.Trim_ID = FactoryOption.Trim_ID 	
			GROUP By tm.trim_ID,bm.BaseModel_ID
		) AS  BP  ON t.trim_ID = BP.Trim_ID AND FactoryOption.MSRP = BP.MSRP 
		INNER JOIN [BaseModelFamily] bmf ON bm.BaseModelFamily_ID=bmf.BaseModelFamily_ID  
		LEFT JOIN BaseModel_BaseModelCategory bmbmc ON bmbmc.BaseModel_ID = bm.BaseModel_ID
		LEFT JOIN [BaseModelCategory] bmc ON BMC.BaseModelCategory_ID = t.BaseModelCategory_ID		
		
	WHERE	bm.BaseModel_ID = ( CASE isnull(@BaseModel_ID,-1) WHEN -1 THEN  bm.BaseModel_ID ELSE @BaseModel_ID  END ) AND
			CASE @Lang WHEN 'en' THEN isnull(bmc.BMC_URL_EN , '')  WHEN 'fr' THEN  isnull( bmc.BMC_URL_FR, '') END   =   
			CASE @Lang WHEN 'en' THEN ISNULL(@BMCategoryURL, isnull(bmc.BMC_URL_EN, '')) WHEN 'fr' THEN ISNULL(@BMCategoryURL, isnull(bmc.BMC_URL_FR, '')) END AND  
			(CASE @Lang WHEN 'en' THEN BMF.BaseModelFamily_Name_EN WHEN 'fr' THEN isnull(BMF.BaseModelFamily_Name_FR,'')  END ) = 
			(CASE @Lang WHEN 'en' THEN ISNULL(@ModelFamilyName,BMF.BaseModelFamily_Name_EN ) WHEN 'fr' THEN ISNULL(@ModelFamilyName,isnull(BMF.BaseModelFamily_Name_FR,'')) END ) AND
			(CASE @Lang WHEN 'en' THEN isnull(BMF.BaseModelFamily_URL_EN,'') WHEN 'fr' THEN  isnull(BMF.BaseModelFamily_URL_FR,'') END ) = 
			(CASE @Lang WHEN 'en' THEN ISNULL(@ModelFamilyUrl, isnull(BMF.BaseModelFamily_URL_EN,'') ) WHEN 'fr' THEN ISNULL(@ModelFamilyUrl, isnull(BMF.BaseModelFamily_URL_FR,'')) END )            
			AND bmc.BMC_NAME_EN != 'other'
	ORDER By BM_Display_Order, TrimName
END



GO
-------------------------

USE [MARV3]
GO
/****** Object:  StoredProcedure [dbo].[pr_HondaCA_SpecsCategoryForTrim_get]    Script Date: 03/14/2012 18:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
--	Created 	Nikhil Patel
-- Updated date: 2012-JAN-06
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[pr_HondaCA_SpecsCategoryForTrim_get] (
	@Lang char(2),
	@TrimID int,
	@TargetID int
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
	   sc.SC_Name AS SpecCategoryName,
	   (CASE @Lang WHEN 'en' THEN sc.SC_Label_EN  WHEN 'fr' THEN sc.SC_Label_FR END ) AS SpecCategoryDescription,
	   sc.SpecCategory_ID,
	   sc.SC_Display_Order
	FROM TrimSpec ts
	INNER JOIN SpecItem si ON si.SpecItem_ID = ts.SpecItem_ID
	INNER JOIN SpecCategory sc ON sc.SpecCategory_ID = si.SpecCategory_ID
	INNER JOIN TrimVisibility tv ON tv.Trim_ID = ts.Trim_ID
	WHERE ts.Trim_ID = @TrimID AND
		tv.Target_ID = @TargetID 
		Group BY  sc.SC_Name ,sc.SC_Label_EN ,sc.SC_Label_FR,sc.SpecCategory_ID,sc.SC_Display_Order
END

GO
-------------------

USE [MARV3]
GO
/****** Object:  StoredProcedure [dbo].[pr_HondaCA_SpecsForTrim_get]    Script Date: 03/14/2012 18:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[pr_HondaCA_SpecsForTrim_get] (
	@Lang char(2),
	@TrimID int,
	@TargetID int, 
	@SpecCategoryID int = null
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
	si.SI_Name AS SpecName
	,(CASE @Lang WHEN 'en' THEN si.SI_Label_EN  WHEN 'fr' THEN si.SI_Label_FR END ) AS SpecLabel
	,(CASE @Lang WHEN 'en' THEN si.SI_Tip_EN  WHEN 'fr' THEN si.SI_tip_FR END ) AS SpecTip
	,(CASE @Lang WHEN 'en' THEN ts.TS_Value_EN  WHEN 'fr' THEN ts.TS_Value_FR END ) AS SpecValue
	,si.SpecCategory_ID
	FROM TrimSpec ts
	INNER JOIN SpecItem si ON si.SpecItem_ID = ts.SpecItem_ID
	INNER JOIN SpecCategory sc ON sc.SpecCategory_ID = si.SpecCategory_ID
	INNER JOIN TrimVisibility tv ON tv.Trim_ID = ts.Trim_ID
	WHERE ts.Trim_ID = @TrimID AND
		tv.Target_ID = @TargetID AND si.SpecCategory_ID = isnull(@SpecCategoryID, si.SpecCategory_ID)
END
GO


