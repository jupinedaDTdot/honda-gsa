﻿-- =============================================
-- Script Template
-- =============================================
ALTER PROCEDURE [dbo].[pr_HondaCA_TransmissionType_get]
@ModelID INT=null, @TrimID INT=null, @TargetID INT, @Lang CHAR (2)
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT      t.Trim_ID AS TrimID, f.FactoryOption_ID AS FactoryOptionID, f.Transmission_Type AS TransmissionType, f.Export_Key AS ExportKey, 
CASE @Lang WHEN 'fr' then f.Transmission_Spec_FR ELSE f.Transmission_Spec_EN END AS TransmissionSpec, @Lang AS Language,
f.MSRP as MSRP , f.FO_Model_Code
FROM         FactoryOption f
					  INNER JOIN Trim AS t ON f.Trim_ID = t.Trim_ID
					  INNER JOIN TrimVisibility AS tv ON t.Trim_ID = tv.Trim_ID 
					  INNER JOIN Model AS m ON m.Model_ID = t.Model_ID 
                      INNER JOIN BaseModel AS bm ON m.BaseModel_ID = bm.BaseModel_ID 
                      INNER JOIN ModelYear ON m.ModelYear_Year = ModelYear.ModelYear_Year 
                      INNER JOIN FactoryOption ON t.Trim_ID = FactoryOption.Trim_ID 
					  --INNER JOIN FactoryOptionVisibility ON f.FactoryOption_ID = FactoryOptionVisibility.FactoryOption_ID
WHERE     (m.Model_ID = isnull(@ModelID,m.Model_ID)) AND (tv.Target_ID = @TargetID) AND (ModelYear.Is_Visible_Flag = 1) AND (t.Trim_ID = isnull(@TrimID,t.Trim_ID) )
GROUP BY t.Trim_ID,f.FactoryOption_ID,f.Transmission_Type,f.Export_Key,f.Transmission_Spec_EN,f.Transmission_Spec_FR,f.MSRP,f.FO_Model_Code,f.FO_Display_Order
ORDER BY f.FO_Display_Order,MSRP



END















