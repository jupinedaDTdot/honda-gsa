﻿-- =============================================
-- Script Template
-- Updated stored procedure by adding dealer caliberID to display caliber type of dealer on search listing page.
-- =============================================
USE [HondaCommonV3]
GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_Dealers_get]    Script Date: 04/13/2012 11:44:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pr_HondaCommon_Dealers_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pr_HondaCommon_Dealers_get]
GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByProv_get]    Script Date: 04/13/2012 11:44:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pr_HondaCommon_DealersByProv_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pr_HondaCommon_DealersByProv_get]
GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByName_get]    Script Date: 04/13/2012 11:44:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pr_HondaCommon_DealersByName_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pr_HondaCommon_DealersByName_get]
GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByID_get]    Script Date: 04/13/2012 11:44:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pr_HondaCommon_DealersByID_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pr_HondaCommon_DealersByID_get]
GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByCoordinates_get]    Script Date: 04/13/2012 11:44:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pr_HondaCommon_DealersByCoordinates_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pr_HondaCommon_DealersByCoordinates_get]
GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByCity_get]    Script Date: 04/13/2012 11:44:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pr_HondaCommon_DealersByCity_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pr_HondaCommon_DealersByCity_get]
GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByZip_get]    Script Date: 04/13/2012 11:44:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pr_HondaCommon_DealersByZip_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[pr_HondaCommon_DealersByZip_get]
GO

USE [HondaCommonV3]
GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_Dealers_get]    Script Date: 04/13/2012 11:44:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[pr_HondaCommon_Dealers_get] (
      @IsExpressDealer bit=null,
      @Lang char(2),
      @ProductLineCode char(1)
)
AS
BEGIN
      
      SET NOCOUNT ON;

      SELECT 
            di.Export_Key,
            di.DealerID,
            di.Award,
            di.IsExpressDealer,
            di.DealerCode,
            di.DealerName,
            di.MainPhone, 
            di.Fax,
            di.Street,
            di.City,
            di.DealerRegionID,
            pr.P_CODE as Province,
            di.PostalCode,
            di.Email, 
            di.HomePage,
            CASE @Lang WHEN 'en' THEN di.DirectionsEng WHEN 'fr' THEN di.DirectionsFre END AS Directions,
            di.PartsPhone,
            di.ServicePhone,
            di.Latitude,
            di.Longitude,
            di.DealerUrlName,
            CASE @Lang WHEN 'en' THEN dc.CaliberNameEng WHEN 'fr' THEN dc.CaliberNameFre END AS CaliberName,
            dpl.ProductLineCode,
            di.IsMUVAuthorizedDealer,
            di.CaliberID
      FROM DealerInformation di
      INNER JOIN DealerRegion dr ON di.DealerRegionID = dr.DealerRegionID
      INNER JOIN Province pr ON dr.ProvinceID = pr.Province_ID
      INNER JOIN DealerProductLine  dpl on dpl.DealerID = di.DealerID
      LEFT JOIN DealerCaliber dc ON dc.CaliberID = di.CaliberID
      WHERE (di.IsExpressDealer = isnull(@IsExpressDealer,IsExpressDealer))
            AND dpl.ProductLineCode = @ProductLineCode AND di.IsActive = 1
      ORDER BY di.DealerName
END




GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByProv_get]    Script Date: 04/13/2012 11:44:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[pr_HondaCommon_DealersByProv_get] (
      @Province varchar(3),
      @Lang char(2),
      @ProductLineCode char(1)
)
AS
BEGIN
      SET NOCOUNT ON;

      SELECT distinct
            di.Export_Key,
            di.DealerID,
            di.Award,
            di.IsExpressDealer,
            di.DealerName,
            di.DealerCode,
            di.PostalCode,
            di.MainPhone,
            di.Email,
            di.HomePage,
            di.Fax,
            ISNULL(di.Street, '') AS Street,
            di.City,
            CASE @Lang WHEN 'en' THEN di.DirectionsEng WHEN 'fr' THEN di.DirectionsFre END AS Directions,
            di.DealerRegionID,
            pr.P_CODE as Province,
            di.PartsPhone,
            di.ServicePhone,
            di.Latitude,
            di.Longitude,
            di.DealerUrlName,
            CASE @Lang WHEN 'en' THEN dc.CaliberNameEng WHEN 'fr' THEN dc.CaliberNameFre END AS CaliberName,
            dpl.ProductLineCode,
            di.IsMUVAuthorizedDealer,
            di.CaliberID                      
      FROM DealerInformation di
      INNER JOIN DealerRegion dr ON di.DealerRegionID =  dr.DealerRegionID
      INNER JOIN Province pr ON dr.ProvinceID = pr.Province_ID   
      INNER JOIN DealerProductLine  dpl on dpl.DealerID = di.DealerID
      LEFT JOIN DealerCaliber dc ON dc.CaliberID = di.CaliberID                    
      WHERE pr.P_CODE= @Province
			AND dpl.ProductLineCode = @ProductLineCode AND di.IsActive = 1      ORDER BY di.DealerName
END



GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByName_get]    Script Date: 04/13/2012 11:44:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE  PROCEDURE [dbo].[pr_HondaCommon_DealersByName_get] (
      @DealerName VARCHAR(100),
      @City VARCHAR(100),
      @Province CHAR(2) = NULL,
      @Lang char(2),
      @ProductLineCode char(1)
)
AS
BEGIN
      SET NOCOUNT ON;

      SET @City = RTRIM(LTRIM(@City))

      SELECT DISTINCT
            di.Export_Key,
            di.DealerID,
            di.Award,
            di.IsExpressDealer,
            di.DealerCode,
            di.DealerName,
            di.MainPhone, 
            di.Fax,
            di.Street,
            di.City,
            di.DealerRegionID,
            pr.P_CODE as Province,
            di.PostalCode,
            di.Email,
            di.PartsEmail,
            di.ServiceEmail,
            di.SalesEmail, 
            di.HomePage,
            CASE @Lang WHEN 'en' THEN di.DirectionsEng WHEN 'fr' THEN di.DirectionsFre END AS Directions,
            di.PartsPhone,
            di.ServicePhone,
            di.OtherPhone,
            di.Latitude,
            di.Longitude,
            di.DealerUrlName,
            CASE @Lang WHEN 'en' THEN dc.CaliberNameEng WHEN 'fr' THEN dc.CaliberNameFre END AS CaliberName,
            dpl.ProductLineCode,
            --di.ProductLineCode,
            di.IsMUVAuthorizedDealer,
            di.CaliberID
      FROM DealerInformation di
      INNER JOIN DealerRegion dr ON di.DealerRegionID = dr.DealerRegionID
      INNER JOIN Province pr ON dr.ProvinceID = pr.Province_ID
      INNER JOIN DealerProductLine  dpl on dpl.DealerID = di.DealerID
	  LEFT JOIN DealerCaliber dc ON dc.CaliberID = di.CaliberID
      WHERE di.Export_Key  = @DealerName 
            AND (REPLACE(REPLACE(REPLACE (di.City,'-',''),'.','') ,'''','') = @City)
            AND (ISNULL(@Province, pr.P_CODE) = pr.P_CODE)
            AND dpl.ProductLineCode = @ProductLineCode
            --AND di.ProductLineCode = @ProductLineCode 
            AND di.IsActive = 1
      ORDER  BY di.DealerName
END



GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByID_get]    Script Date: 04/13/2012 11:44:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Udaya Ratnayake
-- Create date: Mar 1, 2011
-- Description:	gets the delar Information by id
-- =============================================
CREATE  PROCEDURE [dbo].[pr_HondaCommon_DealersByID_get] (
	@ID int,
	@Lang char(2)
)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		di.Export_Key,
		di.DealerID,
		di.Award,
		di.IsExpressDealer,
		di.DealerCode,
		di.DealerName,
		di.MainPhone, 
		di.Fax,
		di.Street,
		di.City, 
		di.DealerRegionID,
		pr.P_CODE as Province,
		di.PostalCode,
		di.Email,
		di.SalesEmail,
        di.PartsEmail,
        di.ServiceEmail, 
		di.HomePage,
		CASE @Lang WHEN 'en' THEN di.DirectionsEng WHEN 'fr' THEN di.DirectionsFre END AS Directions,
		CASE @Lang WHEN 'en' THEN di.DealerInfoEng WHEN 'fr' THEN di.DealerInfoFre END AS DealerInfo,
		CASE @Lang WHEN 'en' THEN dc.CaliberNameEng WHEN 'fr' THEN dc.CaliberNameFre END AS CaliberName,
		di.PartsPhone,
		di.ServicePhone,
		di.Latitude,
		di.Longitude,
		di.DealerUrlName,
		di.IsMUVAuthorizedDealer,
		di.CaliberID
	FROM DealerInformation di
	INNER JOIN DealerRegion dr ON di.DealerRegionID =  dr.DealerRegionID
	INNER JOIN Province pr ON dr.ProvinceID = pr.Province_ID   
	LEFT JOIN DealerCaliber dc ON dc.CaliberID = di.CaliberID                    
	WHERE di.DealerID = @ID
	ORDER BY di.DealerName
END



GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByCoordinates_get]    Script Date: 04/13/2012 11:44:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE  PROCEDURE [dbo].[pr_HondaCommon_DealersByCoordinates_get] (
      @Lat decimal(9, 6),
      @Lon decimal(9, 6),
      @Lang char(2),
      @Unit CHAR (10),
      @RowCount int,
      @ProductLineCode char(1),
      @FilterByPoweHouseDealer bit = 0
)
AS
BEGIN
      SET NOCOUNT ON;

      SELECT TOP(@RowCount)
            di.Export_Key,
            di.DealerID,
            di.Award,
            di.IsExpressDealer,
            di.DealerCode,
            di.DealerName,
            di.MainPhone,
            di.Fax,
            di.Street,
            di.City,
            di.DealerRegionID,
            pr.P_CODE as Province,
            di.PostalCode,
            di.Email,
            di.HomePage,
            CASE @Lang WHEN 'en' THEN di.DirectionsEng WHEN 'fr' THEN di.DirectionsFre END AS Directions,
            di.PartsPhone,
            di.ServicePhone,
            di.Latitude,
            di.Longitude,
            di.DealerUrlName,
            CASE @Lang WHEN 'en' THEN dc.CaliberNameEng WHEN 'fr' THEN dc.CaliberNameFre END AS CaliberName,
            dpl.ProductLineCode,
            di.CaliberID
      FROM DealerInformation di
      INNER JOIN PostalCode pc ON di.PostalCode = pc.PostalCode
      INNER JOIN DealerRegion dr ON di.DealerRegionID = dr.DealerRegionID
      INNER JOIN Province pr ON dr.ProvinceID = pr.Province_ID
      INNER JOIN DealerProductLine  dpl on dpl.DealerID = di.DealerID
      LEFT JOIN DealerCaliber dc ON dc.CaliberID = di.CaliberID
      WHERE  dpl.ProductLineCode = @ProductLineCode AND di.IsActive = 1
                  AND ( CASE @FilterByPoweHouseDealer WHEN 0 THEN '' WHEN 1 THEN dc.CaliberNameEng END)  = 
				( CASE @FilterByPoweHouseDealer WHEN 0 THEN '' WHEN 1 THEN 'Powerhouse' END )

      GROUP BY 
            di.DealerRegionID,
            di.Export_Key,
            di.DealerID,
            di.Award,
            di.IsExpressDealer,
            di.City,
            di.DealerCode,
            di.DealerName,
            di.MainPhone,
            di.Fax,
            di.Street,
            pr.P_CODE,
            di.PostalCode,
            di.Email,
            di.HomePage,
            CASE @Lang WHEN 'en' THEN di.DirectionsEng WHEN 'fr' THEN di.DirectionsFre END,
            di.PartsPhone,
            di.ServicePhone,
            di.Latitude,
            di.Longitude,
            di.DealerUrlName,
            CASE @Lang WHEN 'en' THEN dc.CaliberNameEng WHEN 'fr' THEN dc.CaliberNameFre END,
            dpl.ProductLineCode,
            di.CaliberID
      ORDER BY MIN(dbo.CalculateDistance(pc.Latitude, pc.Longitude, @Lat, @Lon, @Unit))
END



GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByCity_get]    Script Date: 04/13/2012 11:44:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[pr_HondaCommon_DealersByCity_get] (
      @City VARCHAR(100),
      @Province CHAR(2) = NULL,
      @Lang char(2),
      @ProductLineCode char(1),
      @FilterByPoweHouseDealer bit = 0
      
)
AS
BEGIN


      SET NOCOUNT ON;

      SET @City = RTRIM(LTRIM(@City))

      DECLARE @DealerRegionID INT 
      SET @DealerRegionID = null
      SET @DealerRegionID = (SELECT TOP 1 DealerRegionID FROM DealerRegion WHERE ProvinceID = (SELECT TOP 1 Province_ID FROM  Province WHERE P_Code =  ISNULL(@Province, ''))) 

      SELECT DISTINCT
            di.Export_Key, 
            di.DealerID,
            di.Award,
            di.IsExpressDealer,
            di.DealerCode, 
            di.DealerName, 
            di.MainPhone, 
            di.Fax, 
            di.Street, 
            di.City, 
            di.Export_Key,
            di.DealerRegionID, 
            pr.P_CODE as Province,
            di.PostalCode,
            di.Email, 
            di.HomePage,
            CASE @Lang WHEN 'en' THEN di.DirectionsEng WHEN 'fr' THEN di.DirectionsFre END AS Directions,
        di.PartsPhone,
        di.ServicePhone,
            di.Latitude,
            di.Longitude,
            di.DealerUrlName,
            CASE @Lang WHEN 'en' THEN dc.CaliberNameEng WHEN 'fr' THEN dc.CaliberNameFre END AS CaliberName,
            dpl.ProductLineCode,
            --di.ProductLineCode,
            di.IsMUVAuthorizedDealer,
            di.CaliberID
      FROM DealerInformation di 
      INNER JOIN DealerRegion dr ON di.DealerRegionID =  dr.DealerRegionID
      INNER JOIN Province pr ON dr.ProvinceID = pr.Province_ID
      INNER JOIN DealerProductLine  dpl on dpl.DealerID = di.DealerID
	  LEFT JOIN DealerCaliber dc ON dc.CaliberID = di.CaliberID
      WHERE (REPLACE(REPLACE(REPLACE (di.City,'-','') ,'.','') ,'''','')  = @City)        
            AND pr.P_Code = ISNULL(@Province ,pr.P_Code)
            AND dpl.ProductLineCode = @ProductLineCode 
            --AND di.ProductLineCode = @ProductLineCode 
            AND di.IsActive = 1
            AND ( CASE @FilterByPoweHouseDealer WHEN 0 THEN '' WHEN 1 THEN dc.CaliberNameEng END)  = 
				( CASE @FilterByPoweHouseDealer WHEN 0 THEN '' WHEN 1 THEN 'Powerhouse' END )
      ORDER BY di.DealerName
END





GO

/****** Object:  StoredProcedure [dbo].[pr_HondaCommon_DealersByZip_get]    Script Date: 04/13/2012 11:44:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[pr_HondaCommon_DealersByZip_get] (
      @Zip CHAR (6),
      @Lang char(2),
      @RowCount int,
      @ProductLineCode char(1)
)
AS
BEGIN
      SET NOCOUNT ON;
      
      DECLARE @newzip varchar(7)
      DECLARE @Zip_len int
      DECLARE @Unit varCHAR (10)

      SET @Unit = 'Kilometers'
      SET @Zip_len=LEN(@Zip)

      IF @Zip_len>3
            SET @newzip=SUBSTRING(@Zip,1,3)+' '+SUBSTRING(@Zip,4,@Zip_len-3) 
      ELSE 
            SET @newzip=LTRIM(RTRIM(@Zip))      + '%'

      DECLARE     @Lat decimal(9, 6)  -- Will store the latitude of ZIP
      DECLARE     @Lon decimal(9, 6)  -- Will store the longitude of ZIP
      DECLARE @ProvinceCode varchar(4)

      
      SELECT TOP 1 @Lat=Latitude,@Lon=Longitude,@ProvinceCode= ProvinceAbbr FROM PostalCode WHERE PostalCode LIKE @newzip
   	  if(@Lat IS NULL and @Lon IS NULL)
	  if LEN(@newzip) > 3 
		SET @newzip = SUBSTRING(@newzip,1,LEN(@newzip) -1)  + '%'
		SELECT TOP 1 @Lat=Latitude,@Lon=Longitude,@ProvinceCode= ProvinceAbbr FROM PostalCode WHERE PostalCode LIKE @newzip


      SELECT TOP (@RowCount) 
            di.Export_Key,
            dbo.CalculateDistance(pc.Latitude, pc.Longitude, @Lat, @Lon, @Unit) as Distance,
            di.DealerID,
            di.Award,
            di.IsExpressDealer,
            di.DealerName,
            di.DealerCode,
            di.PostalCode,
            di.MainPhone,
            di.Email,
            di.HomePage,
            di.Fax,
            ISNULL(di.Street, '') AS Street,
            di.City,
            di.DealerRegionID,
            pr.P_CODE as Province,
            CASE @Lang WHEN 'en' THEN di.DirectionsEng WHEN 'fr' THEN di.DirectionsFre END AS Directions,
            di.PartsPhone,
            di.ServicePhone,
            di.Latitude,
            di.Longitude,
            di.DealerUrlName,
            CASE @Lang WHEN 'en' THEN dc.CaliberNameEng WHEN 'fr' THEN dc.CaliberNameFre END AS CaliberName,
            dpl.ProductLineCode,
            di.IsMUVAuthorizedDealer,
            di.CaliberID
      FROM DealerInformation di
      LEFT JOIN PostalCode pc ON di.PostalCode = pc.PostalCode
      INNER JOIN DealerRegion dr ON di.DealerRegionID =  dr.DealerRegionID
      INNER JOIN Province pr ON dr.ProvinceID = pr.Province_ID
      INNER JOIN DealerProductLine  dpl on dpl.DealerID = di.DealerID
      LEFT JOIN DealerCaliber dc ON dc.CaliberID = di.CaliberID
      WHERE  dpl.ProductLineCode = @ProductLineCode AND di.IsActive = 1 and pr.P_CODE  = @ProvinceCode
      GROUP BY di.Export_Key,
            pc.Latitude,
            pc.Longitude,
            di.DealerID,
            di.Award,
            di.IsExpressDealer,
            di.DealerName,
            di.DealerCode,
            di.PostalCode,
            di.MainPhone,
            di.Email,
            di.HomePage,
            di.Fax,
            ISNULL(di.Street, ''),
            di.City,
            di.DealerRegionID,
            pr.P_CODE,
            CASE @Lang WHEN 'en' THEN di.DirectionsEng WHEN 'fr' THEN di.DirectionsFre END,
            di.PartsPhone,
            di.ServicePhone,
            di.Latitude,
            di.Longitude, 
            di.DealerUrlName,
            CASE @Lang WHEN 'en' THEN dc.CaliberNameEng WHEN 'fr' THEN dc.CaliberNameFre END,
            dpl.ProductLineCode,
            di.IsMUVAuthorizedDealer,
            di.CaliberID
      ORDER BY Distance
END




GO


