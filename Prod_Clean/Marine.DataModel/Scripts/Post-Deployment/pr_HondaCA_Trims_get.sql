﻿USE [MARV3]
GO
/****** Object:  StoredProcedure [dbo].[pr_HondaCA_Trims_get]    Script Date: 01/15/2015 18:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Nikhil Patel  	
-- Updated on date: Mar 1, 2012  
-- Description: gets trims by productID  
-- =============================================  
  
ALTER PROCEDURE [dbo].[pr_HondaCA_Trims_get]  
@ModelID INT=-1, @TargetID INT, @TrimID INT =-1,@TrimExportKey VARCHAR(50) = null, @Lang CHAR (2),  
 @BaseModelCategory_ID int = null , @BaseModelCategoryURL varchar(50)= null ,   @ModelFamilyURL varchar(50)= null  ,@TopModelYear bit = 0 ,@ModelYear int = 

null
AS  
BEGIN  
   
 SET NOCOUNT ON;  
--Auto_Data_A_Code replaced with ACode as AutoDataACode  
SELECT  distinct      t.Trim_ID AS TrimID,t.ACode AS AutoDataACode, CASE @Lang WHEN 'en' THEN t .T_Name_EN WHEN 'fr' THEN t .T_Name_FR END AS TrimName, 

bm.Export_Key AS BaseModelExportKey,   
						CASE @Lang WHEN 'en' THEN t.T_URL_EN WHEN 'fr' THEN t.T_URL_FR END AS TrimUrl,   
                         CASE @Lang WHEN 'en' THEN t .T_Description_EN WHEN 'fr' THEN t .T_Description_FR END AS TrimDescription,   
                         CASE @Lang WHEN 'en' THEN t .Det_Highlights_EN WHEN 'fr' THEN t .Det_Highlights_FR END AS DETHighlights,   
                         CASE @Lang WHEN 'en' THEN t .Comp_Highlights_EN WHEN 'fr' THEN t .Comp_Highlights_FR END AS CompHighlights, t.Export_key AS 

TrimExportKey,   
                         CASE @Lang WHEN 'en' THEN t .Det_Highlights_Title_EN WHEN 'fr' THEN t .Det_Highlights_Title_FR END AS DetHighlightsTitle,   
                         CASE @Lang WHEN 'en' THEN t .Comp_Highlights_Title_EN WHEN 'fr' THEN t .Comp_Highlights_Title_FR END AS CompHighlightsTitle,   
                         m.ModelYear_Year AS ModelYearYear, CASE @Lang WHEN 'en' THEN bm.BM_Name_EN WHEN 'fr' THEN bm.BM_Name_FR END AS BaseModelName,  
                             (SELECT        TOP (1) Color.Export_Key  
                               FROM            TrimExteriorColor INNER JOIN  
                                                         Color ON TrimExteriorColor.Color_ID = Color.Color_ID  
                               WHERE        (t.Trim_ID = TrimExteriorColor.Trim_ID)  
                               ORDER BY TrimExteriorColor.TEC_Display_Order) AS ColorExportKey, t.T_Display_Order AS TrimDisplayOrder, 

ModelYear.Is_Visible_Flag AS IsVisibleFlag   
                         ,FactoryOption.Transmission_Type AS TransmissionType  
      , FactoryOption.Export_Key AS TransmissionExportKey,   
                         CASE @Lang WHEN 'fr' THEN FactoryOption.Transmission_Spec_FR ELSE FactoryOption.Transmission_Spec_EN END AS TransmissionSpec,   
                         BP.MSRP  
      , @Lang AS Language
      , t.Freight_PDI_Cost as FreightPDI    
      ,( SELECT BMC_NAME_EN FROM  BaseModelCategory BMC WHERE BMC.BaseModelCategory_ID= T.BaseModelCategory_ID )  
      ,t.BaseModelCategory_ID
       ,CASE @Lang WHEN 'en' THEN bmf.BaseModelFamily_URL_EN WHEN 'fr' THEN bmf.BaseModelFamily_URL_FR END AS ModelFamilyURL
       --,CASE @Lang WHEN 'en' THEN bmc.BMC_URL_EN  WHEN 'fr' THEN bmc.BMC_URL_FR END AS ModelCategoryURL
       ,bm.BM_Display_Order
       ,(
		SELECT TOP 1 CASE ISNUMERIC(div.div_value) when 1 then CAST (div.div_value as decimal)else 0 end
		FROM 
		dataitem di 
		inner join dataitemvalue div on div.dataitem_id=di.dataitem_id
		inner join financedata fd on fd.financedata_id=div.financedata_id
		inner join factoryoption fo on fo.factoryoption_id=fd.factoryoption_id
		WHERE 
		di.Di_name='Price Discount Amt' and getdate() between fd.fd_start_date and fd.fd_end_date--or di.Di_name='Price Discount Per' 
		AND fo.FactoryOption_ID = FactoryOption.FactoryOption_ID
       ) AS PriceDiscountAmount
        ,(
		SELECT TOP 1 CASE ISNUMERIC(div.div_value) when 1 then CAST (div.div_value as decimal)else 0 end
		FROM 
		dataitem di 
		inner join dataitemvalue div on div.dataitem_id=di.dataitem_id
		inner join financedata fd on fd.financedata_id=div.financedata_id
		inner join factoryoption fo on fo.factoryoption_id=fd.factoryoption_id
		WHERE 
		di.Di_name='Price Discount Amt' and getdate() between fd.fd_start_date and fd.fd_end_date--or di.Di_name='Price Discount Per' 
		AND fo.FactoryOption_ID = FactoryOption.FactoryOption_ID
       ) AS PriceDiscountPercentage
FROM            Model AS m INNER JOIN  
                         Trim AS t ON m.Model_ID = t.Model_ID INNER JOIN  
                         TrimVisibility AS tv ON t.Trim_ID = tv.Trim_ID INNER JOIN  
                         BaseModel AS bm ON m.BaseModel_ID = bm.BaseModel_ID INNER JOIN  
                         ModelYear ON m.ModelYear_Year = ModelYear.ModelYear_Year   
      INNER JOIN FactoryOption ON t.Trim_ID = FactoryOption.Trim_ID   
      INNER JOIN (select tm.trim_ID, bm.BaseModel_ID, min(FactoryOption.MSRP) AS MSRP from   Model AS m INNER JOIN  
									Trim AS tm ON m.Model_ID = tm.Model_ID INNER JOIN  
									TrimVisibility AS tv ON tm.Trim_ID = tv.Trim_ID INNER JOIN  
									BaseModel AS bm ON m.BaseModel_ID = bm.BaseModel_ID INNER JOIN 
									FactoryOption ON tm.Trim_ID = FactoryOption.Trim_ID 	
					GROUP By tm.trim_ID,bm.BaseModel_ID
				) AS  BP  ON t.trim_ID = BP.Trim_ID AND FactoryOption.MSRP = BP.MSRP
      INNER JOIN BaseModelFamily bmf ON bm.BaseModelFamily_ID = bmf.BaseModelFamily_ID  
                         --INNER JOIN FactoryOptionVisibility ON FactoryOption.FactoryOption_ID = FactoryOptionVisibility.FactoryOption_ID  
                         LEFT JOIN BaseModel_BaseModelCategory bmbmc ON bmbmc.BaseModel_ID = bm.BaseModel_ID  
                         LEFT JOIN BaseModelCategory BMC ON  bmc.BaseModelCategory_ID   = bmbmc.BaseModelCategory_ID --isnull(t.BaseModelCategory_ID, -1 )  
                         WHERE          
                         (m.Model_ID = case @ModelID when -1 then m.model_id else @ModelID end) AND   
                         (tv.Target_ID = @TargetID) and   
                          (ModelYear.Is_Visible_Flag = 1)  AND 
                          t.Trim_ID =  (CASE @TrimID WHEN -1 THEN  t.Trim_ID  ELSE @TrimID  END ) AND  t.Export_key =  isnull(@TrimExportKey,t.Export_key) 

and   
                         m.ModelYear_Year =  isnull(@ModelYear,m.ModelYear_Year) and
                         --FactoryOptionVisibility.Target_ID = @TargetID AND   
            isnull(t.BaseModelCategory_ID, -11) =  ISNULL(@BaseModelCategory_ID, isnull(t.BaseModelCategory_ID,-11))  
           --replace bmc.Export_Key with bmc.BMC_URL_EN  bmc.BMC_URL_FR  
            AND  CASE @Lang WHEN 'en' THEN isnull(bmc.BMC_URL_EN , '')  WHEN 'fr' THEN  isnull( bmc.BMC_URL_FR, '') END   =   
           CASE @Lang WHEN 'en' THEN ISNULL(@BaseModelCategoryURL, isnull(bmc.BMC_URL_EN, '')) WHEN 'fr' THEN ISNULL(@BaseModelCategoryURL, isnull

(bmc.BMC_URL_FR, '')) END  
  
            AND  CASE @Lang WHEN 'en' THEN isnull(bmf.BaseModelFamily_URL_EN , '')  WHEN 'fr' THEN  isnull( bmf.BaseModelFamily_URL_FR, '') END   =   
           CASE @Lang WHEN 'en' THEN ISNULL(@ModelFamilyURL, isnull(bmf.BaseModelFamily_URL_EN , '')) WHEN 'fr' THEN ISNULL(@ModelFamilyURL, isnull

(bmf.BaseModelFamily_URL_FR, '')) END  
           AND 
           CASE @TopModelYear WHEN  0 then (0)  WHEN 1 THEN (m.ModelYear_Year) end   = 
			CASE @TopModelYear WHEN  0 then (0)  WHEN 1 THEN 
			(			
			SELECT max(my2.ModelYear_Year)FROM ModelYear as my2
			INNER JOIN Model m2 ON m2.ModelYear_Year = my2.ModelYear_Year INNER JOIN  
			BaseModel AS bm2 ON m2.BaseModel_ID = bm2.BaseModel_ID INNER JOIN 
			Trim t2 ON t2.Model_ID = m2.Model_ID INNER JOIN  
			TrimVisibility AS tv2 ON t2.Trim_ID = tv2.Trim_ID INNER JOIN 
			FactoryOption fo2 ON t2.Trim_ID = fo2.Trim_ID INNER JOIN 
			BaseModelFamily bmf2 ON bm2.BaseModelFamily_ID = bmf2.BaseModelFamily_ID LEFT JOIN 
			BaseModel_BaseModelCategory bmbmc2 ON bmbmc2.BaseModel_ID = bm2.BaseModel_ID LEFT JOIN
			BaseModelCategory BMC2 ON  bmc2.BaseModelCategory_ID   = bmbmc2.BaseModelCategory_ID
			WHERE t2.T_Name_EN = t.T_Name_EN AND tv2.Target_ID = tv.Target_ID AND my2.Is_Visible_Flag = 1 AND
            CASE @Lang WHEN 'en' THEN isnull(bmc2.BMC_URL_EN , '')  WHEN 'fr' THEN  isnull( bmc2.BMC_URL_FR, '') END   =   
			CASE @Lang WHEN 'en' THEN ISNULL(@BaseModelCategoryURL, isnull(bmc2.BMC_URL_EN, '')) WHEN 'fr' THEN ISNULL(@BaseModelCategoryURL, 

isnull(bmc2.BMC_URL_FR, '')) END  
              AND  CASE @Lang WHEN 'en' THEN isnull(bmf2.BaseModelFamily_URL_EN , '')  WHEN 'fr' THEN  isnull( bmf2.BaseModelFamily_URL_FR, '') END   =   
           CASE @Lang WHEN 'en' THEN ISNULL(@ModelFamilyURL, isnull(bmf2.BaseModelFamily_URL_EN , '')) WHEN 'fr' THEN ISNULL(@ModelFamilyURL, isnull

(bmf2.BaseModelFamily_URL_FR, '')) END  

			
			GROUP BY  t2.T_Name_EN 
			
			) END
  
ORDER BY bm.BM_Display_Order, TrimDisplayOrder  
  
END  

