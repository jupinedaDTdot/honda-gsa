﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*

Following stored procedure updated to add discount amount and discount percentage field.

*/

USE [MARV3]
GO
/****** Object:  StoredProcedure [dbo].[pr_HondaCA_TransmissionType_get]    Script Date: 05/25/2012 14:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[pr_HondaCA_TransmissionType_get]
@ModelID INT=null, @TrimID INT=null, @TargetID INT, @Lang CHAR (2)
AS
BEGIN
	
	SET NOCOUNT ON;

SELECT      t.Trim_ID AS TrimID, f.FactoryOption_ID AS FactoryOptionID, f.Transmission_Type AS TransmissionType, f.Export_Key AS ExportKey, 
CASE @Lang WHEN 'fr' then f.Transmission_Spec_FR ELSE f.Transmission_Spec_EN END AS TransmissionSpec, @Lang AS Language,
f.MSRP as MSRP , f.FO_Model_Code
			,(
				SELECT TOP 1 CASE ISNUMERIC(div.div_value) when 1 then CAST (div.div_value as decimal)else 0 end
				FROM 
				dataitem di 
				inner join dataitemvalue div on div.dataitem_id=di.dataitem_id
				inner join financedata fd on fd.financedata_id=div.financedata_id
				inner join factoryoption fo on fo.factoryoption_id=fd.factoryoption_id
				WHERE 
				di.Di_name='Price Discount Amt' and getdate() between fd.fd_start_date and fd.fd_end_date --or di.Di_name='Price Discount Per' 
				AND fo.FactoryOption_ID = f.FactoryOption_ID
			   ) AS PriceDiscountAmount
			   ,(
				SELECT TOP 1 CASE ISNUMERIC(div.div_value) when 1 then CAST (div.div_value as decimal)else 0 end
				FROM 
				dataitem di 
				inner join dataitemvalue div on div.dataitem_id=di.dataitem_id
				inner join financedata fd on fd.financedata_id=div.financedata_id
				inner join factoryoption fo on fo.factoryoption_id=fd.factoryoption_id
				WHERE 
				di.Di_name='Price Discount Amt' and getdate() between fd.fd_start_date and fd.fd_end_date --or di.Di_name='Price Discount Per' 
				AND fo.FactoryOption_ID = f.FactoryOption_ID
			   ) AS PriceDiscountPercentage
FROM         FactoryOption f
					  INNER JOIN Trim AS t ON f.Trim_ID = t.Trim_ID
					  INNER JOIN TrimVisibility AS tv ON t.Trim_ID = tv.Trim_ID 
					  INNER JOIN Model AS m ON m.Model_ID = t.Model_ID 
                      INNER JOIN BaseModel AS bm ON m.BaseModel_ID = bm.BaseModel_ID 
                      INNER JOIN ModelYear ON m.ModelYear_Year = ModelYear.ModelYear_Year 
                      INNER JOIN FactoryOption ON t.Trim_ID = FactoryOption.Trim_ID 
					  --INNER JOIN FactoryOptionVisibility ON f.FactoryOption_ID = FactoryOptionVisibility.FactoryOption_ID
WHERE     (m.Model_ID = isnull(@ModelID,m.Model_ID)) AND (tv.Target_ID = @TargetID) AND (ModelYear.Is_Visible_Flag = 1) AND (t.Trim_ID = isnull(@TrimID,t.Trim_ID) )
GROUP BY t.Trim_ID,f.FactoryOption_ID,f.Transmission_Type,f.Export_Key,f.Transmission_Spec_EN,f.Transmission_Spec_FR,f.MSRP,f.FO_Model_Code,f.FO_Display_Order
ORDER BY f.FO_Display_Order,MSRP



END

GO


ALTER PROCEDURE [dbo].[pr_HondaCA_Trim_WithBasePrice_get]
 @TargetID INT, @ModelFamilyName varchar(50)=null,@BaseModel_ID int =-1, @ModelFamilyUrl varchar(50)=null, @Lang CHAR (2),
 @BMCategoryURL varchar(50)=null
AS
BEGIN

	

		SELECT distinct	t.Trim_ID AS TrimID, t.ACode AS AutoDataACode, t.Export_key AS TrimExportKey, t.T_Display_Order AS TrimDisplayOrder,
					CASE @Lang WHEN 'en' THEN t .T_Name_EN WHEN 'fr' THEN t .T_Name_FR END AS TrimName, 
					CASE @Lang WHEN 'en' THEN t.T_URL_EN WHEN 'fr' THEN t.T_URL_FR END AS TrimUrl,   
                    CASE @Lang WHEN 'en' THEN t .T_Description_EN WHEN 'fr' THEN t .T_Description_FR END AS TrimDescription,   
                    CASE @Lang WHEN 'en' THEN t .Det_Highlights_EN WHEN 'fr' THEN t .Det_Highlights_FR END AS DETHighlights,   
                    CASE @Lang WHEN 'en' THEN t .Comp_Highlights_EN WHEN 'fr' THEN t .Comp_Highlights_FR END AS CompHighlights, 
					CASE @Lang WHEN 'en' THEN t .Det_Highlights_Title_EN WHEN 'fr' THEN t .Det_Highlights_Title_FR END AS DetHighlightsTitle,   
                    CASE @Lang WHEN 'en' THEN t .Comp_Highlights_Title_EN WHEN 'fr' THEN t .Comp_Highlights_Title_FR END AS CompHighlightsTitle,   
                    m.ModelYear_Year AS ModelYearYear, ModelYear.Is_Visible_Flag AS IsVisibleFlag, bm.Export_Key AS BaseModelExportKey,
                    CASE @Lang WHEN 'en' THEN bm.BM_Name_EN WHEN 'fr' THEN bm.BM_Name_FR END AS BaseModelName,
                    ( SELECT BMC_NAME_EN FROM  BaseModelCategory BMC WHERE BMC.BaseModelCategory_ID= T.BaseModelCategory_ID ),
                    CASE @Lang WHEN 'en' THEN bmf.BaseModelFamily_URL_EN WHEN 'fr' THEN bmf.BaseModelFamily_URL_FR END AS ModelFamilyURL,
                    (SELECT	TOP (1) Color.Export_Key  FROM TrimExteriorColor INNER JOIN  Color ON TrimExteriorColor.Color_ID = Color.Color_ID  
                               WHERE        (t.Trim_ID = TrimExteriorColor.Trim_ID)  
                               ORDER BY TrimExteriorColor.TEC_Display_Order
					) AS ColorExportKey,
                    bm.BM_Display_Order,                    
                    t.BaseModelCategory_ID,
					(select top 1 min(fo.MSRP) AS MSRP from   Model AS m INNER JOIN  
							Trim AS tm ON m.Model_ID = tm.Model_ID INNER JOIN  
							TrimVisibility AS tv ON tm.Trim_ID = tv.Trim_ID INNER JOIN  
							BaseModel AS bm ON m.BaseModel_ID = bm.BaseModel_ID INNER JOIN 
							FactoryOption fo ON tm.Trim_ID = fo.Trim_ID 	
						WHERE t.Trim_ID = fo.Trim_ID 
					) AS MSRP,
					(
		SELECT TOP 1 CASE ISNUMERIC(div.div_value) when 1 then CAST (div.div_value as decimal)else 0 end
		FROM 
		dataitem di 
		inner join dataitemvalue div on div.dataitem_id=di.dataitem_id
		inner join financedata fd on fd.financedata_id=div.financedata_id
		inner join factoryoption fo on fo.factoryoption_id=fd.factoryoption_id
		WHERE 
		di.Di_name='Price Discount Amt' and getdate() between fd.fd_start_date and fd.fd_end_date --or di.Di_name='Price Discount Per' 
		AND fo.Trim_ID = t.Trim_ID
       ) AS PriceDiscountAmount,
        (
		SELECT TOP 1 CASE ISNUMERIC(div.div_value) when 1 then CAST (div.div_value as decimal)else 0 end
		FROM 
		dataitem di 
		inner join dataitemvalue div on div.dataitem_id=di.dataitem_id
		inner join financedata fd on fd.financedata_id=div.financedata_id
		inner join factoryoption fo on fo.factoryoption_id=fd.factoryoption_id
		WHERE 
		di.Di_name='Price Discount Amt' and getdate() between fd.fd_start_date and fd.fd_end_date --or di.Di_name='Price Discount Per' 
		AND fo.Trim_ID = t.Trim_ID
       ) AS PriceDiscountPercentage  ,     
					@Lang AS Language
					       
		FROM    [BaseModel] bm 		
			INNER JOIN model m on m.BaseModel_ID= bm.BaseModel_ID
			INNER JOIN Trim t on t.Model_ID = m.Model_ID
			INNER JOIN TrimVisibility AS tv ON t.Trim_ID = tv.Trim_ID
			INNER JOIN ModelYear ON m.ModelYear_Year = ModelYear.ModelYear_Year				
			--INNER JOIN FactoryOption ON t.Trim_ID = FactoryOption.Trim_ID		
			INNER JOIN [BaseModelFamily] bmf ON bm.BaseModelFamily_ID=bmf.BaseModelFamily_ID  
			LEFT JOIN BaseModel_BaseModelCategory bmbmc ON bmbmc.BaseModel_ID = bm.BaseModel_ID
			LEFT JOIN [BaseModelCategory] bmc ON BMC.BaseModelCategory_ID = t.BaseModelCategory_ID		
			
		
		WHERE	bm.BaseModel_ID = ( CASE isnull(@BaseModel_ID,-1) WHEN -1 THEN  bm.BaseModel_ID ELSE @BaseModel_ID  END ) AND
			CASE @Lang WHEN 'en' THEN isnull(bmc.BMC_URL_EN , '')  WHEN 'fr' THEN  isnull( bmc.BMC_URL_FR, '') END   =   
			CASE @Lang WHEN 'en' THEN ISNULL(@BMCategoryURL, isnull(bmc.BMC_URL_EN, '')) WHEN 'fr' THEN ISNULL(@BMCategoryURL, isnull(bmc.BMC_URL_FR, '')) END AND  
			(CASE @Lang WHEN 'en' THEN BMF.BaseModelFamily_Name_EN WHEN 'fr' THEN isnull(BMF.BaseModelFamily_Name_FR,'')  END ) = 
			(CASE @Lang WHEN 'en' THEN ISNULL(@ModelFamilyName,BMF.BaseModelFamily_Name_EN ) WHEN 'fr' THEN ISNULL(@ModelFamilyName,isnull(BMF.BaseModelFamily_Name_FR,'')) END ) AND
			(CASE @Lang WHEN 'en' THEN isnull(BMF.BaseModelFamily_URL_EN,'') WHEN 'fr' THEN  isnull(BMF.BaseModelFamily_URL_FR,'') END ) = 
			(CASE @Lang WHEN 'en' THEN ISNULL(@ModelFamilyUrl, isnull(BMF.BaseModelFamily_URL_EN,'') ) WHEN 'fr' THEN ISNULL(@ModelFamilyUrl, isnull(BMF.BaseModelFamily_URL_FR,'')) END )            
			AND bmc.BMC_NAME_EN != 'other'
					
	
		ORDER By BM_Display_Order, TrimName
END

GO