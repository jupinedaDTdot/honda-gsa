// $Id: jquery.honda.match_landing.js 15528 2011-08-04 14:49:21Z yuxhuang $

;(function($){

var defaults = {
	lang: 'fr',
	navigationController: null,
	url: '/json/GetProvince.aspx',
	submitSelector: 'a.submit',
	postalCodeSelector: 'input.match_postal',
	formSelector: 'form',
	_last: null
};

$.widget('honda.match_landing', {
	widgetEventPrefix: 'match_landing_',
	
	options: defaults,
	
	_create: function() {
		var self = this, o = this.options;
		
		// success
		function did_finish(event, data, status, xhr) {
			// if not successful
			if (data.exceptions != null) {
				did_finish_with_error(data, status, xhr);
				return;
			}
			
			// successful
			var province = data['province-key'].value;
			$.cookie('hondaprovince', province);
			
			self._complete(event, {});
			
			if (o.navigationController) {
				o.navigationController.expectViewController({
					currentState: 'landing',
					returnValue: true
				});
			}
		}
		
		function did_finish_with_error(event, data, status, xhr) {
			// show error message
			self._error(event, {
				exception: data.exceptions
			});
		}
		
		// form selector
		$(o.formSelector).submit(function(event) {
			// build data
			var data = {
				Lang: o.lang,
				Data: $.toJSON({
					'keys': [
						'zip-code'
					],
					'zip-code': {
						value: $(o.postalCodeSelector, self.element).val().toString().replace(' ', '')
					}
				})
			};
			
			// send the request
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: o.url,
				global: false,
				data: data,
				success: function(data, status, xhr) {
					did_finish.apply(this, [event, data, status, xhr]);
				},
				error: function(data, status, xhr) {
					did_finish_with_error.apply(this, [event, data, status, xhr]);
				}
			});
			return false;
		});
		
		// submit button is clicked
		$(o.submitSelector, self.element).click(function() {
			$(o.formSelector).submit();
			return false;
		});
		
		// on focus of input
		$(o.postalCodeSelector, self.element).focus(function() {
			$(this).val('');
		});
	},
	
	_complete: function(event, ui) {
		this._trigger('complete', event, ui);
	},
	
	_error: function(event, ui) {
		this._trigger('error', event, ui);
	}
});

})(jQuery);

