;(function($){

var defaults = {
	
};

$.widget('honda.future_vehicles', {
	widgetEventPrefix: 'honda_future_vehicles_',
	options: defaults,
	
	_create: function() {
		var self = this, o = this.options, element = this.element;
		
		// video player support
	    $('.future_hero_videos').each(function() {
	        var self = $(this);
	        var options = $.extend(true, {}, $.fn.linkrel.default_options, global_get('player.standard'));
	        // set the width and height according to the container
	        var width = self.width();
	        var height = self.height();
	        if (!self.hasClass('hero') && width > 0 && height > 0) {
	        	options.width = width;
	        	options.height = height;
	        }
	        var url = self.attr('title');
			    var share_url = self.find('a.share').attr('href');
	      
	        if (url.match(/\.(flv|mp4|f4v|m4v)$/i)) {
	        	options.flashvars.xml_location = '';
	        	options.flashvars.video_location = url;
	          if(share_url != ''){
	            options.flashvars.share_link = share_url;
	          }else{
	            options.flashvars.share_link = document.location.href;
	          }
	        }
	        else {
		        options.flashvars.xml_location = url;
		        options.flashvars.video_location = '';
				if(share_url != ''){
					options.flashvars.share_link = share_url;
				}else{
					options.flashvars.share_link = document.location.href;
				}
	        }
			if ($.flash.available) {
		        self.empty().flash(options);
		        self.removeAttr('title');
			}
			else {
				self.html('You need to upgrade your Adobe Flash Player to watch this video. <br /><a href="http://get.adobe.com/flashplayer/" rel="external nofollow">Download it from Adobe</a>.');
			}
	    }).addClass('video_player_processed hero_video_showcase');
		
		// photo slideshow support
		$.ajax({
			url: $('#hero_photos').attr('title'),
			global: false,
			type: 'GET',
			dataType: 'xml',
			success: function(data, status, xhr) {
				
				$('.honda_slideshow_player_support').image_slideshow({
					target: '#hero_photos',
					caption: '.tout_caption',
					dataSource: function() {
						$('#hero_photos').removeAttr('title');
						var self = this, element = this.element, o = this.options;
						var d = $.map($(data).find('tout'), function(el) {
							return {
								url: $('url', el).text(),
								caption: $('caption', el).text()
							};
						});
						return d;
					},
					seek: function(event, ui) {
						var slideshow = $(this).data('image_slideshow');
						$('.desc .index', slideshow.element).text(slideshow.index+1);
					},
					complete: function(event, ui) {
						var slideshow = $(this).data('image_slideshow');
						$('.desc .index', slideshow.element).text(slideshow.index+1);
						$('.desc .total', slideshow.element).text(slideshow.dataSource.length);
					}
				});
				
				$('#hero_photos').mouseover(function() {
					$('.image-slideshow-buttons', this).stop().clearQueue().css('opacity', 1).show();
				}).mouseout(function() {
					$('.image-slideshow-buttons', this).stop().clearQueue().hide();
				});
			}
		});
		
		$('.future_hero_link', element).click(function() {
			var self = $(this);
			
			if (self.hasClass('selected')) {
				$('.future_hero_support').hide();
				$('.future_hero_default').show();
				$('.future_hero_link').removeClass('selected');
			}
			else {
				$('.future_hero_support').hide();
				$(self.attr('rel')).show();
				if (self.attr('rel') == '.future_hero_photos') {
					// $('#hero_photos .image-slideshow-buttons:visible').delay(2000).queue(function() {
					// 	$(this).fadeOut('slow').dequeue();
					// });
				}
				$('.future_hero_link').removeClass('selected');
				self.addClass('selected');
			}
			
			return false;
		});

	}
});

})(jQuery);