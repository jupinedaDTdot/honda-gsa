;(function($, F, undefined) {

var _defaults = {
    url: '',
    name: '',
    type: 'POST',
    submitHandler: function(ids) {
        var o = this.options;
        var form = $('<form>').append(
                $('<input>').attr({
                    type: 'hidden',
                    name: o.name,
                    value: o.valueBuilder.apply(this, [ids])
                })
                ).attr({
                    action: o.url,
                    method: o.type
                }).appendTo('body');
        form.submit();
    },
    valueBuilder: function(arr) {
        return arr.join(',');
    },
    max: 4
};

$.widget('honda.CompareWidget', {
    widgetEventPrefix: 'CompareWidget_',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('honda.CompareWidget.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        // create an array of model ids
        this.compareIDs = [];
    },
    add: function(id, object) {
        var o = this.options;

        if (this.compareIDs.indexOf(id) >= 0) {
            throw new Error('ID: ' + id + ' already exists.');
        }

        if (this.size() >= o.max) {
            throw new Error('Cannot exceed ' + o.max + ' models.');
        }

        if (this.compareIDs.indexOf(id) === -1) {
            this.compareIDs.push(id);
            this._add(null, {
                widget: this.widget(),
                id: id,
                object: object
            });
        }
    },
    remove: function(id, object) {
        var index = this.compareIDs.indexOf(id);
        if (index < 0) {
            return undefined;
        }

        // delete the ID from the array
        this.compareIDs.splice(index, 1);
        this._remove(null, {
            widget: this.widget(),
            id: id,
            object: object
        });
    },
    size: function() {
        return this.compareIDs.length;
    },
    submit: function() {
        var o = this.options;
        o.submitHandler.apply(this, [this.compareIDs]);
    },
    _add: function(event, ui) {
        this._trigger('add', event, ui);
    },
    _remove: function(event, ui) {
        this._trigger('remove', event, ui);
    }
});

})(jQuery, window);