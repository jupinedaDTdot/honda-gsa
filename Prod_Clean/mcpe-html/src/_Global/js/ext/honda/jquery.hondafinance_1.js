﻿/* $Id: jquery.hondafinance.js 26863 2013-10-21 16:12:33Z margaret $ */
/*
Request:

Lang=fr&Data={
"lease": {
"additional_cost_per_km": 0.08,
"term_options": [
24,
30,
36,
42,
48,
54,
60
],
"apr": 2.9,
"term_options_default": 48,
"value": 244.89,
"buyout": 9752.4,
"additional_annual_km": 0,
"est_annual_km": 24000,
"down_payment": 5555,
"est_balance": 18520,
"est_monthly_payment": 244.89,
"amount_on_delivery": 6552.7
},
"finance": {
"amount_on_delivery": 0,
"down_payment": 0,
"apr": 0.9,
"value": 464.38,
"est_balance": 24075,
"est_annual_km": 0,
"term_options_default": 60,
"est_monthly_payment": 464.38,
"term_options": [
24,
30,
36,
42,
48,
54,
60,
72
]
},
"include_fees": {
"value": true
},
"exceptions": null,
"cash_purchase": {
"value": 24075
},
"selected_keys": {
"transmission_key": "725-Manual",
"model_key": "civic_sedan",
"term_options_finance": 60,
"trim_level_key": "exl",
"province_key": "ON",
"warranty": null,
"term_options_lease": 48,
"accessories": null
},
"msrp": {
"include_fees": false,
"value": 24075
},
"keys": [
"finance",
"lease",
"msrp",
"cash_purchase",
"include_fees",
"selected_keys"
]
}

Response:
{
"keys": [
"finance",
"lease",
"msrp",
"cash_purchase",
"include_fees",
"selected_keys"
],
"finance": {
"value": 406.54,
"down_payment": 0.0,
"term_options": [
24,
30,
36,
42,
48,
54,
60,
72
],
"term_options_default": 60,
"est_balance": 24075.0,
"apr": 2.4,
"est_annual_km": 0.0,
"est_monthly_payment": 406.54,
"amount_on_delivery": 0.0
},
"lease": {
"value": 263.62,
"down_payment": 5555.0,
"term_options": [
24,
30,
36,
42,
48,
54,
60
],
"term_options_default": 48,
"est_balance": 18520.0,
"apr": 1.9,
"est_annual_km": 24000.0,
"additional_annual_km": 0.0,
"additional_cost_per_km": 0.08,
"buyout": 12247.2,
"est_monthly_payment": 263.62,
"amount_on_delivery": 6571.43
},
"msrp": {
"value": 24075.0,
"include_fees": false
},
"cash_purchase": {
"value": 24075.0
},
"include_fees": {
"value": true
},
"selected_keys": {
"transmission_key": "725-Manual",
"model_key": "civic_sedan",
"trim_level_key": "exl",
"province_key": "ON",
"term_options_lease": 30,
"term_options_finance": 72,
"accessories": null,
"warranty": null
}
}
*/

; (function ($) {

	function fill_in_blanks(type) {

		if (type != 'finance') {
			// lease down payment
			$('#lease_down_payment').attr('placeholder', '------');
			// lease est balance
			$('#lease_est_balance').text('------');
			// lease apr
			$('#lease_apr').text('------');
			// lease effective apr
			$('#lease_effective_apr').text('------');
			// lease annual km
			$('#lease_est_annual_km').text('------');
			// lease buyout
			$('#lease_buyout').text('------');
			// lease monthly payment
			$('#lease_est_monthly_payment').text('------');
			// lease amount on delivery
			$('#lease_amount_on_delivery').text('------');
			$('.lease_amount').text('------');
			$('#lease_summary tbody').empty();
			$('#lease_rate_period').text('');
		}

		if (type != 'lease') {
			// finance down payment
			$('#finance_down_payment').attr('placeholder', '------');
			// additional km
			$('#lease_additional_annual_km').attr('placeholder', '------');
			// finance est balance
			$('#finance_est_balance').text('------');
			// finance apr
			$('#finance_apr').text('------');
			// finance effective apr
			$('#finance_effective_apr').text('------');
			// finance monthly payment
			$('#finance_est_monthly_payment').text('------');
			// finance amount on delivery
			$('#finance_amount_on_delivery').text('------');
			// msrp
			$('.msrp_amount').text('------');
			$('.cash_purchase_amount').text('------');
			$('.finance_amount').text('------');
			$('#finance_summary tbody').empty();
			$('#finance_rate_period').text('');
		}

	}

	function clear_summary() {
	}

	$.fn.hondafinance = function (options) {
		// initialize
		function init() {
			// initialize options
			var form = $('form');
			var opts = $.extend({}, $.fn.hondafinance.defaults, options, true);
			var trimData_ = null;
			var $loader = $('.loader', this);

			function load_summary(params, types) {
				if (opts.enableSummary) {
					$loader.show();
					var summaryUrl = $.isFunction(opts.summaryUrl) ? opts.summaryUrl.apply(form) : opts.summaryUrl;
					$.ajax({
						type: 'POST',
						url: summaryUrl,
						dataType: 'json',
						global: false,
						data: {
							lang: $.isFunction(opts.lang) ? opts.lang.apply(form) : opts.lang,
							data: $.toJSON(params)
						},
						success: function (data, status, xhr) {
							clear_summary.apply(form, [opts, types]);
							opts.create_summary.apply(form, [data, opts, types]);
							//$('a[href="#payment_options"]').click();
							$.behaviors.cufon_refresh();
							$loader.hide();
						},
						error: function () {
							clear_summary.apply(form, [opts]);
							$.behaviors.cufon_refresh();
							$loader.hide();
						}
					});
				}
			}
			function loadModel() {
				var url = typeof (opts.msrpUrl) == 'function' ? opts.msrpUrl.apply(form) : opts.msrpUrl;

				if (!url) {
					return false;
				}
				console.log("loadmodel", url);
				$loader.show();
				// ajax call
				$.ajax({
					type: 'GET',
					url: url,
					dataType: 'json',
					global: false,
					data: {
					},
					success: function (data, status, xhr) {
						var params = data;
						trimData_ = $.extend(true, {}, data); // copy over the trim data
						opts.populate.apply(form, [data, opts]);
						$loader.hide();
						var types = [];
						if (data.lease && data.lease.apr <= opts.threshold) {
							types.push('lease');
						}
						if (data.finance && data.finance.apr <= opts.threshold) {
							types.push('finance');
						}
						load_summary(params, types);
					},
					error: function () {
						fill_in_blanks();
						$loader.hide();
					}
				});

				return false;
			}

			$(opts.vehicleSelector).change(loadModel);
			$(opts.provinceSelector).change(loadModel);


			// collect parameters from the form
			function collect() {
				var data = {
					keys: ["finance", "lease", "msrp", "cash_purchase", "include_fees", "selected_keys"],
					selected_keys: {
						transmission_key: (typeof (opts.transmission) == 'function' ? opts.transmission.apply(form) : opts.transmission) || trimData_.selected_keys.transmission_key,
						model_key: (typeof (opts.model) == 'function' ? opts.model.apply(form) : opts.model) || trimData_.selected_keys.model_key,
						model_year: (typeof (opts.modelYear) == 'function' ? opts.modelYear.apply(form) : opts.modelYear) || trimData_.selected_keys.model_year,
						term_options_finance: parseInt(typeof (opts.financeTerm) == 'function' ? opts.financeTerm.apply(form) : opts.financeTerm) || (trimData_.finance && trimData_.finance.term_options_default) || 48,
						trim_level_key: (typeof (opts.trimLevel) == 'function' ? opts.trimLevel.apply(form) : opts.trimLevel) || trimData_.selected_keys.trim_level_key,
						province_key: (typeof (opts.province) == 'function' ? opts.province.apply(form) : opts.province) || trimData_.selected_keys.province_key || $.cookie('hondaprovince'),
						warranty: null,
						term_options_lease: parseInt(typeof (opts.leaseTerm) == 'function' ? opts.leaseTerm.apply(form) : opts.leaseTerm) || (trimData_.lease ? trimData_.lease.term_options_default : 48),
						accessories: null
					},
					lease: {
						// term_options: [24, 30, 36, 42, 48, 54, 60],
						// additional_cost_per_km: 0.08,
						// apr: 2.9,
						// term_options_default: 48,
						// value: 244.89,
						// buyout: 9752.4,
						additional_annual_km: parseInt(typeof (opts.leaseAdditionalAnnualKM) == 'function' ? opts.leaseAdditionalAnnualKM.apply(form) : opts.leaseAdditionalAnnualKM) || (trimData_.lease ? trimData_.lease.additional_annual_km : 0)
						// est_annual_km: 24000,
						// down_payment: 5555,
					, down_payment: parseFloat(typeof (opts.leaseDownPayment) == 'function' ? opts.leaseDownPayment.apply(form) : opts.leaseDownPayment) || (trimData_.lease ? trimData_.lease.down_payment : 0)
						// est_balance: 18520,
						// est_monthly_payment: 244.89,
						// amount_on_delivery: 6552.7
					},
					finance: {
						// term_options: [24, 30, 36, 42, 48, 54, 60, 72],
						// amount_on_delivery: 0,
						// down_payment: 0,
						down_payment: parseFloat(typeof (opts.financeDownPayment) == 'function' ? opts.financeDownPayment.apply(form) : opts.financeDownPayment) || (trimData_.finance && trimData_.finance.down_payment) || 0
						// apr: 0.9,
						// value: 464.38,
						// est_balance: 24075,
						// est_annual_km: 0,
						// term_options_default: 60,
						// est_monthly_payment: 464.38
					},
					include_fees: { value: typeof (opts.includeFees) == 'function' ? opts.includeFees.apply(form) : opts.includeFees },
					exceptions: null,
					cash_purchase: {
						value: 0
					},
					msrp: {
						include_fees: typeof (opts.includeFees) == 'function' ? opts.includeFees.apply(form) : opts.includeFees,
						value: 0
					}
				};

				return data;
			}

			// hook up to the form's submit event
			$(form).submit(function (event) {
				// collect parameters
				var url = typeof (opts.financeUrl) == 'function' ? opts.financeUrl.apply(this) : opts.financeUrl;
				url += '?include_fees=' + (typeof (opts.includeFees) == 'function' ? opts.includeFees.apply(this) : opts.include_fees);
				url += '&Lang=' + (typeof (opts.lang) == 'function' ? opts.lang.apply(this) : opts.lang);
				var params = collect.apply(form);
				$loader.show();
				// ajax call
				$.ajax({
					type: 'POST',
					url: url,
					dataType: 'json',
					global: false,
					data: {
						lang: typeof (opts.lang) == 'function' ? opts.lang.apply(this) : opts.lang,
						data: $.toJSON(params)
					},
					success: function (data, status, xhr) {
						opts.populate.apply(form, [data, opts]);
						$loader.hide();
						$.behaviors.cufon_refresh();
						var types = [];
						if (data.lease && data.lease.apr <= opts.threshold) {
							types.push('lease');
						}
						if (data.finance && data.finance.apr <= opts.threshold) {
							types.push('finance');
						}
						load_summary(params, types);
					},
					error: function () {
						fill_in_blanks();
						$.behaviors.cufon_refresh();
						$loader.hide();
					}
				});

				// don't allow default event
				return false;
			});

			$('#lease_down_payment, #finance_down_payment, #lease_additional_annual_km').keypress(function (e) {
				var code = e.keyCode ? e.keyCode : e.which;
				if (
				code == 9 // tab
				|| code >= 48 && code < 58 // numeric keys
				// || code == 46 // dot
				|| (code >= 37 && code <= 40) // arrow keys
				|| (code == 8) // backspace
				|| (code == 46) // delete
			) {
					return;
				}
				e.preventDefault();
				return false;
			});

			$('a.recalculate', this).click(function () {
				$(form).submit();
				return false;
			});
			$('#include_tax', this).click(function () {
				$('#include_tax_summary').prop('checked', $(this).is(':checked'));
				if ($(this).is(':checked'))
					$('#includeFeesDisclaimer').hide();
				else {
					$('#includeFeesDisclaimer').show();
				}
				$(form).submit();
			});

			$('#include_tax_summary', this).click(function () {
				$('#include_tax').prop('checked', $(this).is(':checked'));
				if ($(this).is(':checked'))
					$('#includeFeesDisclaimer').hide();
				else {
					$('#includeFeesDisclaimer').show();
				}
				$(form).submit();
			});

			loadModel.apply(form); // load model on load

			return false;
		}

		return this.each(function () {
			// the finance calculator only applies to forms
			init.apply(this);
		});
	};

	$.fn.hondafinance.defaults = {
		vehicleSelector: '', // these elements' onchange event is being listened, for calculating MSRP
		provinceSelector: '', // these elements' onchange event is being listened, for calculating MSRP
		leaseSummaryContainer: '#lease_summary',
		financeSummaryContainer: '#finance_summary',
		enableSummary: true,
		dateSettings: global_get('format.dateSettings'),
		financeUrl: function () { // url to the honda finance calculator endpoint, or a function returning the endpoint
			return '/buildyourhonda/financial/financial_panel.aspx';
		},
		msrpUrl: function () { // url to the honda finance calculator endpoint, or a function returning the endpoint
		},
		summaryUrl: function () {
		},
		model: function () {
			var vehicle = eval('(' + $('#vehicle', this).val() + ')');
			return vehicle.model_key;
		},
		modelYear: function () {
			var vehicle = eval('(' + $('#vehicle', this).val() + ')');
			return vehicle.model_year;
		},
		trimLevel: function () {
			var vehicle = eval('(' + $('#vehicle', this).val() + ')');
			return vehicle.trim_level_key;
		},
		transmission: function () {
			var vehicle = eval('(' + $('#vehicle', this).val() + ')');
			return vehicle.transmission_key;
		},
		province: function () {

			return $('#province', this).val();
		},
		includeFees: function () {
			var included = $('#include_tax', this).attr('checked');
			return included ? true : false;
		},
		leaseCashOption: '',
		financeCashOption: '',
		leaseDownPayment: function () {
			return $('#lease_down_payment', this).parse();
		},
		financeDownPayment: function () {
			return $('#finance_down_payment', this).parse();
		},
		leaseTerm: function () {
			return $('#lease_term', this).parse();
		},
		financeTerm: function () {
			return $('#finance_term', this).parse();
		},
		leaseAdditionalAnnualKM: function () {
			return $('#lease_additional_annual_km', this).parse();
		},
		threshold: 100,
		populate: function (data, opts) {
			// check exceptions
			$('.exception').hide();
			if ('exceptions' in data && data.exceptions.keys.indexOf('lease') != -1) {
				$.each(data.exceptions.lease, function (index, element) {
					$('#exception_' + element.field).text(element.message).show();
				});
			}

			// msrp
			$('.msrp_amount', this).text(data.msrp.value).format({ format: global_get('format.price').replace('.00', '') });
			$('.cash_purchase_amount', this).text(data.cash_purchase.value).format({ format: global_get('format.price').replace('.00', '') });

			// $('#freight_pda', this).text(data.msrp.value).format({format:global_get('format.price').replace('.00', '')});
			// $('#tax_levis').text(data.msrp.value).format({format:global_get('format.price').replace('.00', '')});
			// $('#total_price').text(data.msrp.value).format({format:global_get('format.price')});

			// threshold
			if (data.lease && data.lease.apr >= opts.threshold) {
				fill_in_blanks('lease');
				$('#lease_term').next('.error').remove().end().after(
				$('<span class="error">').text(global_get('finance_calculator.lease_contact_dealer'))
			);
			}
			else {
				$('.lease_amount', this).text(data.lease.est_monthly_payment).format({ format: global_get('format.price').replace('.00', '') });
				// lease term
				$('#lease_term').empty();
				$.each(data.lease.term_options, function (index, element) {
					$('#lease_term').append($('<option>').attr('value', element).text(element));
				});
				$('#lease_term').val(data.selected_keys.term_options_lease);

				$('#lease_term').next('.error').remove();
				// lease down payment
				$('#lease_down_payment').val(data.lease.down_payment).format();
				// lease est balance
				$('#lease_est_balance').text(data.lease.est_balance).format();
				// lease apr
				$('#lease_apr').text(data.lease.apr);
				// lease annual km
				$('#lease_est_annual_km').text(data.lease.est_annual_km).format();
				// additional km
				$('#lease_additional_annual_km').val(data.lease.additional_annual_km).format({ format: '####' });
				// additioanl cost per km
				$('#lease_additional_cost_per_km').text(data.lease.additional_cost_per_km).format();
				// lease buyout
				$('#lease_buyout').text(data.lease.buyout).format();
				// lease monthly payment
				$('#lease_est_monthly_payment').text(data.lease.est_monthly_payment).format();
				// lease amount on delivery
				$('#lease_amount_on_delivery').text(data.lease.amount_on_delivery).format();
				// rate period
				try {
					if (data.lease.rate_period_start && data.lease.rate_period_end) {
						var s = new Date(data.lease.rate_period_start);
						var e = new Date(data.lease.rate_period_end);
						s = $.datepicker.formatDate($.datepicker.RFC_2822, s, opts.dateSettings);
						e = $.datepicker.formatDate($.datepicker.RFC_2822, e, opts.dateSettings);
						$('#lease_rate_period').text(s + ' - ' + e);
					}
				}
				catch (e) {
					$('#lease_rate_period').text('');
				}
			}

			if (data.finance && data.finance.apr >= opts.threshold) {
				fill_in_blanks('finance');
				$('#finance_term').next('.error').remove().end().after(
				$('<span class="error">').text(global_get('finance_calculator.finance_contact_dealer'))
			);
			}
			else {
				$('.finance_amount', this).text(data.finance.est_monthly_payment).format({ format: global_get('format.price').replace('.00', '') });

				// finance term
				$('#finance_term').empty();
				$.each(data.finance.term_options, function (index, element) {
					$('#finance_term').append($('<option>').attr('value', element).text(element));
				});
				$('#finance_term').val(data.selected_keys.term_options_finance);

				$('#finance_term').next('.error').remove();
				// finance down payment
				$('#finance_down_payment').val(data.finance.down_payment).format();
				// finance est balance
				$('#finance_est_balance').text(data.finance.est_balance).format();
				// finance apr
				$('#finance_apr').text(data.finance.apr).format();
				// finance monthly payment
				$('#finance_est_monthly_payment').text(data.finance.est_monthly_payment).format();
				// finance amount on delivery
				$('#finance_amount_on_delivery').text(data.finance.amount_on_delivery).format();
				// rate period
				try {
					if (data.finance.rate_period_start && data.finance.rate_period_end) {
						var s = new Date(data.finance.rate_period_start);
						var e = new Date(data.finance.rate_period_end);
						s = $.datepicker.formatDate($.datepicker.RFC_2822, s, opts.dateSettings);
						e = $.datepicker.formatDate($.datepicker.RFC_2822, e, opts.dateSettings);
						$('#finance_rate_period').text(s + ' - ' + e);
					}
				}
				catch (e) {
					$('#finance_rate_period').text('');
				}
			}
		},
		//
		_last: null
	};

})(jQuery);
