﻿var DealerHelper = {};
var  _getProvinceCode = function(strprovince) {
            /// <param name="province" type="String">
            /// </param>
    /// <returns type="String"></returns>
    
    
    var provinceCode = '';
    if (strprovince && strprovince!= "") { 
    var province = strprovince.replace(/_/g, " ").toLowerCase();

            switch (province) {
                case 'alberta':
                    provinceCode = 'AB';
                    break;
                case 'british columbia':
                case 'colombie britannique':
                    provinceCode = 'BC';
                    break;
                case 'manitoba':
                    provinceCode = 'MB';
                    break;
                case 'new brunswick':
                case 'nouveau-brunswick':
                case 'nouveau brunswick':
                    provinceCode = 'NB';
                    break;
                case 'newfoundland and labrador':
                case 'terre-neuve et labrador':
                case 'terre neuve et labrador':
                    provinceCode = 'NL';
                    break;
                case 'nova scotia':
                case 'nouvelle-écosse':
                case 'nouvelle écosse':
                    provinceCode = 'NS';
                    break;
                case 'northwest territories':
                case 'territoires du nord-ouest':
                case 'territoires du nord ouest':
                    provinceCode = 'NT';
                    break;
                case 'nunavut':
                    provinceCode = 'NU';
                    break;
                case 'ontario':
                    provinceCode = 'ON';
                    break;
                case 'prince edward island':
                case 'île du prince-édouard':
                case 'île du prince édouard':
                    provinceCode = 'PE';
                    break;
                case 'quebec':
                case 'québec':
                    provinceCode = 'QC';
                    break;
                case 'saskatchewan':
                    provinceCode = 'SK';
                    break;
                case 'yukon':
                case 'territoire du yukon':
                    provinceCode = 'YT';
                    break;
                default:
                    provinceCode = province;
                    break;
            }
            }
            return provinceCode;
        };

DealerHelper.getProvinceList = function(lang) {
    var array = new Array();
    if (lang == 'fr') {
        array = ['Alberta', 'Colombie britannique', 'Manitoba', 'Nouveau-Brunswick', 'Terre-Neuve et Labrador', 'Nouvelle-Écosse', 'Ontario', 'Île du Prince-Édouard', 'Québec', 'Saskatchewan', 'Territoire du Yukon'];

    } else {
        array = ['Alberta', 'British Columbia', 'Manitoba', 'New Brunswick', 'Newfoundland and Labrador', 'Nova Scotia', 'Ontario', 'Prince Edward Island', 'Quebec', 'Saskatchewan', 'Yukon'];
    }
    return array;
};
   
