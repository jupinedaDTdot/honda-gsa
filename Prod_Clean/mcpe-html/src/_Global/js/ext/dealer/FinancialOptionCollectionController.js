﻿var Climax;
(function (climax) {
    (function (dealerLocator) {
      
       
        /// <summary>Constructor.</summary>
        /// <param name="options" type="Object">Options parameter.</param>
        var financeOptionCollectionController = function(options) {
            var self;
           
            self = this;
            // Create fields
            self._apiRoot = options.apiRoot;
            self._financeOptionListElement = options.financialOptionListElement;
            self._language = options.language || "en";
            self._dealerId = options.dealerId;
            // Create the view-models and bind them
            self._financeOptionsViewModel = {
                FinancialOptionsForProductLine: ko.observableArray()
            };

            $.extend(self._financeOptionsViewModel, {
                  HasData: ko.computed(function() {
                    return  self._financeOptionsViewModel.FinancialOptionsForProductLine().length > 0 ;
                })
            } , self._financeOptionsViewModel);


            bindFinanceOptionsByDealerId(self);
//            console.log('self._financeOptionsViewModel.FinancialOptionsForProductLine: constructor');
//            console.log(self._financeOptionsViewModel.FinancialOptionsForProductLine);
              ko.applyBindings(this._financeOptionsViewModel, this._financeOptionListElement);


              //other methods if required. 
             ///<summary>Disposer.</summary>
        financeOptionCollectionController.prototype.dispose = function () {
            // Clear fields
            this._financeOptionListElement = null;
            this._language = null;
            this._dealerId = null;
            this._financeOptionsViewModel = null;
        };

   
        ///<summary>Binds financialOption to the map and dealers list.</summary>
        ///<param name="dealers" type="Object" parameterArray="true">Dealers to be bound.</param>
        var bindFinanceOptions = function (financialOptions,options) {

            var viewModel = new Array();
            if (financialOptions) {
                //financialOptions.ProductLines
                $.each(financialOptions, function (index, item) {
                    var _financialOptionsCollection = item;
                    // Add the financialOption to the view-model
                    var viewModelFinancialOption = generateViewModelFinancialOption(_financialOptionsCollection);
                    if(viewModelFinancialOption!=null) viewModel.push(viewModelFinancialOption);
                });
            }

            //$('.sorry').text(financialOptions.Message);

            if (viewModel.length > 0) {
                // Update view-model
                self._financeOptionsViewModel.FinancialOptionsForProductLine(viewModel);
//                console.log(self._financeOptionsViewModel.FinancialOptionsForProductLine());
//                console.log(self._financeOptionsViewModel.FinancialOptionsForProductLine);
            } 
        };

         var generateViewModelFinancialOption = function (financialOption) {

             if (financialOption) {
                 var viewModelFinancialOption = {}; // $.extend({}, financialOption);}
                 if( !(String.fromCharCode(financialOption.ProductLine.Code)== 'A'  || (String.fromCharCode(financialOption.ProductLine.Code) == 'H')) ) {

                     viewModelFinancialOption.ProductLineName = financialOption.ProductLine.Name;
                     viewModelFinancialOption.ProductLineCode = String.fromCharCode(financialOption.ProductLine.Code); 
                     viewModelFinancialOption.FinancialOptions = financialOption.FinancialOptions;
                     if (financialOption.FinancialOptions.length <= 0 ) return null;

                 }else 
                 return null; 

             return viewModelFinancialOption; 
             }
            return null; 
        };


        // new function MCPE lease dealer 
        ///<summary>Binds the dealers for the provided postal code.</summary>
        ///<param name="postalCode" type="String" mayBeNull="false">Postal code. The postal code format should match 
        ///Canadian postal code form like (A1A1A1 or A1A 1A1)</param>
          function bindFinanceOptionsByDealerId(option) {
            // get url with parameters
            var url = option._apiRoot +  "/financial/{id}".replace("{id}", option._dealerId); //to do add from location.
            
             if ($.browser.msie && window.XDomainRequest) {
                  url = url.concat( (url.indexOf("?") >= 0) ? '&' : '?'  +    'AcceptLanguage=' + option._language);
              }
                          // Get dealer from API and update the view-model
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                crossdomain: true,
                headers: {
                    'Accept-Language': option._language
                },
                success: function (data) {
                    if (data)
                        bindFinanceOptions(data,option);
                },
                error: function (error) {
                    if (!(option._isMobile))
                         $("#divProductLine").hide();
                }
            });
        };


    }; //financeOptionCollectionController end 

    dealerLocator.FinancialOptionCollectionController = financeOptionCollectionController;
    
    })(climax.DealerLocator || (climax.DealerLocator = {}));
})(Climax || (Climax = {}));

