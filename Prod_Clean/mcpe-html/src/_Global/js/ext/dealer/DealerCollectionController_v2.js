﻿/// <reference path="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPqRQTPIAx6XSS5Xo5YywMsyozP7FkgCA&sensor=true"/>
/// <reference path="~/_Global/js/ext/knockout-3.1.0.js"/>
/// <reference path="~/_Global/js/ext/dealer/DealerHelper.js"/>

var Climax;
(function(climax) {
    (function(dealerLocator) {
        var self;
        var recordCount = 10;
        var recordCountCity = 5;
        var mapZoomLevel = 8;

        /// <summary>Constructor.</summary>
        /// <param name="options" type="Object">Options parameter.</param>
        var dealerCollectionController = function(options) {
            self = this;
            // Create fields
            self._clientLocation = options.clientLocation || {};
            self._dealersListElement = options.dealersListElement;
            self._dealersMapElement = options.dealersMapElement;
            self._cityListElement = options.cityListElement;
            self._language = options.language || "en";
            self._selectedDealer = null;
            self._dealerMarkers = new Array();
            self._infoWindows = new Array();
            self._isPopup = options.isPopup;
            self.searchCriteria = options.searchCriteria;
            self._isDealerDetailPage = options.isDealerDetailPage;
            self._pageSize = options.searchCriteria._pageSize;
            self._dealerLocatorUrl = self._language == "fr" ? "/trouver-concessionaire" : "/dealerlocator";
            self._dealerLocation = {};
            self._recordCountDealers = options._recordCountDealers || recordCount;
            if (self.searchCriteria._productLine == undefined) self.searchCriteria._productLine = "";
            if (self.searchCriteria._productLineCode == undefined) self.searchCriteria._productLineCode = "";

            // Create the view-models and bind them
            self._dealersViewModel = {
                AllDealers: ko.observableArray(),
                Dealers: ko.observableArray(),
                Unit: ko.observable(),
                MatchingType: ko.observable(),
                IsPopup: ko.observable(self._isPopup),
                HasSelection: ko.observable(true),
                CurrentPage: ko.observable(0),
                Message: ko.observable(),
                MessageDisplay: ko.observable(false)
            };

            self._dealersViewModel.provinceofFirstDealer = ko.observable('');
            self._dealersViewModel.searchResultHeaderText = ko.observable('');
            self._dealersViewModel.SelectedDealer = ko.observable();
            self._dealersViewModel.setSelectedDealer = function(dealer) {
                self._dealersViewModel.SelectedDealer ( dealer);
            };
            self._dealersViewModel.CurrentLocation = ko.observable('');
            if (options.buttonClickHandler) {
                self._dealersViewModel.buttonClick = function($data) {
                    //to set province value in selected province on page load
                    //self._dealersViewModel.province($data.Dealer.Location.Province.Name);
                    self._dealersViewModel.setSelectedDealer($data);
                    options.buttonClickHandler(self._dealersViewModel, $data);
                };
            }


            self._dealersViewModel.postalCode = ko.observable("").extend({
                required: {
                    message: (self._language == "fr" ? "Ce champ est obligatoire." : "Field is required."),
                },
                pattern: {
                    message: (self._language == "fr" ? "Veuillez entrer un code postal valide." : "Please enter a valid postal code."),
                    params: /^[a-zA-Z][0-9][a-zA-Z]( ?[0-9][a-zA-Z][0-9])$/

                }
            });

            self._dealersViewModel.city = ko.observable("").extend({
                required: true
            });
            self._dealersViewModel.provinceList = ko.observableArray();

            self._dealersViewModel.provinceList(DealerHelper.getProvinceList(self._language));

            self._dealersViewModel.province = ko.observable();
            self._dealersViewModel.selectedDealerDD = ko.observable();



            $.extend(self._dealersViewModel, {
                // computed
                HasNextPage: ko.computed(function() {
                    return (this.CurrentPage() + 1) * self._pageSize < this.AllDealers().length;
                }, self._dealersViewModel),
                HasPrevPage: ko.computed(function() {
                    return this.CurrentPage() > 0;
                }, self._dealersViewModel),
                TotalPages: ko.computed(function() {
                    if (this.AllDealers().length > 0)
                        return Math.ceil(this.AllDealers().length / self._pageSize);
                    else
                        return 1;
                },self._dealersViewModel),
                HasData: ko.computed(function() {
                    return  this.AllDealers().length > 0 ;
                }, self._dealersViewModel),

                // event handlers
                nextPage: function() {
                if ((this.CurrentPage() + 1) * self._pageSize < this.AllDealers().length) {
                        var model = self._dealersViewModel;
                        setDealerPage(model, model.CurrentPage() + 1, self._pageSize);
                    }
                },
                prevPage: function() {
                    var model = self._dealersViewModel;
                    setDealerPage(model, Math.max(model.CurrentPage() - 1, 0), self._pageSize);
                },
                //checkbox event handlers
                IsPowerHouse: ko.observable(self.searchCriteria._isPowoerHouse),
                PowerHouseChanged: function() {
                    self.searchCriteria._isPowoerHouse = this.IsPowerHouse();
                    self.bindDealersBySearchCriteria(self.searchCriteria);
                    return true;
                },
                IsLeasing: ko.observable(self.searchCriteria._isLeasing),
                LeasingChanged: function() {
                    self.searchCriteria._isLeasing= this.IsLeasing();
                    self.bindDealersBySearchCriteria(self.searchCriteria);
                    return true;
                },
                isPostalCodeVisible: ko.observable(false),
                optionYes:  function () {
                    this.isPostalCodeVisible(true);
                },
                optionNo: function() {
                    location.href = self._dealerLocatorUrl;
                },
                getDealerList: function () {
                    //to initialise reset dealers , cities as this functionality is not initialised.
                    this.Dealers(new Array());
                    if(self._isBuildAndPrice)
                        self._cityViewModel.Cities(new Array());
                    self._dealersViewModel.errors = ko.validation.group([self._dealersViewModel.postalCode]);
                    //console.log('before:postalcode:',self._recordCountDealers);
                    if (this.errors().length == 0) {
                        options.searchCriteria._mode = "ZipCode";
                        options.searchCriteria._postalcode = this.postalCode();
                        self.bindDealersBySearchCriteria(self.searchCriteria);
                    }
                },
                getDealerListByCity: function(data1, dealerViewModel) {
                    //to initialise reset dealers , cities as this functionality is not initialised.
                    this.Dealers(new Array());
                    self._dealersViewModel.errors = ko.validation.group([self._dealersViewModel.city]);
                    if (this.errors().length == 0) {
                        if (dealerViewModel.city() != "") {

                            options.searchCriteria._mode = "City";
                            var searchItem = this.city().split(',');
                            var provinceName = "", city = this.city(), provinceCode = "";
                            if (searchItem.length > 1) {
                                provinceName = searchItem[1];
                                city = searchItem[0];
                            }
                            options.searchCriteria._city = city;
                            //console.log('before:city:',self._recordCountDealers);

                            if (provinceName != '')
                                provinceCode = _getProvinceCode(provinceName);
                            options.searchCriteria._province = provinceCode;
                            //use for province search options.searchCriteria._provinceName = province;
                            self.bindDealersBySearchCriteria(self.searchCriteria);
                        }
                    }
                }
            });

            self._dealersViewModel.province.subscribe(function(newValue) {
                    var oriNumber = self._recordCountDealers;
                    //console.log("the new value is " + newValue);
                    self._dealersViewModel.Dealers(new Array());
                    self._dealersViewModel.AllDealers(new Array());
                    options.searchCriteria._mode = "Province";
                    var provinceCode = _getProvinceCode(newValue);
                    options.searchCriteria._province = provinceCode;
                    options.searchCriteria._IncludeOnlyInStock = null;
                   //console.log('before:province:',self._recordCountDealers);
                    if (self.searchCriteria._isInventory || false) {
                        self._recordCountDealers = 1000;
                    }
                    //console.log('after:province:',self._recordCountDealers);

                    var callback = set_SelectedDealer;
                    self.bindDealersBySearchCriteria(self.searchCriteria,callback,oriNumber);

                    //todo: get List Of dealers and bind it to Prefered dealer list.
                    //set the first dealer as selected dealer and set Availabilty and contact Information

//                    if (options.buttonClickHandler) {
//                         self._dealersViewModel.setSelectedDealer(self._dealersViewModel.AllDealers()[0]);
//                    }

                }
            );

            self._cityViewModel = {
                Cities: ko.observableArray()
            };

             $.extend(self._cityViewModel, {
              HasData: ko.computed(function() {
                    return  this.Cities().length > 0 ;
                }, self._cityViewModel),
              getDealerByCity: function (city) {
                  options.searchCriteria._mode = "City";
                  options.searchCriteria._city = city.Name;
                  self.bindDealersBySearchCriteria(self.searchCriteria);
                  if(self._dealersViewModel.AllDealers().length > 0) {
                      $('.FAD-dealers').slideUp(100, function () {
                            $('.FAD-search-results, .FAD-see-all-container').fadeIn(900);
                      });
                  }
              }
              });


            self._pinPath = options._pinPath;
            //set for diffent requirement i.e used on finddealer page, dealer detail page, of used on Mobile page
            self._isMobile = options.searchCriteria._isMobile;
            self._hasMap = (options.dealersMapElement != null);
            self._hasCity = (options.cityListElement != null);

            // Initialize the dealer controller
            if(!self.searchCriteria._isBuildAndPrice) {
                this._initialize();
            }

            //self._dealersViewModel.errors = ko.validation.group(self._dealersViewModel);
            if(options.searchCriteria._mode== 'ZipCode')
                self._dealersViewModel.errors = ko.validation.group([self._dealersViewModel.postalCode]);
            else if (options.searchCriteria._mode== 'City')
                self._dealersViewModel.errors = ko.validation.group([self._dealersViewModel.city]);

            //ko.cleanNode(this._dealersListElement);
            ko.applyBindings(this._dealersViewModel, this._dealersListElement);
            if (self._hasCity) {
                //ko.cleanNode(this._cityListElement);
                ko.applyBindings(this._cityViewModel, this._cityListElement);
            }

        };

        ///<summary>Initializes the dealer-controller object. It populates the map and dealer result set based on the client's location.</summary>
        dealerCollectionController.prototype._initialize = function() {
            //Populate the map and dealer's result set based on the client's location
            this.bindDealersBySearchCriteria(this.searchCriteria);
        };

        ///<summary>Disposer.</summary>
        dealerCollectionController.prototype.dispose = function() {

            // Clear fields
            this._clientLocation = null;
            this._dealersListElement = null;
            this._dealersMapElement = null;
            this._dealerItemSelector = null;
            this._language = null;
            this._selectedDealer = null;
            this._map = null;
            this._dealerMarkers = null;
            this._infoWindows = null;
            this._dealerLocatorUrl = null;
            // Clear view-models
            this._dealersViewModel = null;
            this._self._dealerLocation = null;

        };

        dealerCollectionController.prototype.updateMap = function() {
            setMapBoundaries(self._dealerMarkers);
            google.maps.event.trigger(self._map, 'resize');
            self._map.panTo(self._dealerMarkers[0].getPosition());
        };
        ///<summary>Gets the selected dealer.</summary>
        dealerCollectionController.prototype.getSelectedDealer = function() {
            return this._selectedDealer;
        };

        function setDealerPage(viewModel, pageIndex, pageSize) {
            viewModel.Dealers(viewModel.AllDealers().slice(pageIndex * pageSize, (pageIndex + 1) * pageSize));
            viewModel.CurrentPage(pageIndex);
        }

        ///<summary>Binds dealer to the map and dealers list.</summary>
        ///<param name="dealers" type="Object" parameterArray="true">Dealers to be bound.</param>
        var bindDealers = function(dealers, pageSize, searchCriteria) {
            var recordCounter = self._recordCountDealers;
            if (searchCriteria._isInventory == true)
                if (searchCriteria._mode == 'ip')
                    recordCounter = 5;

            // Clear the effects of previous result
            clearPreviousResultEffects();

            var viewModel = new Array();
            if (dealers) {
                $.each(dealers.Items, function(index, item) {
                    if (viewModel.length >= recordCounter)
                        return false;

                    var dealer = item;
                    // Add the dealer to the view-model
                    var viewModelDealer = generateViewModelDealer(dealer);
                    viewModel.push(viewModelDealer);

                });

                if (dealers.Message != undefined)
                {
                        if (dealers.Message.toLowerCase().includes('no matching location found for this ip') )
                        {
                        self._dealersViewModel.MessageDisplay(false);
                        }
                        else
                        {
                        self._dealersViewModel.MessageDisplay(true);
                        self._dealersViewModel.Message(dealers.Message);
                        }
                }
        }

        //if (dealers.Items.length > 0) {
            if (true) {
                // Update view-model
                self._dealersViewModel.AllDealers(viewModel);
                setDealerPage(self._dealersViewModel, 0, pageSize);
                self._dealersViewModel.Unit(dealers.UnitName);
                self._dealersViewModel.MatchingType(dealers.MatchingType);

                if (self._hasMap && dealers.Items.length > 0) {
                    if (dealers) {
                        var configdata = getDealerLocationDetails(dealers);
                        //ShowMapUsingDealerAPI(configdata, dealers, 'dealersMapSection', '/_Global/img/Honda_pin/honda_pin.png');
                    }
                }
                if (self._hasCity) {
                    if (dealers.Items.length > 0) {
                        if (dealers.Matching.GeoOrigin) {
                            self._dealerLocation = dealers.Matching.GeoOrigin;
                        }else {
                            self._dealerLocation = dealers.Items[0].Dealer.Location;
                        }
                        bindCitiesByLocation(self._dealerLocation);
                    }else {
                        self._cityViewModel.Cities(new Array());//to set empty set for viewModel
                    }
                }
                if (self.searchCriteria._isInventory) {
                if (dealers.Matching)
                    if (dealers.Matching.GeoOrigin)
                        self._dealersViewModel.LocationBasedOnIP = dealers.Matching.GeoOrigin;
                }

                if (dealers.Items.length > 0) {
                     if (dealers.Items[0].Dealer.Location)  self._dealersViewModel.provinceofFirstDealer(dealers.Items[0].Dealer.Location.Province.Name);
                     if (dealers.Matching.GeoOrigin) {
                         var cty = dealers.Matching.GeoOrigin.City.Name, prv = dealers.Matching.GeoOrigin.Province.Abbreviation;
                         var loc = cty + ',' + prv;
                         self._dealersViewModel.CurrentLocation(loc);
                     }
                     self._dealersViewModel.searchResultHeaderText(getsearchResultHeaderText(self._dealersViewModel.provinceofFirstDealer(),searchCriteria,dealers));
                 } else {
                     self._dealersViewModel.provinceofFirstDealer('');
                 }

             } else {
                //todo : no dealer found scenario.
                //this.dispose();

                //self.
            }

        };

        function getsearchResultHeaderText(provinceofFirstDealer, searchCriteria,dealers) {
            var result = '';
            var searchMode = searchCriteria._mode;
            if (! (searchCriteria._mode == undefined || searchCriteria._mode == null)) {
                switch (searchCriteria._productLineCode) {
                 case 'p':
                    result = self._language == 'fr' ? 'Concessionnaires Produits Mécaniques Honda' : 'Honda Power Equipment Dealers';
                    break;
                default :
                    result = self._language == 'fr' ? 'Concessionnaires Honda' : 'Honda Dealers';
                }

                if(dealers.Matching.HandledErrorSequence.length > 0 ){  //if (!(dealers.Matching.HandledErrorSequence.Code == undefined)) {
                    switch (dealers.Matching.MatchingType.Name) {
                    case 'Province':
                        searchMode = 'Province';
                        break;
                    case 'ClosetToCityCoordinate':
                        searchMode = 'ClosetToCityCoordinate';
                        break;
                    case 'IP':
                        searchMode = 'ip';
                        break;
                    default:
                        searchMode = 'Province';
                    }
                    }

                switch (searchMode) {
                case 'ZipCode':
                    //todo : if required  use GeoOrigin object to decide correct message.
                    result +=  (self._language == 'fr' ? ' près de ' : ' near ' ) + searchCriteria._postalcode;//near postalcode
                    break;
                case 'ClosetToCityCoordinate' :
                case 'City':
                    result +=  (self._language == 'fr' ? ' de ' : ' in ')  + provinceofFirstDealer;//result +=  (self._language == 'fr' ? ' près de ' : ' near ' + searchCriteria._city);
                    break;
                case 'Province':
                    result +=  (self._language == 'fr' ? ' de ' : ' in ' ) + provinceofFirstDealer;
                    break;
                case 'ip':
                    result +=  (self._language == 'fr' ? ' proches de vous.' : ' closest to you.');
                    break;
                default:
                    result = result;
                    //console.log('search mode is:' + searchCriteria._mode);
                }

            }
            return result;
        }

///
        ///
        ///
        var getDealerLocationDetails = function(dealers) {
            var viewModel = new Array();
            var configdata = { "nelat": 44.08926, "nelng": -78.692459, "swlat": 43.242616, "swlng": -79.892007, "miniView": true };

            var latList = new Array();
            var longList = new Array();

            if (dealers) {
                $.each(dealers.Items, function(index, item) {
                    var dealer = item;

                    // find min and max values for Map.
                    latList.push(dealer.Dealer.Location.Coordinate.Latitude);
                    longList.push(dealer.Dealer.Location.Coordinate.Longitude);
                });

                //{"nelat":44.08926,"nelng":-78.692459,"swlat":43.242616,"swlng":-79.892007,"miniView":true}
                configdata.nelat = Math.max.apply(Math, latList);
                configdata.nelng = Math.max.apply(Math, longList);
                configdata.swlat = Math.min.apply(Math, latList);
                configdata.swlng = Math.min.apply(Math, longList);

                //console.log(configdata);
            }
            return configdata;
        };


        ///<summary>Generates a view-model dealer from the model dealer.</summary>
        ///<param name="dealer" type="Object" mayBeNull="false">The dealer to be converted to the view-model dealer.</param>
        var generateViewModelDealer = function(dealer) {
            if (!dealer)
                throw { name: 'NullError', message: 'Dealer cannot be null.' };

            var viewModelDealer = $.extend({}, dealer);

            viewModelDealer.PhoneNumberFormatted = ko.computed(function() {
                if (viewModelDealer.Dealer.ContactInformation.Phones == null || viewModelDealer.Dealer.ContactInformation.Phones.length == 0) {
                    return '';
                }
                return formatePhoneNumber(viewModelDealer.Dealer.ContactInformation.Phones[0].Number);
            });

            viewModelDealer.DistanceFormatted = ko.computed(function() {
                    return localizeForNumber(self._language,parseFloat(viewModelDealer.DistanceToOrigin).toFixed(1));
            });


            viewModelDealer.IsPowerHouse = ko.computed(function() {
                if (viewModelDealer.Dealer.Caliber.Code == "1")
                    return true;
                else
                    return false;
            });

            viewModelDealer.PowerHouseText = ko.computed(function() {
                return (viewModelDealer.Dealer.Caliber.Code == "1" ? (self._language == "fr" ? "Centre Honda" : "PowerHouse Dealer") : "");
            });

            if (!(self._isMobile)) {
                viewModelDealer.DealerDetailUrl = ko.computed(function() {
                    var urlTemplte = "{dealerlocator}/{provincename}/{city}/{exportkey}";
                    var url;
                    url = urlTemplte.replace("{dealerlocator}", self._dealerLocatorUrl)
                        .replace("{provincename}", repDealerSearchProvince(viewModelDealer.Dealer.Location.Province.Name))
                        .replace("{city}", repDealerSearch(viewModelDealer.Dealer.Location.City.Name))
                        .replace("{exportkey}", viewModelDealer.Dealer.ExportKey);

                    return url.toLowerCase();
                });
            } else {
                //Mobile specific databinding
                viewModelDealer.DealerMapUrl = ko.computed(function() {
                    //http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=554+College+St.+Toronto,+&ie=UTF8&hq=&hnear=554+College+St.+Toronto,,+,+Canada&t=h&z=17&iwloc=A
                    var urlTemplte = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q={street}+{city},+{provincename}&ie=UTF8&hq=&hnear={street}+{city},,+{provincename},+{country}&t=h&z=17&iwloc=A";
                    var url;
                    url = urlTemplte.replace(/{street}/g, viewModelDealer.Dealer.Location.Address.replace(/ /g, "+"))
                        .replace(/{city}/g, viewModelDealer.Dealer.Location.City.Name)
                        .replace(/{provincename}/g, viewModelDealer.Dealer.Location.Province.Name)
                        .replace(/{country}/g, 'Canada');

                    return url;
                });

                viewModelDealer.PhoneNumberLink = ko.computed(function() {
                    if (viewModelDealer.Dealer.ContactInformation.Phones == null || viewModelDealer.Dealer.ContactInformation.Phones.length == 0) {
                        return '';
                    }
                    return "tel:" + viewModelDealer.Dealer.ContactInformation.Phones[0].Number;
                });

            }

            //contact a Dealeraction todo:remove this method

            viewModelDealer.contactDealer = function(data1, dealerViewModel) {
                //to initialise reset dealers , cities as this functionality is not initialised.
                //this.Dealers(new Array());
                //self._dealersViewModel.errors = ko.validation.group([self._dealersViewModel.city]);
                //if (this.errors().length == 0) {

                //}
            };

            viewModelDealer.DealerName = viewModelDealer.Dealer.Name;
            viewModelDealer.DealerCode =viewModelDealer.Dealer.Code;

            viewModelDealer.isSelected = ko.observable();
            viewModelDealer.selectedChanged = dealerSelectedChangeHandlerNew;
            return viewModelDealer;
        };

        var dealerSelectedChangeHandlerNew = function(currentDealer) {
            if (currentDealer) {
                //showBubbleInLib(".dealer_popup_wrapper", feature, map, marker);
            }
            //event.preventDefault();
        };


        ///<summary>Clears the effects of previous result set from map and the dealers' list.</summary>
        var clearPreviousResultEffects = function() {
            // Scroll the dealer's list section to top
            if (self._dealersListElement != null)
                self._dealersListElement.scrollTop = 0;
            if (self._hasCity) {
                if (self._dealersListElement != null)
                    self._cityListElement.scrollTop = 0;
            }
        };

        // new function MCPE lease dealer
        ///<summary>Binds the dealers for the provided postal code.</summary>
        ///<param name="postalCode" type="String" mayBeNull="false">Postal code. The postal code format should match
        ///Canadian postal code form like (A1A1A1 or A1A 1A1)</param>
        dealerCollectionController.prototype.bindDealersBySearchCriteria = function(searchCriteria,callback,recordnumber) {
            //console.log('bindDealersBySearchCriteria:', self._recordCountDealers);
            // get url with parameters
            var url = getUrlwithParametersforAPI(searchCriteria);
              if ($.browser.msie && window.XDomainRequest) {
                  url = url.concat( ((url.indexOf("?") >= 0) ? '&' : '?' ) +    'AcceptLanguage=' + self._language);
              }
            // Get dealer from API and update the view-model
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                crossdomain: true,
                 headers: {
                    'Accept-Language': self._language
                },
                success: function(data) {
                    if (data) {
                        bindDealers(data, self._pageSize,searchCriteria);

                        if (!(callback==undefined || callback==null))
                            callback(recordnumber);
                    }
                },
                error: function(error) {
                    if (!(self._isMobile)) {
                        if(!(error.responseText || "") == "")
                            $('.sorry').text(error.responseText);
                        else {
                            if (self._language == 'fr')
                                $('.sorry').text("Nous sommes désolés, il n’y aucun concessionnaire {productline} près de votre domicile.".replace("{productline}", searchCriteria._productLine ));
                            else
                                $('.sorry').text("Sorry, there are no {productline} dealers near you".replace("{productline}", searchCriteria._productLine));
                        }
                    }
                }
            });
        };

        //var url = "/api/dealerapi/Honda/dealer/closest";
        //var urlTemplate = "/api/dealerapi/honda/dealer/province/{provinceId}";


        // new function MCPE lease dealer
        ///<summary>Binds the dealers for the provided postal code.</summary>
        ///<param name="postalCode" type="String" mayBeNull="false">Postal code. The postal code format should match
        ///Canadian postal code form like (A1A1A1 or A1A 1A1)</param>
        //dealerCollectionController.prototype.bindCitiesByLocation= function(location) {
        var bindCitiesByLocation = function(location) {
            // get url with parameters
            //url = __apiRoot;
            var urlTemplete = __apiRoot + "/nearbycities/{provincecode}/{latitude}/{longitude}?ip={valueip}"; //to do add from location.
            var url = urlTemplete.replace("{provincecode}", location.Province.Abbreviation).replace("{latitude}", location.Coordinate.Latitude).replace("{longitude}", location.Coordinate.Longitude).replace("{valueip}", self.searchCriteria._userIP);
            if(! (self.searchCriteria._mode == 'Province' ||self.searchCriteria._mode == 'DealerID'  ) ){
                    if (self.searchCriteria._isLeasing == true)
                        url += "&financialOption=leasing";

                    if (self.searchCriteria._isPowoerHouse == true)
                        url += "&powerhouse=true";
             }

             if ($.browser.msie && window.XDomainRequest) {
                  url = url.concat( ((url.indexOf("?") >= 0) ? '&' : '?') +    'AcceptLanguage=' + self._language);
              }

            //location.Coordinate.Latitude, location.Coordinate.Longitude
            // Get dealer from API and update the view-model
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                crossdomain: true,
                headers: {
                    'Accept-Language': self._language
                },
                success: function(data) {
                    if (data)
                        bindCities(data);
                },
                error: function(error) {
                    $('.cities_nearby').hide();
                }
            });
        };

        var bindCities = function(cities) {
            //clearPreviousResultEffects();

            var viewModel = new Array();
            if (cities) {
                if (self._dealerLocation.Province) {
                    $.each(cities, function(index, item) {
                        if (index > (recordCountCity - 1)) {
                            return false;
                        }

                        var city = item;

                        // Add the dealer to the view-model
                        var viewModelCity = generateViewModelCity(city, self._dealerLocation);
                        viewModel.push(viewModelCity);

                    });
                }
            }
            self._cityViewModel.Cities(viewModel);
        };

        var generateViewModelCity = function(city, location) {
            if (!city)
                throw { name: 'NullError', message: 'City cannot be null.' };

            var viewModelCity = $.extend({}, city);

            viewModelCity.Url = ko.computed(function() {

                //var url = template.replace("{city}", repDealerSearch(viewModelCity.Name)).replace("{province}", repDealerSearch(location.Name));

                var urlTemplte = "{dealerlocator}/{provincename}/{city}";
                var url = "";
                url = urlTemplte.replace("{dealerlocator}", self._dealerLocatorUrl)
                    .replace("{provincename}", repDealerSearchProvince(location.Province.Name))
                    .replace("{city}", encodeURIComponent(viewModelCity.Name));
                return url.toLowerCase();
            });

            return viewModelCity;
        };


                ///<summary> get url with parameters.</summary>
        ///<param name="searchCriteria" type="searchCriteria" mayBeNull="false">
        /// "/HttpHandlers/GetDealers.ashx?postalcode={postalcodevale}&financialOption={financialOptionvalue}"
        var getUrlwithParametersforAPI = function(searchCriteria) {

            var url = __apiRoot; //"http://integration.preview.api.honda.ca/dealer/m"

            if (! (searchCriteria._mode == undefined || searchCriteria._mode == null)) {

                url = url.replace("{DealerSearchModeValue}", searchCriteria._mode);
                if (searchCriteria._userIP == "127.0.0.1") searchCriteria._userIP = "66.241.130.174";// IP address out side canada"27.63.255.255";
                switch (searchCriteria._mode) {
                case 'ZipCode':
                    url += ("/postalcode/{postalcodevalue}?ip={valueip}").replace("{postalcodevalue}", searchCriteria._postalcode).replace("{valueip}", searchCriteria._userIP + "");
                    break;
                case 'City':
                    if (!(searchCriteria._province == null || searchCriteria._province == "" || searchCriteria._province == undefined))
                        url += ("/city/{cityvalue}/{provincevalue}?ip={valueip}").replace("{cityvalue}", searchCriteria._city).replace("{provincevalue}", searchCriteria._province).replace("{valueip}", searchCriteria._userIP);
                    else {
                        if (searchCriteria._city == null || searchCriteria._city == undefined || searchCriteria._city == "")
                            searchCriteria._city = searchCriteria._searchCity;
                         url += ("/city/{cityvalue}?ip={valueip}").replace("{cityvalue}", searchCriteria._city).replace("{valueip}", searchCriteria._userIP);
                        }
                    break;
                case 'Province':
                    url += ("/province/{provincevalue}").replace("{provincevalue}", searchCriteria._province == "" ? searchCriteria._provinceName : searchCriteria._province);
                    break;
                case 'DealerID':
                    url += ("/id/{idvalue}").replace("{idvalue}", searchCriteria._dealerID || "");
                    break;
                case 'ip':
                    url += ("/ip/{ipvalue}").replace("{ipvalue}", searchCriteria._userIP || "");
                    break;
                case 'Financial':
                    url += ("/financial/{idvalue}").replace("{idvalue}", searchCriteria._dealerID || "");
                    break;
                default:
                    url = url ;
                    //console.log('search mode is:' + searchCriteria._mode);
                }
                //searchCriteria._mode == 'Province'
                if(! (searchCriteria._mode == 'DealerID'  ) ){

                    if (searchCriteria._isLeasing == true)
                        url += "&financialOption=leasing";

                    if (searchCriteria._isPowoerHouse == true)
                        url += "&powerhouse=true";

                    if (searchCriteria._useRequestIp == true)
                        url += ("&useRequestIp=true");


                     if (!(searchCriteria._productCode == undefined || searchCriteria._productCode == null || searchCriteria._productCode == "")) {
                          if (url.indexOf("?") >= 0)
                            url += ("&productcode={productcode}").replace('{productcode}',searchCriteria._productCode);
                          else
                            url += ("?productcode={productcode}").replace('{productcode}',searchCriteria._productCode);

                          if (!(searchCriteria._IncludeOnlyInStock == undefined || searchCriteria._IncludeOnlyInStock == null )) {
                             url += ("&IncludeOnlyInStock={IncludeOnlyInStockvalue}").replace('{IncludeOnlyInStockvalue}',searchCriteria._IncludeOnlyInStock);
                          }
                     }




                }

            } else {
                //console.log('there is no search mode:notfound value:' + searchCriteria._mode);
                url = "";
            }
            //console.log('Url:',url);
            return url;
        };


        function set_SelectedDealer(recordnumber) {
            if (self._dealersViewModel.Dealers()[0]) {
                self._dealersViewModel.setSelectedDealer(self._dealersViewModel.Dealers()[0]);
                self._dealersViewModel.selectedDealerDD(self._dealersViewModel.SelectedDealer().DealerCode);
            } else {
                //todo: set error Message or take action
            }
            if (recordnumber) self._recordCountDealers = recordnumber;
            //console.log('set_SelectedDealer:province:',self._recordCountDealers);

        }

        dealerLocator.DealerCollectionController = dealerCollectionController;
    })(climax.DealerLocator || (climax.DealerLocator = {}));
})(Climax || (Climax = {}));


/// <param name="str" type="String">
/// </param>
/// <returns type="String"></returns>
function repDealerSearch(str) {
    str = str.replace(/ /g, '_').replace('&', 'AND').replace('(', '').replace(')', '').replace(',', '');//replace('.', '').replace("'", '').
    return str;
};

function repDealerSearchProvince(str) {
    str = str.replace(/ /g, '_').replace('&', 'AND').replace('.', '').replace('(', '').replace(')', '').replace("'", '').replace(',', '').replace(/-/g, '_');
    return str;
}

function formatePhoneNumber(phno) {

    var areaCode = phno.substring(0, 3);
    var firstPart = phno.substring(3, 6);
    var secondPart = phno.substring(6, 10);

    var numberTemplate = '{0}-{1}-{2}';
    var number = numberTemplate
                    .replace("{0}", areaCode)
                    .replace("{1}", firstPart)
                    .replace("{2}", secondPart);

    return number;
}

function localizeForNumber(lang, data) {
    if (lang == "fr")
        return data.replace(".", ",");
    else
        return data;
}
