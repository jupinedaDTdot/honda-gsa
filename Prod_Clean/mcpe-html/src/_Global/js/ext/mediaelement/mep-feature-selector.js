(function ($) {

  $.extend(mejs.MepDefaults, {
    /*videos : [
    {src: 'media/echo-hereweare.mp4', caption: 'Video A', thumb: 'img/thumb1.jpg'},
    {src: 'media/gizmo.mp4', caption: 'Video B', thumb: 'img/thumb2.jpg'}
    ]*/
    selectorBackgroudHeight: 130,
    selectorRows: 1,
    selectorShowWhenHover: true
  });

  var hasTouch = ('ontouchstart' in document);

  // Play list Selector
  $.extend(MediaElementPlayer.prototype, {
    buildselector: function (player, controls, layers, media) {
      var t = this;
      if (t.options.videos.length) {
        t.createButton(controls, layers);
        t.createLayers(player, layers, controls);
      }
    },

    createButton: function (controls, layers) {
      var t = this;
      $('<div class="mejs-button mejs-selector-button mejs-selector"><button type="button"> </button></div>')
				.appendTo(controls)
				.click(function (e) {
				  t.toggleSelectorLayer(layers);
				  e.preventDefault();
				});
    },

    createLayers: function (player, layers, controls) {
      var t = this;

      //wrapped in extra div on purpose, so it won't be resized to 100% height on fullscreen by t.layers.children('.mejs-layer').width('100%').height('100%');
      var selectorBackgroundLayerWrap = $('<div><div class="mejs-overlay-selector-background" style="display:none;"></div></div>')
				.appendTo(layers);
      var selectorBackgroundLayer = selectorBackgroundLayerWrap.find('.mejs-overlay-selector-background');

      var selectorLayer =
				$('<div class="mejs-overlay mejs-layer mejs-overlay-selector" style="display:none;">' +
				  '<h2 class="mejs-overlay-selector-title"></h2>' +
					'<a class="browse prev"></a><a class="browse next"></a>' +
					'<div class="slider-wrap"><ul style=""></ul></div>' +
				'</div>');

      var end = Math.ceil(t.options.videos.length / t.options.selectorRows);

      for (var index = 0; index < end; index++) {
        var li = $('<li>'), video;

        for (var jndex = 0; jndex < t.options.selectorRows; jndex++) {
          var idx = index * t.options.selectorRows + jndex;
          video = t.options.videos[idx];

          var classes = '';
          if (idx + 1 == t.options.start_video_idx) {
            classes += ' active';
          }

          if (video) {
            $('<div>').addClass('mejs-overlay-selector-cell').append(
              $('<a>')
                .attr('data-video-filename', video.src)
                .attr('data-video-idx', idx + 1)
                .addClass('item ' + classes)
                .append(
                  $('<img>')
                    .attr('src', video.thumb)
                )
                .append(
                  $('<span>')
                    .addClass('title')
                    .html(video.caption)
                )
            ).appendTo(li);
          }
        }

        selectorLayer.find('ul').append(li);
      }

      $.each(t.options.videos, function (idx, video) {
        if (video.src != undefined) {
        }
      });
      selectorLayer.appendTo(layers);
      selectorBackgroundLayer.height(0).show();

      if (!hasTouch) {
        selectorLayer.bind('mouseenter mouseover', function () {
          t.options.alwaysShowControls = true;
          t.killControlsTimer();
        });

        selectorLayer.bind('mouseleave', function () {
          t.options.alwaysShowControls = false;
          t.startControlsTimer();
        });

        controls.bind('showсontrols', function () {
          if (t.options.selectorShowWhenHover || t.media.paused) {
            t.showSelectorLayers(layers);
          }
        });

        controls.bind('hideсontrols', function () {
          t.hideSelectorLayers(layers);
        });
      }

      selectorLayer.find('.slider-wrap a').bind(hasTouch ? 'touchend' : 'click', function (e) {
        var $a = $(this);
        t.currentVideoIdx = $a.data('video-idx');

        selectorLayer.find('.slider-wrap a').removeClass('active');
        $a.addClass('active');

        t.playMedia($a.data('video-filename'));

        t.toggleSelectorLayer(layers);
        e.preventDefault();
      });

      selectorLayer.find('.prev').click(function (e) {
        t.slideBack(layers);
        e.preventDefault();
      });
      selectorLayer.find('.next').click(function (e) {
        t.slideForward(layers);
        e.preventDefault();
      });

      if (t.options.selectorTitle) {
        selectorLayer.find('.mejs-overlay-selector-title').html(t.options.selectorTitle);
      }

      if (!t.options.autostart && !mejs.MediaFeatures.isiOS) {
        t.showSelectorLayers(layers);
      }

      t.media.addEventListener('play', function() {
        console.log('video started');
        t.hideSelectorLayers(layers);
      });
      t.media.addEventListener('ended', function (event) {
        console.log('video ended');
        t.showSelectorLayers(layers);
      });

      t.currentSlide = 1;
      t.totalSlides = Math.ceil(t.options.videos.length / t.options.selectorRows);

      t.hideShowNavigation(layers);
    },

    slideBack: function (layers) {
      var t = this;
      if (t.canSlideBack()) {
        t.currentSlide -= t.slidesPerScreen;
        layers.find('.mejs-overlay-selector ul').animate({ translateX: '+=' + t.slideWidth * t.slidesPerScreen + 'px' });
        t.hideShowNavigation(layers);
      }
    },

    slideForward: function (layers) {
      var t = this;
      if (t.canSlideForward()) {
        t.currentSlide += t.slidesPerScreen;
        layers.find('.mejs-overlay-selector ul').animate({ translateX: '-=' + t.slideWidth * t.slidesPerScreen + 'px' });
        t.hideShowNavigation(layers);
      }
    },

    canSlideBack: function () {
      var t = this;
      return this.currentSlide != 1;
    },

    canSlideForward: function () {
      var t = this;
      return t.totalSlides - t.currentSlide >= t.slidesPerScreen;
    },

    hideShowNavigation: function (layers) {
      var t = this;
      if (t.canSlideBack()) {
        layers.find('a.prev').removeClass('disabled');
      } else {
        layers.find('a.prev').addClass('disabled');
      }

      if (t.canSlideForward()) {
        layers.find('a.next').removeClass('disabled');
      } else {
        layers.find('a.next').addClass('disabled');
      }

    },

    toggleSelectorLayer: function (layers) {
      var t = this;
      if (layers.find('.mejs-overlay-selector').is(":visible")) {
        t.hideSelectorLayers(layers);
      } else {
        t.showSelectorLayers(layers);
      }

    },

    showSelectorLayers: function (layers) {
      var t = this;
      layers.find('.mejs-overlay-button').hide();
      layers.find('.mejs-overlay-selector-background').animate({ height: t.options.selectorBackgroudHeight }, 100);
      layers.find('.mejs-overlay-selector').fadeIn(100);

      var selectorLayer = layers.find('.mejs-overlay-selector');
      t.slideWidth = selectorLayer.find('li').outerWidth(true);
      t.sliderWidth = selectorLayer.find('.slider-wrap').width();
      t.slidesPerScreen = Math.floor(t.sliderWidth / t.slideWidth);

      t.hideShowNavigation(layers);
    },

    hideSelectorLayers: function (layers) {
      var t = this;

      layers.find('.mejs-overlay-button').show();

      layers.find('.mejs-overlay-selector-background').animate({ height: 0 }, 100);
      layers.find('.mejs-overlay-selector').fadeOut(100);
    }
  });

})(mejs.$);