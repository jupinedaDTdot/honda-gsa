(function($) {

	$.extend(mejs.MepDefaults, {

	});

	// Share Button
	$.extend(MediaElementPlayer.prototype, {
		buildshare: function(player, controls, layers, media) {
			var t = this;

			var	shareButton =
				$('<div class="mejs-button mejs-share-button mejs-share">' +
					'<button type="button" aria-controls="' + t.id + '" ></button>' +
				'</div>');
				shareButton.appendTo(controls)
				.click(function(e) {
					e.preventDefault();
					t.toggleShareLayer(layers);
				});

			/*var	shareBackgroundLayer =
				$('<div class="mejs-overlay mejs-layer mejs-overlay-share-background" style="display:none;"></div>');
			shareBackgroundLayer.appendTo(layers);*/

			var	shareLayer =
				$('<div class="mejs-overlay mejs-layer mejs-overlay-share" style="display:none;"><div class="mejs-overlay-share-inner">' +
					'<h3>'+t.options.share_box_title+'</h3>' +
					'<p>'+t.options.share_box_subtitle+'</p>' +
					'<div class="share_url_wrap"><div class="share_url"><input type="text" readonly="readonly" value="'+t.buildShareURL()+'"></div></div>' +
					'<a class="close"></a>' +
				'</div></div>');
			shareLayer.appendTo(layers);

			t.buildShareURL();

			shareLayer.find('a.close').click(function(e){
				e.preventDefault();
				t.toggleShareLayer(layers);

			})

		},

		buildShareURL: function() {
			var t = this, o = this.options;
			var url = o.share_url;
			if (o.share_video_anchor_enabled && t.currentVideoIdx > 1) {
				url += '#videoId=' + t.currentVideoIdx;
			}
			return url;
		},

		updateShareURL: function() {
			var t = this, o = this.options;
			t.layers.find('.share_url input').val(t.buildShareURL());
		},

		toggleShareLayer: function(layers) {
			var t = this;
			t.updateShareURL();
			//layers.find('.mejs-overlay-share-background').toggle();
			layers.find('.mejs-overlay-share').toggle();

		}
	});
	
})(mejs.$);