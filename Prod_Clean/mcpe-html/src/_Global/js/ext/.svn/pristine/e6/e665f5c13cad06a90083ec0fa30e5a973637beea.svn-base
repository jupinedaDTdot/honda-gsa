﻿/* $Id$ */

; (function ($) {
  var env = global_get('environment');
  var _defaults = {
    url: '/api/BuildIt/colors/{year}/{vehicle}/{trim}/{lang}',
    vehicle: '',
    trim: '',

    current_exterior: null,
    current_interior: null,

    add_exterior: function (color) { },
    add_interior: function (color) { },
    switch_exterior: function (color_key) { },
    switch_interior: function (color_key) { },

    _last: null
  };

  $.widget('honda.match_color', {
    widgetEventPrefix: 'match_color_',
    options: $.extend(true, {}, _defaults, (function () {
      try {
        return global_get('honda.match_color.defaults');
      }
      catch (e) {
        return {};
      }
    })()),
    _resetData: function () {
      this._exteriors = [];
      this._data = null;
    },
    // initializer
    _create: function () {
      this._resetData();
    },
    // load trim and transmission data from the data source
    loadVehicleAndTrim: function (vehicle, trim, year) {
      var self = this,
			o = this.options;

      if (year) {
        o.year = year;
      }

      if (vehicle) {
        o.vehicle = vehicle;
      }

      if (trim) {
        o.trim = trim;
      }

      this.reloadData();
    },
    reloadData: function () {
      var self = this,
	      o = this.options,
	      url = $.isFunction(o.url) ? o.url() : o.url;

      $.ajax({
        url: url.replace('{year}', o.year).replace('{vehicle}', o.vehicle).replace('{trim}', o.trim).replace('{lang}', o.current_lang).replace('{province}', o.province_key),
        type: 'GET',
        dataType: 'json',
        global: false,
        cache: false,
        success: function (data, status, xhr) {

          self._data = data;
          self._exteriors = data.keys;

          console.log('match color data loaded', data);

          // iterate all trims
          $.each(data.keys, function (index, exterior_key) {

            self._will_add_exterior(null, {
              color_key: exterior_key,
              color: data[exterior_key],
              data: self._data
            });
            o.add_exterior.call(self, {
              color_key: exterior_key,
              color: data[exterior_key],
              data: self._data
            });
            // iterate all transmissions in a trim
            $.each(data[exterior_key].interior.keys, function (index, interior_key) {
              var interior = data[exterior_key].interior[interior_key];

              self._will_add_interior(null, {
                color_key: interior_key,
                color: interior,
                exterior_key: exterior_key,
                data: self._data
              });
              o.add_interior.call(self, {
                color_key: interior_key,
                color: interior,
                exterior_key: exterior_key,
                data: self._data
              });
              self._did_add_interior(null, {
                color_key: interior_key,
                color: interior,
                exterior_key: exterior_key,
                data: self._data
              });
            });

            self._did_add_exterior(null, {
              color_key: exterior_key,
              color: data[exterior_key],
              data: self._data
            });
          });

          self._success(data, status, xhr);
        },
        error: function (xhr, err) {
          self._error(xhr, err);
        }
      });
    },
    switchExterior: function (event, color_key) {
      var self = this,
			o = this.options;

      this._will_switch_exterior(event, { color_key: color_key, data: self._data });
      if (self._exteriors.indexOf(color_key) != -1) {
        o.current_exterior = color_key;
        o.switch_exterior.call(self, { color_key: color_key, data: self._data });
        this._did_switch_exterior(event, { color_key: color_key, data: self._data });
      }
      else {
        throw new Error('color_key does not exist');
      }
    },
    switchInterior: function (event, color_key) {
      var self = this,
			o = this.options,
			data = this._data;
      var exterior_key = o.current_exterior;
      this._will_switch_interior(event, { color_key: color_key, exterior_key: exterior_key, data: self._data });
      if (data[exterior_key].interior.keys.indexOf(color_key) != -1) {
        o.current_interior = color_key;
        o.switch_interior.call(self, { color_key: color_key, exterior_key: exterior_key, data: self._data });
        this._did_switch_interior(event, { color_key: color_key, exterior_key: exterior_key, data: self._data });
      }
      else {
        throw new Error('color_key does not exist');
      }
    },

    // events
    _will_add_exterior: function (event, ui) {
      this._trigger('will_add_exterior', event, ui);
    },
    _did_add_exterior: function (event, ui) {
      this._trigger('did_add_exterior', event, ui);
    },
    _will_add_interior: function (event, ui) {
      this._trigger('will_add_interior', event, ui);
    },
    _did_add_interior: function (event, ui) {
      this._trigger('did_add_interior', event, ui);
    },
    _will_switch_exterior: function (event, ui) {
      this._trigger('will_switch_exterior', event, ui);
    },
    _did_switch_exterior: function (event, ui) {
      this._trigger('did_switch_exterior', event, ui);
    },
    _will_switch_interior: function (event, ui) {
      this._trigger('will_switch_interior', event, ui);
    },
    _did_switch_interior: function (event, ui) {
      this._trigger('did_switch_interior', event, ui);
    },
    _error: function (xhr, err) {
      this._trigger('load_error', null, { xhr: xhr, err: err });
    },
    _success: function (data, status, xhr) {
      this._trigger('load_success', null, { data: data, status: status, xhr: xhr });
    }
  });

})(jQuery);