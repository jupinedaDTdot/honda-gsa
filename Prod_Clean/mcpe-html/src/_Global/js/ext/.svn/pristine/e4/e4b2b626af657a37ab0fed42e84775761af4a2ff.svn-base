﻿/// <reference path="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPqRQTPIAx6XSS5Xo5YywMsyozP7FkgCA&sensor=true"/>
/// <reference path="~/_Global/js/ext/knockout-3.1.0.js"/>

var Climax;
(function(climax) {
    (function(dealerLocator) {
        var self;
        var recordCount = 10;
        var recordCountCity = 5;
        var mapZoomLevel = 8;

        /// <summary>Constructor.</summary>
        /// <param name="options" type="Object">Options parameter.</param>
        var dealerCollectionController = function(options) {
            self = this;

            // Create fields
            self._clientLocation = options.clientLocation || {};
            self._dealersListElement = options.dealersListElement;
            self._dealersMapElement = options.dealersMapElement;
            self._cityListElement = options.cityListElement;
            self._language = options.language || "en";
            self._selectedDealer = null;
            self._dealerMarkers = new Array();
            self._infoWindows = new Array();
            self._isPopup = options.isPopup;
            self.searchCriteria = options.searchCriteria;
            self._isDealerDetailPage = options.isDealerDetailPage;
            self._pageSize = options.searchCriteria._pageSize;
            self._dealerLocatorUrl = self._language == "fr" ? "/trouver-concessionaire" : "/dealerlocator";
            self._dealerLocation = {};
            if (self.searchCriteria._productLine == undefined) self.searchCriteria._productLine = "";
            // Create the view-models and bind them
            self._dealersViewModel = {
                AllDealers: ko.observableArray(),
                Dealers: ko.observableArray(),
                Unit: ko.observable(),
                MatchingType: ko.observable(),
                IsPopup: ko.observable(self._isPopup),
                HasSelection: ko.observable(true),
                CurrentPage: ko.observable(0),
                Message : ko.observable(),
                MessageDisplay : ko.observable(false)
            };

            self._dealersViewModel.postalCode = ko.observable("").extend({
                required: true,
                pattern: {
                    message: (self._language == "fr" ? "Veuillez entrer un code postal valide." : "Please enter a valid postal code."),
                    params: /^[a-zA-Z][0-9][a-zA-Z]( ?[0-9][a-zA-Z][0-9])$/

                }
            });

            $.extend(self._dealersViewModel, {
                // computed
                HasNextPage: ko.computed(function() {
                    return (this.CurrentPage() + 1) * self._pageSize < this.AllDealers().length;
                }, self._dealersViewModel),
                HasPrevPage: ko.computed(function() {
                    return this.CurrentPage() > 0;
                }, self._dealersViewModel),
                TotalPages: ko.computed(function() {
                    if (this.AllDealers().length > 0)
                        return Math.ceil(this.AllDealers().length / self._pageSize);
                    else
                        return 1;
                },self._dealersViewModel),
                HasData: ko.computed(function() {
                    return  this.AllDealers().length > 0 ;
                }, self._dealersViewModel),

                // event handlers
                nextPage: function() {
                if ((this.CurrentPage() + 1) * self._pageSize < this.AllDealers().length) {
                        var model = self._dealersViewModel;
                        setDealerPage(model, model.CurrentPage() + 1, self._pageSize);
                    }
                },
                prevPage: function() {
                    var model = self._dealersViewModel;
                    setDealerPage(model, Math.max(model.CurrentPage() - 1, 0), self._pageSize);
                },
                //checkbox event handlers
                IsPowerHouse: ko.observable(self.searchCriteria._isPowoerHouse),
                PowerHouseChanged: function() {
                    self.searchCriteria._isPowoerHouse = this.IsPowerHouse();
                    self.bindDealersBySearchCriteria(self.searchCriteria);
                    return true;
                },
                IsLeasing: ko.observable(self.searchCriteria._isLeasing),
                LeasingChanged: function() {
                    self.searchCriteria._isLeasing= this.IsLeasing();
                    self.bindDealersBySearchCriteria(self.searchCriteria);
                    return true;
                },
                isPostalCodeVisible: ko.observable(false),
                optionYes:  function () {
                    this.isPostalCodeVisible(true);
                },
                optionNo: function() {
                    location.href = self._dealerLocatorUrl;
                },
                getDealerList: function () {
                    //to initialise reset dealers , cities as this functionality is not initialised.
                    this.Dealers(new Array());
                    self._cityViewModel.Cities(new Array());
                    if (this.errors().length == 0) {
                        options.searchCriteria._mode = "ZipCode";
                        options.searchCriteria._postalcode = this.postalCode();
                        self.bindDealersBySearchCriteria(self.searchCriteria);
                    }
                }
            });

            self._cityViewModel = {
                Cities: ko.observableArray()
            };

             $.extend(self._cityViewModel, {
              HasData: ko.computed(function() {
                    return  this.Cities().length > 0 ;
                }, self._cityViewModel),
              getDealerByCity: function (city) {
                  options.searchCriteria._mode = "City";
                  options.searchCriteria._city = city.Name;
                  self.bindDealersBySearchCriteria(self.searchCriteria);
                  if(self._dealersViewModel.AllDealers().length > 0) {
                      $('.FAD-dealers').slideUp(100, function () {
                            $('.FAD-search-results, .FAD-see-all-container').fadeIn(900);
                      });
                  }
              }
              });


            self._pinPath = options._pinPath;
            //set for diffent requirement i.e used on finddealer page, dealer detail page, of used on Mobile page
            self._isMobile = options.searchCriteria._isMobile;
            self._hasMap = (options.dealersMapElement != null);
            self._hasCity = (options.cityListElement != null);
            self._recordCountDealers = options._recordCountDealers || recordCount;
            // Initialize the dealer controller
            if(!self.searchCriteria._isBuildAndPrice) {
                this._initialize();
            }

            self._dealersViewModel.errors = ko.validation.group(self._dealersViewModel);

            //ko.cleanNode(this._dealersListElement);
            ko.applyBindings(this._dealersViewModel, this._dealersListElement);
            if (self._hasCity) {
                //ko.cleanNode(this._cityListElement);
                ko.applyBindings(this._cityViewModel, this._cityListElement);
            }

        };

        ///<summary>Initializes the dealer-controller object. It populates the map and dealer result set based on the client's location.</summary>
        dealerCollectionController.prototype._initialize = function() {
            //Populate the map and dealer's result set based on the client's location
            this.bindDealersBySearchCriteria(this.searchCriteria);
        };

        ///<summary>Disposer.</summary>
        dealerCollectionController.prototype.dispose = function() {

            // Clear fields
            this._clientLocation = null;
            this._dealersListElement = null;
            this._dealersMapElement = null;
            this._dealerItemSelector = null;
            this._language = null;
            this._selectedDealer = null;
            this._map = null;
            this._dealerMarkers = null;
            this._infoWindows = null;
            this._dealerLocatorUrl = null;
            // Clear view-models
            this._dealersViewModel = null;
            this._self._dealerLocation = null;
        };

        dealerCollectionController.prototype.updateMap = function() {
            setMapBoundaries(self._dealerMarkers);
            google.maps.event.trigger(self._map, 'resize');
            self._map.panTo(self._dealerMarkers[0].getPosition());
        };
        ///<summary>Gets the selected dealer.</summary>
        dealerCollectionController.prototype.getSelectedDealer = function() {
            return this._selectedDealer;
        };

        function setDealerPage(viewModel, pageIndex, pageSize) {
            viewModel.Dealers(viewModel.AllDealers().slice(pageIndex * pageSize, (pageIndex + 1) * pageSize));
            viewModel.CurrentPage(pageIndex);
        }

        ///<summary>Binds dealer to the map and dealers list.</summary>
        ///<param name="dealers" type="Object" parameterArray="true">Dealers to be bound.</param>
        var bindDealers = function(dealers, pageSize) {

            // Clear the effects of previous result
            clearPreviousResultEffects();

            var viewModel = new Array();
            if (dealers) {
                $.each(dealers.Items, function(index, item) {
                    var dealer = item;
                    // Add the dealer to the view-model
                    var viewModelDealer = generateViewModelDealer(dealer);
                    viewModel.push(viewModelDealer);
                });

                    if (dealers.Message != undefined) {
                        if (dealers.Message.toLowerCase().indexOf('no matching location found for this ip')) {
                            self._dealersViewModel.MessageDisplay(false);
                        }
                        else {
                            self._dealersViewModel.MessageDisplay(true);
                            self._dealersViewModel.Message(dealers.Message);
                        }
                    }

                }

            //if (dealers.Items.length > 0) {
            if (true) {
                // Update view-model
                self._dealersViewModel.AllDealers(viewModel);
                setDealerPage(self._dealersViewModel, 0, pageSize);
                self._dealersViewModel.Unit(dealers.UnitName);
                self._dealersViewModel.MatchingType(dealers.MatchingType);

                if (self._hasMap && dealers.Items.length > 0) {
                    if (dealers) {
                        var configdata = getDealerLocationDetails(dealers);
                        ShowMapUsingDealerAPI(configdata, dealers, 'dealersMapSection', '/_Global/img/Honda_pin/honda_pin.png');
                    }
                }
                if (self._hasCity) {
                    if (dealers.Items.length > 0) {
                        //todo : fix issue of NearbyCities
                        if (dealers.Matching.GeoOrigin) {
                            self._dealerLocation = dealers.Matching.GeoOrigin;
                        }else {
                            self._dealerLocation = dealers.Items[0].Dealer.Location;
                        }
                        bindCitiesByLocation(self._dealerLocation);
                    }else {
                        self._cityViewModel.Cities(new Array());//to set empty set for viewModel
                    }
                }
             } else {
                //todo : no dealer found scenario.
                //this.dispose();

                //self.
            }

        };

        ///
        ///
        ///
        var getDealerLocationDetails = function(dealers) {
            var viewModel = new Array();
            var configdata = { "nelat": 44.08926, "nelng": -78.692459, "swlat": 43.242616, "swlng": -79.892007, "miniView": true };

            var latList = new Array();
            var longList = new Array();

            if (dealers) {
                $.each(dealers.Items, function(index, item) {
                    var dealer = item;

                    // find min and max values for Map.
                    latList.push(dealer.Dealer.Location.Coordinate.Latitude);
                    longList.push(dealer.Dealer.Location.Coordinate.Longitude);
                });

                //{"nelat":44.08926,"nelng":-78.692459,"swlat":43.242616,"swlng":-79.892007,"miniView":true}
                configdata.nelat = Math.max.apply(Math, latList);
                configdata.nelng = Math.max.apply(Math, longList);
                configdata.swlat = Math.min.apply(Math, latList);
                configdata.swlng = Math.min.apply(Math, longList);

                console.log(configdata);
            }
            return configdata;
        };


        ///<summary>Generates a view-model dealer from the model dealer.</summary>
        ///<param name="dealer" type="Object" mayBeNull="false">The dealer to be converted to the view-model dealer.</param>
        var generateViewModelDealer = function(dealer) {
            if (!dealer)
                throw { name: 'NullError', message: 'Dealer cannot be null.' };

            var viewModelDealer = $.extend({}, dealer);

            viewModelDealer.PhoneNumberFormatted = ko.computed(function() {
                if (viewModelDealer.Dealer.ContactInformation.Phones == null || viewModelDealer.Dealer.ContactInformation.Phones.length == 0) {
                    return '';
                }
                return formatePhoneNumber(viewModelDealer.Dealer.ContactInformation.Phones[0].Number);
            });

            viewModelDealer.DistanceFormatted = ko.computed(function() {
                    return localizeForNumber(self._language,parseFloat(viewModelDealer.DistanceToOrigin).toFixed(1));
            });


            viewModelDealer.IsPowerHouse = ko.computed(function() {
                if (viewModelDealer.Dealer.Caliber.Code == "1")
                    return true;
                else
                    return false;
            });

            viewModelDealer.PowerHouseText = ko.computed(function() {
                return (viewModelDealer.Dealer.Caliber.Code == "1" ? (self._language == "fr" ? "Centre Honda" : "PowerHouse Dealer") : "");
            });

            if (!(self._isMobile)) {
                viewModelDealer.DealerDetailUrl = ko.computed(function() {
                    var urlTemplte = "{dealerlocator}/{provincename}/{city}/{exportkey}";
                    var url;
                    url = urlTemplte.replace("{dealerlocator}", self._dealerLocatorUrl)
                        .replace("{provincename}", repDealerSearchProvince(viewModelDealer.Dealer.Location.Province.Name))
                        .replace("{city}", repDealerSearch(viewModelDealer.Dealer.Location.City.Name))
                        .replace("{exportkey}", viewModelDealer.Dealer.ExportKey);

                    return url.toLowerCase();
                });
            } else {
                //Mobile specific databinding
                viewModelDealer.DealerMapUrl = ko.computed(function() {
                    //http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=554+College+St.+Toronto,+&ie=UTF8&hq=&hnear=554+College+St.+Toronto,,+,+Canada&t=h&z=17&iwloc=A
                    var urlTemplte = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q={street}+{city},+{provincename}&ie=UTF8&hq=&hnear={street}+{city},,+{provincename},+{country}&t=h&z=17&iwloc=A";
                    var url;
                    url = urlTemplte.replace(/{street}/g, viewModelDealer.Dealer.Location.Address.replace(/ /g, "+"))
                        .replace(/{city}/g, viewModelDealer.Dealer.Location.City.Name)
                        .replace(/{provincename}/g, viewModelDealer.Dealer.Location.Province.Name)
                        .replace(/{country}/g, 'Canada');

                    return url;
                });

                viewModelDealer.PhoneNumberLink = ko.computed(function() {
                    if (viewModelDealer.Dealer.ContactInformation.Phones == null || viewModelDealer.Dealer.ContactInformation.Phones.length == 0) {
                        return '';
                    }
                    return "tel:" + viewModelDealer.Dealer.ContactInformation.Phones[0].Number;
                });

            }

            viewModelDealer.isSelected = ko.observable();
            viewModelDealer.selectedChanged = dealerSelectedChangeHandlerNew;
            return viewModelDealer;
        };

        var dealerSelectedChangeHandlerNew = function(currentDealer) {
            if (currentDealer) {
                //showBubbleInLib(".dealer_popup_wrapper", feature, map, marker);
            }
            //event.preventDefault();
        };


        ///<summary>Clears the effects of previous result set from map and the dealers' list.</summary>
        var clearPreviousResultEffects = function() {
            // Scroll the dealer's list section to top
            if (self._dealersListElement != null)
                self._dealersListElement.scrollTop = 0;
            if (self._hasCity) {
                if (self._dealersListElement != null)
                    self._cityListElement.scrollTop = 0;
            }
        };

        // new function MCPE lease dealer
        ///<summary>Binds the dealers for the provided postal code.</summary>
        ///<param name="postalCode" type="String" mayBeNull="false">Postal code. The postal code format should match
        ///Canadian postal code form like (A1A1A1 or A1A 1A1)</param>
        dealerCollectionController.prototype.bindDealersBySearchCriteria = function(searchCriteria) {

            // get url with parameters
            var url = getUrlwithParametersforAPI(searchCriteria);
              if ($.browser.msie && window.XDomainRequest) {
                  url = url.concat( ((url.indexOf("?") >= 0) ? '&' : '?' ) +    'AcceptLanguage=' + self._language);
              }
            // Get dealer from API and update the view-model
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                crossdomain: true,
                 headers: {
                    'Accept-Language': self._language
                },
                success: function(data) {
                    if (data) {
                        bindDealers(data, self._pageSize);
                    }
                },
                error: function(error) {
                    if (!(self._isMobile)) {
                        if(!(error.responseText || "") == "")
                            $('.sorry').text(error.responseText);
                        else {
                            if (self._language == 'fr')
                                $('.sorry').text("Nous sommes désolés, il n’y aucun concessionnaire {productline} près de votre domicile.".replace("{productline}", searchCriteria._productLine ));
                            else
                                $('.sorry').text("Sorry, there are no {productline} dealers near you".replace("{productline}", searchCriteria._productLine));
                        }
                    }
                }
            });
        };

        //var url = "/api/dealerapi/Honda/dealer/closest";
        //var urlTemplate = "/api/dealerapi/honda/dealer/province/{provinceId}";


        // new function MCPE lease dealer
        ///<summary>Binds the dealers for the provided postal code.</summary>
        ///<param name="postalCode" type="String" mayBeNull="false">Postal code. The postal code format should match
        ///Canadian postal code form like (A1A1A1 or A1A 1A1)</param>
        //dealerCollectionController.prototype.bindCitiesByLocation= function(location) {
        var bindCitiesByLocation = function(location) {
            // get url with parameters
            //url = __apiRoot;
            var urlTemplete = __apiRoot + "/nearbycities/{provincecode}/{latitude}/{longitude}?ip={valueip}"; //to do add from location.
            var url = urlTemplete.replace("{provincecode}", location.Province.Abbreviation).replace("{latitude}", location.Coordinate.Latitude).replace("{longitude}", location.Coordinate.Longitude).replace("{valueip}", self.searchCriteria._userIP);
            if(! (self.searchCriteria._mode == 'Province' ||self.searchCriteria._mode == 'DealerID'  ) ){
                    if (self.searchCriteria._isLeasing == true)
                        url += "&financialOption=leasing";

                    if (self.searchCriteria._isPowoerHouse == true)
                        url += "&powerhouse=true";
             }

             if ($.browser.msie && window.XDomainRequest) {
                  url = url.concat( ((url.indexOf("?") >= 0) ? '&' : '?') +    'AcceptLanguage=' + self._language);
              }

            //location.Coordinate.Latitude, location.Coordinate.Longitude
            // Get dealer from API and update the view-model
            $.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                crossdomain: true,
                headers: {
                    'Accept-Language': self._language
                },
                success: function(data) {
                    if (data)
                        bindCities(data);
                },
                error: function(error) {
                    $('.cities_nearby').hide();
                }
            });
        };

        var bindCities = function(cities) {
            //clearPreviousResultEffects();

            var viewModel = new Array();
            if (cities) {
                if (self._dealerLocation.Province) {
                    $.each(cities, function(index, item) {
                        if (index > (recordCountCity - 1)) {
                            return false;
                        }

                        var city = item;

                        // Add the dealer to the view-model
                        var viewModelCity = generateViewModelCity(city, self._dealerLocation);
                        viewModel.push(viewModelCity);

                    });
                }
            }
            self._cityViewModel.Cities(viewModel);
        };

        var generateViewModelCity = function(city, location) {
            if (!city)
                throw { name: 'NullError', message: 'City cannot be null.' };

            var viewModelCity = $.extend({}, city);

            viewModelCity.Url = ko.computed(function() {

                //var url = template.replace("{city}", repDealerSearch(viewModelCity.Name)).replace("{province}", repDealerSearch(location.Name));

                var urlTemplte = "{dealerlocator}/{provincename}/{city}";
                var url = "";
                url = urlTemplte.replace("{dealerlocator}", self._dealerLocatorUrl)
                    .replace("{provincename}", repDealerSearchProvince(location.Province.Name))
                    .replace("{city}", encodeURIComponent(viewModelCity.Name));
                return url.toLowerCase();
            });

            return viewModelCity;
        };


                ///<summary> get url with parameters.</summary>
        ///<param name="searchCriteria" type="searchCriteria" mayBeNull="false">
        /// "/HttpHandlers/GetDealers.ashx?postalcode={postalcodevale}&financialOption={financialOptionvalue}"
        var getUrlwithParametersforAPI = function(searchCriteria) {

            var url = __apiRoot; //"http://integration.preview.api.honda.ca/dealer/m"

            if (! (searchCriteria._mode == undefined || searchCriteria._mode == null)) {

                url = url.replace("{DealerSearchModeValue}", searchCriteria._mode);
                if (searchCriteria._userIP == "127.0.0.1") searchCriteria._userIP = "66.241.130.174";// IP address out side canada"27.63.255.255";
                switch (searchCriteria._mode) {
                case 'ZipCode':
                    url += ("/postalcode/{postalcodevalue}?ip={valueip}").replace("{postalcodevalue}", searchCriteria._postalcode).replace("{valueip}", searchCriteria._userIP + "");
                    break;
                case 'City':
                    if (!(searchCriteria._province == null || searchCriteria._province == "" || searchCriteria._province == undefined))
                        url += ("/city/{cityvalue}/{provincevalue}?ip={valueip}").replace("{cityvalue}", searchCriteria._city).replace("{provincevalue}", searchCriteria._province).replace("{valueip}", searchCriteria._userIP);
                    else {
                        if (searchCriteria._city == null || searchCriteria._city == undefined || searchCriteria._city == "")
                            searchCriteria._city = searchCriteria._searchCity;
                         url += ("/city/{cityvalue}?ip={valueip}").replace("{cityvalue}", searchCriteria._city).replace("{valueip}", searchCriteria._userIP);
                        }
                    break;
                case 'Province':
                    url += ("/province/{provincevalue}").replace("{provincevalue}", searchCriteria._province == "" ?searchCriteria._provinceName : "");
                    break;
                case 'DealerID':
                    url += ("/id/{idvalue}").replace("{idvalue}", searchCriteria._dealerID || "");
                    break;
                case 'ip':
                    url += ("?ip={ipvalue}").replace("{ipvalue}", searchCriteria._userIP || "");
                    break;
                case 'Financial':
                    url += ("/financial/{idvalue}").replace("{idvalue}", searchCriteria._dealerID || "");
                    break;
                default:
                    console.log('search mode is:' + searchCriteria._mode);
                }

                if(! (searchCriteria._mode == 'Province' ||searchCriteria._mode == 'DealerID'  ) ){

                    if (searchCriteria._isLeasing == true)
                        url += "&financialOption=leasing";

                    if (searchCriteria._isPowoerHouse == true)
                        url += "&powerhouse=true";

                    if (searchCriteria._useRequestIp == true)
                        url += ("&useRequestIp=true");
                }

            } else {
                console.log('there is no search mode:notfound value:' + searchCriteria._mode);
                url = "";
            }
            return url;
        };

        dealerLocator.DealerCollectionController = dealerCollectionController;
    })(climax.DealerLocator || (climax.DealerLocator = {}));
})(Climax || (Climax = {}));


/// <param name="str" type="String">
/// </param>
/// <returns type="String"></returns>
function repDealerSearch(str) {
    str = str.replace(/ /g, '_').replace('&', 'AND').replace('(', '').replace(')', '').replace(',', '');//replace('.', '').replace("'", '').
    return str;
};

function repDealerSearchProvince(str) {
    str = str.replace(/ /g, '_').replace('&', 'AND').replace('.', '').replace('(', '').replace(')', '').replace("'", '').replace(',', '').replace(/-/g, '_');
    return str;
}

function formatePhoneNumber(phno) {

    var areaCode = phno.substring(0, 3);
    var firstPart = phno.substring(3, 6);
    var secondPart = phno.substring(6, 10);

    var numberTemplate = '{0}-{1}-{2}';
    var number = numberTemplate
                    .replace("{0}", areaCode)
                    .replace("{1}", firstPart)
                    .replace("{2}", secondPart);

    return number;
}

function localizeForNumber(lang, data) {
    if (lang == "fr")
        return data.replace(".", ",");
    else
        return data;
}

function ShowMapUsingDealerAPI(config, data, areaId, pin) {
    //loads viewer center and zoom level
    var viewerData = config;
    var dim = $("#" + areaId);
    var center, zoom, mini;
    //var bounds;
    try {
        //viewerData = $.parseJSON(viewerData);
        var ne = new google.maps.LatLng(viewerData.nelat, viewerData.nelng);
        var sw = new google.maps.LatLng(viewerData.swlat, viewerData.swlng);
        var bounds = new google.maps.LatLngBounds(sw, ne);
        center = bounds.getCenter();
        zoom = getBoundsZoomLevel(viewerData.nelat, viewerData.nelng, viewerData.swlat, viewerData.swlng, dim.height() * 0.9, dim.width() * 0.9);
        mini = viewerData.miniView;
    } catch (e) {
        center = null;
    }
    var mapOptions = {
        center: center, //new google.maps.LatLng(-34.397, 150.644),
        zoom: zoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        overviewMapControl: mini,
        overviewMapControlOptions: { opened: true }
    };
    var map = new google.maps.Map(document.getElementById(areaId), mapOptions);

    //creates generic (and global) location bubble object
    var infowindow = new google.maps.InfoWindow({
        //maxWidth: 295,
        content: 'empty'
    });

    function showBubble(dealerPopupWrapper, feature, map, marker) {
        var urlTemplte = "{dealerlocator}/{provincename}/{city}/{exportkey}";
        var url;
        url = urlTemplte.replace("{dealerlocator}", self._dealerLocatorUrl)
                        .replace("{provincename}", repDealerSearchProvince(feature.Location.Province.Name))
                        .replace("{city}", repDealerSearch(feature.Location.City.Name))
                        .replace("{exportkey}", feature.ExportKey);

         url= url.toLowerCase();

        var content = $(dealerPopupWrapper).html()
            .replace('{0}', feature.Name)
            .replace('{1}', feature.Location.Address)
            .replace('{2}', feature.Location.City.Name)
            .replace('{3}', feature.Location.Province.Abbreviation)
            .replace('{4}', feature.Location.PostalCode.Value)
            .replace('{5}', formatePhoneNumber(feature.ContactInformation.Phones[0].Number))
            .replace('{6}', url)
            .replace('{7}', feature.ImageUrl)
            .replace('{8}', feature.Name);
        infowindow.setContent(content);
        infowindow.open(map, marker);
        //todo:api get default image if image not exist.
    }

    //function to add a pin on the map
    function addMarker(feature) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(feature.Location.Coordinate.Latitude, feature.Location.Coordinate.Longitude), //feature.position,
            icon: pin, //iconBase + 'schools_maps.png',//icons[feature.type].icon,
            map: map,
            //flat: false,
            title: feature.Name
        });
        return marker;
    }

    //clicking on the map, but outside the bubble, should close the bubble
    google.maps.event.addListener(map, 'click', function() {
        infowindow.close();
    });

    //add a pin for each dealer location
    var dealers = data;
//	try {
//		dealers = $.parseJSON(dealers);
//	}
//	catch (e) {
//		dealers = null;
//	}

    if (dealers) {
        $.each(dealers.Items, function(index, item) {
            var dealer = item.Dealer;
            if (dealer) {
                var feature = item.Dealer;
                var marker = addMarker(feature);
                //todo:enable this for dealer detial page (need to put condition.)
                //if (this.Clickable) {
                //clicking the location should open the bubble
                google.maps.event.addListener(marker, 'click', function() {
                    showBubble(".dealer_popup_wrapper", feature, map, marker);
                });
                var locationIcon = $("[dealerid='" + feature.Id + "']");
                locationIcon.click(function() {
                    showBubble(".dealer_popup_wrapper", feature, map, marker);
                    event.preventDefault(); //return false;
                });
                //}

                //dealer.Dealer.Location.Coordinate.Latitude;
            }


        });

    }

    function getBoundsZoomLevel(nelat, nelng, swlat, swlng, mapHeight, mapWidth) {
        var WORLD_DIM = { height: 256, width: 256 };
        var ZOOM_MAX = 21;

        function latRad(lat) {
            var sin = Math.sin(lat * Math.PI / 180);
            var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
            return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
        }

        function zoom(mapPx, worldPx, fraction) {
            return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
        }

        var latFraction = (latRad(nelat) - latRad(swlat)) / Math.PI;

        var lngDiff = nelng - swlng;
        var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        var latZoom = zoom(mapHeight, WORLD_DIM.height, latFraction);
        var lngZoom = zoom(mapWidth, WORLD_DIM.width, lngFraction);

        if (lngFraction == 0)
            latZoom = 14;

        return Math.min(latZoom, lngZoom, ZOOM_MAX);
    }

}
