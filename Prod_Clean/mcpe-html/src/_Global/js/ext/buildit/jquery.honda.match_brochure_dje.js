﻿/* $Id$ */

; (function ($) {

    var _defaults = {
        url: '/brochure/Brochure.ashx',
        city_url: '/tools/buildit/dealers/GetDealerCities.aspx?Lang={lang}',
        dealer_url: '/tools/buildit/dealers/GetDealersForCity.aspx?Lang={lang}',

        assets_path: '',

        lang: '',

        firstname: '',
        vehiclename: '',
        trimname: '',

        model_key: '',
        trim_key: '',
        transmission_key: '',
        color_key: '',
        int_color_key: '',
        model_year: '',

        isPricingAvailable: true,
        finance_method: null,
        dealer: null,
        baseCarAsset: '',
        spec_pdf_path: '',

        Name: '',
        FName: '',
        FEmail: '',

        PackageName: '',
        NamePlatePath: '',

        ProductLine: '',

        image_size_factor_large: 35,
        image_size_factor_small: 18,

        accessories: null,
        warranty: null,
        summary: null,

        match_accessory: null,

        _last: null
    };

    $.widget('honda.match_brochure', {
        widgetEventPrefix: 'match_brochure_',
        options: $.extend(true, {}, _defaults, (function () {
            try {
                return global_get('honda.match_brochure.defaults');
            }
            catch (e) {
                return {};
            }
        })()),

        // initializer
        _create: function () {

        },
        _postIFrame: function (data) {
            var self = this, o = this.options;
            var iframe;

            if (window.frames['__honda_match_brochure_helper']) {
                iframe = window.frames['__honda_match_brochure_helper'];
            }
            else {
                iframe = $("<iframe>").attr({
                    name: '__honda_match_brochure_helper',
                    height: 0,
                    width: 0
                }).appendTo('body').get(0);
            }

            var d = document;

            var form = d.createElement('form');
            form.setAttribute('method', 'post');
            form.setAttribute('action', o.url);
            form.setAttribute('target', '_blank');

            $.each(data, function (field, value) {
                var input = d.createElement('input');
                input.setAttribute('type', 'hidden');
                input.setAttribute('name', field);
                input.setAttribute('value', value);
                form.appendChild(input);
            });

            d.body.appendChild(form);
            form.submit();
            d.body.removeChild(form);
        },
        _prepareAccessoryPaths: function (match_accessory, accessories) {
            var self = this, o = this.options;

            // iterate the accessory categories
            if (accessories == null) {
                return;
            }
            $.each(accessories.keys, function (index, category_key) {
                if (accessories[category_key]) {
                    var category = accessories[category_key];
                    if (null == category.keys && accessories[category_key].items) {
                        category = accessories[category_key].items;
                    }

                    if (category && category.keys) {
                        $.each(category.keys, function (index, acc_key) {
                            if (category[acc_key]) {
                                var acc = category[acc_key];

                                // go through the assets
                                $.each(acc.assets, function (asset_type, file) {
                                    if (file) {
                                        file = file.replace(/^.*\/\/[^\/]+/, '');
                                        acc.assets[asset_type] = file;
                                        //								      match_accessory.build_accessory_thumbnail_path(acc_key, accessories, {
                                        //									    model_key: o.model_key,
                                        //									    trim_level_key: o.trim_key,
                                        //									    transmission_key: o.transmission_key,
                                        //									    exterior_key: o.color_key,
                                        //								        model_year: o.model_year,
                                        //									    thumbnail: file
                                        //});
                                    }
                                });
                            }
                        });
                    }

                }
            });
        },
        // load vehicle
        createBrochure: function () {
            var self = this, o = this.options;
            var data = {
                firstname: o.firstname,
                vehiclename: o.vehiclename,
                trimname: o.trimname,
                model_key: o.model_key,
                year_key: o.year_key,
                trim_key: o.trim_key,
                color_key: o.color_key,
                int_color_key: o.int_color_key,
                isPricingAvailable: o.isPricingAvailable,
                finance_method: o.finance_method == null ? ['lease', 'finance'] : o.finance_method,
                dealer: o.dealer,
                baseCarAsset: o.baseCarAsset,
                spec_pdf_path: o.spec_pdf_path,
                accessories: o.accessories,
                warranty: o.warranty,
                summary: o.summary
            };


            self._prepareAccessoryPaths(o.match_accessory, data.accessories);

            self._postIFrame({
                Lang: o.lang,
                Name: o.Name,
                isPricingAvailable: o.isPricingAvailable,
                spec_pdf_path: o.spec_pdf_path,
                model_key: o.model_key,
                year_key: o.year_key,
                trim_key: o.trim_key,
                PackageName: o.PackageName,
                NamePlatePath: o.NamePlatePath,
                color_key: o.color_key,
                YourCarImage: o.image_size_factor_large,
                YourCarImageSmall: o.image_size_factor_small,
                finance_method: o.finance_method == null ? $.toJSON(['lease', 'finance']) : $.toJSON(o.finance_method),
                ProductLine: o.ProductLine,
                Data: $.toJSON(data)
            });

            self._brochure_did_create();
        },
        shareBrochure: function () {
            var self = this, o = this.options;
            var data = {
                firstname: o.firstname,
                vehiclename: o.vehiclename,
                trimname: o.trimname,

                model_key: o.model_key,
                year_key: o.year_key,
                trim_key: o.trim_key,
                color_key: o.color_key,
                int_color_key: o.int_color_key,

                PackageName: o.PackageName,
                NamePlatePath: o.NamePlatePath,

                isPricingAvailable: o.isPricingAvailable,
                finance_method: o.finance_method == null ? ['lease', 'finance'] : o.finance_method,
                dealer: null,
                baseCarAsset: o.baseCarAsset,
                spec_pdf_path: o.spec_pdf_path,
                accessories: o.accessories,
                warranty: o.warranty,
                summary: o.summary
            };

            self._prepareAccessoryPaths(o.match_accessory, data.accessories);

            $.ajax({
                type: 'POST',
                url: o.url,
                data: {
                    Lang: o.lang,
                    Name: o.Name,
                    FName: o.FName,
                    FEmail: o.FEmail,
                    isPricingAvailable: o.isPricingAvailable,
                    spec_pdf_path: o.spec_pdf_path,
                    model_key: o.model_key,
                    year_key: o.year_key,
                    trim_key: o.trim_key,
                    color_key: o.color_key,
                    YourCarImage: o.image_size_factor_large,
                    YourCarImageSmall: o.image_size_factor_small,
                    finance_method: o.finance_method == null ? $.toJSON(['lease', 'finance']) : $.toJSON(o.finance_method),
                    ProductLine: o.ProductLine,
                    Data: $.toJSON(data)
                },
                dataType: 'text',
                success: function () {
                    self._brochure_did_share();
                }
            });

        },
        loadDealerCities: function (postal_code, province) {
            var self = this, o = this.options;

            this._trigger('before_load', null, {});

            $.ajax({
                url: o.city_url.replace('{lang}', o.lang),
                type: 'POST',
                dataType: 'json',
                global: false,
                cache: false,
                data: {
                    Data: $.toJSON({ zip: postal_code, options: null }),
                    Lang: o.lang,
                    province: province || null
                },
                success: function (data, status, xhr) {
                    console.log('cities dealers did load', data);
                    self._city_dealers_did_load(data, status, xhr);

                    self._trigger('load_success', null, {});
                },
                error: function (xhr, err) {
                    self._error(xhr, err);
                }
            });
        },
        loadCities: function (postal_code) {
            var self = this, o = this.options;

            this._trigger('before_load', null, {});

            $.ajax({
                url: o.city_url.replace('{lang}', o.lang),
                type: 'POST',
                dataType: 'json',
                global: false,
                cache: false,
                data: {
                    Data: $.toJSON({ zip: postal_code, options: 'all' }),
                    Lang: o.lang
                },
                success: function (data, status, xhr) {
                    console.log('cities did load', data);
                    self._city_did_load(data, status, xhr);

                    self._trigger('load_success', null, {});
                },
                error: function (xhr, err) {
                    self._error(xhr, err);
                }
            });
        },
        loadDealers: function (postal_code, city_id) {
            var self = this, o = this.options;

            this._trigger('before_load', null, {});

            var data;
            if (city_id) {
                data = { options: { city: city_id }, zip: postal_code };
            }
            else {
                data = { options: null, zip: postal_code };
            }

            $.ajax({
                url: o.dealer_url.replace('{lang}', o.lang),
                type: 'POST',
                // dataType: 'json',
                global: false,
                cache: false,
                data: {
                    Data: $.toJSON(data),
                    Lang: o.lang
                },
                success: function (data, status, xhr) {
                    self._dealer_did_load(data, status, xhr);

                    this._trigger('load_success', null, {});
                },
                error: function (xhr, err) {
                    self._error(xhr, err);
                }
            });
        },
        _city_dealers_did_load: function (data, status, xhr) {
            this._trigger('city_dealers_did_load', null, { data: data, status: status, xhr: xhr });
        },
        _city_did_load: function (data, status, xhr) {
            this._trigger('city_did_load', null, { data: data, status: status, xhr: xhr });
        },
        _dealer_did_load: function (data, status, xhr) {
            this._trigger('dealer_did_load', null, { data: data, status: status, xhr: xhr });
        },
        _brochure_did_create: function () {
            this._trigger('brochure_did_create', null, {});
        },
        _brochure_did_share: function () {
            this._trigger('brochure_did_share', null, {});
        },
        _error: function (xhr, err) {
            this._trigger('load_error', null, { xhr: xhr, err: err });
        },
        _success: function (data, status, xhr) {
            this._trigger('load_success', null, { data: data, status: status, xhr: xhr });
        }
    });

})(jQuery);