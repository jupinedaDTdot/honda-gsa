/* $Id$ */
;(function($){
var env = global_get('environment');
var _defaults = {
	url: '/buildittool/' + env + '/data/JSON/assets/media_gallery_root/{model_key}/media_{lang}.json',
	current_vehicle: '',
	current_lang: '',
	add_image: function(image) {}
};

$.widget('honda.match_photo', {
	widgetEventPrefix: 'match_photo_',
	options: $.extend(true, {}, _defaults, (function(){
		try {
			return global_get('honda.match_photo.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
	_create: function() {
		this.resetData();
	},
	resetData: function() {
		var self = this,
			o = this.options;

		self._data = null;
	},
	show: function() {
		var self = this,
			o = this.options,
			e = this.widget();

		$(e).show();
	},
	hide: function() {
		var self = this,
			o = this.options,
			e = this.widget();

		$(e).hide();
	},
	loadVehicle: function(model_key, lang) {
		var self = this,
			o = this.options;
		if (model_key) {
			o.current_vehicle = model_key;
		}

		if (lang) {
			o.current_lang = lang;
		}

		self.reloadData();
	},
	reloadData: function() {
		var self = this,
			o = this.options;

		var url = o.url.replace('{model_key}', o.current_vehicle);
		url = url.replace('{lang}', o.current_lang);
		
		self.resetData();

		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'text',
			global: false,
			cache: false,
			success: function(data, status, xhr) {
				data = data.replace(/\/\*[\w\W]*?\*\//mg, '');
				data = data.replace(/\/\/[\w\W]*?$/mg, '');
				data = $.parseJSON(data);
				
				self._data = data;
				console.log('match photo data loaded', data);
				
				$.each(data.image_assets.keys, function(index, image_key) {
					self._will_add_image(null, {
						video_key: image_key,
						image: data.image_assets[image_key],
						data: self._data
					});

					o.add_image.call(self, {
						video_key: image_key,
						image: data.image_assets[image_key],
						data: self._data
					});

					self._did_add_image(null, {
						video_key: image_key,
						image: data.image_assets[image_key],
						data: self._data
					});
				});

				self._success(data, status, xhr);
			},
			error: function(xhr, err) {
				console.log(err)
				self._error(xhr, err);
			}
		})
	},
	_success: function(data, status, xhr) {
		this._trigger('load_success', null, {data: data, status: status, xhr: xhr});
	},
	_error: function(xhr, err) {
		this._trigger('load_error', null, {xhr: xhr, err: err});
	},
	_will_add_image: function(event, image) {
		this._trigger('will_add_image', event, image);
	},
	_did_add_image: function(event, image) {
		this._trigger('did_add_image', event, image);
	}
});
})(jQuery);