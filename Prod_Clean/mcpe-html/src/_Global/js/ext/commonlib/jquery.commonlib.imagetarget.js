;(function($, W, D, undefined) {

var _defaults = {
    trigger: 'self',
    triggerEvent: 'click',
    target: '', // has to be an image tag
    image: '', // has to be a URL to an image (in any protocol).
    change: function(event, ui) {}
};

$.widget('commonlib.imagetarget', {
    widgetEventPrefix: 'imagetarget',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.imagetarget.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        var self = this,
            o = this.options,
            w = this.widget();

        self._ensureOptions();

        $(o.trigger == 'self' ? w : o.trigger).bind(o.triggerEvent, function(event) {
            // create a new image element
            var img = $(o.target).clone();
            img.attr('src', o.image);
            $(o.target).replaceWith(img);
            self._change(event);
        });
    },
    _ensureOptions: function() {
        var self = this,
            o = this.options,
            w = this.widget();

        o.target = w.data('target') || o.target;
        o.image = w.data('image') || o.image;
    },
    _change: function(event) {
        this._trigger('change', event, {
            widget: this.widget(),
            target: this.options.target,
            image: this.options.image
        });
    }
});

})(jQuery, window, document);