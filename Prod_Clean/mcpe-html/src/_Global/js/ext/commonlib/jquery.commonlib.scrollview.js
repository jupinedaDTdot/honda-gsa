/* $Id: jquery.commonlib.scrollview.js 21201 2012-11-27 19:17:24Z margaret $ */
; (function ($, M, D, F, require, undefined) {

  var _defaults = {
    type: 'vertical',
    delayed: true,
    handleClass: 'commonlib-scrollview-handle',
    verticalHandleClass: 'commonlib-scrollview-handle-v',
    horizontalHandleClass: 'commonlib-scrollview-handle-h',
    paneClass: 'commonlib-scrollview-pane',
    verticalPaneClass: 'commonlib-scrollview-pane-v',
    horizontalPaneClass: 'commonlib-scrollview-pane-h',
    mousewheelSensitivity: 5,
    touchSensitivity: 0.5,
    nativeScrolling: true,
    useTransform: true,
    complete: function (event, ui) { },
    start: function (event, ui) { },
    stop: function (event, ui) { },
    scroll: function (event, ui) { }
  };

  $.widget('commonlib.scrollview', $.commonlib.widget, {
    widgetEventPrefix: 'scrollview',
    options: $.extend(true, {}, _defaults, (function () {
      try {
        return F.global_get('commonlib.scrollview.defaults');
      } catch (e) {
        return {};
      }
    })()),
    _ensureOptions: function () {
      var self = this,
          o = this.options,
          widget = this.widget();

      o.type = widget.data('scrollview-type') || o.type;
      o.delayed = widget.data('scrollview-delayed') || o.delayed;
      o.vendor = (/webkit/i).test(navigator.appVersion) ? 'webkit' :
            (/firefox/i).test(navigator.userAgent) ? 'Moz' :
              'opera' in window ? 'O' : '';
      o.useTransform = widget.data('scrollviewUsetransform') === undefined ? o.useTransform : widget.data('scrollviewUsetransform');
      o.hasTransform = o.vendor + 'Transform' in document.documentElement.style;

      o.handleClass = widget.data('scrollview-handle-class') || o.handleClass;
      o.verticalHandleClass = widget.data('scrollview-vertical-handle-class') || o.verticalHandleClass;
      o.horizontalHandleClass = widget.data('scrollview-horizontal-handle-class') || o.horizontalHandleClass;

      o.paneClass = widget.data('scrollview-pane-class') || o.paneClass;
      o.verticalPaneClass = widget.data('scrollview-vertical-pane-class') || o.verticalPaneClass;
      o.horizontalPaneClass = widget.data('scrollview-horizontal-pane-class') || o.horizontalPaneClass;
    },
    _isMobileWebkit: function () {
      return /(iphone|ipad|ipod|android)/i.test(navigator.userAgent);
    },
    _create: function () {
      var self = this,
          o = this.options,
          widget = this.widget();

      if (o.nativeScrolling && this._isMobileWebkit()) {
        this._createMobileWebkit();
        this._complete(null);
        return;
      }

      self._ensureOptions();

      // construct panes and handles
      self.panes = {
        v: $('<span>').addClass(o.paneClass).addClass(o.verticalPaneClass).css({
          position: 'absolute',
          top: 0,
          right: 0
        }).appendTo(widget),
        h: $('<span>').addClass(o.paneClass).addClass(o.horizontalPaneClass).css({
          position: 'absolute',
          bottom: 0,
          left: 0
        }).appendTo(widget)
      };

      self.innerPanes = {
        v: $('<span>').addClass(o.paneClass + '-inner').appendTo(self.panes.v).css('display', 'block').hide(),
        h: $('<span>').addClass(o.paneClass + '-inner').appendTo(self.panes.h).css('display', 'block').hide()
      };

      self.handles = {
        v: $('<a>').addClass(o.handleClass).addClass(o.verticalHandleClass).css({
          position: 'relative'
        }).append($('<span>')).appendTo(self.innerPanes.v),
        h: $('<a>').addClass(o.handleClass).addClass(o.horizontalHandleClass).css({
          position: 'relative'
        }).append($('<span>')).appendTo(self.innerPanes.h)
      };

      // make sure the widget is position relative (if it's not given a position value)
      if (widget.css('position') == 'static') {
        widget.css('position', 'relative');
      }

      // make sure you have the only child view
      self.subViews = [
        widget.children().first()
      ];

      if (self.subViews[0].css('position') == 'static') {
        self.subViews[0].css('position', 'relative');
      }

      // attach events
      self._attachEventHandlers();

      self._complete(null);
    },
    _createMobileWebkit: function () {
      var self = this, o = this.options, widget = this.widget(), subview = widget.children().first();

      require(['ext/iscroll/iscroll'], function () {
        if (o.type === 'vertical') {
          self._iscroll = new iScroll(widget[0], {
            hScrollbar: false,
            vScrollbar: true,
            hideScrollbar: true
          });
        }
        else if (o.type === 'horizontal') {
          self._iscroll = new iScroll(widget[0], {
            hScrollbar: true,
            vScrollbar: false
          });
        }
        else {
          self._iscroll = new iScroll(widget[0], {
            hScrollbar: true,
            vScrollbar: true
          });
        }

        self.setNeedsDisplay();

        self.scrollToTop = function () {
          self._iscroll.scrollTo(0, 0);
        };
        self.scrollToLeft = function () {
          self._iscroll.scrollTo(0, 0);
        };
      });

    },
    _attachEventHandlers: function () {
      var self = this,
          o = this.options,
          widget = this.widget();

      self.handles.v.hover(function () {
        $(this).addClass(o.verticalHandleClass + '-hover');
      }, function () {
        $(this).removeClass(o.verticalHandleClass + '-hover');
      }).disableSelection();

      self.handles.h.hover(function () {
        $(this).addClass(o.horizontalHandleClass + '-hover');
      }, function () {
        $(this).removeClass(o.horizontalHandleClass + '-hover');
      }).disableSelection();

      // if we are on touch-enabled device
      if (M.touch || !o.delayed) {
        self._createDelayed();
      } else {
        // lazy initialization until somebody actually moves the mouse cursor to the scrollview area
        widget.one('mouseenter', function () {
          self._createDelayed();
        });
      }
    },
    _scrollWith: function (pane, handle, widget, subview, type, position, event, drag) {
      var o = this.options,
          paneMeasure = 'height',
          handleMeasure = 'outerHeight',
          theother = 'h',
          dir = 'top',
          vendor = o.vendor,
          has3D = 'WebKitCSSMatrix' in window && 'm11' in new WebKitCSSMatrix(),
          trnOpen = has3D ? 'translate3d(' : 'translate(',
          trnClose = has3D ? ',0)' : ')';

      if (type === 'horizontal') {
        paneMeasure = 'width';
        handleMeasure = 'outerWidth';
        theother = 'v';
        dir = 'left';
      }

      // the pane's measure must be strictly greater than the handle's measure
      if (handle[handleMeasure]() >= pane[paneMeasure]()) {
        return;
      }

      // the subview's measure must be strictly greater than parent's measure
      if (widget[paneMeasure]() >= subview[handleMeasure]() + this.panes[theother][paneMeasure]()) {
        return;
      }

      // calculate the new position of the subview
      var newpos = position[dir] / (pane[paneMeasure]() - handle[handleMeasure]()) * (subview[handleMeasure]() - widget[paneMeasure]());
      newpos = Math.round(newpos);

      if (o.useTransform && o.hasTransform) {
        subview[0].style[vendor + 'TransformOrigin'] = '0 0';
        if (type === 'horizontal') {
          subview[0].style[vendor + 'Transform'] = trnOpen + (-newpos) + 'px,0' + trnClose;
        }
        else {
          subview[0].style[vendor + 'Transform'] = trnOpen + '0,' + (-newpos) + 'px' + trnClose;
        }
      }
      else subview.css(dir, -newpos);

      this._scroll(event, type, drag);
    },

    _createDelayed: function () {
      var self = this,
          o = this.options,
          widget = this.widget(),
          inTouchEvent = false, startX, startY, startTop, startLeft;


      function _initHandle(pane, handle, type) {
        require(['ui/jquery.ui.draggable'], function () {
          handle.draggable({
            axis: type === 'vertical' ? 'y' : 'x',
            containment: 'parent',
            scroll: false,
            start: function (event, ui) {
              self._start(event, type, ui);
            },
            stop: function (event, ui) {
              self._stop(event, type, ui);
            },
            drag: function (event, ui) {
              self._scrollWith(pane, handle, widget, self.subViews[0], type, ui.position, event, ui);
            }
          });
        });
        //            handle.bind('mousedown touchstart click', function(event) {
        //                event.preventDefault();
        //            });

        if ('ontouchstart' in window) {
          widget[0].addEventListener('touchstart', function (event) {
            inTouchEvent = true;
            startX = event.touches[0].clientX;
            startY = event.touches[0].clientY;
            startTop = handle.position().top;
            startLeft = handle.position().left;
          });

          widget[0].addEventListener('touchend', function (event) {
            inTouchEvent = false;
          });

          widget[0].addEventListener('touchcancel', function (event) {
            inTouchEvent = false;
          });
        }

        if (type === 'vertical') {
          if ('ontouchstart' in window) {
            widget.bind('touchmove', function (event) {
              if (!inTouchEvent) return;
              var t = event.originalEvent.touches,
                  deltaY = t[0].clientY - startY,
                  newtop = startTop - deltaY * o.touchSensitivity,
                  left = startLeft,
                  maxheight = pane.height() - handle.outerHeight(true);

              newtop = Math.max(newtop, 0);
              newtop = Math.min(maxheight, newtop);

              if (newtop > 0 && newtop < maxheight) {
                event.preventDefault();
                handle.css('top', newtop);
                self._scrollWith(pane, handle, widget, self.subViews[0], type, {
                  top: newtop,
                  left: left
                }, event);
              }
              else {
              }
            });
          }

          require(['includes/jquery.mousewheel'], function () {
            widget.mousewheel(function (event, delta, deltaX, deltaY) {
              var newtop = handle.position().top - deltaY * o.mousewheelSensitivity,
                  left = handle.position().left;

              newtop = Math.max(newtop, 0);
              newtop = Math.min(pane.height() - handle.outerHeight(true), newtop);

              handle.css('top', newtop);

              self._scrollWith(pane, handle, widget, self.subViews[0], type, {
                top: newtop,
                left: left
              }, event);

              event.stopPropagation();
              event.preventDefault();
            });
          });

          // create the scroll to function
          self.scrollToTop = function () {
            handle.css('top', 0);
            self._scrollWith(pane, handle, widget, self.subViews[0], type, {
              top: 0,
              left: handle.position().left
            });
          };

          // listen to click event
          pane.disableSelection().css('-webkit-user-select', 'none').click(function (event) {
            event.preventDefault();

            var x = event.pageX - pane.offset().left, y = event.pageY - pane.offset().top - handle.outerHeight(true) / 2 - ($.browser.msie && $.browser.version.match(/^8\./) ? document.documentElement.scrollTop : 0 /* IE 8 quirk */), newtop = Math.max(0, Math.min(y, pane.height() - handle.outerHeight(true)));

            handle.css('top', newtop + 'px');
            self._scrollWith(pane, handle, widget, self.subViews[0], type, {
              top: newtop,
              left: handle.position().left
            }, event);
          });
        }

        if (type === 'horizontal') {
          if ('ontouchstart' in window) {
            widget.bind('touchmove', function (event) {
              event.preventDefault();
              if (!inTouchEvent) return;

              var t = event.originalEvent.touches,
                  deltaX = t[0].clientX - startX,
                  top = startTop,
                  newleft = startLeft - deltaX * o.touchSensitivity;

              newleft = Math.max(newleft, 0);
              newleft = Math.min(pane.width() - handle.outerWidth(true) / 2 - ($.browser.msie && $.browser.version.match(/^8\./) ? document.documentElement.scrollLeft : 0), newleft);

              handle.css('left', newleft + 'px');
              self._scrollWith(pane, handle, widget, self.subViews[0], type, {
                top: top,
                left: newleft
              }, event);
            });
          }

          // create the scroll to function
          self.scrollToLeft = function () {
            handle.css('left', 0);
            self._scrollWith(pane, handle, widget, self.subViews[0], type, {
              top: handle.position().top,
              left: 0
            });
          };

          // listen to click event
          pane.disableSelection().css('-webkit-user-select', 'none').click(function (event) {
            event.preventDefault();

            var x = event.pageX - pane.offset().left - handle.outerWidth(true) / 2 - ($.browser.msie && $.browser.version.match(/^8\./) ? document.documentElement.scrollLeft : 0 /* IE 8 quirk */), y = event.pageY - pane.offset().top, newleft = Math.max(0, Math.min(x, pane.width() - handle.outerWidth(true)));

            handle.css('left', newleft + 'px');
            self._scrollWith(pane, handle, widget, self.subViews[0], type, {
              top: handle.position().top,
              left: newleft
            }, event);
          });
        }

        pane.show();
      }

      if (o.type === 'vertical') {
        _initHandle(self.innerPanes.v, self.handles.v, 'vertical');
      } else if (o.type === 'horizontal') {
        _initHandle(self.innerPanes.h, self.handles.h, 'horizontal');
      } else {
        _initHandle(self.innerPanes.v, self.handles.v, 'vertical');
        _initHandle(self.innerPanes.h, self.handles.h, 'horizontal');
      }

      widget.css('overflow', 'hidden');
    },
    refresh: function (type) {
      var self = this,
          o = this.options,
          widget = this.widget();

      if (o.nativeScrolling && this._isMobileWebkit()) {
        this._iscroll.refresh();
        this.setNeedsDisplay();
        return;
      }

      if (type === undefined) {
        type = 'both';
      }

      if ((type == 'horizontal' || type == 'both') && (o.type == 'horizontal' || o.type == 'both')) {
        if (widget.width() >= self.subViews[0].outerWidth(true)) {
          self.panes.h.hide();
        } else {
          self.panes.h.show();
        }
      }

      if ((type == 'vertical' || type == 'both') && (o.type == 'vertical' || o.type == 'both')) {
        if (widget.height() >= self.subViews[0].outerHeight(true)) {
          self.panes.v.hide();
        } else {
          self.panes.v.show();
        }
      }

    },
    _complete: function (event) {
      this._trigger('complete', event, {
        widget: this.widget(),
        subViews: this.subViews,
        panes: this.panes,
        handles: this.handles
      });
    },
    _start: function (event, type, drag) {
      this._trigger('start', event, {
        widget: this.widget(),
        subViews: this.subViews,
        panes: this.panes,
        handles: this.handles,
        type: type
      })
    },
    _stop: function (event, type, drag) {
      this._trigger('stop', event, {
        widget: this.widget(),
        subViews: this.subViews,
        panes: this.panes,
        handles: this.handles,
        type: type
      })
    },
    _scroll: function (event, type, drag) {
      this._trigger('scroll', event, {
        widget: this.widget(),
        subViews: this.subViews,
        panes: this.panes,
        handles: this.handles,
        type: type,
        drag: drag
      });
    }
  });

})(jQuery, Modernizr, document, window, require);
