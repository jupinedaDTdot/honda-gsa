;(function($, W, undefined) {

var _defaults = {
    watch: false,
    found: function(event, ui) {},
    error: function(event, ui) {},
    highAccuracy: false,
    timeout: 60000,
    maximumAge: 600000
};

$.widget('commonlib.location', {
    widgetEventPrefix: 'location',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.location.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        this.watchId = null;

        this._ensureOptions();

    },
    _ensureOptions: function() {
        var w = this.widget(),
            o = this.options;

        o.watch = w.data('location-watch') || o.watch;
        o.highAccuracy = w.data('location-high-accuracy') || o.highAccuracy;
        o.timeout = w.data('location-timeout') || o.timeout;
        o.maximumAge = w.data('location-maximum-age') || o.maximumAge;
    },
    start: function() {
        var self = this,
            o = this.options;

        if (!W.navigator.geolocation) {
            return;
        }
        if (o.watch) {
            this.watchId = W.navigator.geolocation.watchPosition(function(position) {
                self._found(position);
            }, function(error) {
                self._error(error);
            }, {
                enableHighAccuracy: o.highAccuracy,
                timeout: o.timeout,
                maximumAge: o.maximumAge
            });
        }
        else {
            W.navigator.geolocation.getCurrentPosition(function(position) {
                self._found(position);
            }, function(error) {
                self._error(error);
            }, {
                enableHighAccuracy: o.highAccuracy,
                timeout: o.timeout,
                maximumAge: o.maximumAge
            });
        }
    },
    stop: function() {
        if (this.watchId) {
            W.navigator.geolocation.clearWatch(this.watchId);
            this.watchId = null;
        }
    },
    _found: function(position) {
        this._trigger('found', null, {
            position: position
        });
    },
    _error: function(error) {
        this._trigger('error', null, {
            error: error
        });
    }
});

})(jQuery, window);