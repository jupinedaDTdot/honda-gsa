/* $Id: jquery.commonlib.collapsible.js 16173 2012-03-21 15:07:33Z felix.huang $ */
;(function($, M, D, F, require, undefined){

var _defaults = {
    defaultClass: 'commonlib-collapsible',
    hoverClass: 'commonlib-collapsible-hover',
    activeClass: 'commonlib-collapsible-active',
    targetClass: 'commonlib-collapsible-target',
    targetActiveClass: 'commonlib-collapsible-target-active',
    active: false,
    group: '__default__',
    target: '',
    effect: 'default',
    exclusive: false,
    show: function(event, ui) {},
    hide: function(event, ui) {}
};

var defaultEffect = function(event, element, action, callback) {
    if (action === 'show') {
        element.stop().show();
    }
    else if (action === 'hide') {
        element.stop().hide();
    }
    callback.call(this, event, element, action);
};

var collapsibleWidgets = {}, activeWidgets = {};

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

$.widget('commonlib.collapsible', $.commonlib.widget, {
    widgetEventPrefix: 'collapsible',
    _created: false,
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.collapsible.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        var self = this,
            o = this.options,
            widget = this.widget();


        self._ensureOptions();

        // ensure group
        if (!collapsibleWidgets[o.group]) {
            collapsibleWidgets[o.group] = [];
        }

        collapsibleWidgets[o.group].push(self);

        // ensure group
        if (!activeWidgets[o.group]) {
            activeWidgets[o.group] = [];
        }

        if (o.active) {
            activeWidgets[o.group].push(self);
        }

        self._attachEventHandlers();

    },
    _ensureOptions: function() {
        var self = this,
            o = this.options,
            widget = this.widget();

        o.group = widget.data('collapsible-group') || o.group;
        o.target = widget.data('collapsible-target') || o.target;
        o.effect = widget.data('collapsible-effect') || o.effect;
        o.active = widget.data('collapsible-active') || o.active;
        o.exclusive = widget.data('collapsible-exclusive') || o.exclusive;

        o.defaultClass = widget.data('collapsible-class') || o.defaultClass;
        o.hoverClass = widget.data('collapsible-hover-class') || o.hoverClass;
        o.activeClass = widget.data('collapsible-active-class') || o.activeClass;
        o.targetClass = widget.data('collapsible-target-class') || o.targetClass;
        o.targetActiveClass = widget.data('collapsible-target-active-class') || o.targetActiveClass;

        o.show = widget.data('collapsible-show') && F[widget.data('collapsible-show')] || o.show;
        o.hide = widget.data('collapsible-hide') && F[widget.data('collapsible-hide')] || o.hide;

        self.group = o.group;
        self.active = o.active;
    },
    _attachEventHandlers: function() {
        var self = this,
            o = this.options,
            widget = this.widget();

        // initialize target
        $(o.target).addClass(o.targetClass);
        if (o.active) {
            self._show();
        }
        else {
            self._hide(null, true);
        }

				self._createDelayed();
        
				// on touch screen devices
        if (M.touch) {
            return;
        }

        // mouse enter event to add hover class
        widget.hover(function() {
            widget.addClass(o.hoverClass);
        }, function() {
            widget.removeClass(o.hoverClass);
        });

    },
    _createDelayed: function() {
        var self = this,
            options = this.options,
            widget = this.widget();

        widget.click(function(event) {
            if (self.active) {
                self._hide(event);
            }
            else {
                self._show(event);
            }
            event.preventDefault();
        });

        this._created = true;
    },
    _defaultEffectCallback: function(event, element, action) {
        var o = this.options,
            widget = this.widget();
        if (action === 'show') {
            widget.addClass(o.activeClass);
            element.addClass(o.targetActiveClass);
            this.active = true;
        }
        else if (action === 'hide') {
            widget.removeClass(o.activeClass);
            element.removeClass(o.targetActiveClass);
            this.active = false;
        }

        this._trigger(action, event, {
            widget: widget,
            target: element
        });
    },
    _show: function(event) {
        var self = this,
            o = this.options,
            target = $(o.target);

        if (o.exclusive) {
            $.commonlib.collapsible.closeGroup(self.group, self);
        }
        if (typeof o.effect === 'function') {
            o.effect.call(this, event, target, 'show', this._defaultEffectCallback());
        }
        else if (o.effect === 'default') {
            defaultEffect.call(this, event, target, 'show', this._defaultEffectCallback);
        }

        activeWidgets[o.group].push(self);
    },
    _hide: function(event, groupClose) {
        var self = this,
            o = this.options,
            target = $(o.target);

        // we cannot close the widget if it's the only active widget in the group
        if (!groupClose && o.exclusive && activeWidgets[o.group].length <= 1) {
            return;
        }
        if (typeof o.effect === 'function') {
            o.effect.call(this, event, target, 'hide', this._defaultEffectCallback());
        }
        else if (o.effect === 'default') {
            defaultEffect.call(this, event, target, 'hide', this._defaultEffectCallback);
        }

        var index = activeWidgets[o.group].indexOf(self);
        if (index != -1) {
            activeWidgets[o.group].splice(index, 1);
        }
    },
    open: function() {
        this._show();
    },
    close: function(groupClose) {
        this._hide(null, groupClose);
    },
    destroy: function() {
        if (collapsibleWidgets[this.group]) {
            var index = collapsibleWidgets[this.group].indexOf(this);
            if (index >= 0) {
                collapsibleWidgets[this.group].splice(index, 1);
            }
        }
        $.Widget.prototype.destroy.apply(this, arguments); // default destroy
    }
});

$.commonlib.collapsible.openGroup = function(group) {
    if (!group) {
        group = _defaults.group;
    }
    if (collapsibleWidgets[group]) {

        $.each(collapsibleWidgets[group], function(index, w) {
            w.open();
        });
    }
};

$.commonlib.collapsible.closeGroup = function(group, currentWidget) {
    if (!group) {
        group = _defaults.group;
    }

    if (collapsibleWidgets[group]) {
        $.each(collapsibleWidgets[group], function(index, w) {
            w.close(true);
        });
    }
};

})(jQuery, Modernizr, document, window, require);