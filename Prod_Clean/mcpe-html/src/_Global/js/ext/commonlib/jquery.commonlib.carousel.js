/* $Id: jquery.commonlib.carousel.js 40326 2015-02-09 17:58:37Z udaya.ratnayake $ */
;(function($, D, F, require, undefined){


var _defaults = {
    vertical: false,
    keyboard: false,
    circular: false,
    items: '.items',
    clonedClass: 'cloned',
    initialIndex: 0,
    disabledClass: 'disabled',
    next: '.next',
    prev: '.prev',
    speed: 400,
    easing: 'swing',
    navi: false, // or target
    interval: false, // or interval
    seek: function() {},
    beforeSeek: function() {}
};

    
$.widget('commonlib.carousel', {
    widgetEventPrefix: 'carousel',
    options: $.extend(true, {}, _defaults, (function(){
        try {
            return F.global_get('commonlib.carousel.defaults');
        }
        catch(e) {
            return {};
        }
    })()),
    _create: function() {
        var self = this,
            o = this.options,
            widget = this.widget();

        self._ensureOptions();

        // we need scrollable to make it work
        require(['ext/tools/scrollable/scrollable'], function() {
            require(['ext/tools/scrollable/scrollable.navigator'], function() {

                widget.scrollable({
                    vertical: o.vertical,
                    keyboard: o.keyboard,
                    circular: o.circular,
                    items: o.items,
                    next: o.next,
                    prev: o.prev,
                    speed: o.speed,
                    easing: o.easing,
                    clonedClass: o.clonedClass,
                    disabledClass: o.disabledClass,
                    onSeek: o.seek,
                    onBeforeSeek: o.beforeSeek
                });

                if (o.navi) {
                    widget.navigator({
                        navi: o.navi
                    });
                }
    
                if (o.interval) {
                    require(['tools/scrollable/scrollable.autoscroll'], function() {
                        widget.autoscroll({
                            interval: o.interval
                        });
                    });
                }
            });
        }); // of require

    },
    _ensureOptions: function() {
        var widget = this.widget(),
            o = this.options;

        o.vertical = widget.data('carousel-vertical') || o.vertical;
        o.keyboard = widget.data('carousel-keyboard') || o.keyboard;
        o.circular = widget.data('carousel-circular') || o.circular;
        o.items = widget.data('carousel-items') || o.items;
        o.next = widget.data('carousel-next') || o.next;
        o.prev = widget.data('carousel-prev') || o.prev;
        o.speed = widget.data('carousel-speed') || o.speed;
        o.easing = widget.data('carousel-easing') || o.easing;
        o.clonedClass = widget.data('carousel-cloned-class') || o.clonedClass;
        o.disabledClass = widget.data('carousel-disabled-class') || o.disabledClass;
        o.seek = widget.data('carousel-seek') ? F[widget.data('carousel-seek')] : o.seek;
        o.beforeSeek = widget.data('carousel-before-seek') ? F[widget.data('carousel-before-seek')] : o.beforeSeek;
        o.navi = widget.data('carousel-navi') || o.navi;
        o.interval = widget.data('carousel-interval') || o.interval;
    },
    play: function () {
    	var widget = this.widget(), api = widget.data('scrollable');
    	console.log(api.play);
    	if (api && typeof api.play === 'function') {
    		api.play();
    	}
    },
    stop: function () {
    	var widget = this.widget(), api = widget.data('scrollable');
    	if (api && typeof api.stop === 'function') {
    		api.stop();
    	}
    },
    pause: function () {
    	var widget = this.widget(), api = widget.data('scrollable');
    	if (api && typeof api.pause === 'function') {
    		api.pause();
    	}
    }
});

})(jQuery, document, window, require);