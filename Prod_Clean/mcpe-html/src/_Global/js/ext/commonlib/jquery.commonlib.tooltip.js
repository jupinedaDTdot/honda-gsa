/* $Id: jquery.commonlib.tooltip.js 15528 2011-08-04 14:49:21Z yuxhuang $ */
/**
 * Usage:
 *
 * <TAG property="act:tooltip"
 *    data-tooltip-content="a simple message without tags."
 *    data-tooltip-content-target="jQuery selector for the content target"
 *    data-tooltip-position-my="left bottom"
 *    data-tooltip-position-at="left top"
 *    data-tooltip-position-offset="0 0">Tooltip trigger widget</TAG>
 */
;(function($, M, W, D, F, require, undefined){

var _defaults = {
    additionalClass: '', // additional classes added to the container
    sibling: false,
    handler: 'rollover', // rollover, custom, touch
    multiple: false, // if set true, allow multiple instances
    rolloverObject: null, // should be a function
    contentTarget: null,
    show: function(event, ui) {
        ui.tooltip.show();
    },
    hide: function(event, ui) {
        ui.tooltip.hide();
    },
    complete: function(event, ui) {}
};

require(['ui/jquery.ui.position'], function() {

var tooltipWidgets = [];

$.widget('commonlib.tooltip', $.commonlib.widget, {
    widgetEventPrefix: 'tooltip',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.tooltip.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        var self = this, o = this.options;

        self.tooltipContainer = null;
        
        o.position.collision = 'none';

        self._ensurePosition();

        self._ensureRolloverObject();
        self._discoverHandler();
        self._attachEventHandlers();

        tooltipWidgets.push(self);
    },
    _ensureRolloverObject: function() {
        var self = this,
            o = this.options,
            widget = this.widget();
        
        self.rolloverObject = typeof o.rolloverObject === 'function' ? o.rolloverObject.apply(self) : (o.rolloverObject || widget);
    },
    _ensureMultipleState: function() {
        var self = this,
            o = this.options;

        if (!o.multiple) {
            return;
        }

        $.each(tooltipWidgets, function(index, tip) {
            if (tip !== self) {
                tip.hide();
            }
        });
    },
    _createDelayed: function() {
        var self = this,
            o = this.options,
            widget = this.widget(),
            contentSelector = widget.data('tooltip-content-target') || o.contentTarget,
            cssClass = widget.data('tooltip-class') || '',
            content = contentSelector ?
                    $(contentSelector).html() :
                    widget.data('tooltip-content'),
            container = null;

        container = $('<div>').addClass('tooltip_container').addClass(o.additionalClass).addClass(cssClass)
            .append(
                $('<div>').addClass('tooltip_arrow pf')
            )
            .append(
                $('<div>').addClass('tooltip_content pf').append(
                    $('<div>').addClass('tooltip_content_top pf')
                ).append(
                    $('<div>').addClass('tooltip_content_mid pf').append(
                        content
                    )
                ).append(
                    $('<div>').addClass('tooltip_content_bottom pf')
                )
            ).hide();

        self.tooltipContainer = container;

        if (o.sibling) {
            if (self.rolloverObject.is('tr')) {
                widget.after(container);
            }
            else {
                self.rolloverObject.append(container);
            }
        }
        else {
            container.appendTo('body');
        }

        // callback
        self._complete(null);
    },
    _discoverHandler: function() {
    },
    _attachEventHandlers: function() {
        var self = this,
            o = this.options,
            method = '_attach' + o.handler.toTitleCase();

        self[method]();
    },
    _attachRollover: function() {
        var self = this,
            o = this.options,
            widget = this.widget();

        self.rolloverObject.one('mouseover', function(event) {
            self.show(event);
        });
    },
    _attachRolloverDelayed: function() {
        var self = this,
            o = this.options,
            widget = this.widget();

        self.rolloverObject.bind('mouseover', function(event) {
            self.show(event);
        });

        self.rolloverObject.bind('mouseout', function(event) {
            self.hide(event);
        });
    },
    _attachTouch: function() {
        var self = this,
            widget = this.widget();

        self.rolloverObject.one('touchstart', function(event) {
            self.show(event);
        });
    },
    _attachTouchDelayed: function() {
        var self = this,
            widget = this.widget(),
            frame = this.tooltipContainer;

        self.rolloverObject.bind('touchstart', function(event) {
            if (frame.is(':hidden')) {
                self.show(event);
            }
            else {
                self.hide(event);
            }
            event.stopPropagation();
            event.preventDefault();
        });
    },
    _rePosition: function() {
        var self = this,
            o = this.options,
            frame = this.tooltipContainer;

        frame.position(o.position);
    },
    destroy: function() {
        if (this.tooltipContainer) {
            this.tooltipContainer.remove();
        }
        if (tooltipWidgets) {
            var index = tooltipWidgets.indexOf(this);
            tooltipWidgets.splice(index, 1);
        }
        $.Widget.prototype.destroy.apply(this, arguments); // default destroy
    },
    show: function(event) {
        var self = this,
            o = this.options,
            widget = this.widget();


        if (self.tooltipContainer === null) {
            self._createDelayed();
        }

        self._ensureMultipleState();


        self._trigger('show', event, {
            widget: widget,
            tooltip: self.tooltipContainer,
            call_Notify: function() {
                self._rePosition();
            }
        });
        self._rePosition();                 // re-position guards to be used twice for browser positioning quirks
        self._rePosition();

    },
    hide: function(event) {
        if (this.tooltipContainer === null) {
            return;
        }
        this._trigger('hide', event, {
            widget: this.widget(),
            tooltip: this.tooltipContainer
        });
    },
    _complete: function(event) {
        var self = this,
            o = this.options,
            method = '_attach' + o.handler.toTitleCase() + 'Delayed';

        if (self[method]) {
            self[method]();
        }

        self._trigger('complete', event, {
            widget: self.widget(),
            frame: self.tooltipContainer
        });
    }
});

});

})(jQuery, Modernizr, window, document, window, require);