/* $Id: jquery.commonlib.popup.js 32588 2014-05-23 22:24:28Z nikhil.patel $ */
;(function($, M, D, F, require, undefined) {

var _defaults = {
    immediate: false,
    defaultClass: 'commonlib-popup',
    modal: false,
    overlay: '#000',
    opacity: 0.8,
    contentTarget: false,
    contentUrl: false,
    contentSelector: false,
    hanger: false,
    handler: 'click',
    fixed: true,
    closeOnClick: true,

    show: function(event, ui) {},
    hide: function(event, ui) {},
    complete: function(event, ui) {}
};

$.widget('commonlib.popup', $.commonlib.widget, {
    widgetEventPrefix: 'popup',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.popup.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        var self = this,
            o = this.options,
            widget = this.widget();

        if ($['jobqueue']) {
            this._queue = new $.jobqueue();
        }
        else {
            this._queue = null;
        }

        this._completed = false;

        self._ensureOptions();
        self._ensurePosition();
        if (o.modal) {
            if (o.contentTarget && $(o.contentTarget).size() > 0) {
                // calculate top
                var top = $.browser.msie ? 50 : '10%';
                require(['tools/toolbox/toolbox.expose', 'tools/overlay/overlay'], function() {
                    widget.overlay({
                        fixed: o.fixed,
                        top: top,
                        mask: {
                            color: o.overlay,
                            opacity: o.opacity
                        },
                        closeOnClick: o.closeOnClick,
                        target: o.contentTarget,
                        onBeforeLoad: function() {
                            var api = widget.data('overlay');
                            api.getOverlay().addClass('commonlib-popup').addClass(o.defaultClass);
                            if (o.contentUrl) {
                                self._beforeAjaxLoad(null, {
                                    frame: api.getOverlay()
                                });
                                $.ajax({
                                    url: o.contentUrl,
                                    success: function(data, status, xhr) {
                                        self._complete(null, {
                                            frame: api.getOverlay(),
                                            call_Notify: function() {}
                                        });

                                        if (o.contentSelector) {
                                            api.getOverlay().html($(o.contentSelector, data).html());
                                        }
                                        else {
                                            api.getOverlay().html(data);
                                        }
                                        self._show(null, {
                                            frame: api.getOverlay(),
                                            call_Notify: function() {}
                                        });
                                    }
                                })
                            }
                            else {
                                self._show(null, {
                                    frame: api.getOverlay(),
                                    call_Notify: function() {}
                                });
                            }

                        },
                        onLoad: function() {
                            var api = widget.data('overlay');
                            self._didshow(null, {
                                frame: api.getOverlay(),
                                call_Notify: function() {}
                            });
                        },
                        onBeforeClose: function() {
                            var api = widget.data('overlay');
                            self._hide(null, {
                                frame: api.getOverlay(),
                                call_Notify: function() {}
                            });
                        },
                        onClose: function() {
                            var api = widget.data('overlay');
                            self._didclose(null, {
                                frame: api.getOverlay(),
                                call_Notify: function() {}
                            });
                        }
                    });

                    if (!o.contentUrl) {
                        var api = widget.data('overlay');
                        self._complete(null, {
                            frame: api.getOverlay()
                            , call_Notify: function() {}
                        });
                    }
                });
            }
        }
        else {
            require(['commonlib/jquery.commonlib.dropdown'], function() {

                widget.dropdown({
                    additionalClass: o.defaultClass + ' commonlib-popup',
                    contentTarget: o.contentTarget,
                    position: o.position,
                    immediate: o.immediate,
                    handler: o.handler,
                    complete: function(event, ui) {
                        if (!o.hanger) {
                            $('.dropdown_button', ui.frame).remove();
                        }
                        if (!o.contentUrl) {
                            self._complete(event, ui);
                        }
                        if (o.contentUrl) {
                            self._beforeAjaxLoad(null, ui);
                            $.ajax({
                                url: o.contentUrl,
                                global: false,
                                success: function(data, status, xhr) {
                                    if (o.contentSelector) {
                                        $('.dropdown_content_mid', ui.frame).html($(o.contentSelector, data).html());
                                    }
                                    else {
                                        $('.dropdown_content_mid', ui.frame).html(data);
                                    }
                                    self._complete(event, ui);
                                    ui.call_Notify();
                                }
                            });
                        }
                    }
                }).bind('dropdownshow', function(event, ui) {
                    self._show(event, ui);
                }).bind('dropdownhide', function(event, ui) {
                    self._hide(event, ui);
                });

            });
        }

    },
    _ensureOptions: function() {
        var o = this.options,
            widget = this.widget();

        o.immediate = widget.data('popup-immediate') || o.immediate;
        o.defaultClass = widget.data('popup-class') || o.defaultClass;
        o.contentTarget = widget.data('popup-content-target') || o.contentTarget || $("<div>").appendTo('body');
        o.contentUrl = widget.data('popup-content-url') || o.contentUrl;

        o.modal = widget.data('popup-modal') || o.modal;
        o.fixed = widget.data('popup-fixed') === undefined ? o.fixed : widget.data('popup-fixed');
        o.overlay = widget.data('popup-overlay') || o.overlay;
        o.opacity = widget.data('popup-opacity') || o.opacity;
        o.hanger = widget.data('popup-hanger') || o.hanger;
        o.handler = widget.data('popup-handler') || o.handler;
        o.contentSelector = widget.data('popup-content-selector') || o.contentSelector;
    },
    close: function() {
        var w = this.widget();
        if (w.data('dropdown')) {
            w.dropdown('hide');
        }
        if (w.data('overlay')) {
            w.data('overlay').close();
        }
    },
    updateFrame: function(callback) {
        callback.apply(this, [this.frame()]);
    },
    frame: function() {
        var w = this.widget(),
            o = this.options;
        if (o.modal) {
            return w.data('overlay').getOverlay();
        }
        else {
            return w.data('dropdown').frameContainer;
        }
    },
    isCompleted: function() {
        return this._completed;
    },
    show: function() {
        this.widget().dropdown('show');
    },
    hide: function() {
        this.widget().dropdown('hide');
    },
    _show: function(event, ui) {
        ui.queue = this._queue;
        this._trigger('show', event, ui);
    },
    _didclose: function(event, ui) {
        ui.queue = this._queue;
        this._trigger('didclose', event, ui);
    },
    _didshow: function(event, ui) {
        ui.queue = this._queue;
        this._trigger('didshow', event, ui);
    },
    _hide: function(event, ui) {
        ui.queue = this._queue;
        this._trigger('hide', event, ui);
    },
    _beforeAjaxLoad: function(event, ui) {
        ui.queue = this._queue;
        this._trigger('beforeajaxload', event, ui);
    },
    _complete: function(event, ui) {
        ui.queue = this._queue;
        this._completed = true;
        this._trigger('complete', event, ui);
    }
});

})(jQuery, Modernizr, document, window, require);