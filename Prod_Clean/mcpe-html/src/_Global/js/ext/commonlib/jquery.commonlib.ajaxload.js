/* $Id: jquery.commonlib.ajaxload.js 15528 2011-08-04 14:49:21Z yuxhuang $ */
;(function($, M, W, D, F, require, undefined){

var _defaults = {
    target: '',
    activeClass: 'active',
    loaderContainerClass: 'loader_container',
    loaderClass: 'bg_loader loader_default',
    contentUrl: '',
    contentTarget: '',
    updateLocation: true,
    loadOnce: false,
    scroll: false,
    group: '__default__',
    cache: true,
    show: function(target) {
        target.fadeIn('slow');
    },
    hide: function(target) {
        target.hide();
    }
};

var groupLoadingStates = {}, ajaxloadWidgets = {}, activeWidgets = {}, historyStates = {};
    
$.widget('commonlib.ajaxload', {
    widgetEventPrefix: 'ajaxload',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.ajaxload.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        var self = this,
            o = this.options;

        self._ensureOptions();
        self._attachEventHandlers();

        // ensure group state is initialized
        if (typeof groupLoadingStates[o.group] === 'undefined') {
            groupLoadingStates[o.group] = false;
        }

        // widget collection
        if (typeof ajaxloadWidgets[o.group] === 'undefined') {
            ajaxloadWidgets[o.group] = [];
        }
        ajaxloadWidgets[o.group].push(self);
        
        // active widgets
        activeWidgets[o.group] = self.widget().hasClass(o.activeClass) ? self : activeWidgets[o.group];

        var url = D.location.protocol + '//' + D.location.host + '/' + o.contentUrl;
        historyStates[url] = self;
    },
    _beforeLoad: function(event) {
        var self = this;
        self._trigger('beforeload', event, {
            widget: self.widget(),
            target: self.options.target
        });
    },
    load: function(event) {
        var self = this,
            o = this.options;

        // if something is loading, just simply reject it
        if (groupLoadingStates[o.group]) {
            return false;
        }

        // now we are loading the content
        groupLoadingStates[o.group] = true;

        if (o.scroll) {
            self.scroll(); // scroll to the area
        }

        var // loader = self._attachLoader(),
            targetPage,
            targetContainer = $(o.target),
            previousActiveWidget = activeWidgets[o.group];

        o.hide(targetContainer);
        var oldContent = targetContainer.html();

        targetContainer.empty();

        self._beforeLoad(null);

        $.ajax({
            global: false,
            type: 'GET',
            url: o.contentUrl,
            dataType: 'html',
            cache: o.cache,
            success: function(data, status, xhr) {
                try {
                    // replace the content in the target container with new content

                    targetPage = $(o.contentTarget, data).clone().hide();
                    targetContainer.html(targetPage.html());

                    // add the display event into the job queue
                    $.jobqueue.add(function() {
//                        self._detachLoader(loader);

                        activeWidgets[o.group] = self;
                        self._updateActiveStates();

                        self._load(event, data); // trigger load event

                        o.show(targetContainer);
                    }, targetContainer);

                }
                finally {
                    groupLoadingStates[o.group] = false; // unlock the group loading state
                    if (o.updateLocation) {
                        self._updateLocation();
                    }
                }
            },
            error: function(xhr, err) {
                try {
                    // show content back
                    targetContainer.html(oldContent);
                    
                    // add the display event into the job queue
                    $.jobqueue.add(function() {
//                        self._detachLoader(loader);


                        activeWidgets[o.group] = previousActiveWidget;
                        self._updateActiveStates();

                        self._error(); // trigger error event

                        o.show(targetContainer);
                    }, targetContainer);

                }
                finally {
                    groupLoadingStates[o.group] = false; // unlock the group loading state
                }
            }
        });
    },
    scroll: function() {
        self._scroll();
    },
    _updateActiveStates: function() {
        var o = this.options;
        // update active widget in a group
        $.each(ajaxloadWidgets[o.group], function(index, element) {
            if (activeWidgets[o.group] === element) {
                element._activate();
            }
            else {
                element._deactivate();
            }
        });
    },
    _activate: function() {
        var o = this.options;
        this.widget().addClass(o.activeClass);
    },
    _deactivate: function () {
        var o = this.options;
        this.widget().removeClass(o.activeClass);
    },
    _attachLoader: function() {
        var self = this,
            o = this.options;

        var loader = $('<div>').addClass(o.loaderContainerClass).append(
				$('<span>').addClass(o.loaderClass)
			).appendTo(o.target);

        return loader;
    },
    _detachLoader: function(loader) {
        if (loader) {
            loader.detach().remove();
        }
    },
    _scroll: function() {
    },
    _load: function(event, content) {
        var o = this.options;
        
        this._trigger('load', event, {
            widget: this.widget(),
            content: content,
            target: o.target
        });
    },
    _error: function() {
        this._trigger('error', null, {
            widget: this.widget()
        });
    },
    _show: function() {

    },
    _updateLocation: function(event) {
        var self = this, o = this.options;

        if (M.history) {
            W.history.replaceState({}, "", o.contentUrl);
        }
        else {
            W.location.hash = '#' + o.contentUrl;
        }
    },
    _ensureOptions: function() {
        var o = this.options,
            widget = this.widget();

        o.target = widget.attr('target') || o.target;
        o.activeClass = widget.data('ajaxload-active-class') || o.activeClass;
        o.loaderContainerClass = widget.data('ajaxload-loader-container-class') || o.loaderContainerClass;
        o.loaderClass = widget.data('ajaxload-loader-class') || o.loaderClass;
        o.contentUrl = widget.data('ajaxload-content-url') || o.contentUrl;
        o.contentTarget = widget.data('ajaxload-content-target') || o.contentTarget;
        o.updateLocation = widget.data('ajaxload-update-location') || o.updateLocation;
        o.loadOnce = widget.data('ajaxload-load-once') || o.loadOnce;
        o.scroll = widget.data('ajaxload-scroll') || o.scroll;
        o.group = widget.data('ajaxload-group') || o.group;
        o.cache = widget.data('ajaxload-cache') || o.cache;
    },
    _attachEventHandlers: function() {
        var self = this,
            widget = this.widget();

        widget.click(function(event) {
            event.preventDefault();
            self._click(event);
        });
    },
    _click: function(event) {
        this.load(event);
    },
    destroy: function() {
        var o = this.options;

        if (ajaxloadWidgets[o.group]) {
            var index = ajaxloadWidgets[o.group].indexOf(this);

            if (index >= 0) {
                ajaxloadWidgets[o.group].splice(index, 1);
            }
        }

        $.Widget.prototype.destroy.apply(this, arguments); // default destroy
    }
});

W.onpopstate = function(event) {
    if (D.location.href in historyStates) {
        historyStates[D.location.href].load();
    }
};
    
})(jQuery, Modernizr, window, document, window, require);