﻿(function () {

	$('#print_instructions_button').click(function (ctx) {
		var printContainer = $(this).attr('rel');
		var extraClass = $(this).data('extraClass') || 'bluetooth';
		var d = $("#PrintIFrame")[0].contentWindow.document;
		var content = $('#' + printContainer).html();
		d.open(); d.close();
		$('body', d).addClass(extraClass);
		$('head', d).append('<link rel=\"Stylesheet\" type=\"text/css\" media=\"print\" charset=\"utf-8\" href=\"/_Global/css/sections/bluetooth_instructions_print.css\" /></head>');
		$('body', d).append(content);

		window.frames['PrintIFrameName'].focus();
		window.frames['PrintIFrameName'].print();
		delete window.frames['PrintIFrameName'];
	});

	var PairingInstructions = {
		init: function () {
			var modelId = '',
				modelYearId = '',
				trimId = '',
				phoneId = '';

			var winURL = window.location.href;
			var array = winURL.split("/");
			phoneId = array.pop();
			trimId = array.pop();
			modelYearId = array.pop();
			modelId = array.pop();

			PairingInstructions._getInstructions(modelId, modelYearId, trimId, phoneId);
		},
		_getInstructions: function (modelId, modelYearId, trimId, phoneId) {
			$('.pairing-instructions-content').append($('<div>').addClass('loader_container').css({ 'text-align': 'center', 'margin-top': '30px' }).append(
				  $('<span>').addClass('bg_loader loader_default').width(32).height(32).css({ margin: 'auto', display: 'inline-block' })
			  ));
			var url = '/api/Bluetooth/pairing/{brandId}/{modelId}/{modelYearId}/{trimId}/{phoneId}/instructions/{language}';
			var apiUrl = url.replace('{brandId}', window._globals['brand']).replace('{language}', window._globals['lang']).replace('{modelId}', modelId).replace('{modelYearId}', modelYearId).replace('{trimId}', trimId).replace('{phoneId}', phoneId);
			$.ajax({
				type: "GET",
				url: apiUrl,
				dataType: "json",
				timeout: 60000,
				success: function (data) {
					if (data) {
						//console.log(data);
						var phoneName = data.ManufacturerName + ' ' + data.PhoneName
						$('#tag-phone-name').text(phoneName);

						PairingInstructions._setImage(window.__PhoneImagePath + data.PhoneImageName, phoneName);

						$('#tag-first-time-p').hide();
						$('#tag-subsequent-p').hide();
						$('#tag-first-time-pairing-on-vehicle').hide();
						$('#tag-first-time-pairing-on-the-phone').hide();
						$('#tag-subsequent-pairing-on-vehicle').hide();
						$('#tag-subsequent-pairing-on-the-phone').hide();

						//pairings
						$.each(data.Pairings, function (i, val) {
							PairingInstructions._getPairingInstructions(val);
						});
					}
					$('.loader_container').remove();
				},
				error: function () {
					$('.loader_container').remove();
				}
			});
		},
		_getPairingInstructions: function (data) {
			var element;
			var string = '';
			var $div = $('<div>');
			//__OnThePhone
			if (data.PairingTypeValue == 'first-time' || data.PairingTypeValue == null) {
				if (data.PairingOnTypeValue == 'on-vehicle' || data.PairingOnTypeValue == null) {
					$('#tag-first-time-p').show();
					$('#tag-first-time-pairing-on-vehicle').show();
					element = $('#tag-first-time-pairing-on-vehicle');
				} else {
					$('#tag-first-time-pairing-on-the-phone').show();
					element = $('#tag-first-time-pairing-on-the-phone');
				}

			} else { //subsequent
				$('#tag-subsequent-p').show();
				if (data.PairingOnTypeValue == 'on-vehicle' || data.PairingOnTypeValue == null) {
					$('#tag-subsequent-pairing-on-vehicle').show();
					element = $('#tag-subsequent-pairing-on-vehicle');
				} else {
					$('#tag-subsequent-pairing-on-the-phone').show();
					element = $('#tag-subsequent-pairing-on-the-phone');
				}
			}

			if (data.PairingOnTypeValue == 'on-vehicle' || data.PairingOnTypeValue == null) {
				$div.append(PairingInstructions._getPairingItems(data));
			} else { // on-the-phone
				//if (data.Language == 'en') $div.append($('<p>').text(window.__OnThePhone));
				//$div.append(PairingInstructions._getPairingItems(data));
				$div.append($('<p>').html(window.__OnThePhone)).append(PairingInstructions._getPairingItems(data));
			}

			element.append($div);
		},
		_getPairingInstructionsOld: function (data) {
			var element;
			var string = '';
			var $div = $('<div>');
			//__OnThePhone
			if (data.PairingTypeValue == 'first-time' || data.PairingTypeValue == null) {
				$('#tag-first-time-pairing').show();
				if (data.PairingTypeValue == 'first-time') {
					$('#tag-first-time-p').show();

				}
				element = $('#tag-first-time-pairing');
			} else { //subsequent
				$('#tag-subsequent-p').show();
				element = $('#tag-subsequent-pairing');
				$('#tag-subsequent-pairing').show();
			}

			if (data.PairingOnTypeValue == 'on-vehicle' || data.PairingOnTypeValue == null) {
				$div.append(PairingInstructions._getPairingItems(data));
			} else { // on-the-phone
				//if (data.Language == 'en') $div.append($('<p>').text(window.__OnThePhone));
				//$div.append(PairingInstructions._getPairingItems(data));
				$div.append($('<p>').html(window.__OnThePhone)).append(PairingInstructions._getPairingItems(data));
			}

			element.append($div);
		},
		_getPairingItems: function (data) {
			var startIndex = data.Items[0] ? data.Items[0].Order : 1;
			var olClass = window._globals['brand'] == 'honda' ? 'pairing_instructions' : 'setup_phone_details';

			var $ol = $('<ol start=' + startIndex + '>').addClass(olClass);

			console.log(data);
			$.each(data.Items, function (i, val) {
				$ol.append($('<li>').append($('<span>').html(val.Instruction)));
			});
			return $ol;
		},
		_setImage: function (url, alt) {
			var img = new Image(),
				defaultImgUrl = window.__GenericPhoneImagePath;
			$('#tag-phone-image').html('');

			img.onload = function () {
				// code to set the src on success
				$('#tag-phone-image').append($('<img>').attr({
					alt: alt,
					src: url + '?crop=(0,0,0,405)&width=285&height=211'
				}));
			};
			img.onerror = function () {
				// doesn't exist or error loading
				$('#tag-phone-image').append($('<img>').attr({
					alt: alt,
					src: defaultImgUrl + '?crop=(0,0,0,405)&width=285&height=211'
				}));
			};

			img.src = url;
		}
	};

	window.onload = PairingInstructions.init;

})();
