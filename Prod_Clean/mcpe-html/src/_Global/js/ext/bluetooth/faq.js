﻿var optionSetObject;
var faqDispayObject;
window.page_init = pageInitHandler;
window.onunload = windowUnloadHandler;

function pageInitHandler() {
    var faqOptionsDiv = $(".vehicleOptionsDiv").get();
    var faqList = $(".faqList").get();
    var faqSelectedOptionsDiv = $(".faqSelectedOptionsDiv").get();
    
    // Instantiate the phone-selector object
    var language = global_get('lang');
    var brand = global_get('brand');
    var timeoutInMilliSecond = 15000;
    optionSetObject = new Climax.Bluetooth.VehiclePhoneSelector(faqOptionsDiv, brand, language, timeoutInMilliSecond);
    faqDispayObject = new Climax.Bluetooth.FaqDisplay(faqList, faqSelectedOptionsDiv, timeoutInMilliSecond);
    
    // Bind the click event handler for any load-instruction element available now or in future.
    $(".getFaq").bind("click", displayFaq);
    
    // Bind collapse/expand behavior to the question-answer pairs
    $('.expandable_list .expandable_item_handle').on('click', collapseExpandHandler);
}

function windowUnloadHandler() {
    // Unbind event handlers
    $(".getFaq").unbind("click", displayFaq);
    $('.expandable_list .expandable_item_handle').off('click', collapseExpandHandler);

    // Dispose objects
    if (optionSetObject) optionSetObject.dispose();
}

function collapseExpandHandler() {
    var $this = $(this);
    if ($this.hasClass('collapsed')) {
        $this.removeClass('collapsed')
            .addClass('expanded')
            .nextAll(".expandable_item_content:not(.hidden)").first().slideDown();

    } else {
        $this.removeClass('expanded').
            addClass('collapsed').
            nextAll(".expandable_item_content:not(.hidden)").first().slideUp();
    }
}

function displayFaq() {
    var language = global_get('lang');
    var brand = global_get('brand');
    var vehicleModelId = $("#vehicleModelSelector").val();
    var vehicleModelText = $("#vehicleModelSelector option:selected").text();
    var vehicleYearId = $("#vehicleYearSelector").val();
    var vehicleYearText = $("#vehicleYearSelector option:selected").text();
    var vehicleTrimId = $("#vehicleTrimSelector").val();
    var vehicleTrimText = $("#vehicleTrimSelector option:selected").text();

    var options = {
        brand: brand,
        language: language,
        vehicleModelId: vehicleModelId,
        vehicleModelText: vehicleModelText,
        vehicleYearId: vehicleYearId,
        vehicleYearText: vehicleYearText,
        vehicleTrimId: vehicleTrimId,
        vehicleTrimText: vehicleTrimText
    };

    // Display proper QAs
    faqDispayObject.getFaq(options);

    // Hide the option section
    var optionsDivQuery = $(".vehicleOptionsDiv");
    optionsDivQuery.hide();

    // Show the FAQ section
    var resultDivQuery = $(".faqResultDiv");
    resultDivQuery.show();

    return false;
}