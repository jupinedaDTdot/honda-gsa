﻿/// <reference path="~/_Global/js/jquery.js"/>
/// <reference path="~/_Global/js/knockout-3.0.0.js"/>
/// <reference path="~/_Global/js/cufon-init.js"/>

var Climax;
(function(climax) {
    (function(bluetooth) {
        //Constructor
        var phoneFeatures = function(table, paginationDiv, selectedOptionsDiv, options, aggregatedCallHistoryFeatureTitle, timeoutInMilliSecond){ 
            // Create instance fields
            this._apiRootUrl = "/api/Bluetooth";
            this._tableQuery = $(table);
            this._paginationQuery = $(paginationDiv);
            this._selectedOptionsQuery = $(selectedOptionsDiv);
            this._aggregatedCallHistoryFeatureTitle = aggregatedCallHistoryFeatureTitle;
            this._tableHead = this._tableQuery.find("thead");
            this._tableBody = this._tableQuery.find("tbody");
            this._phoneTitleHeaderRow = this._tableQuery.find(".phoneTitleHeaderRow", "thead");
            this._phoneCompatibilityHeaderRow = this._tableQuery.find(".phoneCompatibilityHeaderRow", "thead");
            this._timeoutInMilliSecond = timeoutInMilliSecond;

            // Create view-models
            this._paginationModel = ko.observableArray();
            this._headerModel = ko.observableArray();
            this._tableModel = ko.observableArray();
            this._selectedOptions = {
                brand: options.brand,
                language: options.language,
                vehicleModelId: options.vehicleModelId,
                vehicleModelText: options.vehicleModelText,
                vehicleYearId: options.vehicleYearId,
                vehicleYearText: options.vehicleYearText,
                vehicleTrimId: options.vehicleTrimId,
                vehicleTrimText: options.vehicleTrimText,
                phoneManufacturerId: options.phoneManufacturerId,
                phoneManufacturerText: options.phoneManufacturerText
            };

            // Bind the table sections to their view-models
            ko.applyBindings(this._paginationModel, this._paginationQuery[0]);
            ko.applyBindings(this._headerModel, this._phoneTitleHeaderRow[0]);
            ko.applyBindings(this._headerModel, this._phoneCompatibilityHeaderRow[0]);
            ko.applyBindings(this._tableModel, this._tableBody[0]);
            var thisObj = this;
            $.each(this._selectedOptionsQuery, function(index, view) {
                ko.applyBindings(thisObj._selectedOptions, $(view)[0]);
            });
            
        };

        // Disposer
        phoneFeatures.prototype.dispose = function() {
            // Clear instance fields
            this._apiRootUrl = null;
            this._tableQuery = null;
            this._paginationQuery = null;
            this._selectedOptionsQuery = null;
            this._aggregatedCallHistoryFeatureTitle = null;
            this._tableHead = null;
            this._tableBody = null;
            this._phoneTitleHeaderRow = null;
            this._phoneCompatibilityHeaderRow = null;
            this._timeoutInMilliSecond = null;
            this._paginationModel = null;
            this._headerModel = null;
            this._tableModel = null;
            this._selectedOptions = null;
        };

        phoneFeatures.prototype.bindTableDataAndPagination = function(pageIndex, pageSize) {
            var featuresDataUrlTemplate = "{apiRootUrl}/phone/{brandId}/{modelId}/{modelYearId}/{trimId}/{manufacturerId}/phonefeatures/{language}/{pageIndex}/{pageSize}";
            var featuresDataUrl = featuresDataUrlTemplate
                .replace("{apiRootUrl}", this._apiRootUrl)
                .replace("{brandId}", this._selectedOptions.brand)
                .replace("{modelId}", this._selectedOptions.vehicleModelId)
                .replace("{modelYearId}", this._selectedOptions.vehicleYearId)
                .replace("{trimId}", this._selectedOptions.vehicleTrimId)
                .replace("{manufacturerId}", this._selectedOptions.phoneManufacturerId)
                .replace("{language}", this._selectedOptions.language)
                .replace("{pageIndex}", pageIndex)
                .replace("{pageSize}", pageSize);

            var thisObj = this;
            $.ajax({
                type: "GET",
                url: featuresDataUrl,
                timeout: thisObj._timeoutInMilliSecond,
                dataType: "json",
                success: function(data) {
                    // Bind pagination
                    thisObj._paginationModel(phoneFeatures._preparePaginationModel(data.TotalCount, pageSize, pageIndex));
                    thisObj._headerModel(phoneFeatures._prepareHeaderModel(data.List,
                        thisObj._selectedOptions.vehicleModelId,
                        thisObj._selectedOptions.vehicleYearId,
                        thisObj._selectedOptions.vehicleTrimId,
                        thisObj._selectedOptions.phoneManufacturerText));

                    // Bind features data
                    var compelteDataUrlTemplate = "{apiRootUrl}/phone/{brandId}/features/{language}";
                    var compelteDataUrl = compelteDataUrlTemplate
                        .replace("{apiRootUrl}", thisObj._apiRootUrl)
                        .replace("{brandId}", thisObj._selectedOptions.brand)
                        .replace("{language}", thisObj._selectedOptions.language);
                    $.ajax({
                        type: "GET",
                        url: compelteDataUrl,
                        dataType: "json",
                        success: function(completeData) {
                            var result = phoneFeatures._prepareTableModel(data, completeData, thisObj._aggregatedCallHistoryFeatureTitle);
                            thisObj._tableModel(result);
                        }
                    });
                }
            });
        };

        phoneFeatures._prepareHeaderModel = function(data, brand, modelYearId, trimId, phoneManufacturer) {
            return $.map(data, function(item) {
                var urlTemplate = global_get('bluetooth.pairing.instructions');
                var url = (urlTemplate)
                    .replace('{modelId}', brand)
                    .replace('{modelYearId}', modelYearId)
                    .replace('{trimId}', trimId)
                    .replace('{phoneId}', item.Id);
                return $.extend(item, {
                    instructionUrl: url,
                    fullPhoneName: phoneManufacturer + " " + item.DisplayName,
                    compatible: item.Compatible
                });
            });
        };
        
        phoneFeatures._preparePaginationModel = function(count, pageSize, selectedPageIndex) {
            var result = new Array();
            var remainingItemsCount = count;
            var currentFrom = 1;
            var currentPageIndex = 0;
            while (remainingItemsCount > 0) {
                result.push({
                    from: currentFrom,
                    to: Math.min(currentFrom + pageSize - 1, count),
                    pageIndex: currentPageIndex,
                    isSelected: currentPageIndex == selectedPageIndex
                });

                remainingItemsCount = remainingItemsCount - pageSize;
                currentFrom = currentFrom + pageSize;
                currentPageIndex++;
            }
            return result;
        };
        
        phoneFeatures._prepareTableModel = function(list, categories, aggregatedFeatureName) {
            // Deep copy categories array into the result
            var result = $.extend(true, new Array(), categories);
            $.each(list.List, function(idx, phone) {
                phone.feature_statuses = { };
                $.each(phone.FeatureGroups, function(j, featureGroup) {
                    $.each(featureGroup.Features, function(k, feature) {
                        phone.feature_statuses[feature.Id] = feature.Status;
                    });
                });
            });
            $.each(result, function(index, item) {
                if (!item.Features) return;

                $.each(item.Features, function(featureIndex, feature) {
                    feature.phone_statuses = $.map(list.List, function(phone) {
                        return phoneFeatures.toPhoneStatusWithDefault(phone.feature_statuses[feature.Id]);
                    });
                });
            });
            
            // Trim empty data
            phoneFeatures._TrimEmptyData(result);
            
            // Aggregate call history features
            phoneFeatures._aggregateCallHistoryFeatures(result, aggregatedFeatureName);

            return result;
        };

        phoneFeatures._TrimEmptyData = function(data) {
            var dataIndex = data.length;
            while (dataIndex--) {
                var featureCategory = data[dataIndex];
                var featuresIndex = featureCategory.Features.length;
                while (featuresIndex--) {
                    var feature = featureCategory.Features[featuresIndex];
                    var phoneStatusIndex = feature.phone_statuses.length;
                    while (phoneStatusIndex--) {
                        var phoneStatus = feature.phone_statuses[phoneStatusIndex];
                        var areAllPhoneStatusesEmpty = true;
                        // Remove empty phone status
                        if (phoneStatus != -1) {
                            areAllPhoneStatusesEmpty = false;
                            break;
                        }
                    }

                    // Remove empty feature
                    if (areAllPhoneStatusesEmpty) {
                        featureCategory.Features.splice(featuresIndex, 1);
                    }
                }

                // Remove empty feature category
                if (featureCategory.Features.length == 0) {
                    data.splice(dataIndex, 1);
                }
            }
        };
        
        phoneFeatures._aggregateCallHistoryFeatures = function (data, aggregatedFeatureName) {
            // Mapping int value of the enum for "PhonebookImport" Feature Category. This is the category of Call History features
            var callHistoryCategory = 3; 
            var callHistoryFeatureCodes = new Array("call-history---combined", "call-history---incoming", "call-history---missed", "call-history---outgoing");
            var totalFeaturesCount = callHistoryFeatureCodes.length;

            // Find the call history category
            var callHistoryCategoryIndex = -1;
            $.each(data, function(index, item) {
                if (item.Category == callHistoryCategory) {
                    callHistoryCategoryIndex = index;
                    return false;
                }
                return true;
            });

            // Check if the data has the call history features' category
            if (callHistoryCategoryIndex == -1)
                return;
            var availableCallHistoryFeatureIndexes = new Array();
            var availableCallHistoryFeatures = $.map(data[callHistoryCategoryIndex].Features, function(item, index) {
                var callHisotryfeatureIndex = callHistoryFeatureCodes.indexOf(item.Code);
                if (callHisotryfeatureIndex != -1) {
                    availableCallHistoryFeatureIndexes.push(index);
                    return item.Code;
                }
                return null;
            });

            var noneCallHistoryFeatures =
                availableCallHistoryFeatures.indexOf(callHistoryFeatureCodes[0]) == -1 &&
                    availableCallHistoryFeatures.indexOf(callHistoryFeatureCodes[1]) == -1 &&
                    availableCallHistoryFeatures.indexOf(callHistoryFeatureCodes[2]) == -1 &&
                    availableCallHistoryFeatures.indexOf(callHistoryFeatureCodes[3]) == -1;

            // Check if none of the call history features are available
            if (noneCallHistoryFeatures) return;

            // For each phone in the category aggregate the features
            var phoneCount = data[callHistoryCategoryIndex].Features[0].phone_statuses.length;
            var aggregatedFeatureStatuses = { DisplayName: aggregatedFeatureName, Tooltip: null, phone_statuses: new Array() };
            for (var phoneIndex = 0; phoneIndex < phoneCount; phoneIndex++) {
                //get phone features of the current phone
                var featuresStatus = $.map(data[callHistoryCategoryIndex].Features, function(item) {
                    var featureIsCallHisotry = callHistoryFeatureCodes.indexOf(item.Code) != -1;
                    return featureIsCallHisotry ? item.phone_statuses[phoneIndex] : null;
                });

                // Get the aggregated status and add it the features of the related feature category
                var aggregatedStatus = phoneFeatures._getAggregatedFeatureStatus(featuresStatus, totalFeaturesCount);
                aggregatedFeatureStatuses.phone_statuses[phoneIndex] = aggregatedStatus;
            }
            data[callHistoryCategoryIndex].Features.push(aggregatedFeatureStatuses);

            // Remove the call history features. 
            // First sort the array descending and then remove the items of indexes from from the end of the array
            availableCallHistoryFeatureIndexes.sort(function(x, y) { return y - x; });
            $.each(availableCallHistoryFeatureIndexes, function(index, item) {
                data[callHistoryCategoryIndex].Features.splice(item, 1);
            });
        };

        phoneFeatures._getAggregatedFeatureStatus = function(featureStatuses, totalFeaturesCount) {
            if (featureStatuses.length == 0)
                throw "Empty set of features cannot be aggregated.";

            // These int values map the Feature Status enum at the API
            var notTested = 0;
            var nonCompatible = 1;
            var compatible = 2;
            var partialCompatiblity = 3;

            // If any feature status is not tested, return not-tested
            var anyStatusIsNotTested = featureStatuses.indexOf(notTested) != -1;
            if (anyStatusIsNotTested)
                return notTested;

            // If any feature status is non-compatible, return non-compatible
            var anyStatusIsNonCompatible = featureStatuses.indexOf(nonCompatible) != -1;
            if (anyStatusIsNonCompatible)
                return nonCompatible;

            // If any feature status is partially-compatible or any feature is missing, return partially-compatible
            var anyStatusIsPartialCompatiblity = featureStatuses.indexOf(partialCompatiblity) != -1;
            if (anyStatusIsPartialCompatiblity || featureStatuses.length < totalFeaturesCount)
                return partialCompatiblity;

            return compatible;
        };

        phoneFeatures.toPhoneStatusWithDefault = function(status) {
            if (status == 0 || status == 1 || status == 2 || status == 3)
                return status;
            return -1;
        };
        
        bluetooth.PhoneFeatures = phoneFeatures;
    })(climax.Bluetooth || (climax.Bluetooth = { }));
})(Climax || (Climax = { }));