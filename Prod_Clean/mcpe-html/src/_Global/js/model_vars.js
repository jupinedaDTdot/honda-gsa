;(function() {

window._globals['hero.video_showcase'] = {"type":"flash","width":"700","height":"390","hashVersion":"9","swf":"/_Global/swf/honda_videoPlayer.swf","flashvars":{show_controls:'true', xml_location:'/_Global/assets/xml/videos_test.xml'},"params":{"allowScriptAccess":"sameDomain","allowNetworking":"all","wmode":"opaque","quality":"high","play":"true","loop":"false","menu":"false","scale":"showall","bgcolor":"#4c4c4c"}};

window._globals['hero.build_price'] = {"type":"flash","width":"950","height":"650","hashVersion":"9","swf":"http://www.buildyourhonda.ca/main.swf","flashvars":{"lang":"fr","dataRoot":"data%2fJSON","assetsRoot":"data%2fJSON%2fassets","dataDomain":"http%3a%2f%2fbuildyourhonda.ca","assetsDomain":"http%3a%2f%2fbuildyourhonda.ca","mediaDomain":"http%3a%2f%2fbuildyourhonda.ca"},"params":{"allowScriptAccess":"sameDomain","allowNetworking":"all","wmode":"transparent","quality":"high","play":"true","loop":"false","menu":"false","scale":"showall","bgcolor":"#4c4c4c"}};

// window._globals['hero.360_view'] = {type:'image', src:'images/Capture.png', height: 400, image_width: 950, image_height: 400};
window._globals['hero.360_view'] = {"type":"flash","width":"950","height":"402","hashVersion":"9","swf":"/_Global/swf/Hondaca_360Viewer.swf","flashvars":{car360:"/_Global/assets/car360_01.swf"},"params":{"allowScriptAccess":"sameDomain","allowNetworking":"all","wmode":"transparent","quality":"high","play":"true","loop":"false","menu":"false","scale":"showall","bgcolor":"#4c4c4c"}};
window._globals['hero.key_features'] = {"type":"flash","width":"950","height":"470","hashVersion":"9","swf":"/_Global/swf/FeaturesAndBenefits.swf","flashvars":{lang: 'fr', pkg:"/_Global/assets/key_features/odyssey.swf"},"params":{"allowScriptAccess":"sameDomain","allowNetworking":"all","wmode":"transparent","quality":"high","play":"true","loop":"false","menu":"false","scale":"showall","bgcolor":"#4c4c4c"}};

window._globals['model.current_offers'] = 'testing_current_offers.html?';

window._globals['model.book_test_drive'] = {"file":"modal_window.html", "closeBtnSelector":"#test_drive_close_btn"};
window._globals['model.co_model_id'] = 1; // model id of the current page (when we're on model pages)
window._globals['url.trim_selector'] = 'model_overview.html'; // location of trim_selector in model pages (other than model_overview)

})();