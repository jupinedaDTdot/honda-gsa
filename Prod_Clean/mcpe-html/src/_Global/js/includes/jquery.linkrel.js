/* $Id: jquery.linkrel.js 14343 2011-03-10 22:35:43Z yuxhuang $ */
;(function($){
	
function load_image(options) {
	var $ctr = $(options.container);
	// add the loader
	$ctr.addClass('bg_loader');
	$('body').append(
		$('<img>').attr({
			src: options.src,
			width: options.width,
			height: options.height
		})
		.addClass('dn')
		.load(function() {
			$ctr.removeClass('bg_loader');
			$(this).appendTo($ctr).fadeIn('slow');
		})
	);
}


$.fn.linkrel = function(opts) {
	
	var original_height = $(opts.defaultTarget).height();
	
	return $(this).each(function() {
		var self = $(this);

		if (self.hasClass('linkrel_processed')) {
			return;
		}

		var thisopts = $.extend({}, $.fn.linkrel.defaults, opts);
		
		self.data('lock', false);
		
		var target = thisopts.target;
		if (thisopts.targetContent) {
			target = thisopts.targetContent;
		}

		function _action_close() {
			self.data('lock', true);
			if (thisopts.closingAnimation) {
				$(target).animate({height:original_height}).queue(function() {
					$(thisopts.target).hide();
					$(thisopts.defaultTarget).show();
					$(thisopts.group).removeClass('selected');
					$(target).empty().height(original_height);
					self.data('lock', false);
					$(this).dequeue();
				});
			}
			else {
				$(thisopts.target).hide();
				$(thisopts.defaultTarget).show();
				$(thisopts.group).removeClass('selected');
				$(target).empty().height(original_height);
				self.data('lock', false);
			}
			return false;
		}
		
		// close button event
		if (thisopts.closeButton) {
			$(thisopts.closeButton).click(function() {
				_action_close.apply(this);
				return false;
			});
		}
		
		self.click(function() {
			if (self.data('lock')) {
				return false;
			}
			
			// if self is selected, then close the window
			if (self.is('.selected')) {
				_action_close.apply(self);
				return false;
			}
			
			self.data('lock', true);
			// then we need to load the flash, according to parameters sent in rel
			var key = self.attr('rel');
			var options = $.extend({}, $.fn.linkrel.default_options, global_get(key));
			
			$('html,body').animate({
				scrollTop: $(thisopts.target).is(':visible') ? $(thisopts.target).offset().top : $(thisopts.defaultTarget).offset().top
			}, 1000);
			// the loader should run after the animation is done, so the loader image
			// is loaded correctly into the center of the container.
			// this requirement is accomplished by attaching the .queue call to the
			// container after the animation
			$(thisopts.defaultTarget).hide();
			$(thisopts.target).show();
			$(target).removeClass('hero_video_showcase').animate({
				height: key == 'hero.video_showcase' ? (parseInt(options.height) + 40) : options.height
			}).empty().queue(function() {
				if (options.type == 'flash') {
					if ('share_url' in options && options.share_url) {
						options.flashvars.share_link = options.share_url;
					}
					else {
						options.flashvars.share_link = window.location.href;
					}
										
					options.id = 'hero_flash';
					if ($.flash.available) {
						// FIXME dirty hack to make sure the domains for the build it tool comes from the same domain of the page
						if (key == 'hero.build_price') {
							options.flashvars.dataDomain = window.location.protocol + '//' + window.location.host;
							options.flashvars.assetsDomain = window.location.protocol + '//' + window.location.host;
							options.flashvars.mediaDomain = window.location.protocol + '//' + window.location.host;
							var env = global_get('environment');
							options.flashvars.dataRoot = options.flashvars.dataRoot.replace(/\/(LIVE|STAGING)\//, '/' + env + '/');
							options.flashvars.assetsRoot = options.flashvars.assetsRoot.replace(/\/(LIVE|STAGING)\//, '/' + env + '/');
							options.flashvars.mediaRoot = options.flashvars.mediaRoot.replace(/\/(LIVE|STAGING)\//, '/' + env + '/');
						}	
						$(target).flash(options);
					}
					else {
						requires_flash_message(target);
					}
				}
				else if (options.type == 'image') {
					load_image({
						src: options.src + '?' + Math.random(),
						width: options.image_width,
						height: options.image_height,
						container: target
					});
				}
				if (key == 'hero.video_showcase') {
					$(target).addClass('hero_video_showcase');
				}
				$(this).dequeue();
			}).delay(100).queue(function() {
				self.data('lock', false);
				if (options.type == 'flash') {
					if ('hero_flash' in document) {
						document.hero_flash.focus();
					}
					else if (document.getElementById('hero_flash')){
						document.getElementById('hero_flash').focus();
					}
				}
				$(this).dequeue();
			}).find('.rc4').remove();
			
			$(thisopts.group).removeClass('selected');
			self.addClass('selected');
      		$(thisopts.group).filter('[rel="'+self.attr('rel')+'"]').addClass('selected');
			
			return false;
		}).addClass('linkrel_processed');
	});
};

$.fn.linkrel.defaults = {
	closingAnimation: true,
	defaultTarget: '', // default target - required
	targetContent: '', // where to replace - optional (target is used then)
	target: '', // target container - required
	group: '', // group of linkrels - required
	closeButton: '', // selector of the close button - optional
	_last: null
};

$.fn.linkrel.default_options = {
	hasVersion: 9
};

})(jQuery);