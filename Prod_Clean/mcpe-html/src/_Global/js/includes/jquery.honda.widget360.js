;(function($){

var defaults = {
	images: [
//		'Seq_v04_640x378_01.jpg',
//		'Seq_v04_640x378_03.jpg',
//		'Seq_v04_640x378_05.jpg',
//		'Seq_v04_640x378_07.jpg',
//		'Seq_v04_640x378_09.jpg',
//		'Seq_v04_640x378_11.jpg',
//		'Seq_v04_640x378_13.jpg',
	],
	opening: 2,
	cw: true,
	brake: 0.4,
	opening: 2,
	entry: 0.7,
	klass: 'widget360',
	width: 640,
	height: 378

};

$.widget('honda.widget360', {

	options: defaults,

	_create: function() {
		var self = this, o = this.options;

		var $img = $('<img>').attr({
				src: o.images[0]
			}).width(o.width).height(o.height);

		self.element.append($img);

    if (window.tracker_get) {
      (function() {
        var tracker = tracker_get();
        if (!tracker) return;
        
        try {
          tracker.trackEvent('360 View', 'Open');
        } catch (e) { if (window.console) console.log(e); }
      }.call(this));
    }

		require(["ext/jquery-reel/jquery.reel"], function(){

			$('img', self.element).reel({
				images: o.images,
				frames: o.images.length,
				cw: o.cw,
				brake: o.brake,
				opening: o.opening,
				entry: o.entry,
				klass: o.klass,
				wheelable: false
			})

		})
	}
});

})(jQuery);

