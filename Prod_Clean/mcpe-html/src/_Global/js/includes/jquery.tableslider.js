/* $Id: jquery.tableslider.js 14343 2011-03-10 22:35:43Z yuxhuang $ */
;(function($){

$.fn.tableslider = function(options) {
	
	function init(opts) {
		// ignore table slider if its width is not large enough
		if ($(this).width() - opts.fixedColumnWidth <= opts.width) {
			return;
		}

		// clone the table with data and events
		var original_table = this;
		var $tbl = $(this).clone(true).width($(this).outerWidth());
		
		function _offset() {
			var offset = $(original_table).offset();
			return {
				top: Math.ceil(offset.top),
				left: Math.ceil(offset.left + opts.fixedColumnWidth)
			};
		}
		
		// remove the fixed columns
		$tbl.find(opts.columnSelector).filter(opts.fixedColumnSelector).css('visibility', 'hidden').end().end()
			.find(opts.headSelector).filter(opts.fixedColumnSelector).css('visibility', 'hidden');
		
		// current column
		var $currentColumn = $tbl.find(opts.rowSelector).filter(':first')
			.find(opts.headSelector).not(opts.fixedColumnSelector).filter(':first');
		
		// buttons
		var buttonLock_ = false;
		
		var $prevBtn = $('<a>').addClass(opts.prevClass).addClass('pngfix').click(function() {
			if (buttonLock_ || $container.find('.tableslider_prev').hasClass('disabled')) {
				return false;
			}
			buttonLock_ = true;

			var left = $tbl.position().left + $currentColumn.outerWidth();
			// next button is available again
			$container.find('.tableslider_next').removeClass('disabled');
			
			$tbl.animate({
				left: Math.ceil(left)
				},{
				complete: function() {
					// go to previous column
					$currentColumn = $currentColumn.prev(opts.headSelector + ':not(' + opts.fixedColumnSelector + ')');
					// check if previous column exists
					if ($currentColumn.prev(opts.headSelector + ':not(' + opts.fixedColumnSelector + ')').size() == 0) {
						$container.find('.tableslider_prev').addClass('disabled');
					}
					buttonLock_ = false;
				}
			});
			return false;
		}).append($('<span>').addClass('pngfix').html(opts.prevContent));
		
		var $nextBtn = $('<a>').addClass(opts.nextClass).addClass('pngfix').click(function() {
			if (buttonLock_ || $container.find('.tableslider_next').hasClass('disabled')) {
				return false;
			}
			buttonLock_ = true;
			var left = $tbl.position().left - $currentColumn.outerWidth();
			// prev button is available again
			$container.find('.tableslider_prev').removeClass('disabled');
			
			$tbl.animate({
				left: Math.ceil(left)
				},{
				complete: function() {
					// go to next column
					$currentColumn = $currentColumn.next(opts.headSelector + ':not(' + opts.fixedColumnSelector + ')');
					// try to calculate next until the end
					var width = 0;
					$currentColumn.nextAll(opts.headSelector + ':not(' + opts.fixedColumnSelector + ')').each(function() {
						width += $(this).outerWidth(true);
					});
					if (width < opts.width) {
						$container.find('.tableslider_next').addClass('disabled');
					}
					buttonLock_ = false;
				}
			});
			return false;
		}).append($('<span>').addClass('pngfix').html(opts.nextContent));
		
		function _hover() {
			if ($(this).hasClass('hover')) {
				$(this).removeClass('hover');
			}
			else {
				$(this).addClass('hover')
			}
		}
		
		// wrap the table with a container
		var $container = $('<div>').addClass(opts.additionalClass)
			.addClass('tableslider_container')
			.append(
				$('<div>').addClass('tableslider_prev').addClass('pngfix').append(
					$prevBtn
				).hover(_hover, _hover).addClass('disabled')
			)
			.append(
				$('<div>').addClass('tableslider_table').append(
					$tbl
				)
				.width(opts.width).height($(this).outerHeight(true))
				.css({
					position: 'relative',
					overflow: 'hidden'
				})
			)
			.append(
				$('<div>').addClass('tableslider_next').addClass('pngfix').append(
					$nextBtn
				).hover(_hover, _hover)
			);
		
		$tbl = $container.find(opts.tableSelector);
		$tbl.css({
			position: 'absolute',
			left: -opts.fixedColumnWidth
		});
		
		$container.css({
			position: 'absolute',
			top: $(this).position().top,
			left: opts.columnWidth
		})
		.appendTo(opts.outerContainer);

		$(window).resize(function() {
			$container.offset(_offset());
		});
		
		$container.offset(_offset());
		
		$(original_table).trigger('tableslidercomplete', [$container]);
		
		// add the table when mouse over for the first time
		// $(this).bind('mouseover', function() {
		// 	$container
		// 		.offset(_offset())
		// 		.find('.tableslider_table')
		// 		.height($(this).height());
		// });
		
		$(this).addClass('tableslider_processed');
	}
	
	return this.each(function() {
		var opts = $.extend(true, {}, $.fn.tableslider.defaults, options);
		if (!$(this).hasClass('tableslider_processed')) {
			init.call(this, opts);
		}
	});
};

$.fn.tableslider.defaults = {
	width: 450,
	tableSelector: 'table',
	rowSelector: 'tr',
	headSelector: 'th',
	columnSelector: 'td',
	fixedColumnSelector: '.first',
	fixedColumnWidth: 210,
	columnWidth: 150,
	prevClass: 'tableslider_btn_prev',
	prevContent: '',
	nextClass: 'tableslider_btn_next',
	nextContent: '',
	addtionalClass: 'tableslider',
	outerContainer: 'body'
};

})(jQuery);