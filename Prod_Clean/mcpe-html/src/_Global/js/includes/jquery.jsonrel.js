/* $Id: jquery.jsonrel.js 14993 2011-05-27 15:19:30Z yuxhuang $ */
;(function($){

function load_image(options) {
	var $ctr = $(options.container);
	// add the loader
	$ctr.addClass('bg_loader');
	$('body').append(
		$('<img>').attr({
			src: options.src,
			width: options.width,
			height: options.height
		})
		.addClass('dn')
		.load(function() {
			$ctr.removeClass('bg_loader');
			$(this).appendTo($ctr).fadeIn('slow');
		})
	);
}

$.fn.jsonrel = function(opts) {
	return $(this).each(function() {
		var thisopts = $.extend({}, $.fn.jsonrel.defaults, opts);
		
		if ($(this).hasClass('jsonrel_processed')) {
			return;
		}
		$(this).click(function(event) {

            event.preventDefault();

			// then we need to load the flash, according to parameters sent in rel
			try {
				var options = $.evalJSON($(this).attr('rel'));
				options = $.extend({}, $.fn.jsonrel.default_options, options);
			} catch(e) { return; }
			
			$(thisopts.target).empty();
			if (options.type == 'flash') {
				if ($.flash.available) {
					$(thisopts.target).flash(options);
				}
				else {
					requires_flash_message(thisopts.target);
				}
			}
			else if (options.type == 'image') {
				load_image({
					src: options.src + '?' + Math.random(),
					width: options.image_width,
					height: options.image_height,
					container: thisopts.target
				});
			}
			
			$(thisopts.group).removeClass('selected');
			$(this).addClass('selected');
		}).addClass('jsonrel_processed');
	});
};

$.fn.jsonrel.defaults = {
	target: '', // target container
	group: '', // group of linkrels
	_last: null
};

$.fn.jsonrel.default_options = {
	hasVersion: 9
};

})(jQuery);