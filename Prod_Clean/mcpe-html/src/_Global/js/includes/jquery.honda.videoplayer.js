;(function($){

var defaults = {
	width: 640,
	height: 360,
	autostart: true,
	//has to be at least a single element in array. If more then one element provided playlist feature will be enabled
	title: null,
	videos: [
		/*{src: '/old/_Global/media/echo-hereweare.mp4', caption: 'Video A', poster: '/old/_Global/media/echo-hereweare.jpg', thumb: '/old/_Global/media/video_thumb1.png'},
		{src: '/old/_Global/media/gizmo.mp4',          caption: 'Video B', poster: '/old/_Global/media/gizmo.jpg', thumb: '/old/_Global/media/video_thumb2.png'},
		{src: '/old/_Global/media/gizmo.mp4',          caption: 'Video C', poster: '/old/_Global/media/gizmo.jpg', thumb: '/old/_Global/media/video_thumb3.png'}*/
	],
	start_video_idx: 1, //video number (starts with 1) which will be loaded to player automatically from entire playlist based on video index in a playlist
	start_video_id: null, //video which will be loaded to player automatically from entire playlist based on video id. this is will overwrite start_video_idx
	ME_folder: '/old/_Global/js/ext/mediaelement/', //path to folder with flashmediaelement.swf
	xml_location: '', //location of XML file with playlist and labels
	fullscreen_enabled: true,
	share_url: '', //URL to display in a share popup
	share_box_title: 'Share this video',
	share_box_subtitle: 'Copy the URL below to share this',
	share_video_anchor_enabled: false, // if enabled will append #videoId=x to share URL for other then 1st videos.
	overlay_play_button_visible: false,
	timeline_handle_shift_right: 2,
	selector: 'regular', // playlist type. can be regular (overlaying video) or static (for acura advantage)
	selectorRows: 1,
	selectorShowWhenHover: true,
	controls_class: '', //class to be added to player controls e.g. "mejs-controls-square"
	thumb_width: 115,
	thumb_height: 60,
	skin: 'mejs-acura' //skin to be set to player
};

$.widget('honda.videoplayer', {

	options: defaults,

	_create: function() {
		var self = this;
		self._loadCSSdependencies();
	},

	_init: function() {
		var self = this, o = this.options, element = this.element;

		if (o.xml_location.length) {
      self.load_xml_options();
		} else {
      self._init2();
		}
    
	},

	_init2: function() {
		var self = this, o = this.options, element = this.element, player;
    if (o.videos < 1 )
			return;

		if (o.start_video_idx > o.videos.length)
    {
			o.start_video_idx = 1;
    }
    require(["ext/jquery.transform", "ext/mediaelement/mediaelement-and-player"], function() {
    	if (o.selector == 'regular') {
				var selector_version = 'selector';
			}
    	if (o.selector == 'static') {
				var selector_version = 'selectorstatic';
			}
    	require(["swfobject", "ext/mediaelement/mep-feature-swfsupport", "ext/mediaelement/mep-feature-share", "ext/mediaelement/mep-feature-" + selector_version], function() {
    		//overwrite o.start_video_idx if o.start_video_id provided
				if (o.start_video_id && o.start_video_id.length) {
					$.each(o.videos, function(idx, video) {
						if (video.video_id == o.start_video_id) {
							o.start_video_idx = idx + 1;
						}
					})
				}

				self.buildHTML();
        var MEOptions = {
					plugins: ['flash', 'youtube'],
					features: ['swfsupport', 'playpause','volume', 'progress'],
					selectorTitle: o.title,
					videos : o.videos,
					share_url: o.share_url,
					share_box_title: o.share_box_title,
					share_box_subtitle: o.share_box_subtitle,
					start_video_idx: o.start_video_idx,
					share_video_anchor_enabled: o.share_video_anchor_enabled,
					autostart: o.autostart,
					xmlWidth: o.xmlWidth,
					xmlHeight: o.xmlHeight,
					selectorBackgroudHeight: o.selectorBackgroudHeight,
					selectorShowWhenHover: o.selectorShowWhenHover,
					selectorRows: o.selectorRows,
					success: function(media, domNode, t){
            //wrap controls to extra element to have rounded corners
						t.controls.wrapInner('<span class="mejs-controls-wrap" />');

						//start playing mp4 or swf
					  if (o.autostart) {
					    t.playMediaOnInit(o.videos[o.start_video_idx - 1].src);
					  }
					  
            media.addEventListener('pause', function() {
							t.container.removeClass('mejs-state-playing');
						}, true);

						media.addEventListener('ended', function() {
							t.container.removeClass('mejs-state-playing');
						}, true);

						media.addEventListener('play', function() {
              
              t.container.addClass('mejs-state-playing');

              //change title and description when video is played
              if (o.videos.length > 0) {
                (function () {
                  try {
                    var video = { };
                    for (var index = 0; index < o.videos.length; index ++) {
						          var t = new RegExp(o.videos[index].src + '$');
						          if (t.test($(media).attr('src'))) {
						            video = o.videos[index];
						            break;
						          }
						        }
                    $('.videoCaptionObject').html(video.title);
                    $('.videoDescriptionObject').html(video.caption);
                  }
                  catch (e) {}
                }.call(this));
              }

              // if tracker_get exists, let's track the video
						  if (window.tracker_get) {
						    (function() {
						      try {
						        var tracker = tracker_get(), video;
						        if (!tracker) return;
                    // find the video
						        for (var index = 0; index < o.videos.length; index ++) {
						          var t = new RegExp(o.videos[index].src + '$');
						          if (t.test($(media).attr('src'))) {
						            video = o.videos[index];
						            break;
						          }
						        }
						        if (video) {
                      if(window.console) console.log('Video', 'Play', video.caption || video.src);
  						        tracker.trackEvent('Video', 'Play', video.caption || video.src);
						        }
						      } catch (e) { if (window.console) console.log(e); }
						    }.call(this));
						  }


						}, true);
					}
				};
				if (o.fullscreen_enabled)
					MEOptions.features.push('fullscreen');

				if (o.videos.length > 1) {
					MEOptions.features.push(selector_version);
				} if (o.share_url) {
					MEOptions.features.push('share');
				}

				player = new MediaElementPlayer($('video', element)[0], MEOptions);

				//HACK !!! overwriting rail resize function to use .mejs-controls-wrap width instead of .mejs-controls width
				//may need to update function below when on MediaElement upgrade
				player.setControlsSize = function() {

					var t = this,
						usedWidth = 0,
						railWidth = 0,
						rail = t.controls.find('.mejs-time-rail'),
						total = t.controls.find('.mejs-time-total'),
						current = t.controls.find('.mejs-time-current'),
						loaded = t.controls.find('.mejs-time-loaded');
						others = rail.siblings();


					// allow the size to come from custom CSS
					if (t.options && !t.options.autosizeProgress) {
						// Also, frontends devs can be more flexible
						// due the opportunity of absolute positioning.
						railWidth = parseInt(rail.css('width'));
					}

					// attempt to autosize
					if (railWidth === 0 || !railWidth) {

						// find the size of all the other controls besides the rail
						others.each(function() {
							if ($(this).css('position') != 'absolute') {
								usedWidth += $(this).outerWidth(true);
							}
						});

						// fit the rail into the remaining space
						//NEW!!!
						railWidth = t.controls.find('.mejs-controls-wrap').width() - usedWidth - (rail.outerWidth(true) - rail.outerWidth(false));
						//OLD!!!
						//railWidth = t.controls.width() - usedWidth - (rail.outerWidth(true) - rail.outerWidth(false));
					}

					// outer area
					rail.width(railWidth);
					// dark space
					total.width(railWidth - (total.outerWidth(true) - total.width()));
				  
					if (t.setProgressRail)
						t.setProgressRail();
					if (t.setCurrentRail)
						t.setCurrentRail();
				};

				//HACK - overriding default function to shift rail handle to the right, because of design.
				player.setCurrentRail = function() {
					var t = this;

					if (t.media.currentTime != undefined && t.media.duration) {

						// update bar and handle
						if (t.total && t.handle) {
							var
								newWidth = t.total.width() * t.media.currentTime / t.media.duration,
								handlePos = newWidth - (t.handle.outerWidth(true) / 2),
								adjustedHandlePost = handlePos + o.timeline_handle_shift_right;

							if (adjustedHandlePost > t.total.width() - t.handle.outerWidth(true)) {
								adjustedHandlePost = t.total.width() - t.handle.outerWidth(true)
							}

							t.current.width(newWidth);
							t.handle.css('left', adjustedHandlePost);
						}
					}
				}

				player.showControls = function(doAnimation) {
					var t = this;

					doAnimation = typeof doAnimation == 'undefined' || doAnimation;

					if (t.controlsAreVisible)
						return;

					t.controls.trigger('showсontrols');

					if (doAnimation) {
						t.controls
							.css('visibility','visible')
							.stop(true, true).fadeIn(200, function() {t.controlsAreVisible = true;});

						// any additional controls people might add and want to hide
						t.container.find('.mejs-control')
							.css('visibility','visible')
							.stop(true, true).fadeIn(200, function() {t.controlsAreVisible = true;});

					} else {
						t.controls
							.css('visibility','visible')
							.css('display','block');

						// any additional controls people might add and want to hide
						t.container.find('.mejs-control')
							.css('visibility','visible')
							.css('display','block');

						t.controlsAreVisible = true;
					}

					t.setControlsSize();

				};

				player.hideControls = function(doAnimation) {
					var t = this;

					doAnimation = typeof doAnimation == 'undefined' || doAnimation;

					if (!t.controlsAreVisible)
						return;

					t.controls.trigger('hideсontrols');

					if (doAnimation) {
						// fade out main controls
						t.controls.stop(true, true).fadeOut(200, function() {
							$(this)
								.css('visibility','hidden')
								.css('display','block');

							t.controlsAreVisible = false;
						});

						// any additional controls people might add and want to hide
						t.container.find('.mejs-control').stop(true, true).fadeOut(200, function() {
							$(this)
								.css('visibility','hidden')
								.css('display','block');
						});
					} else {

						// hide main controls
						t.controls
							.css('visibility','hidden')
							.css('display','block');

						// hide others
						t.container.find('.mejs-control')
							.css('visibility','hidden')
							.css('display','block');

						t.controlsAreVisible = false;
					}
				};

				//HACK - not hiding controls if mouse over player
				player.meReady = function(media, domNode) {


					var t = this,
						mf = mejs.MediaFeatures,
						autoplayAttr = domNode.getAttribute('autoplay'),
						autoplay = !(typeof autoplayAttr == 'undefined' || autoplayAttr === null || autoplayAttr === 'false'),
						featureIndex,
						feature;

					// make sure it can't create itself again if a plugin reloads
					if (t.created)
						return;
					else
						t.created = true;

					t.media = media;
					t.domNode = domNode;

					if (!(mf.isAndroid && t.options.AndroidUseNativeControls) && !(mf.isiPad && t.options.iPadUseNativeControls) && !(mf.isiPhone && t.options.iPhoneUseNativeControls)) {

						// two built in features
						t.buildposter(t, t.controls, t.layers, t.media);
						t.buildkeyboard(t, t.controls, t.layers, t.media);
						t.buildoverlays(t, t.controls, t.layers, t.media);

						// grab for use by features
						t.findTracks();

						// add user-defined features/controls
						for (featureIndex in t.options.features) {
							feature = t.options.features[featureIndex];
							if (t['build' + feature]) {
								try {
									t['build' + feature](t, t.controls, t.layers, t.media);
								} catch (e) {
									// TODO: report control error
									//throw e;
									//console.log('error building ' + feature);
									//console.log(e);
								}
							}
						}

						t.container.trigger('controlsready');

						// reset all layers and controls
						t.setPlayerSize(t.width, t.height);
						t.setControlsSize();


						// controls fade
						if (t.isVideo) {

							if (mejs.MediaFeatures.hasTouch) {

								// for touch devices (iOS, Android)
								// show/hide without animation on touch

								t.$media.bind('touchstart', function() {


									// toggle controls
									if (t.controlsAreVisible) {
										t.hideControls(false);
									} else {
										if (t.controlsEnabled) {
											t.showControls(false);
										}
									}
								});

							} else {
								// click controls
								var clickElement = (t.media.pluginType == 'native') ? t.$media : $(t.media.pluginElement);

								// click to play/pause
								clickElement.click(function() {
									if (media.paused) {
                    media.play();
									} else {
										media.pause();
									}
								});


								// show/hide controls
								t.container
									.bind('mouseenter mouseover', function () {
										if (t.controlsEnabled) {
											if (!t.options.alwaysShowControls) {
												t.killControlsTimer('enter');
												t.showControls();
												//t.startControlsTimer(2500); //HACK COMMENTED OUT
											}
										}
									})
									.bind('mousemove', function() {
										if (t.controlsEnabled) {
											if (!t.controlsAreVisible) {
												t.showControls();
											}
											//t.killControlsTimer('move');
											if (!t.options.alwaysShowControls) {
												//t.startControlsTimer(2500); //HACK COMMENTED OUT
											}
										}
									})
									.bind('mouseleave', function () {
										if (t.controlsEnabled) {
											if (!t.media.paused && !t.options.alwaysShowControls) {
												//t.startControlsTimer(1000); //HACK COMMENTED OUT
												t.startControlsTimer(100); //HACK NEW LINE ADDED
											}
										}
									});
							  
							}

							// check for autoplay
							if (autoplay && !t.options.alwaysShowControls) {
								t.hideControls();
							}
						  
              if (mf.isiOS && t.options.videos.length > 1) {
                t.hideControls();
                var firstPlay = function() {
                  setTimeout(function() {
                    t.pause();
                    t.showControls();
                    t.showSelectorLayers(t.layers);
                  }, 100);
                  t.media.removeEventListener('play', firstPlay);
                };
                t.media.addEventListener('play', firstPlay);
              }

							// resizer
							if (t.options.enableAutosize) {
								t.media.addEventListener('loadedmetadata', function(e) {
									// if the <video height> was not set and the options.videoHeight was not set
									// then resize to the real dimensions
									if (t.options.videoHeight <= 0 && t.domNode.getAttribute('height') === null && !isNaN(e.target.videoHeight)) {
										t.setPlayerSize(e.target.videoWidth, e.target.videoHeight);
										t.setControlsSize();
										t.media.setVideoSize(e.target.videoWidth, e.target.videoHeight);
									}
								}, false);
							}
						}

						// EVENTS

						// FOCUS: when a video starts playing, it takes focus from other players (possibily pausing them)
						t.media.addEventListener('play', function() {
                // go through all other players
                for (var i=0, il=mejs.players.length; i<il; i++) {
									var p = mejs.players[i];
                  
									if (p.id != t.id && t.options.pauseOtherPlayers && !p.paused && !p.ended) {
										p.pause();
									}
									p.hasFocus = false;
								}

								t.hasFocus = true;
						    
						},false);


						// ended for all
						t.media.addEventListener('ended', function (e) {
							try{
								t.media.setCurrentTime(0);
							} catch (exp) {

							}
							t.media.pause();

							if (t.setProgressRail)
								t.setProgressRail();
							if (t.setCurrentRail)
								t.setCurrentRail();

							if (t.options.loop) {
								t.media.play();
							} else if (!t.options.alwaysShowControls && t.controlsEnabled) {
								t.showControls();
							}
						}, false);

						// resize on the first play
						t.media.addEventListener('loadedmetadata', function(e) {
							if (t.updateDuration) {
								t.updateDuration();
							}
							if (t.updateCurrent) {
								t.updateCurrent();
							}

							if (!t.isFullScreen) {
								t.setPlayerSize(t.width, t.height);
								t.setControlsSize();
							}
						}, false);


						// webkit has trouble doing this without a delay
						setTimeout(function () {
							t.setPlayerSize(t.width, t.height);
							t.setControlsSize();
						}, 0);

						// adjust controls whenever window sizes (used to be in fullscreen only)
						$(window).resize(function() {

							// don't resize for fullscreen mode
							if ( !(t.isFullScreen || (mejs.MediaFeatures.hasTrueNativeFullScreen && document.webkitIsFullScreen)) ) {
								t.setPlayerSize(t.width, t.height);
							}

							// always adjust controls
							t.setControlsSize();
						});

						// TEMP: needs to be moved somewhere else
						if (t.media.pluginType == 'youtube') {
							t.container.find('.mejs-overlay-play').hide();
						}
					}

					// force autoplay for HTML5
					if (autoplay && media.pluginType == 'native') {
            media.load();
						media.play();
					}


					if (t.options.success) {

						if (typeof t.options.success == 'string') {
								window[t.options.success](t.media, t.domNode, t);
						} else {
								t.options.success(t.media, t.domNode, t);
						}
					}
				};




				player.currentVideoIdx = o.start_video_idx;


				if (o.controls_class.length) {
					$('.mejs-controls', player.container).addClass(o.controls_class);
				}

				if (o.overlay_play_button_visible){
					setTimeout(function(){
						player.layers.find('.mejs-overlay-button').show();
					}, 500);
				}

				if (o.skin.length) {
					player.changeSkin(o.skin);
				}

				$('button, .mejs-time-handle', element).live('mouseenter', function(){
					$(this).addClass('hover');
				});

				$('button, .mejs-time-handle', element).live('mouseleave', function(){
					$(this).removeClass('hover');
				});


				//seems can't find a better callback method when all plugins, layers and controls will be initialized.
				setTimeout(function(){self._created();}, 500);

			});
		});
	},

	load_xml_options: function() {
		var self = this, o = this.options, element = this.element;
		$.ajax({
			type: "GET",
			url: o.xml_location,
			dataType: "xml",
			success: function(xml) {
			  o.title = $(xml).find('vidplayer > title').text();
        if ($(xml).find('videos').length) {
					o.videos = [];
					$(xml).find('video').each(function(idx, video_tag) {
					  var src = $(video_tag).attr('url');
					  if ($(video_tag).attr('externalUrl')) {
					    src = $(video_tag).attr('externalUrl');
					    if (src) {
  					    var matches = src.match(/[&?]v=(.*?)([&?]|$)/);
					      if (matches) {
  					      src = 'http://www.youtube.com/watch?v=' + matches[1];
					      }
					    }
					  }
						var video = {
							src: src,							
							thumb: ($(video_tag).attr('thumb')) ? $(video_tag).attr('thumb') + "?width=" + o.thumb_width + "&amp;height=" + o.thumb_height + "&amp;crop=auto" : '',
							caption: $(video_tag).attr('desc') || '',
              title: $(video_tag).attr('title') || '',
							video_id: $(video_tag).attr('videoId') || null
						};
            
						if ($(video_tag).attr('toutImage')) {
							video.poster = $(video_tag).attr('toutImage');
						}
						o.videos.push(video);
            
					})
				}
				if ($(xml).find('labels').length) {
					o.share_box_title = $(xml).find('label[name="shareBoxTitle"]').text();
					o.share_box_subtitle = $(xml).find('label[name="shareBoxSubtitle"]').text();
				}
        
				self._init2();
        
			}
		});
	},

	buildHTML: function() {
		var self = this, o = self.options, element = self.element;

		var $video = $('<video>')
				.attr('width', o.width)
				.attr('height', o.height)
				.attr('controls', 'controls')
				.attr('preload', 'none');

		if (self.shouldEnablePoster()) {
			$video.attr('poster', o.videos[o.start_video_idx-1].poster)
		};

	  var $source;
	  
    if (/^https?:\/\//.test(o.videos[o.start_video_idx - 1].src)) {
      $source = $('<source>')
				.attr('type', 'video/youtube')
				.attr('src', o.videos[o.start_video_idx-1].src)
				.appendTo($video);
    }
	  else {
      $source = $('<source>')
				.attr('type', 'video/mp4')
				.attr('src', o.videos[o.start_video_idx-1].src)
				.appendTo($video);
    }

		var $flashobject = $('<object>')
				.attr('width', o.width)
				.attr('height', o.height)
				.attr('type', 'application/x-shockwave-flash')
				.attr('data', o.ME_folder+'flashmediaelement.swf');

		var $param1 = $('<param>')
				.attr('name', 'movie')
				.attr('value', o.ME_folder+'flashmediaelement.swf')
				.appendTo($flashobject);

		var param2value = 'controls=true&file='+o.videos[o.start_video_idx-1].src;
		if (self.shouldEnablePoster()) {
			param2value += '&poster='+o.videos[o.start_video_idx-1].poster;
		}

		var $param2 = $('<param>')
				.attr('name', 'flashvars')
				.attr('value', param2value)
				.appendTo($flashobject);


		$video.append($flashobject);
		element.empty().append($video);
	},

	shouldEnablePoster: function() {
		var self = this, o = self.options;
		// enable poster only if it's provided and if it's iPad (no autostart) or autostart disabled
		if (o.videos[o.start_video_idx-1].poster && (navigator.platform == "iPad" || o.autostart == false)) {
			return true;
		}
		return false;
	},

	_loadCSSdependencies: function(){
		var self = this;
		self._loadCSSfile('/old/_Global/js/ext/mediaelement/mediaelementplayer.css');
		self._loadCSSfile('/old/_Global/css/video-me.css');
	},

	_loadCSSfile: function(filepath) {

		if (document.createStyleSheet) {
			document.createStyleSheet(filepath);
		}
		else {
			var linkTag = document.createElement('link');
			linkTag.type = 'text/css';
			linkTag.rel = 'stylesheet';
			linkTag.href = filepath;
			document.getElementsByTagName('head')[0].appendChild(linkTag);
		}
	},

	_created: function(event, ui) {
		this._trigger('created', event, ui)
	}

});

})(jQuery);

