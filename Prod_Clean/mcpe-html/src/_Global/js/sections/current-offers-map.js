;(function($, F, require, undefined) {

F.page_init = function(ctx) {
    require(['ext/raphael'], function() {

    var defaults = {
        svgUrl: F.global_get('url.current_offers_map'),
        dataUrl: F.global_get('url.current_offers_data'),
        complete: function(event, ui) {},
        datacomplete: function(event, ui) {},
        itemcomplete: function(event, ui) {},
        itemmouseover: function(event, ui) {},
        itemmouseout: function(event, ui) {},
        itemclick: function(event, ui) {}
    };

    $.widget('honda.image_map', {
        widgetEventPrefix: 'honda_image_map',
        options: defaults,
        
        _create: function() {
            var self = this, o = this.options, element = this.element;
            this.reloadData();
        },

        reloadData: function() {
            var self = this, o = this.options, element = this.element;

            $.ajax({
                url: o.svgUrl,
                global: false,
                type: 'GET',
                dataType: 'xml', // so ie could read it correctly
                success: function(data, status, xhr) {
                    element.empty();
                    var width = parseInt($('svg', data).attr('width'));
                    var height = parseInt($('svg', data).attr('height'));
                    var R = Raphael(element.attr('id'), width, height);
                    var attr = {
                        fill: '#bababa',
                        stroke: '#f6f6f6',
                        'stroke-width': 0,
                        "stroke-linejoin": "miter"
                    };
                    var map = {};
                    var text = {};
                    $('svg > g', data).each(function() {
                        try {
                            var el = $(this);
                            var id = el.attr('id');
                            // the province outline should be one single path
                            // probably compound path
                            $('> path', this).each(function() {
                                var path = $(this);
                                var p = R.path(path.attr('d'));
                                var a = $.extend({}, attr);
                                a.fill = path.attr('fill');
                                p.attr(a);
                                if (!map[id]) {
                                    map[id] = p;
                                }
                            });
                            // text
                            $('g > path', this).each(function() {
                                var path = $(this);
                                var p = R.path(path.attr('d'));
                                var a = $.extend({}, attr);
                                a.fill = path.attr('fill');
                                p.attr(a);
                                if (!text[id]) {
                                    text[id] = new Array();
                                }
                                text[id].push(p);
                            });
                        } catch(e) {
                            console.log(e);
                        }
                    });


                    for (var province in map) {
                        (function(drawing, prov) {
                            drawing[0].style.cursor = 'pointer';
                            drawing.semaphore = 0;
                            // mouse over
                            $(drawing[0]).mouseover(function(event) {
                                drawing.semaphore ++;
                                drawing.attr({fill: '#c00114'});
                                R.safari();
                                self._trigger('itemmouseover', event, {
                                    id: prov,
                                    path: map[prov],
                                    text: text[prov]
                                });
                            });
                            // mouse out
                            $(drawing[0]).mouseout(function(event) {
                                setTimeout(function() {
                                    drawing.semaphore --;
                                    if (drawing.semaphore <= 0) {
                                        drawing.attr({fill: '#bababa'});
                                        R.safari();
                                        self._trigger('itemmouseout', event, {
                                            id: prov,
                                            path: map[prov],
                                            text: text[prov]
                                        });
                                    }
                                }, 1);
                            });
                            // click
                            $(drawing[0]).click(function(event) {
                                self._trigger('itemclick', event, {
                                    id: prov,
                                    path: map[prov],
                                    text: text[prov]
                                });
                            });
                            // text hover, so the text is considered part of the province
                            // may fail here
                            try {
                                if (text[prov] && $.isArray(text[prov])) {
                                    for (var index in text[prov]) {
                                        text[prov][index][0].style.cursor = 'pointer';
                                        $(text[prov][index][0]).mouseover(function(event) {
                                            drawing.attr({fill: '#c00114'});
                                            R.safari();
                                            self._trigger('itemmouseover', event, {
                                                id: prov,
                                                path: map[prov],
                                                text: text[prov]
                                            });
                                        });
                                        // click
                                        $(text[prov][index][0]).click(function(event) {
                                            self._trigger('itemclick', event, {
                                                id: prov,
                                                path: map[prov],
                                                text: text[prov]
                                            });
                                        });
                                    }
                                }
                            } catch(e) {
                                console.log(e);
                            }
                            // trigger the item complete event
                            self._trigger('itemcomplete', null, {
                                id: prov,
                                path: map[prov],
                                text: text[prov]
                            });
                        })(map[province], province);
                    }

                    // load data from the data url
                    try {
                        if (o.dataUrl) {
                            $.ajax({
                                type: 'GET',
                                url: o.dataUrl,
                                success: function(data, status, xhr) {
                                    self._trigger('datacomplete', null, {
                                        data: data,
                                        status: status,
                                        paths: map,
                                        text: text
                                    });
                                },
                                error: function(data, status, xhr) {
                                    console.log(status);
                                }
                            });
                        }
                    } catch(e) {
                        console.log(e.message);
                    }

                    // the widget considered intialized after the SVG is loaded
                    self._trigger('complete', null, {
                        data: data,
                        status: status,
                        paths: map,
                        text: text
                    });
                },
                error: function(data, status, xhr) {
                    console.log(status);
                }
            });
        }
    });

    $(document).ready(function() {
        var draw_box = function (paper, x, y, r, w, h, size, text, href) {
            var et = paper.text(x + w/2, y + h/2, text);
            et.attr({
                'font-family': 'Helvetica, Arial, sans-serif',
                'font-size': size,
                fill: '#fff',
                stroke: '#fff',
                href: href,
                target: '_blank'
            });
            var eb = paper.rect(x, y, w, h, r);
            eb.attr({
                fill: '#c00114',
                stroke: '#fff',
                target: '_blank',
                href: href
            });
            eb.insertBefore(et);
            var s = paper.set();
            s.push(et, eb);
            return s;
        };
        $('#canada_map').image_map({
            svgUrl: F.global_get('url.current_offers_map'),
            dataUrl: F.global_get('url.current_offers_data'),
            itemclick: function(event, ui) {
                var code = ui.id.toUpperCase();
                var href = $('.current-offers select option[rel=\''+code+'\']').attr('value');
                if (!href) {
                    // fallback to Northwestern Territory
                    var href = $('.current-offers select option[rel=NT]').attr('value');
                }
                if (href) {
                    window.location.href = href;
                }

            },
            itemcomplete: function(event, ui) {
                try {
                    var code = ui.id.toUpperCase();
                    // we just process Ontario, let's create two provinces for ontario
                    if (code == 'ON') {
                        // var paper = ui.path.paper;
                        // 					ui.path.subareas = {
                        // 						gta: draw_box(paper, 220, 320, 5, 30, 18, 10, 'GTA', 'http://www.gta.com').hide(),
                        // 						ottawa: draw_box(paper, 255, 320, 5, 58, 18, 10, 'OTTAWA', 'http://www.ottawa.com').hide()
                        // 					};
                        ui.path.iamin = false;
//                        $('#canada_map').append(
//                            $('<a>').addClass('map_link map_ottawa').text('OTTAWA').mouseover(function(){
//                                // ui.path.iamin = true;
//                                // ui.path.attr({fill: '#c00114'});
//                                ui.path.semaphore ++;
//                            }).mouseout(function() {
//                                // ui.path.iamin = false;
//                                ui.path.semaphore --;
//                            }).attr({
//                                href: $('a[href*=acuraott.ca]').attr('href').toString(),
//                                target: '_blank'
//                            })
//                        ).append(
//                            $('<a>').addClass('map_link map_gta').text('GTA').mouseover(function(){
//                                // ui.path.iamin = true;
//                                ui.path.semaphore ++;
//                                // ui.path.attr({fill: '#c00114'});
//                            }).mouseout(function() {
//                                // ui.path.iamin = false;
//                                ui.path.semaphore --;
//                            }).attr({
//                                href: $('a[href*=acuragta.ca]').attr('href').toString(),
//                                target: '_blank'
//                            })
//                        );
                    }
                }
                catch (e) {
                    console.log(e);
                }
            },
            itemmouseover: function(event, ui) {
                try {
                    var code = ui.id.toUpperCase();
                    // we just process Ontario, let's create two provinces for ontario
//                    if (code == 'ON') {
//                        $('.map_ottawa, .map_gta', '#canada_map').addClass('map_link_hover');
//                    }
                }
                catch (e) {
                    console.log(e);
                }
            },
            itemmouseout: function(event, ui) {
                try {
                    var code = ui.id.toUpperCase();
                    // we just process Ontario, let's create two provinces for ontario
//                    if (code == 'ON') {
//                        if (ui.path.iamin) {
//                            return;
//                        }
//                        $('.map_ottawa, .map_gta', '#canada_map').removeClass('map_link_hover');
//                    }
                }
                catch (e) {
                    console.log(e);
                }
            }
        });
    });

    });
};

})(jQuery, window, require);
