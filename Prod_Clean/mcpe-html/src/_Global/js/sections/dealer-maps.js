(function($) {

    window.section_init = function() {
        // load google maps first.
         var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = window.location.protocol + "//maps.googleapis.com/maps/api/js?sensor=false&callback=section_init.dealer_maps_section_load";
        document.body.appendChild(script);

    };

    // callback for google maps api
    window.section_init.dealer_maps_section_load = function() {
        // iterate all google maps elements to initialize them.
        $('[property~="act:map"]').each(function() {
            var elem = $(this);
            var mapItem = $('.map-item', this);

            // activate on click
            mapItem.one('mouseover click', function() {
                require(['ext/maps/markermanager'], function() {

                    var mapOptions = {
                        zoom: 14
                        , mapTypeId: google.maps.MapTypeId.ROADMAP
                        , panControl: false
                        , zoomControl: true
                        , streetViewControl: false
                        , overviewMapControl: false
                        , mapTypeControl: false
                    };

                    var map = new google.maps.Map(mapItem[0], mapOptions);

                    // retrieve latitude and longitude from the
                    var lat = $('.geo .latitude', elem).attr('title');
                    var lng = $('.geo .longitude', elem).attr('title');
                    var loc = new google.maps.LatLng(lat, lng);
                    var image = '/_Global/img/layout/large_pin.png';

                    var mgr = new MarkerManager(map);
                    var batch = [];
                    batch.push(new google.maps.Marker({
                        position: loc
                        , icon: image
                    }));

                    // add marker
                    google.maps.event.addListener(mgr, 'loaded', function() {
                        mgr.addMarkers(batch, 4);
                        mgr.refresh();
                    });

                    // set center of the map
                    map.setCenter(loc);
                });

            });
        });

    };

})(jQuery);