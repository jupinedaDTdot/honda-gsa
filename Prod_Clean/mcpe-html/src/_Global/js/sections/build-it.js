window.section_init = function () {

    require(['ui/jquery.ui.widget', 'includes/jquery.json-2.2.min'], function () {
        require({
            paths: {
                'buildit': 'ext/buildit'
            }
        }, [
        'buildit/jquery.honda.match_landing'
        , 'buildit/jquery.honda.match_choose_model'
        , 'buildit/jquery.honda.match_details'
        , 'buildit/jquery.honda.match_accessory_v2'
        , 'buildit/jquery.honda.match_financial'
        , 'buildit/jquery.honda.match_summary'
        , 'buildit/jquery.honda.match_warranty'
        , 'buildit/jquery.honda.match_brochure'
        , 'includes/jquery.format'
    ], function () {
        require(['mcpe-build-it'], function () {

            var hash = null;

            if (window.location.hash) {
                hash = window.location.hash.substring(1).split(',');
            }

            if (hash && hash.length >= 3) {
                $('#build-it').mcpe_build_it({
                    deep_link_category_key: hash[0]
                    , deep_link_trim_level_key: hash[1]
                    , deep_link_trim_url: hash[2]
                    , deep_link_transmission_key: hash[3]
                    , deep_link_model_year_key: hash[4] || null
                    , deep_link_color_key: (hash[5] || '').replace(/^color\/([a-z0-9_-]+)/i, '$1')
                });
            }
            else {
                $('#build-it').mcpe_build_it();
            }
        });
    });
    });

};