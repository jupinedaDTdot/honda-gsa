window.section_init = function () {

	// track gallery links so we can update wallpaper link as well.
	$('a.gallery_link', '#inner_page_content').live('click', function (event) {
		var url = $(this).data('wallpaper-path');
		$('a.download-wallpaper', '#inner_page_content').attr('href', url);
	});

	// sign up form
	$('a[data-ajaxform-name="SignUpForm"]').bind('popupshow', function () {
		$('form').data('CurrentForm', 'SignUpForm');
	}).bind('popuphide', function () {
		$('form').data('CurrentForm', null);
	});

	$('a[property~="act:ajaxload"]', '.tabs_horizontal').bind('ajaxloadbeforeload', function () {
		var div = $('<div>').addClass('loader_container').append(
            $('<div>').addClass('loader_default bg_loader')
        );
		$('.content_box .content').append(div);
	}).bind('ajaxloadload', function () {
		$('.loader_container', '.content_box .content').remove();
	}).bind('ajaxloaderror', function () {
		$('.loader_container', '.content_box .content').remove();
	});

	$('a[property~="act:video-showcase"]', '.product_heading').click(function (event) {
		event.preventDefault();

		var $container = $('[property~="honda:video-showcase"]');
		var $targetContainer = $('<div></div>').addClass('tout-container').appendTo($container.empty());

		$('.hero-image, .viewer-360', '.product_heading').hide();
		$('.viewer-video-showcase', '.product_heading').show();

		optionsVideoShowcase = $.extend({}, global_get('player.video'), optionsVideoShowcase);

		require(['includes/jquery.honda.videoplayer'], function () {
			$targetContainer.videoplayer(optionsVideoShowcase);
		});

	})

	if ($('.image-navigator-view').size() > 0 && $('.image-view', '.image-navigator-view').size() > 1) {
		$('#popup-image .prev, #popup-image .next').show();
		require(['ext/commonlib/jquery.commonlib.carousel'], function () {

			var currentImageIndex = 0;
			$('.image-navigator-view').carousel({
				items: '.content-view'
                , speed: 'fast'
                , interval: 3000
                , navi: '.rotator-controls'
                , circular: true
                , seek: function (event, index) {
                	currentImageIndex = index;
                }
			});

			$('.inner_footer_r a[property~="act:popup"]').bind('popupshow', function() {
				$('img', '.popup-image-content').each(function() {
					var src = $(this).attr('desc').toString();
					$(this).attr('src', src);
				});
			}).bind('popupdidshow', function() {
				$('.image-navigator-view').carousel('stop');

				var api = $('.popup-image-content').data('scrollable');

				if (!api) {
					setTimeout(function() {
						$('.popup-image-content').carousel({
							items: '.popup-rotator-content',
							speed: 'fast',
							initialIndex: currentImageIndex,
							circular: true,
							seek: function(event, index) {
								// on seek, update the thumbnail index as well
								var api = $('.image-navigator-view').data('scrollable');
								if (api && index != currentImageIndex) {
									api.seekTo(index, 0);
								}
							}
						});
					}, 50);
				} else if (api && currentImageIndex != api.getIndex()) {
					// no 'onSeek' event is triggered here in order to prevent recursive calling
					api.seekTo(currentImageIndex, 0, function() {
					});
				}
			}).bind('popupdidclose', function() {
				$('.image-navigator-view').carousel('play');
			});
		});
	}
};

