window.section_init = function (ctx) {
	// overview
	$('a.thumb-swatch', ctx).each(function () {
		var self = $(this);

		self.click(function (event) {
			event.preventDefault();
			self.parent().addClass('selected').siblings().removeClass('selected');
			var img = $(self.data('target')),
				url = self.data('url'),
				target = $('<img>').addClass('target-image-view').css({
					width: 1,
					height: 1
				}).appendTo('body');


			target.attr({
				src: url
			}).ready(function () {
				img.after(target);
				target.css({ width: '', height: '' });
				img.remove();
				target.show();
			});
		});
	});

	// gallery link
	$('a.cbr_gallery_link', ctx).click(function (event) {
		event.preventDefault();
		var self = $(this);
		if (self.hasClass('selected')) {
			$('ul.buttons a', ctx).first().click();
			self.removeClass('selected');
		}
		else {
			$('a.gallery_link', ctx).first().click();
			self.addClass('selected');
		}
	});

	$('.gallery_link', ctx).each(function (index) {
		var self = $(this);
		self.click(function (event) {
			$('.gallery .viewer').data('index', index);
			$('.gallery-buttons').show();
			$('a.cbr_gallery_link', ctx).addClass('selected');
		});
	});

  $('ul.buttons a[property~="honda:360"]', ctx).first().click(function (event) {
		$('.gallery-buttons').hide();
		$('a.cbr_gallery_link', ctx).removeClass('selected');
	});

  $('[property~="honda:360"]', ctx).click(function () {
    var self = $(this)
      , target = self.data('target')
      , params = $.extend({}, global_get('player.360'));

    params.width = self.data('width') || params.width;
    params.height = self.data('height') || params.height;
    params.flashvars.car360 = self.data('url') || params.flashvars.car360;

    $(target).flash(params);
  });

	// prev-next
	$('.gallery-prev').click(function(event) {
		event.preventDefault();
		var total = $('.gallery_link', ctx).size(),
		    index = $('.gallery .viewer').data('index'),
		    new_index = (index + total - 1) % total;
		$('.gallery_link', ctx).eq(new_index).click();
	});
	$('.gallery-next').click(function (event) {
		event.preventDefault();
		var total = $('.gallery_link', ctx).size(),
		    index = $('.gallery .viewer').data('index'),
		    new_index = (index + 1) % total;
		$('.gallery_link', ctx).eq(new_index).click();
	});
	
	// initial set up
	$('ul.buttons a', ctx).first().click();
	
	// twitter
	$('#twtr-widget-1').addClass('twitter').each(function () {
		$("div.twtr-hd", this).addClass('heading').html("<a href='http://www.twitter.com/hondacanada' target='_blank'>@HondaCanada</a>");
		$("div.twtr-ft", this).hide();
		$('.twtr-tweet-text > p > em:last-child', this).hide();
	});

};