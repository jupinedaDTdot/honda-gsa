/* $Id$ */
/*
* scroll down to the bottom to see what options can be sent in.
*/
;(function($){

$.fn.dropdown = function(options) {
	
	// initialization
	function init() {
		/** variables */
		var opts = $.extend(true, {}, $.fn.dropdown.defaults, options);
		// this
		var $this = $(this);
		// get the button and content
		var $btn = $(this);
		var $btnCopy = $btn.clone();
		
		var $content = opts.rel || $($btn.attr('rel')).clone();
		
		if (opts.sibling) {
			// wrap the button with a parent element
			var $parent = $('<span>');
			
			if ($btn.css('display') == 'inline' || $btn.css('display') == 'inline-block') {
				$parent.css('display', 'inline-block');
			}
			else {
				$parent.css('display', 'block');
			}
			$btn.wrap($parent);
			
			var $outer = $btn.parent();
	
			$outer.css({
				position: opts.relation,
				'z-index': opts.btnZIndex
				}).width($btn.outerWidth()).height($btn.outerHeight());
			$btn.css({
				position: 'relative',
				'z-index': opts.btnZIndex
				});
		}
		else if (opts.body) {
			var $outer = $('body');
			$btn.css({
				position: opts.relation,
				'z-index': opts.btnZIndex
				});
		}
		else {
			var $outer = $('#dropdown_outer').size() > 0
				? $('#dropdown_outer') : $('<div>')
					.attr('id', 'dropdown_outer')
					.css({
						position: 'relative',
						'z-index': 99,
						top: 0,
						left: 0,
						width: 0,
						height: 0
					})
					.prependTo('body');
			$btn.css({
				position: opts.relation,
				'z-index': opts.btnZIndex
				});
		}
		
		/** end */

		function oncomplete() {
			$container.css('z-index', opts.containerZIndex);
			opts.oncomplete.call($container.get(0), $btn);
			$btn.trigger('dropdowncomplete', [$container.get(0)]);
		}
		
		function onshow() {
			// offset
			ensureMultipleState();
			
			var offset = _offset();
			$container.css({
				position: 'absolute',
				top: Math.round(offset.top) + 'px',
				left: Math.round(offset.left) + 'px'
			});
			opts.onshow.call($container.get(0));
			$btn.trigger('dropdownshow', [$container.get(0)]);
		}
		
		function onhide() {
			opts.onhide.call($container.get(0));
			$btn.trigger('dropdownhide', [$container.get(0)]);
		}
		
		function ensureMultipleState() {
			if (opts.multiple) {
				return;
			}
			$('.dropdown_container').trigger('do_hidedropdown');
		}
		
		function attachRollover($e) {
			$e.mouseleave(function() {
				$e.trigger('do_hidedropdown');
			});

			// attach events
			$btn.mouseenter(function() {
				onshow();
				$container.show();
			});
		}
		
		function attachClick($e) {
			// stop event propagation in the container
			$e.click(function(event) {
				event.stopPropagation();
				return false;
			});
			$('a', $e).click(function(ev){
				ev.stopPropagation();
			});
			$('.dropdown_button_right .dd_button', $e).click(function(ev) {
				// ev.stopPropagation();
				$e.trigger('do_hidedropdown');
				return false;
			});
			$btn.click(function(event) {
				if ($e.is(':hidden')) {
					onshow();
					$e.show();
				}
				else {
					$e.trigger('do_hidedropdown');
				}
				event.stopPropagation();
				return false;
			});
			$(document).click(function() {
				$e.trigger('do_hidedropdown');
			});
			
		}
		
		function attachCustom($e) {
			// attach onshow and onhide functions to the container
			$e.onshow = onshow;
			$e.onhide = onhide;
			opts.handler($e, $btn);
		}
		
		function _offset() {
			var top, left;
			var margin_y = 0; //$btn.outerHeight(true) - $btn.outerHeight();
			var margin_x = 0; // $btn.outerWidth(true) - $btn.outerWidth();
			var oftop = $btn.offset().top, ofleft = $btn.offset().left;
			var opttop = typeof(opts.top) == 'function' ? opts.top($btn, $container) : opts.top;
			var optleft = typeof(opts.left) == 'function' ? opts.left($btn, $container) : opts.left;

			if (opts.sibling) {
				oftop = 0; ofleft = 0;
				margin_y = $btn.outerHeight(true) - $btn.outerHeight();
				margin_x = $btn.outerWidth(true) - $btn.outerWidth();
			}
			
			// FIXME the margin calculation is not correct. it has issues when the button
			// has both left margin and right margin.
			if (opts.position == 'bottom') {
				top = oftop + opttop + margin_y;
				left = ofleft + optleft + margin_x + ($btn.outerWidth() - $container.outerWidth());
			}
			else { // bottom
				top = oftop + opttop + margin_y;
				left = ofleft + optleft + margin_x;
			}

			return {
				top: top,
				left: left
			};
		}
		
		/** initializations */
		
		// if width is supplied
		if (opts.width > 0) {
			
			var $container = $('<div>').addClass('dropdown_container').addClass(opts.additionalClass)
				.append(
					$('<div>').addClass('dropdown_button pf').append(
						$('<span>').addClass('dropdown_button_right pf').append(
							$btnCopy.css('visibility', 'hidden')
						)
					)
				).append(
					$('<div>').addClass('clrfix')
				).append(
					$('<div>').addClass('dropdown_content pf').append(
						$('<div>').addClass('dropdown_content_top_l pf').append(
							$('<div>').addClass('dropdown_content_top_r pf')
						)
					).append(
						$('<div>').addClass('dropdown_content_mid_l pf').append(
							$('<div>').addClass('dropdown_content_mid_r pf').append(
								$content.children()
							)
						)
					).append(
						$('<div>').addClass('dropdown_content_bottom_l pf').append(
							$('<div>').addClass('dropdown_content_bottom_r pf')
						)
					)
				).append(
					$('<div>').addClass('clrfix')
				)
				.width(opts.width)
				.appendTo($outer).hide();
				// .insertAfter($btn).hide();

			
			// fix width for IE6 & IE7
			if ($.browser.msie && $.browser.version.match(/^(6\.|7\.)/)) {
				width_ = $btn.outerWidth();
				$container.find('.dropdown_button_right').width(width_);
				
				// fix width
				var width_ = opts.width
					- parseInt($container.find('.dropdown_content_mid_l').css('margin-left'))
					- parseInt($container.find('.dropdown_content_mid_l').css('margin-right'))
					- parseInt($container.find('.dropdown_content_mid_l').css('padding-left'))
					- parseInt($container.find('.dropdown_content_mid_l').css('padding-right'))
					- parseInt($container.find('.dropdown_content_mid_r').css('margin-left'))
					- parseInt($container.find('.dropdown_content_mid_r').css('margin-right'))
					- parseInt($container.find('.dropdown_content_mid_r').css('padding-left'))
					- parseInt($container.find('.dropdown_content_mid_r').css('padding-right'));
				
				// set the width
				$container.find('.dropdown_content_mid_r').width(width_);
							
				// top_r and bottom_r width
				width_ = opts.width
					- parseInt($container.find('.dropdown_content_top_l').css('margin-left'))
					- parseInt($container.find('.dropdown_content_top_l').css('margin-right'))
					- parseInt($container.find('.dropdown_content_top_l').css('padding-left'))
					- parseInt($container.find('.dropdown_content_top_l').css('padding-right'))
					- $container.find('.dropdown_content_top_l').width();
				
				$container.find('.dropdown_content_top_r').width(width_);
				
				width_ = opts.width
					- parseInt($container.find('.dropdown_content_bottom_l').css('margin-left'))
					- parseInt($container.find('.dropdown_content_bottom_l').css('margin-right'))
					- parseInt($container.find('.dropdown_content_bottom_l').css('padding-left'))
					- parseInt($container.find('.dropdown_content_bottom_l').css('padding-right'))
					- $container.find('.dropdown_content_bottom_l').width();
				
				$container.find('.dropdown_content_bottom_r').width(width_);
			}
		}
		else {
			// create the container
			var $container = $('<div>').addClass('dropdown_container').addClass(opts.additionalClass)
				.append(
					$('<div>').addClass('dropdown_button').append(
						$('<span>').addClass('dropdown_button_right').append(
							$btnCopy
						)
					)
				).append(
					$('<div>').addClass('dropdown_content').append(
						$('<div>').addClass('dropdown_content_top')
					).append(
						$('<div>').addClass('dropdown_content_mid').append(
							$content.children()
						)
					).append(
						$('<div>').addClass('dropdown_content_bottom')
					)
				)
				.appendTo($outer).hide();
				// .insertAfter($btn).hide();
		}

		if (typeof opts.handler == 'function') {
			attachCustom($container);
		}
		else if (opts.handler == 'touch') {
		}
		else if (opts.handler == 'click') {
			attachClick($container);
		}
		else {
			attachRollover($container);
		}
		
		$(window).resize(function() {
			var offset = _offset();
			$container.css({
				top: Math.round(offset.top) + 'px',
				left: Math.round(offset.left) + 'px'
			});
		});
		
		// the dropdown hide
		$container.bind('do_hidedropdown', function() {
			onhide.apply($container);
			$container.hide();
		});

		// complete
		oncomplete();
		
		// add processed class
		$(this).addClass('dropdown_processed');
		
		if (opts.debug) {
			$container.show();
		}

	}
	
	return this.each(function(index, element) {
		// don't create dropdown menu if it's already processed
		if ($(element).hasClass('dropdown_processed')) {
			return;
		}
		init.call(element);
	});
};

$.fn.dropdown.defaults = {
	additionalClass: '',
	top: 0, // offset related to the button
	left: 0,
	width: 0,
	relation: 'relative',
	rel: false, // dom element or jquery object of the container content element
	position: 'right', // bottom or right
	debug: false, // if set true, the dropdown would keep open
	handler: 'click', // click, rollover, or a function($container, $btn) to handle the events.
					  // the handler does not implement onshow and onhide events.
	multiple: false, // if set false, showing this dropdown would close all others
	btnZIndex: '999',
	containerZIndex: '9',
	sibling: false, // put the dropdown container as the next sbiling of the button (to solve relative position-ed bugs)
					// normally should be false
	body: false, // append to body directly
	// events
	onshow: function() {},
	onhide: function() {},
	oncomplete: function() {}
};

})(jQuery);