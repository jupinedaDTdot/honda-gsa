/**
 * Modal Window
 *
 * @params closeBtnSelector
 * @params file - file location (absolute file path, relative file path or url)
 * @return void
**/
$.modalWindow = function(params) {
	
	var windowWidth 		= $(window).width(), 
		windowHeight 		= $(window).height(), 
		windowScrollTop 	= $(window).scrollTop(),
		fixedPosition		= true,
		$createdElements 	= {};
	
	var $this = this;
	
	this.init = function() {
		
		$this._create();
		
		$createdElements.modalInnerContent.load(params.file, function(data) {
			$('body').prepend($createdElements.background);
			$('body').prepend($createdElements.modalWindow);

			var modalWidth = $createdElements.modalWindow.width();
			var modalHeight = $createdElements.modalWindow.height();

			var modalLeftPosition = (windowWidth / 2) - (modalWidth / 2);
			var modalTopPosition = (fixedPosition == false) ? (windowHeight / 2) - (modalHeight / 2) + windowScrollTop : windowScrollTop ;			
			if ($.browser.msie && $.browser.version.match(/^6\./)) {
				modalTopPosition += document.documentElement.scrollTop;
			}

			$createdElements.modalWindow.css({left:modalLeftPosition, top:modalTopPosition});

			$createdElements.background.fadeIn('slow');
			$createdElements.modalWindow.fadeIn('slow');
			
			var $form = $('form', $createdElements.modalInnerContent);
			
			// the user should be able to close a window via an explicit instruction to the browser
			$(params.closeBtnSelector).click(function() {
				$this.close();
				return false;
			});
			
			// hook the change event for changing provinces
			$('select[name=dealer_province]', $form).change(function() {
				var self = $(this);
				$.ajax({
					type: 'POST',
					url: global_get('url.province_dealers') + self.val(),
					dataType: 'text',
					success: function(data, status, xhr) {
						var options = data.split(/\r?\n/);
						$('select[name=preferred_dealer]', $form).empty();
						
						$.each(options, function(index, line) {
							var value = line.split('=');
							// ignore incorrect lines
							if (value.length < 2) {
								return;
							}
							$('<option>').attr('value', value[0]).text(value[1]).appendTo(
								$('select[name=preferred_dealer]', $form)
							);
						});
					}
				});
			});
			
			// the user feels better that clicking on the overlay would close the modal window.
			$createdElements.background.click(function() {
				$this.close();
				return false;
			});
			
			// the submit button is an anchor, we need to listen to the events and prevent the
			// default behavior
			$('a.submit_btn', $createdElements.modalInnerContent).click(function() {
				$form.submit();
				return false;
			});
			
			// the form's default behavior is disabled because we don't want page refresh in
			// the browser. use AJAX instead.
			$form.submit(function(event) {
				
				var ret = $.form_validate($form);
				if (!ret) {
					return false;
				}
				
				var $self = $(this);
				var $bg = $createdElements.background;
				var $mod = $createdElements.modalInnerContent;
				var $wnd = $createdElements.modalWindow;

				// the user knows we are submitting the form
				$('<div>').addClass('modal_window_loader')
					.css({
						'z-index': 2000,
						'opacity': 0
					})
					.append(
						$('<div>').addClass('modal_window_loader_ani')
					)
					.appendTo($mod).position({
						my: 'center center',
						at: 'center center',
						of: $mod,
						offset: $.browser.mozilla || $.browser.opera ? '0' : '0 ' + $mod.height()
					})
					.animate({
						'opacity': 0.75
					});
				// GET requests are unable to handle a possible long request query string,
				// so use POST instead.
				setTimeout(function() {
					$.ajax({
						type: 'POST',
						url: $self.attr('action'),
						data: $self.serialize(),
						success: function(data, status, xhr) {
							// we put some success indicator here so the user will know
							// the submit process is successful.
							$('.modal_window_loader', $mod).fadeOut('slow', function() {
								$(this).remove();
								$('<div>').addClass('modal_window_loader')
									.css({
										'z-index': 2000,
										'opacity': 0
									})
									.append(
										$('<div>').addClass('modal_window_success')
									)
									.appendTo($mod).position({
										my: 'center center',
										at: 'center center',
										of: $mod,
										offset: $.browser.mozilla || $.browser.opera ? '0' : '0 ' + $mod.height()
									})
									.animate({'opacity': 0.75})
									.delay(800).queue(function() {
										$(this).dequeue();
										$this.close();
									});
									$createdElements.modalWindow.find('.pf').each(function() {
										if (typeof DD_belatedPNG != 'undefined') {
											DD_belatedPNG.fixPng(this);
										}
									});
							});
						},
						error: function(data, status, xhr) {
							// we put some success indicator here so the user will know
							// the submit process fails.
							$('.modal_window_loader', $mod).fadeOut('slow', function() {
								$(this).remove();
								$('<div>').addClass('modal_window_loader')
									.css({
										'z-index': 2000,
										'opacity': 0
									})
									.append(
										$('<div>').addClass('modal_window_fail')
									)
									.appendTo($mod).position({
										my: 'center center',
										at: 'center center',
										of: $mod,
										offset: $.browser.mozilla || $.browser.opera ? '0' : '0 ' + $mod.height()
									})
									.animate({'opacity': 0.75})
									.delay(800)
									.animate({'opacity': 0})
									.queue(function() {
										$(this).remove();
										// TODO put validation information into the form
										
										$(this).dequeue();
									});
							});
						}
					});
					}
					, 800	
				);

				
				return false;
			});

      $('input[name=contact_first_name], input[name=contact_last_name]').keydown(function(e) {
        var code = e.keyCode ? e.keyCode : e.which;
        // allow keys
        if (code >= 65 && code <= 90 // A-Z
            || code == 8 // backspace
            || code == 9 // tab
            || code == 32 // space bar
            || code >= 37 && code <= 40 // arrows
            || code == 46 // delete
            || code == 36 // home
            || code == 35 // end
        ) {
          return;
        }
        e.preventDefault();
        return false;
      });
			
			// dependand on Cufon
			Cufon.refresh();
		});
	};
	
	this._create = function() {
		var clr = $('<div>').addClass('clrfix');

		// setup modal window container
		var modalWindow = $('<div>').attr('id','modal_window').css({display:'none'});

		// setup modal top
		var modalTop = $('<div>').attr('id', 'modal_window_top');
		var modalTopLeft = $('<div>').attr('id', 'modal_window_top_left').addClass('pf');
		var modalTopRight = $('<div>').attr('id', 'modal_window_top_right').addClass('pf');

		// build modal top
		$(modalTop).append(modalTopLeft).append(modalTopRight).append(clr.clone());

		// setup modal content container
		var modalContent = $('<div>').attr('id', 'modal_window_content').addClass('pf');
		var modalInnerContent = $('<div>').attr('id', 'modal_window_inner_content').addClass('pf').appendTo(modalContent);

		// setup modal bottom
		var modalBottom = $('<div>').attr('id', 'modal_window_bottom');
		var modalBottomLeft = $('<div>').attr('id', 'modal_window_bottom_left').addClass('pf');
		var modalBottomRight = $('<div>').attr('id', 'modal_window_bottom_right').addClass('pf');

		// build modal top
		$(modalBottom).append($(modalBottomLeft)).append($(modalBottomRight)).append(clr.clone());

		// build modal window
		$(modalWindow).append($(modalTop)).append($(modalContent)).append($(modalBottom));
		
		var background = $('<div>').css({display:'none',position:'absolute', 'z-index':'1000', width:$(window).width(), height:$(document).height(), left: 0, top: 0, opacity:'0.75', background:'black'}).attr({id:'modal_window_bg'});
		
		$createdElements = {background:$(background), modalWindow:$(modalWindow), modalInnerContent:$(modalInnerContent)};
		
		$(window).resize(function() {
			$(background).css({width:$(window).width(), height:$(document).height()});
			var modalWindowNewLeftPosition = ($(window).width() / 2) - ($createdElements.modalWindow.width() / 2)
			$createdElements.modalWindow.css({left:modalWindowNewLeftPosition});
		});
		
		$(window).scroll(function() {
			if(fixedPosition == false){
				var modalWindowNewTopPosition = ($(window).height() / 2) - ($createdElements.modalWindow.height() / 2) + $(window).scrollTop();
				$createdElements.modalWindow.css({top:modalWindowNewTopPosition});
			}
		});
	};
	
	this.close = function() {
		$createdElements.background.fadeOut('slow', function(){
			$(this).remove();
		});
		$createdElements.modalWindow.fadeOut('slow', function(){
			$(this).remove();
		});
	};
	
	$this.init();
}