// $Id$
;
(function(jQuery, require, Modernizr) {
  var $ = jQuery;

  $.fn.cufonText = function() {
    var text = '';
    $('cufon', this).each(function() {
      text += $(this).attr('alt');
    });

    return text;
  };

  // set the global behavior cases, $.behavior_cases, @see $.behavior_case_defaults for an example
  $.behavior_case_defaults = {
    ready: 'all', // 'all', or regular expression to match behavior names, or an array of behavior names
    ajax: [
        'ajax_selectivizr',
      'edit_mode'
    ],
    inner_page_links: [
      'cufon_refresh',
      'model_gallery',
      'video_player_support',
      'gallery_links',
      'tableslider',
      'slidedown_bars',
      'accessory_tooltips',
      'show_hide_select',
      'inner_page_links',
      'trim_selector_transmission',
      'accessory_trim_selector',
      'warranty_trim_selector',
      'cycle_tout',
      'honda_accessories',
      'honda_warranty',
      'act_scrollview',
      'act_popup',
      'act_popup_video',
      'section_init',
      'page_init',
      'edit_mode',
      'ajax_selectivizr'
    ],
    honda_accessories: [
    // 'show_hide_select',
      'accessory_tooltips',
      'slidedown_bars'
    ],
//    act_popup: [
//                'cufon_refresh'
//                , 'act_fieldselect'
//                , 'act_tooltip'
//            ],
    cufon: /^cufon/
  };

  $.behavior_cases = $.extend(true, { }, $.behavior_case_defaults, $.behavior_cases || { });

  $.browser.mobilesafari = ( /(ipad|iphone|ipod)/ ).test(navigator.userAgent.toLowerCase());

  // set the global timeout for ajax calls here.
  $.ajaxSetup({
    timeout: 10000
  });

  $.flash.expressInstall = '/_Global/swf/playerProductInstall.swf';

  function check_inline_debug_status() {
    var get_param = function(name) {
      var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
      if (!results) {
        return 0;
      }
      return results[1] || 0;
    };
    if (get_param('__inlineDebug') || window.location.hash === '#__inlineDebug') {
      return true;
    }

    return false;
  }

  $._DEBUG = check_inline_debug_status();

  if (typeof console == 'undefined') {
    if ($._DEBUG) {
      // load firebug lite if console doesn't exist
      (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'getfirebug.com/firebug-lite.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
      })();
    } else {
      window.console = {
        log: function() {
        }
      };
    }
  }


  if ($._DEBUG) {
    console.log('JavaScript debugging enabled.');
  }

  $.behaviors = { }

  $._searchTimer = null;

  function url(pathname, options) {
    var o = $.extend(true, { }, url.defaults, options);
    if (o.absolute) {
      return global_get('base_url') + pathname;
    } else {
      return pathname;
    }
  }

  url.defaults = {
    absolute: false
  };

  function try_execute(func, ctx, ajax) {
    var args = Array.prototype.slice.call(arguments);
    args.shift();
    try {
      return func.apply(this, args);
    } catch(e) {
      if ($._DEBUG && typeof(console) != 'undefined') {
        console.log(e);
      }
    }
  }


  $.fn.enable_video_player = function() {
    return this.each(function() {
      var self = $(this);
      var options = $.extend(true, { }, $.fn.linkrel.default_options, global_get('player.standard'));
      // set the width and height according to the container
      var width = self.data('width') || parseInt(self.css('width'));
      var height = self.data('height') || parseInt(self.css('height'));
      if (!self.hasClass('hero') && width > 0 && height > 0) {
        options.width = width;
        options.height = height;
      } else if (!self.hasClass('hero')) {
        options.width = 645; // quick fix
        options.height = 395; // quick fix
      }
      var url = self.attr('title');
      var share_url = window.location.href;

      if (url.match( /\.(flv|mp4|f4v|m4v)$/i )) {
        options.flashvars.xml_location = '';
        options.flashvars.video_location = url;
        if (share_url) {
          options.flashvars.share_link = share_url;
        } else {
          options.flashvars.share_link = document.location.href;
        }
      } else {
        options.flashvars.xml_location = url;
        options.flashvars.video_location = '';
        if (share_url != '') {
          options.flashvars.share_link = share_url;
        } else {
          options.flashvars.share_link = document.location.href;
        }
      }
      if ($.flash.available) {
        self.empty().flash(options);
        self.removeAttr('title');
      } else {
        requires_flash_message(self);
      }
    });
  };

  $.behaviors.careers_page_check = function(ctx, ajax) {
    if (ajax) {
      return;
    }
    if (window.location.hostname == 'honda.ca' && window.location.pathname.match( /^\/(careers|carrieres)/ )) {
      window.location.href = window.location.protocol + '//www.honda.ca' + window.location.pathname;
    }
  };

  $.behaviors.ajax_start = function(ctx) {
    $('html').removeClass('ajax-complete').addClass('ajax-loading');
    if (!$.flash.available) {
      $('body').addClass('no-flash');
    }
  };

  $.behaviors.cufon_refresh = function(ctx) {
    var typefaces = $.get_typefaces();

    $.each(typefaces, function(index, face) {
      if ($(face.elements, ctx).size() > 0) {
        Cufon.refresh(face.elements);
      }
    });
  };

  $.behaviors.pngfix_ie6 = function(ctx, ajax) {
    if (typeof DD_belatedPNG != 'undefined') {
      $('.pf', ctx).each(function() {
        DD_belatedPNG.fixPng(this);
      });
      $('.gmap_result_list .bg, .gmap_result_detail .bg, #feature_area img[src$=.png], .cap_top_default, .cap_bottom_default, .cap_bottom_columns, .wrapper_columns, .wrapper_overlay, .widgets li, #trim_selector .preview img, .featured_search_results img', ctx).each(function() {
        DD_belatedPNG.fixPng(this);
      });
    }
    $('html').addClass('pngfix-complete');
  };

  // tout button multi fix
  $.behaviors.feature_tout_button_multiline_fix = function(ctx, ajax) {
    if (ajax) {
      return;
    }

    $('#feature_area .buttons li a', ctx).each(function() {
      var width = 0;
      $('cufon', this).each(function() {
        width += $(this).outerWidth();
      });
      if (width > $(this).innerWidth() || $(this).text().toString().length > 25) {
        $(this).parents('li').first().addClass('multi');
      }

      $(this).parents('li').first().addClass('cufon-ready');
    });
  };

  /*
  $.behaviors.select_expander = function(ctx) {
  $('.select_expander').each(function() {
  if($.browser.msie && $.browser.version.match(/^(6|7)\./)){
  $(this)
  .mouseover(function() {
  $(this)
  .data('origWidth', $(this).css('width'))
  .width(400);
  })
  .mouseout(function() {
  $(this).width($(this).data('origWidth'));
  })
  .css({'zoom':1, 'z-index':10, 'position':'relative'});
  }else{
  return false;
  }
  });
  };
  */

  $.behaviors.select_file_selector = function(ctx) {
    $('[property~="act:file-selector"]', ctx).each(function() {
      $(this).change(function() {
        if ($(this).val() != '') {
          window.open($(this).val(), 'file_window');
        }
      });
    });
  };

  function update_offers_trim_selector() {
    // get the current trim selector's value
    var value = '';
    try {
      var $trim = $('ul.tabs li a.trim_select.selected', '#trim_selector');
      var trimkey = $trim.attr('name');
      var target = $trim.attr('rel');
      var transkey = $('.transmission a.active, .transmission a.selected', target).attr('rel');
      transkey = transkey.split('|')[1];
      value = trimkey + '|' + transkey;
    } catch(e) {
      console.log(e);
    } finally {
      // update offer's widget
      console.log(value);
      $('select.offer_trim_selector').val(value);
    }

  }

  $.behaviors.trim_selector = function(ctx) {
    var $ctnr = $($('#trim_selector ul.tabs li a.trim_select_default').attr('rel'));
    $('#trim_selector ul.tabs li:not(.special) a').each(function() {
      if ($(this).hasClass('trim_selector_processed')) {
        return;
      }
      $(this).click(function() {
        var $old = $($('#trim_selector ul.tabs li a.selected').attr('rel'));
        var $new = $($(this).attr('rel'));
        // change links
        var oldname = $('#trim_selector ul.tabs li a.selected').attr('name');
        var newname = $(this).attr('name');

        // show hide all competitors
        $('#trim_selector .competitor-group')
          .hide()
          .filter('[rel="' + newname + '"]').show();

        $('#trim_selector .button_group a, #trim_selector .compare_container .col a').each(function() {
          var href = $(this).attr('href');
          var parts = href.split('/');
          var last = parts.pop();
          var reg = new RegExp('^' + oldname, 'g');
          last = last.replace(reg, newname);
          parts.push(last);
          $(this).attr('href', parts.join('/'));
        });

        $('#trim_selector ul.tabs li a.selected').removeClass('selected');
        $(this).addClass('selected');
        $old.hide();
        $new.show();
        $new.find('.scrollbox').scrollview({ delayed: false });

        // change current offers trim_key
        var offers_url = global_get('model.current_offers');
        if (offers_url) {
          offers_url = offers_url.replace('trimkey=' + oldname, 'trimkey=' + newname);
          window._globals['model.current_offers'] = offers_url;
        }
        // update offers widget
        update_offers_trim_selector();

        $.load_current_offers();
      }).addClass('trim_selector_processed');
    }).first().click();
  };

  $.behaviors.offers_trim_selector = function(ctx, ajax) {
    if (ajax) {
      return;
    }
    $('.offer_trim_selector').change(function() {
      var val = $(this).val();
      val = val.split('|');
      // change current offers trim_key
      var offers_url = global_get('model.current_offers');
      offers_url = offers_url.replace( /&?trimkey=[^&]*/ , '&trimkey=' + val[0]);
      offers_url = offers_url.replace( /&?transkey=[^&]*/ , '&transkey=' + val[1]);

      // update trim selector's trim
      $('ul.tabs li:not(.special) a', '#trim_selector').filter('[name=' + val[0] + ']').click();
      // update trim selector's transmission
      $('.transmission a', '#trim_selector').filter('[rel$=' + val[1] + ']').click();

      window._globals['model.current_offers'] = offers_url;
      $.load_current_offers();
    });
  };

  $.behaviors.trim_selector_loader = function(ctx, ajax) {
    if (!ajax && $('#outer_trim_selector_default').size() > 0) {
      $('#trim_selector').appendTo($('#outer_trim_selector').empty());
      $('#outer_trim_selector_default').remove();
      return;
    }

    if (ajax || $('#trim_selector').size() > 0 || $('#outer_trim_selector').size() == 0) {
      return;
    }

    var url = global_get('url.trim_selector');
    var co_model_id = $('#co_model_id').val() || global_get('model.co_model_id');
    $.ajax({
      url: url + '?co_model_id=' + co_model_id,
      dataType: 'html',
      success: function(data, status, xhr) {
        var content = $('#trim_selector', data).hide();
        $('#outer_trim_selector').find('.loader').stop().fadeOut('slow', function() {
          $('#outer_trim_selector').empty().append(content);
          Cufon.refresh();
          $('#trim_selector').stop().fadeIn('slow');
        });
      }
    });
  };

  $.behaviors.trim_selector_images = function(ctx, ajax) {
    $('#trim_selector .preview').each(function() {
      var $image_container = $('.image', this);
      var $image = $('.image img', this).clone();
      var $title = $('.colour_title', this);
      var uri = $image.attr('src');
      $('.colour a:not(.trim_selector_colour_swatch_processed)', this).each(function() {
        var self = $(this).addClass('trim_selector_colour_swatch_processed');
        var rel = self.attr('rel');
        var title = self.attr('title');
        var components = uri.split('/');
        var file = components.pop();
        components.pop(); // colour
        components.push(rel);
        components.push(file);
        self.click(function(event) {
          if (! /\/_Global\// .test(uri)) {
            $image.attr({ src: components.join('/') });
            $image_container.empty().append($image.clone());
            if (typeof DD_belatedPNG != 'undefined') {
              DD_belatedPNG.fixPng($image_container.find('img').get(0));
            }
          }
          $title.text(title);
          Cufon.replace($title, { fontFamily: 'National Semibold' }, true);
          self.parent().addClass('selected').siblings().removeClass('selected');
          event.preventDefault();
        });
      });
    });
  };

  $.behaviors.trim_selector_transmission = function(ctx, ajax) {
    $('#trim_selector .transmission a:not(.trim_selector_transmission_processed)').each(function() {
      var self = $(this).addClass('trim_selector_transmission_processed');
      var $price = self.parents('.price').find('.number');
      var rel = $(this).attr('rel');
      rel = rel.split('|');
      var price = rel[0];
      var transmission_key = rel[1];

      self.click(function() {
        if (self.hasClass('active')) {
          return false;
        }
        $price.text(price).format({ format: global_get('format.price_rounded') });
        self.siblings('a').removeClass('active');
        self.addClass('active');
        Cufon.replace($price, { fontFamily: 'National Semibold' }, true);

        // change current offers transkey
        var offers_url = global_get('model.current_offers');
        if (offers_url) {
          offers_url = offers_url.replace( /transkey=[A-Z0-9-]+/i , 'transkey=' + transmission_key);
          window._globals['model.current_offers'] = offers_url;
          $.load_current_offers();

          // update offers widget
          update_offers_trim_selector();

        }

        return false;
      });
    });
    $('#trim_selector .price').each(function() {
      $('.transmission a', this).removeClass('active');
      $('.transmission a:last', this).addClass('active');
    });
  };

  $.behaviors.act_scrollview = function(ctx, ajax) {
    if ($('[property~="act:scrollview"]', ctx).size() > 0) {
      require(['commonlib/jquery.commonlib.scrollview'], function() {
        $('[property~="act:scrollview"]', ctx).each(function() {
          var elem = this;
          $.jobqueue.add(function() {
            $(elem).scrollview({ delayed: false });
          });
        });
      });
    }
  };

  $.behaviors.act_carousel = function(ctx) {
    if ($('[property~="act:carousel"]').size() > 0) {
      require(['commonlib/jquery.commonlib.carousel'], function() {
        $('[property~="act:carousel"]').each(function() {
          var self = $(this), size = self.data('carouselSize') || 4, circular = self.data('carouselCircular');
          var options = { };
          if (!circular) {
            options = {
              seek: function() {
                if (this.getIndex() >= this.getSize() - size) {
                  $('.next', self).addClass('disabled');
                }
              },
              beforeSeek: function(event, idx) {
                if (this.getIndex() >= this.getSize() - size) {
                  if (idx > this.getIndex()) {
                    return false;
                  }
                }
              }
            };
          }
          self.carousel(options);
        });
      });
    }
  };

  $.behaviors.act_popup_video = function(ctx, ajax) {

    if (!navigator.userAgent.match( /MobileSafari/ ) && $('a[property~="act:popup-video"]').size() > 0) {
      require(['tools/overlay/overlay', 'tools/toolbox/toolbox.expose'], function() {
        $('a[property~="act:popup-video"]').click(function(event) {
          var anchor = this;
          event.preventDefault();
          event.stopPropagation();
          // attach the film
          var div = $('<div>').addClass('popup-video').click(function(event) {
            event.stopPropagation();
          }).prependTo('body');

          div.overlay({
            load: true,
            fixed: false,
            mask: {
              color: '#000',
              opacity: 0.8
            },
            onBeforeLoad: function() {
              $('<div>').appendTo(this.getOverlay()).flash({
                swf: '/_Global/swf/honda_videoPlayer.swf',
                allowFullScreen: 'true',
                allowScriptAccess: 'sameDomain',
                flashvars: {
                  xml_location: (global_get('player.single_suffix') || '') + $(anchor).attr('href'),
                  language: global_get('lang'),
                  disableShare: true
                },
                scale: 'noorder',
                quality: 'best',
                wmode: 'transparent',
                width: 610,
                height: 343,
                play: true
              });
            }
          });
        });


        $(document).click(function() {
          $('.popup-video').remove();
        });
      });
    }

  };

//  $('#like').html('<fb:like href="' + url + '" layout="button_count" show_faces="false" width="65" action="like" font="segoe ui" colorscheme="light" />');
//    if (typeof FB !== 'undefined') {
//        FB.XFBML.parse(document.getElementById('like'));
//    }
  
  

  $.behaviors.act_popup = function (ctx, ajax) {
    var mask = global_get('popup.mask');
    var currentImage = 0;

    if ($('[property~="act:popup"]', ctx).size() > 0) {
        require(['commonlib/jquery.commonlib.popup'], function (e) {
            function isNumber (o) {
              return ! isNaN (o-0) && o != null;
            }
            
//            function getWidthBasedOnResolution()
//            {
//              var width = 0;
//              //alert(screen.height + " " + screen.width);
//              if (screen.height >= 900 && screen.width >= 1152) {
//                width = 0;
//              }
//              else {
//                if (screen.height == 600 && screen.width == 800) {
//                  width = 400;
//                }
//                else {
//                  width = 500;
//                }
//              }
//                return width;
//            }

            function reloadSocialMediaButtons(url, imageUrl)
            {
                var tempUrl;
                //facebook
                $('#facebookDiv').text("");
                $('#facebookDiv').append($("<span>" + facebook + "</span>"));
            
                if (typeof FB !== 'undefined') {
                    $('.fb-like').attr('href',imageUrl); 
                    //$('.fb-like').href = url;
                    FB.XFBML.parse(document.getElementById('facebookDiv'));
                }

                //pinterest
                $('#pinterDiv').text("");
                $('#pinterDiv').append($("<span>" + pinterest + "</span>"));
                tempUrl = $('.pin-it-button').attr('href');
                tempUrl = tempUrl.replace("[URL]", url).replace("[MEDIA]", imageUrl);
                $('.pin-it-button').attr('href',tempUrl); 
                
                //twitter
                var ctrlString = twitter.replace("[qstring]", ($.param({ url: imageUrl }))); 
                $('#twitterDiv').html("");
                $('#twitterDiv').html(ctrlString);
                
                //google plus
                gapi.plusone.render("googleDiv", {"size": "medium", "href": imageUrl});
            }

            
            function toggleImages(i)
            {
                
                $('.imgEnlargedImage').attr('src',imgArray[i]); 
                //if (getWidthBasedOnResolution() > 0)
                //  $('.imgEnlargedImage').attr('width',width); 
                $('.imgCaption p').replaceWith("<p>" + captionArray[i] + "</p>");
                //var newUrl = getCurrentUrl(i);
                var a = ++i;
                
                //determine urls
                var currentUrl = "" + window.location + "";
                var n = currentUrl.lastIndexOf("/");
                var pageUrl, newUrl;
                if (isNumber(currentUrl.substr(n+1)))
                    pageUrl = currentUrl.substr(0, n);
                else
                    pageUrl = currentUrl;
                newUrl = pageUrl + "/" + a;
                
                reloadSocialMediaButtons(currentUrl, newUrl);
            }
            var imagesJsonStr = $('.bigImagesDetails').val();
            //social media
            var facebook = $('.likeBtn').val();
            var pinterest = $('.pinitBtn').val();
            var twitter = $('.twitterBtn').val();
            //var google = $('.googleBtn').val();

            var imgObj = jQuery.parseJSON(imagesJsonStr);
            var imgArray = new Array();
            var captionArray = new Array();
            
            $.each(imgObj, function(i) {
                imgArray[i] = this['url'];
                captionArray[i] = this['text'];
            });
            
            $('.image-popup').click(function() {
                var that = this;
                currentImage = $(that).attr('count');
                toggleImages(currentImage);
            });
            
            $('.previous-btn, .gallery-enlarge-previous').click(function() {
                currentImage == 0 ? currentImage = imgArray.length - 1 : currentImage --;
                toggleImages(currentImage);
            }).bind('mouseover mouseenter', function () {
              $(this).addClass('gallery-enlarge-previous-hover');
            }).bind('mouseleave', function () {
              $(this).removeClass('gallery-enlarge-previous-hover');
            })
            
            $('.next-btn, .gallery-enlarge-next').click(function() {
                currentImage == imgArray.length - 1 ? currentImage = 0 : currentImage ++;
                toggleImages(currentImage);
            }).bind('mouseover mouseenter', function () {
              $(this).addClass('gallery-enlarge-next-hover');
            }).bind('mouseleave', function () {
              $(this).removeClass('gallery-enlarge-next-hover');
            })
            
            $('[property~="act:popup"]', ctx).each(function () {
                var elem = this;
                var thisImage = $('#smallImage').attr('src');
                $(elem).popup({
                    overlay: mask.color,
                    opacity: mask.opacity,
                    complete: function (event, ui) {
                    },
                    show: function (event, ui) {
                        if (ui.call_Notify) ui.call_Notify();
                        $('.gallery-enlarge-primary', ui.frame).show();
                        // update button height
                      setTimeout(function() {
                        var height = $('.img-wrapper', ui.frame).height();
                        $('.gallery-enlarge-next span, .gallery-enlarge-previous span', ui.frame).height(height);
                      }, 500);
                    },
                    hide: function (event, ui) {
                        $('.gallery-enlarge-primary', ui.frame).hide();
                    }
                });
              
            });
        });
    }
};

$.behaviors.act_fieldselect = function (ctx, ajax) {
            if ($('[property~="act:fieldselect"]', ctx).size() > 0) {
                require(['commonlib/jquery.commonlib.fieldselect'], function () {
                    $('[property~="act:fieldselect"]', ctx).fieldselect({
                        complete: function (event, ui) {
                            fixpng(ui.button);
                            var text = $('span', ui.button).html();
                            Cufon.replace(ui.button, { fontFamily: F.FONT_NATIONAL_EXTRABOLD }, true);
                            ui.button.bind('dropdowncomplete', function (ev, dd) {
                                fixpng(dd.frame);
                                $('span', dd.placeholder).html(text);
                                Cufon.replace($('span', dd.placeholder), { fontFamily: F.FONT_NATIONAL_EXTRABOLD }, true);
                            });
                        },
                        change: function (event, ui) {
                            Cufon.replace(ui.button, { fontFamily: F.FONT_NATIONAL_EXTRABOLD }, true);
                            Cufon.replace(ui.placeholder, { fontFamily: F.FONT_NATIONAL_EXTRABOLD }, true);
                        }
                    });
                });
            }
        };

        $.behaviors.act_tooltip = function (ctx, ajax) {
            if ($('[property~="act:tooltip"]', ctx).size() > 0) {
                require(['commonlib/jquery.commonlib.tooltip'], function () {
                    $('[property~="act:tooltip"]', ctx).tooltip({
                        complete: function (event, ui) {
                            fixpng(ui.tooltip);
                        }
                    });
                })
            }
        };
  $.behaviors.act_toutrotator = function (ctx, ajax) {
    if ($('[property~="act:toutrotator"]', ctx).size() > 0) {
      require(['commonlib/jquery.commonlib.toutrotator', 'jquery.swfobject-1.1.1.min'], function() {
        $('[property~="act:toutrotator"]', ctx).each(function() {
          var self = $(this),
              url = self.data('url'),
              width = self.data('width') || 950,
              height = self.data('height') || 400;
          // empty the toutrotator and add the stage and controls
          self.empty().append(
            $('<div>').addClass('toutrotator-stage')
          ).append(
            $('<span>').addClass('toutrotator-controls')
          );

          self.bind('toutrotatorload', function(event, ui) {
            self.data('currentTout', ui.tout);
            if (window.tracker_get) {
              var tracker = tracker_get();
              if (tracker && tracker.trackEvent && ui.tout) tracker.trackEvent('Tout Rotator', 'Load', ui.tout.caption);
            }
          });
          self.bind('toutrotatorplay', function(event, ui) {
            if (window.tracker_get) {
              var tracker = tracker_get();
              if (tracker && tracker.trackEvent && ui.tout) tracker.trackEvent('Tout Rotator', 'Play', ui.tout.caption);
            }
          });
          self.bind('toutrotatorpause', function(event, ui) {
            if (window.tracker_get) {
              var tracker = tracker_get();
              if (tracker && tracker.trackEvent && ui.tout) tracker.trackEvent('Tout Rotator', 'Pause', ui.tout.caption);
            }
          });
          self.toutrotator({
            flashPreloaderPrefix: '/_Global/swf/preloader.swf?autoplay=1&swf=',
            stage: $('.toutrotator-stage', self),
            controller: $('.toutrotator-controls', self),
            width: width,
            height: height,
            touts: function(callback) {
              $.ajax({
                type: 'get',
                url: url,
                dataType: 'xml',
                success: function(data, status) {
                  // construct the array
                  var result = [];
                  $('tout', data).each(function() {
                    var tout = $(this),
                        toutUrl = $('> url', tout).text(),
                        link = $('> link', tout).text(),
                        caption = $('> caption', tout).text(),
                        duration = parseInt(tout.attr('activeTime')) * 1000,
                        type = 'blank',
                        target = /^http:/i .test(toutUrl) ? '_blank' : '',
                        fallback = $('> fallback', tout),
                        standalone = tout.attr('standalone') == 'true',
                        hasFallback = fallback.size() > 0,
                        fallbackTout = { };

                    if (hasFallback) {
                      fallbackTout = {
                        type: 'image',
                        url: $('> url', fallback).text(),
                        link: $('> link', fallback).text(),
                        caption: $('> caption', fallback).text(),
                        duration: parseInt(fallback.attr('activeTime')) * 1000,
                        target: target
                      };
                    }

                    // determine the type
                    if ( /\.(png|jpg|gif)$/i .test(toutUrl)) {
                      type = 'image';
                    } else if ( /\.swf$/i .test(toutUrl)) {
                      type = 'flash';
                    } else {
                      type = 'iframe';
                    }

                    var toutItem = {
                      type: type,
                      url: toutUrl,
                      link: link,
                      caption: caption,
                      standalone: standalone,
                      align: 't',
                      salign: 't',
                      wmode: 'transparent',
                      duration: duration,
                      target: target
                    };

                    if (hasFallback) {
                      toutItem.fallback = fallbackTout;
                    }
                    result.push(toutItem);
                  });

                  callback(null, result);
                },
                error: function(xhr, err) {
                  callback(err, null);
                }
              });
            },
            init: function() {
              $('.toutrotator-progress-inner', this).append($('<span>').addClass('toutrotator-progress-dot'));
            }
          });
          $('.toutrotator-stage', self).bind('click', function(event) {
            if (window.tracker_get) {
              var tout = self.data('currentTout'), tracker = tracker_get();
              if (tracker && tracker.trackEvent && tout) tracker.trackEvent('Tout Rotator', 'Select', tout.caption);
            }
          });
        });
      });
    }
  };
  $.behaviors.cycle_tout = function(ctx, ajax) {
    if ($('[property|="act:cycle-tout"]:not(.cycle-tout-processed)').size() > 0) {
      require(['behaviors/cycle-tout'], function() {
        $('[property|="act:cycle-tout"]').cycle_tout();
      });
    }
  };

  $.behaviors.expandable_list = function(ctx) {
    // expandable menu slide in and out
    $('.expandable_list .question:not(.expandable_list_processed)').each(function() {
      $(this).click(function() {
        if ($(this).parent().hasClass('expanded')) {
          $(this).parent().find('.answer').stop().slideUp('slow', function() {
            $(this).addClass('dn');
            $(this).parent().removeClass('expanded gray_wall');
          });
        } else {
          $(this).parent().addClass('expanded gray_wall');
          $(this).parent().find('.answer').stop().slideDown('slow', function() {
            $(this).removeClass('dn');
          });
        }
      }).hover(function() {
        $(this).toggleClass('hover');
      }, function() {
        $(this).toggleClass('hover');
      }).addClass('expandable_list_processed');
    });
  };


  $.behaviors.book_test_drive = function(ctx) {
    $('.book_a_test_drive:not(.book_test_drive_processed)').each(function() {
      $(this).click(function() {
        var params = {
          file: $(this).attr('href'),
          closeBtnSelector: '#test_drive_close_btn'
        };
        //'modal_window.html'
        // modalWindow(params);
        var modal = $.modalWindow(params);
        // modal.init();
        return false;
      }).addClass('book_test_drive_processed');
    });
  };

  $.behaviors.popup_overlay = function(ctx, ajax) {
    if (ajax) {
      return;
    }
    if ($('a.popup-overlay', ctx).size() > 0)
      require(['tools/overlay/overlay', 'tools/toolbox/toolbox.expose'], function() {
        $('a.popup-overlay', ctx).each(function() {
          var self = $(this);

          self.overlay({
            mask: {
              color: "#000000",
              opacity: 0.75
            },
            closeOnClick: true
          });

          // get the target
          var target = $(self.attr('rel'));
          if (target.size() > 0) {
            require(['form_validate'], function() {
              $('a.submit_btn', target).click(function(event) {
                event.preventDefault();
                try {
                  var $btn = $(this);
                  if ($.form_validate($('form'))) {
                    $('form').append(
                      $('<input>').attr({
                        type: 'hidden',
                        name: 'formName',
                        value: $(this).attr('name')
                      })
                    );
                    var loader = $("<img src='/_Global/img/layout/loader_small.gif' />");
                    $btn.after(loader);
                    $.ajax({
                      global: false,
                      url: $('form').attr('action'),
                      type: $('form').attr('method') ? $('form').attr('method') : 'POST',
                      data: $('form').serialize(),
                      success: function(data, status, xhr) {
                        $btn.siblings('img').remove();
                        $btn.siblings('.failure').hide();
                        $btn.siblings('.success').show().end().delay(1000).queue(function() {
                          $btn.siblings('.success').hide();
                          self.data('overlay').close();
                          $(this).dequeue();
                        });
                      },
                      error: function(xhr, error) {
                        $btn.siblings('img').remove();
                        $btn.siblings('.success').hide();
                        $btn.siblings('.failure').show();
                      }
                    });
                  }

                } catch(e) {
                  console.log(e);
                }
              });
            });
          }
        });
      });
  };

  $.behaviors.model_gallery = function(ctx) {
    // gallery mouse over effects
    $('.thumbnails a', ctx).each(function() {
      if ($(this).hasClass('model_gallery_processed')) {
        return;
      }
      $(this).mouseover(function() {
        // blocking
        if ($(this).data('active')) {
          return;
        }
        $(this).data('active', true);
        $(this).find('img').stop().fadeTo('fast', 0.2);
      }).mouseout(function() {
        var $e = $(this);
        $(this).find('img').stop().fadeTo('fast', 1.0, function() {
          $e.data('active', false);
        });
      }).addClass('model_gallery_processed');
    });
  };

  $.behaviors.gallery_links = function(ctx) {
    // gallery links
    if ($('a.gallery_link:not(.gallery_link_processed)', ctx).size() > 0) {
      var groups = { };
      require(['jquery.jsonrel'], function() {
        var first = true;
        $('a.gallery_link:not(.gallery_link_processed)', ctx).each(function() {
          var self = $(this), group = self.data('group');
          self.jsonrel({
            target: group ? '.gallery .viewer[data-group="' + group + '"]' : '.gallery .viewer',
            group: 'a.gallery_link [data-group="' + group + '"]'
          }).addClass('gallery_link_processed');

          if (group && !groups[group]) {
            groups[group] = true;
            self.click();
            if (first) first = false;
          }
          if (first) {
            self.click();
            first = false;
          }
        });
        // for each group, enable the first item.
      });
    }

  };

  $.behaviors.external_links = function(ctx) {
    // external links
    $('a[rel="external"]:not(.external_link_processed)').each(function() {
      $(this).attr('target', '_blank').removeAttr('rel').addClass('external_link_processed');
    });
  };

  function fixpng(e) {
    if (!$.browser.msie || !$.browser.version.match( /^6\./ ) || typeof DD_belatedPNG == 'undefined') {
      return;
    }
    $('*', e).each(function() {
      var image = $(this).css('background-image').toString();
      var is_image = $(this).attr('src');
      if (image.match( /\.png/ ) || is_image && typeof is_image == 'string' && is_image.match( /\.png/ )) {
        DD_belatedPNG.fixPng(this);
      }
    });
  }

  if (Modernizr && !Modernizr.input['placeholder']) {
    $.behaviors.placeholder = function(ctx) {
      $('[placeholder]:not(.placeholder_processed)').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
          input.removeClass('placeholder');
        }
      }).blur(function() {
        var input = $(this);
        if (input.val() == '') {
          input.addClass('placeholder');
          input.val(input.attr('placeholder'));
        }
      }).blur()
        .addClass('placeholder_processed')
        .parents('form').submit(function() {
          $(this).find('[placeholder]').each(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
              input.val('');
            }
          });
        });
    };
  }

  $.behaviors.form_validation = function(ctx, ajax) {
    if (ajax) {
      return;
    }

    if (location.hostname.match( /honda\.ca$/ ) || location.hostname.match( /acura\.ca$/ )) {
      require(['tools/validator'], function() {
        $.tools.validator.addEffect('none', function() {
        }, function() {
        });

        $('form', ctx).validator({
          effect: 'none',
          inputEvent: 'focus keyup',
          lang: global_get('lang'),
          onBeforeFail: function(event, field) {
            $(field).parents('.input_box').addClass('err_input_box');
          },
          onSuccess: function(event, fields) {
            fields.each(function() {
              $(this).parents('.input_box').removeClass('err_input_box');
            });
          }
        });

        $('[data-type|="submit"][data-role|="button"]', ctx).click(function(event) {
          var api = $('form', ctx).data('validator');
          if (!Modernizr.input['placeholder']) {
            $('[placeholder]', ctx).each(function() {
              var input = $(this);
              if (input.val() == input.attr('placeholder')) {
                input.val('');
              }
            });
          }
          if (!api.checkValidity()) {
            event.preventDefault();
          }
        });
      });
    }
  };

  $.behaviors.accessory_tooltips = function(ctx) {
    if ($('.accessory_tooltip_right:not(.tooltip_processed)', ctx).size() > 0) {
      require(['jquery.tooltip'], function() {
        $('.accessory_tooltip_right:not(.tooltip_processed)', ctx).each(function() {
          var self = $(this), extraClass = self.data('tooltipExtraClass') || '';
          self.tooltip({
            top: self.data('tooltipOffsetTop') || -35,
            left: self.data('tooltipOffsetLeft') || 0,
            position: 'right',
            sibling: $.browser.msie ? false : true,
            additionalClass: 'accessory_tooltip_white ' + extraClass,
            rolloverObject: function() {
              return $(this).closest('li, .award-list-item');
            },
            oncomplete: function($container) {
              fixpng($container);
              self.parents('li')
                .data('tooltip_container', $container);
            }
          }).addClass('tooltip_processed')
            .bind('tooltipshow', function(event, container) {
              var price = $(this).parent().next('.price').text();
              $('.tooltip_price', container).text(price);
            });
        });
      });
    }
  };

  $.behaviors.tooltips_default = function(ctx) {
    // Tooltips
    if ($('.tooltip_right').size() > 0 || $('.compare_tooltip').size() > 0
      || $('.tooltip_button:not(.accessory_tooltip_right)').size() > 0 || $('.tooltip_msrp').size() > 0
        || $('.tooltip_msrp').size() > 0 || $('.offers_tooltip_msrp').size() > 0
          || $('.tooltip_arrow_right_up').size() > 0)
      require(['jquery.tooltip'], function() {
        $('.tooltip_right').tooltip({
          top: -35,
          position: 'right',
          additionalClass: 'compare_result_tooltip',
          oncomplete: function($container) {
            fixpng($container);
          }
        });

        $('.compare_tooltip').tooltip({
          top: -35,
          position: 'right',
          additionalClass: 'compare_result_tooltip',
          oncomplete: function($container) {
            fixpng($container);
          }
        });
        $('.tooltip_button:not(.accessory_tooltip_right)').tooltip({
          additionalClass: 'tooltip_bottom',
          oncomplete: function($container) {
            fixpng($container);
          }
        });

        $('.tooltip_msrp').each(function() {
          var self = $(this);
          self.tooltip({
            position: {
              my: 'right bottom',
              at: 'right top',
              of: self,
              offset: '25 0',
              colision: 'fit'
            },
            additionalClass: 'tooltip_msrp_alt',
            oncomplete: function($container) {
            fixpng($container);
          }
        });
        });

        $('.offers_tooltip_msrp').tooltip({
          top: 17,
          left: -42,
          position: 'right',
          additionalClass: 'tooltip_bottom',
          oncomplete: function($container) {
            fixpng($container);
          }
        });
        $('.tooltip_arrow_right_up').tooltip({
          left: -273,
          additionalClass: 'tooltip_bottom tooltip_vin',
          oncomplete: function($container) {
            fixpng($container);
          }
        });
      });
  };


  $.behaviors.load_current_offers = function(ctx, ajax) {
    if (ajax) {
      return;
    }
    $.load_current_offers = function() {
      if ($('#current_offers_content').size() == 0) {
        return;
      }
      $('#current_offers_content').find('.yes_offers, .no_offers').hide();
      $('#current_offers_content .loading_offers').stop().fadeIn('slow');
      try {
        $.ajax({
        // this is a defined web service endpoint, change it here
          url: global_get('model.current_offers'),
          type: "GET",
          dataType: "text",
          global: false,
          success: function(data) {
            try {
              data = $.parseJSON(data);
              // console.log(data);
            } catch(e) {
              this.error();
              return;
            }
            if (!data) {
              this.error();
              return;
            }
            if (typeof data != 'object' || ($.isArray(data) && data.length == 0)) {
              $('#current_offers_content .loading_offers').stop().fadeOut('slow', function() {
                $('#current_offers_content .no_offers').stop().slideDown('slow');
              });
            } else if (typeof data == 'object' && $.isArray(data.list) && data.list.length == 0) {
              $('#current_offers_content .loading_offers').stop().fadeOut('slow', function() {
                $('#current_offers_content .no_offers')
                  .find('a').attr('href', data.learn_more).end()
                  .stop().slideDown('slow');
              });
            } else if (typeof data == 'object' && $.isArray(data.list)) {
              var $ul = $('<ul>');
              $('#location_offers li:not(.button)').remove();
              $.each(data.list, function(index, value) {
                var $heading = $('<div>').addClass('head fnats').text(value.heading);
                var $rate = $('<div>').addClass('fl rate').text(value.rate);
                var $term = $('<div>').addClass('fl term').text(value.term);
                var $amount = $('<div>').addClass('fl amount').text(value.amount);
                var $period = $('<div>').addClass('fl period').text(value.period);
                var $url = $('<a>').addClass('fr btn_right_arrow').attr({
                  href: value.url,
                  target: '_blank'
                });
                var $clr = $('<div>').addClass('clr');

                var $li = $('<li>').append($heading).append($rate).append($term).append($url).append($amount).append($period).append($clr);
                /*
                <div class="head fnats">LEASE</div>
                <div class="fl rate">0.8%</div>
                <div class="fl term">$0 down and 60 months term</div>
                <a class="fr btn_right_arrow" href="#"></a>
                <div class="fl amount">$225.64</div>
                <div class="fl period">monthly payment</div>
                <div class="clr"></div>
                */
                $('#location_offers li.button').before($li)
                  .find('a').attr('href', data.learn_more);
              });
              $('#location_offers li:first').addClass('first');

              $('#current_offers_content .loading_offers').stop().fadeOut('slow', function() {
                $('#current_offers_content .yes_offers').stop().slideDown('slow');
              });
            }
            var province = $.cookie('hondaprovince');
            $('#current_offers_content p.single strong').text(
              $('a[rel="' + province + '"]:first').text()
            );
            Cufon.replace($('.rate, .amount, .fnats', '#current_offers_content'), { fontFamily: 'National Semibold' }, true);
          },
          error: function() {
            $('#current_offers_content .loading_offers').stop().fadeOut('slow', function() {
              $('#current_offers_content .no_offers').stop().slideDown('slow');
            });
          }
        });
      } catch(e) {
        $('#current_offers_content .loading_offers').stop().fadeOut('slow', function() {
          $('#current_offers_content .no_offers').stop().slideDown('slow');
        });
      }
    };

    if ($('#current_offers_content').size() == 0) {
      return;
    }

    $.load_current_offers();

  };


  $.behaviors.dropdown_select_box = function(ctx) {
    // Drop downs
    // Lazy initialization to alleviate JavaScript load
    $('.current_offers a.select_right.dd_button').one('mouseover', function() {
      var $btn = $(this);

      require(['jquery.dropdown'], function() {
        $btn.dropdown({
          top: -38,
          left: -8,
          additionalClass: 'select_box',
          body: true,
          btnZIndex: 1,
          oncomplete: function() {
            var $container = $(this);
            // hook up to the events
            $('.btn', this).each(function() {
              $(this).addClass('btn_hover').css({ 'z-index': 10 })
                .find('span > *').hide();
              $(this).click(function() {
                return false;
              });

            });
            fixpng(this);
            Cufon.replace($('.btn', this), { fontFamily: 'National Extrabold' }, true);

            $('.dropdown_list a:not(.current_offers_processed)', this).each(function() {
              var province = $(this).attr('rel');
              $(this).click(function() {
                $.cookie('hondaprovince', province, { path: '/', expires: 30 });
                // run ajax
                $.load_current_offers();
                $container.hide();
                $btn.trigger('dropdownhide');
                return false;
              }).addClass('current_offers_processed');
            });
          }
        }).bind('dropdownshow', function() {
          $(this).addClass('btn_hover');
        }).bind("dropdownhide", function() {
          $(this).removeClass('btn_hover');
        });

      });
    });

    // Drop downs
    // Lazy initialization to alleviate JavaScript load
    $('a.select_right.dd_button').one('mouseover', function() {
      var self = $(this);
      require(['jquery.dropdown'], function() {
        self.dropdown({
          top: -38,
          left: -8,
          btnZIndex: 1,
          additionalClass: 'select_box',
          oncomplete: function() {
            $('.btn', this).each(function() {
              $(this).addClass('btn_hover').find('span > *').hide();
              $(this).click(function() {
                return false;
              });
            });
            fixpng(this);
            Cufon.replace($('.btn', this), { fontFamily: 'National Extrabold' }, true);
          }
        }).bind('dropdownshow', function() {
          $(this).addClass('btn_hover');
        }).bind("dropdownhide", function() {
          $(this).removeClass('btn_hover');
        });
      });
    });
  };

  $.behaviors.dropdown_brochure_box = function (ctx, ajax) {
    // Lazy initialization to alleviate JavaScript load
    $('a.btn_brochure.dd_button').one('mouseover', function() {
      var self = $(this);
      require(['jquery.dropdown'], function() {
        self.dropdown({
          relation: 'static',
          body: true,
          top: function () {
            if ($.browser.msie) {
              if (/^7\./.test($.browser.version)) {
                return -14;
              }
              else (/^[89]\./.test($.browser.version))
              {
                return -18;
              }
            }
            return -18;
          },
          left: -10,
          additionalClass: 'brochure_box',
          position: 'right',
          oncomplete: function() {
            $.behaviors.ajax_selectivizr(this, ajax);
          }
        }).bind('dropdownshow', function() {
          $(this).addClass('btn_brochure_hover');
        }).bind("dropdownhide", function() {
          $(this).removeClass('btn_brochure_hover');
        });
      });
    });
  };

  $.behaviors.dropdown_vehicle_box = function(ctx) {
    // Lazy initialization to alleviate JavaScript load
    // dropdowns for vehicle select bottom within a rel
    $('.rel a.select_bottom.dd_button').one('mouseover', function() {
      var self = $(this);
      require(['jquery.dropdown'], function() {
        self.dropdown({
          left: 10,
          top: -8,
          additionalClass: 'vehicle_box vehicle_list',
          position: 'bottom',
          handler: 'click',
          //            body: true,
          sibling: true,
          oncomplete: function() {
            $('.btn', this).each(function() {
              $(this).addClass('btn_hover').find('span > *').hide();
            });
            fixpng(this);
            Cufon.replace($('.dd_button', this), { fontFamily: 'National Extrabold' }, true);
          }
        }).bind('dropdownshow', function() {
          $(this).addClass('btn_hover');
        }).bind("dropdownhide", function() {
          $(this).removeClass('btn_hover');
        });
      });
    });
    // Lazy initialization to alleviate JavaScript load
    // dropdowns for vehicle select bottom
    $('a.select_bottom.dd_button').one('mouseover', function() {
      var self = $(this);
      require(['jquery.dropdown'], function() {
        self.dropdown({
          left: 10,
          top: -8,
          additionalClass: 'vehicle_box vehicle_list',
          position: 'bottom',
          handler: 'click',
          btnZIndex: 5,
          containerZIndex: 3,
          //			body: true,
          sibling: true,
          oncomplete: function() {
            $('.btn', this).each(function() {
              $(this).addClass('btn_hover').find('span > *').hide();
            });
            fixpng(this);
            Cufon.replace($('.btn', this), { fontFamily: 'National Extrabold' }, true);
          }
        }).bind('dropdownshow', function() {
          $(this).addClass('btn_hover');
        }).bind("dropdownhide", function() {
          $(this).removeClass('btn_hover');
        });
      });
    });

    // dropdowns for region bottom within a rel
    $('a.region_bottom.dd_button').one('mouseover', function() {
      var self = $(this);
      require(['jquery.dropdown'], function() {
        self.dropdown({
          left: ($.browser.msie && $.browser.version.match( /^(6|7)\./ )) ? 15 : 10,
          top: -8,
          width: 216,
          additionalClass: 'vehicle_box dealer_region_box',
          position: 'bottom',
          handler: 'click',
          btnZIndex: 5,
          containerZIndex: 3,
          body: true,
          // sibling: true,
          oncomplete: function($btn) {
            $btn.addClass('btn_hover').find('span > *').hide();
            if ($.browser.msie && $.browser.version.match( /^(6|7)\./ )) {
              $('.btn', this).addClass('btn_hover')
                .css({
                  visibility: 'visible',
                  left: $.browser.version.match( /^6\./ ) ? -1 : 0
                })
                .find('span > *').hide();
            }

            fixpng(this);
            //				Cufon.refresh();
            Cufon.replace($('.btn', this), { fontFamily: 'National Extrabold' }, true);
          }
        });
      });
    }).mouseover(function() {
      if ($(this).data('dropdown')) {
        return;
      }
      $(this).addClass('btn_hover');
    }).mouseout(function() {
      if ($(this).data('dropdown')) {
        return;
      }
      $(this).removeClass('btn_hover');
    }).bind('dropdownshow', function() {
      $(this).data('dropdown', true);
      $(this).addClass('btn_hover');
    }).bind("dropdownhide", function() {
      $(this).data('dropdown', false);
      $(this).removeClass('btn_hover');
    });
  };

  function dropdowncomplete($btn) {
    var container = this;
    if ($.browser.msie) {
      // apply selectivizr
      $.behaviors.ajax_selectivizr(container, true);
      // check buttons
      if (/^[678]\./.test($.browser.version)) {
        $('.dropout-content li a', container).click(function(event) {
          event.preventDefault();
          window.location.href = $(this).attr('href');
        }).bind('mouseover mouseenter', function () {
          $(this).addClass('hover');
        }).bind('mouseleave', function () {
          $(this).removeClass('hover');
        })
    }
    }
    }

  function corp_landing_complete($btn) {
    $('.dropdown_button', this).click(function() {
      window.location.href = $btn.attr('href');
    });
    fixpng(this);
    Cufon.replace($('.h6', this), { fontFamily: 'National' });
  }

  $.behaviors.dropdown_search_box = function(ctx) {
    var offset = '-161 -30';
    if ($.browser.mozilla) {
      offset = '-160 -29';
    } else if ($.browser.msie && $.browser.version.match( /^(6|7)\./ )) {
      offset = '-166 -30';
    }
    var data_ = $.extend(true, { }, global_get('search_suggest.data'));
    var query_type = global_get('search_suggest.query_type');

    if ($('#search input').size() > 0)
      require(['ui/jquery.ui.autocomplete'], function() {
        var autocomplete = $('#search input').autocomplete({
          minLength: 3,
          source: function(request, response) {
            var self = this;
            data_.query = request.term;
            if (query_type) {
              if (query_type.type == 'query') {
                data_.query = request.term;
              } else if (query_type.type == 'field') {
                if (data_.partialMetaFields) {
                  data_.partialMetaFields = data_.partialMetaFields + '.' + query_type.field + ':' + request.term;
                } else {
                  data_.partialMetaFields = query_type.field + ':' + request.term;
                }
                data_.query = ''; // just a general term for the gsa to ignore
              }
            }
            var _url = global_get('url.search_link_fallback').url;
            // go to the url
            if (window.location.hostname.match( /^(integration\.)?(www\.|staging\.|preview\.|)(dev\.)?(honda|acura)\.ca/ )) {
            } else {
              _url = url(_url, { absolute: true });
            }
            self._link = {
              copy: global_get('url.search_link_fallback').copy,
              url: _url + escape(request.term)
            };
            delete data_._last;

            $.ajax({
              type: 'POST',
              url: global_get('url.search_suggest'),
              data: data_,
              dataType: 'json',
              global: false,
              success: function(data, status, xhr) {
                self._link.copy = data.link.copy;
                if ('groups' in data && data.groups.length == 0) {
                  data.groups = [{ title: '', value: '!stop' }];
                }
                // manipulate the groups so the empty group goes to the end.
                for (var index = 0; index < data.groups.length; index++) {
                  if (data.groups[index].name == '') {
                    var t = data.groups[index];
                    data.groups.splice(index, 1);
                    data.groups.push(t);
                    break;
                  }
                }
                // if (t) {
                //         }
                response(data.groups);
              },
              error: function(data, status, xhr) {
                response([{ title: '', value: '!stop' }]);
              }
            });
          },
          offset: offset,
          select: function(event, ui) {
            $._searchTimer = true;
            // go to the url
            window.location.href = ui.item.url;
          }
        }).keyup(function() {
          if ($(this).val() != '') {
            $('#search a.clear').removeClass('dn');
          } else {
            $('#search a.clear').addClass('dn');
          }
        }).bind('autocompletesearch', function() {
          $('#search a.clear').addClass('autocomplete-loading');
        }).bind('autocompleteopen', function(event, ui) {
          $('#search a.clear').removeClass('autocomplete-loading');
          var autocomplete = $(this).data('autocomplete');
          // remove ui-menu-item for button
          $('.search-box-button', ui.element).removeClass('ui-menu-item');
          // position it
          var menu = autocomplete.menu;
          menu.element.position({
            my: 'left top',
            of: this,
            at: 'left bottom',
            offset: offset
          });
        }).data('autocomplete');

        $('#search a.clear').click(function() {
          $('#search input').val('').blur();
          $(this).addClass('dn');
          return false;
        });

        $('#search').css({
          position: 'relative',
          'z-index': 900
        });

        autocomplete._renderMenu = function(ul, items) {
          var self = this;
          this.menu.element.addClass('search_box');
          $('<li>').addClass('search-box-cap-top').appendTo(ul);
          try {
            if (items.length > 0 && items[0] && items[0].value != '!stop') {
              $.each(items, function(index, group) {
                var group_title = $('<li>').addClass('search-box-group')
                  .append($('<div>').addClass('h5a').html(group.name));
                if (!group.name && items.length == 1) {
                  group_title.children().removeClass('h5a');
                }
                group_title.appendTo(ul);

                $.each(group.items, function(index, item) {
                  if (item.title) {
                    self._renderItem(ul, item);
                  }
                });
              });
            } else {
              var no_result_copy = global_get('url.search_no_result_copy');
              $('<li>').addClass('ui-menu-item').append($('<div>').text(no_result_copy)).appendTo(ul);
            }
          } catch(e) {
            var no_result_copy = global_get('url.search_no_result_copy');
            $('<li>').addClass('ui-menu-item').append($('<div>').text(no_result_copy)).appendTo(ul);
          }
          $('<li>').addClass('search-box-button').append(
			$('<a>').addClass('btn hand btn_search_results fnateb').append(
              $('<span>').html(self._link.copy)
            ).attr('href', self._link.url)
          ).appendTo(ul);
          $('<li>').addClass('search-box-cap-bottom').appendTo(ul);
          ul.css({
            'z-index': 700
          });
          fixpng(ul);
        };

        // <div class="fl image"><img src="images/placeholder_search_dropdown_image.gif" /></div>
        // <div class="fl content">
        // 	<div class="h6">2010 Civic Hybrid</div>
        // 	<p>Starting MSRP <strong>$38,762</strong></p>
        // </div>
        // <div class="clrfix"></div>
        autocomplete._renderItem = function(ul, item) {
          if (!item) {
            return null;
          }
          return $("<li></li>")
            .data("item.autocomplete", item)
            .append(
				$('<a>').addClass('hand fc').append(
                $('<div>').addClass('fl image').append(
                  $('<img>').attr({
                    src: item.image
                  })
                )
              ).append(
                $('<div>').addClass('fl content').append(
                  $('<div>').addClass('h6').html(item.title)
                ).append(
                  $('<p>').html(item.copy)
                )
              ).append(
                $('<div>').addClass('clrfix')
              )
                .attr('href', item.url)
            )
            .appendTo(ul);
        };
      });


  };

  $.behaviors.enable_search_enter = function(ctx, ajax) {
    $('#search input, .search_input input').bind('keydown', function(e) {
      var self = $(this);
      var code = e.keyCode ? e.keyCode : e.which;
      if (code == 13) {
        e.preventDefault();
        self.trigger('autocompletesearch');
        setTimeout(function() {
          if (!$._searchTimer) {
            var url = global_get('url.search_link_fallback').url + self.val();
            // go to the url
            if (window.location.hostname.match( /^(integration\.)?(www\.|staging\.|preview\.|)(dev\.)?(honda|acura)\.ca/ )) {
              window.location.href = url;
            } else {
              window.location.href = global_get('base_url') + url;
            }
          }
        }, 1000);
      }
    });
  };

  $.behaviors.postal_code_validation = function(ctx, ajax) {
    $('.postal_code input, .find_dealer_container input:first, input.match_postal')
      .keydown(function(e) {
        var self = $(this);
        var code = e.keyCode ? e.keyCode : e.which;
        var val = $(this).val();
        if (code == 13) { // ENTER
          e.preventDefault();
          // special check for find a dealer page
          var self = $(this);
          var button = self.parents('.form_item').siblings('a');
          button = button.size() == 0 ? self.next('a') : button;

          if (button.size() > 0) {
            var onclick = button.attr('onclick');
            if (onclick) {
              window.location.href = button.attr('href');
            }
          } else {
            $('form').submit();
          }
        }
      }).attr({
        autocomplete: 'off'
      });
  };

  $.behaviors.dropdown_menu_box = function(ctx) {
    if ($('#left_nav a[rel]').size() > 0) {
      require(['jquery.dropdown'], function() {
        $('#left_nav a[rel]').dropdown({
          top: -5,
          left: -13 + 217,
          additionalClass: 'menu_box left_nav',
          btnZIndex: 8,
          containerZIndex: 5,
          sibling: false,
          body: true,
          handler: 'rollover',
          oncomplete: function($btn) {
            $('.dropdown_button a', this).addClass('hover').css('visibility', 'visible');
            fixpng(this);
          }
        }).bind('dropdownshow', function() {
          $(this).addClass('hover');
        }).bind('dropdownhide', function() {
          $(this).removeClass('hover');
        });
      });
    }
  };

  var top_menu_dropdown_handler = function($container, $btn) {

    var animating = false;

    $btn.bind($.browser.mobilesafari ? 'touchstart' : 'mouseenter', function(event) {
      if (animating) {
        event.stopPropagation();
        return false;
      }
      animating = true;
      $container.onshow();
      $container.find('.dropdown_content').hide();
      $container.show();
      $container.find('.dropdown_content').stop().slideDown('fast', function() {
        animating = false;
      });
      event.stopPropagation();
      return false;
    });
    var _hide = function() {
      if (animating) {
        setTimeout(_hide, 50);
        return;
      }
      animating = true;
      $container.onhide();
      $container.find('.dropdown_content').stop().slideUp('fast', function() {
        $container.hide();
        animating = false;
      });
    };
    $container.bind('mouseleave', _hide);
    //	$container.find('.dropdown_button').bind($.browser.mobilesafari ? 'touchstart' : 'click', function(event) {
    //		$container.onhide();
    //		$container.find('.dropdown_content').stop().slideUp('fast', function() {
    //			$container.hide();
    //		});
    //		event.stopPropagation();
    //		return false;
    //	});
    //	$container.find('.dropdown_content').bind($.browser.mobilesafari ? 'touchstart' : 'click', function(event) {
    //		// event.stopPropagation();
    //		// return false;
    //	});
    $(document).click(function(event) {
      // if ((!$.browser.msie && event.button != 0) || ($.browser.msie && event.button != 1)) {
      // 	return;
      // }
      if (animating) {
        return;
      }
      animating = true;
      $container.onhide();
      $container.find('.dropdown_content').stop().slideUp('fast', function() {
        $container.hide();
        animating = false;

      });
    });
  };

  var landing_dropdown_handler = function($container, $btn) {
    $btn.mouseenter(function(event) {
      $container.onshow();
      $container.find('.dropdown_content').hide();
      $container.show();
      $container.find('.dropdown_content').slideDown('fast');
    });
    $container.mouseleave(function(event) {
      $container.onhide();
      // $container.find('.dropdown_content').slideUp('fast', function() {
      $container.hide();
      // });
    });
  };

  $.behaviors.dropdown_corporate_landing = function(ctx) {
    if ($('.homepage_menu li a:not(.homepage_menu_processed)').size() > 0)
      require(['jquery.dropdown'], function() {
        $('.homepage_menu li a:not(.homepage_menu_processed)').each(function() {
          var classes = $(this).parent().attr('class') || '';
          classes = classes.replace('fl ', 'corp_landing_');
          $(this).addClass('homepage_menu_processed').dropdown({
            top: 0,
            left: 0,
            additionalClass: 'corp_landing_box ' + classes,
            position: 'right',
            handler: landing_dropdown_handler,
            body: true,
            btnZIndex: '0',
            parentZIndex: '0',
            containerZIndex: '30000',
            oncomplete: corp_landing_complete
          });
        });
      });
  };


  $.behaviors.dropdown_primary_nav_shopping = function(ctx) {
    // dropdown for shopping tools
    var init = function() {
      var self = $(this);
      require(['jquery.dropdown'], function() {
        self.dropdown({
          top: 59,
          left: function($btn, $container) {
            return 578 - $btn.position().left;
          },
          additionalClass: 'shopping_box',
          position: 'right',
          handler: 'rollover',
          body: true,
          btnZIndex: '0',
          parentZIndex: '0',
          containerZIndex: '30000',
          oncomplete: dropdowncomplete
        });
      });
    };

    $('#shopping_dropdown').each(init);
  };

  $.behaviors.dropdown_primary_nav_cars = function(ctx) {
    // dropdown for cars
  
    var init = function () {
      var self = $(this);
      require(['jquery.dropdown'], function() {
        self.dropdown({
          top: 60,
          left: function($btn, $container) {
            return -$btn.position().left - 3;
          },
          additionalClass: 'car_box',
          position: 'right',
          //handler: top_menu_dropdown_handler,
          handler: 'rollover',
          btnZIndex: '0',
          parentZIndex: '0',
          body: false,
          containerZIndex: '30000',
          oncomplete: dropdowncomplete
        });
      });
    };
    
      
    $(".trucks_list_container").hide();
    $(".hybrids_list_container").hide();
    $("#MainMenu_TrucksList_Active").hide();
    $("#MainMenu_HybridsList_Active").hide();

    $('#cars_dropdown').each(init);
  };


  $.behaviors.dropdown_city_autocomplete = function(ctx, ajax) {
    if (ajax || $('.dropdown_autocomplete_city').size() == 0) {
      return;
    }
    // different offsets for firefox/other browsers
    require(['ui/jquery.ui.autocomplete'], function() {
      $('.dropdown_autocomplete_city').each(function() {
        if ($(this).is('.dropdown_autocomplete_city_sidebar')) {
          var offset = '-9 0';
          if ($.browser.mozilla) {
            offset = '-9 0';
          } else if ($.browser.ie && $.browser.version.match( /^(6|7)\./ )) {
            offset = '-9 -2';
          } else if ($.browser.webkit) {
            offset = '-10 0';
          }
        } else {
          var offset = '-7 0';
          if ($.browser.mozilla) {
            offset = '-7 1';
          } else if ($.browser.ie && $.browser.version.match( /^(6|7)\./ )) {
            offset = '-7 -1';
          }
        }
        $(this).autocomplete({
          minLength: global_get('autocomplete.search_city.min_length'),
          source: function(request, response) {
            $.ajax({
              url: global_get('url.search_city') + request.term,
              dataType: 'text',
              global: false,
              success: function(data, status, xhr) {
                var candidates = $.grep(data.split("\n"), function(element, index) {
                  return element.replace( /^\s*(.*?)\s*/ , '$1') != '';
                });

                response(candidates);
              }
            });
          },
          offset: offset,
          additionalClass: $(this).hasClass('dropdown_autocomplete_city_sidebar') ? 'dropdown_dealer_city dropdown_dealer_city_sidebar' : 'dropdown_dealer_city'
        }).bind('autocompleteselect', function(event, ui) {
          var self = $(this);
          var button = self.parents('.form_item').siblings('a');
          button = button.size() == 0 ? self.next('a') : button;

          if (button.size() > 0) {
            setTimeout(function() {
              window.location.href = button.attr('href');
            }, 500);
          }
        });
        var renderMenu = $(this).data('autocomplete')._renderMenu;
        $(this).data('autocomplete')._renderMenu = function(ul, items) {
          if (this.element.hasClass('dropdown_autocomplete_city_sidebar')) {
            this.menu.element.addClass('dropdown_dealer_city dropdown_dealer_city_sidebar');
          }
          else {
            this.menu.element.addClass('dropdown_dealer_city');
          }

          renderMenu.apply(this, [ul, items]);
          fixpng(ul);
        };
      });
    });
  };

  $.behaviors.slidedown_bars = function(ctx) {
    // slide down support
    $('a.slidedown', ctx).each(function() {
      if ($(this).hasClass('slidedown_processed')) {
        return;
      }
      var $btn = $(this);
      var $rel = $($(this).attr('rel'));
      $(this).click(function() {
        if ($btn.is('.selected')) {
          // $rel.slideUp('fast', function() {
          // 	$btn.removeClass('selected');
          // });
          $rel.hide();
          $btn.removeClass('selected');
        } else {
          // $rel.slideDown('fast');
          $rel.show();
          $btn.addClass('selected');
        }
        // notify the window to resize (since the body size has been changed)
        // $(window).resize();
        return false;
      }).addClass('slidedown_processed');
    });
    $('a.btn_collapse, a.btn-collapse').click(function () {
      $('a.slidedown').each(function () {
        var $btn = $(this);
        var $rel = $($(this).attr('rel'));
        if ($btn.is('.selected')) {
          $rel.hide();
          $btn.removeClass('selected');
        }
      });
      // $(window).resize();
      return false;
    });
    $('a.btn_expand, a.btn-expand').click(function () {
      $('a.slidedown').each(function () {
        var $btn = $(this);
        var $rel = $($(this).attr('rel'));
        if ($btn.is(':not(.selected)')) {
          $rel.show();
          $btn.addClass('selected');
        }
      });
      // $(window).resize();
      return false;
    });
  };

  $.behaviors.act_swfobject = function(ctx, ajax) {
    if (!$.fn.flash) {
      return;
    }
    $('[property~="act:swfobject"]', ctx).each(function() {
      var self = $(this);

      if (self.data('actSwfobject')) {
        return;
      }

      self.data('actSwfobject', true);

      if ($.flash.available) {
        self.flash({
          width: self.data('width'),
          height: self.data('height'),
          swf: self.data('url'),
          wmode: 'transparent'
        });
      } else {
        requires_flash_message(self);
      }
    });
  };

  // FELIX - Need to add functionality to load a flash player as soon as the document is ready
  $.behaviors.video_links = function(ctx, ajax) {
    if (!$.fn.flash) {
      return;
    }

    $('a.video_link:not(.video_link_processed)').each(function() {
      var self = $(this);
      self.click(function() {
        var options = $.extend(true, { }, global_get('player.standard'));
        options.flashvars.video_location = self.attr('rel');
        if ($.flash.available) {
          $('.gallery .viewer').empty().flash(options);
        } else {
          requires_flash_message('.gallery .viewer');
        }
        return false;
      });
    }).addClass('video_link_processed');

    $('a.video_link:first').click();
  };

  // VIDEO PLAYER DOCUMENTATION
  // - share functionality by default pulls the current URL you are on
  // - you can programmatically force a URL using this syntax
  /*
  <div title="/path/to/file">
  <a class="share" href="http://www.shareurl.com"></a>
  </div>
  */
  $.behaviors.video_player_support = function(ctx, ajax) {

    var getVideoIdFromHash = function() {
      if (window.location.hash) {
        var matches = /videoId=(\d+)/ .exec(window.location.hash);
        if (matches) {
          return matches[1];
        }
      }
      return false;
    };

    $('.video_player_support:not(.video_player_processed)', ctx).each(function() {
      var self = $(this);
      var options = global_get('player.standard');
        options.autostart = self.data('autostart') === undefined ? options.autostart : self.data('autostart');
      // set the width and height according to the container
      var width = self.width();
      var height = self.height();
      if (width > 0 && height > 0) {
        options.width = width;
        options.height = height;
      }
      var url = self.attr('title');
      var share_url = self.find('a.share').attr('href');

      if (url.match( /\.mp4$/i )) {
        options.xml_location = global_get('player.single_suffix') + url;
      } else {
        options.xml_location = url;
      }

      if (share_url && share_url != '') {
        options.share_url = share_url;
      } else {
        options.share_url = document.location.href;
      }


      options.start_video_idx = getVideoIdFromHash() || 1;

      require(['includes/jquery.honda.videoplayer'], function() {
        self.videoplayer(options).bind('videoplayercreated', function() {
          Cufon.replace($('span.title', self), { fontFamily: 'National Extrabold' }, true);
          Cufon.replace($('.mejs-overlay-share h3', self), { fontFamily: 'National Extrabold' }, true);
          Cufon.replace($('.mejs-overlay-share p', self), { fontFamily: 'National Extrabold' }, true);
        });
      });
    }).addClass('video_player_processed');
  };

  $.behaviors.slideshow_player_support = function(ctx, ajax) {
    if (!$.fn.flash) return;

    if ($('.slideshow_player_support:not(.slideshow_player_processed)', ctx).size() > 0)
      require(['jquery.linkrel'], function() {
        $('.slideshow_player_support:not(.slideshow_player_processed)', ctx).each(function() {
          var self = $(this);
          var options = $.extend(true, { }, $.fn.linkrel.default_options, global_get('player.slideshow'));
          // set the width and height according to the container
          var width = self.width();
          var height = self.height();
          if (width > 0 && height > 0) {
            // options.width = width;
            // options.height = height;
          }
          var url = self.attr('title');
          options.flashvars.xml_location = url;
          if ($.flash.available) {
            self.empty().flash(options);
            self.removeAttr('title');
          } else {
            requires_flash_message(self);
          }
        });
      });
  };

  var __innerPageLoading = false;

  $.load_inner_page_link = function(state) {
    if ($('body').hasClass('inner_page_link_in_progress')) {
      return false;
    }
    $('body').addClass('inner_page_link_in_progress');

    var new_default_loader = function() {
      if ($('#inner_page_content').parent().find('.loader_container').size() > 0) {
        return $('<div>').addClass('loader_container');
      }
      else {
      return $('<div>').addClass('loader_container').css({ 'text-align': 'center', 'margin-top': '30px' }).append(
				  $('<span>').addClass('bg_loader loader_default').width(32).height(32).css({ margin: 'auto', display: 'inline-block' })
			  );
      }
    };
    
    state = state || History.getState();
    if ($.browser.msie) state.msie = true;
    window.__state = state;

        var shouldScroll = true; //$tab.hasClass('inner_page_scroll');
        // replacing $tab with the one inside .tabs_horizontal
        
        if (state.data.scrolling === false) {
          shouldScroll = false;
        }
        

        if (shouldScroll) {
          $('html,body').stop().animate({
            scrollTop: $('.main-col-2').offset().top
          }, 1000);
        }

        // search for this url
        var url = state.url.replace(/^https?:\/\/[^/]+/i, ''),
        allowInnerPageReload = true;
        url = url.replace(/#.*$/, '');
        // allowInnerPageReload |= $('a.inner_page_link[href="' + url + '"]').size() > 0;

    if (allowInnerPageReload && !__innerPageLoading) {
        $('#inner_page_content').hide().queue(function() {
        __innerPageLoading = true;
          var $this = $(this);
          var $page;
          var success = false;
          $this
            .each(function() {
					    $.ajax({
					      global: false,
					      url: url,
                success: function(data, status, xhr) {
					        $page = $('#inner_page_content', data).clone();
					        $page.hide().replaceAll($this);
					        success = true;
					      },
                complete: function(data, status, xhr) {
					        if (!success) {
					          return;
					        }
					        try {
                if (!Modernizr.history) {
                  if (state.data.hash)
                    window.location.hash = state.data.hash;
                  else
                    window.location.hash = '#' + url;
                }
					          execute_behaviors('inner_page_links', '#inner_page_content', true, 2);
                    $.jobqueue.add(function() {
					            $('.loader_container').remove();
					            $page.stop().fadeIn('slow');
					          }, $('#inner_page_content'));
                  } finally {
					      __innerPageLoading = false;
					          $('body').removeClass('inner_page_link_in_progress');
					        }
					      },
                error: function() {
					        // FIXME error handling code here
					        $('.loader_container').remove();
					        $this.stop().fadeIn('slow');
					        $('body').removeClass('inner_page_link_in_progress');
					    __innerPageLoading = false;
					      }
					    });
					  });
          $(this).dequeue();
        })
  			  .parent().append(new_default_loader());
        }
  };
        
  $.behaviors.history_state_handler = function(ctx, ajax) {
    if (ajax) return;

    require(['advanced-history'], function() {
      var History = window.History;
      if (!History.enabled) return false;

      History.Adapter.bind(window, 'statechange', function() {
        $.load_inner_page_link();
    });
    });
  };

  $.behaviors.inner_page_links = function (ctx) {
    // if (typeof DD_belatedPNG != 'undefined') {
    // 	return;
    // }
    require(['advanced-history'], function() {
      var History = window.History;
      if (!History.enabled) return false;

      // inner page loader
      $('a.inner_page_link:not(.inner_page_link_processed)', ctx).each(function () {
        $(this).addClass('inner_page_link_processed').click(function(event) {
          event.preventDefault();
        
          if ($('body').hasClass('inner_page_link_in_progress')) {
            return false;
      }
          var self = $(this);

          var url = $(this).attr('href'), scroll = $(this).data('scrolling');
          if (Modernizr.history) {
          History.pushState({innerPageLink: true, scrolling: scroll}, $('title').text(), url);
          } else {
            $.load_inner_page_link({
              title: $('title').text(),
              url: url,
              data: {
                innerPageLink: true,
                scrolling: scroll
              }
            });
          }

          return false;
      });
    });
    });

  };

//  $.behaviors.load_hash_inner_page_link = function (ctx, ajax) {
//    if (ajax) {
//      return;
//    }
//    // load the page if hash exists
//    if (window.location.hash) {
//      var hash = window.location.hash.substring(1) || '';

//      if (hash.length == 0 || hash[0] != '/') {
//        return;
//      }

//      $('a.inner_page_link[href="' + hash + '"]', 'ul.tabs_horizontal').click();
//    }
//    // jQuery.fn.hashchange.delay = 200;
//    // // listen to hash change
//    // $(window).hashchange(function() {
//    // 	if ($('body').hasClass('inner_page_link_in_progress')) {
//    // 		return;
//    // 	}
//    // 	if (window.location.hash && window.location.hash.match(/^#\//)) {
//    // 		var hash = window.location.hash.substring(1);
//    // 		$('a.inner_page_link[href="'+hash+'"]', 'ul.tabs_horizontal').click();
//    // 	}
//    // });
//  };

  $.behaviors.tableslider = function (ctx) {
    // table slider
    if ($('.tableslider_enabled', ctx).size() > 0)
      require(['jquery.tableslider'], function() {
        $('.tableslider_enabled', ctx).each(function() {
          if ($('tr.first th', this).size() <= 6) {
            $(this).css('width', '100%').parent().width(645);
          } else {
            $(this).one('mouseover', function() {
              $(this).bind('tableslidercomplete', function(event, $container) {
                $container.css({ 'z-index': 1 });
                var height = $container.find('table').innerHeight();
                $container.find('.tableslider_btn_prev').height(height).parent().hide().end().end()
                  .find('.tableslider_btn_next').height(height).parent().hide();
                $(this).parents('.specs_outer_container').mouseover(function() {
                  $container.find('.tableslider_prev').show().end()
                    .find('.tableslider_next').show();
                }).mouseleave(function() {
                  //						$container.find('.tableslider_prev').stop().animate({opacity: 0}).end()
                  //							.find('.tableslider_next').stop().animate({opacity: 0});
                });
              })
                .tableslider({
                  width: 440,
                  outerContainer: $(this).parents('.specs_outer_container')
                });
            });
          }
        });
      });
  };

  $.behaviors.show_hide_select = function(ctx) {
    $('select.show_hide_select', ctx).each(function() {
      if ($(this).hasClass('show_hide_select_processed')) {
        return;
      }
      var $select = $(this);
      $(this).change(function() {
        var target = $(this).val();
        var cls = $(this).attr('name');
        if ($select.hasClass('show_hide_select_scroll')) {
          $('html,body').stop().animate({ scrollTop: $select.offset().top }, 1000);
        }
        $('.' + cls).filter('.selected').hide().removeClass('selected');
        $(target).show().addClass('selected');
      }).addClass('show_hide_select_processed');
    });
    $('a.show_hide_select:not(.show_hide_select_processed)', ctx).each(function() {
      $(this).click(function(event) {
        var target = $(this).attr('href');
        var rel = $(this).attr('rel');
        var old = $('a.show_hide_select')
          .filter('.selected[rel=' + rel + ']').removeClass('selected')
          .parent().removeClass('selected').end()
          .attr('href');
        if (old != '#' && $(old).size() > 0) {
          $(old).hide();
          if (target != '#') {
            $(target).show();
          }
        } else {
          if (target != '#') {
            $(target).show();
          }
        }
        $(this).addClass('selected').parent().addClass('selected');
        cufon_refresh_replace('body');
        //			$.jobqueue.add(function() {
        //				Cufon.refresh();
        //			});
        event.preventDefault();
      }).addClass('show_hide_select_processed');
    });
  };

  $.behaviors.tablesorter = function(ctx, ajax) {
    if ($('#sort_province').size() > 0)
      require(['jquery.tablesorter'], function() {
        $('#sort_province').tablesorter({
          cssAsc: 'asc',
          cssDesc: 'desc',
          cssHeader: 'sort',
          headers: [
            { sorter: false },
            { sorter: 'text' },
            { sorter: false },
            { sorter: false },
            { sorter: 'text' },
            { sorter: 'digit' }
          ],
          textExtraction: 'complex'
        }).bind('sortEnd', function() {
          $(this).find('tbody tr').removeClass().end()
            .find('tbody tr:first').addClass('first').end()
            .find('tbody tr:last').addClass('last bottom');
        });
      });
  };

  $.behaviors.scrollpane = function(ctx, ajax) {
    if ($('.vscrollpane_enabled').size() > 0)
      require(['jquery.scrollpane'], function() {
        $('.vscrollpane_enabled').scrollpane({
          direction: 'vertical'
        });
      });
  };

  $.behaviors.accessory_trim_selector = function(ctx, ajax) {
    var province = $.cookie('hondaprovince') ? $.cookie('hondaprovince') : 'ON';
    $('#accessory_province').val(province);

    $('#accessory_trim:not(.accessory_trim_proccessed)', ctx)
      .addClass('accessory_trim_proccessed')
      .each(function() {
        var self = $(this);
        self.change(function() {
          $('.honda_accessories_enabled').data('honda_accessories').load();
        });
      });

    function update_top5_accessory_price(labour) {
      // update the top 5 vehicle prices
      $('ul.top5 li').each(function() {
        var self = $(this);
        var price;
        try {
          var base_price = self.data('base_price') - 0;
          var values = self.find('.tooltip_button').attr('name');
          values = values.split('|');

          var frt = values[0];
          if (!frt.match( /^\$/ )) {
            frt = frt.replace(',', '.');
            frt = frt.replace( /[^0-9.]/g , '');
          }
          frt = frt - 0;
          var discount = values[1];
          if (!discount.match( /^\$/ )) {
            discount = discount.replace(',', '.');
            discount = discount.replace( /[^0-9.]/g , '');
          }
          discount = discount - 0;

          console.log(labour, discount, frt);

          price = base_price * (100 - discount) / 100 + frt * labour;
          if (isNaN(price)) {
            throw "nan";
          }
          // update the price
          self.find('.price').text(price).format({ format: global_get('format.price') }).show();
          $('.tooltip_price', self).text(price).format({ format: global_get('format.price') });
        } catch(e) {
          price = '';
          // update the price
          self.find('.price').text(price).show();
          $('.tooltip_price', self).text(price);
        } finally {
        }
      });
    }

    $('#accessory_province:not(.accessory_trim_proccessed)', ctx)
      .addClass('accessory_trim_proccessed')
      .each(function() {
        var self = $(this);

        // store the base prices for top 5 accessories
        $('ul.top5 li').each(function() {
          var base_price = $(this).children('.price').text();
          if (base_price.match( /^\$/ )) {
            base_price = $(this).children('.price').parse() - 0;
          } else {
            base_price = $(this).children('.price').text();
            base_price = base_price.replace(',', '.');
            base_price = base_price.replace( /[^0-9.]/g , '');
            base_price = base_price - 0;
          }

          $(this).children('.price').text('');
          $(this).data('base_price', base_price);
        });

        self.change(function() {
          var labour = $('option[value=' + self.val() + ']', self).attr('class') || '0';
          labour = labour.replace(',', '.');
          labour = labour.replace( /[^0-9.]/g , '');
          labour = labour - 0;

          update_top5_accessory_price(labour);
          $('.honda_accessories_enabled').data('honda_accessories').load();
        });
      });
    $.jobqueue.add(function() {
      try {
        var labour = $('option[value=' + province + ']', '#accessory_province').attr('class') || '0';
        labour = labour.replace(',', '.');
        labour = labour.replace( /[^0-9.]/g , '');
        labour = labour - 0;

        if ($('#accessory_province').size() > 0) {
          update_top5_accessory_price(labour);
        }
      } catch(e) {
      }
    }, ajax, 2);
  };


  $.behaviors.honda_accessories = function(ctx, ajax) {
    // will work in ajax

    if ($('.honda_accessories_enabled:not(.honda_accessories_processed)', ctx).size() > 0)
      require(['jquery.honda_accessories'], function() {
        $('.honda_accessories_enabled:not(.honda_accessories_processed)', ctx).each(function() {
          var self = $(this);
          var accessory_index = 1;

          self.addClass('honda_accessories_processed').honda_accessories({
            sections: global_get('honda_accessories.section_titles'),
            url: global_get('url.honda_accessories'),
            color_thumbnail_prefix: global_get('url.honda_accessories.color_thumbnail_prefix'),
            thumbnail_prefix: global_get('url.honda_accessories.thumbnail_prefix'),
            before_send: function(callback) {
              self.stop().fadeOut('slow', function() {
                // self.before($('<div id="accessory_loader">').addClass('bg_loader').width(32).height(32).css('margin', '0 auto 0 auto'));
                callback.apply(this);
              });

              return true;
            },
            error: function(data, status, xhr) {
              self.empty();
              $('#accessory_loader').remove();
              self.stop().fadeIn('slow');
            },
            success: function() {
              execute_behaviors('honda_accessories', '.section-wrapper', true, 2);
            },
            create_section: function(title, items) {
              var selected = global_get('accessory.slidedown_open') ? ' selected' : '';
              var $sec = $('<div>').append(
                $('<div>').addClass('flex_bar').append(
                  $('<span>').append(
                    $('<a>').addClass('slidedown' + selected).attr({
                      href: 'javascript:void(0)',
                      rel: '#accessory_' + title.toLowerCase()
                    }).text(title)
                  )
                )
              );

              var $ul = $('<ul>').addClass('accessory').attr('id', 'accessory_' + title.toLowerCase());
              if ('' === selected) {
                $ul.hide();
              }
              $.each(items, function(index, accessory) {
                $ul.append(accessory);
              });

              $sec.append($ul);

              return $sec.get(0);
            },
            create_accessory: function(item) {
              var $li = $('<li>').append(
                $('<div>').addClass('name').append(
                  $('<a>').addClass('tooltip_button accessory_tooltip_right').attr({
                    rel: '#accessory_item_' + accessory_index
                  }).text(item.name)
                ).append(
                  $('<div>').addClass('tooltip').attr('id', 'accessory_item_' + accessory_index).append(
							$('<div>').addClass('accessory-tooltip').append(
                      $('<img>').attr({
                        alt: item.description_title,
                        src: item.thumbnail
                      })
                    ).append(
                      $('<div>').addClass('title').append(
                        $('<div>').text(item.title)
                      ).append(
                        $('<div>').text(item.price).format({ format: global_get('format.price') })
                      )
                    ).append(
                      $('<p>').addClass('text').text(item.description)
                    )
                  )
                )
              ).append(
                $('<div>').addClass('price').text(item.price).format({ format: global_get('format.price') })
              ).append(
                $('<div>').addClass('clr')
              );

              accessory_index++;

              return $li.get(0);
            },
            append_to_page: function(sections) {
              self.empty();
              $.each(sections, function(index, section) {
                self.append(section);
              });
              self.find('li').hover(function() {
                $(this).addClass('hover');
              }, function() {
                $(this).removeClass('hover');
              });
              $('#accessory_loader').remove();
              self.stop().fadeIn('slow');
            }
          });
        });
      });
  };


  $.behaviors.honda_warranty = function(ctx, ajax) {
    if ($('.honda_warranty_enabled:not(.honda_warranty_processed)', ctx).size() > 0)
      require(['jquery.honda_warranty'], function() {
        $('.honda_warranty_enabled:not(.honda_warranty_processed)', ctx).each(function() {
          var self = $(this);

          self.addClass('honda_warranty_processed').honda_warranty({
            url: global_get('url.honda_warranty'),
            before_send: function(callback) {
              self.stop().animate({ opacity: 0.01 }, 400, function() {
                self.before($('<div id="warranty_loader">').addClass('bg_loader').width(32).height(32).css('margin', '0 auto 0 auto'));
                callback.apply(this);
              });
              return true;
            },
            error: function(data, status, xhr) {
              self.empty();
              $('#warranty_loader').remove();
              self.animate().animate({ opacity: 1 }, 400);
            },
            create_section: function(title, items) {
              var $sec = $('<li>').addClass('fl');
              $('<div>').addClass('flex-bar').append(
                $('<span>').text(title)
              ).appendTo($sec);
              var $tbody = $('<tbody>');
              $.each(items, function(index, element) {
                $tbody.append(element);
              });
              $('<table>').addClass('tbl tbl-warranty').append($tbody).appendTo($sec);

              return $sec.get(0);
            },
            create_item: function(item) {
              var $tr = $('<tr>');
              $tr.append(
                $('<td>').addClass('first odd').append(
                  $('<div>').append(
                    $('<span>').text(item.label)
                  )
                )
              ).append(
                $('<td>').addClass('last even').append(
                  $('<div>').append(
                    $('<span>').text(item.price).format({ format: global_get('format.price') })
                  )
                )
              );

              return $tr.get(0);
            },
            append_to_page: function(sections) {
              self.empty();
              $.each(sections, function(index, section) {
                self.append(section);
              });
              self.find('li:first').addClass('first');
              self.find('tr:first').addClass('first top');
              self.find('tr:last').addClass('last bottom');
              self.append($('<li>').addClass('clr'));
              $('#warranty_loader').remove();
              self.stop().animate({ opacity: 1 }, 400);
            }
          });
        });
      });

  };

  $.behaviors.warranty_trim_selector = function(ctx, ajax) {
    $('#warranty_trim:not(.warranty_trim_processed)', ctx).addClass('warranty_trim_processed').change(function() {
      $('.honda_warranty_enabled').data('honda_warranty').load();
    });
  };

  $.behaviors.build_it_tool = function(ctx, ajax) {
    if ($('#build_it_tool').size() > 0) {
      require(['jquery.honda.build_it', 'jquery.honda.match_landing', 'jquery.honda.match_vehicle', 'jquery.tooltip', 'ui/jquery.ui.slider', 'tools/overlay/overlay', 'tools/toolbox/toolbox.expose'], function() {
        $('#build_it_tool').build_it({
          do_show: function(e) {
            $(e).stop().fadeIn();
          },
          do_hide: function(e) {
            $(e).stop().hide();
          }
        });
      });
    }
  };

  $.behaviors.survey = function(ctx, ajax) {

    if (!global_get('survey.enabled')) {
      return;
    }

    if (ajax) {
      return;
    }

    //    if (location.pathname == '/' || location.pathname == '/french' || location.pathname == '/francais') {
    //        return;
    //    }

    require(['tools/toolbox/toolbox.expose', 'tools/overlay/overlay'], function() {

      $.ajax({
        type: 'GET',
        global: false,
        url: global_get('url.survey_window'),
        success: function(data) {

          var surveyId = $('input[name=SurveyID]');
          //                if (surveyId.size() == 0) {
          //                    return;
          //                }
          surveyId = surveyId.val();


          var enforced = true; // $('input[name=SurveyEnforced]').val();
          //                enforced = enforced.match(/^true$/i) ? true: false;

          var surveyCookieName = "hasTakenSurvey";
          var surveyPageViewCookieName = "takeSurveyPageView";
          var surveyCookieValue = "YES";
          var welcomepop;
          //var surveyLink = "http://limesurvey.stage2.griplimited.com/index.php?sid=74123&newtest=Y&lang=en;";
          var surveyLink = global_get('url.survey') + surveyId;

          window.openSurvey = function() {
            $('#survey').data('overlay').close();
            var windowNow = window.open(surveyLink, "", "location=0,status=0,scrollbars=0,width=725px,height=800px,left=75px,top=75px");
            $.cookie(surveyCookieName, surveyCookieValue, { expires: 30, path: '/' });
          };

          window.openLater = function() {
            $('#survey').data('overlay').close();
            $.cookie(surveyCookieName, surveyCookieValue, { expires: 7, path: '/' });
          };

          var openRandowWelcomeSurvey = function() {
            if ($.cookie(surveyCookieName) == surveyCookieValue) {
              return;
            }

            //return true;
            //alert("GetCookie(surveyCookieName):"+GetCookie(surveyCookieName));

            // setTimeout(openWelcomeSurvey,1);return;

            //Check if user has already done the survey
            if ($.cookie(surveyCookieName) != surveyCookieValue) {
              //Show Survey randomnly , frecuency 1 in 10
              var randomnumber = Math.floor(Math.random() * 10);
              if (randomnumber == 9) {
                setTimeout(openWelcomeSurvey, 1);
              }
            }
          };

          window.openWelcomeSurvey = function() {
            if ($.cookie(surveyCookieName) == surveyCookieValue) {
              return;
            }

            var overlay = $('<div>').attr('id', 'survey').addClass('popup-survey').append(data);
            overlay.hide().appendTo('body');

            cufon_refresh_replace(overlay);
            require(['tools/overlay/overlay', 'tools/toolbox/toolbox.expose'], function() {
              overlay.overlay({
                top: 160,
                fixed: false,
                mask: {
                  color: '#7f7f7f',
                  loadSpeed: 200,
                  opacity: 0.5
                },
                load: true
              });
            });
          };

          var pageView = parseInt($.cookie(surveyPageViewCookieName)) || 0;
          // if pageView is less than three, increase the variable by one and return (we don't need the survey)
          if (pageView < 3) {
            pageView = pageView + 1;
            $.cookie(surveyPageViewCookieName, pageView);
            return;
          }

          if ($.cookie(surveyCookieName) == surveyCookieValue) {
            return;
          }

          if (global_get('survey.whole_site')) {
            if (enforced) {
              openWelcomeSurvey();
            } else {
              openRandowWelcomeSurvey();
            }
          }
        }
      });

    });


  };

  $.behaviors.certified_used_form = function(ctx, ajax) {
    $('#certified_used_all_models').click(function() {
      $(this).parent().parent().find('input[type=checkbox]').attr('checked', $(this).attr('checked'));
    });
    $('#certified_used_all_years').click(function() {
      $(this).parent().parent().parent().find('input[type=checkbox]').attr('checked', $(this).attr('checked'));
    });
  };

  $.behaviors.home_footer_links = function(ctx, ajax) {
    $('a.home_footer_links:not(.home_footer_links_processed)').click(function() {
      var target = $(this).attr('href');
      if ($(target).hasClass('dn')) {
        $(target).removeClass('dn');
        $(this).addClass('selected');
      } else {
        $(target).addClass('dn');
        $(this).removeClass('selected');
      }
      return false;
    }).mousedown(function(e) {
      e.stopPropagation();
      return false;
    }).addClass('home_footer_links_processed');
    var target = $('a.home_footer_links').attr('href');
    if (target && $(target).size() > 0) {
      $(target).mousedown(function(e) {
        e.stopPropagation();
        return false;
      });
      $(document).mousedown(function() {
        var target = $('a.home_footer_links').attr('href');
        if ($(target).hasClass('dn')) {
          return;
        }
        $(target).addClass('dn');
      });
    }
  };

  $.behaviors.set_hero_flash_focus = function(ctx, ajax) {
    if ('hero_flash' in document) {
      document.hero_flash.focus();
    } else if (document.getElementById('hero_flash')) {
      document.getElementById('hero_flash').focus();
    }
  };

  $.behaviors.maintenance_calculator_buttons = function(ctx, ajax) {
    $('a.previous_arrow, a.next_arrow', '.maintenace_calculator_container').each(function() {
      var self = $(this);
      var caption = $('span', this).text();
      var caption_hover = self.attr('title');
      self.removeAttr('title');

      self.hover(function() {
        $('span', this).text(caption_hover);
        Cufon.replace(this, { fontFamily: 'National Extrabold' }, true);
      }, function() {
        $('span', this).text(caption);
        Cufon.replace(this, { fontFamily: 'National Extrabold' }, true);
      });
    });
  };

  $.behaviors.contact_us_form = function(ctx, ajax) {
    if (ajax) {
      return;
    }
    $('.ask_a_question').each(function() {
      var self = $(this);
      $('.owner input', self).click(function() {
        var val = $(this).val();
        console.log(val)
        if (val.toUpperCase() == 'YES') {
          $('.offer_yes', self).show();
        } else {
          $('.offer_yes', self).hide();
        }
      });
    });
    var val = $('.ask_a_question .owner input:checked').val();

    $('.ask_a_question .offer_yes').hide();
    if (val.toUpperCase() == 'YES') {
      $('.ask_a_question .offer_yes').show();
    }
  };

  $.behaviors.future_vehicles = function(ctx, ajax) {
    if ($('.honda_future_vehicles').size() > 0)
      require(['jquery.honda.future_vehicles'], function() {
        $('.honda_future_vehicles').future_vehicles();
      });
  };

  var tracker_inited = false;

  $.behaviors.init_web_tracker = function(ctx, ajax) {
    if (typeof _gat != 'undefined' || ajax || tracker_inited) {
      return;
    }

    var _trackerQueue = [];

    jQuery.UserTracker = {
      schedule: function(callback) {
        var tracker = jQuery.__webTracker;
        if (tracker) {
          callback.apply(tracker);
        } else {
          _trackerQueue.push(callback);
        }
      },
      executeQueue: function(tracker) {
        // run the queue
        for (var idx = 0; idx < _trackerQueue.length; idx++) {
          _trackerQueue[idx].apply(tracker);
        }
        // clear the queue
        _trackerQueue.length = 0;
      }
    };

    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);

  };

  $.behaviors.web_tracker = function(ctx, ajax) {
    if (ajax || tracker_inited) {
      return;
    }
    var run_count = 0;
    tracker_inited = true;

    var add_web_tracker = function() {
      if (typeof _gat == 'undefined' && run_count < 5) {
        run_count++;
        setTimeout(add_web_tracker, 400);
        return;
      }

      // create a new tracker and set the tracker
      var tracker = jQuery.__webTracker = new Honda.WebTracker(null, _gat, global_get('tracker.account'));
      tracker_set(tracker);

      // hook up tracker events
      // O1 - Primary Navigation
      // @see navigation initialization code
      $("#MainMenu_CarsList").click(function () {  
        tracker.trackPrimaryNavigation('Cars');
           $("#MainMenu_CarsList_Active").show();
           $("#MainMenu_TrucksList_Active").hide();
           $("#MainMenu_HybridsList_Active").hide();
           
           $(".cars_list_container").show();
           $(".trucks_list_container").hide();
           $(".hybrids_list_container").hide();
      });
    
    $("#MainMenu_TrucksList").click(function () {  
        tracker.trackPrimaryNavigation('Trucks');
           $("#MainMenu_TrucksList_Active").show();
           $("#MainMenu_HybridsList_Active").hide();           
           $("#MainMenu_CarsList_Active").hide();

           $(".trucks_list_container").show();
           $(".cars_list_container").hide();
           $(".hybrids_list_container").hide();           
      });
    
    $("#MainMenu_HybridsList").click(function () {  
        tracker.trackPrimaryNavigation('Hybrids');
           $("#MainMenu_HybridsList_Active").show();           
           $("#MainMenu_TrucksList_Active").hide();           
           $("#MainMenu_CarsList_Active").hide();
           
           $(".hybrids_list_container").show();
           $(".cars_list_container").hide();
           $(".trucks_list_container").hide();
      });
    
    
    
      $('#cars_dropdown').click(function () {  
       // tracker.trackPrimaryNavigation('Cars');
      });
      $('#trucks_dropdown').click(function () {
       // tracker.trackPrimaryNavigation('Trucks');
      });
      $('#hybrids_dropdown').click(function () {
       // tracker.trackPrimaryNavigation('Hybrids');
      });

      // O2 - Widgets
      // Action: When the user clicks on the following widget buttons:
      // CURRENT OFFER - Click on "Find More Offers" button
      $('#location_offers li.button a.btn').click(function() {
        tracker.trackWidget('Find More Offers', 'Current Offers');
      });
      // HONDA FINANCE - Click on "Learn More" button
      $('ul.widgets li.honda_finance a.btn').click(function() {
        tracker.trackWidget('Learn More', 'Honda Finance');
      });
      // REVIEWS - Click on "Read More Reviews" button
      $('ul.widgets li.review a.btn').click(function() {
        tracker.trackWidget('Read More Reviews', 'Reviews');
      });

      // O3 - File Download
      // Action: When the user downloads a file from the website, attach this behaviour when the button is clicked to download the file
      // Models
      $('ul.widgets a.btn_brochure').bind('dropdowncomplete', function(event, ctr) {
        $('a[href$=".pdf"]', ctr).click(function() {
          var url = $(this).attr('href');
          tracker.trackDownload(url);
        });
      });

      //M1 - All
      // Action: When the user clicks on any of the Tout controls
      $('a.hero_link, a.hero_link_js').click(function () {
        var rel = $(this).attr('rel');
        rel = rel.replace('hero.', '');
        var paths = window.location.pathname.split('/');
        tracker.trackTout(paths[1], rel);
      });

      // M2 - Trim Selector
      // Action: When the user selects a trim level
      $('#trim_selector ul.tabs li:not(.special) a').click(function() {
        var trim = $(this).attr('name');
        tracker.trackTrimLevel(trim);
      });

      // M3 - Trim Selector
      // Action: When the user selects a colour swatch
      $('#trim_selector .colour span.dd a').click(function() {
        var color = $(this).attr('rel');
        tracker.trackTrimColor(color);
      });

      // M5 - Trim Selector
      // Action: When the user clicks on the "Compare" button
      $('#trim_selector ul.buttons a[href^="/compare"]').click(function() {
        tracker.trackTrimCompare();
      });

      // M6 - Book a Test Drive
      // Action: When the user clicks on the "Book a Test Drive" button
      $('ul.widgets li.find_a_dealer a:last').click(function() {
        var paths = window.location.pathname.split('/');
        tracker.trackBookTestDrive(paths[1]);
      });
      // NOTE: When the user completes the Book a Test Drive form and it submits successfully, run this GA JS code (only on success)
      // TODO until we have the book test drive form working


      // M7 - Microsite Tout
      // Action: When the user clicks on the microsite tout in the left sidebar, before the user exits the page
      $('ul.widgets li.first.special a:last').click(function() {
        var paths = window.location.pathname.split('/');
        tracker.trackMicrositeTout(paths[1]);
      });

      // M8 - Interior Page Navigation
      // Case 1: First load with Hash
      if (window.location.hash.match( /^#\// )) {
        var url = window.location.hash.substring(1);
        tracker.trackPage(url);
      }
        // Case 2: First load no Hash
      else {
        tracker.trackPage();
      }

      // Case 3: Click on the navigation
      $('a.inner_page_link').click(function() {
        var url = $(this).attr('href');
        tracker.trackPage(url);
      });

      // case X: special links check
      $('a.trackable').click(function () {
        var url = $(this).attr('href'), title = $(this).attr('title');
        tracker.trackEvent('Trackable Link', title || url);
      });

      $('.dealer_detail.vcard').each(function() {
        var name = $('.fn', this).text();
        // FD4 - Dealer Details
        // Action: When the user clicks on the email (mailto:email-address)
        $('a.email', this).click(function() {
          tracker.trackDealerContactEmail(name);
        });
        // FD5 - Dealer Details
        // Action: When the user clicks to view the dealer's website
        $('a.url', this).click(function() {
          var path = window.location.pathname.split('/'), slug;
          do {
            slug = path.pop();
          } while (slug != '');
          tracker.trackDealerWebsite(slug);
        });
      });

      // execute all tracker queues
      jQuery.UserTracker.executeQueue(tracker);
    };

    require(['includes/tracker'], function () {
      setTimeout(add_web_tracker, 1500);
    });
  };

  $.behaviors.dirty_all_vehicle_links_fix_for_ie6_ie7 = function(ctx, ajax) {
    if (ajax) {
      return;
    }
    if ($.browser.msie && $.browser.version.match( /^(6|7)\./ )) {

      $('.all_models .models_section ul li').click(function() {
        var a = $('a:first', this);
        window.location.href = url(a.attr('href'));
      });

    }

  };

  $.behaviors.external_cuv_find_dealer_widget = function(ctx, ajax) {
    if (ajax) {
      return;
    }

    var domain = window.location.hostname;
    if (domain.match( /^cuv\.honda\.ca$/ )
      || domain.match( /^honda\.strathcom\.com$/ )
        || (!domain.match( /^(preview\.|staging\.|www\.|)honda\.ca$/ ) && !window['__doPostBack'])) {

      window.__doPostBack = function(type, field) {
        var form = jQuery('#fmFindADealer');
        jQuery('input[type=text]:first', form).attr('name', 'postalcode');
        jQuery('input[type=text]:last', form).attr('name', 'city');
        if (type.match( /PostalCode/ )) {
          form.append(
            jQuery("<input>").attr({
              type: 'hidden',
              name: 'type',
              value: 'postalcode'
            })
          );
          form.submit();
        } else if (type.match( /CitySearch/ )) {
          form.append(
            jQuery("<input>").attr({
              type: 'hidden',
              name: 'type',
              value: 'city'
            })
          );
          form.submit();
        }
      };
    }

  };

  $.behaviors.ajax_selectivizr = function(ctx, ajax) {
//    if (ajax && window.$selectivizr) $selectivizr();
  };

  $.behaviors.ajax_complete = function (ctx, ajax) {
    $('html').removeClass('ajax-loading').addClass('ajax-complete');
  };

  $.behaviors.section_init = function(ctx, ajax) {
    if (window['section_init']) window.section_init(ctx, ajax);
  };

  $.behaviors.page_init = function(ctx, ajax) {
    if (window['page_init']) window.page_init(ctx, ajax);
  };

  // $.behaviors.google_maps_demo = function(ctx, ajax) {
  // 	if (ajax || typeof(google) == 'undefined' || !('maps' in google))
  // 		return;
  // 	var latlng = new google.maps.LatLng(-34.397, 150.644);
  // 	var myOptions = {
  // 		zoom: 8,
  // 		center: latlng,
  // 		navigationControl: true,
  // 		navigationControlOptions: {
  // 			style: google.maps.NavigationControlStyle.ZOOM_PAN
  // 		},
  // 		scaleControl: true,
  // 		mapTypeControl: false,
  // 		mapTypeId: google.maps.MapTypeId.ROADMAP
  // 	};
  // 	var map = new google.maps.Map(document.getElementById("gmap"), myOptions);
  // };

  /*
  Behaviors management code
  */

  // override Array.prototype.indexOf if neccessary, for IEs
  if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(elt /*, from*/) {
      var len = this.length >>> 0;

      var from = Number(arguments[1]) || 0;
      from = (from < 0)
        ? Math.ceil(from)
        : Math.floor(from);
      if (from < 0)
        from += len;

      for (; from < len; from++) {
        if (from in this &&
          this[from] === elt)
          return from;
      }
      return -1;
    };
  }


  // attach the behaviors to document ready and ajaxSuccess events
  var execute_behaviors = function(behavior_case, ctx, ajax, time) {
    var e = this;

    var run_single_behavior = function(func) {
      if (time > 0) {
        $.jobqueue.add(function() {
          try_execute(func, ctx, ajax);
        }, e, time);
      } else {
        try_execute(func, ctx, ajax);
      }
    };

    // if ($.behavior_cases[behavior_case]) {
    // 	$.each($.behavior_cases[behavior_case], function(index, behavior) {
    // 		run_single_behavior($.behaviors[behavior]);
    // 	});
    // }
    // else {
    $.each($.behaviors, function(behavior, func) {
      // all behaviors
      if ($.behavior_cases[behavior_case] == 'all') {
        run_single_behavior(func);
      }
        // behavior case is an array and contains the behavior
      else if (typeof($.behavior_cases[behavior_case]) == 'object' && $.behavior_cases[behavior_case].indexOf(behavior) !== -1) {
        run_single_behavior(func);
      }
        // behavior case is a regular expression
      else if (typeof($.behavior_cases[behavior_case]) == 'function' &&
        $.behavior_cases[behavior_case].constructor.toString().match( /regexp/i ) != null) {
        if (behavior.match($.behavior_cases[behavior_case])) {
          run_single_behavior(func);
        }
      }
    });
    // }
  };

  /* open black book page helper function */
  window.openBlackBook = function(language) {
    var url = '/tools/blackbook/blackbook.aspx?siteLanguage=' + language;
    window.open(url, 'mywindow');
    return true;
  };

  /* open test drive function */
  window.openTestDrive = function(modelname_en, language) {
    var url = 'https://www.customerreach.ca/hondacm/scheduler/appointment2.aspx?txt_modelname_en={MODELNAME_EN}&txt_modelname_fr={MODELNAME_EN}&txt_language={LANGUAGE}';
    url = url.replace( /\{MODELNAME_EN\}/g , encodeURI(modelname_en));
    url = url.replace('{LANGUAGE}', encodeURI(language));
    window.open(url, 'mywindow', 'width=748,height=770,scrollbars=1,resizable=1');
    return true;
  };

  window.openFlyer = function(flyerUrl, backTitle, backUrl, target, flyerElement) {
    require(['includes/flyer'], function() {
      var flyer = new WebFlyer(flyerUrl, backTitle, backUrl, target, flyerElement);
      flyer.open();
    });
  };

  $(document).ready(function() {
    execute_behaviors('ready', document, false, 2);
  });

})(jQuery, window['require'] || function () { }, window['Modernizr']);



 

