(function ($, F) {

	// CONSTANTS
	F.FONT_FOUNDERS_CONDENSED = 'Founders Grotesk Condensed';
	F.FONT_NATIONAL = 'National';
	F.FONT_NATIONAL_SEMIBOLD = 'National Semibold';
	F.FONT_NATIONAL_EXTRABOLD = 'National Extrabold';

	window.global_get = function (key) {
		if (!('_globals' in window)) {
			return null;
		}
		else if (!(key in window._globals)) {
			return null;
		}
		return window._globals[key];
	};

	window.cufon_refresh_replace = function (ctx) {
		var typefaces = $.get_typefaces();

		$.each(typefaces, function (index, face) {
			if ($(face.elements, ctx).size() > 0) {
				Cufon.refresh(face.elements);
			}
		});
	};

	window.requires_flash_message = function (context) {
		var noscript = $('.flash_disabled', context);
		if (noscript.size() > 0) {
			noscript.show();
		}
		else {
			if (global_get('lang') == 'fr') {
				$(context).html('<div class="flash_disabled"><div class="flash_logo"><p class="message"><span class="fnat">Vous devez mettre à jour Adobe Flash Player pour voir ce contenu.</span></p><p><a class="btn primary" href="http://get.adobe.com/flashplayer/" rel="external" target="_blank"><span>Téléchargez-le sur le site d’Adobe</span></a></p></div></div>');
			}
			else {
				$(context).html('<div class="flash_disabled"><div class="flash_logo"><p class="message"><span class="fnat">You need to upgrade your Adobe Flash Player to view this content.</span></p><p><a class="btn primary" href="http://get.adobe.com/flashplayer/" rel="external" target="_blank"><span>Download it from Adobe</span></a></p></div></div>');
			}
			$('.flash_disabled', context).show();
			cufon_refresh_replace(context);
		}
	};



	function main() {
		// disable Cufon for Googlebot, IE 6 & 7
		// if (navigator.userAgent.match(/Googlebot\/[23456789]\./) || $.browser.msie && /^[67]\./.test($.browser.version)) {
		if (typeof Cufon === 'undefined') {
			window.Cufon = window.Cufon || {
        replace: function () { return Cufon; },
        refresh: function () { return Cufon; },
        now: function () { return Cufon; },
        set: function () { return Cufon; }
			};
			Cufon.replace = function () { return Cufon; };
		}


		// don't process for the second time
		if ($('body').hasClass('cufon_processed')) {
			return;
		}

		if ($.browser.msie && $.browser.version.match(/^9\./)) {
			Cufon.set('engine', 'canvas');
		}

		$.get_typefaces = function () {
			return [
			{
			  elements: '#feature_area .hero_menu .hero_menu_info .video_showcase .hero_link_js_trigger, #language li a, #feature_area .buttons > li > a, #feature_area .buttons_expanded > li a.future_hero_link, h1:not(.head_compare_links), .h1, .h4, h5, .h5, h2.detail, .find_dealer_subheading,  .tabs_horizontal li a.selected, #book_test_drive .h1, .editor .h3, .find_dealer_subheading, h2.top5, .model_specs .h2',
				options: { fontFamily: 'National', hover: true }
			},
			{
				elements: '.input_special a, .input_box a, .alert-box .alert-nav a',
				options: { fontFamily: 'National Extrabold' }
			},
			{
				elements: '.search_options li:not(.search_sub_header) label, #trim_selector .content .preview .colour_title , #trim_selector .price .disclaimer, #trim_selector .preview .colour dt, #trim_selector .number, .offers .rate, .offers .amount, #honda-indy-content .honda-indy-nav ul li a, #honda-indy-touts .wide_three_column li a, #honda-indy-content .honda-indy-content .page-coming-soon, #honda-indy-content .honda-indy-content .tickets-price dl, .honda-indy #honda-indy-content .honda-indy-content .honda-indy-schedule th, .honda-indy #honda-indy-content .honda-indy-content .honda-indy-schedule td',
				options: { fontFamily: 'National Semibold' }
			},
			{
        elements: '.fnatb',
        options: { fontFamily: FONT_NATIONAL, fontWeight: 'bold', hover: true }
      },
			{
				elements: '.fnat',
				options: { fontFamily: FONT_NATIONAL, hover: true }
			},
			{
				elements: '.fnats',
				options: { fontFamily: FONT_NATIONAL_SEMIBOLD, hover: true }
		  },
			  {
			    elements: '.fantx',
			    options: { fontFamily: FONT_NATIONAL_EXTRABOLD, hover: true }
			  },
			  {
			    elements: '.ffgc',
			    options: { fontFamily: FONT_FOUNDERS_CONDENSED, hover: true }
			  },
			{
				elements: 'a.btn_brochure, a.btn_download_pdf',
				options: { fontFamily: 'National', hover: true }
			},
			{
				elements: '#book_test_drive .h1, .editor .h3, .editor h2, #match_landing h3',
				options: { fontFamily: 'National' }
			},
			{
				elements: 'a.btn:not(.primary_nav_dd, .btn_brochure_action)',
				options: { fontFamily: 'National Extrabold' }
			},
			{
				elements: '.tabs_horizontal li > a:not(.selected)',
				options: {
					fontFamily: 'National',
					hover: true
				}
			},
			{
				elements: '.tabs_horizontal .dd a',
				options: {
					fontFamily: 'National',
					hover: true
				}
			},
			{
				elements: '#secondary_nav a',
				options: {
					fontFamily: 'National Extrabold',
					hover: true
				}
			},
			{
				elements: '.hero_menu ul:not(.operations) a, .hero_menu_single ul a',
				options: {
					fontFamily: 'National',
					hover: true
				}
			},
			{
				elements: '#blue_jays_events .h2',
				options: {
					fontFamily: 'National'
				}
			}
		];
		};

		// Cufons
		Cufon.now();

		var typefaces = $.get_typefaces();
    for (var index = 0; index < typefaces.length; index++) {
      if (window.__boldOnly) {
        if (typefaces[index].options.fontFamily == FONT_NATIONAL && typefaces[index].options.fontWeight == 'bold') {
          Cufon.replace(typefaces[index].elements, typefaces[index].options);
        }
      }
      else {
        Cufon.replace(typefaces[index].elements, typefaces[index].options);
      }
    }

		$('body').addClass('cufon_processed');
	}

	require(['modernizr-1.6'], main);
})(jQuery, window);
