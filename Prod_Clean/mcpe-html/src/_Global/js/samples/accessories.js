{
  "keys": [
    "packages",
    "entertainment",
    "exterior",
    "interior"
  ],
  "packages": {
    "keys": [
      "51366_protection_package",
      "51940_honda_ilinc_kit",
      "51365_aero_kit_package"
    ],
    "51366_protection_package": {
      "name": "Protection Package",
      "price": 379.04,
      "residual": null,
      "description_title": "Protection Package",
      "description_body": "We have combined cargo trays, all-weather black floor mats and splashguards into one convenient package with a 10% discount.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "protection_package_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    },
    "51940_honda_ilinc_kit": {
      "name": "Honda iLinc Kit",
      "price": 382.07,
      "residual": null,
      "description_title": "Honda iLinc Kit",
      "description_body": "Play Music from your iPod from your Audio System while using the vehicle's controls. Also charges your iPod while in use.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": null
      },
      "excludes": null,
      "includes": null
    },
    "51365_aero_kit_package": {
      "name": "Aero Kit Package",
      "price": 1834.42,
      "residual": null,
      "description_title": "Aero Kit Package",
      "description_body": "Make the drive even more exciting with the addition of our Aero Kit that includes the Front, Rear and Side Skirts in a convienent package.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "aero_kit_package_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51393_front_skirt",
          "51394_rear_skirt",
          "51395_side_skirt"
        ]
      },
      "includes": null
    }
  },
  "entertainment": {
    "keys": [
      "51370_cd_changer___8_disc",
      "51367_civic_i_linc_kit",
      "51369_bass_works_system",
      "51371_cd_changer_magazine___8_disc"
    ],
    "51370_cd_changer___8_disc": {
      "name": "CD Changer - 8 Disc",
      "price": 912.69,
      "description_title": "CD Changer - 8 Disc",
      "description_body": "Load up to 8 discs of your favourite music, then sit back and enjoy hours of uninterrupted driving and listening pleasure. Includes Attachment.",
      "assets": {
        "thumbnail": "cd_changer___8_disc_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51367_civic_i_linc_kit": {
      "name": "Civic i-Linc Kit",
      "price": 358.12,
      "description_title": "Civic i-Linc Kit",
      "description_body": "Designed specifically for Honda vehicles, the Honda i-Linc allows you to play music from your Apple iPod directly from your vehicle’s audio system with CD quality results.  You can use your vehicle’s audio control to play your music, and the i-Linc charges your iPod while in use.",
      "assets": {
        "thumbnail": "civic_i_linc_kit_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51369_bass_works_system": {
      "name": "Bass Works System",
      "price": 548.72,
      "description_title": "Bass Works System",
      "description_body": "Enhance the sound of your premium audio kit with the Bass Works System for your Civic. Includes Attachment",
      "assets": {
        "thumbnail": "bass_works_system_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51371_cd_changer_magazine___8_disc": {
      "name": "CD Changer Magazine - 8 Disc",
      "price": 65.71,
      "description_title": "CD Changer Magazine - 8 Disc",
      "description_body": "An additional magazine for your 8 Disc CD Changer.",
      "assets": {
        "thumbnail": "cd_changer_magazine___8_disc_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    }
  },
  "exterior": {
    "keys": [
      "51387_fog_lights",
      "51392_rear_splash_guards",
      "51385_car_cover",
      "51388_full_nose_mask",
      "51391_rear_bumper_applique",
      "51393_front_skirt",
      "51395_side_skirt",
      "51394_rear_skirt",
      "51386_engine_block_heater",
      "51389_hood_edge_deflector"
    ],
    "51387_fog_lights": {
      "name": "Fog Lights",
      "price": 677.4,
      "residual": null,
      "description_title": "Fog Lights",
      "description_body": "For even more admiring looks, the addition of Fog Lights will complement the driving fun of your Civic, while helping to increase visibility during adverse driving conditions.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "fog_lights_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "51392_rear_splash_guards": {
      "name": "Rear Splash Guards",
      "price": 96.94,
      "residual": null,
      "description_title": "Rear Splash Guards",
      "description_body": "A custom fit for your Honda, splash guards made from heavy duty, injection-moulded polymers help protect the rear quarter panels.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "rear_splash_guards_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    },
    "51385_car_cover": {
      "name": "Car Cover",
      "price": 236.84,
      "residual": null,
      "description_title": "Car Cover",
      "description_body": "Imprinted with the Civic logo, this soft and breathable car cover helps protect the finish of your Civic from dust and debris.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "car_cover_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "51388_full_nose_mask": {
      "name": "Full Nose Mask",
      "price": 218.44,
      "residual": null,
      "description_title": "Full Nose Mask",
      "description_body": "Dress up your Honda Complement the sportiness of your Civic with this custom-fitted Full Nose Mask, while helping to protect its front end from small stones and road debris.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "full_nose_mask_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    },
    "51391_rear_bumper_applique": {
      "name": "Rear Bumper Applique",
      "price": 96.08,
      "residual": null,
      "description_title": "Rear Bumper Applique",
      "description_body": "Urethane film protects the rear bumper finish while loading and unloading. The UV resistant clear coat protects the rear bumper from scratches and scrapes when loading your items in your vehicle. Cannot be installed with Rear Skirt.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "rear_bumper_applique_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    },
    "51393_front_skirt": {
      "name": "Front Skirt",
      "price": 555.87,
      "residual": null,
      "description_title": "Front Skirt",
      "description_body": "Further define the aerodynamic styling of your Civic with the addition of a Front Skirt.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "front_skirt_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    },
    "51395_side_skirt": {
      "name": "Side Skirt",
      "price": 848.68,
      "residual": null,
      "description_title": "Side Skirt",
      "description_body": "Make the drive even more exciting with the addition of Front, Rear and Side Skirts while adding to the aerodynamic styling of your Civic.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "side_skirt_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "51394_rear_skirt": {
      "name": "Rear Skirt",
      "price": 616.81,
      "residual": null,
      "description_title": "Rear Skirt",
      "description_body": "Rear Skirts further define the aerodynamic styling of your Civic Coupe.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "rear_skirt_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    },
    "51386_engine_block_heater": {
      "name": "Engine Block Heater",
      "price": 195.99,
      "residual": 225.0,
      "description_title": "Engine Block Heater",
      "description_body": "Don't let the cold Canadian winters stop you from enjoying your new car. The addition of the Engine Block Heater will help ensure a smooth start even on the coldest days. Includes Engine Block Heater Bracket Kit.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "engine_block_heater_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "51389_hood_edge_deflector": {
      "name": "Hood Edge Deflector",
      "price": 162.44,
      "residual": null,
      "description_title": "Hood Edge Deflector",
      "description_body": "Protect your new Honda's fine finish from small stones, road debris, and flying insects with a hood edge protector.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "hood_edge_deflector_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    }
  },
  "interior": {
    "keys": [
      "51396_security_system_with_attachment",
      "51382_leather_steering_wheel_cover",
      "51383_trunk_edge___bumper_protector",
      "51378_floor_liner",
      "51374_auto_day_night_mirror_with__compass",
      "51375_cargo_hook",
      "51381_interior_trim_kit___carbon_fibre_look",
      "51376_cargo_net",
      "51372_all_weather_floor_mats___black",
      "51379_interior_illumination___blue",
      "51377_cigarette_lighter",
      "51380_interior_trim_kit___silver",
      "51384_trunk_tray",
      "51373_ashtray___cup_holder_style"
    ],
    "51396_security_system_with_attachment": {
      "name": "Security System with Attachment",
      "price": 429.13,
      "description_title": "Security System with Attachment",
      "description_body": "This Security System is designed to protect your Civic from intruders, giving you that extra peace of mind. Includes attachment. Key FOB not included and not required",
      "assets": {
        "thumbnail": "security_system_with_attachment_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51382_leather_steering_wheel_cover": {
      "name": "Leather Steering Wheel Cover",
      "price": 185.32,
      "description_title": "Leather Steering Wheel Cover",
      "description_body": "",
      "assets": {
        "thumbnail": "leather_steering_wheel_cover_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51383_trunk_edge___bumper_protector": {
      "name": "Trunk Edge & Bumper Protector",
      "price": 67.38,
      "description_title": "Trunk Edge & Bumper Protector",
      "description_body": "Keep the rear bumper and trunk edge of your Honda scuff, scratch and damage free, with this unique protection system. Load and unload large objects such as suitcases, strollers and golf clubs, without the worry of permanently damaging your Honda. Attaches to the trunk floor (Velcro) and folds out to protect the bumper area. Non-slip rubber backing, Fuss-free installation takes only seconds.",
      "assets": {
        "thumbnail": "trunk_edge___bumper_protector_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51378_floor_liner": {
      "name": "Floor Liner",
      "price": 214.44,
      "description_title": "Floor Liner",
      "description_body": "",
      "assets": {
        "thumbnail": "floor_liner_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51374_auto_day_night_mirror_with__compass": {
      "name": "Auto Day/Night Mirror with  Compass",
      "price": 536.67,
      "description_title": "Auto Day/Night Mirror with  Compass",
      "description_body": "The auto day/night mirror helps to reduce the glare of bright headlights from the cars behind you. Attachment is included.",
      "assets": {
        "thumbnail": "auto_day_night_mirror_with__compass_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51375_cargo_hook": {
      "name": "Cargo Hook",
      "price": 29.36,
      "description_title": "Cargo Hook",
      "description_body": "A Cargo hook provides an excellent way to store your groceries in your enviromentally friendly bags. (Price is for one hook)",
      "assets": {
        "thumbnail": "cargo_hook_thumbnail.jpg",
        "off_car": null
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    },
    "51381_interior_trim_kit___carbon_fibre_look": {
      "name": "Interior Trim Kit - Carbon Fibre look",
      "price": 340.98,
      "description_title": "Interior Trim Kit - Carbon Fibre look",
      "description_body": "Enhance the interior of your vehicle with this trim kit that gives your Honda a truly unique look and feel.",
      "assets": {
        "thumbnail": "interior_trim_kit___carbon_fibre_look_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51376_cargo_net": {
      "name": "Cargo Net",
      "price": 116.88,
      "description_title": "Cargo Net",
      "description_body": "From groceries to parcels, this convenient Cargo Net helps keep items secure and easy to retrieve from the trunk.",
      "assets": {
        "thumbnail": "cargo_net_thumbnail.jpg",
        "off_car": null
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    },
    "51372_all_weather_floor_mats___black": {
      "name": "All-Weather Floor Mats - Black",
      "price": 159.44,
      "description_title": "All-Weather Floor Mats - Black",
      "description_body": "Protect the interior of your Civic with All Weather Floor Mats. They are durable, fade resistant and have deep water retaining ridges to protect the interior of your Civic.",
      "assets": {
        "thumbnail": "all_weather_floor_mats___black_thumbnail.jpg",
        "off_car": null
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    },
    "51379_interior_illumination___blue": {
      "name": "Interior Illumination - Blue",
      "price": 223.94,
      "description_title": "Interior Illumination - Blue",
      "description_body": "Soft glow of light derive from the driver/passenger seat foot wells and illuminate the center console.",
      "assets": {
        "thumbnail": "interior_illumination___blue_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51377_cigarette_lighter": {
      "name": "Cigarette Lighter",
      "price": 65.81,
      "description_title": "Cigarette Lighter",
      "description_body": "",
      "assets": {
        "thumbnail": "cigarette_lighter_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51380_interior_trim_kit___silver": {
      "name": "Interior Trim Kit - Silver",
      "price": 340.98,
      "description_title": "Interior Trim Kit - Silver",
      "description_body": "Enhance the interior of your vehicle with this trim kit that gives your Honda a truly unique look and feel.",
      "assets": {
        "thumbnail": "interior_trim_kit___silver_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "51384_trunk_tray": {
      "name": "Trunk Tray",
      "price": 159.44,
      "description_title": "Trunk Tray",
      "description_body": "For all those fun things you love to do, this durable Trunk Tray is easy to remove and clean, and is designed to protect the original carpeting in your Civic.",
      "assets": {
        "thumbnail": "trunk_tray_thumbnail.jpg",
        "off_car": null
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "51395_side_skirt"
        ]
      },
      "includes": null
    },
    "51373_ashtray___cup_holder_style": {
      "name": "Ashtray - Cup Holder Style",
      "price": 62.48,
      "description_title": "Ashtray - Cup Holder Style",
      "description_body": "Front seat passengers can enjoy the added convenience of an ashtray.",
      "assets": {
        "thumbnail": "ashtray___cup_holder_style_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    }
  }
}