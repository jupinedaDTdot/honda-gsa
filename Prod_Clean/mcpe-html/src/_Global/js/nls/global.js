var lang = 'en', env = 'LIVE';
define({
    'fr': true
    , 'auto': true
    , 'fr-auto': true
    , 'acura': true
    , 'fr-acura': true
    , 'atv': true
    , 'fr-atv': true
    , 'mc': true
    , 'fr-mc': true
    , 'marine': true
    , 'fr-marine': true
    , 'engine': true
    , 'fr-engine': true
    , root: {
        base_url: window.location.protocol + '//' + window.location.host,
        lang: 'en',
        'format.price': '$#,###.00',
        'format.price_rounded': '$#,###',
        environment: 'LIVE',
        // urls
        'url.base': '',
        'url.province_dealers': '/modelpages/BookTestDriveDealers.aspx?lang=' + lang + '&province=',
        'url.finance_calculator': '/tools/buildit/financialcalculator/financial_panel.aspx',
        'url.finance_calculator_summary': '/tools/buildit/financialcalculator/summary.aspx',
        'url.finance_calculator_msrp': function () {
            // build the msrp url based on vehicle and province selector
            var province = $('#province', this).val();
            var vehicle = eval('(' + $('#vehicle', this).val() + ')');
            var included = $('#include_tax', this).attr('checked');

            if (null == vehicle) {
                return false;
            }

            var url = '/buildittool/' + global_get('environment') + '/data/JSON/' + global_get('lang') + '/' + province + '/' + vehicle.model_key
                + '/' + vehicle.trim_level_key + "/" + vehicle.transmission_key + '/financial_presets.json?include_fees='
                + (included ? 'true' : 'false');

            return url;
        },
        'finance_calculator.lease_contact_dealer': "*Please contact your dealer for lease options.",
        'finance_calculator.finance_contact_dealer': "*Please contact your dealers for finance options.",
        // honda accessories
        'url.honda_accessories': function () {
            var province = $('#accessory_province').val();
            province = province ? province : 'ON';
            var uri = $('#accessory_trim').val();
            uri = uri.split('/');
            uri.splice(uri.indexOf('JSON') + 2, 0, province); // insert province into the URI
            var transmission = uri.pop();
            uri.pop();
            uri.pop();
            uri.push(transmission);
            return uri.join('/') + '/accessories.json';
        },
        'url.honda_accessories.color_thumbnail_prefix': function (key) {
            //        /buildittool/LIVE/data/JSON/assets/accessories/civic_coupe/dx/alabaster_silver_metallic/748-Manual/protection_package_thumbnail.jpg
            var uri = $('#accessory_trim').val();
            uri = uri.split('/');
            var transmission = uri.pop();
            var color = uri.pop(); // color
            var trim = uri.pop();
            var model = uri.pop();
            var acc = uri.pop();
            uri.pop(); // lang
            uri.push('assets');
            uri.push(acc);
            uri.push(model);
            uri.push(trim);
            uri.push(color);
            uri.push(transmission);
            return uri.join('/');
        },
        'url.honda_accessories.thumbnail_prefix': function (key) {
            var uri = $('#accessory_trim').val();
            uri = uri.split('/');
            var transmission = uri.pop();
            uri.pop(); // color
            var trim = uri.pop();
            var model = uri.pop();
            var acc = uri.pop();
            uri.pop(); // lang
            uri.push('assets');
            uri.push(acc);
            uri.push(model);
            uri.push(trim);
            uri.push(color);
            uri.push(transmission);
            return uri.join('/') + '/' + key;
        },
        'honda_accessories.section_titles': {
            packages: {
                title: 'Packages'
            },
            entertainment: {
                title: 'Entertainment'
            },
            exterior: {
                title: 'Exterior'
            },
            interior: {
                title: 'Interior'
            }
        }
        , 'popup.mask': {
            color: '#000'
            , opacity: 0.8
        }
        , 'accessory.slidedown_open': true,
        // city suggest search url
        'url.search_city': '/dealerlocator/GetCityService.aspx?sCity=',
        'url.search_suggest': '/_Global/service/GSAService.svc/SuggestiveSearch',
        'search_suggest.query_type': {
            type: 'field',
            field: 'GSA_Keywords'
        },
        'search_suggest.data': {
            client: 'honda_suggestive',
            collections: 'honda_' + (__hsite ? __hsite : ''),
            lang: 'lang_' + lang,
            perGroup: 5,
            requiredMetaFields: '',
            partialMetaFields: '',
            filter: 0,
            _last: null
        },
        'url.search_link_fallback': {
            url: '/search/',
            copy: 'Search for all results on honda.ca'
        },
        'url.search_no_result_copy': 'Sorry, we cannot find a shortcut. Search all of Honda.ca.',
        // survey urls
        'url.survey': 'http://www.hondasurvey.ca/powerequipment/?lang=en&sid=',
        'survey.enabled': false,
        'survey.css': '/_Global/css/common/survey.css',
       	'url.survey_window': '/survey/survey_eng.html',
        // french version
        // 'url.survey': 'http://hondasurvey.ca/?sid=97273&lang=fr',
        // 'url.survey_window': 'survey_fre.html',
        // 'url.survey_close_button': '/_Global/img/survey/Fre/close.gif',
        // arguments
        'autocomplete.search_city.min_length': 2,

        // flash player
        'player.standard': { 'type': 'flash', 'width': '700', 'height': '390', 'hashVersion': '9', 'swf': '/_Global/swf/honda_videoPlayer.swf', 'flashvars': { show_controls: 'true', video_location: '' }, 'allowScriptAccess': 'sameDomain', 'allowNetworking': 'all', 'wmode': 'opaque', 'quality': 'high', 'play': 'true', 'loop': 'false', 'menu': 'false', 'scale': 'showall', 'bgcolor': '#4c4c4c' },

        // slideshow player
        'player.slideshow': { 'type': 'flash', 'width': '950', 'height': '425', 'hashVersion': '9', 'swf': '/_Global/swf/honda_slideShow.swf', 'flashvars': { 'xml_location': '' }, 'allowScriptAccess': 'sameDomain', 'allowfullscreen': 'true', 'allowNetworking': 'all', 'wmode': 'opaque', 'quality': 'high', 'play': 'true', 'loop': 'false', 'menu': 'false', 'scale': 'showall', 'bgcolor': '#ffffff' },


		// 360 viewer
		'player.360': {
			width: "730"
			, height: "370"
			, hashVersion: "9"
			, swf: "/_Global/swf/Hondaca_360Viewer.swf"
			, flashvars: {
				car360: ""
			}
			, allowScriptAccess: "sameDomain"
			, allowNetworking: "all"
			, wmode: "transparent"
			, quality: "high"
			, play: "true"
			, loop: "false"
			, menu: "false"
			, scale: "showall"
			, bgcolor: "#ffffff"
		},

		'player.video':	{
			width: 700
			, height: 350
			, autostart: true
			, fullscreen_enabled: false
			, skin: 'mejs-honda'
		},

        // canada map
        'url.current_offers_map': '/_Global/svg/canada.xml',

        // google analytics account
        'tracker.account': 'UA-5158555-27',

        // recaptcha
        'recaptcha.public_key': '6LdBLcESAAAAANjZQggpl7gk3QyauYh3INzDctO0',


        // CompareWidget
        'honda.CompareWidget.defaults': {
            url: '/compare/'
        },

        // Compare App
        'honda.CompareApp': {
            modelSpecsUrl: '/service/trimListing.aspx?modelfamily={modelFamily}'
        },
        'honda.compare.messages': {
            error_add: 'Error occurs while retrieving {0}.'
        }

        , 'url.honda_match.models': function () {
            return '/buildittool/' + env + '/data/JSON/en/models.json';
        }
        , 'url.honda_match.models_financial': function () {
            var province = $.cookie('hondaprovince');
            province = province ? province : 'ON';
            return '/buildittool/' + env + '/data/JSON/en/' + province + '/models_financial.json?include_fees=false';
        }
        , 'url.honda_match.thumbnail_path': function () {
            return '/buildittool/' + env + '/data/json/assets/{model_key}/hondaca_modelNav.png';
        }
        , 'url.honda_match.thumbnail_width': 59
        , 'url.honda_match.thumbnail_height': 37
        , 'honda_match.format': {
            suffix: {
                lease: '/month',
                finance: '/month',
                seats: ' Seats',
                fuel_efficiency: ' L/100km'
            },
            prefix: {
                msrp: '$',
                lease: '$',
                finance: '$'
            },
            precision: {
                msrp: 1,
                lease: 0.01,
                finance: 0.01,
                seats: 1,
                fuel_efficiency: 0.1
            }
        }
        , 'build_it.assets_path': 'assets/'
        , 'build_it.gallery_path': '/buildittool/' + env + '/data/JSON/assets/media_gallery_root/'
        , 'build_it.thumbnail_path': function () {
            return '/buildittool/' + env + '/data/json/assets/{model_key}/hondaca_modelNav.png?crop=(0,0,160,100)&width=118&height=74';
        }
        , 'build_it.accessory.thumbnail_width': 132
        , 'build_it.accessory.thumbnail_height': 84
        , 'build_it.accessory.image_width': 250
        , 'build_it.accessory.image_height': 160
        , 'build_it.format.price': '<span>$</span>#,###.00'
        , 'build_it.format.price_rounded': '<span>$</span>#,###'
        , 'build_it.url.dealerLocator': '/dealerlocator'
        , 'build_it.summary.bottom.group': 'DueOnDeliverySubItemsGroup'
        , 'honda.match_accessory.defaults': {
            default_thumbnail: '/_Global/img/honda/NA_thumbnail.jpg'
        }
        , 'text.decimal.separator': '.'
        , 'text.manual': 'Manual'
        , 'text.automatic': 'Automatic'
        , 'text.cvt': 'CVT'
        , 'text.invalid_postal': 'Invalid postal code.'
        , 'text.msrp': 'MSRP'
        , 'text.vehicle_name': '%(year)s %(model)s'
        , 'text.lease': 'Lease'
        , 'text.finance': 'Finance'
        , 'text.accessories': 'Accessories'
        , 'text.comprehensive': 'Comprehensive'
        , 'text.partNumber': 'Part #'
        , 'text.cost': 'Cost'
        , 'honda.match_financial.defaults': {
            url: '/tools/buildit/financialcalculator/financial_panel.aspx'
            , msrp_url: '/buildittool/LIVE/data/JSON/{lang}/{province_key}/{model_key}/{trim_level_key}/{transmission_key}/financial_presets.json?include_fees={include_fees}&exterior_key={exterior_key}'
        }
        // FIXME brochure app should be updated to handle empty accessory results
        , 'build_it.brochure_accessory.defaults': {
            "interior": null,
            "labels": {
                "interior": "Interior",
                "exterior": "Exterior",
                "packages": "Packages",
                "entertainment": "Entertainment"
           },
            "keys": [
                "packages",
                "entertainment",
                "exterior",
                "interior"
            ],
            "exterior": null,
            "packages": null,
            "entertainment": null
        }
    }
});
