// $Id: app.js 20538 2012-10-25 18:26:28Z felix.huang $

require(['core/modernizr-1.6'], function () {

  (function ($, W, D, F, M, require, undefined) {

    F.edit_mode = !!F['edit_mode'];
    $.behaviors = {};

    // jquery ui group
    require(['prepare', 'ui/jquery.ui.core'], function () {
      require(['ui/jquery.ui.widget'], function () {
        var req = ['ui/jquery.ui.mouse', 'ui/jquery.ui.autocomplete', 'commonlib/jquery.commonlib.widget'];
        require(req, function () {
          main();
        });
      });
    });

    function main() {

      require([
    'includes/jquery.cookie',
    'commonlib/jquery.commonlib.dropdown',
    'commonlib/jquery.commonlib.scrollview',
    'includes/jquery.swfobject-1.1.1',
    'includes/jquery.video.player'], function () {

      var deps = [];

      // set the global behavior cases, $.behavior_cases, @see $.behavior_case_defaults for an example
      $.behavior_case_defaults = {
        ready: 'all', // 'all', or regular expression to match behavior names, or an array of behavior names
        ajax: ['cufon_refresh',
                'model_gallery',
                'external_links',
                'act_fieldset',
                'act_scrollview',
                'act_tooltip',
                'act_audio',
                'act_popup',
                'slidedown_bars',
                'gallery_links',
                'inner_page_links',
                'video_player_support',
                'show_hide_select',
                'edit_mode'
            ],
        act_popup: [
                'cufon_refresh'
                , 'act_fieldselect'
                , 'act_tooltip'
            ],
        primary_navigation: [
                'cufon_refresh',
                'pngfix_ie6',
                'external_links',
                'edit_mode'
            ],
        act_ajaxload: [
                'cufon_refresh',
                'model_gallery',
                'pngfix_ie6',
                'external_links',
                'video_player_support',
                'gallery_links',
                'act_fieldselect',
                'act_more_text',
                'act_less_text',
                'act_scrollview',
                'act_tooltip',
                'act_popup',
                'act_ajaxload',
                'act_collapsible',
                'accessory_trim_selector', // WORKAROUND
                'honda_accessories',
                'edit_mode'
            ],
        adjust_content_height: [
            ],
        honda_accessories: [
                'accessory_tooltips',
                'slidedown_bars'
            ],
        cufon: /^cufon/
      };

      $.behavior_cases = $.extend(true, {}, $.behavior_case_defaults, $.behavior_cases || {});

      $.browser.mobilesafari = M.touch;

      // set the global timeout for ajax calls here.
      $.ajaxSetup({
        timeout: 30000
      });

      $.flash.expressInstall = '/_Global/swf/playerProductInstall.swf';

      function check_inline_debug_status() {
        var get_param = function (name) {
          var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(W.location.href);
          if (!results) { return 0; }
          return results[1] || 0;
        };
        if (get_param('__inlineDebug')) {
          return true;
        }
        if (W.location.hash == '#__inlineDebug') {
          return true;
        }

        return false;
      }

      $._DEBUG = check_inline_debug_status();

      if (typeof console == 'undefined') {
        if ($._DEBUG) {
          // load firebug lite if console doesn't exist
          (function () {
            var ga = D.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == D.location.protocol ? 'https://' : 'http://') + 'getfirebug.com/firebug-lite.js';
            var s = D.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        }
        else {
          W.console = {
            log: function () { }
          };
        }
      }


      if ($._DEBUG) {
        console.log('JavaScript debugging enabled.');
      }

      $._searchTimer = null;

      function url(pathname, options) {
        var o = $.extend(true, {}, url.defaults, options);
        if (o.absolute) {
          return F.global_get('base_url') + pathname;
        }
        else {
          return pathname;
        }
      }

      url.defaults = {
        absolute: false
      };


      $.behaviors.ajax_start = function (ctx) {
        $('html').removeClass('ajax-complete').addClass('ajax-loading');
        if (!$.flash.available) {
          $('body').addClass('no-flash');
        }
      };

      $.behaviors.cufon_refresh = function (ctx) {

        if (!($.browser.msie && $.browser.version.match(/^(6|7)\./))) {
          var typefaces = F.get_typefaces();

          for (var index = 0; index < typefaces.length; index++) {
            //                    $(typefaces[index].elements).each(function () {
            //                        $(this).data('cufonText', $(this).text());
            //                    });
            Cufon.replace($(typefaces[index].elements, ctx), typefaces[index].options, true);
          }
        }
      };


      $.behaviors.pngfix_ie6 = function (ctx) {
        if ($.browser.msie && $.browser.version.match(/^6\./)) {
          $('.pf, .gmap_result_list .bg, .gmap_result_detail .bg, #feature_area img[src$=.png], .cap_top_default, .cap_bottom_default, .cap_bottom_columns, .wrapper_columns, .wrapper_overlay, .widgets li, #trim_selector .preview img, .featured_search_results img, .wrapper_top_cap, .wrapper_bottom_cap', ctx).each(function () {
            DD_belatedPNG.fixPng(this);
          });
        }
        $('html').addClass('pngfix-complete');
      };

      $.behaviors.hide_inner_page_links_hash = function (ctx, ajax) {
        // load the page if hash exists
        if (W.location.hash && W.location.hash != '#video') {
          var hash = W.location.hash.substring(1);
          if ($('a.inner_page_link[href=' + hash + ']').size() > 0) {
            $('#inner_page_content').hide();
          }
        }
      };

      $.behaviors.select_file_selector = function (ctx) {
        $('[property~="act:file-selector"]', ctx).each(function () {
          $(this).change(function () {
            if ($(this).val() != '<NONE>') {
              W.open($(this).val(), 'file_window');
            }
          });
        });
      };

      function fixpng(e) {
        function fix() {
          var image = $(this).css('background-image').toString();
          var is_image = $(this).attr('src');
          if (image.match(/\.png/) || is_image && typeof is_image == 'string' && is_image.match(/\.png/)) {
            DD_belatedPNG.fixPng(this);
          }
        } F.forBrowser('msie', /^6\./, function () {
          $(e).each(fix);
          $('.pf', e).each(fix);
        });
      }

      $.behaviors.act_placeholder = function (ctx) {

        $('input, textarea').not('.placeholder_processed').focus(function () {
          var input = $(this);
          if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
          }
        }).blur(function () {
          var input = $(this);
          if (input.val() == '') {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
          }
        }).blur()
	.addClass('placeholder_processed');

        $('form').submit(function () {
          $('[placeholder]', this).each(function () {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
              input.val('');
            }
          });
        });
      };

      $.behaviors.act_fieldselect = function (ctx, ajax) {
        if ($('[property~="act:fieldselect"]', ctx).size() > 0) {
          require(['commonlib/jquery.commonlib.fieldselect'], function () {
            $('[property~="act:fieldselect"]', ctx).fieldselect({
              complete: function (event, ui) {
                fixpng(ui.button);
                var text = $('span', ui.button).html();
                Cufon.replace(ui.button, { fontFamily: F.FONT_NATIONAL_EXTRABOLD }, true);
                ui.button.bind('dropdowncomplete', function (ev, dd) {
                  fixpng(dd.frame);
                  $('span', dd.placeholder).html(text);
                  Cufon.replace($('span', dd.placeholder), { fontFamily: F.FONT_NATIONAL_EXTRABOLD }, true);
                });
              },
              change: function (event, ui) {
                Cufon.replace(ui.button, { fontFamily: F.FONT_NATIONAL_EXTRABOLD }, true);
                Cufon.replace(ui.placeholder, { fontFamily: F.FONT_NATIONAL_EXTRABOLD }, true);
              }
            });
          });
        }
      };

      $.behaviors.act_tooltip = function (ctx, ajax) {
        if ($('[property~="act:tooltip"]', ctx).size() > 0) {
          require(['commonlib/jquery.commonlib.tooltip'], function () {
            $('[property~="act:tooltip"]', ctx).tooltip({
              complete: function (event, ui) {
                fixpng(ui.tooltip);
              }
            });
          })
        }
      };

      $.behaviors.act_more_text = function (ctx, ajax) {
        $('[property~="act:more-text"]', ctx).each(function () {
          var self = $(this);
          if (self.data('act_more_text')) {
            return;
          }

          self.data('act_more_text', true);

          var target = self.attr('href');

          if (target) {
            self.click(function (event) {
              event.preventDefault();
              $(target).show();
              self.hide();
              $('[property~="act:less-text"][href="' + target + '"]').show();
            });
          }
        });
      };

      $.behaviors.act_less_text = function (ctx, ajax) {
        $('[property~="act:less-text"]', ctx).each(function () {
          var self = $(this);
          if (self.data('act_less_text')) {
            return;
          }

          self.data('act_less_text', true);

          var target = self.attr('href');

          if (target) {
            self.click(function (event) {
              event.preventDefault();
              $(target).hide();
              self.hide();
              $('[property~="act:more-text"][href="' + target + '"]').show();
            });
          }
        });
      };

      $.behaviors.act_flash = function (ctx, ajax) {
        if ($('[property~="act:flash"]', ctx).size() > 0) {
          require(['includes/jquery.swfobject-1.1.1'], function () {
            $('[property~="act:flash"]', ctx).each(function () {
              var self = $(this)
                            , url = self.data('url')
                            , width = self.data('width')
                            , height = self.data('height');


              self.flash({
                swf: url
                            , width: width
                            , height: height
                            , wmode: 'transparent'
              });
            })
          });
        }
      };



      $.behaviors.act_flash_target = function (ctx, ajax) {
        if ($('[property~="act:flash-target"]', ctx).size() > 0) {
          require(['includes/jquery.swfobject-1.1.1'], function () {
            $('[property~="act:flash-target"]', ctx).each(function () {
              var self = $(this)

              self.click(function (event) {
                event.preventDefault();

                var url = self.data('url')
                            	, width = self.data('width')
                            	, height = self.data('height')
                            	, target = $(self.data('target'));

                target.flash({
                  swf: url
	                            , width: width
	                            , height: height
	                            , wmode: 'transparent'
                });
              });

            })
          });
        }
      };


      $.behaviors.act_scrollview = function (ctx, ajax) {
        var localQueue = new $.jobqueue();
        if ($('[property~="act:scrollview"]', ctx).size() > 0) {
          require(['commonlib/jquery.commonlib.scrollview'], function () {
            $('[property~="act:scrollview"]', ctx).each(function () {
              var elem = this;
              localQueue.add(function () {
                $(elem).scrollview();
              }, D, 10);
            });

          });
        }
      };

      $.behaviors.act_toutrotator = function (ctx, ajax) {
        if ($('[property~="act:toutrotator"]', ctx).size() > 0) {
          require(['commonlib/jquery.commonlib.toutrotator', 'includes/jquery.swfobject-1.1.1'], function () {
            $('[property~="act:toutrotator"]', ctx).each(function () {
              var self = $(this),
            url = self.data('url'),
            width = self.data('width') || 950,
            height = self.data('height') || 400;
              // empty the toutrotator and add the stage and controls
              self.empty().append(
          $('<div>').addClass('toutrotator-stage')
        ).append(
          $('<span>').addClass('toutrotator-controls')
        );

              self.bind('toutrotatorload', function (event, ui) {
                self.data('currentTout', ui.tout);
                if (window.tracker_get) {
                  var tracker = tracker_get();
                  if (tracker && tracker.trackEvent && ui.tout) tracker.trackEvent('Tout Rotator', 'Load', ui.tout.caption);
                }
              });
              self.bind('toutrotatorplay', function (event, ui) {
                if (window.tracker_get) {
                  var tracker = tracker_get();
                  if (tracker && tracker.trackEvent && ui.tout) tracker.trackEvent('Tout Rotator', 'Play', ui.tout.caption);
                }
              });
              self.bind('toutrotatorpause', function (event, ui) {
                if (window.tracker_get) {
                  var tracker = tracker_get();
                  if (tracker && tracker.trackEvent && ui.tout) tracker.trackEvent('Tout Rotator', 'Pause', ui.tout.caption);
                }
              });

              self.toutrotator({
                flashPreloaderPrefix: '/_Global/swf/preloader.swf?autoplay=1&swf=',
                stage: $('.toutrotator-stage', self),
                controller: $('.toutrotator-controls', self),
                width: width,
                height: height,
                touts: function (callback) {
                  $.ajax({
                    type: 'get',
                    url: url,
                    dataType: 'xml',
                    success: function (data, status) {
                      // construct the array
                      var result = [];
                      $('tout', data).each(function () {
                        var tout = $(this),
			                toutUrl = $('> url', tout).text(),
			                link = $('> link', tout).text(),
			                caption = $('> caption', tout).text(),
			                duration = parseInt(tout.attr('activeTime')) * 1000,
			                type = 'blank',
			                target = /^http:/i.test(toutUrl) ? '_blank' : '',
			                fallback = $('> fallback', tout),
			                standalone = tout.attr('standalone') == 'true',
			                hasFallback = fallback.size() > 0,
			                fallbackTout = {};

                        if (hasFallback) {
                          fallbackTout = {
                            type: 'image',
                            url: $('> url', fallback).text(),
                            link: $('> link', fallback).text(),
                            caption: $('> caption', fallback).text(),
                            duration: parseInt(fallback.attr('activeTime')) * 1000,
                            target: target
                          };
                        }

                        // determine the type
                        if (/\.(png|jpg|gif)$/i.test(toutUrl)) {
                          type = 'image';
                        }
                        else if (/\.swf$/i.test(toutUrl)) {
                          type = 'flash';
                        }
                        else {
                          type = 'iframe';
                        }

                        var toutItem = {
                          type: type,
                          url: toutUrl,
                          link: link,
                          caption: caption,
                          standalone: standalone,
                          align: 't',
                          salign: 't',
                          wmode: 'transparent',
                          duration: duration,
                          target: target
                        };

                        if (hasFallback) {
                          toutItem.fallback = fallbackTout;
                        }
                        result.push(toutItem);
                      });

                      callback(null, result);
                    },
                    error: function (xhr, err) {
                      callback(err, null);
                    }
                  });
                },
                init: function () {
                  $('.toutrotator-progress-inner', this).append($('<span>').addClass('toutrotator-progress-dot'));
                }
              });

              $('.toutrotator-stage', self).bind('click', function (event) {
                if (window.tracker_get) {
                  var tout = self.data('currentTout'), tracker = tracker_get();
                  if (tracker && tracker.trackEvent && tout) tracker.trackEvent('Tout Rotator', 'Select', tout.caption);
                }
            });
          });
          });
        }
      };


      $.behaviors.act_popup = function (ctx, ajax) {
        var localQueue = new $.jobqueue(10)
                , mask = global_get('popup.mask');
        if ($('[property~="act:popup"]', ctx).size() > 0) {
          require(['commonlib/jquery.commonlib.popup'], function () {
            $('[property~="act:popup"]', ctx).each(function () {
              var elem = this;
              localQueue.add(function () {
                $(elem).popup({
                  overlay: mask.color
                                , opacity: mask.opacity
                                , complete: function (event, ui) {
                                  $('.dropdown_content_mid > *', ui.frame).hide();
                                  $('.dropdown_content_mid', ui.frame).append(
                                        $('<div>').addClass('loader_container').append(
                                            $('<div>').addClass('bg_loader loader_gray')
                                        )
                                    );

                                  localQueue.add(function () {
                                    F.execute_behaviors('act_popup', ui.frame, true, 2, ui.queue); // use another local queue
                                    ui.queue.add(function () {
                                      $('.dropdown_content_mid > .loader_container', ui.frame).remove();
                                      $('.dropdown_content_mid > *', ui.frame).show();
                                      ui.call_Notify();
                                    });
                                  }, true, 2);
                                }
                                , show: function (event, ui) {
                                  ui.call_Notify();
                                }
                });
              }, D, 10);
            });
          });
        }
      };


      $.behaviors.act_collapsible = function (ctx, ajax) {
        if ($('[property~="act:collapsible"], [property~="act:collapsible-expand"], [property~="act:collapsible-collapse"]', ctx).size() > 0) {
          require(['commonlib/jquery.commonlib.collapsible'], function () {
            $('[property~="act:collapsible"]', ctx).collapsible({
              show: function (event, ui) {
                ui.widget.addClass('active selected');
              },
              hide: function (event, ui) {
                ui.widget.removeClass('active selected');
              }
            });

            // expand all
            $('[property~="act:collapsible-expand"]', ctx).click(function () {
              var self = $(this),
                    group = self.data('collapsible-group');
              $.commonlib.collapsible.openGroup(group);
            });

            // collapse all
            $('[property~="act:collapsible-collapse"]', ctx).click(function () {
              var self = $(this),
                    group = self.data('collapsible-group');

              $.commonlib.collapsible.closeGroup(group);
            });
          });
        }
      };

      $.behaviors.act_tabs = function (ctx, ajax) {
        if ($('[property~="act:tabs"]', ctx).size() > 0) {
          require(['commonlib/jquery.commonlib.collapsible'], function () {
            $('[property~="act:tabs"]', ctx).collapsible({
              exclusive: true,
              activeClass: 'selected',
              show: function (event, ui) {
                Cufon.replace(this, { fontFamily: F.FONT_NATIONAL, hover: true }, true);
              },
              hide: function (event, ui) {
                Cufon.replace(this, { fontFamily: F.FONT_NATIONAL, hover: true }, true);
              }
            });
          });
        }
      };

      $.behaviors.act_carousel = function (ctx) {
        if ($('[property~="act:carousel"]').size() > 0) {
          require(['commonlib/jquery.commonlib.carousel'], function () {
            $('[property~="act:carousel"]').each(function () {
              var self = $(this);
              self.carousel();
            });
          });
        }
      };

      $.behaviors.act_menu = function (ctx) {
        $('[property~="act:menu"]').each(function () {
          var self = $(this);
          var position = {
            my: 'left top',
            at: 'right top'
          };
          if (self.data('menu-type') == 'brochure') {
            position.offset = '-10px -2px';
            F.forBrowser('msie', /^[678]\./, function () {
              position.offset = '-9px -2px';
            });
          }
          self.dropdown({
            contentTarget: self.data('menu-content-target'),
            top: -5,
            left: -13 + 217,
            position: position,
            flexibleWidth: self.data('menu-flexible-width'),
            additionalClass: 'menu_box' + (self.data('menu-class') ? (' ' + self.data('menu-class')) : ''),
            btnZIndex: 8,
            containerZIndex: 5,
            sibling: false,
            body: true,
            handler: 'rollover',
            complete: function (event, ui) {
              fixpng(ui.frame);
              if (self.data('menu-type') == 'brochure') {
                $('.dropdown_button .btn_large', ui.frame).addClass('btn_large_hover');
                F.forBrowser('msie', /^8\./, function () {
                  var text = $('.dropdown_button .btn_large > span.downloads', ui.frame).text();
                  $('.dropdown_button .btn_large > span.downloads', ui.frame).text(text);
                });
              }
              Cufon.replace($('.dropdown_button .btn_large > span.downloads', ui.frame), { fontFamily: FONT_NATIONAL }, true);
            }
          }).bind('dropdownshow', function () {
            $(this).addClass('hover');
          }).bind('dropdownhide', function () {
            $(this).removeClass('hover');
          });
        });
      };

      $.behaviors.act_ajaxload = function (ctx, ajax) {
        if (ajax) {
          return;
        }

        if ($('a[property~="act:ajaxload"]').size() > 0) {
          require(['commonlib/jquery.commonlib.ajaxload'], function () {
            $('[property~="act:ajaxload"]').each(function () {
              var self = $(this);

              self.ajaxload({
                target: '#inner_page_content',
                activeClass: 'selected',
                contentTarget: '#inner_page_content',
                contentUrl: self.attr('href'),
                beforeLoad: function (event, ui) {
                  // attach loader here
                }
                            , load: function (event, ui) {
                              if (self.parents('.tabs_horizontal').size() > 0 || self.parents('.tabs_horizontal_fixed').size() > 0) {
                                F.execute_behaviors('act_ajaxload', '.content_box .content', true);
                              }
                              else {
                                F.execute_behaviors('act_ajaxload', ui.target, true);
                              }
                            }
                            , error: function (event, ui) {
                              if (self.parents('.tabs_horizontal').size() > 0 || self.parents('.tabs_horizontal_fixed').size() > 0) {
                                F.execute_behaviors('act_ajaxload', '.content_box .content', true);
                              }
                              else {
                                F.execute_behaviors('act_ajaxload', ui.target, true);
                              }
                            }
              });
            });
          });
        }
      };

      $.behaviors.act_ajaxform = function (ctx) {
        if ($('[property~="act:ajaxform"]', ctx).size() > 0) {
          require(['commonlib/jquery.commonlib.ajaxform'], function () {
            $('[property~="act:ajaxform"]', ctx).each(function () {
              var self = $(this);
              self.ajaxform({
                validate: function (form) {
                  var api = $(form).data('validator');
                  return api.checkValidity();
                },
                success: function (event, ui) {
                  var o = $(this).data('ajaxform').options,
                                    button = $(o.button, o.form);

                  $(button).siblings('.msg').hide().filter('.success').show();

                  $(this).delay(1000).queue(function () {
                    if (self.data('popup')) {
                      self.popup('close');
                    }
                    // clear the form
                    var es = $('input:not(:checkbox):not(:radio), select, textarea', o.form).filter('[data-form="' + $(button).attr('name') + '"]').val('');
                    $('input:checkbox, input:radio', o.form).filter('[data-form="' + $(button).attr('name') + '"]').removeAttr('checked');

                    $(button).siblings('.msg').hide();
                    $(this).dequeue();
                  });
                },
                error: function (event, ui) {
                  var o = $(this).data('ajaxform').options,
                                    button = $(o.button, o.form);
                  // error display?
                  $(button).siblings('.msg').hide().filter('.fail').show();
                }
              });
            });
          });
        }
      };

      $.behaviors.act_imagetarget = function (ctx) {
        if ($('[property~="act:imagetarget"]', ctx).size() > 0) {
          require(['commonlib/jquery.commonlib.imagetarget'], function () {
            $('[property~="act:imagetarget"]', ctx).imagetarget();
          });
        }
      };

      $.behaviors.act_close = function (ctx) {
        $('[property~="act:close"]', ctx).each(function () {
          var self = $(this);
          if (self.data('close')) {
            return;
          }

          self.click(function (event) {
            var target = self.data('close-target');
            $(target).hide();
            event.preventDefault();
          });
        });
      };

      $.behaviors.act_audio = function (ctx) {
        if ($('[property~="act:audio"]', ctx).size() > 0) {
          require(['commonlib/jquery.commonlib.audioplayer'], function () {
            $('[property~="act:audio"]', ctx).audioplayer({
              player: 'flash'
            });
          });
        }
      };

      window._validatorBeforeFail = function (event, field) {
        field.addClass('field-error');
        field.parent('.input_box').addClass('field-input-error');
        field.parent('.field-input').addClass('field-input-error');
        field.next('.field-err-msg').show();
        // if is grouped data
        if (field.data('group')) {
          $('.field-err-msg').filter('[data-group="' + field.data('group') + '"]').show();
        }
      };

      window._validatorOnSuccess = function (event, fields) {
        fields.removeClass('field-error');
        fields.parent('.input_box').removeClass('field-input-error');
        fields.parent('.field-input').removeClass('field-input-error');
        fields.next('.field-err-msg').hide();
        // if is grouped data
        fields.each(function () {
          var field = $(this);
          if (field.data('group')) {
            $('.field-err-msg').filter('[data-group="' + field.data('group') + '"]').hide();
          }
        });
      };

      window._validatorOnFail = function () {
        return false;
      };

      window._attachFormValidator = function (elem) {
        if ($('form').data('validator')) {
          $('form').data('validator').destroy();
        }
        $('form').validator({
          inputEvent: null,
          errorInputEvent: 'change',
          lang: global_get('lang'),
          errorClass: 'field-error',
          onBeforeFail: function (event, field) {
            if (field.data('form') == $('form').data('CurrentForm')) {
              field.addClass('field-error');
              field.parent('.input_box').addClass('field-input-error');
              field.parent('.field-input').addClass('field-input-error');
              field.next('.field-err-msg').show();
              // if is grouped data
              if (field.data('group')) {
                $('.field-err-msg').filter('[data-group="' + field.data('group') + '"]').show();
              }
              return true;
            }
            return false;
          },
          onFail: _validatorOnFail,
          onSuccess: _validatorOnSuccess
        });
      };


      window._formValidate = function (elems) {
        var inputs = $(elems).validator({
          inputEvent: null,
          errorInputEvent: 'change',
          lang: global_get('lang'),
          errorClass: 'field-error',
          onBeforeFail: _validatorBeforeFail,
          onFail: _validatorOnFail,
          onSuccess: _validatorOnSuccess
        });
        var api = inputs.data('validator');
        if (!Modernizr.input['placeholder']) {
          inputs.filter('[placeholder]').each(function () {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
              input.val('');
            }
          });
        }

        return api.checkValidity();
      };

      $.behaviors.form_validation = function (ctx, ajax) {
        if (ajax) {
          return;
        }
        require(['ext/tools/validator/validator'], function () {

          $('[data-type|="submit"]', ctx).click(function (event) {

            var api = $('form', ctx).data('validator');
            if (!Modernizr.input['placeholder']) {
              $('[placeholder]', ctx).each(function () {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                  input.val('');
                }
              });
            }

            if (!api.checkValidity()) {
              event.preventDefault();
            }
          });

          $.tools.validator.fn('[type="checkbox"][data-group]', function (el, v) {
            var group = el.data('group');
            // we count at least one input from the checkboxes

            var g = $('[type="checkbox"][data-group="' + group + '"]:checked');
            return g.size() >= 1 ? true : "Please select at least one value in the group";
          });

          _attachFormValidator();

        });
      };

      // autocomplete search box (will escalate it into a honda widget eventually so it could be used across the board)
      $.behaviors.autocomplete_search_box = function (ctx) {
        var offset = '-161 -29';
        if ($.browser.mozilla) {
          offset = '-161 -28';
        }
        else if ($.browser.msie && $.browser.version.match(/^(6|7)\./)) {
          offset = '-161 -30';
        }
        var data_ = $.extend(true, {}, F.global_get('search_suggest.data'));
        var query_type = F.global_get('search_suggest.query_type');
        var autocomplete = $('#search input').autocomplete({
          position: {
            offset: offset
          },
          minLength: 3,
          source: function (request, response) {
            var self = this;
            data_.query = request.term;
            if (query_type) {
              if (query_type.type == 'query') {
                data_.query = request.term;
              }
              else if (query_type.type == 'field') {
                if (data_.partialMetaFields) {
                  data_.partialMetaFields = data_.partialMetaFields + '.' + query_type.field + ':' + request.term;
                }
                else {
                  data_.partialMetaFields = query_type.field + ':' + request.term;
                }
                data_.query = ''; // just a general term for the gsa to ignore
              }
            }
            var _url = F.global_get('url.search_link_fallback').url;
            // go to the url
            if (W.location.hostname.match(/^(www\.|staging\.|preview\.|)honda\.ca/)) {
            }
            else {
              _url = url(_url, { absolute: true });
            }
            self._link = {
              copy: F.global_get('url.search_link_fallback').copy,
              url: _url + W.encodeURI(request.term)
            };
            delete data_._last;

            $.ajax({
              type: 'POST',
              url: F.global_get('url.search_suggest'),
              data: data_,
              dataType: 'json',
              global: false,
              success: function (data, status, xhr) {
                self._link.copy = data.link.copy;
                if ('groups' in data && data.groups.length == 0) {
                  data.groups = [{ title: '', value: '!stop'}];
                }
                // manipulate the groups so the empty group goes to the end.
                for (var index = 0; index < data.groups.length; index++) {
                  if (data.groups[index].name == '') {
                    var t = data.groups[index];
                    data.groups.splice(index, 1);
                    data.groups.push(t);
                    break;
                  }
                }
                // if (t) {
                //         }
                response(data.groups);
              },
              error: function (data, status, xhr) {
                response([{ title: '', value: '!stop'}]);
              }
            });
          },
          select: function (event, ui) {
            $._searchTimer = true;
            // go to the url
            W.location.href = ui.item.url;
          },
          search: function (event, ui) {
            $('#search a.clear').addClass('autocomplete-loading');
          },
          open: function (event, ui) {
            autocomplete.menu.element.addClass('search_box');
            $('#search a.clear').removeClass('autocomplete-loading');
            // remove ui-menu-item for button
            $('.search-box-button', autocomplete.menu.element).removeClass('ui-menu-item');
            Cufon.replace($('.search-box-button .btn', autocomplete.menu.element), { fontFamily: 'National Extrabold', hover: true }, true);
          }
        }).keyup(function () {
          if ($(this).val() != '') {
            $('#search a.clear').removeClass('dn');
          }
          else {
            $('#search a.clear').addClass('dn');
          }
        }).data('autocomplete');

        // TODO enable autocomplete when meta data is in.
        $('#search input').autocomplete('disable');

        $('#search a.clear').click(function () {
          //                $('#search input').val('').blur();
          //                $(this).addClass('dn');
          return false;
        });

        $('#search').css({
          position: 'relative',
          'z-index': 900
        });

        autocomplete._renderMenu = function (ul, items) {
          var self = this;
          $('<li>').addClass('search-box-cap-top').appendTo(ul);
          try {
            if (items.length > 0 && items[0] && items[0].value != '!stop') {
              $.each(items, function (index, group) {
                var group_title = $('<li>').addClass('search-box-group')
						.append($('<div>').addClass('h5a').html(group.name));
                if (!group.name && items.length == 1) {
                  group_title.children().removeClass('h5a');
                }
                group_title.appendTo(ul);

                $.each(group.items, function (index, item) {
                  if (item.title) {
                    self._renderItem(ul, item);
                  }
                });
              });
            }
            else {
              var no_result_copy = F.global_get('url.search_no_result_copy');
              $('<li>').addClass('ui-menu-item').append($('<div>').text(no_result_copy)).appendTo(ul);
            }
          } catch (e) {
            var no_result_copy = F.global_get('url.search_no_result_copy');
            $('<li>').addClass('ui-menu-item').append($('<div>').text(no_result_copy)).appendTo(ul);
          }
          $('<li>').addClass('search-box-button').append(
			$('<a>').addClass('btn secondary hand').append(
				$('<span>').html(self._link.copy)
			).attr('href', self._link.url)
		).appendTo(ul);
          $('<li>').addClass('search-box-cap-bottom').appendTo(ul);
          ul.css({
            'z-index': 700
          });
          fixpng(ul);
        };

        // <div class="fl image"><img src="images/placeholder_search_dropdown_image.gif" /></div>
        // <div class="fl content">
        // 	<div class="h6">2010 Civic Hybrid</div>
        // 	<p>Starting MSRP <strong>$38,762</strong></p>
        // </div>
        // <div class="clrfix"></div>
        autocomplete._renderItem = function (ul, item) {
          if (!item) {
            return null;
          }
          return $("<li></li>")
			.data("item.autocomplete", item)
			.append(
				$('<a>').addClass('hand').append(
					$('<div>').addClass('fl image').append(
						$('<img>').attr({
						  src: item.image
						})
					)
				).append(
					$('<div>').addClass('fl content').append(
						$('<div>').addClass('h6').html(item.title)
					).append(
						$('<p>').html(item.copy)
					)
				).append(
					$('<div>').addClass('clrfix')
				)
				.attr('href', item.url)
			)
			.appendTo(ul);
        };
      };

      $.behaviors.enable_search_enter = function (ctx, ajax) {
        $('#search a.clear').click(function (ev) {
          var self = $('#search input, .search_input input');
          ev.preventDefault();
          self.trigger('autocompletesearch');
          setTimeout(function () {
            if (!$._searchTimer) {
              var url = F.global_get('url.search_link_fallback').url + self.val();
              // go to the url
              if (W.location.hostname.match(/^(www\.|staging\.|preview\.|)honda\.ca/)) {
                W.location.href = url;
              }
              else {
                W.location.href = F.global_get('base_url') + url;
              }
            }
          }, 1000);
        });
        $('#search input, .search_input input').bind('keydown.autocomplete', function (e) {
          var self = $(this);
          var code = e.keyCode ? e.keyCode : e.which;
          if (code == 13) {
            self.trigger('autocompletesearch');
            setTimeout(function () {
              if (!$._searchTimer) {
                var url = F.global_get('url.search_link_fallback').url + self.val();
                // go to the url
                if (W.location.hostname.match(/^(www\.|staging\.|preview\.|)honda\.ca/)) {
                  W.location.href = url;
                }
                else {
                  W.location.href = F.global_get('base_url') + url;
                }
              }
            }, 1000);
            e.preventDefault();
            return false;

          }
        });
      };

      $.behaviors.postal_input_enter = function (ctx, ajax) {
        $('.postal_code input, .find_dealer_container input:first, input.match_postal')
        .keydown(function (e) {
          var self = $(this);
          var code = e.keyCode ? e.keyCode : e.which;
          var val = $(this).val();
          if (code == 13) { // ENTER
            // special check for find a dealer page
            var button = self.parents('.form_item').siblings('a');
            button = button.size() == 0 ? self.next('a') : button;

            if (button.size() > 0) {
              var onclick = button.attr('onclick');
              if (onclick) {
                W.location.href = button.attr('href');
              }
            }
            else {
              $('form').submit();
            }
            e.preventDefault();
            return false;
          }
        }).attr({
          autocomplete: 'off'
        });
      };

      function dropdowncomplete(event, ui) {
        var position = ui.widget.position();
        $('.btn span > *', ui.frame).hide();
        $('.btn.dd_button', ui.frame).addClass('btn-hover');
        if (ui.frame.hasClass('car_box') && !ui.frame.hasClass('shopping_box')) {
          ui.frame.addClass('navigation-dropdown');
        }
        if (ui.frame.hasClass('shopping_box') || ui.frame.hasClass('corp_landing_box') || ui.frame.hasClass('search_box')) {
        }
        else {
          $('.dropdown_button', ui.frame).css('left', position.left);
          $('.btn', ui.frame).mouseover(function () {
            $(this).addClass('btn-hover');
            Cufon.replace($(this), { fontFamily: F.FONT_NATIONAL_EXTRABOLD, hover: true }, true);
          }).mouseout(function () {
            $(this).removeClass('btn-hover');
            Cufon.replace($(this), { fontFamily: F.FONT_NATIONAL_EXTRABOLD, hover: true }, true);
          });
        }
        if (ui.frame.hasClass('shopping_box')) {
          F.forBrowser('msie', function () {
            $('dd', ui.frame).mouseover(function () {
              $('a', this).addClass('hover');
              Cufon.replace($('a', this), { fontFamily: F.FONT_FOUNDERS_CONDENSED }, true);
            }).mouseout(function () {
              $('a', this).removeClass('hover');
              Cufon.replace($('a', this), { fontFamily: F.FONT_FOUNDERS_CONDENSED }, true);
            });
          });
          Cufon.replace($('.shopping_list_container a', ui.frame), { fontFamily: F.FONT_FOUNDERS_CONDENSED, hover: true }, true);
        }
        if ($('ul > li', ui.frame).size() > 5) {
          $('.hscrollpane_enabled', ui.frame).scrollpane({
            direction: 'horizontal',
            elementSelector: 'li',
            sideSize: 207,
            cursor: 'pointer'
          });
        }

        $('.dropdown_button a', ui.frame).addClass('ffgc').html(ui.widget.html());

        fixpng(ui.frame);
        Cufon.replace($('.ffgc', ui.frame), { fontFamily: F.FONT_FOUNDERS_CONDENSED, hover: true }, true);
        Cufon.replace($('a.btn', ui.frame), { fontFamily: F.FONT_NATIONAL_EXTRABOLD }, true);
        $('span.h3 > *', ui.frame).hide();
      }



      $.behaviors.primary_navigation_special = function (ctx) {
        // dropdown for shopping tools
        var init = function () {
          $(this).dropdown({
            additionalClass: 'shopping_box',
            position: {
              my: 'right top',
              at: 'right bottom',
              of: '#primary_nav',
              offset: '2 20'
            },
            complete: function (event, ui) {
              // calculate the width of anchor
              var width = $('#primary_nav').width();
              $('li:not(.last)', '#primary_nav').each(function () {
                width -= $(this).width();
              });

              // update the anchor width
              width -= 24;
              $('.dropdown_button_right a', ui.frame).width(width);
              dropdowncomplete.apply(this, [event, ui]);
            }
          });
        };

        $('#primary_nav li.last > a').each(init);
      };

      $.behaviors.primary_navigation = function (ctx) {
        // dropdown for cars
        var init = function (index, element) {
          $(element).dropdown({
            additionalClass: 'car_box' + (index > 0 ? ' hybrid_box' : ''),
            position: {
              my: 'left top',
              at: 'left bottom',
              of: '#primary_nav',
              offset: '-4 18'
            },
            complete: function (event, ui) {
              var self = $(this);

              // load the content
              var load_content = function (container, content, url, callback) {
                var loader = $('<div>').addClass('target-ctr ldr').css({ position: 'relative' }).append($('<div>').css({
                  position: 'absolute',
                  top: '50%',
                  'margin-top': '-16px',
                  left: '50%',
                  'margin-left': '-16px'
                }).addClass('loader_default bg_loader'));

                $(container).after(loader);

                $(container).hide().load(url + ' ' + content, function () {
                  // attach scroll view
                  $('.target-ctr .products:first', ui.frame).scrollview({
                    mousewheelSensitivity: 16,
                    delayed: false,
                    type: 'vertical',
                    scroll: function (event, type, drag) {
                      var e = $('.series-item.active', ui.frame),
                                        popup = e.data('popup');

                      if (popup) {
                        popup.position({
                          my: 'left top',
                          at: 'left top',
                          of: e,
                          collision: 'none'
                        });
                      }
                    }
                  }).css('overflow', 'hidden');
                  F.execute_behaviors('primary_navigation', $('.dropdown_content_mid', ui.frame), true, 2);
                  // append link to product items

                  $.jobqueue.add(function () {
                    $(container).nextAll('.ldr').remove();
                    $(container).show();
                    $('.right-boxes .products', ui.frame).scrollview('refresh');
                    if ($('.series-popup:visible', ui.frame).size() == 0 && $('.series .series-item', ui.frame).first().data('popup')) {
                      $('.series .series-item', ui.frame).first().data('popup').display();
                    }
                  }, true, 2);
                  if (callback) {
                    $.jobqueue.add(function () {
                      // activate the first series
                    }, true, 2);

                    callback();
                  }
                });
              };
              load_content($('.target-ctr', ui.frame), '.navigation-dropdown .content_section', self.data('url'), function () {

                // assign click jobs
                $('.series .series-item', ui.frame).each(function (index, element) {
                  var item = this;
                  var popup;

                  // add active state to myself and add inactive states to siblings
                  $(this).removeClass('inactive').addClass('active')
                                   .siblings('.series-item').removeClass('active').addClass('inactive');

                  // add active popup item if the popup item does not exist.
                  if ($(this).data('popup')) {
                    popup = $(this).data('popup');
                  }
                  else {
                    popup = $('<div>').addClass('series-popup').append(
                                        $(this).clone()
                                        );

                    popup.hide();
                    popup.css({
                      position: 'absolute'
                    });

                    // move it
                    popup.insertAfter($(this).parents('.right-boxes'));
                    fixpng($('.series-item', popup));
                    $(this).data('popup', popup);
                  }

                  popup.display = function () {
                    // position and show the popup
                    popup.show();
                    popup.position({
                      my: 'left top',
                      at: 'left top',
                      of: item,
                      collision: 'none'
                    });
                  };

                  $(this).click(function () {
                    // hide other popups
                    $(this).siblings('.series-item').each(function () {
                      var p = $(this).data('popup');
                      if (p) {
                        p.hide();
                      }
                    });

                    popup.display();

                    $('.products', ui.frame).data('scrollview').scrollToTop();
                    load_content($('.right-boxes .products .subview', ui.frame), '.navigation-dropdown .products .subview > *', $(this).data('url'), function () {
                      $('.right-boxes .products', ui.frame).scrollview('refresh');
                    });
                  });
                });
              });
              dropdowncomplete.call(this, event, ui);
            }
          });
        };

        $('#primary_nav li:not(.last):not(.alternate) > a').each(init);
      };

      // city autocomplete box (will escalate it into a honda widget eventually so it could be used across the board)
      $.behaviors.dropdown_city_autocomplete = function (ctx, ajax) {
        if (ajax || $('.dropdown_autocomplete_city').size() == 0) {
          return;
        }
        // different offsets for firefox/other browsers
        $('.dropdown_autocomplete_city').each(function () {
          var self = $(this), offset;

          if (self.is('.dropdown_autocomplete_city_sidebar')) {
            offset = '-9 0';
            if ($.browser.mozilla) {
              offset = '-9 0';
            }
            else if ($.browser.ie && $.browser.version.match(/^(6|7)\./)) {
              offset = '-9 -2';
            }
            else if ($.browser.webkit) {
              offset = '-10 0';
            }
          }
          else {
            offset = '-7 0';
            if ($.browser.mozilla) {
              offset = '-7 1';
            }
            else if ($.browser.ie && $.browser.version.match(/^(6|7)\./)) {
              offset = '-7 -1';
            }
          }
          self.autocomplete({
            minLength: F.global_get('autocomplete.search_city.min_length'),
            source: function (request, response) {
              $.ajax({
                url: F.global_get('url.search_city') + request.term,
                dataType: 'text',
                global: false,
                success: function (data, status, xhr) {
                  var candidates = $.grep(data.split("\n"), function (element, index) {
                    return element.replace(/^\s*(.*?)\s*/, '$1') != '';
                  });

                  response(candidates);
                }
              });
            },
            position: {
              my: 'left top',
              at: 'left bottom',
              of: self,
              offset: offset
            },
            select: function (event, ui) {
              var self = $(this);
              var button = self.parents('.form_item').siblings('a');
              button = button.size() == 0 ? self.next('a') : button;

              if (button.size() > 0) {
                setTimeout(function () {
                  W.location.href = button.attr('href');
                }, 500);
              }
            },
            create: function (event, ui) {
              var autocomplete = self.data('autocomplete');
              if (self.hasClass('dropdown_autocomplete_city_sidebar')) {
                autocomplete.menu.element.addClass('dropdown_dealer_city dropdown_dealer_city_sidebar');
              }
              else {
                autocomplete.menu.element.addClass('dropdown_dealer_city');
              }
            }
          });
          var renderMenu = $(this).data('autocomplete')._renderMenu;
          $(this).data('autocomplete')._renderMenu = function (ul, items) {
            renderMenu.apply(this, [ul, items]);
            fixpng(ul);
          };
        });
      };

      // video links impl
      $.behaviors.video_links = function (ctx, ajax) {
        if (!$.fn.flash) {
          return;
        }

        $('a.video_link:not(.video_link_processed)').each(function () {
          var self = $(this);
          self.click(function () {
            var options = $.extend(true, {}, F.global_get('player.standard'));
            options.flashvars.video_location = self.attr('rel');
            if ($.flash.available) {
              $('.gallery .viewer').empty().flash(options);
            }
            else {
              requires_flash_message('.gallery .viewer');
            }
            return false;
          });
        }).addClass('video_link_processed');

        $('a.video_link:first').click();
      };

      // VIDEO PLAYER DOCUMENTATION
      $.behaviors.video_player_support = function (ctx, ajax) {
        if (!$.fn.flash) return;
        if ($('.video_player_support:not(.video_player_processed)', ctx).size() > 0) {
          require(['includes/jquery.linkrel', 'includes/jquery.video.player'], function () {
            $('.video_player_support:not(.video_player_processed)', ctx).each(function () {
              $(this).enable_video_player();
            }).addClass('video_player_processed');
          });
        }
      };

      $.behaviors.slideshow_player_support = function (ctx, ajax) {
        if (!$.fn.flash) return;

        if ($('.slideshow_player_support:not(.slideshow_player_processed)').size() > 0) {
          require(['includes/jquery.linkrel'], function () {
            setTimeout(function () {

              $('.slideshow_player_support:not(.slideshow_player_processed)').each(function () {
                var self = $(this);
                var options = $.extend(true, {}, $.fn.linkrel.default_options, F.global_get('player.slideshow'));
                options.swf = options.swf + '?_=' + Math.random();
                // set the width and height according to the container
                var width = self.width();
                var height = self.height();
                if (width > 0 && height > 0) {
                  options.width = width;
                  options.height = height;
                }
                var url = self.attr('title');
                options.flashvars.xml_location = url;
                if ($.flash.available) {
                  self.empty().flash(options);
                  self.removeAttr('title');
                }
                else {
                  requires_flash_message(self);
                }
              });

            }, 0);
          });
        }
      };

      $.behaviors.gallery_links = function (ctx) {
        if (!$.fn.flash) {
          return;
        }
        // gallery links
        if ($('a.gallery_link', ctx).size() > 0) {
          require(['includes/jquery.jsonrel', 'includes/jquery.json-2.2'], function () {
            $('a.gallery_link', ctx).jsonrel({
              target: '.gallery .viewer',
              group: 'a.gallery_link'
            }).addClass('gallery_link_processed');
          });
        }
      };

      $.behaviors.model_gallery = function (ctx) {
        // gallery mouse over effects
        $('.thumbnails a', ctx).each(function () {
          if ($(this).hasClass('model_gallery_processed')) {
            return;
          }
          $(this).mouseover(function () {
            // blocking
            if ($(this).data('active')) {
              return;
            }
            $(this).data('active', true);
            $(this).find('img').stop().fadeTo('fast', 0.2);
          }).mouseout(function () {
            var $e = $(this);
            $(this).find('img').stop().fadeTo('fast', 1.0, function () {
              $e.data('active', false);
            });
          }).addClass('model_gallery_processed');
        });
      };

      $.behaviors.external_links = function (ctx) {
        // external links
        $('a[rel~="external"]').each(function () {
          if ($(this).hasClass('external_link_processed')) {
            return;
          }
          $(this).attr('target', '_blank').addClass('external_link_processed');
        });
      };

      // FIXME
      $.behaviors.load_hash_inner_page_link = function (ctx, ajax) {
        if (ajax) {
          return;
        }
        // load the page if hash exists
        if (W.location.hash && W.location.hash != '#video') {
          var hash = W.location.hash.substring(1);
          $('a[property~="act:ajaxload"][href="' + hash + '"]', 'ul.tabs_horizontal').click();
        }
      };

      // Fun easter stuff
      $.behaviors.egg0xdeadbeef = function (ctx, ajax) {
        if (ajax)
          return;

        // check background image
        var image = $('.background_image', ctx).css('background-image');
        if (image && image.match(/^url\(/)) {
          $('.wrapper + .cap_bottom_default, .wrapper_columns + .wrapper_bottom_cap', ctx).each(function () {
            var self = $(this),
                        clickOrder = 0;

            function fun_stuff() {
              var wrapper = self.prev();
              self.hide();
              wrapper[0].style.webkitTransition = '-webkit-transform 6.18s ease-in-out';
              wrapper[0].style.webkitTransform = 'rotate3d(1, 1, 1, 360deg) scale3d(0.01, 0.01, 0.01)';
              wrapper[0].style.webkitTransformOrigin = '0 0 0';
              wrapper[0].style.MozTransition = '-moz-transform 6.18s linear';
              wrapper[0].style.MozTransform = 'rotate(360deg) scale(0.01, 0.01)';
              wrapper[0].style.MozTransformOrigin = '0 0';
              wrapper[0].style.oTransition = '-o-transform 6.18s linear';
              wrapper[0].style.oTransform = 'rotate(360deg) scale(0.01, 0.01)';
              wrapper[0].style.oTransformOrigin = '0 0 0';
              wrapper[0].style.msTransition = '-ms-transform 6.18s linear';
              wrapper[0].style.msTransform = 'rotate(360deg) scale(0.01, 0.01)';
              wrapper[0].style.msTransformOrigin = '0 0';
              setTimeout(function () {
                wrapper[0].style.webkitTransform = '';
                wrapper[0].style.MozTransform = '';
                wrapper[0].style.oTransform = '';
                wrapper[0].style.msTransform = '';
                self.show();
              }, 6180);
            }

            self.click(function (event) {
              var offsetX = event.pageX - self.offset().left;
              if (clickOrder == 0 && offsetX < 20) {
                clickOrder = 1;
              }
              else if (clickOrder == 1 && offsetX > 960) {
                fun_stuff();
                clickOrder = 0;
              }
            });
          });
        }
      };

      $.behaviors.survey = function (ctx, ajax) {
        if (ajax) {
          return;
        }

        var surveyCookieName = "hasTakenSurvey";
        var surveyPageViewCookieName = "takeSurveyPageView";
        var surveyCookieValue = "YES";

        var survey_enabled = global_get('survey.enabled') || false;

        if (!survey_enabled || $.cookie(surveyCookieName) == surveyCookieValue) {
          return;
        }

        var pageView = parseInt($.cookie(surveyPageViewCookieName)) || 0;
        // if pageView is less than three, increase the variable by one and return (we don't need the survey)
        if (pageView < 3) {
          pageView = pageView + 1;
          $.cookie(surveyPageViewCookieName, pageView);
          return;
        }

        require(['tools/toolbox/toolbox.expose', 'tools/overlay/overlay'], function () {

          // attach css styles


          $.ajax({
            type: 'GET'
                    , global: false
                    , url: global_get('survey.css')
                    , dataType: 'text'
                    , success: function (data) {

                      var s = document.createElement('style');
                      s.setAttribute('type', 'text/css');
                      try {
                        s.appendChild(document.createTextNode(data));
                      }
                      catch (e) {
                        s.styleSheet.cssText = data;
                      }

                      document.getElementsByTagName('head')[0].appendChild(s);

                      $.ajax({
                        type: 'GET'
                            , global: false
                            , url: global_get('url.survey_window')
                            , success: function (data) {

                              var surveyId = $('input[name=SurveyID]');
                              surveyId = surveyId.val();


                              var enforced = true; // $('input[name=SurveyEnforced]').val();
                              var surveyLink = global_get('url.survey') + surveyId;

                              window.openSurvey = function () {
                                $('#survey').data('overlay').close();
                                var windowNow = window.open(surveyLink, "", "location=0,status=0,scrollbars=1,width=672px,height=800px,left=75px,top=75px");
                                $.cookie(surveyCookieName, surveyCookieValue, { expires: 30, path: '/' });
                              };

                              window.openLater = function () {
                                $('#survey').data('overlay').close();
                                $.cookie(surveyCookieName, surveyCookieValue, { expires: 7, path: '/' });
                              };

                              var openRandowWelcomeSurvey = function () {
                                if ($.cookie(surveyCookieName) == surveyCookieValue) {
                                  return;
                                }

                                //Check if user has already done the survey
                                if ($.cookie(surveyCookieName) != surveyCookieValue) {
                                  //Show Survey randomnly , frecuency 1 in 10
                                  var randomnumber = Math.floor(Math.random() * 10);
                                  if (randomnumber == 9) {
                                    setTimeout(openWelcomeSurvey, 1);
                                  }
                                }
                              };

                              window.openWelcomeSurvey = function () {
                                if ($.cookie(surveyCookieName) == surveyCookieValue) {
                                  return;
                                }

                                var overlay = $('<div>').attr('id', 'survey').addClass('popup-survey').append(data);
                                overlay.hide().appendTo('body');

                                Cufon.replace($('.fnat', overlay), { fontFamily: F.FONT_NATIONAL }, false);
                                Cufon.replace($('.btn', overlay), { fontFamily: F.FONT_NATIONAL_EXTRABOLD }, false);
                                F.forBrowser('msie', /^[678]\./, function () {
                                  $('.btn', overlay).mouseenter(function () {
                                    $(this).addClass('btn-hover');
                                  }).mouseleave(function () {
                                    $(this).removeClass('btn-hover');
                                  });
                                });

                                overlay.overlay({
                                  top: 160,
                                  fixed: false,
                                  mask: {
                                    color: '#7f7f7f'
                                            , loadSpeed: 200
                                            , opacity: 0.5
                                  }
                                        , load: true
                                })
                              };

                              if ($.cookie(surveyCookieName) == surveyCookieValue) {
                                return;
                              }

                              if (enforced) {
                                openWelcomeSurvey();
                              }
                              else {
                                openRandowWelcomeSurvey();
                              }
                            }
                      });


                    }
          });


        });


      };


      //$.behaviors.tablesorter = function(ctx, ajax) {
      //	$('#sort_province').tablesorter({
      //		cssAsc:'asc',
      //		cssDesc:'desc',
      //		cssHeader:'sort',
      //		headers: [
      //			{ sorter: false },
      //			{ sorter: 'text' },
      //			{ sorter: false },
      //			{ sorter: false },
      //			{ sorter: 'text' },
      //			{ sorter: 'digit' }
      //		],
      //		textExtraction: 'complex'
      //	}).bind('sortEnd', function(){
      //		$(this).find('tbody tr').removeClass().end()
      //			.find('tbody tr:first').addClass('first').end()
      //			.find('tbody tr:last').addClass('last bottom');
      //	});
      //};

      $.behaviors.body_resize = function (ctx, ajax) {
        if (ajax) {
          return;
        }
        var bodyHeight = $('body').height();

        var resize = function () {
          var w = $(window).width()
                    , h = $(window).height();

          w = w > 1000 ? w : 1000;
          $('html, body').width(w);

          if (h > bodyHeight) {
            $('html,body').height(h);
          }
          else {
            $('html,body').css('height', '');
          }
        };

        $(window).resize(resize);
      };


      $.behaviors.section_init = function (ctx, ajax) {
        if (ajax) {
          return;
        }

        // we got to make sure the init entry point is executed
        var func = function () {
          if (!F['section_init'] && retry > 0) {
            retry -= 1;
            setTimeout(func, 100);
            return;
          }

          if (retry == 0) {
            return;
          }

          F.section_init(ctx);
        };

        var retry = 5;

        setTimeout(func, 100);

      };

      $.behaviors.page_init = function (ctx, ajax) {
        if (ajax) {
          return;
        }

        // we got to make sure the init entry point is executed
        var func = function () {
          if (!F['page_init'] && retry > 0) {
            retry -= 1;
            setTimeout(func, 100);
            return;
          }
          if (retry == 0) {
            return;
          }

          F.page_init(ctx);
        };

        var retry = 5;

        setTimeout(func, 100);
      };

      var tracker_inited = false;

      $.behaviors.init_web_tracker = function (ctx, ajax) {
        if (typeof _gat != 'undefined' || ajax || tracker_inited) {
          return;
        }
        var _gaq = _gaq || [];

        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

      };

      $.behaviors.web_tracker = function (ctx, ajax) {
        if (ajax || tracker_inited) {
          return;
        }
        var run_count = 0;
        tracker_inited = true;

        var add_web_tracker = function () {
          if (typeof _gat == 'undefined' && run_count < 5) {
            run_count++;
            setTimeout(add_web_tracker, 400);
            return;
          }

          // create a new tracker and set the tracker
          var tracker = new Honda.WebTracker(null, _gat, global_get('tracker.account'));
          tracker_set(tracker);

          // Case 1: First load with Hash
          if (window.location.hash.match(/^#\//)) {
            var url = window.location.hash.substring(1);
            tracker.trackPage(url);
          }
          // Case 2: First load no Hash
          else {
            tracker.trackPage();
          }

          // Case 3: Click on the navigation
          $('a[property~="act:ajaxload"]').click(function () {
            var url = $(this).attr('href');
            if (url.match(/^\//)) {
              tracker.trackPage(url);
            }
          });

          // case X: special links check
          $('a.trackable').click(function () {
            var url = $(this).attr('href'), title = $(this).attr('title');
            tracker.trackEvent('Trackable Link', title || url);
          });

        };

        require(['includes/tracker'], function () {
          setTimeout(add_web_tracker, 500);
        });
      };

      $.behaviors.ajax_complete = function (ctx, ajax) {
        $('html').removeClass('ajax-loading').addClass('ajax-complete');
      };

      /* open black book page helper function */
      W.openBlackBook = function (language) {
        var url = '/tools/blackbook/blackbook.aspx?siteLanguage=' + language;
        W.open(url, 'mywindow');
        return true;
      };

      /* open test drive function */
      W.openTestDrive = function (modelname_en, modelname_fr, language) {
        var url = 'https://www.customerreach.ca/hondacm/scheduler/appointment2.aspx?txt_modelname_en={MODELNAME_EN}&txt_modelname_fr={MODELNAME_FR}&txt_language={LANGUAGE}';
        url = url.replace('{MODELNAME_EN}', encodeURI(modelname_en));
        url = url.replace('{MODELNAME_FR}', encodeURI(modelname_fr));
        url = url.replace('{LANGUAGE}', encodeURI(language));
        W.open(url, 'mywindow', 'width=748, height=770');
        return true;
      };


      if (F.edit_mode) {
        deps.push('includes/edit-mode');
      }

      $(D).ready(function () {
        require(deps, function () {
          F.execute_behaviors('ready', D, false, 2);
        });
      });

    }); // of require

    } // of function main()
  })(jQuery, window, document, window, Modernizr, require);

});                                                                                 // of require
