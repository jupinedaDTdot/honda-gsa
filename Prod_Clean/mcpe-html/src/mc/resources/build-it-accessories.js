{
  "keys": [
    "packages",
    "entertainment",
    "exterior",
    "interior"
  ],
  "packages": {
    "keys": [
      "52812_protection_package___black",
      "54396_aero_kit_package"
    ],
    "52812_protection_package___black": {
      "name": "Protection Package - Black",
      "price": 379.04,
      "residual": null,
      "description_title": "Protection Package - Black",
      "description_body": "The Protection Package for the 2012 Civic includes all season mats in black, trunk tray, and rear splash guards. Get a 10% discount when you purchase these items as part of the Protection Package. Price shown already discounted.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "protection_package___black_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "54396_aero_kit_package": {
      "name": "Aero Kit Package",
      "price": 1414.82,
      "residual": null,
      "description_title": "Aero Kit Package",
      "description_body": "Make the drive even more exciting with the addition of our Aero Kit that includes the front, rear, and side Skirts in a convienent package.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "aero_kit_package_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "exterior"
        ],
        "exterior": [
          "54519_front_skirt",
          "54566_rear_skirt",
          "54616_side_skirt"
        ]
      },
      "includes": null
    }
  },
  "entertainment": null,
  "exterior": {
    "keys": [
      "54426_body_side_moldings",
      "54519_front_skirt",
      "54566_rear_skirt",
      "54616_side_skirt",
      "53745_full_nose_mask",
      "53808_hood_edge_deflector",
      "53864_moonroof_visor",
      "54049_engine_block_heater",
      "54134_fog_lights",
      "54174_16__alloy_wheel",
      "54223_17__alloy_wheel",
      "54352_rear_splash_guards",
      "54476_touch_up_paint_pens",
      "54245_locking_wheel_nuts"
    ],
    "54426_body_side_moldings": {
      "name": "Body Side Moldings",
      "price": 307.94,
      "residual": null,
      "description_title": "Body Side Moldings",
      "description_body": "Add aerodynamic appeal to your Civic with Body Side Moldings.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "body_side_moldings_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "54519_front_skirt": {
      "name": "Front Skirt",
      "price": 394.94,
      "residual": null,
      "description_title": "Front Skirt",
      "description_body": "Further define the aerodynamic styling of your Civic with the addition of a Front Skirt.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "front_skirt_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "packages"
        ],
        "packages": [
          "54396_aero_kit_package"
        ]
      },
      "includes": null
    },
    "54566_rear_skirt": {
      "name": "Rear Skirt",
      "price": 404.94,
      "residual": null,
      "description_title": "Rear Skirt",
      "description_body": "Rear Skirts further define the aerodynamic styling of your Civic.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "rear_skirt_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "packages"
        ],
        "packages": [
          "54396_aero_kit_package"
        ]
      },
      "includes": null
    },
    "54616_side_skirt": {
      "name": "Side Skirt",
      "price": 614.94,
      "residual": null,
      "description_title": "Side Skirt",
      "description_body": "Make the drive even more exciting with the addition of Front, Rear and Side Skirts while adding to the aerodynamic styling of your Civic.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "side_skirt_thumbnail.jpg"
      },
      "excludes": {
        "keys": [
          "packages"
        ],
        "packages": [
          "54396_aero_kit_package"
        ]
      },
      "includes": null
    },
    "53745_full_nose_mask": {
      "name": "Full Nose Mask",
      "price": 214.44,
      "residual": null,
      "description_title": "Full Nose Mask",
      "description_body": "Dress up your Honda Complement the sportiness of your Civic with this custom-fitted Full Nose Mask, while helping to protect its front end from small stones and road debris. Cannot be installed with Hood Edge Deflector.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "full_nose_mask_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "53808_hood_edge_deflector": {
      "name": "Hood Edge Deflector",
      "price": 157.44,
      "residual": null,
      "description_title": "Hood Edge Deflector",
      "description_body": "Protect the hood of your Honda from small stones and other debris with a hood edge protector.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "hood_edge_deflector_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "53864_moonroof_visor": {
      "name": "Moonroof Visor",
      "price": 168.44,
      "residual": null,
      "description_title": "Moonroof Visor",
      "description_body": "This tinted acrylic Moonroof Wind Deflector helps reduce glare by providing a sunscreen effect and, when it's open, minimizes wind noise.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "moonroof_visor_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "54049_engine_block_heater": {
      "name": "Engine Block Heater",
      "price": 157.99,
      "residual": 225.0,
      "description_title": "Engine Block Heater",
      "description_body": "Don't let the cold Canadian winters stop you from enjoying your new car. The addition of the Engine Block Heater will help ensure a smooth start even on the coldest days. Includes Engine Block Heater Bracket Kit.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "engine_block_heater_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "54134_fog_lights": {
      "name": "Fog Lights",
      "price": 561.94,
      "residual": 700.0,
      "description_title": "Fog Lights",
      "description_body": "For even more admiring looks, the addition of Fog Lights will complement the driving fun of your Civic, while helping to increase visibility during adverse driving conditions.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "fog_lights_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "54174_16__alloy_wheel": {
      "name": "16\" Alloy Wheel",
      "price": 1107.76,
      "residual": null,
      "description_title": "16\" Alloy Wheel",
      "description_body": "Attract even more attention with these stunning 16\" Alloy Wheels structurally matched to the original vehicle design so that handling remains undisturbed. Machine Finish-Kaiser Silver",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "16__alloy_wheel_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "54223_17__alloy_wheel": {
      "name": "17\" Alloy Wheel",
      "price": 1211.76,
      "residual": null,
      "description_title": "17\" Alloy Wheel",
      "description_body": "Attract even more attention with these stunning 17\" Alloy Wheels structurally matched to the original vehicle design so that handling remains undisturbed. Painted Finish-Blade Silver",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "17__alloy_wheel_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "54352_rear_splash_guards": {
      "name": "Rear Splash Guards",
      "price": 96.94,
      "residual": null,
      "description_title": "Rear Splash Guards",
      "description_body": "A custom fit for your Honda, splash guards made from heavy duty, injection-moulded polymers help protect the rear quarter panels.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "rear_splash_guards_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "54476_touch_up_paint_pens": {
      "name": "Touch-Up Paint Pens",
      "price": 12.26,
      "residual": null,
      "description_title": "Touch-Up Paint Pens",
      "description_body": "Matched to your original paint colour, keep the Touch-Up Paint Pen on hand to conveniently cover minor scratches should they occur.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "touch_up_paint_pens_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    },
    "54245_locking_wheel_nuts": {
      "name": "Locking Wheel Nuts",
      "price": 75.74,
      "residual": null,
      "description_title": "Locking Wheel Nuts",
      "description_body": "Help protect the wheels and tires of your Honda against theft.",
      "assets": {
        "front": null,
        "back": null,
        "front_off_car": null,
        "back_off_car": null,
        "thumbnail": "locking_wheel_nuts_thumbnail.jpg"
      },
      "excludes": null,
      "includes": null
    }
  },
  "interior": {
    "keys": [
      "53687_ashtray_cup_holder_style",
      "52999_cargo_net",
      "54623_all_weather_floor_mats___black",
      "53389_trunk_tray",
      "53526_auto_day_night_mirror",
      "53676_interior_illumination___blue",
      "52866_cargo_hook"
    ],
    "53687_ashtray_cup_holder_style": {
      "name": "Ashtray Cup Holder Style",
      "price": 45.3,
      "description_title": "Ashtray Cup Holder Style",
      "description_body": "Front seat passengers can enjoy the added convenience of an ashtray.",
      "assets": {
        "thumbnail": "ashtray_cup_holder_style_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "52999_cargo_net": {
      "name": "Cargo Net",
      "price": 96.94,
      "description_title": "Cargo Net",
      "description_body": "From groceries to parcels, this convenient Cargo Net helps keep items secure and easy to retrieve from the trunk.",
      "assets": {
        "thumbnail": "cargo_net_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "54623_all_weather_floor_mats___black": {
      "name": "All-Weather Floor Mats - Black",
      "price": 159.44,
      "description_title": "All-Weather Floor Mats - Black",
      "description_body": "Protect the interior of your Civic with All Weather Floor Mats. They are durable, fade resistant and have deep water retaining ridges to protect the interior of your Civic.",
      "assets": {
        "thumbnail": "all_weather_floor_mats___black_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "53389_trunk_tray": {
      "name": "Trunk Tray",
      "price": 158.44,
      "description_title": "Trunk Tray",
      "description_body": "This durable Trunk Tray is easy to remove and clean, and is designed to protect the original carpeting in your Civic.",
      "assets": {
        "thumbnail": "trunk_tray_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "53526_auto_day_night_mirror": {
      "name": "Auto Day/Night Mirror",
      "price": 401.88,
      "description_title": "Auto Day/Night Mirror",
      "description_body": "The auto day/night mirror with compass helps to reduce the glare of bright headlights from the cars behind you. Price includes attachment.",
      "assets": {
        "thumbnail": "auto_day_night_mirror_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "53676_interior_illumination___blue": {
      "name": "Interior Illumination - Blue",
      "price": 194.94,
      "description_title": "Interior Illumination - Blue",
      "description_body": "Soft glow of light derive from the driver/passenger seat foot wells and illuminate the center console.",
      "assets": {
        "thumbnail": "interior_illumination___blue_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    },
    "52866_cargo_hook": {
      "name": "Cargo Hook",
      "price": 29.36,
      "description_title": "Cargo Hook",
      "description_body": "A Cargo hook provides an excellent way to store your groceries in your enviromentally friendly bags. Price is for one cargo hook.",
      "assets": {
        "thumbnail": "cargo_hook_thumbnail.jpg",
        "off_car": null
      },
      "excludes": null,
      "includes": null
    }
  }
}