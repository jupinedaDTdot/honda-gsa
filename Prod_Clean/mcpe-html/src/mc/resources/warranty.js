{
  "keys": [
    "comprehensive_199",
    "lease_207"
  ],
  "comprehensive_199": {
    "keys": [
      "_5_year_100000_km_200",
      "_6_year_100000_km_201",
      "_6_year_160000_km_202",
      "_7_year_130000_km_203",
      "_7_year_160000_km_204",
      "_7_year_200000_km_205",
      "_8_year_200000_km_206"
    ],
    "_5_year_100000_km_200": {
      "label": "* 5 Year/100,000 km",
      "price": 818.0
    },
    "_6_year_100000_km_201": {
      "label": "* 6 Year/100,000 km",
      "price": 918.0
    },
    "_6_year_160000_km_202": {
      "label": "* 6 Year/160,000 km",
      "price": 1308.0
    },
    "_7_year_130000_km_203": {
      "label": "* 7 Year/130,000 km",
      "price": 1248.0
    },
    "_7_year_160000_km_204": {
      "label": "* 7 Year/160,000 km",
      "price": 1408.0
    },
    "_7_year_200000_km_205": {
      "label": "* 7 Year/200,000 km",
      "price": 1748.0
    },
    "_8_year_200000_km_206": {
      "label": "* 8 Year/200,000 km",
      "price": 1988.0
    },
    "isExclusive": true,
    "allowMultipleSelection": false,
    "title": "Comprehensive"
  },
  "lease_207": {
    "keys": [
      "first_period_of_coverage_4_years_or_100000_km_208",
      "first_period_of_coverage_5_years_or_120000_km_209"
    ],
    "first_period_of_coverage_4_years_or_100000_km_208": {
      "label": "First Period of Coverage: 4 Years or 100,000 km*",
      "price": 568.0
    },
    "first_period_of_coverage_5_years_or_120000_km_209": {
      "label": "First Period of Coverage: 5 Years or 120,000 km*",
      "price": 778.0
    },
    "isExclusive": false,
    "allowMultipleSelection": true,
    "title": "Lease"
  }
}