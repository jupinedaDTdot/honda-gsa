﻿/*
 * jPlayer Plugin for jQuery JavaScript Library
 * http://www.happyworm.com/jquery/jplayer
 *
 * Copyright (c) 2009 - 2010 Happyworm Ltd
 * Dual licensed under the MIT and GPL licenses.
 *  - http://www.opensource.org/licenses/mit-license.php
 *  - http://www.gnu.org/copyleft/gpl.html
 *
 * Author: Mark J Panaghiston
 * Date: 20th December 2010
 */

package happyworm.jPlayer {
	import flash.events.Event;
	
	public class JplayerEvent extends Event {
		
		// The event strings must match those in the JavaScript's $.jPlayer.event object

		public static const JPLAYER_READY:String = "ready";
		public static const JPLAYER_RESIZE:String = "resize"; // Not applicable
		public static const JPLAYER_ERROR:String = "error";

		public static const JPLAYER_LOADSTART:String = "loadstart";
		public static const JPLAYER_PROGRESS:String = "progress";
		public static const JPLAYER_SUSPEND:String = "suspend"; // Not implemented
		public static const JPLAYER_ABORT:String = "abort"; // Not implemented
		public static const JPLAYER_EMPTIED:String = "emptied"; // Not implemented
		public static const JPLAYER_STALLED:String = "stalled"; // Not implemented
		public static const JPLAYER_PLAY:String = "play";
		public static const JPLAYER_PAUSE:String = "pause";
		public static const JPLAYER_LOADEDMETADATA:String = "loadedmetadata"; // MP3 has no equivilent
		public static const JPLAYER_LOADEDDATA:String = "loadeddata"; // Not implemented
		public static const JPLAYER_WAITING:String = "waiting"; // Not implemented
		public static const JPLAYER_PLAYING:String = "playing"; // Not implemented
		public static const JPLAYER_CANPLAY:String = "canplay"; // Not implemented
		public static const JPLAYER_CANPLAYTHROUGH:String = "canplaythrough"; // Not implemented
		public static const JPLAYER_SEEKING:String = "seeking";
		public static const JPLAYER_SEEKED:String = "seeked";
		public static const JPLAYER_TIMEUPDATE:String = "timeupdate";
		public static const JPLAYER_ENDED:String = "ended";
		public static const JPLAYER_RATECHANGE:String = "ratechange"; // Not implemented
		public static const JPLAYER_DURATIONCHANGE:String = "durationchange"; // Not implemented
		public static const JPLAYER_VOLUMECHANGE:String = "volumechange"; // See JavaScript

		// Events used internal to jPlayer's Flash.
		public static const DEBUG_MSG:String = "debug_msg";

		public var data:JplayerStatus;
		public var msg:String = ""

		public function JplayerEvent(type:String, data:JplayerStatus, msg:String = "", bubbles:Boolean = false, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
			this.data = data;
			this.msg = msg;
		}
		public override function clone():Event {
			return new JplayerEvent(type, data, msg, bubbles, cancelable);
		}
		public override function toString():String {
			return formatToString("JplayerEvent", "type", "bubbles", "cancelable", "eventPhase", "data", "msg");
		}
	}
}