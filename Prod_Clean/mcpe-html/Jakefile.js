// This is the build file for Honda MCPE system

var F = require('functools'),
    Path = require('path'),
    fs = require('fs');

// CONSTANTS
var SRC_SCRIPTS = 'src/_Global/js',
    SRC_STYLESHEETS = 'src/_Global/css',
    SRC_PHP = 'src';

var DIST_SCRIPTS = 'dist/_Global/js',
    DIST_STYLESHEETS = 'dist/_Global/css',
    DIST_HTML = 'dist';

var SCRIPTS = {
  'lib.js': [
    'jquery-1.4.4.js',
    'cufon.js'
  ],
  'fonts.js': [
  ],
  'fonts-fr.js': [
  ],
  'main.js': [
  ]
};

String.prototype.toTitleCase = function () {
	var A = this.split(' '), B = [];
	for (var i = 0; A[i] !== undefined; i++) {
		B[B.length] = A[i].substr(0, 1).toUpperCase() + A[i].substr(1);
	}
	return B.join(' ');
};

var ROOT = process.cwd();

console.log('*** Honda HTML/CSS build system ***');

// clean the dist directory
task('clean', [], function() {
  // remove everything in dist/
  fs.readdir('dist', function(err, files) {
    files.forEach(function(file) {
      switch (file) {
        case '.svn':
          break;
        default:
          rm (Path.join('dist', file), function() {});
          break;
      }
    });
  });
});

task('default', [], function() {
    var prefixes = [
            '^(landing)(-?(.+)\\.html)?',
            '^(compare)(-?(.+)\\.html)?',
            '^(gear-apparel)(-?(.+)\\.html)?',
            '^(build-price)(-?(.+)\\.html)?',
            '^(current-offers)(-?(.+)\\.html)?',
            '^(find-a-dealer)(-?(.+)\\.html)?',
            '^(help-me-choose)(-?(.+)\\.html)?',
            '^(parts-service)(-?(.+)\\.html)?',
            '^(honda-advantage)(-?(.+)\\.html)?',
            '^(product-listing)(-?(.+)\\.html)?',
            '^(product)(-?(.+)\\.html)?',
            '^(lease)(-?(.+)\\.html)?',
            '^(finance)(-?(.+)\\.html)?',
            '^(safety)(-?(.+)\\.html)?',
            '^(accessories)(-?(.+)\\.html)?',
            '^(news-events)(-?(.+)\\.html)?',
            '^(commercial-product)(-?(.+)\\.html)?'
    ];
    ['src/pe/', 'src/mc/', 'src/atv/', 'src/shared/'].forEach(function(folder) {
        var results = {};
        fs.readdir(folder, function(err, files) {
            if (err) {
                return;
            }
            files.forEach(function(filename) {
                prefixes.forEach(function(prefix) {
                    var r = new RegExp(prefix);
                    var m = filename.match(r);
                    if (m) {
                        var group = m[1];
                        if (!results[group]) {
                            results[group] = [];
                        }
                        var name = m[3] || m[1];
                        name = name.replace(/-/g, ' ').toTitleCase();

                        var fr_path = 'fr-' + filename,
                            fr_anchor = '<span>FR</span>&nbsp;';
                        try {
                             fr_anchor = fs.lstatSync(folder + fr_path).isFile() && ('<a href="'+fr_path+'" class="fr">FR</a>&nbsp;');
                        }
                        catch(e) {
//                            console.log(e)
                        }
                        results[group].push('<dd>'+fr_anchor+'<a href="'+filename+'">'+name+'</a></dd>');
                    }
                });
            });

            var content = '';
            for (var group in results) {
                var name = group.replace(/-/g, ' ').toTitleCase();
                content += '<dl>';
                content += '<dt>' + name + '</dt>';
                content += results[group].join('\n');
                content += '</dl>';
            }

            fs.readFile('src/index_template.html', function(err, data) {
                if (err) throw err;

                var r = data.toString().replace('<!-- LIST -->', content);
                fs.writeFileSync(folder + 'index.html', r);
            });
        });

    });
});

// javascript builder
namespace('js', function() {

  mkdirsSync(DIST_SCRIPTS, 0755);

  // build javascript file dependencies
  function build_js_dep(fileList) {
    return F.map(function(file) {
      return Path.join(SRC_SCRIPTS, file);
    }, fileList);
  }
  
  // javascript minifier
  function minify(code) {
    var ug = require('uglify-js');
    var jsp = ug.parser;
    var pro = ug.uglify;
    
    var ast = jsp.parse(code);
    ast = pro.ast_mangle(ast);
    ast = pro.ast_squeeze(ast);
    return pro.gen_code(ast);
  }
  
  // create tasks
  for (var target in SCRIPTS) {
    (function(target) {
      var file_list = build_js_dep(SCRIPTS[target]);
      var dst_file = Path.join(DIST_SCRIPTS, target);

      file(dst_file, file_list, function() {
        console.log('Building ' + target + ' ...');

        // read the source files
        var source_code = '';
        file_list.forEach(function(file) {

          source_code += fs.readFileSync(file, 'utf8');

        });

        var dst_code = minify(source_code);

        // write to destiation
        fs.writeFileSync(dst_file, dst_code, 'utf8');

        complete();

      }, true);
    })(target);
  }
});

// main builder
namespace('build', function() {
  
  mkdirsSync(DIST_STYLESHEETS, 0755);
  
  desc('Build CSS files');
  task('css', [], function() {
    complete();
  }, true);
  
  desc('Build JS files');
  // create js dependencies
  var js_deps = [];
  for (var target in SCRIPTS) {
    js_deps.push('js:' + Path.join(DIST_SCRIPTS, target));
  }

  task('js', js_deps, function() {
    complete();
  }, true);
  
  desc('Build PHP files');
  task('php', ['build:css', 'build:js'], function() {
    complete();
  }, true);
  
  task('all', ['build:php'], function() {
    
  });
  
});

/***** FUNCTIONS *****/
function mkdirsSync (dirname, mode) {
  if (mode === undefined) mode = 0777 ^ process.umask();
  var pathsCreated = [], pathsFound = [];
  var fn = dirname;
  while (true) {
    try {
      var stats = fs.statSync(fn);
      if (stats.isDirectory())
        break;
      throw new Error('Unable to create directory at '+fn);
    }
    catch (e) {
      if (e.errno === 2) {
        pathsFound.push(fn);
        fn = Path.dirname(fn);
      }
      else {
        throw e;
      }
    }
  }
  for (var i=pathsFound.length-1; i>-1; i--) {
    var fn = pathsFound[i];
    fs.mkdirSync(fn, mode);
    pathsCreated.push(fn);
  }
  return pathsCreated;
}

process.on("exit", function () {
  if (failedToRemove.length === 0) return
  sys.error("")
  console.log("The following files and folders could not be removed", "!")
  console.log("You should remove them manually.", "!")
  sys.error( "\nsudo rm -rf "
           + failedToRemove.map(JSON.stringify).join(" ")
           )
})

var waitBusy = {}
  , maxTries = 3
  , failedToRemove = []
function rm (p, cb_) {

  if (!p) return cb_(new Error("Trying to rm nothing?"))

  function cb (er) {
    if (er) {
      if (er.message.match(/^EBUSY/)) {
        // give it 3 tries
        // windows takes a while to actually remove files from folders,
        // leading to this odd EBUSY error when you try to rm a directory,
        // even if it's actually been emptied.
        if (waitBusy[p] === null) waitBusy[p] = maxTries
        if (waitBusy[p]) {
          waitBusy[p] --
          // give it 100 ms more each time.
          var time = (maxTries - waitBusy[p]) * 100
          return setTimeout(function () { rm(p, cb_) }, time)
        }
      }
      failedToRemove.push(p)
      console.log(p, "rm fail")
      console.log(er.message, "rm fail")
    } else delete waitBusy[p]
    cb_(null, er)
  }

  fs.lstat(p, function (er, s) {
    if (er) return cb()
    if (s.isFile() || s.isSymbolicLink()) {
      fs.unlink(p, cb)
    } else {
      fs.readdir(p, function (er, files) {
        if (er) return cb(er)
        ;(function rmFile (f) {
          if (!f) fs.rmdir(p, cb)
          else rm(Path.join(p, f), function (_, er) {
            if (er) return cb(er)
            rmFile(files.pop())
          })
        })(files.pop())
      })
    }
  })
}
