var fs = require('fs');

// answer set
var answers = {
    'lawnmowers': [
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B', 'C', 'D'],
        ['A', 'B'],
    ],
    'water-pump': [
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
    ],
    'trimmer': [
        ['A', 'B', 'C'],
        ['A', 'B', 'C', 'D'],
        ['A', 'B'],
        ['A', 'B'],
        ['A', 'B', 'C'],
    ],
    'tiller': [
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B', 'C', 'D'],
    ],
    'generator': [
        ['A', 'B', 'C'],
        ['A', 'B', 'C'],
        ['A', 'B'],
        ['A', 'B', 'C'],
        ['A', 'B'],
        ['A', 'B', 'C'],
    ]
};

function matrix_permute(fd, matrix, stack, answer_index, answer_count) {
    if (answer_index < answer_count) {
        for (var option_index = 0; option_index < matrix[answer_index].length; option_index++) {
            stack.push(matrix[answer_index][option_index]);
            matrix_permute(fd, matrix, stack, answer_index + 1, answer_count);
            stack.pop();
        }
    }
    else {
        // output the whole set
        fs.writeSync(fd, '"' + stack.join('","') + '"\n');
    }
}

for (var output_file in answers) {

    // get the max length of the answer_set items
    (function(output_file) {
        var answer_set = answers[output_file];
        fs.open(output_file + '.csv', 'w', function(err, fd) {
            if (err) {
                throw err;
            }

            console.log('creating ' + output_file);

            var max_length = 0;
            answer_set.forEach(function(item) {
                max_length = item.length > max_length ? item.length : max_length;
            });

            // create header
            fs.writeSync(fd, '"1"');
            for (var col = 1; col < answer_set.length; col ++) {
                fs.writeSync(fd, ',"' + (col + 1) + '"');
            }
            fs.writeSync(fd, "\n");

            matrix_permute(fd, answer_set, [], 0, answer_set.length);
        });
    })(output_file);
}