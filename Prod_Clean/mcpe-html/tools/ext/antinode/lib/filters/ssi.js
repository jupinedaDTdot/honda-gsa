var fs 		= require('fs'),
	path	= require('path'),
	events  = require('events'),
	vm      = require('vm'),
	F       = require('functools'),
	log     = require('../log'),
	fst     = require('./filterstream');


var filter = function(pathname, data, ondata, globals) {
	
	if ( ! data) {
		return ondata(data);
	}
	
	data = data.toString();
	
	data = data.replace(/"/gm, '\\"');
	data = '__write("' + data + '"); __end();';
	data = data.replace(/<!--#if expr=\\"([^"]+)\\"-->/gim, '"); if ($1) { __write("');
	data = data.replace(/<!--#elif expr=\\"([^"]+)\\"-->/gim, '"); } else if ($1) { __write("');
	data = data.replace(/<!--#else-->/gim, '"); else { __write("');
	data = data.replace(/<!--#endif-->/gim, '")} __write("');
	data = data.replace(/<!--#include (file|virtual)=\\"([^"]+)\\"-->/gim, '"); __include("$2","$1"); __write("');
	data = data.replace(/<!--#set var=\\"([^"]+)\\" value=\\"([^"]+)\\"-->/gim, '"); $1="$2"; __set("$1", "$2"); __write("');
	data = data.replace(/<!--#exec (cgi|cmd)=\\"([^"]+)\\"-->/gim, '"); __exec("$2","$1"); __write("');
	data = data.replace(/<!--#config (timefmt|sizefmt|errmsg)=\\"([^"]+)\\"-->/gim, '"); __config("$2","$1"); __write("');
	data = data.replace(/<!--#echo var=\\"([^"]+)\\"-->/gim, '"); __echo("$1"); __write("');
	data = data.replace(/<!--#(flastmod|fsize) (file|virtual)=\\"([^"]+)\\"-->/gim, '"); __$1("$3","$2")');
	data = data.replace(/<!--#printenv-->/gim, '"); __printenv(); __write("');
	data = data.replace(/\n/gm, '\\n');
	data = data.replace(/\r/gm, '\\r');
	
	if (globals === undefined) {
	    globals = {};
	}
	
	var buffer = '';
    var sandbox = {
		__write: function(value) {
			buffer += value;
		},
		__set: function(httpvar, value) {
            sandbox[httpvar] = value;
            globals[httpvar] = value;
		},
		__echo: function(httpvar) {
            buffer += sandbox[httpvar];
		},
		__include: function(filepath, type) {
			if (type == 'file') {
				filepath = path.join(path.dirname(pathname), filepath);
			}

			var content = fs.readFileSync(filepath);
			// do filtering iteratively
			filter(filepath, content, function(filtered_data) {
			    buffer += filtered_data;
			}, globals);
		},
		__exec: function(cmd, type) {

		   //TODO
		},
		__config: function(key, value) {

		   //TODO
		},
		__flastmod: function(file, type) {

		   //TODO
		},
		__fsize: function(file, type) {

		   //TODO
		},
		__end: function() {
			ondata(buffer);
		},
	};
	
  	vm.runInNewContext(data, sandbox);
}
module.exports.createFilterStream = function(pathname, headers) {
    return new fst.FilterStream(F.partial(filter, [pathname]), headers);
};
