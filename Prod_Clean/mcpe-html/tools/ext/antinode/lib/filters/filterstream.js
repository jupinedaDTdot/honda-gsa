var fs 		= require('fs'),
	path	= require('path'),
	events  = require('events'),
	vm      = require('vm'),
	F       = require('functools'),
	log     = require('../log');
	
var FilterStream = function(filter, headers) {
    
    var self = this;
    
    log.debug('SSI filter created.');

    this.readable = true;
    this.filter = filter;

    this._encoding = 'utf8';
    this._pipeDestination = null;
    this._pipeOptions = {
        pause: true,
    };
    
    this._pipeDataListener = function(data, encoding) {
        this._pipeDestination.write(data, encoding);
    };
    
    this._pipeEndListener = function() {
        this._pipeDestination.end();
        this._pipeDestination.destroy();
    };
    
    this._pipeCloseListener = function() {
        this._pipeDestination.destroy();
    };
    
    this._pipeErrorListener = function(err) {
        this._pipeDestination.emit('error', err);
    };
    
    this._data = '';
    this._buffer = null;
    this._offset = 0;
    
    // readable stream methods
    this.setEncoding = function(encoding) {
        this._encoding = encoding;
    };
    this.pause = function() {
        throw new Error('pause is not supported.');
    };
    this.resume = function() {
        throw new Error('resume is not supported.');
    };
    this.pipe = function(destination, options) {
        this._pipeDestination = destination;
        this._pipeOptions = options;
        this.addListener('data', this._pipeDataListener);
        this.addListener('end', this._pipeEndListener);
        this.addListener('close', this._pipeCloseListener);
        this.addListener('error', this._pipeErrorListener);
    };
    this.destroySoon = function() {
        // do nothing
    };
    
    // writable stream methods
    this.writable = true;
    this.write = function(data, encoding) {
        if (typeof data === 'object') {
            var buffer;
            
            // this.emit('data', data);
            
            if (self._buffer) {
                buffer = new Buffer(self._buffer.length + data.length);
                self._buffer.copy(buffer);
            }
            else {
                buffer = new Buffer(data.length);
            }
            
            data.copy(buffer, self._offset);
            self._offset += data.length;
            self._buffer = buffer;
            
            return true;
        }
        else if (typeof data === 'string') {
            if (typeof encoding === 'undefined') {
                encoding = self._encoding;
            }
            
            this._data += data;
            return true;
        }
        
        this.emit('error', new Error('Data is of invalid type: ' + typeof data));
    };
    this.end = function(data, encoding) {
        try {
            if (self._buffer) {
                if (data) {
                    self.write(data);
                }
                // deal with buffer
                self._data = self._buffer.toString(self._encoding);
                self.filter(self._data, function(filtered_data) {
                    var buf = new Buffer(filtered_data, self._encoding);
                    headers['Content-Length'] = buf.length;
                    self.emit('data', buf);
                    self.emit('end');
                });
            }
            else if (typeof data === 'string') {
                if (typeof encoding === 'undefined') {
                    encoding = self._encoding;
                }
                if (data) {
                    self.write(data, encoding);
                }
                self.filter(self._data, function(filtered_data) {
                    headers['Content-Length'] = filtered_data.length;
                    self.emit('data', self._data, encoding);
                    self.emit('end');
                });
            }
        }
        catch (e) {
            self.emit('error', e);
        }
        finally {
            self.destroy();
        }
        
    };
    
    // both stream methods
    this.destroy = function() {
        this.emit('close');
        this.readable = false;
        this.writable = false;
        this._data = null;
        this.removeAllListeners('data');
        this.removeAllListeners('end');
        this.removeAllListeners('error');
        this.removeAllListeners('close');
        this.removeAllListeners('fd');
        this.removeAllListeners('drain');
    };
};

FilterStream.prototype = new events.EventEmitter();

module.exports.FilterStream = FilterStream;
