#!/bin/bash

export NODE_PATH=`pwd`/tools/jake/lib:`pwd`/tools/UglifyJS/lib:`pwd`/tools/ext
if [ x$OS == xWindows_NT ]; then
  export PATH=`pwd`/tools/win32:$PATH
  export NODE=node.exe
else
  export PATH=`pwd`/tools:$PATH
  export NODE=node
fi
export JAKE="$NODE `pwd`/tools/jake/lib/jake.js"
echo `pwd`

$JAKE $*
# ROOT=$(dirname $(python -c 'import sys,os;print os.path.realpath(sys.argv[1])' $0))
# BUILD=$ROOT/requirejs/build/build.sh
# $BUILD $ROOT/../src/_Global/js/app.build.js

