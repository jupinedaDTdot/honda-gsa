@echo off
SET NODE_PATH=./tools/jake/lib:./tools/UglifyJS/lib:./tools/ext
SET PATH=.\tools\win32;%PATH%
SET NODE=node.exe
SET JAKE=%NODE% tools/jake/lib/jake.js

rem %JAKE% %1 %2 %3
CD ..
%JAKE%

SET BUILD=.\tools\requirejs\build\build.bat
CALL %BUILD% src\_Global\js\app.build.js

