#!/bin/bash
ROOT=$(dirname $(python -c 'import sys,os;print os.path.realpath(sys.argv[1])' $0))
cd $ROOT
mkdir -p ../nginx-temp/logs
mkdir -p ../nginx-temp/temp
cd ../nginx-temp
echo Development server started.
$ROOT/nginx/nginx -c $ROOT/nginx/conf/nginx.conf
