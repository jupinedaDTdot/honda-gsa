@echo off
mkdir ..\nginx-temp
mkdir ..\nginx-temp\logs
mkdir ..\nginx-temp\temp
cd ..\nginx-temp
echo Development server started.
..\tools\nginx\nginx.exe -c ..\tools\nginx\conf\nginx.conf
