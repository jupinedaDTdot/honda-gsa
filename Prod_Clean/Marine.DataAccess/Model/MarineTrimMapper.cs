﻿using System.Data;
using System.Data.SqlClient;

using HondaCA.Entity;
using HondaCA.DataAccess;
using HondaCA.Entity.Model;

namespace Marine.DataAccess.Model
{
    public class MarineTrimMapper : TrimMapper
    {
        public EntityCollection<Trim> SelectTrimWithBasePrice(string categoryURL, int targetID, string language)
        {
            StoredProcedureName = "[dbo].[pr_HondaCA_Trim_WithBasePrice_get]";
            IDataParameter param = new SqlParameter("BMCategoryURL", categoryURL);
            m_Parameters.Add(param);
            param = new SqlParameter("TargetID", targetID);
            m_Parameters.Add(param);
            param = new SqlParameter("Lang", language);
            m_Parameters.Add(param);
            return GetData();
        }
    }
}
