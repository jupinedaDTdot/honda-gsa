﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HondaCA.Common;
using Navantis.Honda.CMS.Demo;

namespace Marine
{
    public partial class TestPage : CmsContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.IsNotCMSPage = true;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = Language.French;
            base.InitializeCulture();
            Response.Write("resoruce text:" + this.ResourceManager.GetString("test"));
        }
    }
}