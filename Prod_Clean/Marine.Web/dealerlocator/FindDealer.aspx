﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FindDealer.aspx.cs" Inherits="Marine.Web.FindDealer" EnableViewState="false"  MasterPageFile="~/_Global/master/TwoColMaster.master" %>--%>
<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="false"  MasterPageFile="~/_Global/master/TwoColMaster.master" %>


<%--<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagName="FindADealer" TagPrefix="uc2" %>--%>

<asp:Content ID="contentHead" runat="server" ContentPlaceHolderID="head">
	<link rel="stylesheet" href="/_Global/css/sections/find-a-dealer.css" type="text/css" media="screen" charset="utf-8" />
	<link href="/_Global/css/marine/sections/find-a-dealer.css" rel="stylesheet" type="text/css" />
	<style type="text/css">v\:* { behavior: url(#default#VML); }</style>
    <script type="text/javascript" src="/_Global/js/ext/knockout-3.1.0.js"></script>
    <script src="/_Global/js/ext/knockout.validation.js" type="text/javascript"></script>
</asp:Content>

<%--<asp:Content ID="LeftColContent" ContentPlaceHolderID="Widgets" runat="server">
	<li class="find-a-dealer dn">
		<uc2:FindADealer ID="FindADealer1" runat="server" />
	</li>
	<li class="cities_nearby dn" id="liCitieNearYou" data-bind="visible:$data.HasData()">
		<div class="top_cap pf"></div>
		<div class="content pf">
			<h4><%= ResourceManager.GetString("txtCities_Near_You")%></h4>
			<ul id="left_nav" data-bind="foreach:$data.Cities">
			    <li>
			        <a data-bind="attr:{href : $data.Url()}">
			            <span class="cityName" data-bind="text: $data.Name"></span>
			        </a>
			    </li>

			</ul>
		</div>
		<div class="bottom_cap pf"></div>
	</li>
	<li class="powerhouse_dealers dn">
		<div class="top_cap pf"></div>
		<div class="content pf">
			<img src="<%= ResourceManager.GetString("pathPowerHouseImage2") %>" alt="" />
			<p class="heading"><asp:Literal ID="LiteralPowerHouseDealerHeader" runat="server"></asp:Literal></p>
			<asp:Literal ID="LiteralPowerHouseDealerBlurb" runat="server"></asp:Literal>
			<a href="<%= ResourceManager.GetString("urlPowerhouseDealers") %>" class="btn secondary"><span><%= ResourceManager.GetString("LearnMore") %></span></a>
		</div>
		<div class="bottom_cap pf"></div>
	</li>
</asp:Content>--%>


<%--<asp:Content ID="additional" runat="server" ContentPlaceHolderID="insertDivatBottom">
</asp:Content>

<asp:Content ID="JavaScript" ContentPlaceHolderID="IncludeJavaScripatEnd" runat="server">
    <script type="text/javascript" src="/_Global/js/includes/jquery.xdomainrequest.min.js"></script>
	<script type="text/javascript" src="/_Global/js/maps/googlemaps.js"></script>
    <script type="text/javascript" src="/_Global/js/ext/dealer/DealerHelper.js"></script>
    <script type="text/javascript" src="/_Global/js/ext/dealer/DealerCollectionController.js"></script>
    <script type="text/javascript" src="/_Global/js/ext/dealer/dealerlocator.js"></script>
    <script type="text/javascript">
        var __provinceName = "<%= _provinceName %>";
        var __postalCode = "<%= _postalCode %>";
        var __province = "<%= _province %>";
        var __city = "<%= _city %>";
        var __searchCity = "<%= _searchCity %>";
        var __notFound = "<%= _notFound %>";
        var __mode = "<%= _mode %>";
        var __userIP = "<%= UserIP %>";
        var __apiRoot = "<%= ConfigurationManager.AppSettings["getDealersUrl"] %>";
        var __lang = "<%=this.PageLanguage%>";
        var __isPowerHouseDealer = "<%=_isPowerHouseDealer%>";
        var __productLine = "<%= ResourceManager.GetString("HondaMarine") %>"; 
	</script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<%=ConfigurationManager.AppSettings["GoogleMapKey:Honda"]%>&sensor=false"> </script>
</asp:Content>--%>