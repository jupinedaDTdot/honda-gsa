﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/cmsOneColMaster.master" AutoEventWireup="true" EnableViewState="false" CodeBehind="DealerSearchOptions.aspx.cs" Inherits="Marine.Web.dealerlocator.DealerSearchOptions" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="stylesheet" href="/_Global/css/sections/find-a-dealer.css" type="text/css" media="screen" charset="utf-8" />
    <link href="/_Global/css/marine/sections/find-a-dealer.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="cap_top_default pf"></div>
	<div class="container pf">
        <script  type="text/javascript">
            function ValidateCity() {
                var x = document.getElementById("<%=txtCity1.ClientID%>");//ctl00_ContentArea_txtCity1   
                var val = x.value;
                if (val == "") {
                    alert("<%=ResourceManager.GetString("txtEmptySearchBoxMessage")%>");
                    x.focus();
                    return false;
                }
                return true;
            }
    
            function ValidatePostalCode() {
                var x = document.getElementById("<%=txtPostalCode.ClientID%>"); //ctl00_ContentArea_txtPostalCode
                var val = x.value;
                if (val == "") {
                   alert("<%=ResourceManager.GetString("txtEmptySearchBoxMessage")%>");
                    x.focus();
                    return false;
                }
                return true;
            }
        </script>
        <div class="content_container">
        <div class="content_section first_section tac">
            <h1><asp:Literal runat="server" ID="LitFindYourHondaDealer"></asp:Literal></h1>
            <h2 class="search-options"><%=ResourceManager.GetString("txtSearchOptions")%></h2>
            <div class="primary-box">
                <span class="top-left"></span>
                <span class="top-right"></span>
                <span class="bottom-left"></span>
                <span class="bottom-right"></span>
                <dl class="fc search-options">
                    <dd class="fl postal-code">
                        <p class="heading"><asp:Literal runat="server" ID="LitZipCode"></asp:Literal></p>
                        <div class="input_box">
                            <asp:TextBox name="postal_code" pattern="[a-zA-Z][0-9][a-zA-Z]( ?[0-9][a-zA-Z][0-9])?"
                                MaxLength="7" TabIndex="1" class="input_large" ID="txtPostalCode" runat="server"></asp:TextBox>
                        </div>
                        <div class="search_button_area">
                            <asp:LinkButton class="btn primary" TabIndex="2" ID="lnkBPostalCode" runat="server"
                                Text="Go" OnClick="lnkBPostalCode_OnClick" OnClientClick=" return ValidatePostalCode()"
                                data-type="submit" data-role="button"><span><%=ResourceManager.GetString("txtBeginSearch") %></span></asp:LinkButton>
                        </div>
                        <label for="fc phd_only" class="phd_only">
                            <asp:CheckBox ID="CheckBoxPhdPC" runat="server" />
                        </label>
                    </dd>
                    <dd class="fl divider"></dd>
                    <dd class="fl city">
                        <p class="heading"><asp:Literal runat="server" ID="LitCity"></asp:Literal></p>
                        <div class="input_box">
                            <asp:TextBox TabIndex="3" class="input_large dropdown_autocomplete dropdown_autocomplete_city"
                                ID="txtCity1" runat="server" name="city"></asp:TextBox>
                        </div>
                        <div class="search_button_area">
                            <asp:LinkButton class="btn primary" TabIndex="4" ID="lnkBCitySearch" runat="server"
                                OnClick="lnkBCitySearch_OnClick" OnClientClick=" return ValidateCity()"><span><%=ResourceManager.GetString("txtBeginSearch") %></span></asp:LinkButton>
                        </div>
                        <label for="fc phd_only" class="phd_only">
                            <asp:CheckBox ID="CheckBoxPhdCity" runat="server" />
                        </label>
                    </dd>
                    <asp:Panel ID="Panel1" runat="server" Visible="false">
                    <dd class="fl divider"></dd>
                    <dd class="fl region pf">
                        <p class="heading"><asp:Literal runat="server" ID="LitRegion"></asp:Literal></p>
                        <select name="RegionSelectBox" onchange="form.submit()"
                             data-fieldselect-pane-class="field-select-dropdown" 
                             data-fieldselect-flexible-width="true" 
                             data-fieldselect-button-class="pf" 
                             property="act:fieldselect">
                            <asp:Literal ID="LiteralRegionSelectOptions" runat="server"></asp:Literal>
                        </select>
                        <div class="dn">
                            <asp:Literal ID="LiteralHidddenDealerList" runat="server"></asp:Literal>
                        </div>
                    </dd>
                    </asp:Panel>
                </dl>
            </div>
        </div>
        <div class="fc content_section powerhouse-dealers">
            <uc:ElementControl ID="ElementControl1" runat="server" ElementName="GenericContent_FFH" />
            <asp:Literal ID="LiteralPowerHouseFFH" runat="server"></asp:Literal>
            <%--<div class="fl powerhouse-building"><img src="<%=ResourceManager.GetString("imgPowerhouseBuilding") %>" alt="" /></div>
            <div class="fl content">
                <p class="heading"><%=ResourceManager.GetString("HondaPowerHouseDealer") %></p>
                <%=ResourceManager.GetString("txtPowerhouseDealerBlurbLanding") %>
            </div>
            <div class="fl powerhouse-logo"><img src="<%=ResourceManager.GetString("imgPowerhouseLogo") %>" alt="" /></div>--%>
        </div>
        </div>
    </div>
	<div class="cap_bottom_default pf"></div>
</asp:Content>