﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HondaCA.Entity.Common;
using HondaCA.Entity;
using HondaCA.Common;
using Navantis.Honda.CMS.Demo;

namespace Marine.Web
{
    public partial class GetCityService : CmsContentBasePage  //BasePage
    {
      
      
        protected override void OnInit(EventArgs e)
        {
            IsNotCMSPage = true;
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //CityServiceProxy cs = new CityServiceProxy();
            string lang = Request.QueryString["L"] != null ? Request.QueryString["L"] : "";
            lang = lang.Length > 2 ? lang.Substring(0, 2) : lang;
            if (lang.ToUpper() == Language.French.GetCultureStringValue().Substring(0, 2).ToUpper())
                this.PageLanguage = Language.French;
            else
                this.PageLanguage = Language.English;
            base.InitializeCulture();
          
            string city = Request.QueryString["sCity"] != null ? Request.QueryString["sCity"].Trim() : "";
            string province;//= Request.QueryString["sProvince"] != null ? Request.QueryString["sProvince"].Trim() : "";
            string strrows = HondaCA.Common.Global.CityServiceRowCount;//Request.QueryString["rows"] != null ? Request.QueryString["rows"] : "0";


            int rows;
            Int32.TryParse(strrows, out rows);
            //City [] cityList=  cs.GetCityList(city, province, rows);
            HondaCA.Service.Common.CityService cityservice = new HondaCA.Service.Common.CityService();
           
            if (city.Contains(","))
            {
                province = city.Substring(city.IndexOf(",")+1, city.Length - city.IndexOf(",")-1).Trim();
                city = city.Substring(0, city.IndexOf(",") ).Trim();
            }
            else
                province = "";
            
          HondaCA.Entity.EntityCollection<HondaCA.Entity.Common.City> cityList = cityservice.GetCitiesByPartOfCityProvince(city, province,rows,PageLanguage);
            foreach (HondaCA.Entity.Common.City obj in cityList)
            {
               Response.Write(obj.Name+ "\n");
            }
        }

    }
}

