﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using HondaCA.Common;
using HondaDealer.Service;
using HondaDealer.Entity;
using System.Web.UI.HtmlControls;
using System.Configuration;
using HondaCA.WebUtils.StringUtility;
using Newtonsoft.Json;

namespace Marine.Web.dealerlocator
{
	public partial class DealerDetail : Navantis.Honda.CMS.Demo.CmsContentBasePage//BasePage
	{
		protected int dealerID;

		DealerInformation dealerInfo;
		protected string dealerName, city, province, country;
		bool hasValueSalesH = false, hasValuePartsH = false, hasValueServiceH = false;
		protected string ProductLineCodePowerEquipment = "p";

		protected override void OnInit(EventArgs e)
		{
			IsNotCMSPage = true;
			base.OnInit(e);

			var ctrlBody = (HtmlControl)this.Page.Master.Master.FindControl("body");
			if (ctrlBody != null)
				ctrlBody.Attributes["class"] = ctrlBody.Attributes["class"] == null ? "page-find-a-dealer-details-1" : " page-find-a-dealer-details-1";
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			string lang = Request.QueryString["L"] ?? "";
			lang = lang.Length > 2 ? lang.Substring(0, 2) : lang;
			if (lang.ToUpper() == Language.French.GetCultureStringValue().Substring(0, 2).ToUpper())
				this.PageLanguage = Language.French;
			else
				this.PageLanguage = Language.English;

			base.InitializeCulture();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			LiteralPowerHouseDealerHeader.Text = ResourceManager.GetString("txtPowerHouseDealers");
			LiteralPowerHouseDealerBlurb.Text = ResourceManager.GetString("txtPowerHouseDealerBlurb");
			this.ToggleURL = ResourceManager.GetString("DealerLocatorRootFolderToggle");
			country = ConfigurationManager.AppSettings["Country"];

			if (!IsPostBack)
			{
				dealerName = Request.QueryString["DealerName"] ?? "";
				city = Request.QueryString["City"] ?? "";
				province = Request.QueryString["Province"] ?? "";

				if (dealerName != string.Empty && city != string.Empty && province != string.Empty)
				{
					setPage();
					setRepDealersHour();
				}
				//Book a test drive
			}
		}

		protected void Page_PreRender(Object sender, EventArgs E)
		{
			if (dealerID != 0)
			{
				RepeaterItem itemSale = getLastItemofRepeator(rptDealerSalesHour, hasValueSalesH);
				RepeaterItem itemParts = getLastItemofRepeator(rptDealerPartsHour, hasValuePartsH);
				RepeaterItem itemService = getLastItemofRepeator(rptDealerServiceHour, hasValueServiceH);
				if (hasValueSalesH) addAtttoRowToRep(itemSale, "tr_rpt", "class", "last bottom");
				if (hasValuePartsH) addAtttoRowToRep(itemParts, "tr_rpt", "class", "last bottom");
				if (hasValueServiceH) addAtttoRowToRep(itemService, "tr_rpt", "class", "last bottom");
			}
		}

		protected void setPage()
		{
			DealerInfoService ds = new DealerInfoService();
			RegionService rs = new RegionService();

			string tmpProvince = "";
			tmpProvince = HondaDealer.Service.DealerLocatorCommon.RepRevDealerSearch(province);
			province = rs.GetProvinceCodeByUrlName(province, PageLanguage.GetCultureStringValue());//rs.GetProvinceCodeByName(CommonFunction.RepRevDealerSearch(province), PageLanguage.GetCultureStringValue());
			city = HondaDealer.Service.DealerLocatorCommon.RepRevDealerSearch(city);

			dealerInfo = ds.GetDealerByName(city, province, dealerName, (HondaDealer.Common.Language)PageLanguage);

			if (dealerInfo != null)
			{
				dealerID = dealerInfo.DealerID;

				#region title
				this.Title = string.Format(ResourceManager.GetString("DealerDetailPageTitle"), dealerInfo.DealerName, dealerInfo.City, tmpProvince, country);

				HtmlMeta metaKeywords = new HtmlMeta();
				metaKeywords.Name = "keywords";

				metaKeywords.Content = string.Format(ResourceManager.GetString("DealerDetailMetaKeywords"), dealerInfo.DealerName, dealerInfo.City);
				Header.Controls.Add(metaKeywords);

				HtmlMeta metaDescription = new HtmlMeta();
				metaDescription.Name = "discription";
				metaDescription.Content = string.Format(ResourceManager.GetString("DealerDetailMetaKeywordDesc"), dealerInfo.DealerName, dealerInfo.Street, dealerInfo.City, tmpProvince);

				Header.Controls.Add(metaDescription);

				#endregion

				var location = new
				{
					dealerInfo.Latitude,
					dealerInfo.Longitude,
					dealerInfo.DealerName,
					dealerInfo.Street,
					dealerInfo.City,
					dealerInfo.Province,
					dealerInfo.PostalCode,
					dealerInfo.MainPhone,
					//DealerUrl = dealerUrl,
					//DealerImage = dealerImageUrl
					Clickable = false
				};
				var locations = new List<object>() { location };
				MapDataLocations.InnerText = JsonConvert.SerializeObject(locations);

				var viewArea = new
				{
					nelat = dealerInfo.Latitude,
					nelng = dealerInfo.Longitude,
					swlat = dealerInfo.Latitude,
					swlng = dealerInfo.Longitude,
					miniView = false
				};
				MapDataView.Value = JsonConvert.SerializeObject(viewArea);

				LitCities_Near_You.Text = ResourceManager.GetString("txtHondaDealerShip");
				rptCityList.DataSource = new CityService().GetDealerCitiesByZip(dealerInfo.PostalCode.Replace(" ", ""), (HondaDealer.Common.Language)PageLanguage);
				rptCityList.DataBind();

				#region "setLiteral"

				littxtSales.Text = ResourceManager.GetString("txtSales");
				littxtParts.Text = ResourceManager.GetString("txtParts");
				littxtService.Text = ResourceManager.GetString("txtService");

				//Update code for display email address if relative email address found then display it otherwise display default email.
				litSalesPhoneFax.Text = getDealerServiceTypePhoneFax(dealerInfo.MainPhone, dealerInfo.Fax, dealerInfo.MainPhone, string.IsNullOrEmpty(dealerInfo.SalesEmail) ? dealerInfo.Email : dealerInfo.SalesEmail);
				litPartsPhoneFax.Text = getDealerServiceTypePhoneFax(dealerInfo.PartsPhone, dealerInfo.Fax, dealerInfo.MainPhone, string.IsNullOrEmpty(dealerInfo.PartsEmail) ? dealerInfo.Email : dealerInfo.PartsEmail);
				litServicePhoneFax.Text = getDealerServiceTypePhoneFax(dealerInfo.ServicePhone, dealerInfo.Fax, dealerInfo.MainPhone, string.IsNullOrEmpty(dealerInfo.ServiceEmail) ? dealerInfo.Email : dealerInfo.ServiceEmail);

				DealerHoursService dhs = new DealerHoursService();
				litSalesNotes.Text = dhs.getDealerHoursNotesByOperationType(dealerID, DealerHoursService.DealerOperationType.Sales, (HondaDealer.Common.Language)PageLanguage);
				litPartsNotes.Text = dhs.getDealerHoursNotesByOperationType(dealerID, DealerHoursService.DealerOperationType.Parts, (HondaDealer.Common.Language)PageLanguage);
				litServiceNotes.Text = dhs.getDealerHoursNotesByOperationType(dealerID, DealerHoursService.DealerOperationType.Service, (HondaDealer.Common.Language)PageLanguage);
				if (System.IO.File.Exists(Server.MapPath(string.Format(@"{0}/{1}_{2}_dealer_image.png", ConfigurationManager.AppSettings["DealerImagePath"] ?? string.Empty, dealerInfo.DealerCode, dealerInfo.DealerUrlName))))
				{
					imgDealerImage.Src = string.Format(@"{0}/{1}_{2}_dealer_image.png?width=340&height=205", ConfigurationManager.AppSettings["DealerImagePath"] ?? string.Empty, dealerInfo.DealerCode, dealerInfo.DealerUrlName);
				}
				else
				{
					imgDealerImage.Src = string.Format(@"{0}/{1}?width=340&height=205", ConfigurationManager.AppSettings["DealerImagePath"] ?? string.Empty, ConfigurationManager.AppSettings["dealer_default_image"] ?? "R_dealer_default_image.png");
				}

				LitDealerName.Text = dealerInfo.DealerName;
				LitAddress.Text = ResourceManager.GetString("txtAddress");
				setDealerAddress(dealerInfo);

				if (dealerInfo.Directions != null)
				{
					if (dealerInfo.Directions.Trim() != "")
					{
						LitDirection.Text = ResourceManager.GetString("txtDirection");
						LitDirectionText.Text = dealerInfo.Directions;
					}
				}
				#endregion

				//if (dealerInfo.IsExpressDealer || dealerInfo.Award)
				//{
				//    divAboutDealer.Visible = true;
				//    LitaboutLabel.Text = string.Format(ResourceManager.GetString("txtAbout"), dealerInfo.DealerName);
				//    if (dealerInfo.IsExpressDealer) setExpressDealer();
				//    if (dealerInfo.Award) setQualityDealer();
				//    else divQualityDealer.Visible = false;
				//}

				//Product Lines ::: Code commented for old dealer detail category.
				//DealerProductLineService ProductLineService = new DealerProductLineService();
				//EntityCollection<DealerProductLine> ProductLines = ProductLineService.GetDealerProductLinesByDealerID(dealerInfo.DealerID, (HondaDealer.Common.Language)PageLanguage);

				////::::::::::::Remove this code below this line if ATV is not required to show as a different Product Line
				////Look for Motorcycle Product Line. It is alos same as ATV product line. So Duplicate it.
				//DealerProductLine tmpProductLine = new DealerProductLine();

				//if (ProductLines.SingleOrDefault(item => item.ProductLineCode == DealerProductLineService.ProductLineCodeMotorcycle) != null)
				//{
				//    tmpProductLine.ProductLineName = ResourceManager.GetString("txtATVs");
				//    tmpProductLine.ProductLineCode = "V";
				//}
				//ProductLines.Add(tmpProductLine);
				//::::::::::::Remove this code above this line if ATV is not required to show as a different Product Line


				////New Code ::::::Sep 26, 2011::::Dealer display belogning product category.

				ProductCategoryService prodCatService = new ProductCategoryService();
				EntityCollection<ProductCategory> prodCategories = prodCatService.GetProductCategoryByDealerID(dealerInfo.DealerID, this.PageLanguage.GetCultureStringValue());

				List<ProductCategory> ProductlineCategories = new List<ProductCategory>();

				var groupCategories = from ProCategories in prodCategories group ProCategories by ProCategories.ProductLineCode into grp select new { ProductLineCode = grp.Key, Category = grp };

				foreach (var item in groupCategories)
				{
					ProductCategory pitem = new ProductCategory();
					pitem.ProductLineCode = item.ProductLineCode;
					foreach (var cat in item.Category)
					{
						pitem.ProductLineName = cat.ProductLineName;
						pitem.ProductCategoryName += (string.IsNullOrEmpty(pitem.ProductCategoryName) ? "" : ", ") + cat.ProductCategoryName;
					}
					ProductlineCategories.Add(pitem);
				}

				if (ProductlineCategories != null)
				{
					divProductLine.Visible = ProductlineCategories.Count > 0 ? true : false;
					RepeaterProductLine.DataSource = ProductlineCategories;
					RepeaterProductLine.DataBind();
				}
				else
				{
					divProductLine.Visible = false;
				}
				if (dealerInfo.IsMUVAuthorizedDealer)
				{
					LiteralBigRedDealer.Text = ResourceManager.GetString("txtAuthorirzedBigRedDealer");
					LiteralBigRedDealer.Visible = true;
				}
				else
				{
					LiteralBigRedDealer.Visible = false;
				}

				try
				{
					if (dealerInfo.CaliberName.ToLower().Contains(DealerInfoService.DealerCaliberPowerhouse.ToLower()))
					{
						divPowerhouseLogo.Visible = true;
					}
					else
					{
						divPowerhouseLogo.Visible = false;
					}
				}
				catch (Exception)
				{
					divPowerhouseLogo.Visible = false;
				}

				if (!string.IsNullOrEmpty(dealerInfo.DealerInfo))
				{
					divDealerBlurb.Visible = true;
					imgDealerImage.Src = "/_Global/img/layout/" + ResourceManager.GetString("PowerhouseDealerImage");
					LitQualityDealerText.Text = dealerInfo.DealerInfo;
					LitaboutLabel.Text = string.Format(ResourceManager.GetString("txtAboutThisDealer"), dealerInfo.DealerName);
				}
				else
				{
					divDealerBlurb.Visible = false;
				}
			}
		}

		protected void RepeaterProductLine_ItemDataBound(Object sender, RepeaterItemEventArgs e)
		{
			//DealerProductLine objProductLine = (DealerProductLine)e.Item.DataItem;
			ProductCategory objProductCategory = (ProductCategory)e.Item.DataItem;
			HtmlControl ddProductLine = (HtmlControl)e.Item.FindControl("ddProductLine");
			Literal LiteralProdutLineName = (Literal)e.Item.FindControl("LiteralProdutLineName");
			Literal LiteralProductLineDescription = (Literal)e.Item.FindControl("LiteralProductLineDescription");

			switch (objProductCategory.ProductLineCode)
			{
				case DealerProductLineService.ProductLineCodeEngines:
					ddProductLine.Attributes.Add("class", string.Format("{0}{1}", "engines", e.Item.ItemIndex == 0 ? " first" : ""));
					LiteralProductLineDescription.Text = ResourceManager.GetString("txtEnginesLine");
					break;
				case DealerProductLineService.ProductLineCodeMarine:
					ddProductLine.Attributes.Add("class", string.Format("{0}{1}", "marine", e.Item.ItemIndex == 0 ? " first" : ""));
					LiteralProductLineDescription.Text = ResourceManager.GetString("txtMarineLine");
					break;
				case DealerProductLineService.ProductLineCodeMotorcycle:
					ddProductLine.Attributes.Add("class", string.Format("{0}{1}", "motorcycles", e.Item.ItemIndex == 0 ? " first" : ""));
					LiteralProductLineDescription.Text = ResourceManager.GetString("txtMotorcycleProductLine");
					break;
				case DealerProductLineService.ProductLineCodePowerEquipment:
					ddProductLine.Attributes.Add("class", string.Format("{0}{1}", "power-equipment", e.Item.ItemIndex == 0 ? " first" : ""));
					LiteralProductLineDescription.Text = ResourceManager.GetString("txtPowerEquipmentLine");
					break;
				//::::::::::::Remove this code below this line if ATV is not required to show as a different Product Line
				case DealerProductLineService.ProductLineCodeATV:
					ddProductLine.Attributes.Add("class", string.Format("{0}{1}", "atvs", e.Item.ItemIndex == 0 ? " first" : ""));
					LiteralProductLineDescription.Text = ResourceManager.GetString("txtAtvProductLine");
					break;
				case DealerProductLineService.ProductLineCodeSXS:
					ddProductLine.Attributes.Add("class", string.Format("{0}{1}", "sxs", e.Item.ItemIndex == 0 ? " first" : ""));
					LiteralProductLineDescription.Text = ResourceManager.GetString("txtSXSProductLine");
					break;

				//::::::::::::Remove this code above this line if ATV is not required to show as a different Product Line
			}

			LiteralProductLineDescription.Text = objProductCategory.ProductCategoryName;
			LiteralProdutLineName.Text = objProductCategory.ProductLineName;
		}

		protected string getDealerServiceTypePhoneFax(string phone1, string fax, string mainPhone, string email)
		{
			string phonefax = "";
			string formatText = "<strong>{0}</strong><span>{1}</span><br />";

			phone1 = StringUtility.formatPhoneNo(phone1 ?? string.Empty);
			fax = StringUtility.formatPhoneNo(fax ?? string.Empty);
			mainPhone = StringUtility.formatPhoneNo(mainPhone ?? string.Empty);

			if (!string.IsNullOrEmpty(phone1)) phonefax = string.Format(formatText, "P: ", phone1);
			else if (!string.IsNullOrEmpty(mainPhone)) phonefax = string.Format(formatText, "P: ", mainPhone);
			if (!string.IsNullOrEmpty(fax)) phonefax += string.Format(formatText, "F: ", fax);
			if (!string.IsNullOrEmpty(email)) phonefax += string.Format(formatText, "E: ", "<a href='mailto:" + email + "'>" + email + "</a>");
			return phonefax;
		}

		//protected void  setExpressDealer()
		//{
		//    divExpressDealer.Visible = true;
		//    imgExpressDealer.Src = "/_Global/img/layout/" + ResourceManager.GetString("ExpressDealerImage");
		//    imgExpressDealer.Alt = "NoImage";
		//    LitExpressDealerHeader.Text = ResourceManager.GetString("ExpressDealerHeader");
		//    LitExpressDealerText.Text = ResourceManager.GetString("ExpressDealer");
		//}

		//protected void  setQualityDealer()
		//{
		//    divQualityDealer.Visible = true;
		//    imgQualityDealer.Src = "/_Global/img/layout/" + ResourceManager.GetString("QualityDealerImage");
		//    imgQualityDealer.Alt = "NoImage";
		//    LitQualityDealerHeader.Text = ResourceManager.GetString("QualityDealerHeader");
		//    LitQualityDealerText.Text = ResourceManager.GetString("QualityDealer");
		//}

		protected void setDealerAddress(DealerInformation objDealerInfo)
		{
			string ph = "<strong>P: </strong> <span class='tel'><span class='type dn'>work</span> {0}</span><br />";
			string fx = "<strong>F: </strong> <span class='tel'><span class='type dn'>fax</span> {0}</span><br />";
			string em = "<strong>E: </strong> <a href='mailto:{0}' class='email'>{0}</a><br />";
			string web = "<strong>W: </strong> <a class='url external external_link_processed' rel='external' target='_blank' href='http://{0}'>{0}</a><br />";

			LitStreet.Text = objDealerInfo.Street;
			LitLocality.Text = objDealerInfo.City;
			LitPostalCode.Text = objDealerInfo.PostalCode;
			LitRegionCode.Text = objDealerInfo.Province;
			//abbrRegion.TagName = objDealerInfo.Province; 

			if (!(objDealerInfo.MainPhone == null || objDealerInfo.MainPhone.Trim() == string.Empty)) LitPhone.Text = string.Format(ph, StringUtility.formatPhoneNo(objDealerInfo.MainPhone ?? string.Empty));
			if (!(objDealerInfo.Fax == null || objDealerInfo.Fax.Trim() == string.Empty)) LitFax.Text = string.Format(fx, StringUtility.formatPhoneNo(objDealerInfo.Fax ?? string.Empty));
			if (!(objDealerInfo.Email == null || objDealerInfo.Email.Trim() == string.Empty)) LitEmail.Text = string.Format(em, objDealerInfo.Email);
			if (!(objDealerInfo.HomePage == null || objDealerInfo.HomePage.Trim() == string.Empty)) LitWeb.Text = string.Format(web, objDealerInfo.HomePage);
		}

		protected void rptCityList_ItemDataBound(Object sender, RepeaterItemEventArgs e)
		{
			var objCity = (HondaDealer.Entity.City)e.Item.DataItem;
			if (objCity != null)
			{
				var li = ((HtmlGenericControl)e.Item.FindControl("li"));
				var hypCity = ((HyperLink)e.Item.FindControl("hypCity"));

				hypCity.NavigateUrl = ResourceManager.GetString("DealerLocatorRootFolder") + "/" + objCity.ProvinceUrlName.ToLower() + "/" + DealerLocatorCommon.RepDealerSearch(objCity.Name).ToLower();

				if (PageLanguage == Language.French)
					hypCity.ToolTip = ResourceManager.GetString("sHondaDealers") + " " + objCity.Name;
				else
					hypCity.ToolTip = objCity.Name + " " + ResourceManager.GetString("sHondaDealers");

				hypCity.Text = (objCity.Name ?? string.Empty).ToUpper();
				if (e.Item.ItemIndex == 0) 
					li.Attributes.Add("class", "first");
			}
		}

		protected void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.DataItem != null)
			{
				KeyValuePair<string, string>? objDealerHour = (KeyValuePair<string, string>)e.Item.DataItem;
				if (objDealerHour != null)
				{
					Literal LitDay = ((Literal)e.Item.FindControl("LitDay"));
					Literal LitHours = ((Literal)e.Item.FindControl("LitHours"));
					HtmlTableRow tr = ((HtmlTableRow)e.Item.FindControl("tr_rpt"));
					if (this.PageLanguage == Language.French)
						LitDay.Text = objDealerHour.HasValue ? getDaysNameforFrench(objDealerHour.Value.Key.ToLower()) : "";
					else
						LitDay.Text = objDealerHour.HasValue ? objDealerHour.Value.Key : "";
					LitHours.Text = objDealerHour.HasValue ? objDealerHour.Value.Value : ""; ;
					if (e.Item.ItemIndex == 0) tr.Attributes.Add("class", "first top");
				}
			}
		}

		string getDaysNameforFrench(string dayName)
		{
			string frdayName = "";

			switch (dayName)
			{
				case "monday":
					frdayName = "Lundi";
					break;
				case "tuesday":
					frdayName = "Mardi";
					break;
				case "wednesday":
					frdayName = "Mercredi";
					break;
				case "thursday":
					frdayName = "Jeudi";
					break;
				case "friday":
					frdayName = "Vendredi";
					break;
				case "saturday":
					frdayName = "Samedi";
					break;
				case "sunday":
					frdayName = "Dimanche";
					break;
			}
			return frdayName;
		}

		protected void setRepDealersHour()
		{
			DealerHoursService dhs = new DealerHoursService();

			var SalesCollection = dhs.getDealerHourstByOperationType(dealerID, DealerHoursService.DealerOperationType.Sales);
			var PartsCollection = dhs.getDealerHourstByOperationType(dealerID, DealerHoursService.DealerOperationType.Parts);
			var ServiceCollection = dhs.getDealerHourstByOperationType(dealerID, DealerHoursService.DealerOperationType.Service);

			if (SalesCollection.Count > 0)
			{
				var tmpSalesCollection = SalesCollection.Where(x => !string.IsNullOrEmpty(x.Value));

				if (tmpSalesCollection.Count() > 0) hasValueSalesH = true;
				else hasValueSalesH = false;
			}
			if (PartsCollection.Count > 0)
			{
				var tmpPartsCollection = PartsCollection.Where(x => !string.IsNullOrEmpty(x.Value));

				if (tmpPartsCollection.Count() > 0) hasValuePartsH = true;
				else hasValuePartsH = false;
			}
			if (ServiceCollection.Count > 0)
			{
				var tmpServiceCollection = ServiceCollection.Where(x => !string.IsNullOrEmpty(x.Value));

				if (tmpServiceCollection.Count() > 0) hasValueServiceH = true;
				else hasValueServiceH = false;
			}

			if (hasValueSalesH)
			{
				rptDealerSalesHour.DataSource = SalesCollection;
				rptDealerSalesHour.DataBind();
				divHoursSection.Visible = true;
			}

			if (hasValuePartsH)
			{
				rptDealerPartsHour.DataSource = PartsCollection;
				rptDealerPartsHour.DataBind();
				divHoursSection.Visible = true;
			}

			if (hasValueServiceH)
			{
				rptDealerServiceHour.DataSource = ServiceCollection;
				rptDealerServiceHour.DataBind();
				divHoursSection.Visible = true;
			}

			if (!(hasValueSalesH || hasValuePartsH || hasValueServiceH))
			{
				divHoursSection.Visible = false;
			}
		}

		protected RepeaterItem getLastItemofRepeator(Repeater repeater, bool hasValue)
		{
			if (hasValue)
			{
				if (repeater.Items.Count > 0)
				{
					RepeaterItem item = repeater.Items[repeater.Items.Count - 1];
					return item;
				}
				else
				{
					return null;
				}
			}
			else { return null; }
		}

		protected void addAtttoRowToRep(RepeaterItem item, string controlName, string attName, string attValue)
		{
			if (item != null)
			{
				HtmlTableRow tr = ((HtmlTableRow)item.FindControl(controlName));
				tr.Attributes.Add(attName, attValue);
			}
		}
	}
}