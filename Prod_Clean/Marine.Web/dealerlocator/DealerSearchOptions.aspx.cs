﻿#region

using System;
using System.Text;
using System.Web.UI.HtmlControls;
using HondaDealer.Common;
using HondaDealer.Entity;
using HondaDealer.Service;
using Marine.Web._Global.Controls;
using Navantis.Honda.CMS.Client.Elements;
using Navantis.Honda.CMS.Demo;

#endregion

namespace Marine.Web.dealerlocator
{
    public partial class DealerSearchOptions : CmsContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            MainUrl = this.Request.QueryString["rootfolder"] ?? "";
            base.OnInit(e);

            var ctrlBody = (HtmlControl) Page.Master.Master.Master.FindControl("body");

            if (ctrlBody != null)
            {
                ctrlBody.Attributes["class"] = ctrlBody.Attributes["class"] == null
                    ? "page-find-a-dealer"
                    : " page-find-a-dealer";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            SetDefaulPage();

            var GencontentFffh =
                (ContentPage.Elements.ContainsKey("GenericContent_FFH")
                    ? ContentPage.Elements["GenericContent_FFH"].DeserializeElementObject()
                    : null) as FreeFormHtml;
            if (GencontentFffh != null)
                if (GencontentFffh.ContentElement.Display) LiteralPowerHouseFFH.Text = GencontentFffh.Html;

            if (IsPostBack)
            {
                //check for region selector postback
                string selectedRegion = null;
                try
                {
                    selectedRegion = Request.Form["RegionSelectBox"];
                }
                catch (Exception)
                {
                    selectedRegion = null;
                }
                if (!string.IsNullOrEmpty(selectedRegion))
                {
                    Response.Redirect(string.Format("{0}/{1}", ResourceManager.GetString("urlFindADealer"),
                        selectedRegion));
                }
            }
            else
            {
                CheckBoxPhdCity.Text = ResourceManager.GetString("txtCheckBoxrPHD");
                CheckBoxPhdPC.Text = ResourceManager.GetString("txtCheckBoxrPHD");
            }
        }

        protected void SetDefaulPage()
        {
            LitZipCode.Text = ResourceManager.GetString("txtPostalCode");
            LitCity.Text = ResourceManager.GetString("txtCity");
            LitRegion.Text = ResourceManager.GetString("txtRegion");
            LitFindYourHondaDealer.Text = ResourceManager.GetString("txtFindYourHondaDealer");
            var rs = new RegionService();
            var regionList = new EntityCollection<Region>();
            regionList = rs.GetAllRegions((Language) PageLanguage);

            string url;
            var sb = new StringBuilder();
            LiteralRegionSelectOptions.Text = string.Format(@"<option value=""{0}"">{1}</option>", "",
                ResourceManager.GetString("txtSelectYourRegion"));
            foreach (var region in regionList)
            {
                LiteralRegionSelectOptions.Text += string.Format(@"<option value=""{0}"">{1}</option>",
                    region.ProvinceUrlName, region.Name);
                url = ResourceManager.GetString("DealerLocatorRootFolder") + "/" + region.ProvinceUrlName;
                sb.Append(string.Format("<a href='{0}' title='{1}'>{2}</a>", url,
                    string.Format(ResourceManager.GetString("RegionToolTip"), region.Name), region.Name));
                //Add Nunavut to NWT
                if (region.Code.ToLower() == "nt")
                {
                    LiteralRegionSelectOptions.Text += string.Format(@"<option value=""{0}"">{1}</option>",
                        region.ProvinceUrlName, "Nunavut");
                    sb.Append(string.Format("<a href='{0}' title='{1}'>{2}</a>", url,
                        string.Format(ResourceManager.GetString("RegionToolTip"), "Nunavut"), "Nunavut"));
                }
            }
            LiteralHidddenDealerList.Text = sb.ToString();
        }

        protected void lnkBCitySearch_OnClick(object sender, EventArgs e)
        {
            var url = string.Empty;

            if (CheckBoxPhdCity.Checked)
            {
                url = FindADealer.RedirectCity(txtCity1.Text.Trim(), ResourceManager.GetString("urlDealerLocatorPhd"),
                    PageLanguage);
            }
            else
            {
                url = FindADealer.RedirectCity(txtCity1.Text.Trim(),
                    ResourceManager.GetString("DealerLocatorRootFolder"), PageLanguage);
            }

            if (url != string.Empty)
            {
                if (!(url.Contains("NoProvince")))
                    Response.Redirect(url);
                else
                {
                    var strcityprovince = txtCity1.Text.Trim().Split(',');
                    var urlparam = string.Empty;
                    if (strcityprovince.Length > 1)
                        Response.Redirect(
                            DealerLocatorCommon.getSearchErrorUrl(
                                DealerLocatorCommon.RepDealerSearch(strcityprovince[0]),
                                DealerLocatorCommon.RepDealerSearch(strcityprovince[1]),
                                ResourceManager.GetString("DealerLocatorRootFolder")));
                    else
                        Response.Redirect(
                            DealerLocatorCommon.getSearchErrorUrl(
                                DealerLocatorCommon.RepDealerSearch(txtCity1.Text.Trim()),
                                ResourceManager.GetString("DealerLocatorRootFolder")));
                }
            }
        }

        protected void lnkBPostalCode_OnClick(object sender, EventArgs e)
        {
            var url = string.Empty;

            if (CheckBoxPhdPC.Checked)
            {
                url = DealerLocatorCommon.redirectPostalCode(txtPostalCode.Text.Trim(),
                    ResourceManager.GetString("urlDealerLocatorPhd"), false);
            }
            else
            {
                url = DealerLocatorCommon.redirectPostalCode(txtPostalCode.Text.Trim(),
                    ResourceManager.GetString("DealerLocatorRootFolder"), false);
            }
            if (url != string.Empty) Response.Redirect(url);
        }
    }
}