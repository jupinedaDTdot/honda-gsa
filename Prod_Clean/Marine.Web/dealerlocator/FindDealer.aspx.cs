﻿#region

using System;
using System.Configuration;
using System.Globalization;
using System.Web.UI.HtmlControls;
using HondaCA.Common;
using HondaDealer.Service;
using Navantis.Honda.CMS.Demo;
using Newtonsoft.Json;
using LocationService = HondaCA.Service.Common.LocationService;

#endregion

namespace Marine.Web
{
    //public partial class FindDealer : CmsContentBasePage
    //{
    //    #region Variable

    //    public string _city;
    //    public bool _isPowerHouseDealer;
    //    public DealerLocationService.DealerSearchMode _mode;
    //    public string _notFound;
    //    public string _postalCode, _province;
    //    public string _provinceName;
    //    public string _searchCity;
    //    protected string _dealerFilterBy = string.Empty;
    //    private readonly RegionService _rs = new RegionService();
    //    //private bool _locationIsApproximate;

    //    #endregion

    //    #region Property

    //    protected bool ShowDistance { get; set; }

    //    #endregion

    //    #region Method

    //    public static string ConcatAddressParts(string part1, string part2)
    //    {
    //        if (string.IsNullOrWhiteSpace(part1) && string.IsNullOrWhiteSpace(part2))
    //            return string.Empty;
    //        if (string.IsNullOrWhiteSpace(part1))
    //            return part2.Trim();
    //        if (string.IsNullOrWhiteSpace(part2))
    //            return part1.Trim();
    //        return part1.Trim() + ", " + part2.Trim();
    //    }

    //    protected override void OnInit(EventArgs e)
    //    {
    //        IsNotCMSPage = true;
    //        base.OnInit(e);

    //        if (Page.Master == null || Page.Master.Master == null) return;

    //        var ctrlBody = (HtmlControl)Page.Master.Master.FindControl("body");
    //        if (ctrlBody == null) return;
    //        ctrlBody.Attributes["class"] = ctrlBody.Attributes["class"] == null
    //            ? "page-find-a-dealer-results"
    //            : " page-find-a-dealer-results";
    //    }

    //    protected void Page_Load(object sender, EventArgs e)
    //    {
    //        InitializePage();
    //        //if (!IsPostBack)
    //        //    ShowLocations();
    //    }

    //    //protected void ShowLocations()
    //    //{
    //    //    _provinceName = Request.QueryString["ProvinceName"] ?? string.Empty;
    //    //    _postalCode = (Request.QueryString["PostalCode"] ?? string.Empty).Trim().Replace(" ", string.Empty);
    //    //    _province = Request.QueryString["Province"] ?? string.Empty;
    //    //    _city = Request.QueryString["City"] ?? string.Empty;
    //    //    _searchCity = Request.QueryString["searchCity"] ?? string.Empty;
    //    //    _notFound = Request.QueryString["notfound"] ?? string.Empty;

    //    //    _dealerFilterBy = Request.QueryString["DealerFilterBy"] ?? string.Empty;
    //    //    if (_dealerFilterBy != string.Empty)
    //    //    {
    //    //        if (_dealerFilterBy == ResourceManager.GetString("powerhousedealer") ||
    //    //            _dealerFilterBy == ResourceManager.GetString("powerhouseleasedealer"))
    //    //            _isPowerHouseDealer = true;
    //    //    }
    //    //    else
    //    //    {
    //    //        _isPowerHouseDealer = false;
    //    //    }

    //    //    string title = string.Empty, subTitle = string.Empty;
    //    //    _searchCity = _searchCity.Replace("_", " ");
    //    //    _city = _city.Replace("_", " ");
    //    //    var searchTitle = string.Empty;
    //    //    if (!string.IsNullOrEmpty(_provinceName))
    //    //    {
    //    //        searchTitle = _provinceName;
    //    //        _mode = DealerLocationService.DealerSearchMode.Province;
    //    //    }
    //    //    else if (!string.IsNullOrEmpty(_postalCode))
    //    //    {
    //    //        _mode = DealerLocationService.DealerSearchMode.ZipCode;
    //    //    }
    //    //    else if (!string.IsNullOrEmpty(_province) && !string.IsNullOrEmpty(_city))
    //    //    {
    //    //        searchTitle = _city;
    //    //        _mode = DealerLocationService.DealerSearchMode.City;
    //    //    }
    //    //    else if (!string.IsNullOrEmpty(_searchCity))
    //    //    {
    //    //        _mode = DealerLocationService.DealerSearchMode.City;
    //    //    }
    //    //    else if (!string.IsNullOrEmpty(_notFound))
    //    //    {
    //    //        _mode = DealerLocationService.DealerSearchMode.SearchNotFoundError;
    //    //    }

    //    //    switch (_mode)
    //    //    {
    //    //        case DealerLocationService.DealerSearchMode.Province:
    //    //            title = _province;
    //    //            subTitle = _province;
    //    //            break;
    //    //        case DealerLocationService.DealerSearchMode.ZipCode:
    //    //            title = _postalCode;
    //    //            subTitle = _postalCode;
    //    //            break;
    //    //        case DealerLocationService.DealerSearchMode.City:
    //    //            title = string.IsNullOrEmpty(_city) ? string.IsNullOrEmpty(_searchCity) ? string.Empty : _searchCity : _city;
    //    //            subTitle = string.IsNullOrEmpty(_city)
    //    //                ? string.IsNullOrEmpty(_searchCity) ? string.Empty : _searchCity
    //    //                : _city + ", " + _province;
    //    //            break;
    //    //        default:
    //    //            title = string.IsNullOrEmpty(_city)
    //    //                ? (string.IsNullOrEmpty(_searchCity)
    //    //                    ? (string.IsNullOrEmpty(_province)
    //    //                        ? (string.IsNullOrEmpty(_postalCode) ? string.Empty : _postalCode)
    //    //                        : _province)
    //    //                    : _searchCity)
    //    //                : _city;
    //    //            subTitle = string.IsNullOrEmpty(_city)
    //    //                ? (string.IsNullOrEmpty(_searchCity)
    //    //                    ? (string.IsNullOrEmpty(_province)
    //    //                        ? (string.IsNullOrEmpty(_postalCode) ? string.Empty : _postalCode)
    //    //                        : _province)
    //    //                    : _searchCity)
    //    //                : _city;
    //    //            break;
    //    //    }

    //    //    UpdateTitle(title);
    //    //    UpdateSubTitle(subTitle);

    //    //    var provinceName = _rs.GetProvinceNameByUrlName(_province, PageLanguage.GetCultureStringValue());
    //    //    SetMetaTag(_mode, _searchCity, CultureInfo.CurrentCulture.TextInfo.ToTitleCase(searchTitle), 0, provinceName,
    //    //        _postalCode);
    //    //}

    //    //protected void SetMetaTag(DealerLocationService.DealerSearchMode searchMode, string searchCity,
    //    //    string searchText, int seachResult, string province, string postalCode)
    //    //{
    //    //    var metaKeywords = new HtmlMeta { Name = "keywords" };
    //    //    var metaDescription = new HtmlMeta { Name = "description" };
    //    //    switch (searchMode)
    //    //    {
    //    //        case DealerLocationService.DealerSearchMode.City:
    //    //            if (searchCity != string.Empty)
    //    //            {
    //    //                Title = string.Format(ResourceManager.GetString("FindDealerPageTitleByCitySearch"),
    //    //                    searchText);
    //    //                metaKeywords.Name = "robots";
    //    //                metaKeywords.Content = "noindex";
    //    //            }
    //    //            else
    //    //            {
    //    //                Title = string.Format(ResourceManager.GetString("FindDealerPageTitleByCity"), searchText,
    //    //                    province);
    //    //                metaKeywords.Content = string.Format(ResourceManager.GetString("CityMetaKeywords"), searchText,
    //    //                    province);
    //    //                metaDescription.Content = string.Format(ResourceManager.GetString("CityMetaKeywordDesc"),
    //    //                    searchText);
    //    //            }
    //    //            break;
    //    //        case DealerLocationService.DealerSearchMode.Province:
    //    //            Title = string.Format(ResourceManager.GetString("FindDealerPageTitleByProvince"), searchText);
    //    //            metaKeywords.Content = string.Format(ResourceManager.GetString("ProvinceMetaKeywords"), searchText);
    //    //            metaDescription.Content = string.Format(ResourceManager.GetString("ProvinceMetaKeywordDesc"),
    //    //                searchText);
    //    //            break;
    //    //        case DealerLocationService.DealerSearchMode.ZipCode:
    //    //            Title = string.Format(ResourceManager.GetString("FindDealerPageTitleByPostalCode"), searchText);
    //    //            metaKeywords.Content = string.Format(ResourceManager.GetString("PostalCodeMetaKeywords"), searchText);
    //    //            metaDescription.Content = string.Format(ResourceManager.GetString("PostalCodeMetaKeywordDesc"),
    //    //                searchText, postalCode);
    //    //            break;
    //    //    }

    //    //    if (searchMode == DealerLocationService.DealerSearchMode.City ||
    //    //        searchMode == DealerLocationService.DealerSearchMode.ZipCode ||
    //    //        searchMode == DealerLocationService.DealerSearchMode.Province)
    //    //    {
    //    //        if (searchCity == string.Empty)
    //    //        {
    //    //            Header.Controls.Add(metaDescription);
    //    //        }
    //    //        Header.Controls.Add(metaKeywords);
    //    //    }
    //    //}

    //    private void UpdateTitle(string title)
    //    {
    //        switch (_mode)
    //        {
    //            case DealerLocationService.DealerSearchMode.City:
    //                title = string.Format(ResourceManager.GetString("DealerTitleByCity"),
    //                    CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title));
    //                break;
    //            case DealerLocationService.DealerSearchMode.Province:
    //                title =
    //                    string.Format(
    //                        _isPowerHouseDealer
    //                            ? ResourceManager.GetString("DealerTitleByProvPHD")
    //                            : ResourceManager.GetString("DealerTitleByProv"), title);
    //                title = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title);
    //                break;
    //            case DealerLocationService.DealerSearchMode.ZipCode:
    //                title = string.Format(ResourceManager.GetString("DealerTitleByPostal"), title);
    //                break;
    //            case DealerLocationService.DealerSearchMode.SearchNotFoundError:
    //                title = ResourceManager.GetString("txtFindYourHondaDealer");
    //                break;
    //            default:
    //                title = string.Empty;
    //                break;
    //        }
    //        LitFindYourHondaDealer.Text = title;
    //    }

    //    private void UpdateSubTitle(string subTitle)
    //    {
    //        var tmpMaxstrlength = ConfigurationManager.AppSettings["MaxStringLengthForFindDealerLabel"] ?? "40";
    //        var maxStrLength = 40;
    //        Int32.TryParse(tmpMaxstrlength, out maxStrLength);

    //        switch (_mode)
    //        {
    //            case DealerLocationService.DealerSearchMode.City:
    //                if ((subTitle ?? string.Empty).Length <= maxStrLength || maxStrLength == 0)
    //                    subTitle =
    //                        string.Format(
    //                            _isPowerHouseDealer
    //                                ? ResourceManager.GetString("DealerTitleSubByCityPHD")
    //                                : ResourceManager.GetString("DealerTitleSubByCity"), subTitle);
    //                else
    //                    subTitle = _isPowerHouseDealer
    //                        ? ResourceManager.GetString("DealerTitleSubPHDNearBy")
    //                        : ResourceManager.GetString("DealerTitleSubNearBy");
    //                break;
    //            case DealerLocationService.DealerSearchMode.Province:
    //                subTitle =
    //                    string.Format(
    //                        _isPowerHouseDealer
    //                            ? ResourceManager.GetString("DealerTitleSubByProvPHD")
    //                            : ResourceManager.GetString("DealerTitleSubByProv"), subTitle);
    //                break;
    //            case DealerLocationService.DealerSearchMode.ZipCode:
    //                subTitle =
    //                    string.Format(
    //                        _isPowerHouseDealer
    //                            ? ResourceManager.GetString("DealerTitleSubByPostalPHD")
    //                            : ResourceManager.GetString("DealerTitleSubByPostal"), subTitle);
    //                break;
    //        }
    //        LitSearchResult.Text = subTitle;
    //    }

    //    private void InitializePage()
    //    {
    //        var lang = Request.QueryString["L"] ?? string.Empty;
    //        lang = lang.Length > 2 ? lang.Substring(0, 2) : lang;
    //        PageLanguage = String.Equals(lang, Language.French.GetCultureStringValue().Substring(0, 2), StringComparison.CurrentCultureIgnoreCase) ? Language.French : Language.English;
    //        base.InitializeCulture();
    //        ToggleURL = ResourceManager.GetString("DealerLocatorRootFolderToggle");
    //        Title = "Honda";
    //        LiteralPowerHouseDealerHeader.Text = ResourceManager.GetString("txtPowerHouseDealers");
    //        LiteralPowerHouseDealerBlurb.Text = ResourceManager.GetString("txtPowerHouseDealerBlurb");
    //        //SetClientLocationData();
    //    }

    //    //private void SetClientLocationData()
    //    //{
    //    //    const string requestIp = "66.241.130.174"; //Request.UserHostAddress;
    //    //    var locationService = new LocationService();
    //    //    var clientLocationByIp = locationService.GetLocationByIP_ValidateIP(requestIp);
    //    //    var javaScriptformattedClientLocation =
    //    //        new
    //    //        {
    //    //            latitude = clientLocationByIp.Latitude,
    //    //            longitude = clientLocationByIp.Longitude,
    //    //            city = clientLocationByIp.City,
    //    //            region = clientLocationByIp.Region,
    //    //            postalCode = clientLocationByIp.ZipCode
    //    //        };
    //    //    clientLocation.Value = JsonConvert.SerializeObject(javaScriptformattedClientLocation, Formatting.Indented);
    //    //}

    //    #endregion
    //}
}