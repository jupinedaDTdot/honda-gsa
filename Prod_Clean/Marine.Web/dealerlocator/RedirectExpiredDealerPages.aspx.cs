﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using HondaCA.Common;
using HondaDealer.Service;
using HondaDealer.Entity;

namespace Marine.Web.dealerlocator
{
    public partial class RedirectExpiredDealerPages : CmsContentBasePage
    {
        DealerInformation dealerInfo;
        public string url;

        protected override void OnInit(EventArgs e)
        {
            IsNotCMSPage = true;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string provinceName;
            string lang = Request.QueryString["L"] != null ? Request.QueryString["L"] : "";
            string sDealerID = Request.QueryString["DealerID"] != null ? Request.QueryString["DealerID"] : "";  
            int DealerID=0;
            
            lang = lang.Length > 1 ? lang.Substring(0, 1) : lang;
            
            if (lang.ToUpper() == Language.French.GetCultureStringValue().Substring(0, 1).ToUpper())
                this.PageLanguage = Language.French;
            else
                this.PageLanguage = Language.English;
            
            base.InitializeCulture();
            DealerInfoService ds = new DealerInfoService();
            RegionService rs = new RegionService();
            Int32.TryParse(sDealerID, out DealerID);
            //Response.Write("sDealerID:" + sDealerID);
            if (DealerID != 0)
            {   dealerInfo = ds.GetDealersByID(DealerID, (HondaDealer.Common.Language)PageLanguage);
            if (dealerInfo != null)
            {
                provinceName = DealerLocatorCommon.RepDealerSearch(rs.GetProvinceUrlNameByCode(dealerInfo.Province, PageLanguage.GetCultureStringValue()));
                url = "http://" + HondaCA.Common.Global.CurrentDomain + (ResourceManager.GetString("DealerLocatorRootFolder") + "/" + provinceName + "/" + DealerLocatorCommon.RepDealerSearch(dealerInfo.City) + "/" + dealerInfo.DealerUrlName).ToLower();
                //Response.Redirect( url);
                //Server.Transfer("/dealerlocator" + string.Format("/DealerDetail.aspx?Province={0}&City={1}&DealerName={2}", provinceName, CommonFunction.RepDealerSearch(dealerInfo.City), dealerInfo.DealerUrlName));
            }
            else
                url = "http://" + HondaCA.Common.Global.CurrentDomain + ResourceManager.GetString("DealerLocatorRootFolder") ;

            Uri urlRedirect = new Uri(url);
            Response.Status = "301 Moved Permanently";
            Response.AddHeader("Location", urlRedirect.AbsoluteUri);

            }

        }
    }
}
