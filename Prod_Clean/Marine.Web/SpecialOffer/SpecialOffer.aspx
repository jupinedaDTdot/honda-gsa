﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true" CodeBehind="SpecialOffer.aspx.cs" Inherits="HondaPE.Web.SpecialOffer.SpecialOffer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/current-offers.css" rel="stylesheet" type="text/css" />    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="cap_top_default pf"></div>
	<div class="container pf">
		<div class="content_container">
			<div class="fc content_section first_section">
				<div class="fl special-offers-info">
					<div class="special-offers">
						<h1><%= ResourceManager.GetString("txtSpecialOffers")%></h1>
						<%--<h2 class="small"><%= ResourceManager.GetString("txtSpecialOfferTitle")%></h2>
						<p><%= ResourceManager.GetString("txtSpecialOfferSubTitle")%></p>--%>
					</div>
					<div class="current-offers">
						<h2 class="small"><%= ResourceManager.GetString("txtViewCurrentOffer")%></h2>
						<select property="act:fieldselect"
                                data-fieldselect-button-class="pf"
                                data-fieldselect-pane-class="field-select-right" onchange="javascript:location.href=this.value;">
                            <asp:Literal ID="LiteralRegionSelectOptions" runat="server"></asp:Literal>
                            <asp:Literal ID="LitRegionList_SO" Visible="false" runat="server" ></asp:Literal>
                        </select>
					</div>                    
				</div>
				<div class="fr current_offers_map" id="canada_map">
				</div>
			</div>
            <div class="dn">
                <asp:Literal ID="LiteralHidddenOfferList" runat="server"></asp:Literal>
            </div>
		</div>
	</div>
	<div class="cap_bottom_default pf"></div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BottomCap" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
<script src="/_Global/js/sections/current-offers-map.js" type="text/javascript"></script>
</asp:Content>
