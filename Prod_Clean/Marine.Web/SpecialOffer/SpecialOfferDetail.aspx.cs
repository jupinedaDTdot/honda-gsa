﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using Marine.Web.ContentPages;
using HondaCA.WebUtils.CMS;


namespace Marine.Web.SpecialOffer
{
    public partial class SpecialOfferDetail : ContentBasePage
    {
        string Provine { get; set; }
        string Title { get; set; }
        int OfferID;
        string parentURL = string.Empty;
        protected override void OnInit(EventArgs e)
        {
            //this.MainUrl = HttpContext.Current.Request.RawUrl.ToLower();
            Provine = Request.Form["OfferProv"] ?? "ontario";
            int.TryParse(Request.Form["OfferID"], out OfferID);
            parentURL = Request.RawUrl.Contains("fr") ? "/offres-speciales" : "/special-offers";
            this.MainUrl =  parentURL +"/" + Provine;
            if (string.IsNullOrEmpty(Request.Form["OfferID"])) Response.Redirect(this.MainUrl);
            base.OnInit(e);
        }               

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-current-offers-details");
                this.IsMasterBodyClassUpdate = true;
            }

            aViewAll.HRef = this.MainUrl;

            GenericContent gc = CMSHelper.getCmsElementFromCmsPage<GenericContent>(this.ContentPage, "GenericContent_GC1");           
            bool visible = gc.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (visible)
            {
                if(gc != null && gc.Items != null)
                {
                    foreach (GenericContentItem item in gc.Items)
	                {
		                if(item.DisplayOrder == OfferID)
                        {
                            topImage.Text = string.Format(@"<img alt="""" src=""{0}"">", item.Image);
                            litTitle.Text = item.Title;
                            litSummery.Text = item.Summary;
                        }
	                }                    
                }
            }

        }
    }
}