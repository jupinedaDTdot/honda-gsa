﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;

using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.Common;
using HondaDealer.Service;
using HondaDealer.Entity;

namespace HondaPE.Web.SpecialOffer
{
    public partial class SpecialOffer : CmsContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-current-offers-landing");
                this.IsMasterBodyClassUpdate = true;
            }

            HondaDealer.Service.RegionService rs = new HondaDealer.Service.RegionService();
            EntityCollection<Region> regionList = new EntityCollection<Region>();
            try
            {
                regionList = rs.GetAllRegions(PageLanguage.GetCultureStringValue());
            }
            catch (Exception ex)
            {
                throw new Exception("hondaPE:specialoffer:SetProvinceList" + ex.Message, ex);
            }

            string url=string.Empty;
            StringBuilder sb = new StringBuilder();
            LiteralRegionSelectOptions.Text = string.Format(@"<option value=""{0}"">{1}</option>", "", ResourceManager.GetString("txtSelectYourRegion"));            
            foreach (HondaDealer.Entity.Region region in regionList)
            {
                LiteralRegionSelectOptions.Text += string.Format(@"<option value=""{0}"" rel=""{1}"" title=""{2}"">{3}</option>", string.Format("{0}/{1}", ResourceManager.GetString("txtSpecialOffersURL"), region.ProvinceUrlName), region.Code, string.Format("{1} {0}", ResourceManager.GetString("txtSpecialOffers"), region.Name), region.Name);
                url = ResourceManager.GetString("txtSpecialOffersURL") + "/" + region.ProvinceUrlName;
                sb.Append(string.Format("<a href='{0}' title='{1}' rel='{2}'>{3}</a>", url, string.Format("{1} {0}", ResourceManager.GetString("txtSpecialOffers"), region.Name), region.Code, region.Name));

                //Add Nunavut to NWT
                if (region.Code.ToLower() == "nt")
                {
                    LiteralRegionSelectOptions.Text += string.Format(@"<option value=""{0}"" rel=""{1}"" title=""{2}"">{3}</option>", string.Format("{0}/{1}", ResourceManager.GetString("txtSpecialOffersURL"), "nunavut"), "NU", string.Format("{1} {0}", ResourceManager.GetString("txtSpecialOffers"), "Nunavut"), "Nunavut");
                    sb.Append(string.Format("<a href='{0}' title='{1}' rel='{2}'>{3}</a>", url, string.Format("{1} {0}", ResourceManager.GetString("txtSpecialOffers"), "Nunavut"), region.Code, "Nunavut"));
                }
            }
            LiteralHidddenOfferList.Text = sb.ToString();
        }
    }
}