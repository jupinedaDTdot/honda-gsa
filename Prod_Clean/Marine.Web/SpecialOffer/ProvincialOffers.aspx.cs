﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using HondaCA.Entity.Common;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using Marine.Web.ContentPages;
using HondaDealer.Service;
using HondaCA.Common;
using HondaCA.WebUtils.CMS;
using HondaCA.WebUtils.StringUtility;
namespace Marine.Web.SpecialOffer
{
    public partial class ProvincialOffers : ContentBasePage
    {
        protected string provinceName { get; set; }
        private string _provinceUrlKey;
        //private string _ProvinceOfferTitle;
        //private string _NationalOfferTitle;
        //private string _ProvinceOfferHtml;
        //private string _NationalOfferHtml;
        const int textMaxLength = 300;
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-current-offers-provincial-landing");
                this.IsMasterBodyClassUpdate = true;
            }

            _provinceUrlKey = _provinceUrlKey = Request.QueryString["ProvinceName"];
            RegionService regionService = new RegionService();
            provinceName = regionService.GetProvinceNameByUrlKey(PageLanguage.GetCultureStringValue(), _provinceUrlKey);

            int intNoSpecialOffer = 0;
            //_NationalOfferTitle = string.Format(@"<h2>{0}</h2>", ResourceManager.GetString("txtNationalOffers"));
            //_NationalOfferHtml = @"<div class=""np content_section"">" + _NationalOfferTitle + ResourceManager.GetString("txtSpecialOfferNonOfferMessage") + "</div>";

            Navantis.Honda.CMS.Client.Elements.Menu menu = CMSHelper.getElementThroughContentPointerFromCmsPage<Navantis.Honda.CMS.Client.Elements.Menu>(this.ContentPage, "GenericContentPointer_CP");
            if (menu != null)
            {
                bool isVisibleMenu = menu.ContentElement.Display ? true : this.IsStaging ? true : false;
                if (isVisibleMenu)
                {
                    userMenu.menuTitle = ResourceManager.GetString("txtSpecialOffers");
                    userMenu.LoadData(setMenu(menu));
                }
                else
                {
                    userMenu.Visible = false;
                    userMenu.menuCMSControlName = "GenericContentPointer_CP";
                }
            }


            GenericContent gContent = CMSHelper.getCmsElementFromCmsPage<GenericContent>(this.ContentPage, "GenericContent_GC1");
            if (gContent != null)
            {
                rptProvOffer.Visible = gContent.ContentElement.Display ? true : this.IsStaging ? true : false;
                if (rptProvOffer.Visible)
                {
                    if (gContent != null && gContent.Items != null && gContent.Items.Count > 0)
                    {
                        rptProvOffer.DataSource = gContent.Items;
                        rptProvOffer.DataBind();
                    }
                    else
                    {
                        intNoSpecialOffer += 1;
                        divProvincial1.Visible = false;
                        //litNonProvOffer.Text = _ProvinceOfferHtml;
                    }
                }
                else
                    rptProvOffer.Visible = false;
            }
            else
            {
                intNoSpecialOffer += 1;
                divProvincial1.Visible = false;
                //litNonProvOffer.Text = _ProvinceOfferHtml;
            }

            GenericContent gNationContent = CMSHelper.getElementThroughContentPointerFromCmsPage<GenericContent>(this.ContentPage, "GenericContentPointer_CP2");
            if (gNationContent != null)
            {
                rptNationalOffer.Visible = gNationContent.ContentElement.Display ? true : IsStaging ? true : false;
                if (rptNationalOffer.Visible)
                {
                    if (gNationContent != null && gNationContent.Items != null && gNationContent.Items.Count > 0)
                    {
                        rptNationalOffer.DataSource = gNationContent.Items;
                        rptNationalOffer.DataBind();
                    }
                    else
                    {
                        intNoSpecialOffer += 1;
                        divNational1.Visible = false;
                        //LitNonNationalOffer.Text = _NationalOfferHtml;
                    }
                }
            }
            else
            {
                intNoSpecialOffer += 1;
                divNational1.Visible = false;
                //LitNonNationalOffer.Text = _NationalOfferHtml;
            }

            if (intNoSpecialOffer >= 2)
            {
                litNonProvOffer.Visible = true;
                divProvincial.Visible = this.IsStaging ? true : false;
                divNational.Visible = this.IsStaging ? true : false;
                //_ProvinceOfferTitle = string.Format(string.Format(@"<h2>{0}</h2>", ResourceManager.GetString("txtProvinceOffer")), provinceName);
                litNonProvOffer.Text = @"<div class=""np"">" + ResourceManager.GetString("txtSpecialOfferNonOfferMessage") + "</div>";
            }

        }

        protected void rptProvOffer_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            //Literal litProvHeading = e.Item.FindControl("litProvHeading") as Literal;
            Literal litTitle = e.Item.FindControl("litTitle") as Literal;
            Literal litSummery = e.Item.FindControl("litSummery") as Literal;
            Literal litMoreText = e.Item.FindControl("litMoreText") as Literal;
            HtmlContainerControl mainDiv = e.Item.FindControl("mainDiv") as HtmlContainerControl;
            HtmlAnchor lnkFindMore = e.Item.FindControl("lnkFindMore") as HtmlAnchor;
            Image imgOffer = e.Item.FindControl("imgOffer") as Image;

            GenericContentItem gItem = e.Item.DataItem as GenericContentItem;

            if (e.Item.ItemIndex == 0)
            {
                mainDiv.Attributes.Add("class", "np");
                //litProvHeading.Text = _ProvinceOfferTitle;
            }
            else
            {
                mainDiv.Attributes.Add("class", "content_section_dotted");
            }

            if (!string.IsNullOrEmpty(gItem.MoreTextLinkURL) && gItem.MoreTextLinkURL.ToLower().EndsWith(".pdf")) // pdf is provided
            {
                lnkFindMore.Attributes.Add("class", "dl-icon");
                litSummery.Text = gItem.Summary;
            }
            else if (!string.IsNullOrEmpty(gItem.MoreTextLinkURL)) // Detail page links
            {
                lnkFindMore.Attributes.Add("class", "btn primary");
                lnkFindMore.HRef = gItem.MoreTextLinkURL;
                litSummery.Text = gItem.Summary;
            }
            else if (!string.IsNullOrEmpty(gItem.Summary)) // offer details page
            {
                lnkFindMore.Attributes.Add("class", "btn primary");
                string strevent = string.Format(@"ShowOfferDetail('{0}','{1}','{2}','{3}')", gItem.DisplayOrder, RemoveSpecialChar(gItem.Title), ResourceManager.GetString("urlSpecialOfferDetail"), _provinceUrlKey);
                lnkFindMore.Attributes.Add("OnClick", strevent);
                litSummery.Text = gItem.Summary;
                lnkFindMore.Visible = false;
            }
            else
                lnkFindMore.Visible = false;

            lnkFindMore.HRef = gItem.MoreTextLinkURL ?? "";
            lnkFindMore.Attributes.Add("target", "_blank");
            litTitle.Text = gItem.Title;
            litMoreText.Text = gItem.MoreText;
            imgOffer.ImageUrl = string.Format("{0}?Crop=auto&maxwidth={1}&maxheight={2}", gItem.Image, 310, 150);
        }

        protected void rptNationalOffer_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            Literal litProvHeading = e.Item.FindControl("litProvHeading") as Literal;
            Literal litTitle = e.Item.FindControl("litTitle") as Literal;
            Literal litSummery = e.Item.FindControl("litSummery") as Literal;
            Literal litMoreText = e.Item.FindControl("litMoreText") as Literal;
            HtmlContainerControl mainDiv = e.Item.FindControl("mainDiv") as HtmlContainerControl;
            HtmlAnchor lnkFindMore = e.Item.FindControl("lnkFindMore") as HtmlAnchor;
            Image imgOffer = e.Item.FindControl("imgOffer") as Image;
            GenericContentItem gItem = e.Item.DataItem as GenericContentItem;

            if (e.Item.ItemIndex == 0)
            {
                mainDiv.Attributes.Add("class", "np first_national_offer_section");
                //litProvHeading.Text = "<h2>" + ResourceManager.GetString("txtNationalOffers") + "</h2>";
            }
            else
            {
                mainDiv.Attributes.Add("class", "content_section_dotted");
            }

            if (!string.IsNullOrEmpty(gItem.MoreTextLinkURL) && gItem.MoreTextLinkURL.ToLower().EndsWith(".pdf")) // pdf is provided
            {
                lnkFindMore.Attributes.Add("class", "dl-icon");
                litSummery.Text = gItem.Summary;
            }
            else if (!string.IsNullOrEmpty(gItem.MoreTextLinkURL)) // Detail page links
            {
                lnkFindMore.Attributes.Add("class", "btn primary");
                lnkFindMore.HRef = gItem.MoreTextLinkURL;
                litSummery.Text = gItem.Summary;
            }
            else if (!string.IsNullOrEmpty(gItem.Summary)) // offer details page
            {
                lnkFindMore.Attributes.Add("class", "btn primary");
                string strevent = string.Format(@"ShowOfferDetail('{0}','{1}','{2}','{3}')", gItem.DisplayOrder, RemoveSpecialChar(gItem.Title), ResourceManager.GetString("urlSpecialOfferDetail"), _provinceUrlKey);
                lnkFindMore.Attributes.Add("OnClick", strevent);
                litSummery.Text = gItem.Summary;
                lnkFindMore.Visible = false;
            }
            else
                lnkFindMore.Visible = false;

            lnkFindMore.HRef = gItem.MoreTextLinkURL ?? "";
            lnkFindMore.Attributes.Add("target", "_blank");
            litTitle.Text = gItem.Title;
            litMoreText.Text = gItem.MoreText;
            imgOffer.ImageUrl = string.Format("{0}?Crop=auto&maxwidth={1}&maxheight={2}", gItem.Image, 310, 150);
        }

        private string RemoveSpecialChar(string strhtml)
        {
            if (strhtml != null)
                return strhtml.Replace("'", string.Empty).Replace("\"", string.Empty);
            else
                return string.Empty;
        }
    }
}