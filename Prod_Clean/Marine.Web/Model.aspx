﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Model.aspx.cs" Inherits="Navantis.Honda.CMS.Demo.Model" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Register TagPrefix="uc" TagName="ElementControl" Src="~/Cms/Console/ElementControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="ElementControlCompaq" Src="~/Cms/Console/ElementControlCompaq.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <table border="1" cellpadding="0" cellspacing="0" width="100%">
    
        <tr>
            <td colspan="2">
                    <strong>Related Pages</strong><br />
                    <div>
                        <%
                        if (this.RelatedPages.Count > 0)                            
                        {
                            foreach (string relatedPageName in this.RelatedPages.Keys)
                            {
                                foreach (ContentPage cp in this.RelatedPages[relatedPageName])
                                {%>
                                    <strong>Related Page Name:</strong> <%=relatedPageName%> <strong>Related Page URL:</strong> <%=cp.MainURL%>  
                                    <strong>Related content elements:</strong>
                                    <%foreach (string elementName in cp.Elements.Keys)
                                      { %>
                                      <%=elementName %>;
                                    <%} %>
                                    <br />
                                <%}
                            }
                        }%>
                    </div>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                 <div class="has-edit-button">
                    <uc:ElementControlCompaq id="ElementControlCompaq9" runat="server" ElementName="ProductSelectionWizard" />
                    <strong>ProductSelectionWizard</strong><br />
                    ProductSelectionWizard element Content<br />
                    <div>
                        <%if (this.ContentPage.Elements.ContainsKey("ProductSelectionWizard"))
                        {
                            ProductSelectionWizard gl = (this.ContentPage.Elements["ProductSelectionWizard"].DeserializeElementObject() as ProductSelectionWizard);
                            if (gl != null && gl.Items != null)
                            {
                                foreach (ProductSelectionQuestion gli in gl.Items)
                                {%>
                                    <%=gli.Title
                                    
                                    gli.QuestionItems.Capacity
                                    %> 
                                 
                                    <br />
                                <%}
                            }
                        }%>
                    </div>
                </div>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                 <div class="has-edit-button">
                    <uc:ElementControlCompaq id="ElementControlCompaq8" runat="server" ElementName="GenericLink" />
                    <strong>GenericLink</strong><br />
                    GenericLink element Content<br />
                    <div>
                        <%if (this.ContentPage.Elements.ContainsKey("GenericLink"))
                        {
                            GenericLink gl = (this.ContentPage.Elements["GenericLink"].DeserializeElementObject() as GenericLink);
                            if (gl != null && gl.Items != null)
                            {
                                foreach (GenericLinkItem gli in gl.Items)
                                {%>
                                    <%=gli.Title%> - 
                                    <%=gli.File%> - 
                                    <%=gli.LinkText%> ( <%=gli.LinkURL%> }
                                    <br />
                                <%}
                            }
                        }%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="has-edit-button">
                    <uc:ElementControlCompaq id="ElementControlCompaq7" runat="server" ElementName="RelatedProduct" />
                    <strong>RelatedProduct</strong>
                    <br />
                    RelatedProduct element Content
                    <br />
                    <div>
                        <%if (this.ContentPage.Elements.ContainsKey("RelatedProduct"))
                        {
                            RelatedProduct gc = (this.ContentPage.Elements["RelatedProduct"].DeserializeElementObject() as RelatedProduct);
                            if (gc != null && gc.Items != null)
                            {
                                foreach (RelatedProductItem gcItem in gc.Items)
                                {%>
                                    <%=gcItem.ModelID%> - 
                                    <%=gcItem.TrimID%> 
                                    <br />
                                <%}
                            }
                        }%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="has-edit-button">
                    <uc:ElementControlCompaq id="ElementControlCompaq6" runat="server" ElementName="AdBuilderNewAsset" />
                    <strong>AdBuilderNewAsset</strong>
                    <br />
                    AdBuilderNewAsset element Content
                    <br />
                    <div>
                        <%if (this.ContentPage.Elements.ContainsKey("AdBuilderNewAsset"))
                        {
                            AdBuilderNewAsset gc = (this.ContentPage.Elements["AdBuilderNewAsset"].DeserializeElementObject() as AdBuilderNewAsset);
                            if (gc != null && gc.Items != null)
                            {
                                foreach (AdBuilderNewAssetItem gcItem in gc.Items)
                                {%>
                                    <%=gcItem.ContentID%> - 
                                    <%=gcItem.ItemID%> 
                                    <br />
                                <%}
                            }
                        }%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="has-edit-button">
                    <uc:ElementControlCompaq id="ElementControlCompaq5" runat="server" ElementName="AdBuilderFeaturedAsset" />
                    <strong>AdBuilderFeaturedAsset</strong>
                    <br />
                    AdBuilderFeaturedAsset element Content
                    <br />
                    <div>
                        <%if (this.ContentPage.Elements.ContainsKey("AdBuilderFeaturedAsset"))
                        {
                            AdBuilderFeaturedAsset gc = (this.ContentPage.Elements["AdBuilderFeaturedAsset"].DeserializeElementObject() as AdBuilderFeaturedAsset);
                            if (gc != null && gc.Items != null)
                            {
                                foreach (AdBuilderFeaturedAssetItem gcItem in gc.Items)
                                {%>
                                    <%=gcItem.ContentID%> - 
                                    <%=gcItem.ItemID%> 
                                    <br />
                                <%}
                            }
                        }%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="has-edit-button">
                    <uc:ElementControlCompaq id="ElementControlCompaq4" runat="server" ElementName="AdBuilderAsset" />
                    <strong>AdBuilderAsset</strong>
                    <br />
                    AdBuilderAsset element Content
                    <br />
                    <div>
                        <%if (this.ContentPage.Elements.ContainsKey("AdBuilderAsset"))
                        {
                            AdBuilderAsset gc = (this.ContentPage.Elements["AdBuilderAsset"].DeserializeElementObject() as AdBuilderAsset);
                            if (gc != null && gc.Items != null)
                            {
                                foreach (AdBuilderAssetItem gcItem in gc.Items)
                                {%>
                                    <%=gcItem.Title%> - 
                                    <%=gcItem.PreviewThumbnail%> - 
                                    <%=gcItem.Notes%>
                                    <br />
                                <%}
                            }
                        }%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="has-edit-button">
                    <uc:ElementControlCompaq id="ElementControlCompaq3" runat="server" ElementName="GenericContent" />
                    <strong>GenericContent</strong>
                    <br />
                    GenericContent element Content
                    <br />
                    <div>
                        <%if (this.ContentPage.Elements.ContainsKey("GenericContent"))
                        {
                            GenericContent gc = (this.ContentPage.Elements["GenericContent"].DeserializeElementObject() as GenericContent);
                            if (gc != null && gc.Items != null)
                            {
                                foreach (GenericContentItem gcItem in gc.Items)
                                {%>
                                    <%=gcItem.Title%> - 
                                    <%=gcItem.Image%> - 
                                    <%=gcItem.Summary%>
                                    <br />
                                <%}
                            }
                        }%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div>
                    <strong>Tout</strong>
                    <br />
                    <uc:ElementControl id="ElementControl7" runat="server" ElementName="Tout" />
                    Tout Element Content
                    <br />
                    <div>                   
                        <%Tout Tout = this.ContentPage.Elements["Tout"].DeserializeElementObject() as Tout;
                        if (Tout != null && Tout.Items != null)
                        {
                            foreach (ToutItem tiItem in Tout.Items)
                            {%>
                                <%=tiItem.Name%> - 
                                <%=tiItem.Title%> - 
                                <%=tiItem.URL%> - 
                                <%=tiItem.PathReference%>
                                <br />
                            <%}
                        } %>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div>
                    <strong>Menu</strong>
                    <br />
                    <uc:ElementControl id="ElementControl5" runat="server" ElementName="Menu" />
                    Menu element Content
                    <br />
                    <div>
                        <%Navantis.Honda.CMS.Client.Elements.Menu menu = (this.ContentPage.Elements["Menu"].DeserializeElementObject() as Navantis.Honda.CMS.Client.Elements.Menu);
                        if (menu != null && menu.Items != null)
                        {
                            foreach (Navantis.Honda.CMS.Client.Elements.MenuItem menuItem in menu.Items)
                            {%>
                                Top level item <%= menuItem.Name%>
                                <br />
                                <%foreach (Navantis.Honda.CMS.Client.Elements.MenuItem subMenuItem in menuItem.ChildItems)
                                {%>
                                    2nd level item <%= subMenuItem.Name%>
                                    <br />
                                <%}
                            }
                        }%>
                    </div>
                </div>
                <br />
                <div class="has-edit-button">
                    <uc:ElementControlCompaq id="ElementControl4" runat="server" ElementName="Brochure" />
                    <strong>Brochure</strong>
                    <br />
                    Brochure element Content
                    <br />
                    <div>
                        <%Brochure brochure = (this.ContentPage.Elements["Brochure"].DeserializeElementObject() as Brochure);
                        if (brochure != null)
                        {%>
                            <%=brochure.PDF%>
                            <br />
                            <%=brochure.URL%>
                            <br />
                        <%}%>
                    </div>
                </div>
                <div>
                    <strong>GenericForm</strong>
                    <br />
                    <uc:ElementControl id="ElementControl6" runat="server" ElementName="GenericForm" />
                    GenericForm Element Content
                    <br />
                    <div>
                        <%if (this.ContentPage.Elements.ContainsKey("GenericForm"))
                        {
                            GenericForm genericForm = this.ContentPage.Elements["GenericForm"].DeserializeElementObject() as GenericForm;
                            if (genericForm != null && genericForm.Items != null)
                            {
                                foreach (GenericFormField vlItem in genericForm.Items)
                                {%>
                                    <strong>Name:</strong>
                                    <%=vlItem.Name%>
                                    <strong>Type:</strong>
                                    <%=vlItem.Type%> 
                                    <br />
                                <%}%>
                                <br />
                                go through each field and create the form
                                <br />
                                <div>
                                    <%foreach (GenericFormField field in genericForm.Items)
                                    {
                                        if(field.Type== GenericFormFieldType.TextBox)
                                        {%>
                                            <%=field.Label%>:
                                            <input id="<%=field.Name%>" name="<%=field.Name%>" value="<%=field.Value%>" type="text" />
                                            <br />
                                            <br />
                                        <%}
                                        if(field.Type== GenericFormFieldType.CheckBox)
                                        {%>
                                            <input id="<%=field.Name%>" name="<%=field.Name%>" type="checkbox" /> <%=field.Label%>
                                            <br />
                                            <br />
                                        <%}
                                    }%>

                                    note: you can use jquery/ajax to submit to another aspx and to improve UI. And use the field.ValidationMessage and field.IsRequired to implement validation<br />
                                    After submit form, you can use Report interface /Content/Report/ExportGenericFormResponse to export excel file.
                                    <br />
                                    <input type="submit" name="submitGenericForm" value="Submit Generic Form Test" />
                                </div>
                           <%}
                        }%>
                        <br />
                    </div>
                </div>
            </td>
            <td>
                <div>
                    <strong>ModelNav</strong>
                    <br />
                    <uc:ElementControl id="modelNavElementConsole" runat="server" ElementName="ModelNav" />
                    ModelNav Element Content
                    <br />
                    <div>
                        <%Navantis.Honda.CMS.Client.Elements.ModelNav modelNav = (this.ContentPage.Elements["ModelNav"].DeserializeElementObject() as Navantis.Honda.CMS.Client.Elements.ModelNav);
                        if (modelNav != null && modelNav.Items != null)
                        {
                            foreach (Navantis.Honda.CMS.Client.Elements.ModelNavItem modelNavItem in modelNav.Items)
                            { %>
                                Top level navigation <%= modelNavItem.Name%> - <%= modelNavItem.Title%> - <%= modelNavItem.URL%>
                                <br />
                                <%foreach (Navantis.Honda.CMS.Client.Elements.ModelNavItem subModelNavItem in modelNavItem.ChildItems)
                                {%>
                                    Sub navigation <%= subModelNavItem.Name%> - <%= subModelNavItem.Title%> - <%= subModelNavItem.URL%>
                                    <br />
                                <%}
                            }
                        }%>
                    </div>
                </div>
                <%--<div>
                    <strong>GenericForm</strong><br />
                    <uc:ElementControl id="genericFormElementConsole" runat="server" ElementName="GenericForm" />
                    ModelNav Element Content<br />
                    <div>
                            <%
                                Navantis.Honda.CMS.Client.Elements.GenericForm genericForm = (this.ContentPage.Elements["GenericForm"].DeserializeElementObject() as Navantis.Honda.CMS.Client.Elements.GenericForm);
                                if (genericForm != null && genericForm.Fields != null)
                                {
                                    foreach (Navantis.Honda.CMS.Client.Elements.GenericFormField genericFormField in genericForm.Fields)
                                    { %>
                                        Field : <%= genericFormField.Name%> - <%= genericFormField.Label%> - <%= genericFormField.IsRequired%> - <%= genericFormField.Value%><br />
                                    <%}
                                } %>
                    </div>
                </div>--%>
                <div class="has-edit-button">
                    <uc:ElementControlCompaq id="ElementControlCompaq1" runat="server" ElementName="ModelOverview" />
                    <strong>ModelOverview</strong>
                    <br />
                    ModelOverview element Content
                    <br />
                    <div>
                        <%FreeFormHtml modelOverview = (this.ContentPage.Elements["ModelOverview"].DeserializeElementObject() as FreeFormHtml);
                        if (modelOverview != null)
                        {%>
                            <%=modelOverview.Html%>
                            <br />
                        <%}%>
                    </div>
                </div>
                <div>
                    <strong>Gallery</strong>
                    <br />
                    <uc:ElementControl id="ElementControl3" runat="server" ElementName="Gallery" />
                    Gallery Element Content
                    <br />
                    <div>
                        <%Gallery Gallery = this.ContentPage.Elements["Gallery"].DeserializeElementObject() as Gallery;
                        if (Gallery != null && Gallery.Items != null)
                        {
                            foreach (GalleryItem vlItem in Gallery.Items)
                            {%>
                                <a href="<%=vlItem.ThumbAsset %>"><%=vlItem.ThumbAsset%></a>
                            <%}
                        }%>
                    </div>
                </div>
                <div>
                    <strong>Reviews</strong>
                    <br />
                    <uc:ElementControl id="ElementControl10" runat="server" ElementName="Review" />
                    Review Element Content
                    <br />
                    <div>
                        <%if (this.ContentPage.Elements.ContainsKey("Review"))
                        {
                            Review review = this.ContentPage.Elements["Review"].DeserializeElementObject() as Review;
                            if (review != null && review.Items != null)
                            {
                                foreach (ReviewItem vlItem in review.Items)
                                {%>
                                    <strong><%=vlItem.Title%></strong>
                                    <br />
                                    <%=vlItem.Summary%>
                                    <br/>
                                    <%=vlItem.Source%>
                                    <a href="<%=vlItem.LinkURL %>">Read more</a>
                                <%}
                            }
                        }%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div>
                    <strong>VehicleLife</strong>
                    <br />
                    <uc:ElementControl id="ElementControl2" runat="server" ElementName="VehicleLife" />
                    VehicleLife Element Content
                    <br />
                    <div>
                        <%VehicleLife vehicleLife = this.ContentPage.Elements["VehicleLife"].DeserializeElementObject() as VehicleLife;
                        if (vehicleLife != null && vehicleLife.Items != null)
                        {
                            foreach (VehicleLifeItem vlItem in vehicleLife.Items)
                            {%>
                                <strong>Title:</strong><%=vlItem.Title%>
                                <strong>URL:</strong><%=vlItem.URL %>
                                <strong>ImageURL:</strong><%=vlItem.ImageURL %> 
                                <br />
                            <%}
                        }%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <strong>GenericUrlOne</strong>
                    <br />
                    <uc:ElementControl id="ElementControl1" runat="server" ElementName="GenericUrlOne" />
                    GenericUrlOne Element Content
                    <br />
                    <div>
                        <%GenericUrl genericUrl1 = this.ContentPage.Elements["GenericUrlOne"].DeserializeElementObject() as GenericUrl;
                        if (genericUrl1 != null)
                        {%>
                            <a href="<%=genericUrl1.URL %>"><%=genericUrl1.Title%></a> <%=genericUrl1.URL%>
                        <%}%>
                    </div>
                </div>
            </td>
            <td>
                <div>
                    <strong>Top 5 Accessories</strong>
                    <br />
                    <uc:ElementControl id="ElementControl9" runat="server" ElementName="TopAccessories" />
                    TopAccessories Element Content
                    <br />
                    <div>
                        <%if (this.ContentPage.Elements.ContainsKey("TopAccessories"))
                        {
                            TopAccessories topAccessories = this.ContentPage.Elements["TopAccessories"].DeserializeElementObject() as TopAccessories;
                            if (topAccessories != null && topAccessories.Items != null)
                            {
                                foreach (TopAccessoriesItem vlItem in topAccessories.Items)
                                {%>
                                 <strong>AccessoryItemID:</strong><%=vlItem.AccessoryItemID%>
                                 <br />
                                <%}
                            }
                        }%>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <strong>ContentPointer</strong>
                    <br />
                    <%if (this.ContentPage.Elements.ContainsKey("ContentPointer"))
                    {%>    
                        <uc:ElementControl id="ElementControl8" runat="server" ElementName="ContentPointer" />
                        ContentPointer Element Content
                        <br />
                        <div>
                            <%ContentPointer contentPointer = this.ContentPage.Elements["ContentPointer"].DeserializeElementObject() as ContentPointer;
                            if (contentPointer != null)
                            {%>
                                 <strong>Target Content ID:</strong><%=contentPointer.ContentID%>
                                 <br />
                                 <strong>Target Content Type:</strong>
                                 <%IElementContent iecForCP = contentPointer.GetTargetContentElement();
                                 if(iecForCP != null)
                                    Response.Write(iecForCP.ToString());%>
                                 <br />
                                 <strong>Target Page:</strong>
                                 <%if (iecForCP != null)
                                    Response.Write(iecForCP.ContentElement.Parent.MainURL);%>
                                 <br />                                
                            <%}%>
                        </div>
                    <%}%>
                </div>
            </td>
            <td>
                <div>
                    <strong>HondaPlusCoverageComprehensive</strong>
                    <br />
                    <%if (this.ContentPage.Elements.ContainsKey("HondaPlusCoverageComprehensive"))
                    {%>    
                        <uc:ElementControl id="ElementControl12" runat="server" ElementName="HondaPlusCoverageComprehensive" />
                        ContentPointer Element Content<br />
                        <div>
                            <%HondaPlusCoverageComprehensive hpcc = this.ContentPage.Elements["HondaPlusCoverageComprehensive"].DeserializeElementObject() as HondaPlusCoverageComprehensive;
                            if (hpcc != null)
                            {%>
                                <strong>TODO:</strong>TODO<br />
                            <%}%>
                        </div>
                    <%}%>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="has-edit-button">
                    <uc:ElementControlCompaq id="ElementControlCompaq2" runat="server" ElementName="Specification" />
                    <strong>Specification</strong>
                    <br />
                    Specification element Content
                    <br />
                    <div>
                        <%Specification spec = (this.ContentPage.Elements["Specification"].DeserializeElementObject() as Specification);
                        if (spec != null && spec.ContentTable != null)
                        {%>
                            Table Heading: <%=spec.ContentTable.Heading%>
                            <br />
                            <%foreach (Section sec in spec.ContentTable.Sections)
                            {%>
                                <strong>Section Heading: <%=sec.Heading%> </strong>
                                <br />
                                <%for(int trimIndex = 0; trimIndex < sec.Trims.Count; trimIndex++)
                                {%>
                                    Trim Value: <%=sec.Trims[trimIndex]%>
                                    <br />                      
                                  
                                    <%foreach (TrimSectionValues trimSecV in sec.TrimValues)
                                    {%>
                                        trim section value title: <%=trimSecV.Title%>
                                        <strong>trim section value text:</strong>
                                        <%if (trimSecV.Value.Count > trimIndex)
                                            Response.Write(trimSecV.Value[trimIndex]);
                                        else
                                            Response.Write("<font color='red'>Error to get this value.</font>");%>
                                        <br />            
                                    <%}            
                                }
                            }
                        }%>
                    </div>
                </div>
            </td>
        </tr>
    </table>    
</asp:Content>
