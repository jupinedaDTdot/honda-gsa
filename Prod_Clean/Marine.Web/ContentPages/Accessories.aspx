﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="Accessories.aspx.cs" Inherits="Marine.Web.ContentPages.Accessories" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="Menu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/_Global/css/sections/accessories.css" />
    <link rel="stylesheet" href="/_Global/css/marine/sections/accessories.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    <uc:Menu ID="userMenu" runat="server" menuMainLiCSSClass="menu-accessories" menuCMSControlName="GenericContentPointer_CP"></uc:Menu>
    <li class="find-a-dealer">
	    <uc:FindADealer ID="FindADealer" runat="server" />
    </li> 
    <li id="Li1" class="accessories" runat="server">
        <div class="top_cap pf"></div>
	    <div class="content pf">
		    <a href="<%=ResourceManager.GetString("PartsServicesUrl")%>">
			    <img src="/_Global/img/layout/accessories-sidebar-img.gif" alt="" />
		    </a>
		    <p class="heading"><%=ResourceManager.GetString("PartsServices")%></p>
	    </div>
	    <div class="bottom_cap pf"></div>
    </li> 
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentArea" runat="server">
<div class="find">
    <div class="content_container">
	    <div class="content_section first_section">
		    <h1><%=ResourceManager.GetString("txtFindAccessories") %></h1>
		    <div class="fc questions">
			    <div class="question question1">
				    <h3><%=ResourceManager.GetString("txtSelectCategory")%></h3>
                    <asp:DropDownList ID="DropDownListCategory" runat="server" AutoPostBack="true"></asp:DropDownList>
                    <asp:HiddenField ID="HiddenFieldSelectedCategory" runat="server" />
			    </div>
			    <div class="question question2">
				    <h3><%=  Marine.Web.Code.CommonFunctions.ucFirst(ResourceManager.GetString("txtSelectModel"))%></h3>
                    <asp:Literal ID="LiteralModelDropDown" runat="server"></asp:Literal>
			    </div>
		    </div>
            <%--Province:
            <asp:DropDownList ID="DropDownListProvince" runat="server" AutoPostBack="false"></asp:DropDownList>--%>
	    </div>
        <asp:Panel ID="PanelAccessoriesResult" runat="server" Visible="true">
	    <div class="fc content_section product">
		    <div class="fl product_name">
			    <div class="h1"><asp:Literal ID="LiteralModelName" runat="server"></asp:Literal></div>
			    <div class="h2">
          <span><asp:Literal ID="LiteralSeriesName" runat="server"></asp:Literal></span>
          <a class="pf btn-help btn-tooltips" style="display:inline-block;position:relative;z-index:1;"
						property="act:tooltip"
						data-position="right bottom"
						data-tooltip-class="tooltip-right-alt"
						data-tooltip-content-target="#product-type-help1" id="ucToolTipSeries" runat="server" Visible="false"></a>
          </div>

				<asp:PlaceHolder runat="server" Visible="false" ID="phComingSoon">
                <div class="price coming-soon">
                    <span class="price-value"><%= ResourceManager.GetString("txtComingSoon") %></span>
				    <span class="price-label"><%=ResourceManager.GetString("MSRP")%></span>
                </div>
                </asp:PlaceHolder>
				<asp:PlaceHolder runat="server" Visible="false" ID="phHasNoDiscount">				
			    <div class="price">
				    <span class="price-value"><asp:Literal ID="LiteralMsrpValue" runat="server"></asp:Literal></span>
				    <span class="price-label"><%=ResourceManager.GetString("MSRP")%></span>
			    </div>
				</asp:PlaceHolder>
        <asp:PlaceHolder runat="server" Visible="False" ID="phHasDiscount">
			    <div class="fc small-price price msrp">
				    <del><span class="strike"></span><span class="price-value"><asp:Literal runat="server" ID="DiscountMsrp"></asp:Literal></span></del>
				    <span class="price-label"><%=ResourceManager.GetString("MSRP")%></span>
			    </div>
			    <div class="fc small-price price discount">
				    <span class="price-value"><asp:Literal runat="server" ID="DiscountDiscount"></asp:Literal></span>
				    <span class="price-label"><%=ResourceManager.GetString("Discount")%></span>
			    </div>
			    <div class="fc price">
				    <span class="price-value"><asp:Literal runat="server" ID="DiscountFinalPrice"></asp:Literal></span>
				    <span class="price-label"><%=ResourceManager.GetString("YourPrice")%></span>
			    </div>
        </asp:PlaceHolder>
		    </div>
		    <div class="fr product_image"><asp:Literal ID="LiteralImage" runat="server"></asp:Literal></div>
	    </div>
	    <%--<div class="nb content_section">
		    <div class="bar">
			    <h2><asp:Literal ID="LiteralAccessoryTitle" runat="server"></asp:Literal></h2>
		    </div>
			<ul class="accessories-list">
                <asp:Literal ID="LiteralAccessoryItems" runat="server"></asp:Literal>
			</ul>						
	    </div>--%>
        <asp:Repeater runat="server" ID="repAccessories" OnItemDataBound="repAccessories_ItemDataBound">
            <ItemTemplate>
                <div class="nb np nm content_section">
                    <asp:Literal runat="server" ID="litCategoryName"></asp:Literal>
                    <ul class="accessories-list">
                        <asp:Literal ID="LiteralAccessoryItems" runat="server"></asp:Literal>
                    </ul>
                    <asp:Literal ID="LiteralAccessoryToolTips" runat="server"></asp:Literal>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div class="disclaimer"><%=ResourceManager.GetString("txtAccessoriesDisclaimer") %></div>        
        </asp:Panel>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
    <div id="product-type-help1" class="dn">
	    <div class="tooltip_heading"><asp:Literal ID="LiteralTooltipSeriesHeader" runat="server"></asp:Literal></div>
        <p><asp:Literal ID="LiteralTooltipSeriesDescription" runat="server"></asp:Literal></p>
    </div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>