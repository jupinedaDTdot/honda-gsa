﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils.CMS;
using HondaCA.Service.Model;
using HondaCA.Entity;
using HondaCA.Common;
using HondaCA.Entity.Model;
using Marine.Service.Model;


namespace Marine.Web.ContentPages
{
    public partial class SiteMap : ContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            GetTrimList();

            FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH");
            litFFH.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (litFFH.Visible)
            {
                litFFH.Text = ffh.Html;
            }

        }

        private void GetTrimList()
        {
            StringBuilder sb = new StringBuilder();

            HondaModelService hmServices = new HondaModelService();
            EntityCollection<ModelCategory> modelCategories = hmServices.GetModelCategory(this.PageLanguage, this.TargetID);
            int cCnt = 0;
            sb.AppendFormat(@"<ul class=""fc inner_three_column_sitemap"">");
            foreach (ModelCategory mCat in modelCategories)
            {
                sb.AppendFormat(@"<li class=""fl{0}""><h2 class=""fnat"">{1}</h2>", cCnt++ == 0 ? " first" : "", mCat.CategoryName);
                sb.AppendFormat(@"<ul class=""sitemap"">");
                MarineTrimService trimService = new MarineTrimService();
                EntityCollection<Trim> listTrims = new EntityCollection<Trim>();
                listTrims = trimService.GetTrimWithBasePrice(mCat.CategoryUrl, this.TargetID, this.PageLanguage.GetCultureStringValue());
                foreach (Trim trim in listTrims)
                {
                    sb.AppendFormat(@"<li><a href=""{0}"">{1}</a></li>", "/" + mCat.CategoryUrl + "/" + trim.TrimUrl, trim.TrimName);
                }
                sb.AppendFormat(@"</ul></li>");
            }
            sb.AppendFormat(@"</ul>");
            phTrims.Controls.Add(new LiteralControl(sb.ToString()));
        }
    }
}