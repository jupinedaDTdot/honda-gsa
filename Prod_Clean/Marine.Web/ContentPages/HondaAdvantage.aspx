﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true" CodeBehind="HondaAdvantage.aspx.cs" Inherits="Marine.Web.ContentPages.HondaAdvantage" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="Menu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
    <link href="/_Global/css/marine/sections/honda-advantage.css" rel="stylesheet" type="text/css" />
    <link href="/_Global/css/marine/sections/commercial-product.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">    
    <div id="feature_area">        
        <div class="container"> 
            <uc:ElementControl ID="ElementControl1" runat="server" ElementName="GenericContent_Tout" />
            <asp:PlaceHolder ID="plTopBanner" runat="server"></asp:PlaceHolder> 
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<%--<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
        <uc:Menu ID="leftMenu" runat="server" menuMainLiCSSClass="menu-advantage" menuCMSControlName="GenericContentPointer_CP" menuCSSH4="sun_icon"></uc:Menu>   
        <li class="find-a-dealer">
            <uc:FindADealer ID="FindADealer1" runat="server" />	          
        </li>
</asp:Content>--%>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="cap_top_default pf"></div>
    <div class="container">
        <div class="content_container">
            <div>
                <uc:ElementControl ID="ElementControl2" runat="server" ElementName="GenericContent_FFH" />
                <asp:PlaceHolder ID="plMiddleContent" runat="server"></asp:PlaceHolder>
            </div>
            <div class="content_section" runat="server" id="divGC" visible="false">
                <uc:ElementControl ID="ElementControl3" runat="server" ElementName="GenericContent_GC1" />
	            <ul class="fc commercial-touts">                    
                    <asp:PlaceHolder ID="plGenericContent" runat="server"></asp:PlaceHolder>
                </ul>
            </div>
        </div>
    </div>
    <div class="cap_bottom_default pf"></div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BottomCap" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>
