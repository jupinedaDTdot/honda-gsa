﻿using System;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using System.Text;

namespace Marine.Web.ContentPages
{
	public class ContentBasePage : CmsContentBasePage
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
		}

		protected string setFreeFormHtml(string sElementName)
		{
			FreeFormHtml content = (this.ContentPage.Elements.ContainsKey(sElementName) ? this.ContentPage.Elements[sElementName].DeserializeElementObject() : null) as FreeFormHtml;
			if (content != null)
			{

				if (content.ContentElement.Display)
					return content.Html;
				else
					return string.Empty;
			}
			throw new Exception(string.Format("Element : {0} Not found in Page:{1}", sElementName, this.MainUrl));
		}

		public string setMenu(Navantis.Honda.CMS.Client.Elements.Menu content)
		{
			StringBuilder sb = new StringBuilder();
			StringBuilder sb2 = new StringBuilder();

			if (content != null)
			{

				if (content.ContentElement.Display || IsStaging == true)
				{
					if (content.Items != null)
					{
						sb.Append(@"<ul id=""left_nav"">");
						int topMenuCnt = 0;

						foreach (Navantis.Honda.CMS.Client.Elements.MenuItem menuItem in content.Items)
						{
							string liClass = "";
							if (topMenuCnt == 0)
							{
								liClass = "first";
							}
							//if ((menuItem.Url.ToLower() == Request.RawUrl.ToLower()) || (Request.RawUrl.ToLower().IndexOf(menuItem.Url.ToLower()).Equals(0)))
							if ((menuItem.Url ?? "").ToLower() == Request.RawUrl.ToLower())
							{
								liClass = liClass == "" ? "selected" : liClass + " selected";
							}
							liClass = (liClass == "") ? "" : @" class='" + liClass + "'";

							sb.AppendFormat(@"<li{0}><a href=""{1}"" {2}  >{3}</a></li>"
												, liClass
												, menuItem.Url ?? ""
												, menuItem.ChildItems != null ? ((menuItem.ChildItems.Count > 0) ? @" property=""act:menu"" data-menu-class=""left_nav"" data-menu-content-target=""#" + (menuItem.Name ?? "") + @"_Menu""" : "") : ""
												, menuItem.Text ?? "");
							if (menuItem.ChildItems != null)
							{
								if (menuItem.ChildItems.Count > 0)
								{
									int childMenuCnt = 0;
									sb2.AppendFormat(@"<div class=""dn"" id=""{0}""><ul class=""second_nav"">", (menuItem.Name ?? "") + "_Menu");
									foreach (Navantis.Honda.CMS.Client.Elements.MenuItem subMenuItem in menuItem.ChildItems)
									{
										sb2.Append(string.Format(@"<li{0}><a href=""{1}"">{2}</a></li>", childMenuCnt == 0 ? @" class=""first""" : childMenuCnt == menuItem.ChildItems.Count - 1 ? @" class=""last""" : "", subMenuItem.Url ?? "", subMenuItem.Text ?? ""));
										childMenuCnt++;
									}
									sb2.Append(@"</ul></div>");
								}
							}
							topMenuCnt++;
						}
						sb.Append(@"</ul>");
					}
				}
			}
			//else                
			//throw new Exception(string.Format("Element : {0} Not found in Page:{1}", content.ContentElement.ElementName, this.MainUrl));

			return sb.ToString() + sb2.ToString();
		}

		protected string setTout(string sElementName, bool setVisibility)
		{
			StringBuilder sb = new StringBuilder();
			Tout Tout = getTout(sElementName);

			if (Tout != null && Tout.Items != null)
			{
				if (Tout.ContentElement.Display || !setVisibility)
				{
					foreach (ToutItem tiItem in Tout.Items)
					{
						if (tiItem.URL == null)
						{
							sb.Append(string.Format("<img alt='{0}' src='{1}' />", tiItem.Title, tiItem.PathReference));
						}
						else
						{
							sb.Append(string.Format("<a href='{0}' title='{1}' >", tiItem.URL, tiItem.Title));//target='_blank'
							sb.Append(string.Format("<img alt='{0}' src='{1}' />", tiItem.Title, tiItem.PathReference));
							sb.Append("</a>");
						}
					}
					return sb.ToString();
				}
			}
			return "";
		}
	}
}