﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="OilAndChemicalResult.aspx.cs" Inherits="Marine.Web.ContentPages.OilAndChemicalResult" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="Menu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc"  TagName="FindADealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <link rel="Stylesheet" href="/_Global/css/sections/parts-service.css" type="text/css" />
    <link href="/_Global/css/marine/sections/parts-service.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    <ul class="fl widgets">
        <uc:Menu ID="userMenu" runat="server" menuMainLiCSSClass="menu menu-parts-service" menuCMSControlName="GenericContentPointer_CP"></uc:Menu>                			   
	    <li class="powerhouse-dealers-alternate" runat="server" id="divPowerHouse" visible="false">
	        <div class="top_cap pf"></div>
	        <div class="content pf">
		        <a href="<%=ResourceManager.GetString("urlPowerhouseDealers") %>">
			        <img src="/_Global/img/layout/powerhouse-dealers-sidebar-img.gif" alt="" />
		        </a>
		        <p class="heading"><%=ResourceManager.GetString("HondaPowerHouseDealer")%></p>
	        </div>
	        <div class="bottom_cap pf"></div>
        </li>
	    <li class="accessories" runat="server" id="divAccessories" visible="false">
            <div class="top_cap pf"></div>
	        <div class="content pf">
		        <a href="<%=ResourceManager.GetString("urlOilsAndChemicals") %>">
			        <img src="/_Global/img/layout/accessories-sidebar-img.gif" alt="" />
		        </a>
		        <p class="heading"><%=ResourceManager.GetString("txtGenuineOilChemical")%></p>
	        </div>
	        <div class="bottom_cap pf"></div>
        </li>        
        <li class="find-a-dealer">
            <uc:FindADealer ID="FindADealer" runat="server"></uc:FindADealer>                              
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="content_container">
        <div class="content_section first_section">
			<uc:ElementControl ID="ElementControl1" runat="server" ElementName="GenericContent_FFH" />
            <asp:PlaceHolder ID="plhTopFFH" runat="server"></asp:PlaceHolder>
		</div>
        <div class="content_section first_section top-pagination"  runat="server" id= "divPagerTop">
            <div class="pagination">
                <asp:DataPager runat="server" ID="dPagerTop" PagedControlID="ListViewAccessories" PageSize="9" Visible="true" OnInit="dPagerTop_Init" >                        
                    <Fields>                        
                        <asp:NextPreviousPagerField ShowPreviousPageButton="true" ShowNextPageButton="false" ButtonCssClass="pf page-prev"  />
                        <asp:NumericPagerField ButtonCount="10" CurrentPageLabelCssClass="pf page active" NumericButtonCssClass="pf page" />
                        <asp:NextPreviousPagerField ShowNextPageButton="true" ShowPreviousPageButton="false" ButtonCssClass="pf page-next" />
                    </Fields>
                </asp:DataPager>                    
            </div>
        </div>
        <div>
            <uc:ElementControl id="ElementControl2" runat="server"  ElementName="Accessories" Visible="true"/> 
            <div class="nb content_section">
                <asp:ListView ID="ListViewAccessories" runat="server" OnItemDataBound="ListViewAccessories_ItemDataBound" OnPagePropertiesChanged="ListViewAccessories_PagePropertiesChanged" OnPagePropertiesChanging="ListViewAccessories_PagePropertiesChanging">
                    <LayoutTemplate>
                        <asp:Placeholder runat="Server" ID="itemPlaceholder"></asp:Placeholder>                    
                    </LayoutTemplate>
                    <ItemTemplate>
                        <asp:Literal runat="server" ID="litdivProductRowTop" > </asp:Literal>
                        <div class="fl first product-item" style="position: relative;" runat="server" id="divproductitem">
                            <a  href="#" runat="server" id="hrefProdImg"
                                class="pf btn-tooltips"
                                property="act:tooltip"
                                data-position-my="right center"
								data-position-at="left center"
                                data-position-offset="-20px 0"
                                data-position-collision="fit"
								data-tooltip-class="tooltip-left-alt">

                                <div class="product-image" runat="server" id="divProdImg"><img alt="" runat="server" id="imgProdImg"></div>
                                <h3 class="price-heading"><asp:Literal runat="server" ID="litProdTitle"></asp:Literal></h3>
                                <div class="fc final price">
                                    <strong><asp:Literal runat="server" ID="litProdPrice"></asp:Literal></strong>
                                    <span><%=ResourceManager.GetString("MSRP")%></span>
                                </div>
                            </a>
                            <span class="product_overlay dn" runat="server" id="spanOverlayProd">
                                <div class="image"><img alt="" runat="server" id="OverlayProdImg" /></div>
                                <div class="desc">
                                    <h3><asp:Literal runat="server" ID="litOverlayProdTitle"/></h3>
                                    <span class="part-number"><asp:Literal runat="server" ID="litPrtNumber"></asp:Literal></span>
                                    <div class="fc final price">
                                        <asp:Literal runat="server" ID="litOverlayProdPrice"/>
                                        <span><%=ResourceManager.GetString("MSRP")%></span>
                                    </div>
                                    <asp:Literal runat="server" ID="litOverlayDiscriptionList"/>
                                    <div class="clr"></div>
                                </div>
                            </span>
                        </div>                        
                        <asp:Literal runat="server" ID="litdivProductRowBottom" > </asp:Literal>                        
                    </ItemTemplate>
                </asp:ListView>            
            </div>
        </div>
        <div class="content_section bottom-pagination" runat="server" id= "divPagerBottom">
            <div class="pagination">
                <asp:DataPager runat="server" ID="dPaggerBottom" PagedControlID="ListViewAccessories" PageSize="9" Visible="true" OnInit="dPagerTop_Init" >                        
                    <Fields>                        
                        <asp:NextPreviousPagerField ShowPreviousPageButton="true" ShowNextPageButton="false" ButtonCssClass="pf page-prev"  />
                        <asp:NumericPagerField ButtonCount="10" CurrentPageLabelCssClass="pf page active" NumericButtonCssClass="pf page" />
                        <asp:NextPreviousPagerField ShowNextPageButton="true" ShowPreviousPageButton="false" ButtonCssClass="pf page-next" />
                    </Fields>
                </asp:DataPager>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>
