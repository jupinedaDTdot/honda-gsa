﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Navantis.Honda.CMS.Client.Elements;
using Marine.Web.ContentPages;
using HondaCA.WebUtils.CMS;
using System.IO;
using Marine.Web.Code;
using System.Text;
//using HondaCA.Common;
//using System.Text;
//using Marine.Web.Code;
//using System.IO;

namespace Marine.Web.ContentPages
{
    public partial class OilAndChemicalResult : ContentBasePage
    {
        int rowCounter = 0;
        int abtCounter;
        int totItems = 0;
        int pageindex;

        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-parts-service");
                this.IsMasterBodyClassUpdate = true;
            }

            FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage,"GenericContent_FFH");
            plhTopFFH.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plhTopFFH.Visible)
            {
                plhTopFFH.Controls.Add(new LiteralControl(ffh.Html));
            }
            
            IElementContent element = this.ContentPage.Elements["GenericContentPointer_CP"].DeserializeElementObject() as IElementContent;
            bool isVisibleMenu = element.ContentElement.Display ? true : this.IsStaging ? true : false;            
            if (isVisibleMenu)
            {
                userMenu.menuTitle = ResourceManager.GetString("PartnService");
                userMenu.LoadData(setMenu(getMenuFromContentPointer("GenericContentPointer_CP")));
            }
            else
            {
                userMenu.menuCMSControlName = "GenericContentPointer_CP";
                userMenu.Visible = false;
            }

            Navantis.Honda.CMS.Client.Elements.Accessories accessories = getAccessoriesElement("Accessories");
            setAccessories(accessories);
            if (IsPostBack) rowCounter = 0;            

        }

        protected Navantis.Honda.CMS.Client.Elements.Accessories getAccessoriesElement(string sElementName)
        {
            Navantis.Honda.CMS.Client.Elements.Accessories content = CMSHelper.getCmsElementFromCmsPage<Navantis.Honda.CMS.Client.Elements.Accessories>(this.ContentPage, sElementName);
            return content;
        }

        private void setAccessories(Navantis.Honda.CMS.Client.Elements.Accessories accessories)
        {
            if (accessories != null)
            {
                try
                {
                    if (accessories.ContentElement.Display)
                    {
                        if (accessories.Items.Count > 0)
                        {
                            totItems = accessories.Items.Count;
                            ListViewAccessories.DataSource = accessories.Items;
                            ListViewAccessories.DataBind();
                            dPaggerBottom.PageSize = dPagerTop.PageSize;
                            if (accessories.Items.Count <= dPagerTop.PageSize + 1)
                            {
                                divPagerTop.Visible = false;
                                divPagerBottom.Visible = false;
                            }
                            else
                            {
                                divPagerTop.Visible = true;
                                divPagerBottom.Visible = true;
                            }
                        }
                    }
                }
                catch (Exception ex) { }
            }
        }

        protected void ListViewAccessories_PagePropertiesChanged(object sender, EventArgs e)
        {
            this.ListViewAccessories.DataBind();
        }

        protected void ListViewAccessories_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pageindex = e.StartRowIndex / e.MaximumRows;
            abtCounter = e.StartRowIndex;
        }

        protected void ListViewAccessories_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            int RowItem = 3;
            decimal price = 0;            
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                ListViewDataItem row = (ListViewDataItem) e.Item;
                AccessoriesItem aItem = row.DataItem as AccessoriesItem;                
                if (aItem != null)
                {
                    HtmlAnchor hrefProdImg = e.Item.FindControl("hrefProdImg") as HtmlAnchor;
                    if (rowCounter % RowItem == 0)
                    {                        
                        Literal litdivProductRowTop = e.Item.FindControl("litdivProductRowTop") as Literal;
                        hrefProdImg.Attributes.Add("data-tooltip-class", "tooltip-right-alt");
                        hrefProdImg.Attributes.Add("data-position-my", "left center");
                        hrefProdImg.Attributes.Add("data-position-at", "right center");
                        hrefProdImg.Attributes.Add("data-position-offset", "20px 0");                        
                        if (rowCounter == 0)
                        {
                            litdivProductRowTop.Text = string.Format(@"<div class=""fc first product"">");
                        }
                        else
                        {                            
                            litdivProductRowTop.Text = string.Format(@"</div><div class=""fc product"">");
                        }
                    }
                    if (rowCounter == dPagerTop.PageSize - 1 || (abtCounter == totItems - 1))
                    {
                        Literal litdivProductRowBottom = e.Item.FindControl("litdivProductRowBottom") as Literal;
                        litdivProductRowBottom.Text = "</div>";
                    }                    
                    HtmlGenericControl divproductitem = e.Item.FindControl("divproductitem") as HtmlGenericControl;
                    HtmlGenericControl spanOverlayProd = e.Item.FindControl("spanOverlayProd") as HtmlGenericControl;
                    HtmlImage imgProdImg = e.Item.FindControl("imgProdImg") as HtmlImage;
                    HtmlImage OverlayProdImg = e.Item.FindControl("OverlayProdImg") as HtmlImage;

                    Literal litProdPrice = e.Item.FindControl("litProdPrice") as Literal;
                    Literal litProdTitle = e.Item.FindControl("litProdTitle") as Literal;
                    Literal litOverlayProdTitle = e.Item.FindControl("litOverlayProdTitle") as Literal;
                    Literal litPrtNumber = e.Item.FindControl("litPrtNumber") as Literal;
                    Literal litOverlayProdPrice = e.Item.FindControl("litOverlayProdPrice") as Literal;
                    Literal litOverlayDiscriptionList = e.Item.FindControl("litOverlayDiscriptionList") as Literal;
                    
                    if (!string.IsNullOrEmpty(aItem.Image))
                    {
                        if (File.Exists(HttpContext.Current.Server.MapPath(aItem.Image)))
                        {
                            imgProdImg.Src = string.Format(@"{0}?maxheight=94&Crop=auto", aItem.Image);
                            OverlayProdImg.Src = string.Format(@"{0}?maxwidth=250&maxheight=222&Crop=auto", aItem.Image);
                        }
                        else
                        {
                            imgProdImg.Src = CommonFunctions.getDefaultImagesofProd(PageLanguage);
                            OverlayProdImg.Src = CommonFunctions.getDefaultImagesofOverlayProd(PageLanguage);
                        }
                    }
                    else
                    {
                        imgProdImg.Src = CommonFunctions.getDefaultImagesofProd(PageLanguage);
                        OverlayProdImg.Src = CommonFunctions.getDefaultImagesofOverlayProd(PageLanguage);
                    }

                    hrefProdImg.Attributes.Add("data-tooltip-content-target", ".product_overlay_"+ rowCounter.ToString().Trim());
                    hrefProdImg.HRef = aItem.Link;

                    spanOverlayProd.Attributes.Add("class", "product_overlay_"+ rowCounter.ToString().Trim()+" dn");

                    Decimal.TryParse(aItem.Price ?? string.Empty, out price);
                    if ((aItem.Price ?? string.Empty) == string.Empty || (aItem.Price ?? string.Empty) == "0")
                        litProdPrice.Text = string.Empty;
                    else if (price == 0)
                        litProdPrice.Text = aItem.Price;//display text if canot be converted to int 
                    else
                        litProdPrice.Text = string.Format(ResourceManager.GetString("valMSRP5"), price);

                    litOverlayProdPrice.Text = litProdPrice.Text;
                    
                    litProdTitle.Text = string.IsNullOrEmpty(aItem.Title) ? string.Empty :  string.IsNullOrEmpty(aItem.PartNumber) ? aItem.Title : aItem.Title + "<br> Part # :" + aItem.PartNumber;
                    litOverlayProdTitle.Text = litProdTitle.Text;
                    litOverlayDiscriptionList.Text = getAccessoriesDescription(aItem.Description ?? string.Empty);
                    divproductitem.Attributes.Add("class", string.Format("fl {0} product-item", (rowCounter % RowItem) == 0 ? "first" : string.Empty));
                    
                    rowCounter++;
                    abtCounter++;
                }
            }
        }

        private string getAccessoriesDescription(string description)
        {
            StringBuilder sb = new StringBuilder();
          //return description;
            if (!string.IsNullOrEmpty(description))
            {
              IList<string> descList = CommonFunctions.getDescriptionItems(description);

              if (descList.Count > 1)
              {
                sb.AppendFormat(@"<ul>");
                foreach (string str in descList)
                {
                  sb.AppendFormat(@"<li>{0}</li>", str);
                }
                sb.AppendFormat(@"</ul>");
              }
              else
              {
                return string.Format("<p>{0}</p>", descList[0]);
              }
            }
          return sb.ToString();
        }            

        protected void dPagerTop_Init(object sender, EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl.ToLower();
            base.OnInit(e);

            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            DataPager dp = (DataPager)sender;
            NextPreviousPagerField fieldPrev = (NextPreviousPagerField)dp.Fields[0];
            fieldPrev.PreviousPageText = ResourceManager.GetString("txtPrevious");
            NextPreviousPagerField fieldNext = (NextPreviousPagerField)dp.Fields[2];
            fieldNext.NextPageText = ResourceManager.GetString("txtNext");
        }
    }
}