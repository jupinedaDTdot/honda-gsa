﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils.CMS;

namespace Marine.Web.ContentPages
{
    public partial class PartAndService : ContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);            
        }
        protected void Page_Load(object sender, EventArgs e)
        {           
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();
            
            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "staging page page-parts-service-overview-product page-parts-service");
                this.IsMasterBodyClassUpdate = true;
            }

            IElementContent element = this.ContentPage.Elements["GenericContentPointer_CP"].DeserializeElementObject();
            Navantis.Honda.CMS.Client.Elements.Menu menu = CMSHelper.getElementThroughContentPointerFromCmsPage<Navantis.Honda.CMS.Client.Elements.Menu>(this.ContentPage, "GenericContentPointer_CP");
            userMenu.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (userMenu.Visible)
            {
                userMenu.menuTitle = ResourceManager.GetString("PartnService");
                userMenu.LoadData(setMenu(menu));
            }
            else
            {
                userMenu.menuCMSControlName = "GenericContentPointer_CP";                
            }

            switch (this.MainUrl)
            {
                case "/parts-service/honda-genuine-parts":
                case "/pieces-service/pieces-honda-dorigine":
                case "/parts-service/honda-genuine-oil-chemicals":
                case "/pieces-service/huiles-produits-chimiques-honda-dorigine":
                case "/parts-service/warranty":
                case "/pieces-service/garantie":
                case "/parts-service/storage-tips":
                case "/pieces-service/conseils-sur-le-remisage":
                case "/parts-service/maintenance-safety":
                case "/pieces-service/securite-dentretien":
                case "/pieces-service/garantie/questions-frequentes":
                case "/parts-service/warranty/faq":
                    divTout.Visible = true;
                    litContentTopDiv.Text = @"<div class=""content_container"">";
                    litContentBottomDiv.Text = @"</div>";
                    break;
            }

            if (divTout.Visible)
            {
                Tout tout = CMSHelper.getCmsElementFromCmsPage<Tout>(this.ContentPage, "GenericContent_Tout");
                plhTopTout.Visible = tout.ContentElement.Display ? true : this.IsStaging ? true : false;
                if (plhTopTout.Visible)
                {
                    if(tout != null)
                        plhTopTout.Controls.Add(new LiteralControl(getToutImage(tout)));
                }
            }

            FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH");
            plhMiddleTopContent.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plhMiddleTopContent.Visible)
            {
                plhMiddleTopContent.Controls.Add(new LiteralControl(ffh.Html));
            }

        }       
    }
}