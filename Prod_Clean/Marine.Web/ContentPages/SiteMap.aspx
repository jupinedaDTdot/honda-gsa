﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true" CodeBehind="SiteMap.aspx.cs" Inherits="Marine.Web.ContentPages.SiteMap" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/marine/sections/sitemap.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="cap_top_default pf"></div>
    <div class="container">
        <div class="content_container">
            <div class="fc content_section first_section">
                <h1><%=ResourceManager.GetString("SiteMap")%></h1>      
                <asp:PlaceHolder runat="server" ID="phTrims"></asp:PlaceHolder>   
            </div>
            <uc:ElementControl ID="ElementControl1" runat="server" ElementName="GenericContent_FFH" />
            <asp:Literal ID="litFFH" runat="server"></asp:Literal>
         </div>
    </div>
    <div class="cap_bottom_default pf"></div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BottomCap" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>
