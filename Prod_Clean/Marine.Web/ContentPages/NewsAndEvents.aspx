﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="NewsAndEvents.aspx.cs" Inherits="Marine.Web.ContentPages.NewsAndEvents" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="LeftSideMenu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/news-events.css" rel="stylesheet" type="text/css" />
    <link href="/_Global/css/marine/layout.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">    
    <uc:LeftSideMenu ID="leftsidemenu1" runat="server" menuMainLiCSSClass="menu-news-events" menuCMSControlName="GenericContentPointer_CP" />
    <li class="find-a-dealer">
        <uc:FindADealer ID="findadealer1" runat="server" />
    </li>    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="content_container">
        <div>
            <%--<uc:ElementControl ID="ElementControl1" runat="server" ElementName="GenericContent_FFH" />--%>
            <asp:PlaceHolder runat="server" ID="plmiddleFFH"></asp:PlaceHolder>
        </div>
        <div id="newDetail" runat="server" visible="false">
            <input type="hidden" name="DNewsID" id="DNewsID" value="<%=NewsID %>" />
            <input id="DNewstitle" name="DNewstitle" type="hidden" value="<%=Newstitle %>" />
            <%--<uc:ElementControl ID="newsDetailElement" runat="server" ElementName="GenericContentPointer_CP2" Visible="false" />--%>
            <asp:PlaceHolder runat="server" ID="plhNewsDetail" Visible="false"></asp:PlaceHolder>
        </div>
        <div runat="server" id="datapart" visible="false">
            <div class="content_section">
                <div>
			        <%--<uc:ElementControl ID="ElementControlGC" runat="server" ElementName="GenericContent_GC1" Visible="false" />--%>
                    <asp:PlaceHolder runat="server" ID="plhGenericContent" Visible="false"></asp:PlaceHolder>
                </div>
		    </div>
            <div>
                <%--<uc:ElementControl ID="ElementControlCP" runat="server" ElementName="GenericContentPointer_CP2" Visible="false" />--%>
                <asp:PlaceHolder runat="server" id="plhGenericContentPointer_CP2" Visible="false"></asp:PlaceHolder>
            </div>		    
       </div>

       <div runat="server" id="idvNewsList" visible="false">
            <%--<uc:ElementControl ID="ElementNews" runat="server" ElementName="GenericContentPointer_CP2" Visible="false" />--%>
            <input type="hidden" name="NewsID" id="NewsID" />
            <input id="Newstitle" name="Newstitle" type="hidden" runat="server" />
            <asp:ListView ID="ListView1" runat="server" 
                onitemdatabound="ListView1_ItemDataBound" Visible="false" OnPagePropertiesChanged="ListView_PagePropertiesChanged">
                <LayoutTemplate>
                    <asp:Placeholder runat="Server" ID="itemPlaceholder"></asp:Placeholder>                    
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="content_section_dotted">
						<h2><asp:Literal runat="server" ID="litTitle"/></h2>
						<h3><asp:Literal runat="server" ID="litDate"/></h3>
                        <asp:Literal runat="server" ID="litImage"/>						
                        <p><asp:Literal runat="server" ID="litSummery"/>
                        </p>
					</div>
                </ItemTemplate>
           </asp:ListView>

           <div class="np content_section bottom-pagination" runat="server" id="divPager" visible="false">
			    <div class="fc pagination">
					<asp:DataPager runat="server" ID="PeopleDataPager" PagedControlID="ListView1" PageSize="5" Visible="false" OnInit="PeopleDataPager_Init">                        
                        <Fields>
                            <asp:NextPreviousPagerField ShowPreviousPageButton="true" ShowNextPageButton="false" ButtonCssClass="fl pf page-prev"  />
                            <asp:NumericPagerField ButtonCount="5" CurrentPageLabelCssClass="fc page pf active" NumericButtonCssClass="fc pf page" />
                            <asp:NextPreviousPagerField ShowNextPageButton="true" ShowPreviousPageButton="false" ButtonCssClass="fr pf page-next" />
                        </Fields>
                    </asp:DataPager>
			    </div>
		    </div>
       </div>

       <div runat="server" id="divAutoTrade" visible="false">
            <div class="content_section questions">
				<div class="question question1">
					<h3><%=ResourceManager.GetString("txtSelectDivision")%></h3>
					<select>
                        <option><%=ResourceManager.GetString("txtEngines")%></option>
                        <option><%=ResourceManager.GetString("txtMarine")%></option>
						<option><%=ResourceManager.GetString("txtMotorcycles")%></option>                        
                        <option selected><%=ResourceManager.GetString("txtPowerEquipment")%></option>
                        
					</select>
				</div>
				<div class="question question2">

					<h3><%=ResourceManager.GetString("txtSelectDivision")%></h3>
					<select onchange="javascript:location.href=this.options[selectedIndex].value;">
						<option><%=ResourceManager.GetString("txtSelectOne")%></option>
						<asp:Literal ID="litProvince" runat="server"></asp:Literal>
					</select>
				</div>
				<div class="clr"></div>
			</div>
            <div>
                <%--<uc:ElementControl ID="ElementAutoTrade" runat="server" ElementName="GenericContent_GC1" Visible="false" />--%>
                <asp:PlaceHolder runat="server" id="plhAutoTrade" Visible="false"></asp:PlaceHolder>                
            </div>         
       </div>
    </div>


</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
<script>
    function ShowNewsDetail(id,title, url) {
        var form = document.getElementsByTagName('form')[0];

        $('input[name="NewsID"]').val(id);
        $('input[name="Newstitle"]').val(title);
        form.action = url;
        form.submit();
    }
    function changeCMSLanguage(url) {
        var form = document.getElementsByTagName('form')[0];
        form.action = url;
        form.submit();
    }
</script>
</asp:Content>
