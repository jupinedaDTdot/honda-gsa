﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils.CMS;

namespace Marine.Web.ContentPages
{
    public partial class Finance : ContentBasePage
    {
        protected string cssClassContainer = string.Empty;
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            cssClassContainer = (this.MainUrl == "/finance/faq" || this.MainUrl == "/financement/questions-frequentes") ? "faq privacy" : "credit faq privacy";

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-finance-overview page-finance");
                this.IsMasterBodyClassUpdate = true;
            }

            IElementContent element = this.ContentPage.Elements["GenericContentPointer_CP"].DeserializeElementObject();
            Navantis.Honda.CMS.Client.Elements.Menu menu = CMSHelper.getElementThroughContentPointerFromCmsPage<Navantis.Honda.CMS.Client.Elements.Menu>(this.ContentPage, "GenericContentPointer_CP");
            userMenu.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (userMenu.Visible)
            {
                userMenu.menuTitle = ResourceManager.GetString("txtFinance");
                userMenu.LoadData(setMenu(menu));
            }
            else
            {
                userMenu.menuCMSControlName = "GenericContentPointer_CP";
            }

            Tout tout = CMSHelper.getCmsElementFromCmsPage<Tout>(this.ContentPage, "GenericContent_Tout");
            plhTopTout.Visible = tout.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plhTopTout.Visible)
            {
                plhTopTout.Controls.Add(new LiteralControl(getToutImage(tout)));
            }

            FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage,"GenericContent_FFH");
            plhMiddleTopContent.Visible = ffh.ContentElement.Display ? true : IsStaging ? true : false;

            if (plhMiddleTopContent.Visible)
            {
                plhMiddleTopContent.Controls.Add(new LiteralControl(ffh.Html));
            }

           


        }
    }
}