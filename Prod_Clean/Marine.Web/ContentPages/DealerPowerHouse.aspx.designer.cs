﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Marine.Web.ContentPages {
    
    
    public partial class DealerPowerHouse {
        
        /// <summary>
        /// ElementControl1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::HondaCA.Web.Cms.Console.ElementControlCompaq ElementControl1;
        
        /// <summary>
        /// plTopBanner control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder plTopBanner;
        
        /// <summary>
        /// eleControlMiddle1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::HondaCA.Web.Cms.Console.ElementControlCompaq eleControlMiddle1;
        
        /// <summary>
        /// plhMiddleTopContent control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder plhMiddleTopContent;
        
        /// <summary>
        /// FindPHDDealer1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Marine.Web._Global.Controls.FindPHDDealer FindPHDDealer1;
        
        /// <summary>
        /// ElementControl2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::HondaCA.Web.Cms.Console.ElementControlCompaq ElementControl2;
        
        /// <summary>
        /// plGenericThreeColumns control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder plGenericThreeColumns;
        
        /// <summary>
        /// ElementControl3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::HondaCA.Web.Cms.Console.ElementControlCompaq ElementControl3;
        
        /// <summary>
        /// plGenericTwoColumns control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder plGenericTwoColumns;
        
        /// <summary>
        /// LiteralRegionSelectOptions control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal LiteralRegionSelectOptions;
    }
}
