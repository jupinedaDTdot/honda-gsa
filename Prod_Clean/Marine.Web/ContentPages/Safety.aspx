﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="Safety.aspx.cs" Inherits="Marine.Web.ContentPages.Safety" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="Menu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>
<%@ Register Src="~/_Global/Controls/DownloadLink.ascx" TagPrefix="uc" TagName="DownloadButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/safety.css" rel="stylesheet" type="text/css" />
    <link href="/_Global/css/marine/sections/safety.css" rel="stylesheet" type="text/css" />    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    <li class="buttons" runat="server" id="liDownload">
	    <div class="top_cap pf"></div>
	    <div class="content pf">
            <div class="edit-container-should-hide"><uc:ElementControl runat="server" ID="ElementCOntrol3" ElementName="GenericContentPointer_CP" /><%=(isStaging ? "Specific DLs"  : string.Empty)%></div>
		    <uc:DownloadButton runat="server" ID="DownloadButton1" anchocssClass="btn_large first" elementName="GenericContentPointer_CP" elementName2="GenericContent_Link" elementName3="GenericContent_Link" btncssClass="downloads" ></uc:DownloadButton>        
	    </div>       
    </li>
      <uc:Menu ID="userMenu" runat="server" menuMainLiCSSClass="menu safety" menuCMSControlName="GenericContentPointer_CP" ></uc:Menu> 
    <li class="find-a-dealer">
	    <uc:FindADealer ID="FindADealer1" runat="server" />
    </li>
    <li>&nbsp;</li>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="fr content_box">
        <div class="top_cap pf"></div>
			<div class="content pf">
                <div class="content_container">
					<div class="fc content_section first_section">
                        <div class="banner">
                            <uc:ElementControl runat="server" ID="ElementControl1" ElementName="GenericContent_Tout" />
                            <asp:PlaceHolder runat="server" ID="plhMainTout"></asp:PlaceHolder>
                        </div>
                        <h1 class="nm np"><%=ResourceManager.GetString("txtSafetyHeading") %></h1>
                    </div>
                    <div class="nb np content_section">
                        <div>
                            <uc:ElementControl id="ElementControl2" runat="server"  ElementName="GenericContent_FFH" />
                            <asp:PlaceHolder ID="plhMiddleTopContent" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>
                </div>
            </div>
		<div class="bottom_cap pf"></div>        
    </div>    
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>

