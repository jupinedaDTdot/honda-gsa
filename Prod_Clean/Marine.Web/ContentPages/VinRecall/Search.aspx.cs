﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;

using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.Common;
using HondaCA.Service.Common;
using HondaCA.Entity.Common;
using HondaCA.WebUtils.CMS;

namespace Marine.Web.ContentPages.VinRecall
{
    public partial class Search : ContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            InitBody();
            InitLeftNavMenu();
            InitDownloadButton();
            InitSearchForm();
        }

        private void InitBody()
        {
            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-safety");
                this.IsMasterBodyClassUpdate = true;
            }

            IElementContent element = this.ContentPage.Elements["GenericContent_FFH"].DeserializeElementObject() as IElementContent;
            plhMiddleTopContent.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;

            if (plhMiddleTopContent.Visible == true)
            {
                plhMiddleTopContent.Controls.Add(new LiteralControl(CMSHelperExtention.setFreeFormHtml("GenericContent_FFH", this.ContentPage, this.MainUrl)));
            }
        }

        private void InitDownloadButton()
        {
            IElementContent element = this.ContentPage.Elements["GenericContentPointer_CP2"].DeserializeElementObject();
            DownloadButton1.isVisible = element.ContentElement.Display ? true : IsStaging ? true : false;

            if (DownloadButton1.isVisible)
            {
                DownloadButton1.linkTitle = ResourceManager.GetString("Downloads");
                GenericLink gl = null;
                gl = getGenericLinkFromContentPointer("GenericContentPointer_CP2");
                DownloadButton1.LoadData(gl);
            }
        }

        private void InitLeftNavMenu()
        {
            IElementContent element = this.ContentPage.Elements["GenericContentPointer_CP"].DeserializeElementObject() as IElementContent;
            bool isVisibleMenu = element.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (isVisibleMenu)
            {
                userMenu.menuTitle = ResourceManager.GetString("Safety");
                userMenu.LoadData(setMenu(getMenuFromContentPointer("GenericContentPointer_CP")));
            }
            else
            {
                userMenu.menuCMSControlName = "GenericContentPointer_CP";
                userMenu.Visible = false;
            }
        }

        private void InitSearchForm()
        {
            lblEnterVinLabel.Text = ResourceManager.GetString("VinRecallLabel");
            lnkSubmitVin.Text = ResourceManager.GetString("VinRecallSubmitButtonText");
        }

        protected void lnkSubmitVin_Click(object s, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtVin.Text.Trim()))
            {
                Response.Redirect("~" + ResourceManager.GetString("VinRecallUrl") + "/" + txtVin.Text);
            }
        }
    }
}