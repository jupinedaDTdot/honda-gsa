﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="Results.aspx.cs" Inherits="Marine.Web.ContentPages.VinRecall.Results" %>

<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="Menu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>
<%@ Register Src="~/_Global/Controls/DownloadLink.ascx" TagPrefix="uc" TagName="DownloadButton" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/safety.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    <li class="buttons">
        <div class="top_cap pf">
        </div>
        <div class="content pf">
            <uc:DownloadButton runat="server" ID="DownloadButton1" anchocssClass="btn_large first" elementName="GenericContentPointer_CP2" btncssClass="downloads"></uc:DownloadButton>
        </div>
    </li>
    <li>
        <uc:Menu ID="userMenu" runat="server" menuMainLiCSSClass="menu safety" menuCMSControlName="GenericContentPointer_CP"></uc:Menu>
    </li>
    <li class="find-a-dealer">
        <uc:FindADealer ID="FIndADealer1" runat="server" />
    </li>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentArea" runat="server">
    <div id="inner_page_content" class="vin_lookup">
        <div class="section" runat="server" id="content_container">
            <h1>
                <asp:Label ID="lblVinRecallPageTitle" runat="server" /></h1>
            <div class="fc">
                <div class="fl your_vin">
                    <asp:Label ID="lblVinNumberTitle" runat="server" />
                    <strong>
                        <asp:Label ID="lblVinNumber" runat="server" /></strong>
                </div>
            </div>
            <div class="content_subsection">
                <uc:ElementControl ID="elInvalidVin" runat="server" ElementName="GenericContent_FFH" />
                <uc:ElementControl ID="elWrongProductLine" runat="server" ElementName="GenericContent_FFH2" />
                <uc:ElementControl ID="elValidVin" runat="server" ElementName="VinRecallDetails_FFH" />
                <asp:PlaceHolder ID="phMainContent" runat="server"></asp:PlaceHolder>
            </div>
            <div class="content_subsection" style="margin-right: -50px;">
                <asp:Panel ID="pnlRecallResults" runat="server">
                    <h2><asp:Literal runat="server" ID="ltRecallResultsTitle"></asp:Literal></h2>
                    <asp:GridView ID="gvRecallNotifications" ShowFooter="false" ShowHeader="false" GridLines="None" OnRowDataBound="gv_OnRowDataBound" AutoGenerateColumns="false" runat="server" CssClass="vin-recall-list">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hplPDFName" class="vin-recall-link" runat="server" Visible="false">
                                        <asp:Image ID="imgPdf" runat="server" ImageUrl="~/_Global/img/layout/gray_pdf_icon.gif" /></asp:HyperLink>
                                    <asp:PlaceHolder ID="phTitle" runat="server"></asp:PlaceHolder><br><br>
                                    <asp:PlaceHolder ID="phDescription" runat="server"></asp:PlaceHolder>
                                    <uc:ElementControl ID="elRecallDefaultDescription" runat="server" ElementName="GenericContent_FFH5" Visible="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>                    
                </asp:Panel>
                <br />
                <asp:Panel ID="pnlProductUpdateResults" runat="server">
                    <h2><asp:Literal runat="server" ID="ltProductUpdatesTitle"></asp:Literal></h2>
                    <asp:GridView ID="gvProductUpdates" ShowFooter="false" ShowHeader="false" GridLines="None" OnRowDataBound="gv_OnRowDataBound" AutoGenerateColumns="false" runat="server" CssClass="vin-recall-list">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hplPDFName" class="vin-recall-link" runat="server" Visible="false">
                                        <asp:Image ID="imgPdf" runat="server" ImageUrl="~/_Global/img/layout/gray_pdf_icon.gif" /></asp:HyperLink>
                                    <asp:PlaceHolder ID="phTitle" runat="server"></asp:PlaceHolder><br><br>
                                    <asp:PlaceHolder ID="phDescription" runat="server"></asp:PlaceHolder>
                                    <uc:ElementControl ID="elRecallDefaultDescription" runat="server" ElementName="GenericContent_FFH5" Visible="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>                    
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>
