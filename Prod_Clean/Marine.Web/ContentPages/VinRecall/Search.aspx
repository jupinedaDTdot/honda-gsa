﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master"
    AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="Marine.Web.ContentPages.VinRecall.Search" %>

<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="Menu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>
<%@ Register Src="~/_Global/Controls/DownloadLink.ascx" TagPrefix="uc" TagName="DownloadButton" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/safety.css" rel="stylesheet" type="text/css" />
    <link href="/_Global/css/common/forms.css" rel="stylesheet" type="text/css" />
    <link href="/_Global/css/mc/sections/safety.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    <li class="buttons">
        <div class="top_cap pf">
        </div>
        <div class="content pf">
            <uc:DownloadButton runat="server" ID="DownloadButton1" anchocssClass="btn_large first"
                elementName="GenericContentPointer_CP2" btncssClass="downloads"></uc:DownloadButton>
        </div>
    </li>
    <li>
        <uc:Menu ID="userMenu" runat="server" menuMainLiCSSClass="menu safety" menuCMSControlName="GenericContentPointer_CP">
        </uc:Menu>
    </li>
    <li class="find-a-dealer">
        <uc:FindADealer ID="FIndADealer1" runat="server" />
    </li>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="content_container">
        <uc:ElementControl ID="ElementControl1" runat="server" ElementName="GenericContent_FFH"
            Visible="true" />
        <asp:PlaceHolder ID="plhMiddleTopContent" runat="server"></asp:PlaceHolder>
    </div>
    <div class="vin-recall-search-container">
        <asp:Label ID="lblEnterVinLabel" runat="server" />
        <asp:TextBox ID="txtVin" runat="server" />
        <asp:LinkButton ID="lnkSubmitVin" OnClick="lnkSubmitVin_Click" runat="server" CssClass="submit-link" />
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd"
    runat="server">
</asp:Content>
