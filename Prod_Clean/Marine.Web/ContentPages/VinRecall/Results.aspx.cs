﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.Common;
using HondaCA.Entity.Common;
using HondaCA.Entity.VinRecall;
using HondaCA.Service.Common;
using HondaCA.WebUtils.CMS;

namespace Marine.Web.ContentPages.VinRecall
{
    /// <summary>
    /// Results of a VIN recall search
    /// 
    /// Resource strings used on this page:
    /// - VinRecallLookupPageTitle
    /// - VinRecallNumLabel
    /// - VinRecallNoDescriptionTitle
    /// - VinRecallResultsUrl
    /// - VinRecallUrl
    /// - VinRecallNoDescriptionTitle
    /// - Downloads
    /// - Safety
    /// 
    /// App Settings used on this page:
    /// - ProductLineCode
    /// - VinRecallPdfDirPath
    /// - X:RecallsUrl
    /// </summary>
    public partial class Results : ContentBasePage
    {
        private string _productLine;
        private HondaCA.Service.VinRecall.VinRecallService _vinRecallService;
        private string lang;

        protected override void OnInit(EventArgs e)
        {
            lang = Request.QueryString["lang"];

            if (lang == "fr")
            {
                this.PageLanguage = HondaCA.Common.Language.French;
            }
            else
            {
                this.PageLanguage = HondaCA.Common.Language.English;
            }

            base.InitializeCulture();

            // The URL assigned to this page in CMS is /safety/recalls/results but it is accessed using
            // a VIN URL parameter (/safety/recalls/xxxxxxxxx) so we have to assign the URL manually
            this.MainUrl = ResourceManager.GetString("VinRecallResultsUrl");
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _vinRecallService = new HondaCA.Service.VinRecall.VinRecallService();
            _productLine = "P"; //System.Configuration.ConfigurationManager.AppSettings["ProductLineCode"];

            InitBody();
            InitLeftNavMenu();
            InitDownloadButton();

            // Make sure VIN supplied in the query string is there and not equal to the hidden page URL (/results)
            if (Request.QueryString["vin"] != null && Request.QueryString["vin"] != "results")
            {
                ProcessVin(Request.QueryString["vin"]);
            }
            else
            {
                Response.Redirect(ResourceManager.GetString("VinRecallUrl"));
            }
        }

        /// <summary>
        /// Process the VIN extracted from the URL
        /// </summary>
        /// <param name="vin">A non-empty string representing the Vehicle Identification Number</param>
        private void ProcessVin(string vin)
        {
            HondaCA.Service.VinRecall.VinService vinService = new HondaCA.Service.VinRecall.VinService();
            string lang = this.PageLanguage == Language.English ? "en" : "fr";

            lblVinRecallPageTitle.Text = ResourceManager.GetString("VinRecallLookupPageTitle");
            lblVinNumberTitle.Text = ResourceManager.GetString("VinRecallNumLabel");
            lblVinNumber.Text = vin;

            Vin vinInfo = vinService.GetVinInfo(vin, lang);

            if (vinInfo == null)
            {
                elInvalidVin.Visible = true;
                elWrongProductLine.Visible = false;
                elValidVin.Visible = false;
                pnlRecallResults.Visible = false;
                pnlProductUpdateResults.Visible = false;

                ProcessInvalidVin();
            }
            else
            {
                elInvalidVin.Visible = false;

                if (vinInfo.ProductLine == null)
                {
                    throw new Exception("The ProductLine property for the VIN [" + vin + "] is null");
                }

                // Check if the VIN is from the current brand        
                if (vinInfo.ProductLine != _productLine)
                {
                    elWrongProductLine.Visible = true;
                    elValidVin.Visible = false;
                    ProcessIncorrectProductLineVin(vin, vinInfo.ProductLine);                    
                }
                else
                {
                    // Valid VIN belonging to the current product line
                    pnlRecallResults.Visible = false;
                    pnlProductUpdateResults.Visible = false;
                    elWrongProductLine.Visible = false;
                    elValidVin.Visible = true;
                    ProcessValidVin(vin, lang);
                }
            }
        }

        /// <summary>
        /// Handle a VIN that was not found or invalid
        /// </summary>
        private void ProcessInvalidVin()
        {
            phMainContent.Controls.Add(new LiteralControl(setFreeFormHtml("GenericContent_FFH")));
        }

        /// <summary>
        /// Process a valid VIN that belongs to a different product line and display results to the user
        /// </summary>
        private void ProcessIncorrectProductLineVin(string vin, string productLine)
        {
            // TODO: This needs to be modified to account for Honda/Acura being /recalls/xxxx and MCPE being /safety/recalls/xxxx
            var wrongBrandHtml = setFreeFormHtml("GenericContent_FFH2");

            // Get the appropriate URL from the Web.config
            var recallsUrl = System.Configuration.ConfigurationManager.AppSettings[string.Format("{0}:RecallsUrl", productLine)];
            recallsUrl = Path.Combine(recallsUrl, vin);

            switch (productLine)
            {
                case "H":
                    {
                        if (this.PageLanguage == Language.French)
                        {
                            recallsUrl = recallsUrl.Replace("recalls", "recallsfre");
                        }
                        wrongBrandHtml = wrongBrandHtml.Replace("{BRAND_NAME}", "Honda automobile").Replace("{BRAND_RECALL_URL}", recallsUrl);
                        break;
                    }
                case "A":
                    {
                        if (this.PageLanguage == Language.French)
                        {
                            recallsUrl = recallsUrl.Replace("recalls", "recallsfre");
                        }
                        wrongBrandHtml = wrongBrandHtml.Replace("{BRAND_NAME}", "Acura").Replace("{BRAND_RECALL_URL}", recallsUrl);
                        break;
                    }
                case "M":
                    {
                        if (this.PageLanguage == Language.French)
                        {
                            recallsUrl = recallsUrl.Replace("safety/recalls", "securite/recalls_fr");
                        }
                        wrongBrandHtml = wrongBrandHtml.Replace("{BRAND_NAME}", ResourceManager.GetString("txtMotorcycles")).Replace("{BRAND_RECALL_URL}", recallsUrl);
                        break;
                    }
                case "P":
                    {
                        if (this.PageLanguage == Language.French)
                        {
                            recallsUrl = recallsUrl.Replace("safety/recalls", "securite/recalls_fr");
                        }
                        wrongBrandHtml = wrongBrandHtml.Replace("{BRAND_NAME}", ResourceManager.GetString("txtPowerEquipment")).Replace("{BRAND_RECALL_URL}", recallsUrl);
                        break;
                    }
            }

            phMainContent.Controls.Add(new LiteralControl(wrongBrandHtml));
        }

        /// <summary>
        /// Process a valid VIN and display results to the user
        /// </summary>
        private void ProcessValidVin(string vin, string lang)
        {
            HondaCA.Entity.EntityCollection<HondaCA.Entity.Model.VinRecallType> recalls = new HondaCA.Entity.EntityCollection<HondaCA.Entity.Model.VinRecallType>();

            recalls = _vinRecallService.GetVinRecallInfo(vin, lang);

            if (recalls.Count() == 0)
            {
                // No recalls found
                phMainContent.Controls.Add(new LiteralControl(setFreeFormHtml("GenericContent_FFH4")));
            }
            else
            {
                // Split into recalls and product updates
                var productRecalls = recalls.Where(x => x.IsRecall);
                var productUpdates = recalls.Where(x => !x.IsRecall);

                // Recalls
                if (productRecalls.Count() > 0)
                {
                    ltRecallResultsTitle.Text = string.Format("{0}&nbsp;({1})", ResourceManager.GetString("VinRecallNotificationsTitle"), productRecalls.Count());
                    pnlRecallResults.Visible = true;
                    gvRecallNotifications.DataSource = productRecalls;
                    gvRecallNotifications.DataBind();
                }
                else
                {
                    pnlRecallResults.Visible = false;
                }

                // Product updates
                if (productUpdates.Count() > 0)
                {
                    ltProductUpdatesTitle.Text = string.Format("{0}&nbsp;({1})", ResourceManager.GetString("VinRecallProductUpdateNotificationsTitle"), productUpdates.Count());
                    pnlProductUpdateResults.Visible = true;
                    gvProductUpdates.DataSource = productUpdates;
                    gvProductUpdates.DataBind();
                }
                else
                {
                    pnlProductUpdateResults.Visible = false;
                }

                phMainContent.Controls.Add(new LiteralControl(setFreeFormHtml("VinRecallDetails_FFH")));
            }
        }

        /// <summary>
        /// Populate a GridView row in the list of results with appropriate data
        /// </summary>
        private void PopulateGridRow(GridViewRow gvRow)
        {
            // Get the row placeholder controls
            HyperLink hplPDFName = (HyperLink)gvRow.FindControl("hplPDFName");
            PlaceHolder phTitle = (PlaceHolder)gvRow.FindControl("phTitle");
            PlaceHolder phDescription = (PlaceHolder)gvRow.FindControl("phDescription");
            Image imgPdf = (Image)gvRow.FindControl("imgPdf");

            // Get the data
            string description = (string)DataBinder.Eval(gvRow.DataItem, "Description");
            string title = (string)DataBinder.Eval(gvRow.DataItem, "Title");
            string pdfName = (string)DataBinder.Eval(gvRow.DataItem, "PDFName");
            var pdfPath = string.IsNullOrEmpty(pdfName) ? null : GetPdfFilePath(GetPdfFileName(pdfName));

            // One of the new requirements is to change the default description (when the recall campaign doesn't have one) to 
            // a CMS element so the business can update it themselves. The following logic either adds a literal control with that
            // CMS element for no description or a label with the campaign description if one exists.  Note that if no description exists
            // there _should_ be no title or PDF either so the entire row in the grid will be the default description.  
            // So the row will either be:
            //  [PDF link] [Title] [Description]
            //      or
            //  [Description]
            //GG - Use title instead of description
            //if (string.IsNullOrEmpty(description))
            if (string.IsNullOrEmpty(title))
            {
                LiteralControl ltDescription = new LiteralControl(setFreeFormHtml("GenericContent_FFH5"));
                phDescription.Controls.Add(ltDescription);
            }
            else
            {
                // Check for a PDF
                if (pdfPath != null)
                {
                    hplPDFName.NavigateUrl = pdfPath;               
                }else{
                    hplPDFName.NavigateUrl = "";
                    imgPdf.ImageUrl = "~/_Global/img/layout/honda_logo_marines.jpg";
                    //imgPdf.ImageUrl = ResourceManager.GetString("imgPDFURL");
                }
                //imgPdf.Width = 78;
                //imgPdf.Height = 63;
                hplPDFName.Visible = true;

                Label lblTitle = new Label();
                lblTitle.CssClass = "vin-recall-title";
                lblTitle.Text = title;
                phTitle.Controls.Add(lblTitle);


                // Check for description
                if (!string.IsNullOrEmpty(description))
                {
                    Label lblDescription = new Label();
                    lblDescription.CssClass = "vin-recall-desc";
                    lblDescription.Text = description;
                    phDescription.Controls.Add(lblDescription); 
                }

               
            }
        }

        /// <summary>
        /// Get the full path of a PDF file based on its name
        /// </summary>
        /// <param name="pdfName"></param>
        /// <returns>The full path of the PDF or null if the file does not exist</returns>
        private string GetPdfFilePath(string pdfName)
        {
            var vinRecallPath = System.Configuration.ConfigurationManager.AppSettings["VinRecallPath"];
            var vinPdfPath = Path.Combine(vinRecallPath, GetPdfFileName(pdfName));

            if (!File.Exists(Server.MapPath(vinPdfPath))) return null;
            return vinPdfPath;
        }

        private void InitBody()
        {
            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-safety");
                this.IsMasterBodyClassUpdate = true;
            }
        }

        private void InitDownloadButton()
        {
            IElementContent element = this.ContentPage.Elements["GenericContentPointer_CP2"].DeserializeElementObject();
            DownloadButton1.isVisible = element.ContentElement.Display ? true : IsStaging ? true : false;

            if (DownloadButton1.isVisible)
            {
                DownloadButton1.linkTitle = ResourceManager.GetString("Downloads");
                GenericLink gl = null;
                gl = getGenericLinkFromContentPointer("GenericContentPointer_CP2");
                DownloadButton1.LoadData(gl);
            }
        }

        private void InitLeftNavMenu()
        {
            IElementContent element = this.ContentPage.Elements["GenericContentPointer_CP"].DeserializeElementObject() as IElementContent;
            bool isVisibleMenu = element.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (isVisibleMenu)
            {
                userMenu.menuTitle = ResourceManager.GetString("Safety");
                userMenu.LoadData(setMenu(getMenuFromContentPointer("GenericContentPointer_CP")));
            }
            else
            {
                userMenu.menuCMSControlName = "GenericContentPointer_CP";
                userMenu.Visible = false;
            }
        }

        private string GetPdfFileName(string pdf)
        {
            string tmppdfname = string.Empty;
            if (pdf != null)
            {
                if (pdf.Contains(@"/"))
                {
                    tmppdfname = pdf.Substring(pdf.LastIndexOf(@"/") + 1, pdf.Length - (pdf.LastIndexOf(@"/") + 1));
                }
                else
                    tmppdfname = pdf;
            }

            return tmppdfname;
        }

        protected void gv_OnRowDataBound(object s, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                PopulateGridRow(e.Row);
            }
        }
    }
}