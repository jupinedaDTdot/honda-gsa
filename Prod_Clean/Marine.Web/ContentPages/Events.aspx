﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="Marine.Web.ContentPages.Events" %>

<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="LeftSideMenu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/news-events.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/_Global/css/pe/news-events.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    
    <uc:LeftSideMenu ID="leftsidemenu1" runat="server" menuMainLiCSSClass="menu-news-events" menuCMSControlName="GenericContent_Menu" />
    <li class="find-a-dealer">
        <uc:FindADealer ID="findadealer1" runat="server" />
    </li>    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BgIllustration" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ContentArea" runat="server">

<div class="content_container">
					<div class="events">
                    <%--<uc:ElementControl ID="ElementControl2" runat="server" ElementName="GenericContentPointer_CP2" />--%>
						<div class="fc title_section content_section first_section">
							<h1 class="fl"><%=ResourceManager.GetString("txtEvents")%></h1>
							<asp:LinkButton Visible="false" class="fr btn secondary download" target="_blank" runat="server" ID= "hrefDownloadEvents" OnClick="hrefDownloadEvents_Click" ><span><%=ResourceManager.GetString("DownloadEventsListing")%></span> </asp:LinkButton>
						</div>
						<div class="content_section">
							<div class="fc three-boxes boxes">
								<div class="first box">
									<div class="h3"><%=ResourceManager.GetString("SelectADivision")%></div>
                                    <asp:DropDownList ID="DropDownListDivision" OnTextChanged="DropDownListDivision_OnSelectedIndexChanged" runat="server"  OnSelectedIndexChanged="DropDownListDivision_OnSelectedIndexChanged"  AutoPostBack="true"></asp:DropDownList>
								</div>
								<div class="box">
									<div class="h3"><%=ResourceManager.GetString("SelectAProvince")%></div class="h3">
                                    <asp:DropDownList ID="DropDownListProvince" OnTextChanged="DropDownListProvince_OnSelectedIndexChanged" runat="server"  OnSelectedIndexChanged="DropDownListProvince_OnSelectedIndexChanged"  AutoPostBack="true"></asp:DropDownList>
								</div>
								<div class="box">
									<div class="h3"><%=ResourceManager.GetString("SelectEventType")%></div>
                                    <asp:DropDownList ID="DropDownListEventType" OnTextChanged="DropDownListEventType_OnSelectedIndexChanged" runat="server"  OnSelectedIndexChanged="DropDownListEventType_OnSelectedIndexChanged"  AutoPostBack="true"></asp:DropDownList>                                    
								</div>
							</div>
						</div>

                      <asp:ListView ID="ListViewEvents" runat="server" onitemdatabound="ListViewEvents_ItemDataBound" Visible="true" OnPagePropertiesChanged="ListViewEvents_PagePropertiesChanged"  OnPagePropertiesChanging="ListViewEvents_PagePropertiesChanging"  >
                            <LayoutTemplate>
                                <asp:Placeholder runat="Server" ID="itemPlaceholder"></asp:Placeholder>                    
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div class="content_section_dotted" runat="server" id="divContentSection">
							    <div class="event-type" runat="server" id="divEventType"><asp:Literal runat="server" ID="litEventType"></asp:Literal></div>
							    <h2><asp:Literal runat="server" ID="litEventTitle"></asp:Literal></h2>
							    <div class="event-image" runat="server" id="divEventImage"><img  alt="" runat="server" id="imgEventImage" /></div>
							    <dl class="fc event-item">
								    <div class="fl event-item-col" runat="server" id="divDate">
									    <dt><%=ResourceManager.GetString("Date")%></dt>
									    <dd><asp:Literal runat="server" ID="litDate"></asp:Literal></dd>
								    </div>
								    <div class="fl event-item-col" runat="server" id="divLocation">
									    <dt><%=ResourceManager.GetString("Location")%></dt>
									    <dd><asp:Literal runat="server" ID="litLocation"></asp:Literal></dd>
								    </div>
								    <div class="fl" runat="server" id="divHours">
									    <dt><%=ResourceManager.GetString("txtHours")%></dt>
									    <dd><asp:Literal runat="server" ID="litHours"></asp:Literal></dd>
								    </div>
							    </dl>		
                                <p runat="server" id="paraPreviewDesc"><asp:Literal runat="server" ID="litPreviewDesc" > </asp:Literal></p>                                				
							    <div class="fc buttons" runat="server" id="divButtons">
								    <a class="fl btn secondary first" runat="server" id="hrefFindOutMore" target="_blank"><span><%=ResourceManager.GetString("FindOutMore")%></span></a>
								    <a class="fl btn secondary" runat="server" id="hrefDownloadPDF"><span><%=ResourceManager.GetString("downloadpdf")%></span></a>
							    </div>
						    </div>                               
                          </ItemTemplate>
                     </asp:ListView>
                    
                    <div class="fc content_section np" runat="server" id="divDataPager">
                        <div class="fc pagination">					
					    <asp:DataPager runat="server" ID="DataPager1" PagedControlID="ListViewEvents" PageSize="8" Visible="true" OnInit="DataPager1_Init"  >                        
                            <Fields>
                                <asp:NextPreviousPagerField ShowPreviousPageButton="true" ShowNextPageButton="false" ButtonCssClass="pf page-prev"  />
                                <asp:NumericPagerField ButtonCount="10" CurrentPageLabelCssClass="pf page active" NumericButtonCssClass="page" />
                                <asp:NextPreviousPagerField ShowNextPageButton="true" ShowPreviousPageButton="false" ButtonCssClass="pf page-next" />
                            </Fields>
                        </asp:DataPager>
    			        </div>
                    </div>

                       <div runat="server" id="divNoEvents" visible="false">
                        <h3><asp:Literal runat="server" ID="LitNoEvents" > </asp:Literal>  </h3>
                    </div>

					</div>

</div>


</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>
