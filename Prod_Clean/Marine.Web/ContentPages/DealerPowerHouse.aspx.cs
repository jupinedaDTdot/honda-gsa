﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Navantis.Honda.CMS.Client.Elements;
using Navantis.Honda.CMS.Demo;
using System.Text;
using HondaCA.Common;
using HondaDealer.Service;
using HondaDealer.Entity;


namespace Marine.Web.ContentPages
{
    public partial class DealerPowerHouse : ContentBasePage
    {
        protected string domain = "http://" + HondaCA.Common.Global.CurrentDomain.ToLower();

        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-find-a-dealer-powerhouse");
                this.IsMasterBodyClassUpdate = true;
            }

            IElementContent element = this.ContentPage.Elements["GenericContent_Tout"].DeserializeElementObject() as IElementContent;
            plTopBanner.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;

            if (plTopBanner.Visible == true)
                plTopBanner.Controls.Add(new LiteralControl(setTout("GenericContent_Tout",plTopBanner.Visible)));

            element = this.ContentPage.Elements["GenericContent_FFH"].DeserializeElementObject() as IElementContent;
            plhMiddleTopContent.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;


            if (plhMiddleTopContent.Visible == true)
                plhMiddleTopContent.Controls.Add(new LiteralControl(setFreeFormHtml("GenericContent_FFH")));

            GenericContent gContent = this.ContentPage.Elements["GenericContent_GC1"].DeserializeElementObject() as GenericContent;
            plGenericThreeColumns.Visible = gContent.ContentElement.Display ? true : this.IsStaging ? true : false;            

            if(plGenericThreeColumns.Visible == true)
                plGenericThreeColumns.Controls.Add(new LiteralControl(setGenericContent(gContent)));
        
            gContent = this.ContentPage.Elements["GenericContent_GC2"].DeserializeElementObject() as GenericContent;
            plGenericTwoColumns.Visible = gContent.ContentElement.Display ? true : this.IsStaging ? true : false;            

            if(plGenericTwoColumns.Visible == true)
                plGenericTwoColumns.Controls.Add(new LiteralControl(setGenericContentTwoCol(gContent)));

            //Find a dealer Button
            //HyperLinkFindADealer.Text = string.Format("<span>{0}</span>",ResourceManager.GetString("FindADealer"));
            //HyperLinkFindADealer.Attributes.Add("class", (PageLanguage == Language.French? "btn secondary_large": "btn secondary"));
            //HyperLinkFindADealer.NavigateUrl = ResourceManager.GetString("DealerLocatorRootFolder");

            //Region Drop down for Map
            DoRegionDropDown();
        }

        protected string setGenericContent(GenericContent content)
        {
            StringBuilder sb = new StringBuilder();
            if (content != null)
            {
                if (content.ContentElement.Display || IsStaging == true)
                {
                    if (content.Items != null)
                    {
                        int firstElement = 0;
                        foreach(Navantis.Honda.CMS.Client.Elements.GenericContentItem item in content.Items)
                        {
                            string cssClass = firstElement == 0 ? "col first" : "col";
                            sb.Append(string.Format(@"<div class=""{0}""><img src=""{1}"" alt=""{2}"" title=""{2}"" /><h3>{2}</h3>{3}</div>", cssClass,item.Image,item.Title,item.Summary));                            
                            firstElement++;
                        }
                    }
                }
            }
            return sb.ToString();
        }

        protected string setGenericContentTwoCol(GenericContent content)
        {
            StringBuilder sb = new StringBuilder();
            if (content != null)
            {
                if (content.ContentElement.Display || IsStaging == true)
                {
                    if (content.Items != null)
                    {
                        int firstElement = 0;
                        foreach(Navantis.Honda.CMS.Client.Elements.GenericContentItem item in content.Items)
                        {
                            string cssClass = firstElement == 0 ? "sub_section honda_hotel" : "sub_section powerhouse_special_offer";
                            string cssClass1 = firstElement == 0 ? "class='download'":"";
                            sb.Append(string.Format(@"<div class=""{0}""><h3>{1}</h3>{2}<p {3}><a href=""{4}"">{5}</a></p></div>", cssClass,item.Title,item.Summary,cssClass1,item.MoreTextLinkURL,item.MoreText));                            
                            firstElement++;
                        }
                    }
                }
            }
            return sb.ToString();
        }

        protected void DoRegionDropDown()
        {
            HondaDealer.Service.RegionService rs = new HondaDealer.Service.RegionService();
            EntityCollection<Region> regionList = null;
            try
            {
                regionList = rs.GetAllRegions(PageLanguage.GetCultureStringValue());
            }
            catch (Exception)
            {
                regionList = null;
            }

            if (regionList != null)
            {
                LiteralRegionSelectOptions.Text = string.Empty;
                foreach (HondaDealer.Entity.Region region in regionList)
                {
                    LiteralRegionSelectOptions.Text += string.Format(@"<option value=""{0}"" rel=""{1}"" title=""{2}"">{3}</option>"
                                                                        ,string.Format("{0}/{1}", ResourceManager.GetString("urlFindPHD"),region.ProvinceUrlName)
                                                                        ,region.Code
                                                                        ,string.Format(ResourceManager.GetString("DealerTitleByProv"),region.Name)
                                                                        ,region.Name);
                }
            }
        }
    }
}