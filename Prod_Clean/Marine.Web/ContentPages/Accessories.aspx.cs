﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Marine.Service.Extensions;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils.CMS;
using HondaCA.Entity;
using HondaCA.Service.Model;
using HondaCA.Entity.Model;
using System.Text;
using HondaCA.Common;
using System.IO;
using HondaAccessories.Service;
using HondaAccessories.Entity;
using Marine.Web.Code;
using Navantis.Honda.CMS.Demo;

namespace Marine.Web.ContentPages
{
  public partial class Accessories : ContentBasePage
  {
    private int SelectedTrimID;
    private string SelectedModelFamilyUrl;
    private string ImageFolder = string.Empty;
    

    protected override void OnInit(EventArgs e)
    {
      this.MainUrl = HttpContext.Current.Request.RawUrl;
      base.OnInit(e);
      System.Web.UI.HtmlControls.HtmlControl ctrlBody = (System.Web.UI.HtmlControls.HtmlControl)this.Page.Master.Master.Master.FindControl("body");
      if (ctrlBody != null)
      {
        ctrlBody.Attributes["class"] = ctrlBody.Attributes["class"] == null ? "page-accessories" : " page-accessories";
      }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      this.PageLanguage = getLanguageForCMSPage();
      base.InitializeCulture();
      //Set the following value in WebConfig's appsetting
      ImageFolder = string.Format(@"{0}/{1}/", ConfigurationManager.AppSettings["AssetPathAccessories"], 9999);
      IElementContent element = this.ContentPage.Elements["GenericContentPointer_CP"].DeserializeElementObject() as IElementContent;
      Navantis.Honda.CMS.Client.Elements.Menu menu = CMSHelper.getElementThroughContentPointerFromCmsPage<Navantis.Honda.CMS.Client.Elements.Menu>(this.ContentPage, "GenericContentPointer_CP");
      userMenu.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;

      
      if (userMenu.Visible)
      {
        userMenu.menuTitle = ResourceManager.GetString("txtAccessories");
        userMenu.LoadData(setMenu(menu));
      }
      else
      {
        userMenu.menuCMSControlName = "GenericContentPointer_CP";
      }

      if (this.IsPostBack)
      {
        //start with hiding Results
        PanelAccessoriesResult.Visible = false;
        try
        {
          SelectedModelFamilyUrl = DropDownListCategory.SelectedValue;
          int.TryParse(Request.Form["trim_id"], out SelectedTrimID);
          //SelectedProvinceCode = DropDownListProvince.SelectedValue;
        }
        catch (Exception)
        {
          SelectedModelFamilyUrl = string.Empty;
          SelectedTrimID = 0;
          //SelectedProvinceCode = string.Empty;
        }
      }
      else
      {
        //Select a Category DropDown
        PopulateCategoryDropDown();
      }
      //Populate Model Drop down
      PopulateModelDropDown();

      PartsAndAccessory partAccessories = ShowHideResultsPanel();
      repAccessories.DataSource = partAccessories.Category;
      repAccessories.DataBind();

      if (PanelAccessoriesResult.Visible)
      {
        HondaPE.Service.Model.PETrimService ts = new HondaPE.Service.Model.PETrimService();
        Trim oTrim = ts.GetTrimByTrimID(targetID: this.TargetID, TrimID: SelectedTrimID, language: PageLanguage.GetCultureStringValue(), ModelID: -1);
        ModelCategory oCategory = null;
        HondaCA.Service.Model.HondaModelService ms = new HondaCA.Service.Model.HondaModelService();
        HondaCA.Entity.EntityCollection<ModelCategory> oCategories = ms.GetModelCategory(PageLanguage, TargetID);
        try
        {
          oCategory = oCategories.FirstOrDefault(x => x.CategoryID == oTrim.BaseModelCategoryID);
        }
        catch (Exception)
        {
        }

        LiteralImage.Text = string.Empty;

        if (oTrim != null)
        {
          decimal MSRP;
          if (!this.IsQuebecPricing)
          {
            MSRP = oTrim.MSRP;
          }
          else
          {
            MSRP = oTrim.MSRP + oTrim.FreightPDI;
          }
          oTrim = oTrim.GetPriceAdjustedTrim(TargetID, PageLanguage);
          LiteralModelName.Text = oTrim.TrimName;
          LiteralMsrpValue.Text = string.Format(ResourceManager.GetString("valMsrp"), "$", MSRP);
          DiscountFinalPrice.Text = string.Format(ResourceManager.GetString("valMsrp"), "$", oTrim.GetFinalPrice(this.IsQuebecPricing));
          DiscountDiscount.Text = string.Format(ResourceManager.GetString("valMSRP4"), oTrim.GetDiscount(this.IsQuebecPricing));
          DiscountMsrp.Text = string.Format(ResourceManager.GetString("valMSRP4"), MSRP);
          string IamgePath = Marine.Web.Code.CommonFunctions.BuildModelImageUrl(oTrim);
          if (File.Exists(Server.MapPath(IamgePath)))
          {
            LiteralImage.Text = string.Format(@"<img src=""{0}?Crop=auto&maxWidth={1}&maxHeight={2}"" alt"""" />", IamgePath, 161, 240);
          }

          //HondaCA.Service.Model.HondaModelService ms = new HondaModelService();
          //HondaCA.Entity.EntityCollection<ModelCategory> oCategories = ms.GetModelCategoryByModelFamilyUrl(this.TargetID,SelectedModelFamilyUrl,PageLanguage);
          //ModelCategory oCateory = null;
          HondaCA.Entity.Model.Colour oColour = null;

          if (ts.IsComingSoonWithColor(oTrim, oColour))
          {
            phComingSoon.Visible = true;
            phHasNoDiscount.Visible = false;
          }
          else
          {
            if (oTrim.HasDiscount())
            {
              phHasDiscount.Visible = true;
            }
            else
            {
              phHasNoDiscount.Visible = true;
            }
            phComingSoon.Visible = false;
          }

          try
          {
            //oCateory = oCategories[0];
            LiteralSeriesName.Text = oCategory != null ? oCategory.CategoryName : DropDownListCategory.Text.ToLower().EndsWith("_fr") == true ? DropDownListCategory.Text.Substring(0, DropDownListCategory.Text.ToLower().IndexOf("_fr")) : DropDownListCategory.Text; //oCateory.CategoryName;
            ucToolTipSeries.Visible = true;
            LiteralTooltipSeriesHeader.Text = oCategory.CategoryName;
            LiteralTooltipSeriesDescription.Text = Marine.Web.Code.ModelHelper.ProcessModelCategoryTooltip(oCategory.CategoryDescription);

          }
          catch (Exception)
          {
          }
        }
      }
    }


    //Category Dropdown
    protected void PopulateCategoryDropDown()
    {
      HondaModelService ms = new HondaModelService();
      HondaCA.Entity.EntityCollection<ModelCategory> oModelCategories = ms.GetModelCategory(PageLanguage, this.TargetID);

      if (oModelCategories != null)
      {
        if (oModelCategories.Count > 0)
        {
          foreach (ModelCategory oModelCategory in oModelCategories)
          {
            DropDownListCategory.Items.Add(new ListItem(Marine.Web.Code.CommonFunctions.ucWords(oModelCategory.CategoryName), oModelCategory.CategoryUrl));
          }
          DropDownListCategory.Items.Insert(0, new ListItem(ResourceManager.GetString("txtSelectOne"), ""));
        }
      }
    }

    //Model Dropdown
    protected void PopulateModelDropDown()
    {
      StringBuilder sb = new StringBuilder();
      sb.AppendFormat(@"<select name=""trim_id"" onchange=""form.submit()"">");
      sb.AppendFormat(@"<option value="""">{0}</option>", ResourceManager.GetString("txtSelectOne"), "");

      if (DropDownListCategory.SelectedIndex > 0)
      {
        string ModelCategoryUrl = DropDownListCategory.SelectedValue;
        if (!string.IsNullOrEmpty(ModelCategoryUrl))
        {
          HondaModelService ms = new HondaModelService();
          try
          {
            HondaCA.Entity.EntityCollection<HondaModel> oModels = ms.GetModelsByCategory(this.TargetID, PageLanguage, ModelCategoryUrl);
            if (oModels != null)
            {
              try
              {
                Marine.Service.Model.MarineTrimService trimService = new Marine.Service.Model.MarineTrimService();
                HondaCA.Entity.EntityCollection<Trim> oTrims = trimService.GetTrimWithBasePrice(ModelCategoryUrl, this.TargetID, PageLanguage.GetCultureStringValue());
                if (oTrims != null)
                {
                  if (oTrims.Count > 0)
                  {
                    foreach (Trim oTrim in oTrims)
                    {
                      sb.AppendFormat(@"<option value=""{0}""{2}>{1}</option>", oTrim.TrimID.ToString(), oTrim.TrimName, (oTrim.TrimID == SelectedTrimID ? " selected='selected'" : ""));
                    }
                  }
                }
              }
              catch (Exception)
              {
              }
            }
          }
          catch (Exception)
          {
          }
        }
      }
      sb.AppendFormat("</select>");
      LiteralModelDropDown.Text = sb.ToString();
    }

    protected PartsAndAccessory ShowHideResultsPanel()
    {
      PartsAndAccessory oPartsAndAccessories = new PartsAndAccessory();
      if (HiddenFieldSelectedCategory.Value == DropDownListCategory.SelectedValue)
      {
        int.TryParse(Request.Form["trim_id"], out SelectedTrimID);
      }
      else
      {
        SelectedTrimID = 0;
        HiddenFieldSelectedCategory.Value = DropDownListCategory.SelectedValue;
      }

      if (!string.IsNullOrEmpty(SelectedModelFamilyUrl) && SelectedTrimID > 0)
      {
        PartsAndAccessoriesService paService = new PartsAndAccessoriesService();
        oPartsAndAccessories = paService.GetPartsAndAccessories(SelectedTrimID, this.TargetID, (HondaAccessories.Common.Language)PageLanguage, System.Configuration.ConfigurationManager.AppSettings["ProductLineCode"], ImageFolder);
      }
      return oPartsAndAccessories;
    }

    protected void repAccessories_ItemDataBound(Object sender, RepeaterItemEventArgs e)
    {
      Category category = (Category)e.Item.DataItem;
      if (category != null)
      {
        Literal litCategoryName = e.Item.FindControl("litCategoryName") as Literal;
        Literal LiteralAccessoryItems = e.Item.FindControl("LiteralAccessoryItems") as Literal;
        Literal LiteralAccessoryToolTips = e.Item.FindControl("LiteralAccessoryToolTips") as Literal;

        if (category.Accessories.Count > 0)
        {
          PanelAccessoriesResult.Visible = true;
          litCategoryName.Text = string.Format(@"<div class=""bar""><h2>{0}</h2></div>", category.CategoryName);
          IList<Accessory> oAccessories = category.Accessories;
          StringBuilder sbAccItems = new StringBuilder();
          StringBuilder sbAccToolTips = new StringBuilder();
          foreach (Accessory oAccessory in oAccessories)
          {
            string AccPrice = string.Empty;
            try
            {
              if (oAccessory.Price > 0)
              {
                AccPrice = string.Format("{0:C2}", oAccessory.Price);
              }
              else
              {
                AccPrice = ResourceManager.GetString("txtComingSoon");
              }
            }
            catch (Exception)
            {
              AccPrice = string.Empty;
            }
            string AccName = CommonFunctions.StringToHtmlEntities(oAccessory.Name);
            string AccDesc = CommonFunctions.StringToHtmlEntities(oAccessory.DescriptionBody);

            sbAccItems.AppendFormat(@"<li class=""fc"">
                                                    <a class=""fl item""
            					                        property=""act:tooltip""
                                                        data-position-my=""left center""
                                                        data-position-at=""right center""
                                                        data-position-offset=""20px 0""
            					                        data-position-collision=""fit""
            					                        data-tooltip-class=""tooltip-right""
            					                        data-tooltip-content-target=""#accessories-item{2}"">{0}{3}
                                                    </a>
            					                    <span class=""price"">{1}</span>
            				                        </li>"
                                , AccName
                                , string.Empty // AccPrice
                                , oAccessory.ItemID
                                , string.IsNullOrWhiteSpace(oAccessory.ItemNumber) ? "" : string.Format(@"<br /><q class=""part-number"">{0}: {1}</q>", ResourceManager.GetString("txtPartNumberLabel"), oAccessory.ItemNumber)
                                ); // <q class=""db part-number"">{3}</q>

            string image = string.Empty;

            if (!string.IsNullOrEmpty(oAccessory.Assets.Thumbnail))
            {
              image = ImageFolder + oAccessory.Assets.Thumbnail;
              if (!File.Exists(Server.MapPath(image)))
              {
                image = string.Format(@"<div class=""image""><img src=""{0}?Crop=auto&Width=250&Height=199"", alt"""" /></div>", string.Format(@"{0}/{1}_{2}",
                                      ConfigurationManager.AppSettings["AssetPathAccessories"],
                                      PageLanguage.GetCultureStringValue().Substring(0, 2),
                                      "NA_thumbnail.png"));
              }
              else
              {
                image = string.Format(@"<div class=""image""><img src=""{0}{1}?Crop=auto&Width=250&Height=199"", alt"""" /></div>", ImageFolder, oAccessory.Assets.Thumbnail);
              }
            }
            else
            {
              image = string.Format(@"<div class=""image""><img src=""{0}?Crop=auto&Width=250&Height=199"", alt"""" /></div>", string.Format(@"{0}/{1}_{2}",
                                        ConfigurationManager.AppSettings["AssetPathAccessories"],
                                        PageLanguage.GetCultureStringValue().Substring(0, 2),
                                        "NA_thumbnail.png"));
            }
            sbAccToolTips.AppendFormat(
              @"<div id=""accessories-item{0}"" class=""dn"">
            	                                        {1}
            	                                        <div class=""tooltip_heading""><strong>{2}</strong></div>
            	                                        <p>{5}</p><br />
                                                      <p>{4}</p>
                                                    </div>",
              oAccessory.ItemID, image, AccName, string.Empty /* AccPrice */,
              ResourceManager.GetString("txtSeeDealerForPricing"), AccDesc); //  <div class=""tooltip_price""><span>{4}</span></div>

          }

          LiteralAccessoryItems.Text = sbAccItems.ToString();
          LiteralAccessoryToolTips.Text = sbAccToolTips.ToString();
        }
      }
    }
  }
}