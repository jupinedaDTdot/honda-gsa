﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils.CMS;

namespace Marine.Web.ContentPages
{
    public partial class powerupevent : ContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            Tout tout = CMSHelper.getCmsElementFromCmsPage<Tout>(this.ContentPage, "GenericContent_Tout");
            if (tout != null)
            {
                TopTout.Visible = tout.ContentElement.Display ? true : this.IsStaging ? true : false;
                if (TopTout.Visible) TopTout.Text = getToutImage(tout);
            }

            FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH");
            if (ffh != null)
            {
                MiddleContent.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
                if (MiddleContent.Visible) MiddleContent.Text = ffh.Html;
            }
        }
    }
}