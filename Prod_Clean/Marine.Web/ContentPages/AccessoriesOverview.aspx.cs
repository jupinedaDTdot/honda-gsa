﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils.CMS;

namespace Marine.Web.ContentPages
{
    public partial class AccessoriesOverview : ContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);
            System.Web.UI.HtmlControls.HtmlControl ctrlBody = (System.Web.UI.HtmlControls.HtmlControl)this.Page.Master.Master.Master.FindControl("body");

            if (ctrlBody != null)
            {
                ctrlBody.Attributes["class"] = ctrlBody.Attributes["class"] == null ? "page-accessories" : " page-accessories";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            Tout tout = CMSHelper.getCmsElementFromCmsPage<Tout>(this.ContentPage, "GenericContent_Tout");
            plhMainTout.Visible = tout.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plhMainTout.Visible)
            {
              plhMainTout.Controls.Add(new LiteralControl(GetToutRotatorHtml(MainUrl, "GenericContent_Tout", 655, 180)));
            }

            FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH");
            plhMainFFH.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plhMainFFH.Visible)
            {
                plhMainFFH.Controls.Add(new LiteralControl(ffh.Html));
            }

            FreeFormHtml ffh2 = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH2");
            plhMainFFH2.Visible = ffh2.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plhMainFFH2.Visible)
            {
                plhMainFFH2.Controls.Add(new LiteralControl(ffh2.Html));
            }

            userMenu.menuTitle = ResourceManager.GetString("txtAccessories");                    
            Navantis.Honda.CMS.Client.Elements.Menu menu = CMSHelper.getElementThroughContentPointerFromCmsPage<Navantis.Honda.CMS.Client.Elements.Menu>(this.ContentPage, "GenericContentPointer_CP");
            if (menu != null)
            {
                userMenu.Visible = menu.ContentElement.Display ? true : this.IsStaging ? true : false;
                userMenu.menuCMSControlName = "GenericContentPointer_CP";
                if (userMenu.Visible)
                    userMenu.LoadData(setMenu(menu));                
            }                    
        }
    }
}