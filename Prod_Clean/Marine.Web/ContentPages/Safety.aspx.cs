﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils.CMS;

namespace Marine.Web.ContentPages
{
    public partial class Safety : ContentBasePage
    {
        protected bool isStaging;
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();
            isStaging = this.IsStaging;
            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-safety");
                this.IsMasterBodyClassUpdate = true;
            }

            Tout tout = CMSHelper.getCmsElementFromCmsPage<Tout>(this.ContentPage, "GenericContent_Tout");
            plhMainTout.Visible = tout.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plhMainTout.Visible)
            {
                plhMainTout.Controls.Add(new LiteralControl(getToutImage(tout)));
            }


            //FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH");
            //plhMiddleTopContent.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            //if (plhMiddleTopContent.Visible)
            //{
            //    plhMiddleTopContent.Controls.Add(new LiteralControl(ffh.Html));
            //}

            //IElementContent element = this.ContentPage.Elements["GenericContentPointer_CP"].DeserializeElementObject();
            //GenericLink gl = CMSHelper.getElementThroughContentPointerFromCmsPage<GenericLink>(this.ContentPage, "GenericContentPointer_CP");
            //DownloadButton1.isVisible = element.ContentElement.Display ? true : IsStaging ? true : false;
            //liDownload.Visible = element.ContentElement.Display ? true : IsStaging ? true : false;
            //if (DownloadButton1.isVisible)
            //{
            //    DownloadButton1.isVisible = false;
            //    DownloadButton1.linkTitle = ResourceManager.GetString("Downloads");
            //    DownloadButton1.LoadData(gl);
            //}    

            IElementContent element = this.ContentPage.Elements["GenericContent_FFH"].DeserializeElementObject() as IElementContent;
            plhMiddleTopContent.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;

            if (plhMiddleTopContent.Visible == true)
                plhMiddleTopContent.Controls.Add(new LiteralControl(setFreeFormHtml("GenericContent_FFH")));


            element = this.ContentPage.Elements["GenericContentPointer_CP"].DeserializeElementObject() as IElementContent;
            bool isVisibleMenu = element.ContentElement.Display ? true : this.IsStaging ? true : false;
            //isVisibleMenu = false;
            if (isVisibleMenu)
            {
                userMenu.menuTitle = ResourceManager.GetString("Safety");
                userMenu.LoadData(setMenu(getMenuFromContentPointer("GenericContentPointer_CP")));
            }
            else
            {
                userMenu.menuCMSControlName = "GenericContentPointer_CP";
                userMenu.Visible = false;
            }
        }
    }
}