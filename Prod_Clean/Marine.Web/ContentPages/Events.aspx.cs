﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.Service.Common;
using HondaCA.Entity;
using HondaCA.Entity.Common;
using HondaCA.Common;
using Marine.Web.Code;
using System.IO;

using HondaCA.WebUtils.CMS;
using System.Configuration;

namespace Marine.Web.ContentPages
{
    public partial class Events : ContentBasePage
    {
        int rowCounter = 0;
        int abtCounter;
        int totItems = 0;
        int pageindex;
        const int rowNos = 3;
        bool IsPrint = false;
        

        protected override void OnInit(EventArgs e)
        {
            if ((Request.QueryString["Print"] ?? string.Empty) == "1")
            {
                IsPrint = true;
            }

            if(IsPrint)
                this.MainUrl = Request.QueryString["BaseUrl"] ?? HttpContext.Current.Request.RawUrl.ToLower();
            else 
                this.MainUrl = HttpContext.Current.Request.RawUrl.ToLower();
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;

            if (body != null)
            {
                string cssClass = string.Empty;
                cssClass = "page-news-events";
                body.Attributes.Add("class", cssClass);
                this.IsMasterBodyClassUpdate = true;
            }



            if (IsPrint)
            {
                PrintEvents();
            }
            else
            {
                setMenu();
                //setEvents();
            }

            if (!IsPostBack)
            {
                setDropDowns();
            }
            else
            {
                rowCounter = 0;
            }
        }

        protected void PrintEvents()
        {
            string url = string.Format(Request.RawUrl);
            url = string.Format("/ContentPages/EventsPrint.aspx?Lang={0}&BaseUrl={1}&sDivision={2}&sProvince={3}&eventElement={4}", PageLanguage.GetCultureStringValue(), Request.RawUrl ?? string.Empty, DropDownListDivision.SelectedValue, DropDownListProvince.SelectedValue ?? string.Empty, DropDownListEventType.SelectedValue ?? string.Empty);

            ExpertPdf.HtmlToPdf.PdfConverter pdfConverter = new ExpertPdf.HtmlToPdf.PdfConverter();

            string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
            //string headerAndFooterHtmlUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/footer.htm";

            //enable footer
            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.PdfPageSize = ExpertPdf.HtmlToPdf.PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.TopMargin = 20;
            // set the footer height in points
            pdfConverter.PdfFooterOptions.FooterHeight = 67;
            pdfConverter.PdfFooterOptions.DrawFooterLine = false;
            //pdfConverter.PdfFooterOptions.HtmlToPdfArea = new HtmlToPdfArea(headerAndFooterHtmlUrl);

            Navantis.Honda.CMS.Core.Common.HtmlToPDF htmltopdf = new Navantis.Honda.CMS.Core.Common.HtmlToPDF();
            htmltopdf.Converter = pdfConverter;

            htmltopdf.WritePDF(this.Response, url, string.Format("Events_{0}.pdf", PageLanguage.GetCultureStringValue().Substring(0, 2)));
            Response.Write(url);
        }

        //private void setEvents()
        //{
        //    Event eventElement = CMSHelper.getCmsElementFromRelatedPage<Event>(this.RelatedPages, "Event");
        //    string eventTypeValue = DropDownListEventType.SelectedValue;
        //    //if (eventTypeValue == "All") eventTypeValue = string.Empty;
        //    string filterbyEndDate = ConfigurationManager.AppSettings["Event_FilterByEndDate"] ?? "true";
        //    eventElement = EventsHelper.FilterandSortEventsForDate(eventElement, filterbyEndDate.ToLower() == "true" ? true : false);
        //    eventElement = EventsHelper.FilterEvents(DropDownListDivision.SelectedValue, DropDownListProvince.SelectedValue, eventTypeValue, eventElement);
        //    setEvents(eventElement);
        //}

        //protected void setEvents(Navantis.Honda.CMS.Client.Elements.Event content)
        //{
        //    if (content != null)
        //    {
        //        try
        //        {
        //            if (content.ContentElement.Display)
        //            {
        //                if (content.Items.Count > 0)
        //                {
        //                    divNoEvents.Visible = false;
        //                    ListViewEvents.Visible = true;
        //                    totItems = content.Items.Count;
        //                    ListViewEvents.DataSource = content.Items;
        //                    ListViewEvents.DataBind();

        //                    //DataPager1.PageSize = DataPager1.PageSize;
        //                    if (content.Items.Count <= DataPager1.PageSize)
        //                    {
        //                        divDataPager.Visible = false;
        //                    }
        //                    else
        //                    {
        //                        divDataPager.Visible = true;
        //                    }


        //                }
        //                else
        //                {
        //                    ListViewEvents.Visible = false;
        //                    divDataPager.Visible = false;
        //                    divNoEvents.Visible = true;
        //                    LitNoEvents.Text = ResourceManager.GetString("NoEventsMessage");
        //                }

        //            }

        //        }
        //        catch (Exception ex) { }

        //    }
        //    else
        //    {
        //        LitNoEvents.Visible = true;
        //        LitNoEvents.Text = ResourceManager.GetString("NoEventsMessage");
        //    }
        //}

        private void setMenu()
        {

            Navantis.Honda.CMS.Client.Elements.Menu menuElement = CMSHelper.getCmsElementFromRelatedPage<Navantis.Honda.CMS.Client.Elements.Menu>(this.RelatedPages, "GenericContent_Menu");

            //PageSetting pg = CMSHelper.getCmsElementFromCmsPage<PageSetting>(this.ContentPage, "PageSetting");
            if (menuElement != null)
            {
                if (menuElement.ContentElement.Display)
                {
                    leftsidemenu1.menuTitle = ResourceManager.GetString("News_events");
                    leftsidemenu1.LoadData(setMenu(menuElement));
                }
                else
                {
                    leftsidemenu1.menuCMSControlName = "GenericContent_Menu";
                    leftsidemenu1.Visible = false;
                }

            }


        }

        private void setDropDowns()
        {
            HondaCA.Service.Common.RegionService rs = new HondaCA.Service.Common.RegionService();
            EntityCollection<Region> regionList = new EntityCollection<Region>();
            IList<string> eventDivisionsList = CommonFunctions.getEventDivisions(this.PageLanguage);
            IList<string> eventTypesList = CommonFunctions.getEventTypes(this.PageLanguage);

            regionList = rs.GetAllRegions(PageLanguage.GetCultureStringValue());
            if (regionList.Count > 0)
            {
                foreach (Region region in regionList)
                {
                    DropDownListProvince.Items.Add(new ListItem(region.Name ?? string.Empty, region.Code ?? string.Empty));
                }

                DropDownListProvince.Items.Insert(0, new ListItem(ResourceManager.GetString("txtSelectOne"), string.Empty));
            }

            if (eventDivisionsList.Count > 0)
            {
                foreach (string str in eventDivisionsList)
                {
                    DropDownListDivision.Items.Add(new ListItem(str ?? string.Empty, str ?? string.Empty));
                }
                DropDownListDivision.Items.Insert(0, new ListItem(ResourceManager.GetString("txtSelectOne"), string.Empty));
            }

            if (eventTypesList.Count > 0)
            {
               // DropDownListEventType.Items.Add(new ListItem(ResourceManager.GetString("All"), "All"));
                foreach (string str in eventTypesList)
                {
                    DropDownListEventType.Items.Add(new ListItem(str ?? string.Empty, str ?? string.Empty));
                }
                DropDownListEventType.Items.Insert(0, new ListItem(ResourceManager.GetString("txtSelectOne"), string.Empty));
            }


        }

        protected void DropDownListDivision_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            //setEvents();
            DataPager1.SetPageProperties(0,DataPager1.MaximumRows, true); 
        }

        protected void DropDownListEventType_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            //setEvents();
            DataPager1.SetPageProperties(0, DataPager1.MaximumRows, true); 
        }

        protected void DropDownListProvince_OnSelectedIndexChanged(Object sender, EventArgs e)
        {

            //setEvents();
            DataPager1.SetPageProperties(0, DataPager1.MaximumRows, true); 
        }

        protected void hrefDownloadEvents_Click(Object sender, EventArgs e)
        {
            IsPrint = true;
            PrintEvents();

        }

        protected void DataPager1_Init(object sender, EventArgs e)
        {
            if ((Request.QueryString["Print"] ?? string.Empty ) == "1")
            {
                IsPrint = true;
            }

            if (IsPrint)
                this.MainUrl = Request.QueryString["BaseUrl"] ?? HttpContext.Current.Request.RawUrl.ToLower();
            else
                this.MainUrl = HttpContext.Current.Request.RawUrl.ToLower();

            base.OnInit(e);

            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();


            DataPager dp = (DataPager)sender;
            NextPreviousPagerField fieldPrev = (NextPreviousPagerField)dp.Fields[0];
            fieldPrev.PreviousPageText = ResourceManager.GetString("txtPrevious");
            NextPreviousPagerField fieldNext = (NextPreviousPagerField)dp.Fields[2];
            fieldNext.NextPageText = ResourceManager.GetString("txtNext");
        }

        protected void ListViewEvents_PagePropertiesChanged(object sender, EventArgs e)
        {
            this.ListViewEvents.DataBind();
        }

        protected void ListViewEvents_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            pageindex = e.StartRowIndex / e.MaximumRows;
            abtCounter = e.StartRowIndex;
        }

        protected void ListViewEvents_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                ListViewDataItem row = (ListViewDataItem)e.Item;
                EventItem item = row.DataItem as EventItem;
                if (item != null)
                {
                    if (rowCounter == 0)
                    {
                        HtmlGenericControl divContentSection = (HtmlGenericControl)e.Item.FindControl("divContentSection");
                        divContentSection.Attributes.Add("class", "content_section");
                    }
                    HtmlGenericControl divEventType = (HtmlGenericControl)e.Item.FindControl("divEventType");
                    Literal litEventType = (Literal)e.Item.FindControl("litEventType");
                    Literal litEventTitle = (Literal)e.Item.FindControl("litEventTitle");
                    HtmlGenericControl divDate = (HtmlGenericControl)e.Item.FindControl("divDate");
                    HtmlImage imgEventImage = (HtmlImage)e.Item.FindControl("imgEventImage");
                    HtmlGenericControl divEventImage = (HtmlGenericControl)e.Item.FindControl("divEventImage");
                    HtmlGenericControl divLocation = (HtmlGenericControl)e.Item.FindControl("divLocation");
                    HtmlGenericControl divHours = (HtmlGenericControl)e.Item.FindControl("divHours");
                    Literal litDate = (Literal)e.Item.FindControl("litDate");
                    Literal litLocation = (Literal)e.Item.FindControl("litLocation");
                    Literal litHours = (Literal)e.Item.FindControl("litHours");
                    HtmlGenericControl divButtons = (HtmlGenericControl)e.Item.FindControl("divButtons");
                    HtmlAnchor hrefFindOutMore = (HtmlAnchor)e.Item.FindControl("hrefFindOutMore");
                    HtmlAnchor hrefDownloadPDF = (HtmlAnchor)e.Item.FindControl("hrefDownloadPDF");

                    HtmlGenericControl paraPreviewDesc = (HtmlGenericControl)e.Item.FindControl("paraPreviewDesc");
                    Literal litPreviewDesc = (Literal)e.Item.FindControl("litPreviewDesc");
                    string sProvince = string.Empty;

                    divEventType.Visible = true;
                    divEventImage.Visible = true;
                    divDate.Visible = true;
                    divLocation.Visible = true;
                    divHours.Visible = true;
                    divButtons.Visible = false;

                    litEventType.Text = CommonFunctions.getStringListBySeperater(item.EventTypes, ", ");

                    if (string.IsNullOrEmpty(litEventType.Text))
                        divEventType.Visible = false;

                    litEventTitle.Text = item.Title ?? string.Empty;
                    if (string.IsNullOrEmpty(item.Tout))
                        divEventImage.Visible = false;
                    else
                        imgEventImage.Src = item.Tout;

                    if (item.Date == null)
                        divDate.Visible = false;
                    else
                        litDate.Text = item.Date.ToString();

                    //DateTime defaultDate = new DateTime(0001, 01, 01);


                    //if (item.StartDate != null && item.EndDate != null)
                    //{
                    //    if (item.StartDate != defaultDate && item.EndDate != defaultDate)
                    //    {

                    //    }
                    //    else 
                    //    {

                    //    }
                    //}

                    //01/01/0001

                    RegionService rs = new RegionService();
                    sProvince = rs.GetProvinceNameByCode(item.Province ?? string.Empty, PageLanguage.GetCultureStringValue());

                    if (!string.IsNullOrEmpty(item.Location) == true && !string.IsNullOrEmpty(sProvince) == true)
                    {
                        litLocation.Text = item.Location + ", " + sProvince;
                    }
                    else if (!string.IsNullOrEmpty(item.Location) == true)
                    {
                        litLocation.Text = item.Location;
                    }
                    else if (!string.IsNullOrEmpty(sProvince) == true)
                    {
                        litLocation.Text = sProvince;
                    }
                    else
                        divLocation.Visible = false;


                    if (string.IsNullOrEmpty(item.Hours))
                        divHours.Visible = false;
                    else
                        litHours.Text = item.Hours;
                    if (!string.IsNullOrEmpty(item.MoreURL))
                    {
                        hrefFindOutMore.HRef = item.MoreURL;
                        divButtons.Visible = true;
                        hrefFindOutMore.Visible = true;
                    }
                    else
                        hrefFindOutMore.Visible = false;


                    if (!string.IsNullOrEmpty(item.PDF))
                    {
                        hrefDownloadPDF.HRef = item.PDF;
                        divButtons.Visible = true;
                        hrefDownloadPDF.Visible = true;
                    }
                    else
                        hrefDownloadPDF.Visible = false;


                    string readMoreUrlLink = string.Empty;
                    if (!string.IsNullOrEmpty(item.PreviewDescription))
                    {
                        litPreviewDesc.Text = item.PreviewDescription;
                    }
                    else
                        paraPreviewDesc.Visible = false;



                    rowCounter++;
                    abtCounter++;
                }

            }
        }

       
    }
}