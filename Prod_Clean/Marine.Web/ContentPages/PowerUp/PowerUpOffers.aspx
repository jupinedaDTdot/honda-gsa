﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/_Global/master/CmsOneColMaster.master"
    CodeBehind="PowerUpOffers.aspx.cs" Inherits="Marine.Web.ContentPages.PowerUp.PowerUpOffers" %>

<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/_Global/css/sections/product-listing.css" />
    <link rel="stylesheet" href="/_Global/css/marine/sections/product-listing.css" />
    <link rel="stylesheet" href="/_Global/css/marine/sections/spring-sales.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="cap_top_default pf">
    </div>
    <div class="container pf">
        <div class="content_container spring-sales">
            <div class="content_section offers">
                <div class="banner">
                    <uc:ElementControl runat="server" ID="Element1" ElementName="GenericContent_Tout" />
                    <asp:PlaceHolder ID="PlaceHolderTout" runat="server"></asp:PlaceHolder>
                </div>
                <div>
                    <uc:ElementControl runat="server" ID="Element2" ElementName="GenericContent_FFH" />
                    <asp:Literal ID="LiteralTopText" runat="server"></asp:Literal>
                </div>
                <div class="cap_top_default_sub">
                </div>
                <div class="offers-list">
<%--                    <div class="offers-list">
                        <ul class="fc">
                            <li class="fl">
                                <div class="img-wrapper">
                                    <img style="" src="/Content/marine.honda.ca/fc5f7f81-2044-46e4-a424-3e73f7efe389/GenericContent_FFH2/BWA_CTA-Snowblowers.png">
                                </div>
                                <div class="top-content-text">
                                    <h2>
                                        Blow Winter Away</h2>
                                    <p>
                                        Ditch the hard work and uncover savings on durable and reliable Honda snowblowers
                                        starting at just $729.</p>
                                </div>
                            </li>
                            <li class="fl">
                                <div class="img-wrapper">
                                    <img style="" src="/Content/marine.honda.ca/fc5f7f81-2044-46e4-a424-3e73f7efe389/GenericContent_FFH2/BWA_CTA-Snowblowers.png">
                                </div>
                                <div class="top-content-text">
                                    <h2>
                                        Help me Choose</h2>
                                    <p>
                                        To help make the best decision, use this <a href="/help-me-choose/snowblowers">convenient
                                            tool</a> to discover the right Honda snowblower for your snow removal needs.
                                    </p>
                                </div>
                            </li>
                            <li class="fl">
                                <div class="img-wrapper">
                                    <img style="" src="/Content/marine.honda.ca/fc5f7f81-2044-46e4-a424-3e73f7efe389/GenericContent_FFH2/BWA_CTA-Snowblowers.png">
                                </div>
                                <div class="top-content-text">
                                    <h2>
                                        Help me Choose</h2>
                                    <p>
                                        To help make the best decision, use this <a href="/help-me-choose/snowblowers">convenient
                                            tool</a> to discover the right Honda snowblower for your snow removal needs.
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>--%>
                    <div>
                        <uc:ElementControl runat="server" ID="ElementControl2" ElementName="GenericContent_FFH2" />
                        <asp:Literal ID="LiteralMiddleText" runat="server"></asp:Literal>
                    </div>
                    <div id="Div1" class="product-listings" runat="server" visible="false">
                        <asp:Literal ID="LiteralModelList" runat="server" Visible="false"></asp:Literal>
                    </div>
                    <asp:Repeater ID="RepeaterCategroy" runat="server" OnItemDataBound="RepeaterCategroy_ItemDataBound"
                        Visible="true">
                        <HeaderTemplate>
                            <div class="product-listings">
                                <asp:Literal ID="LiteralSubWrapperStart" runat="server"></asp:Literal>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="h2">
                                <asp:Literal ID="LiteralCategory" runat="server"></asp:Literal></div>
                            <asp:Repeater ID="RepeaterTrims" runat="server" OnItemDataBound="RepeaterTrims_ItemDataBound">
                                <HeaderTemplate>
                                    <div class="fc product">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div id="divTrims" runat="server">
                                        <%--<asp:Literal ID="LiteralYear" runat="server"></asp:Literal>--%>
                                        <asp:Literal ID="LiteralImage" runat="server"></asp:Literal>
                                        <asp:Literal ID="LiteralModelName" runat="server"></asp:Literal>
                                        <asp:Literal ID="LiteralPrice" runat="server"></asp:Literal>
                                    </div>
                                    <div>
                                        <!-- IE QUIRK -->
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="RepeaterAtvCategory" runat="server" OnItemDataBound="RepeaterCategroy_ItemDataBound"
                        Visible="true">
                        <HeaderTemplate>
                            <div class="product-listings">
                                <asp:Literal ID="LiteralSubWrapperStart" runat="server"></asp:Literal>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="h2">
                                <asp:Literal ID="LiteralCategory" runat="server"></asp:Literal></div>
                            <asp:Repeater ID="RepeaterTrims" runat="server" OnItemDataBound="RepeaterTrims_ItemDataBound">
                                <HeaderTemplate>
                                    <div class="fc product">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div id="divTrims" runat="server">
                                        <asp:Literal ID="LiteralYear" runat="server"></asp:Literal>
                                        <asp:Literal ID="LiteralImage" runat="server"></asp:Literal>
                                        <asp:Literal ID="LiteralModelName" runat="server"></asp:Literal>
                                        <asp:Literal ID="LiteralPrice" runat="server"></asp:Literal>
                                    </div>
                                    <div>
                                        <!-- IE QUIRK -->
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="cap_bottom_default_sub">
                </div>
                <div class="fc spring-disclaimer">
                    <div>
                    <uc:ElementControl runat="server" ID="ElementControl1" ElementName="GenericContent_FFH3" />
                    </div>
                    <div>
                    <uc:ElementControl runat="server" ID="ElementControl3" ElementName="GenericContent_FFH4" />
                    </div>

                    <asp:Literal ID="LiteralBottomText" runat="server"></asp:Literal>
                </div>
                <div class="bottom_cap pf">
                </div>
            </div>
        </div>
    </div>
    <div class="cap_bottom_default pf">
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd"
    runat="server">
    <script type="text/javascript" src="/_Global/js/sections/product-listing.js"></script>
</asp:Content>
