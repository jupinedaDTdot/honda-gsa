﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using Navantis.Honda.CMS.Client.Elements;
using System.Text;
using HondaCA.Common;
using System.Web.UI.HtmlControls;
using HondaCA.WebUtils.CMS;
using Marine.Web.Code;
using HondaATV.Service.Model;

namespace Marine.Web.ContentPages.PowerUp
{
    public partial class PowerUpOffers : ContentBasePage
    {

        protected EntityCollection<Trim> ExcludeModelsMC = new EntityCollection<Trim>();
        protected EntityCollection<Trim> ExcludeModelsATV = new EntityCollection<Trim>();
        protected string sCategoryUrl = string.Empty;
        protected int ItemsPerRow = 5;
        protected bool IsAtv = false;
        protected bool IsGetAway = false;
        protected bool ExcludeItems = true;
        public string language = string.Empty;
        private string rooturl = string.Empty;

        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);

            rooturl = Request.QueryString["RootUrl"] ?? string.Empty;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            language = this.PageLanguage == Language.English ? "en" : "fr";

            string cssClass = "page-product-listing";
            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", cssClass);
                this.IsMasterBodyClassUpdate = true;
            }

            //Top Tout
            Tout tout = CMSHelper.getCmsElementFromCmsPage<Tout>(this.ContentPage, "GenericContent_Tout");
            PlaceHolderTout.Visible = tout.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (PlaceHolderTout.Visible)
            {
                PlaceHolderTout.Controls.Add(new LiteralControl(this.getToutImage(tout)));
            }

            //Top Text
            FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH");
            LiteralTopText.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (LiteralTopText.Visible)
            {
                LiteralTopText.Text = ffh.Html ?? string.Empty;
            }

            //Middle Text
            ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH2");
            LiteralMiddleText.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (LiteralMiddleText.Visible)
            {
                LiteralMiddleText.Text = ffh.Html ?? string.Empty;
            }

            //Bottom Text
            ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, (this.IsQuebecPricing) ? "GenericContent_FFH4" : "GenericContent_FFH3");
            LiteralBottomText.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (LiteralBottomText.Visible)
            {
                LiteralBottomText.Text = ffh.Html ?? string.Empty;
            }

            //MC List
            //HondaModelService service = new HondaModelService();
            //EntityCollection<ModelCategory> listModelCat = service.GetModelCategory(this.PageLanguage, this.TargetID);

            HondaCA.Service.Model.HondaModelService hm = new HondaCA.Service.Model.HondaModelService();

            //hmList = hm.GetModelFamily(PageLanguage);
            EntityCollection<ModelCategory> listModelCat = hm.GetModelCategory(PageLanguage, this.TargetID);

            if (listModelCat != null && listModelCat.Count > 0)
            {
                RepeaterCategroy.DataSource = listModelCat;
                RepeaterCategroy.DataBind();
            }
            else
            {
                RepeaterCategroy.Visible = false;
            }
            //LiteralModelList.Text = getProductDetailForAllCategory();

            //if (rooturl != "/shiftintored" && rooturl != "/passezaurouge")
            //{
            //    //ATV
            //    IsAtv = true;
            //    listModelCat = null;
            //    //HondaMC.Service.Model.ATVModelService atvms = new HondaMC.Service.Model.ATVModelService();
            //    //listModelCat = atvms.SelectCategoryByModelFamilyName(this.TargetID, CommonFunctions.ATVFamilyName, this.PageLanguage);

            //    //HondaCA.Service.Model.HondaModelService hm = new HondaCA.Service.Model.HondaModelService();

            //    //hmList = hm.GetModelFamily(PageLanguage);
            //    listModelCat = hm.GetModelCategory(PageLanguage, this.TargetID);

            //    if (listModelCat != null && listModelCat.Count > 0)
            //    {
            //        RepeaterAtvCategory.DataSource = listModelCat;
            //        RepeaterAtvCategory.DataBind();
            //    }
            //    else
            //    {
            //        RepeaterCategroy.Visible = false;
            //    }
            //}

            //if (rooturl.ToLower().Contains("/everydaygetaway") || rooturl.ToLower().Contains("/evadezvous"))
            //{
            //    IsGetAway = true;
            //}
        }

        public new string getToutImage(Tout tout)
        {
            StringBuilder sb = new StringBuilder();
            if (tout != null && tout.Items != null)
            {
                if (tout.ContentElement.Display)
                {
                    if (string.IsNullOrEmpty(tout.Items[0].URL))
                        sb.AppendFormat(@"<img src=""{0}"" alt="""" />", tout.Items[0].PathReference);
                    else
                        sb.AppendFormat(@"<a href=""{0}""><img src=""{1}"" alt="""" /></a>", tout.Items[0].URL, tout.Items[0].PathReference);
                }
            }
            return sb.ToString();
        }

        protected void RepeaterCategroy_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Literal LiteralCategory = e.Item.FindControl("LiteralCategory") as Literal;
                Repeater RepeaterTrims = e.Item.FindControl("RepeaterTrims") as Repeater;
                ModelCategory oCategory = e.Item.DataItem as ModelCategory;
                sCategoryUrl = oCategory.CategoryUrl;
                
                //LiteralCategory.Text = string.Format(@"<a name=""{2}{0}""><h2>{1}</h2></a>", oCategory.ExportKey, (!IsAtv && oCategory.CategoryName.ToLower() == "sport" ? "Sport and Standard" : oCategory.CategoryName), IsAtv ? "atv" : "");

                LiteralCategory.Text = string.Format(@"{0}<a name=""{1}""></a><div class=""h1"">{2}</div><p>{3}</p>", "", oCategory.ExportKey, oCategory.CategoryName, oCategory.CategoryDescription.Substring(0, oCategory.CategoryDescription.IndexOf("<"))); 


                //Get trims for the model
                List<Trim> listTrims = new List<Trim>();

                Marine.Service.Model.MarineTrimService trimServiceMapper = new Marine.Service.Model.MarineTrimService();
                listTrims = trimServiceMapper.GetTrimWithBasePrice(sCategoryUrl, this.TargetID, this.PageLanguage.GetCultureStringValue());

                if (listTrims != null && listTrims.Count > 0)
                {
                    listTrims = listTrims.ToList<Trim>();
                    // remove all models without discounts
                    listTrims.RemoveAll(x => x.PriceDiscountAmount <= 0);
                }

                if (listTrims != null && listTrims.Count > 0)
                {
                    RepeaterTrims.DataSource = listTrims;
                    RepeaterTrims.DataBind();
                }
            }
        }

        protected void RepeaterTrims_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HtmlGenericControl divTrims = e.Item.FindControl("divTrims") as HtmlGenericControl;
                //Literal LiteralYear = e.Item.FindControl("LiteralYear") as Literal;
                Literal LiteralImage = e.Item.FindControl("LiteralImage") as Literal;
                Literal LiteralModelName = e.Item.FindControl("LiteralModelName") as Literal;
                Literal LiteralPrice = e.Item.FindControl("LiteralPrice") as Literal;
                Trim oTrim = e.Item.DataItem as Trim;

                if (e.Item.ItemIndex == 0 || e.Item.ItemIndex % ItemsPerRow == 0)
                    divTrims.Attributes.Add("class", "fl first product-item");
                else
                    divTrims.Attributes.Add("class", "fl product-item");
                string tooltipURL = string.Empty;
                //if (IsAtv)
                //{
                //    tooltipURL = "/atvtooltips/" + sCategoryUrl + "/" + oTrim.TrimID + "/" + language + "/compare";
                //}
                //else
                //{

                //tooltips/portables/BF8/English/0
                // Hardcode discount
                //oTrim.PriceDiscountAmount = 1000; 


                tooltipURL = "/tooltips/" + sCategoryUrl + "/" + oTrim.TrimUrl + "/" + this.PageLanguage.ToString() + "/" + oTrim.PriceDiscountAmount.ToString();
                
                  ///  tooltipURL = "/tooltips/" + sCategoryUrl + "/" + oTrim.TrimID + "/" + language + "/0";
                //}
                string trimURL = string.Empty;
                //trimURL = CommonFunctions.getModelUrl(sCategoryUrl, oTrim.ModelYearYear, oTrim.TrimUrl, TargetID);
                trimURL = CommonFunctions.GetMainUrl(sCategoryUrl, oTrim.TrimUrl);

                //LiteralYear.Text = string.Format(@"<div class=""fnat year"">{0}</div>", oTrim.ModelYearYear.ToString());
                LiteralImage.Text = string.Format(@"<div class=""product-image"" property=""act:popup"" data-msrp=""{0}"" data-id=""{1}"" data-position=""{2} top"" data-popup-modal=""false"" data-popup-class=""toolbox-top-alt {3}"" data-popup-content-url=""{4}"" data-popup-handler=""rollover"">
                                                        <a href=""{5}"" rel=""nofollow""><img src=""{6}?Crop=auto&width={7}&height={8}"" alt="""" /></a>
                                                    </div>"
                                                    , oTrim.MSRP
                                                    , oTrim.TrimExportKey
                                                    , e.Item.ItemIndex % ItemsPerRow > 2 ? "left" : "right"
                                                    , e.Item.ItemIndex % ItemsPerRow > 2 ? "toolbox-right" : "toolbox-left"
                                                    , tooltipURL
                                                    , trimURL
                                                    , IsAtv ? (Request.IsSecureConnection ? "https://" : "http://") + Request.Url.Host.ToLower().Replace("motorcycle", "atv") + BuildModelImageUrlAtv(oTrim) : CommonFunctions.BuildModelImageUrl(oTrim)
                                                    , 90
                                                    , 125);
                LiteralModelName.Text = string.Format(@"<h3 class=""fnat price-heading""><a href=""{0}"">{1}</a></h3>", trimURL, oTrim.TrimName);
                LiteralPrice.Text = getPrice(oTrim);
            }
        }

        protected string getProductDetailForAllCategory()
        {
            StringBuilder sb = new StringBuilder();
            //HondaModelService service = new HondaModelService();
            //EntityCollection<ModelCategory> listModelCat = service.GetModelCategory(this.PageLanguage, this.TargetID);

            HondaCA.Service.Model.HondaModelService hm = new HondaCA.Service.Model.HondaModelService();
            EntityCollection<ModelCategory> listModelCat = hm.GetModelCategory(PageLanguage, this.TargetID);

            //sb.AppendFormat(@"<div class=""content_section primary_section"">");
            int cnt = 0;
            foreach (ModelCategory cat in listModelCat)
            {
                if (cnt != 0) sb.AppendFormat(@"<div class=""sub_section"">");

                sb.AppendFormat(@"<a name=""{0}""><div class=""h2"">{1}</div></a>", cat.ExportKey, cat.CategoryName);
                
                //if (!string.IsNullOrEmpty(cat.CategoryDescription)) sb.AppendFormat("<p>{0}</p>", cat.CategoryDescription);
                sb.AppendFormat(getCategorywiseProductDetail(cat.CategoryUrl));

                if (cnt != 0) sb.AppendFormat("</div>");

                cnt++;
            }
            //sb.AppendFormat("</div>");

            return sb.ToString();
        }

        protected string getCategorywiseProductDetail(string sCategoryUrl)
        {
            StringBuilder sb = new StringBuilder();

            List<Trim> listTrims = new List<Trim>();
            Marine.Service.Model.MarineTrimService trimServiceMapper = new Marine.Service.Model.MarineTrimService();
            listTrims = trimServiceMapper.GetTrimWithBasePrice(sCategoryUrl, this.TargetID, this.PageLanguage.GetCultureStringValue());

            if (listTrims != null && listTrims.Count > 0)
            {
                listTrims = listTrims.ToList<Trim>();
                // remove all models without discounts
                listTrims.RemoveAll(x => x.PriceDiscountAmount <= 0);
            }


            if (listTrims.Count > 0)
            {
                sb.Append(string.Format(@"<div class=""fc product"">"));
                sb.Append(getTrimsList(listTrims, sCategoryUrl));
                sb.Append(string.Format(@"</div>"));
            }
            return sb.ToString();
        }

        protected string getTrimsList(List<Trim> oTrims, string sCategoryUrl)
        {
            StringBuilder sb = new StringBuilder();
            int firstElement = 0; int elementIndex = 0, numberPerRow = 5;

            foreach (Trim oTrim in oTrims)
            {
                string divclass = firstElement == 0 ? "fl first product-item" : "fl product-item";
                string tooltipURL = string.Empty;
                //if (IsAtv)
                //{
                //    tooltipURL = (Request.IsSecureConnection ? "https://" : "http://") + Request.Url.Host.ToLower().Replace("motorcycle", "atv") + "/tooltips/" + sCategoryUrl + "/" + oTrim.TrimID + "/?compare=0";// +language;
                //}
                //else
                //{
                    //tooltipURL = "/tooltips/" + sCategoryUrl + "/" + oTrim.TrimID + "/?compare=0";// +language;
                    tooltipURL = "/tooltips/" + sCategoryUrl + "/" + oTrim.TrimUrl + "/" + this.PageLanguage.ToString() + "/" + oTrim.PriceDiscountAmount.ToString();

                //}

                string trimURL = string.Empty;//"/" + sCategoryUrl + "/" + oTrim.TrimUrl + "/" + oTrim.ModelYearYear ;
                //trimURL = CommonFunctions.getModelUrl(sCategoryUrl, oTrim.ModelYearYear, oTrim.TrimUrl, TargetID);
                trimURL = CommonFunctions.GetMainUrl(sCategoryUrl,oTrim.TrimUrl);

                sb.AppendFormat(@"<div class=""{0}"">
                                            <div class=""product-image"" property=""act:popup"" data-msrp=""{10}"" data-id=""{1}"" data-position=""{6} top"" data-popup-modal=""false"" data-popup-class=""toolbox-top-alt {7}"" data-popup-content-url=""{2}"" data-popup-handler=""rollover"">
                                                <a href=""{8}"" rel=""nofollow""><img src=""{3}?Crop=auto&width={4}&height={5}"" width=""150"" height=""75"" /></a>
                                            </div>"
                                            , divclass
                                            , oTrim.TrimExportKey
                                            , tooltipURL
                                            , CommonFunctions.BuildModelImageUrl(oTrim)
                                            , 150
                                            , IsAtv ? 100 : 75
                                            , elementIndex % numberPerRow > 2 ? "left" : "right"
                                            , elementIndex % numberPerRow > 2 ? "toolbox-right" : "toolbox-left"
                                            , trimURL
                                            , oTrim.ModelYearYear
                                            , oTrim.MSRP
                        );

                elementIndex++;
                //+ "-"  +  oTrim.ModelYearYear.ToString() code is added temporarily, remove later on   
                sb.AppendFormat(@"<h3 class=""fnat price-heading""><a href=""{1}"">{0}</a></h3>"
                                            , oTrim.TrimName
                                            , trimURL
                          );
                sb.Append(getPrice(oTrim));

                //sb.AppendFormat(@"<p>{0}</p></div><div><!-- IE QUIRK --></div>", oTrim.TrimDescription);
                sb.AppendFormat(@"</div><div><!-- IE QUIRK --></div>");

                if (firstElement == 4)
                    firstElement = 0;
                else
                    firstElement++;
            }

            return sb.ToString();
        }

        protected string getPrice(Trim oTrim)
        {
            decimal DiscountAmount;
            decimal DiscountedPrice;

            PETrimService ts = new PETrimService();  
            StringBuilder sb = new StringBuilder();

            if (oTrim != null)
            {
				var MSRP = ts.GetMSRPWithoutColor(oTrim);
				if (this.IsQuebecPricing)
					MSRP = ts.GetMSRPWithoutColor(oTrim) + oTrim.FreightPDI;
                Colour oColour = ts.GetcolorByColorExportKey(TargetID, oTrim.TrimID, oTrim.ColorExportKey, PageLanguage);
                //if (oColour != null)
                //{
                //    DiscountAmount = PETrimService.getDiscountAmount((oColour.MSRP_Markup > 0 ? oColour.MSRP_Markup : oTrim.MSRP), oColour.Discount, oColour.DiscountPercentage);
                //    DiscountedPrice = PETrimService.getDiscountedPrice((oColour.MSRP_Markup > 0 ? oColour.MSRP_Markup : oTrim.MSRP), oColour.Discount, oColour.DiscountPercentage);
                //}
                //else
                //{
                    DiscountAmount = PETrimService.getDiscountAmount(MSRP, oTrim.PriceDiscountAmount, oTrim.PriceDiscountPercentage);
					DiscountedPrice = PETrimService.getDiscountedPrice(MSRP, oTrim.PriceDiscountAmount, oTrim.PriceDiscountPercentage);
                //}
                if (ts.IsComingSoonWithoutColor(oTrim))
                {
                    sb.AppendFormat(@"  <div class=""fc final price coming-soon""><strong class=""fnat"">{0}</strong><span>{1}</span></div>", ResourceManager.GetString("txtComingSoon"), ResourceManager.GetString("MSRP")); //Price without any kind of discount
                }
                else
                {
	                

                    if (DiscountAmount != 0)
                    {
                        sb.AppendFormat(@"<div class=""fc original price""><del><span class=""strike""></span><strong class=""fnat"">{0}</strong></del><span>{1}</span></div>", string.Format(ResourceManager.GetString("valMSRP4"), MSRP), ResourceManager.GetString("MSRP"));
                        sb.AppendFormat(@"<div class=""fc special price""><strong class=""fnat"">{0}</strong><span>{1}</span></div>", string.Format(ResourceManager.GetString("valMSRP4"), DiscountAmount), ResourceManager.GetString("Discount"));
                        sb.AppendFormat(@"<div class=""fc final price""><strong class=""fnat"">{0}</strong><span>{1}</span></div>", string.Format(ResourceManager.GetString("valMSRP4"), DiscountedPrice), ResourceManager.GetString("YourPrice"));
                    }
                    else       //Without discount
                    {
                        sb.AppendFormat(@"<div class=""fc final price""><strong class=""fnat"">{0}</strong></div>", string.Format(ResourceManager.GetString("valMSRP2"), "$", MSRP, ResourceManager.GetString("MSRP"))); //Price without any kind of discount
                    }
                }

            }
            return sb.ToString();
        }

        protected string getPriceAtv(Trim oTrim)
        {
            decimal DiscountAmount = PETrimService.getDiscountAmount(oTrim.MSRP, oTrim.PriceDiscountAmount, oTrim.PriceDiscountPercentage);
            decimal DiscountedPrice = PETrimService.getDiscountedPrice(oTrim.MSRP, oTrim.PriceDiscountAmount, oTrim.PriceDiscountPercentage);
            PETrimService ts = new PETrimService();
            StringBuilder sb = new StringBuilder();
            Colour oColour = null;

            if (ts.IsComingSoonWithColor(oTrim, oColour))
            {
                sb.AppendFormat(@"  <div class=""fc final price coming-soon""><strong class=""fnat"">{0}</strong><span>{1}</span></div>", ResourceManager.GetString("txtComingSoon"), ResourceManager.GetString("MSRP")); //Price without any kind of discount
            }
            else
            {
                if (DiscountAmount != 0)
                {
                    sb.AppendFormat(@"<div class=""fc original price""><del><span class=""strike""></span><strong>{0}</strong></del><span>{1}</span></div>", string.Format(ResourceManager.GetString("valMSRP4"), oTrim.MSRP), ResourceManager.GetString("MSRP"));
                    sb.AppendFormat(@"<div class=""fc special price""><strong class=""fnat"">{0}</strong><span>{1}</span></div>", string.Format(ResourceManager.GetString("valMSRP4"), DiscountAmount), ResourceManager.GetString("Discount"));
                    sb.AppendFormat(@"<div class=""fc final price""><strong class=""fnat"">{0}</strong><span>{1}</span></div>", string.Format(ResourceManager.GetString("valMSRP4"), DiscountedPrice), ResourceManager.GetString("YourPrice"));
                }
                else       //Without discount
                    sb.AppendFormat(@"  <div class=""fc final price"">{0}</div>", string.Format(ResourceManager.GetString("valMSRP2"), "$", oTrim.MSRP, ResourceManager.GetString("MSRP"))); //Price without any kind of discount
            }
            return sb.ToString();
        }

        public static string BuildModelImageUrlAtv(Trim objTrim, string ColorExportKey = null)
        {
            ColorExportKey = string.IsNullOrEmpty(ColorExportKey) ? objTrim.ColorExportKey : ColorExportKey;
            string ModelImage = string.Format("{0}_{1}_{2}", objTrim.TrimExportKey, ColorExportKey, "front.png");
            string ImagePath = string.Format("{0}/{1}/{2}/{3}"
                                                    , "/assets/Models"
                                                    , (objTrim.ModelYearYear > 0 ? objTrim.ModelYearYear.ToString() : "9999")
                                                    , objTrim.BaseModelExportKey
                                                    , ModelImage
                                                 );
            //if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(ImagePath)))
            //{
            //    return ImagePath;
            //}
            //else
            //{
            //    ImagePath = string.Format("{0}/{1}"
            //                                        , "/assets/Models"
            //                                        , "default_atv_image.jpg"
            //                                     );
            //}
            return ImagePath;
        }
    }
}