﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HondaCA.WebUtils.CMS;
using Navantis.Honda.CMS.Client.Elements;

namespace Marine.Web.ContentPages.PowerUp
{
    public partial class PowerUpLanding : ContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            if (this.MainUrl == "/rockthered") splashContainer.Attributes["class"] = splashContainer.Attributes["class"] + " rockthered";

            Tout tout = CMSHelper.getCmsElementFromCmsPage<Tout>(this.ContentPage, "GenericContent_Tout");
            PlaceHolderTout.Visible = tout.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (PlaceHolderTout.Visible)
            {
                PlaceHolderTout.Controls.Add(new LiteralControl(getToutImage(tout)));
            }

            //Top Text
            FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH");
            LiteralTopText.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (LiteralTopText.Visible)
            {
                LiteralTopText.Text = ffh.Html ?? string.Empty;
            }

            //Bottom Text
            ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, (this.IsQuebecPricing) ?   "GenericContent_FFH3":  "GenericContent_FFH2");
            LiteralBottomText.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (LiteralBottomText.Visible)
            {
                LiteralBottomText.Text = ffh.Html ?? string.Empty;
            }

            


        }
    }
}