﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.Web;
using HondaCA.Common;
using HondaCA.WebUtils.CMS;
using System.Text;

namespace Marine.Web.ContentPages
{
    public partial class HondaAdvantage : ContentBasePage
    {
        string ModelFamilyUrl = string.Empty;
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            ModelFamilyUrl = Request.QueryString["ModelFamilyUrl"] ?? string.Empty;
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if(body != null)
            {

                body.Attributes.Add("class", "page-landing page-honda-advantage" + (string.IsNullOrEmpty(ModelFamilyUrl) ? string.Empty : (" page-product-" + ModelFamilyUrl)));
                this.IsMasterBodyClassUpdate = true;
            }

            if (this.MainUrl == "/produits-pour-lentreprise" || this.MainUrl == "/commercial-products")
            {
                body.Attributes.Add("class", "page-commerical-product");
                this.IsMasterBodyClassUpdate = true;

                divGC.Visible = true;

                GenericContent gc = CMSHelper.getCmsElementFromCmsPage<GenericContent>(this.ContentPage,"GenericContent_GC1");
                plGenericContent.Visible = gc.ContentElement.Display ? true : this.IsStaging ? true : false;
                plGenericContent.Controls.Add(new LiteralControl(getGenericContent(gc)));
                
            }


            Tout tout = CMSHelper.getCmsElementFromCmsPage<Tout>(this.ContentPage, "GenericContent_Tout");
            plTopBanner.Visible = tout.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plTopBanner.Visible)
            {
                if(this.MainUrl == "/produits-pour-lentreprise" || this.MainUrl == "/commercial-products")
                    plTopBanner.Controls.Add(new LiteralControl(getToutImage(tout)));
                else
                    plTopBanner.Controls.Add(new LiteralControl(getTopHeader(tout)));
            }

            FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH");
            bool visibleffh = ffh.ContentElement.Display ? true : IsStaging ? true : false;
            if(visibleffh)
            {
                plMiddleContent.Controls.Add(new LiteralControl(ffh.Html));
            }
        }

        protected string getGenericContent(GenericContent gc)
        {
            StringBuilder sb = new StringBuilder();
            if (gc != null)
            {
                if (gc.ContentElement.Display || IsStaging == true)
                {
                    if (gc.Items != null)
                    {
                        int firstElement = 0;
                        foreach (Navantis.Honda.CMS.Client.Elements.GenericContentItem item in gc.Items)
                        {

                            sb.Append(string.Format(@"<li class=""{0}""><img src=""{1}"" alt="""" />", firstElement == 0 ? "first fl" : "fl", item.Image));
                            sb.Append(string.Format(@"{0}</li>", item.Summary));
                            firstElement++;
                        }
                    }
                }
            }
            return sb.ToString();
        }

        protected string getTopHeader(Tout tout)
        {
            string filename = string.Empty, res = string.Empty;
            if (tout != null && tout.Items != null)
            {
                if (tout.ContentElement.Display)
                {

                    VideoFile vf = new VideoFile();
                    vf.tout = tout;
                    vf.mode = VideoFile.Mode.Advntages;
                    vf.lang = PageLanguage.GetCultureStringValue().Substring(0, 2);
                    filename = vf.createAutomotiveFile(this.PageLanguage, Marine.Web.VideoFile.Mode.Advntages);

                    res = string.Format(@"<div class=""hero slideshow_player_support"" title=""{0}""></div>", "/_Global/xml/Gallery/" + HttpUtility.UrlEncode(vf.newFileName));
                    return res;
                }
            }

            return "";
        }
    }
}