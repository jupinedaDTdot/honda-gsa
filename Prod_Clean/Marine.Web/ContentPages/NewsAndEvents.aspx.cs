﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;

using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.Service.Common;
using HondaCA.Entity;
using HondaCA.Entity.Common;
using HondaCA.Common;
using HondaCA.WebUtils.StringUtility;

namespace Marine.Web.ContentPages
{
    public partial class NewsAndEvents : ContentBasePage
    {
        protected string NewsID = string.Empty;
        const int textMaxLength = 300;
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl.ToLower();
            base.OnInit(e);
        }            
        
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();
            
            string Newstitle = string.Empty;
            if (this.MainUrl == "/news-events/news-detail" || this.MainUrl == "/news-events/news-detail_fr" || this.MainUrl == "/nouvelles-evenements/details-des-nouvelles")
            {
                //Regex reg = new Regex("[^A-Za-z0-9]");
                //String output = reg.Replace(input, String.Empty);
                NewsID = Request.Form["NewsID"];
                Newstitle = Request.Form["Newstitle"];

                if (!string.IsNullOrEmpty(Request.Form["DNewsID"])) NewsID = Request.Form["DNewsID"];
                if (!string.IsNullOrEmpty(Request.Form["DNewstitle"])) Newstitle = Request.Form["DNewstitle"];
                
            }
            else
                NewsID = Request.QueryString["NewsID"] == string.Empty ? string.Empty : Request.QueryString["NewsID"];
            string strProvince = Request.QueryString["Province"] ==  string.Empty ? string.Empty : Request.QueryString["Province"];
            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                string cssClass = string.Empty;
                if(this.PageLanguage == Language.English)
                    cssClass = this.MainUrl == "/news-events/news-detail" ? "page-news-events-overview" : this.MainUrl == "/news" ? "page-news-events-news" : "page-news-events-show-info";
                else
                    cssClass = this.MainUrl == "/news-events" ? "page-news-events-overview" : this.MainUrl == "/nouvelles" ? "page-news-events-news" : "page-news-events-show-info";

                body.Attributes.Add("class", cssClass);
                this.IsMasterBodyClassUpdate = true;
            }

            idvNewsList.Visible = false;
            //ElementNews.Visible = false;
            ListView1.Visible = false;
            divPager.Visible = false;
            PeopleDataPager.Visible = false;
            
            RegionService rs = new RegionService();
            HondaCA.Entity.EntityCollection<Region> regionList = new HondaCA.Entity.EntityCollection<Region>();
            regionList = rs.GetAllRegions(PageLanguage.GetCultureStringValue());

            StringBuilder sb1 = new StringBuilder();
            foreach (Region region in regionList)
            {
                string strSelected = string.Empty;
                strSelected =   ! string.IsNullOrEmpty(strProvince)  ? strProvince.ToLower() == region.Name.ToLower() ? "Selected" :"" : "";
                sb1.Append(string.Format(@"<option value=""{0}"" {2}>{1}</option>", "/news-events/" + NewsID+"/" + region.Name.Replace(" ","-").ToLower(), region.Name, strSelected));
            }
            
            litProvince.Text = sb1.ToString();

            IElementContent element = null;
            if (this.RelatedPages.Count > 0)
            {
                foreach (string relatedPageName in this.RelatedPages.Keys)
                {
                    foreach (ContentPage cp in this.RelatedPages[relatedPageName])
                    {
                        foreach (KeyValuePair<string, ContentElement> el in cp.Elements)
                        {
                            switch (el.Key)
                            {
                                case "GenericContentPointer_CP":
                                    try
                                    {
                                        leftsidemenu1.FindControl("ElementControl1").Visible = false;
                                    }
                                    catch (Exception) { }
                                    element = cp.Elements["GenericContentPointer_CP"].DeserializeElementObject() as IElementContent;
                                    leftsidemenu1.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;
                                    if (leftsidemenu1.Visible)
                                    {
                                        leftsidemenu1.menuTitle = ResourceManager.GetString("News_events");
                                        leftsidemenu1.LoadData(setMenu(this.GetMenuFromContentPointer("GenericContentPointer_CP", cp)));
                                    }
                                    else
                                    {
                                        leftsidemenu1.menuCMSControlName = "GenericContentPointer_CP";
                                        leftsidemenu1.Visible = false;
                                    }
                                    break;
                                case "GenericContent_FFH":
                                    element = cp.Elements["GenericContent_FFH"].DeserializeElementObject() as IElementContent;
                                    plmiddleFFH.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;
                                    if (plmiddleFFH.Visible)
                                    {
                                        plmiddleFFH.Controls.Add(new LiteralControl(this.GetFreeFormHtml("GenericContent_FFH", cp)));
                                    }
                                    break;
                                case "GenericContentPointer_CP2":
                                    if (this.MainUrl == "/news-events")
                                    {
                                        datapart.Visible = true;
                                        plhGenericContent.Visible = true;
                                        plhGenericContentPointer_CP2.Visible = true;
                                        
                                        GenericContent gc = cp.Elements["GenericContent_GC1"].DeserializeElementObject() as GenericContent;
                                        plhGenericContent.Visible = gc.ContentElement.Display ? true : this.IsStaging ? true : false;
                                        if (plhGenericContent.Visible)
                                        {
                                            plhGenericContent.Controls.Add(new LiteralControl(getStringFromGC(gc)));
                                        }

                                        element = cp.Elements["GenericContentPointer_CP2"].DeserializeElementObject() as IElementContent;
                                        plhGenericContentPointer_CP2.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;
                                        if (plhGenericContentPointer_CP2.Visible)
                                        {
                                            GenericContent gc2 = null;
                                            gc2 = this.GetContentPointerElement("GenericContentPointer_CP2", gc2, cp) as GenericContent;
                                            plhGenericContentPointer_CP2.Controls.Add(new LiteralControl(getStringNews(gc2, "top")));
                                        }
                                    }
                                    else if(this.MainUrl == "/news-events/news-detail" || this.MainUrl == "/nouvelles-evenements/details-des-nouvelles")
                                    {
                                        newDetail.Visible = true;
                                        element = cp.Elements["GenericContentPointer_CP2"].DeserializeElementObject() as IElementContent;
                                        plhNewsDetail.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;
                                        if (plhNewsDetail.Visible)
                                        {
                                            GenericContent gc2 = null;
                                            gc2 = this.GetContentPointerElement("GenericContentPointer_CP2", gc2, cp) as GenericContent;
                                            plhNewsDetail.Controls.Add(new LiteralControl(getStringNewsDetail(gc2, NewsID)));
                                        }
                                    }
                                    else if (this.MainUrl == "/news-events/tradeshow" || this.MainUrl.Contains("/news-events/tradeshow") || this.MainUrl.Contains("/nouvelles-evenements/exposition-commerciale"))
                                    {
                                        divAutoTrade.Visible = true;
                                        if (strProvince != string.Empty)
                                        {
                                            if (!string.IsNullOrEmpty(strProvince))
                                            {
                                                GenericContent gContent2 = cp.Elements["GenericContent_GC1"].DeserializeElementObject() as GenericContent;
                                                plhAutoTrade.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;
                                                if (plhAutoTrade.Visible)
                                                {
                                                    plhAutoTrade.Controls.Add(new LiteralControl(getStringAutoTradeGC(gContent2, "TradeShow")));
                                                }
                                            }
                                        }
                                    }
                                    else if (this.MainUrl == "/news-events/outdoor-events" || this.MainUrl.Contains("/news-events/outdoor-events") || this.MainUrl.Contains("/nouvelles-evenements/extérieur-evenements"))
                                    {
                                        divAutoTrade.Visible = true;
                                        if (strProvince != string.Empty)
                                        {
                                            if (!string.IsNullOrEmpty(strProvince))
                                            {
                                                GenericContent gContent2 = cp.Elements["GenericContent_GC1"].DeserializeElementObject() as GenericContent;
                                                plhAutoTrade.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;
                                                if (plhAutoTrade.Visible)
                                                {
                                                    plhAutoTrade.Controls.Add(new LiteralControl(getStringAutoTradeGC(gContent2, "OutDoorEvent")));
                                                }
                                            }
                                        }
                                    }
                                    else if (this.MainUrl == "/news" || this.MainUrl == "/nouvelles")
                                    {
                                        idvNewsList.Visible = true;
                                        ListView1.Visible = true;
                                        divPager.Visible = true;
                                        PeopleDataPager.Visible = true;

                                        element = cp.Elements["GenericContentPointer_CP2"].DeserializeElementObject() as IElementContent;
                                        ListView1.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;
                                        if (ListView1.Visible)
                                        {
                                            NewsDataBind(cp);
                                            //GenericContent gContent2 = null;
                                            //gContent2 = this.GetContentPointerElement("GenericContentPointer_CP2", gContent2, cp) as GenericContent;
                                            //if (gContent2 != null && gContent2.Items != null)
                                            //{
                                            //    ListView1.DataSource = gContent2.Items;
                                            //    ListView1.DataBind();

                                            //    if (gContent2.Items.Count < PeopleDataPager.PageSize + 1)
                                            //        PeopleDataPager.Visible = false;
                                            //    else
                                            //        PeopleDataPager.Visible = true;
                                            //}                                            
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
            }

        }
        
        private string GetFreeFormHtml(string sElementName, ContentPage cp)
        {
            FreeFormHtml content = (cp.Elements.ContainsKey(sElementName) ? cp.Elements[sElementName].DeserializeElementObject() : null) as FreeFormHtml;
            if (content != null)
            {
                if (content.ContentElement.Display)
                    return content.Html;
                else
                    return string.Empty;
            }
            throw new Exception(string.Format("Element : {0} not found in Page:{1}",sElementName,this.MainUrl));
        }

        private IElementContent GetContentPointerElement(string sElementName, IElementContent elementType, ContentPage cp)
        {
            ContentPointer contentPointer = cp.Elements[sElementName].DeserializeElementObject() as ContentPointer;
            IElementContent element = null;
            if (contentPointer != null)
            {
                element = contentPointer.GetTargetContentElement();
            }
            return element;
        }
        private Navantis.Honda.CMS.Client.Elements.Menu GetMenuFromContentPointer(string sElementName, ContentPage cp)
        {
            Navantis.Honda.CMS.Client.Elements.Menu menu = null;
            menu = this.GetContentPointerElement(sElementName, menu, cp) as Navantis.Honda.CMS.Client.Elements.Menu;
            return menu;
        }

        protected string getStringNewsDetail(GenericContent gc, string NewsID)
        {
            StringBuilder sb = new StringBuilder();

            int MatchID = Convert.ToInt32(NewsID);

            if (gc.Items != null && gc != null)
            {
                foreach (GenericContentItem item in gc.Items)
                {
                    if (item.DisplayOrder == MatchID)
                    {
                        sb.Append(string.Format(@"<h2>{0}</h2><h3>{1}</h3>", item.Title, item.MoreText));
                        if (!string.IsNullOrEmpty(item.Image))
                            sb.Append(string.Format(@"<div class=""pic""><img src=""{0}""></div>", item.Image));
                        sb.Append(string.Format(@"<div class=""grouped""><p>{0}</p></div>", item.Summary));
                    }
                }
            }
            return sb.ToString();
        }

        protected string getStringAutoTradeGC(GenericContent gc, string optType)
        {
            StringBuilder sb = new StringBuilder();
            if (gc.Items != null && gc != null)
            {
                int itemFirst = 0;
                foreach (GenericContentItem item in gc.Items)
                {
                    string cssClass = itemFirst == 0 ? "content_section info" : "content_section_dotted";

                    sb.Append(string.Format(@"<div class=""{0}""> <h2>{1}</h2><h3>{2}</h3>", cssClass, item.Title,item.MoreText));
                    if(!string.IsNullOrEmpty(item.Image))
                        sb.Append(string.Format(@"<div class=""pic""><img src=""{0}""></div>", item.Image));
                    sb.Append(string.Format(@"{0}</div>", item.Summary));
                    itemFirst++;
                }
            }
            else
            {
                sb.Append(string.Format(@"<div class=""content_section""><p class=""info-msg"">"));
                if (optType == "TradeShow")
                    sb.Append(string.Format(@"Sorry, there is no further Trade Show events currently planned in this province.<br>Please check surrounding areas for upcoming events."));
                if (optType == "OutDoorEvent")
                    sb.Append(string.Format(@"Sorry, there is no further Outdoor events currently planned in this province.<br>Please check surrounding areas for upcoming events."));
                sb.Append(string.Format(@"</p></div>"));
            }
            return sb.ToString();
        }

        protected string getStringNews(GenericContent gc, string optNews)
        {
            StringBuilder sb = new StringBuilder();
            
            if(gc != null && gc.Items != null)
            {
                int itemFirst=0;
                foreach(GenericContentItem item in gc.Items)
	            {
                    string strClass = itemFirst ==0 ? "content_section" : "content_section_dotted";
                    if(itemFirst ==0)
                        sb.AppendFormat(@"<div class=""{0}""><div class=""h1 fl"">{1}</div><p class=""fr""><a href=""/{1}"" class=""btn secondary""><span>{2}</span></a></p><div class=""clr""></div>",strClass,ResourceManager.GetString("News").ToLower(),ResourceManager.GetString("txtViewAll"));
                    else
                        sb.AppendFormat(@"<div class=""{0}"">",strClass);

                    //int minstr = item.Summary.Length > 300 ? 300 : item.Summary.Length;
                    string strUrl = "/news-events/news-detail/";// +item.DisplayOrder;
                    string strLink = "<a onclick=showdetail('"+item.DisplayOrder+"','"+item.Title+"') href='" + strUrl + "'>" + ResourceManager.GetString("txtReadMore") + "</a>";
                    sb.AppendFormat(@"<h2>{0}</h2><h3>{1}</h3>", item.Title,item.MoreText);
                    if (!string.IsNullOrEmpty(item.Image))
                        sb.AppendFormat(@"<div class=""pic""><img src=""{0}""></div>", item.Image);
                    //sb.AppendFormat(@"<p>{0}</p>", item.Summary.Substring(0, minstr) + " " + strLink);                   
                    sb.Append(string.Format(@"<p>{0}</p>", item.Summary.TruncateAtWord(textMaxLength, " ") + " " + strLink));                   
                    sb.AppendFormat(@"</div>");

                    itemFirst++;

                    if (itemFirst >= 5 && optNews != "all")
                        break;
	            }
            }           
            return sb.ToString();
        }

        protected string getStringFromGC(GenericContent content)
        {
            StringBuilder sb = new StringBuilder();

            if (content != null && content.Items != null)
            {
                foreach (GenericContentItem item in content.Items)
                {
                    sb.AppendFormat(@"<div class=""item""><div class=""thumb""><img src=""{0}"" alt=""""/></div><div class=""desc""><h2 class=""small"">{1}</h2>{2}",item.Image,item.Title,item.Summary);
                    if(item.MoreText != string.Empty && item.MoreTextLinkURL != string.Empty)
                    {
                        sb.AppendFormat(@"<p><a href=""{0}"">{1}</a></p>",item.MoreTextLinkURL,item.MoreText);
                    }
                    sb.AppendFormat(@"</div><div class=""clr""></div></div>");                    
                }
            }
            return sb.ToString();
        }

        protected void PeopleDataPager_Init(object sender, EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl.ToLower();
            base.OnInit(e);

            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            DataPager dp = (DataPager)sender;
            NextPreviousPagerField fieldPrev = (NextPreviousPagerField)dp.Fields[0];
            fieldPrev.PreviousPageText = ResourceManager.GetString("txtPrevious");
            NextPreviousPagerField fieldNext = (NextPreviousPagerField)dp.Fields[2];
            fieldNext.NextPageText = ResourceManager.GetString("txtNext");
        }

        private void NewsDataBind(ContentPage cp)
        {
            GenericContent gContent2 = null;
            gContent2 = this.GetContentPointerElement("GenericContentPointer_CP2", gContent2, cp) as GenericContent;
            if (gContent2 != null && gContent2.Items != null)
            {
                ListView1.DataSource = gContent2.Items;
                ListView1.DataBind();
            }
        }

        protected void ListView_PagePropertiesChanged(object sender, EventArgs e)
        {
            this.ListView1.DataBind();
        }

        protected void ListView1_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if(e.Item.ItemType == ListViewItemType.DataItem)
            {
                ListViewDataItem row = (ListViewDataItem)e.Item;
                GenericContentItem item = (GenericContentItem) row.DataItem;
                Literal litTitle = (Literal)e.Item.FindControl("litTitle");
                litTitle.Text = item.Title;
                Literal litDate = (Literal)e.Item.FindControl("litDate");
                litDate.Text = item.MoreText;
                
                if (!string.IsNullOrEmpty(item.Image))
                {
                    Literal litImage = (Literal)e.Item.FindControl("litImage");
                    litImage.Text = @"<div><img src="""  + item.Image + @"""/></div>";
                }

                //int minstr = item.Summary.Length > 300 ? 300 : item.Summary.Length;

                string path= string.Empty;
                if (this.PageLanguage == Language.English)
                    path = "/news-events/news-detail";
                else
                    path = "/nouvelles-evenements/details-des-nouvelles";
                string strLink = @"<a onclick='ShowNewsDetail(" + item.DisplayOrder + ",\"" + RemoveSpecialChar(item.Title) + "\",\"" + path + "\")' href=\"#\">" + ResourceManager.GetString("txtReadMore") + "</a>";
                Literal litSummery = (Literal)e.Item.FindControl("litSummery");
                //litSummery.Text = item.Summary.Substring(0, minstr) +" " + strLink;                                
                litSummery.Text = item.Summary.TruncateAtWord(textMaxLength, " ") + " " + strLink;                                
            }
        }
        private string RemoveSpecialChar(string strhtml)
        {
            return strhtml.Replace("'", string.Empty).Replace("\"", string.Empty);
         }
                
    }
}