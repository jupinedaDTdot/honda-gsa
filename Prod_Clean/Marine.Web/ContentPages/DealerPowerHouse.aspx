﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true" CodeBehind="DealerPowerHouse.aspx.cs" Inherits="Marine.Web.ContentPages.DealerPowerHouse" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/FindPHDDealer.ascx" TagName="FindADealer" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/find-a-dealer.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="cap_top_default pf"></div>
	<div class="container pf">
		<div class="content_container">
            <div class="content_section top_graphic">
                <uc:ElementControl id="ElementControl1" runat="server"  ElementName="GenericContent_Tout" Visible="true" />         
                <asp:PlaceHolder ID="plTopBanner" runat="server" > </asp:PlaceHolder>				
			</div>
            <div class="powerhouse_section">
				<div class="content_section left">
					<uc:ElementControl id="eleControlMiddle1" runat="server"  ElementName="GenericContent_FFH" Visible="true" />         
                    <asp:PlaceHolder ID="plhMiddleTopContent" runat="server" > </asp:PlaceHolder>
				</div>
                <div class="fr">
					<div class="find-dealer-widget-insert">
                        <uc2:FindADealer ID="FindPHDDealer1" runat="server" />
                    </div>
                    <div class="find-dealer-widget-insert-cap"></div>
                </div>
				<%--<div class="content_section btn map-button">
					<p class="fr"><asp:HyperLink ID="HyperLinkFindADealer" runat="server" Visible="false"></asp:HyperLink></p>
				</div>
                <div id="canada_map"></div>--%>
				<div class="clr"></div>
			</div>
            <div class="content_section">
				<div class="h1 mb10"><%=ResourceManager.GetString("txtCustomerSatisfaction")%></div>
				<h2 class="small mb30"><%=ResourceManager.GetString("txtDealerCommitment")%></h2>
				<div class="sub_section">
					<uc:ElementControl id="ElementControl2" runat="server"  ElementName="GenericContent_GC1" Visible="true" />         
                    <asp:PlaceHolder ID="plGenericThreeColumns" runat="server" > </asp:PlaceHolder>
					<div class="clr"></div>
				</div>
			</div>
            <div class="content_section honda_hotel">
				<uc:ElementControl id="ElementControl3" runat="server"  ElementName="GenericContent_GC2" Visible="true" />         
                <asp:PlaceHolder ID="plGenericTwoColumns" runat="server" > </asp:PlaceHolder>
				<div class="clr"></div>
			</div>
        </div>
	</div>
	<div class="cap_bottom_default pf"></div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BottomCap" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
<div class="current-offers dn">
	<h2 class="small"><%= ResourceManager.GetString("txtViewCurrentOffer")%></h2>
	<select>
        <asp:Literal ID="LiteralRegionSelectOptions" runat="server"></asp:Literal>
    </select>
</div>        
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
    <script type="text/javascript" src="/_Global/js/sections/current-offers-map.js"></script>
</asp:Content>
