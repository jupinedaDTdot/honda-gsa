﻿using System;
using System.Web;
using HondaCA.WebUtils;
using HondaCA.WebUtils.CMS;
using HondaCA.Common;
using Navantis.Honda.CMS.Client;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Globalization;
using Navantis.Honda.CMS.Client.Elements;
using Microsoft.Practices.EnterpriseLibrary.Caching;

using Marine.Web;

namespace Navantis.Honda.CMS.Demo
{
	public class CmsContentBasePage : CmsBasePage
	{
		public virtual Language PageLanguage { get; set; }
		ICacheManager _CacheManager;

		private bool _shouldPreserveMainUrlQueryString = false;
		public virtual bool ShouldPreserveMainUrlQueryString { get { return _shouldPreserveMainUrlQueryString; } set { _shouldPreserveMainUrlQueryString = value; } }

		public ICacheManager HCacheManager
		{
			set { _CacheManager = value; }
			get { return _CacheManager; }
		}

		public CmsContentBasePage()
		{
		}

		public ResourceManager ResourceManager
		{
			get
			{
				Assembly assembly = Assembly.GetExecutingAssembly();
				ResourceManager resourceManager = new ResourceManager("Marine.Web.Resources.Strings", assembly);
				return resourceManager;
			}
		}

		public int TargetID
		{
			get
			{
				return HondaCA.Common.Global.TargetID;
			}
		}

		public bool IsMasterBodyClassUpdate
		{
			get;
			set;
		}

		public string ToggleURL { get; set; } //for Non cms pages ==> if required to implement  

		protected new virtual void InitializeCulture()
		{
			if (PageLanguage == 0)
				PageLanguage = Language.English;

			SetCulture(PageLanguage);
		}

		private void SetCulture(Language language)
		{
			string cultureString = Cultures.GetCultureStringValue(language);
			UICulture = cultureString;
			Culture = cultureString;

			Thread.CurrentThread.CurrentCulture =
				CultureInfo.CreateSpecificCulture(cultureString);
			Thread.CurrentThread.CurrentUICulture = new
				CultureInfo(cultureString);
			base.InitializeCulture();
		}

		protected override void OnInit(EventArgs e)
		{
			//try {
			HCacheManager = CacheFactory.GetCacheManager();
			if (ShouldPreserveMainUrlQueryString)
			{
				//if (string.IsNullOrWhiteSpace(MainUrl))
				//{
				//  MainUrl = Request.RawUrl;
				//}
			}
			else
			{
				if (string.IsNullOrWhiteSpace(MainUrl))
				{
					//MainUrl = RequestUtils.CreateUriFromPathQuery(Request.RawUrl).AbsolutePath;
				}
				else
				{
					MainUrl = RequestUtils.CreateUriFromPathQuery(MainUrl).AbsolutePath;
				}
			}
			base.OnInit(e);
			//}
			//catch (Exception ex)
			//{
			//    if (ex is HttpException)
			//    {
			//        if (ex.Message == "404 Page Not Found")
			//            Response.Redirect("/error/404");
			//    }
			//    throw ex;
			//}
		}

		public Language getLanguageForCMSPage()
		{
			Language tmpLanguage;
			if (this.PageSetting != null)
				tmpLanguage = (Language)this.PageSetting.Language;// cms class should pass Value 1 or 2 
			else
				tmpLanguage = Language.English;

			return tmpLanguage;
		}

		public IElementContent getContentPointerElement(string sElementName, IElementContent elementType)
		{
			ContentPointer contentPointer = this.ContentPage.Elements[sElementName].DeserializeElementObject() as ContentPointer;
			IElementContent element = null;
			if (contentPointer != null)
			{
				element = contentPointer.GetTargetContentElement();
			}
			return element;
		}

		public Navantis.Honda.CMS.Client.Elements.Menu getMenuFromContentPointer(string sElementName)
		{
			Navantis.Honda.CMS.Client.Elements.Menu menu = null;
			menu = getContentPointerElement(sElementName, menu) as Navantis.Honda.CMS.Client.Elements.Menu;
			return menu;
		}

		public Navantis.Honda.CMS.Client.Elements.FreeFormHtml getFreeFormHtmlFromContentPointer(string sElementName)
		{
			Navantis.Honda.CMS.Client.Elements.FreeFormHtml ffh = null;
			ffh = getContentPointerElement(sElementName, ffh) as Navantis.Honda.CMS.Client.Elements.FreeFormHtml;
			return ffh;
		}

		public Navantis.Honda.CMS.Client.Elements.GenericLink getGenericLinkFromContentPointer(string sElementName)
		{
			Navantis.Honda.CMS.Client.Elements.GenericLink gl = null;
			gl = getContentPointerElement(sElementName, gl) as Navantis.Honda.CMS.Client.Elements.GenericLink;
			return gl;
		}

		public int getElementHeight(string sElementName)
		{
			IElementContent element = this.ContentPage.Elements[sElementName].DeserializeElementObject() as IElementContent;
			int height = 0;
			if (element != null)
			{
				Int32.TryParse(element.ContentElement.Height ?? "0", out height);
			}
			return height;
		}

		public int getElementWidth(string sElementName)
		{
			IElementContent element = this.ContentPage.Elements[sElementName].DeserializeElementObject() as IElementContent;
			int width = 0;
			if (element != null)
			{
				Int32.TryParse(element.ContentElement.Width ?? "0", out width);
			}
			return width;
		}

		public bool getElementVisiblity(string sElementName)
		{
			IElementContent element = this.ContentPage.Elements[sElementName].DeserializeElementObject() as IElementContent;
			bool visiblity = false;
			if (element != null)
			{
				visiblity = element.ContentElement.Display;
			}
			return visiblity;
		}

		public string getProvinceCodeFromCookie(BaseMaster baseMaster)
		{
			string currentProvinceCode = string.Empty;
			BaseMaster basemst = baseMaster;
			HttpCookie currentCookie;
			if (basemst != null)
				if (basemst.currentCookie != null)
				{
					currentProvinceCode = basemst.currentCookie.Value ?? string.Empty;
				}
				else
				{
					currentCookie = Request.Cookies.Get("hondaprovince");
					currentProvinceCode = string.Empty;
					if (currentCookie != null) { currentProvinceCode = currentCookie.Value; }
					if (string.IsNullOrEmpty(currentProvinceCode))
					{
						string provCode = string.Empty;
						HondaCA.Service.Common.LocationService ls = new HondaCA.Service.Common.LocationService();
						string ip = Request.ServerVariables[HondaCA.Common.Global.getRemotAddressConst].ToString();
						provCode = ls.getProvinceCodeByIP(ip, PageLanguage);
						if (string.IsNullOrEmpty(provCode)) provCode = "ON";

						if (!string.IsNullOrEmpty(provCode))
						{
							HttpCookie h = new HttpCookie("hondaprovince");
							h.Value = provCode;
							Response.Cookies.Add(h);
						}
					}
				}
			return currentProvinceCode;
		}

		public FreeFormHtml getFreeFormHtmlFromContentPage(string sElementName, ContentPage contentPage)
		{
			if (contentPage != null)
			{
				FreeFormHtml ffh = (contentPage.Elements.ContainsKey(sElementName) ? contentPage.Elements[sElementName].DeserializeElementObject() : null) as FreeFormHtml;
				return ffh;
			}
			else return null;
		}

		public Navantis.Honda.CMS.Client.Elements.GenericContent getGenericContentFromContentPointer(string sElementName)
		{
			Navantis.Honda.CMS.Client.Elements.GenericContent gc = null;
			gc = getContentPointerElement(sElementName, gc) as Navantis.Honda.CMS.Client.Elements.GenericContent;
			return gc;
		}

		[Obsolete("Use 'GetToutRotatorHtml' instead.")]
		protected string setToutwithXML(string sElementName, VideoFile.Mode md)
		{
			Tout Tout = getTout(sElementName);
			string fileName = string.Empty, res = string.Empty;
			if (Tout != null && Tout.Items != null)
			{
				if (Tout.ContentElement.Display && Tout.Items.Count > 0)
				{
					VideoFile vf = new VideoFile();
					vf.tout = Tout;
					vf.mode = md; //VideoFile.Mode.Automotive;
					vf.lang = PageLanguage.GetCultureStringValue().Substring(0, 2);
					//fileName = vf.createAutomotiveFile();
					fileName = vf.createAutomotiveFile(this.PageLanguage, md);

					if (this.Page.ToString().ToLower().Contains("default_aspx"))
					{
						res = string.Format(@"<div class""hero""><img class='round-corners-big' src='_Global\img\layout\round_corners_950_331.png' width='950px' height='331px' /><div class=""hero slideshow_player_support"" width='950px' height='355px' title=""{0}""></div></div>", "/_Global/xml/gallery/" + HttpUtility.UrlEncode(vf.newFileName));
					}
					else
					{
						res = string.Format(@"<div class=""hero slideshow_player_support"" title=""{0}""></div>", "/_Global/xml/gallery/" + HttpUtility.UrlEncode(vf.newFileName));
					}
					return res;
				}
			}
			return "";
		}

		protected string GetToutRotatorHtml(string url, string sElementName, int width = 950, int height = 400)
		{
			Tout tout = getTout(sElementName);
			string fileName = string.Empty, res = string.Empty;
			if (tout.IsVisible())
			{
				res = string.Format(@"<div class='hero toutrotator' property='act:toutrotator' data-url='{0}' data-width='{1}' data-height='{2}'></div>",
									URLUtils.BuildRotatorPlaylistUrl(url, sElementName),
									width,
									height
				  );
			}
			return res;
		}

		protected string GetToutWithXml(string sElementName, int width = 0, int height = 0)
		{
			try
			{
				Tout tout = getTout(sElementName);
				if (tout == null)
				{
					tout = (Tout)getContentPointerElement(sElementName, tout);
				}
				string res;

				if (getElementWidth(sElementName) > 0)
				{
					width = getElementWidth(sElementName);
				}
				if (getElementHeight(sElementName) > 0)
				{
					height = getElementHeight(sElementName);
				}

				if (tout.IsVisible())
				{
					res = string.Format(@"<div class=""hero toutrotator"" property=""act:toutrotator"" data-url=""{0}"" data-width='{1}' data-height='{2}'></div>", HttpUtility.HtmlAttributeEncode(URLUtils.BuildRotatorPlaylistUrl(MainUrl, sElementName)), width, height);
					return res;
				}
			}
			catch (Exception)
			{
				// there is an error.
			}
			return string.Empty;
		}

		protected string getToutImage(Tout tout)
		{
			string filename = string.Empty, res = string.Empty;
			if (tout != null && tout.Items != null)
			{
				if (tout.ContentElement.Display && tout.Items.Count > 0)
				{
					res = string.Format(@"<img src=""{0}"" alt="""" />", tout.Items[0].PathReference);
					return res;
				}
			}
			return "";
		}

		protected Tout getTout(string sElementName)
		{
			Tout tout = (this.ContentPage.Elements.ContainsKey(sElementName) ? this.ContentPage.Elements[sElementName].DeserializeElementObject() : null) as Tout;
			return tout;
		}

		public string getGenericLinks(string sElementName)
		{
			GenericLink gl = getGenericLinkFromContentPointer(sElementName);
			string res = string.Empty;
			if (gl != null && gl.Items != null)
			{
				if (gl.ContentElement.Display)
				{
					foreach (GenericLinkItem item in gl.Items)
					{
						res += string.Format(@"<li><h3>{0}</h3><a href=""{1}"" Title=""{2}"" target=""_blank"">{2}</a></li>", item.Title, item.File, item.LinkText);
					}
					return res;
				}
			}
			return "";
		}
	}
}