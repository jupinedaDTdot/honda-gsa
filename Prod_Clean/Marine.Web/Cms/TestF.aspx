﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="Navantis.Honda.CMS.Demo.TestF" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Register TagPrefix="uc" TagName="ElementControl" Src="~/Cms/Console/ElementControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="ElementControlCompaq" Src="~/Cms/Console/ElementControlCompaq.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="1" cellpadding="0" cellspacing="0" width="100%">   
        <tr>
            <td>
                <strong>Use this page to test all the <%=this.ContentPage.Elements.Count %> CMS editors. <a href="/Cms/Display.aspx?url=/Cms/Test.aspx">Click here to display all the cmsContents of this cmsPage</a>.
                <br />
                This cmsPage contains one cmsContent for each cmsElement of which cmsElement.Name equals to its cmsElementType.Name</strong>
            </td>    
        </tr>
        <%if (this.ContentPage.ProductID != null)
        {%>
            <tr>
                <td>
                    <strong>
                    <%if (this.ContentPage.TrimID == null)
                    {%>
                        This is a model page for model with Product_ID equals to <%=this.ContentPage.ProductID%>
                    <%}
                    else
                    {%>
                        This is a trim page for model with Product_ID equals to <%=this.ContentPage.ProductID%> and Trim_ID equals to <%=this.ContentPage.TrimID%>
                    <%}%>
                    </strong>
                    <br />
                    <br />
                </td>
            </tr>
        <%}%>
        <tr>
            <td>
                <strong>Related Pages:</strong>
                <br />
                <div>
                    <%if (this.RelatedPages.Count > 0)
                    {
                        foreach (string relatedPageName in this.RelatedPages.Keys)
                        {
                            foreach (ContentPage cp in this.RelatedPages[relatedPageName])
                            {%>
                                <strong>Related Page Name:</strong> <%=relatedPageName%> <strong>Related Page URL:</strong> <%=cp.MainURL%>  
                                <strong>Related Page Product ID:</strong> <%=cp.ProductID%>  
                                <strong>Related content elements:</strong>
                                <%foreach (string elementName in cp.Elements.Keys)
                                {%>
                                    <%=elementName%>;
                                <%}%>
                                <br />
                            <%}
                        }
                    }
                    else
                    {%>
                        This page has no related pages.
                        <br />
                    <%}%>
                </div>
            </td>
        </tr>

        <%                   
        if (this.ContentPage.Elements.ContainsKey("News"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="newsElementControl" runat="server" ElementName="News" />
                        <strong>News</strong>
                        <br />
                        News element Content - Date Modified: <%=this.ContentPage.Elements["News"].LastUpdated%>
                        <br />
                        <div>
                            <%News gc = (this.ContentPage.Elements["News"].DeserializeElementObject() as News);
                            if (gc != null && gc.Items != null)
                            {
                                foreach (NewsItem gcItem in gc.Items)
                                {%>
                                    <%=gcItem.Title%> - 
                                    <%=gcItem.Image%> - 
                                    <%=gcItem.Summary%>
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%} 
         if (this.ContentPage.Elements.ContainsKey("EngineApplication"))
        {%>
            <tr>
                <td colspan="2">
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq27" runat="server" ElementName="EngineApplication" />
                        <div>
                            <strong>EngineApplication</strong>
                            <br />
                            EngineApplication element Content - Date Modified: <%=this.ContentPage.Elements["EngineApplication"].LastUpdated%>
                            <br />
                            <%EngineApplication mrla = (this.ContentPage.Elements["EngineApplication"].DeserializeElementObject() as EngineApplication);
                            if (mrla != null && mrla.Items != null)
                            {
                                foreach (EngineApplicationColumn mrlai in mrla.Items)
                                {%>
                                    <%=mrlai.ImageEn%>
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}%>
        
        <%if (this.ContentPage.Elements.ContainsKey("Accessories"))
        {%>
            <tr>
                <td colspan="2">
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq23" runat="server" ElementName="Accessories" />
                        <div>
                            <strong>Accessories</strong>
                            <br />
                            Accessories element Content - Date Modified: <%=this.ContentPage.Elements["Accessories"].LastUpdated%>
                            <br />
                            <%Accessories a = (this.ContentPage.Elements["Accessories"].DeserializeElementObject() as Accessories);
                            if (a != null && a.Items != null)
                            {
                                foreach (AccessoriesItem aItem in a.Items)
                                {%>
                                    <%=aItem.Title%> - 
                                    <%=aItem.Price%> -
                                    <%=aItem.Size%> - 
                                    <%=aItem.Link%> - 
                                    <%=aItem.Image%> - 
                                    <%=aItem.Colour%> - 
                                    <%=aItem.Description%>
                                    <%=aItem.ThumbnailImage%>
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("Event"))
        {%>
            <tr>
                <td colspan="2">
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq24" runat="server" ElementName="Event" />
                        <strong>Event</strong>
                        <br />
                        Event element Content - Date Modified: <%=this.ContentPage.Elements["Event"].LastUpdated%>
                        <br />
                        <div>
                            <%Event e = (this.ContentPage.Elements["Event"].DeserializeElementObject() as Event);
                            if (e != null && e.Items != null)
                            {
                                foreach (EventItem eItem in e.Items)
                                {%>
                                    <%=eItem.Title%> - 
                                    <%=eItem.Hours%> -
                                    <%=eItem.Province%>
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("ToutHighlightPoint"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq11" runat="server" ElementName="ToutHighlightPoint" />
                        <strong>ToutHighlightPoint</strong>
                        <br />
                        ToutHighlightPoint element Content - Date Modified: <%=this.ContentPage.Elements["ToutHighlightPoint"].LastUpdated%>
                        <br />
                        <div>
                            <%ToutHighlightPoint thp = (this.ContentPage.Elements["ToutHighlightPoint"].DeserializeElementObject() as ToutHighlightPoint);
                            if (thp != null && thp.Items != null)
                            {
                                foreach (ToutHighlightPointItem thpItem in thp.Items)
                                {%>
                                    <%=thpItem.Title%> - 
                                    <%=thpItem.Image%> - 
                                    <%=thpItem.Summary%> -
                                    <%=thpItem.X.ToString()%> -
                                    <%=thpItem.Y.ToString()%>
                                    <br />
                                <%}
                            }%>                        
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("PartsAndAccessories"))        
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq10" runat="server" ElementName="PartsAndAccessories" />
                        <strong>PartsAndAccessories</strong>
                        <br />
                        PartsAndAccessories element Content - Date Modified: <%=this.ContentPage.Elements["PartsAndAccessories"].LastUpdated%>
                        <br />
                        <div>                        
                            <%PartsAndAccessories paa = (this.ContentPage.Elements["PartsAndAccessories"].DeserializeElementObject() as PartsAndAccessories);
                            if (paa != null && paa.Items != null)
                            {
                                foreach (PartsAndAccessoriesItem paaItem in paa.Items)
                                {%>
                                    <%=paaItem.ProductName%> - 
                                    <%=paaItem.ProductDetail%> - 
                                    <%=paaItem.Price%> -
                                    <%=paaItem.Link%>
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("ProductSelectionWizard"))
        {%>
            <tr>
                <td>
                     <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq9" runat="server" ElementName="ProductSelectionWizard" />                    
                        <strong>ProductSelectionWizard</strong><br />
                        ProductSelectionWizard element Content - Date Modified: <%=this.ContentPage.Elements["ProductSelectionWizard"].LastUpdated%>
                        <br />
                        <div>                        
                            <%ProductSelectionWizard psw = (this.ContentPage.Elements["ProductSelectionWizard"].DeserializeElementObject() as ProductSelectionWizard);
                            if (psw != null && psw.Items != null)
                            {
                                foreach (ProductSelectionQuestion psq in psw.Items)
                                {%>
                                    <%=psq.Title%> -                                 
                                    <%=psq.QuestionText%> -
                                    <%=psq.QuestionSubText%>
                                    <br />
                                    <%if (psq.QuestionItems != null)
                                    {
                                        foreach (ProductSelectionQuestionItem psqItem in psq.QuestionItems)
                                        {%>
                                            <%=psqItem.Title%> - 
                                            <%=psqItem.SubTitle%>
                                            <%=psqItem.Image%>
                                            <br />
                                            <%if (psqItem.Products != null)
                                            {
                                                foreach (ProductSelectionQuestionItemProductItem psqipi in psqItem.Products)
                                                {%>
                                                    <%=psqipi.ModelName%> - <%=psqipi.TrimName%>
                                                    <br />
                                                <%}
                                            }
                                        }
                                    }
                                }
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("GenericLink"))
        {%>
            <tr>
                <td>
                     <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq8" runat="server" ElementName="GenericLink" />
                        <strong>GenericLink</strong><br />
                        GenericLink element Content - Date Modified: <%=this.ContentPage.Elements["GenericLink"].LastUpdated%>
                        <br />
                        <div>
                            <%GenericLink gl = (this.ContentPage.Elements["GenericLink"].DeserializeElementObject() as GenericLink);
                            if (gl != null && gl.Items != null)
                            {
                                foreach (GenericLinkItem gli in gl.Items)
                                {%>
                                    <%=gli.Title%> - 
                                    <%=gli.File%> - 
                                    <%=gli.LinkText%> ( <%=gli.LinkURL%> }
                                    <br />
                                <%}
                            }%>
                         </div>       
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("RelatedProduct"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq7" runat="server" ElementName="RelatedProduct" />
                        <strong>RelatedProduct</strong>
                        <br />
                        RelatedProduct element Content - Date Modified: <%=this.ContentPage.Elements["RelatedProduct"].LastUpdated%>
                        <br />
                        <div>
                            <%RelatedProduct gc = (this.ContentPage.Elements["RelatedProduct"].DeserializeElementObject() as RelatedProduct);
                            if (gc != null && gc.Items != null)
                            {
                                foreach (RelatedProductItem gcItem in gc.Items)
                                {%>
                                    <%=gcItem.ModelID%> - 
                                    <%=gcItem.TrimID%> 
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}

        if (this.ContentPage.Elements.ContainsKey("AdBuilderNewAsset"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq6" runat="server" ElementName="AdBuilderNewAsset" />
                        <strong>AdBuilderNewAsset</strong>
                        <br />
                        AdBuilderNewAsset element Content - Date Modified: <%=this.ContentPage.Elements["AdBuilderNewAsset"].LastUpdated%>
                        <br />
                        <div>
                            <%AdBuilderNewAsset gc = (this.ContentPage.Elements["AdBuilderNewAsset"].DeserializeElementObject() as AdBuilderNewAsset);
                            if (gc != null && gc.Items != null)
                            {
                                foreach (AdBuilderNewAssetItem gcItem in gc.Items)
                                {%>
                                    <%=gcItem.ContentID%> - 
                                    <%=gcItem.ItemID%> 
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("AdBuilderFeaturedAsset"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq5" runat="server" ElementName="AdBuilderFeaturedAsset" />
                        <strong>AdBuilderFeaturedAsset</strong>
                        <br />
                        AdBuilderFeaturedAsset element Content - Date Modified: <%=this.ContentPage.Elements["AdBuilderFeaturedAsset"].LastUpdated%>
                        <br />
                        <div>
                            <%AdBuilderFeaturedAsset gc = (this.ContentPage.Elements["AdBuilderFeaturedAsset"].DeserializeElementObject() as AdBuilderFeaturedAsset);
                            if (gc != null && gc.Items != null)
                            {
                                foreach (AdBuilderFeaturedAssetItem gcItem in gc.Items)
                                {%>
                                    <%=gcItem.ContentID%> - 
                                    <%=gcItem.ItemID%> 
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("AdBuilderAsset"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq4" runat="server" ElementName="AdBuilderAsset" />
                        <strong>AdBuilderAsset</strong>
                        <br />
                        AdBuilderAsset element Content - Date Modified: <%=this.ContentPage.Elements["AdBuilderAsset"].LastUpdated%>
                        <br />
                        <div>                        
                            <%AdBuilderAsset aba = (this.ContentPage.Elements["AdBuilderAsset"].DeserializeElementObject() as AdBuilderAsset);
                            if (aba != null && aba.Items != null)
                            {
                                foreach (AdBuilderAssetItem abaItem in aba.Items)
                                {%>
                                    <%=abaItem.Title%> - 
                                    <%=abaItem.PreviewThumbnail%> - 
                                    <%=abaItem.Notes%>
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("GenericContent"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq3" runat="server" ElementName="GenericContent" />
                        <strong>GenericContent</strong>
                        <br />
                        GenericContent element Content - Date Modified: <%=this.ContentPage.Elements["GenericContent"].LastUpdated%>
                        <br />
                        <div>
                            <%GenericContent gc = (this.ContentPage.Elements["GenericContent"].DeserializeElementObject() as GenericContent);
                            if (gc != null && gc.Items != null)
                            {
                                foreach (GenericContentItem gcItem in gc.Items)
                                {%>
                                    <%=gcItem.Title%> - 
                                    <%=gcItem.Image%> - 
                                    <%=gcItem.Summary%>
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("Tout"))
        {%> 
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq12" runat="server" ElementName="Tout" />                               
                        <strong>Tout</strong>
                        <br />
                        Tout element Content - Date Modified: <%=this.ContentPage.Elements["Tout"].LastUpdated%>
                        <br />
                        <div>       
                            <%Tout tout = this.ContentPage.Elements["Tout"].DeserializeElementObject() as Tout;
                            if (tout != null && tout.Items != null)
                            {
                                foreach (ToutItem tItem in tout.Items)
                                {%>
                                    <%=tItem.Name%> - 
                                    <%=tItem.Title%> - 
                                    <%=tItem.URL%> - 
                                    <%=tItem.PathReference%>
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("Menu"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq13" runat="server" ElementName="Menu" />
                        <strong>Menu</strong>
                        <br />
                        Menu element Content - Date Modified: <%=this.ContentPage.Elements["Menu"].LastUpdated%>          
                        <div>
                            <%Navantis.Honda.CMS.Client.Elements.Menu menu = (this.ContentPage.Elements["Menu"].DeserializeElementObject() as Navantis.Honda.CMS.Client.Elements.Menu);
                            if (menu != null && menu.Items != null)
                            {
                                foreach (Navantis.Honda.CMS.Client.Elements.MenuItem menuItem in menu.Items)
                                {%>
                                    Top level item <%= menuItem.Name%>
                                    <br />
                                    <%foreach (Navantis.Honda.CMS.Client.Elements.MenuItem subMenuItem in menuItem.ChildItems)
                                    {%>
                                        2nd level item <%= subMenuItem.Name%>
                                        <br />
                                    <%}
                                }
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("Brochure"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControl4" runat="server" ElementName="Brochure" />
                        <strong>Brochure</strong>
                        <br />
                        Brochure element Content - Date Modified: <%=this.ContentPage.Elements["Brochure"].LastUpdated%>          
                        <br />
                        <div>
                            <%Brochure brochure = (this.ContentPage.Elements["Brochure"].DeserializeElementObject() as Brochure);
                            if (brochure != null && !(string.IsNullOrEmpty(brochure.PDF) && string.IsNullOrEmpty(brochure.URL)))
                            {%>
                                <%=brochure.PDF%>
                                <br />
                                <%=brochure.URL%>
                                <br />
                            <%}%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
        if (this.ContentPage.Elements.ContainsKey("PressRelease"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq25pr" runat="server" ElementName="PressRelease" />
                        <strong>PressRelease</strong>
                        <br />
                        PressRelease element Content - Date Modified: <%=this.ContentPage.Elements["PressRelease"].LastUpdated%>          
                        <br />
                        <div>
                            <%PressRelease pr = (this.ContentPage.Elements["PressRelease"].DeserializeElementObject() as PressRelease);
                            if (pr != null)
                            {%>
                                <%=pr.Title%>
                                <br />
                                <%=pr.PostDate%>
                                <br />
                                <%=pr.Summary%>
                                <br />
                                <%=pr.Thumbnail%>
                                <br />
                                <%=pr.Image%>
                                <br />
                            <%}%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
          
            
        if (this.ContentPage.Elements.ContainsKey("GenericForm"))
        {%>   
            <tr>
                <td>
                     <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq14" runat="server" ElementName="GenericForm" />                     
                        <strong>GenericForm</strong>
                        <br />
                        GenericForm element Content - Date Modified: <%=this.ContentPage.Elements["GenericForm"].LastUpdated%>          
                        <br />
                        <div>
                            <%GenericForm genericForm = this.ContentPage.Elements["GenericForm"].DeserializeElementObject() as GenericForm;
                            if (genericForm != null && genericForm.Items != null)
                            {
                                foreach (GenericFormField gfItem in genericForm.Items)
                                {%>
                                    <strong>Name:</strong>
                                    <%=gfItem.Name%>
                                    <strong>Type:</strong>
                                    <%=gfItem.Type%> 
                                    <br />
                                <%}%>
                                <br />
                                go through each field and create the form
                                <br />
                                <div>
                                    <%foreach (GenericFormField gfField in genericForm.Items)
                                    {
                                        if (gfField.Type == GenericFormFieldType.TextBox)
                                        {%>
                                            <%=gfField.Label%>:
                                            <input id="<%=gfField.Name%>" name="<%=gfField.Name%>" value="<%=gfField.Value%>" type="text" />
                                            <br />
                                            <br />
                                        <%}
                                        else if (gfField.Type == GenericFormFieldType.CheckBox)
                                        {%>
                                            <input id="<%=gfField.Name%>" name="<%=gfField.Name%>" type="checkbox" /> <%=gfField.Label%>
                                            <br />
                                            <br />
                                        <%}
                                    }%>

                                    note: you can use jquery/ajax to submit to another aspx and to improve UI. And use the field.ValidationMessage and field.IsRequired to implement validation<br />
                                    After submit form, you can use Report interface /Content/Report/ExportGenericFormResponse to export excel file.
                                    <br />
                                    <input type="submit" name="submitGenericForm" value="Submit Generic Form Test" />
                                </div>
                           <%}%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}

        if (this.ContentPage.Elements.ContainsKey("ModelNav"))
        {%> 
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq15" runat="server" ElementName="ModelNav" />
                        <strong>ModelNav</strong>
                        <br />
                        ModelNav element Content - Date Modified: <%=this.ContentPage.Elements["ModelNav"].LastUpdated%>          
                        <br />
                        <div>
                            <%ModelNav modelNav = (this.ContentPage.Elements["ModelNav"].DeserializeElementObject() as ModelNav);
                            if (modelNav != null && modelNav.Items != null)
                            {
                                foreach (ModelNavItem modelNavItem in modelNav.Items)
                                {%>
                                    Top level navigation <%= modelNavItem.Name%> - <%= modelNavItem.Title%> - <%= modelNavItem.URL%>
                                    <br />
                                    <%foreach (ModelNavItem subModelNavItem in modelNavItem.ChildItems)
                                    {%>
                                        Sub navigation <%= subModelNavItem.Name%> - <%= subModelNavItem.Title%> - <%= subModelNavItem.URL%>
                                        <br />
                                    <%}
                                }
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}

        if (this.ContentPage.Elements.ContainsKey("FreeFormHtml"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq1" runat="server" ElementName="FreeFormHtml" />
                        <strong>FreeFormHtml</strong>
                        <br />
                        FreeFormHtml element Content - Date Modified: <%=this.ContentPage.Elements["FreeFormHtml"].LastUpdated%>          
                        <br />
                        <div>
                            <%FreeFormHtml ffh = (this.ContentPage.Elements["FreeFormHtml"].DeserializeElementObject() as FreeFormHtml);
                            if (ffh != null)
                            {%>
                                <%=ffh.Html%>
                                <br />
                            <%}%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}

        if (this.ContentPage.Elements.ContainsKey("Gallery"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq16" runat="server" ElementName="Gallery" />
                        <strong>Gallery</strong>
                        <br />
                        Gallery element Content - Date Modified: <%=this.ContentPage.Elements["Gallery"].LastUpdated%>          
                        <br />
                        <div>                       
                            <%Gallery Gallery = this.ContentPage.Elements["Gallery"].DeserializeElementObject() as Gallery;
                            if (Gallery != null && Gallery.Items != null)
                            {
                                foreach (GalleryItem gItem in Gallery.Items)
                                {%>
                                    <a href="<%=gItem.ThumbAsset %>"><%=gItem.ThumbAsset%></a>
                                <%}
                            }%>
                        </div>                    
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("Review"))
        {%>
            <tr>
                <td>
                   <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq17" runat="server" ElementName="Review" />
                        <strong>Review</strong>
                        <br />
                        Review element Content - Date Modified: <%=this.ContentPage.Elements["Review"].LastUpdated%>          
                        <br />
                        <div>
                            <%Review review = this.ContentPage.Elements["Review"].DeserializeElementObject() as Review;
                            if (review != null && review.Items != null)
                            {
                                foreach (ReviewItem vlItem in review.Items)
                                {%>
                                    <strong><%=vlItem.Title%></strong>
                                    <br />
                                    <%=vlItem.Summary%>
                                    <br/>
                                    <%=vlItem.Source%>
                                    <a href="<%=vlItem.LinkURL %>">Read more</a>
                                <%}
                            }%>
                        </div>

                    </div>
                </td>
            </tr>
        <%}
        
        if (this.ContentPage.Elements.ContainsKey("VehicleLife"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq18" runat="server" ElementName="VehicleLife" />
                        <strong>VehicleLife</strong>
                        <br />
                        VehicleLife element Content - Date Modified: <%=this.ContentPage.Elements["VehicleLife"].LastUpdated%>          
                        <br />
                        <div>
                            <%VehicleLife vehicleLife = this.ContentPage.Elements["VehicleLife"].DeserializeElementObject() as VehicleLife;
                            if (vehicleLife != null && vehicleLife.Items != null)
                            {
                                foreach (VehicleLifeItem vlItem in vehicleLife.Items)
                                {%>
                                    <strong>Title:</strong><%=vlItem.Title%>
                                    <strong>URL:</strong><%=vlItem.URL%>
                                    <strong>ImageURL:</strong><%=vlItem.ImageURL%> 
                                    <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
        
        if (this.ContentPage.Elements.ContainsKey("GenericUrl"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq19" runat="server" ElementName="GenericUrl" />
                        <strong>GenericUrl</strong>
                        <br />
                        GenericUrl element Content - Date Modified: <%=this.ContentPage.Elements["GenericUrl"].LastUpdated%>          
                        <br />
                        <div>                        
                           <% GenericUrl genericUrl = this.ContentPage.Elements["GenericUrl"].DeserializeElementObject() as GenericUrl;
                            if (genericUrl != null)
                            {%>
                                <a href="<%=genericUrl.URL %>"><%=genericUrl.Title%></a> <%=genericUrl.URL%>
                            <%}%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("TopAccessories"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq20" runat="server" ElementName="TopAccessories" />                    
                        <strong>TopAccessories</strong>
                        <br />
                        TopAccessories element Content - Date Modified: <%=this.ContentPage.Elements["TopAccessories"].LastUpdated%>          
                        <br />
                        <div>
                            <%TopAccessories topAccessories = this.ContentPage.Elements["TopAccessories"].DeserializeElementObject() as TopAccessories;
                            if (topAccessories != null && topAccessories.Items != null)
                            {
                                foreach (TopAccessoriesItem taItem in topAccessories.Items)
                                {%>
                                 <strong>AccessoryItemID:</strong><%=taItem.AccessoryItemID%>
                                 <br />
                                <%}
                            }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
        
        if (this.ContentPage.Elements.ContainsKey("ContentPointer"))
        {%>
            <tr>
                <td>
                     <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq21" runat="server" ElementName="ContentPointer" />
                        <strong>ContentPointer</strong>
                        <br />
                        ContentPointer element Content - Date Modified: <%=this.ContentPage.Elements["ContentPointer"].LastUpdated%>          
                        <br />
                        <div>                        
                            <%ContentPointer contentPointer = this.ContentPage.Elements["ContentPointer"].DeserializeElementObject() as ContentPointer;
                            if (contentPointer != null)
                            {
                                IElementContent iecForCP = contentPointer.GetTargetContentElement();%>
                                <strong>Target Content ID:</strong><%=contentPointer.ContentID%>
                                <br />
                                <strong>Target Content Type:</strong>
                                 
                                <%if(iecForCP != null)
                                    Response.Write(iecForCP.ToString());%>
                                <br />
                                <strong>Target Page:</strong>
                                <%if (iecForCP != null)
                                    Response.Write(iecForCP.ContentElement.Parent.MainURL);%>
                                <br />                                
                            <%}%>
                        </div>
                    </div>
                </td>      
            </tr>        
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("Placeholder"))
        {%> 
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq22" runat="server" ElementName="Placeholder" />
                        <strong>Placeholder</strong>
                        <br />
                        Placeholder element Content - Date Modified: <%=this.ContentPage.Elements["Placeholder"].LastUpdated%>          
                        <br />
                        <div>
                            <%Placeholder ph = this.ContentPage.Elements["Placeholder"].DeserializeElementObject() as Placeholder;
                            if (ph != null)
                            {%>
                                <%=ph.ContentElement.ID%>                            
                            <%}%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}
            
        if (this.ContentPage.Elements.ContainsKey("Specification"))
        {%>
            <tr>
                <td>
                    <div class="has-edit-button">
                        <uc:ElementControlCompaq id="ElementControlCompaq2" runat="server" ElementName="Specification" />
                        <strong>Specification</strong>
                        <br />
                        Specification element Content - Date Modified: <%=this.ContentPage.Elements["Specification"].LastUpdated%>          
                        <br />
                        <div>
                            <%Specification spec = (this.ContentPage.Elements["Specification"].DeserializeElementObject() as Specification);
                            if (spec != null && spec.ContentTable != null)
                            {%>
                                Table Heading: <%=spec.ContentTable.Heading%>
                                <br />
                                <%foreach (Section sec in spec.ContentTable.Sections)
                                {%>
                                    <strong>Section Heading: <%=sec.Heading%> </strong>
                                    <br />
                                    <%for (int trimIndex = 0; trimIndex < sec.Trims.Count; trimIndex++)
                                    {%>
                                        Trim Value: <%=sec.Trims[trimIndex]%>
                                        <br />                      
                                  
                                        <%foreach (TrimSectionValues trimSecV in sec.TrimValues)
                                        {%>
                                            trim section value title: <%=trimSecV.Title%>
                                            <strong>trim section value text:</strong>
                                            <%if (trimSecV.Value.Count > trimIndex)
                                                Response.Write(trimSecV.Value[trimIndex]);
                                            else
                                                Response.Write("<font color='red'>Error to get this value.</font>");%>
                                            <br />            
                                        <%}
                                      }
                                  }
                              }%>
                        </div>
                    </div>
                </td>
            </tr>
        <%}%>
    </table>    
</asp:Content>
