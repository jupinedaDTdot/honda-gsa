﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeBehind="AccessDenied.aspx.cs" Inherits="Navantis.Honda.CMS.Demo.Cms.AccessDenied" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head id="Head1" runat="server">
    <title></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />    
    

</head>
<body>
    <form id="Form1" runat="server">


    <div class="page">
        <div class="header">
            <div class="title">
                <h1>
                    Honda.ca CMS - ASP.NET Application
                </h1>
            </div>
            <div class="clear hideSkiplink">
            </div>
        </div>
        <div class="main" style="color:Red;font-weight:bold">
                You are not authorized to access this page.
                <br />
                <a href="/Asimo.Security/Logout.aspx">Log out</a>, or click the 'Back' button to return to the previous page.
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="footer">
        
    </div>
    </form>
</body>
</html>

