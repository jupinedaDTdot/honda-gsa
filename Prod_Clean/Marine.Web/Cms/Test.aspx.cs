﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Navantis.Honda.CMS.Client;
using Navantis.Honda.CMS.Core;
using Navantis.Honda.CMS.Client.Elements;

namespace Navantis.Honda.CMS.Demo
{
    public partial class Test : CmsBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            //this is to test all the element types
            string url = this.Request.QueryString["url"];
            this.MainUrl = string.IsNullOrEmpty(url) ? "/Cms/Test.aspx" : url;
            
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                // determine which button was used
                if (!string.IsNullOrEmpty(this.Request.Form["submitGenericForm"]))
                {
                    var elements = this.ContentPage.Elements;
                    //add a generic form response
                    if (elements.ContainsKey("GenericForm"))
                    {
                        GenericForm genericForm = elements["GenericForm"].DeserializeElementObject() as GenericForm;
                        if (genericForm != null && genericForm.Items != null)
                        {
                            //get the value from the Request form
                            foreach (GenericFormField field in genericForm.Items)
                                field.Value = this.Request.Form[field.Name];

                            //insert into cmsGenericFormResponse and cmsGenericFormResponseItem table by calling the helper method
                            GenericFormHelper.CreateGenericFormResponse(genericForm.ContentElement.ID, genericForm.Fields);
                        }
                    }
                }
            }
        }
    }

}