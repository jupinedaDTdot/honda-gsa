﻿using System;
using System.Web;
using System.Configuration;
using System.Collections.Generic;

using Navantis.Honda.CMS.Core.Model;
using Navantis.Honda.CMS.Core.Service;
using Navantis.Honda.CMS.Core.Unity;
using Navantis.Honda.CMS.Core.Common;
using Navantis.Honda.CMS.Client.Elements;
using Navantis.Honda.CMS.Client;

using Microsoft.Practices.Unity;


namespace Navantis.Honda.CMS.Demo.Cms.Console
{
    public partial class SetPidCookie : System.Web.UI.Page
    {
        public string ModelName { get; set; }
        public string ModelYear { get; set; }
        public string TrimName { get; set; }
        public List<cmsPage> PagesRelatedTo { get; set; }

        #region Private Fields

        private IPageService pageService = Bootstrapper.Container.Resolve<IPageService>();
        //private IContentService contentService = Bootstrapper.Container.Resolve<IContentService>();
        //private IHondaCarsService hondaCarsService = Bootstrapper.Container.Resolve<IHondaCarsService>();
        private string application = ConfigurationManager.AppSettings[Constants.ApplicationSettings.CmsApplication];
        private int productID = 0;
        private int? trimID = null;
        private int? previousTrimID = null;
        private string returnURL = null;
        private string language = null;
        private Guid pageID;

        #endregion

        #region EventHandlers

        protected void Page_Load(object sender, EventArgs e)
        {
            var querystring = this.Request.QueryString;
            foreach (string key in querystring.Keys)
            {
                switch (key)
                {
                    case "ProductID":
                        productID = int.Parse(querystring["ProductID"]);
                        break;
                    case "ReturnURL":
                        returnURL = querystring["ReturnURL"];
                        break;
                    case "PreviousTrimID":
                        previousTrimID = int.Parse(querystring["PreviousTrimID"]);
                        break;
                    case "PageID":
                        pageID = new Guid(querystring["PageID"]);
                        break;
                    case "Language":
                        language = querystring["Language"];
                        break;
                    default:
                        break;
                }
            }

            IHondaCarsService hondaCarsService = Bootstrapper.Container.Resolve<IHondaCarsService>();
            if (previousTrimID.HasValue)
                trimID = hondaCarsService.GetTrimIDFromModel(application, productID, previousTrimID.Value);

            //cmsPage page = previousTrimID.HasValue ? pageService.CheckCmsPageExist(returnURL, application, productID, trimID.Value) : pageService.CheckCmsPageExist(returnURL, application, productID);
            //cmsPage page = pageService.CheckCmsPageExist(returnURL, application, language, productID, trimID);

            if (pageService.CheckCmsPageExist(returnURL, application, language, productID, trimID) == null)
            {
                ModelName = hondaCarsService.GetModelName(productID);
                TrimName = (trimID.HasValue) ? hondaCarsService.GetTrimName(trimID) : null;
                ModelYear = hondaCarsService.GetModelYear(productID);
                PagesRelatedTo = pageService.GetPagesRelatedToCurrentPage(pageID);
            }
            else
            {
                SetCookies(returnURL, language, productID, trimID);

                // Re-direct to returning URL
                Response.Redirect(returnURL);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            btnYes.Click += new EventHandler(btnYes_Click);
            btnNo.Click += new EventHandler(btnNo_Click);
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            Response.Redirect(returnURL);
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            SetCookies(returnURL, language, productID, trimID);

            // only done to create non-existing page
            /*cmsPage page = (previousTrimID.HasValue)
                            ? pageService.GetCmsPageIncludeContent(returnURL, application, productID, trimID.Value)
                            : pageService.GetCmsPageIncludeContent(returnURL, application, productID, null);*/

            //cmsPage page = pageService.GetCmsPageIncludeContent(CommonService.IsStaging_FromWebConfig(), returnURL, application, language, productID, trimID);
            cmsPage page = pageService.GetCmsPageIncludeContent(CommonService.IsStaging_FromWebConfig(), returnURL, CmsHttpContext.Current.SiteID, language, CmsHttpContext.Current.CmsUser.User_Name, productID, trimID);

            if (CommonService.IsCheckboxChecked(this.Request.Form["Update"]))
            {
                IContentService contentService = Bootstrapper.Container.Resolve<IContentService>();
                PageSetting pageSetting = new ContentElement(contentService.GetPageSettingContent(pageID)).DeserializeElementObject() as PageSetting;
                if (pageSetting != null && pageSetting.RelatedPages != null)
                {
                    List<cmsPage> cmsRelatedPages = new List<cmsPage>();
                    pageSetting.RelatedPages.ForEach(rp => cmsRelatedPages.Add(pageService.GetCmsPage(rp.PageID)));

                    //cmsPage pageWithRP = pageService.UpdateRelatedPages(page, cmsRelatedPages);
                    pageService.UpdateRelatedPages(page, cmsRelatedPages);
                }
            }

            // Re-direct to returning URL
            Response.Redirect(returnURL);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Set cookie values
        /// </summary>
        private void SetCookies(string returnURL, string language, int productID, int? trimID)
        {
            // Get cookie from the current request.
            HttpCookie cookieApplication = Request.Cookies.Get("CMS_APPLICATION");
            HttpCookie cookieProductId = Request.Cookies.Get("CMS_PRODUCT_ID");
            HttpCookie cookieTrimId = Request.Cookies.Get("CMS_TRIM_ID");
            HttpCookie cookieMainUrl = Request.Cookies.Get("CMS_MAIN_URL");
            HttpCookie cookieLanguage = Request.Cookies.Get("CMS_LANGUAGE");

            // Check if cookie exists in the current request.
            if (cookieApplication == null || cookieProductId == null || cookieTrimId == null || cookieMainUrl == null || cookieLanguage == null)
            {
                //// Create cookie.
                //cookieApplication = new HttpCookie("CMS_APPLICATION");
                //// No expiration datetime is set making this a browser session cookie
                //// Set value of cookie to product id.
                //cookieApplication.Value = application;
                //// Insert the cookie in the current HttpResponse.
                //Response.Cookies.Add(cookieApplication);
                Response.Cookies.Add(new HttpCookie("CMS_APPLICATION") { Value = application });

                //// Create cookie.
                //cookieProductId = new HttpCookie("CMS_PRODUCT_ID");
                //// No expiration datetime is set making this a browser session cookie
                //// Set value of cookie to product id.
                //cookieProductId.Value = productID.ToString();
                //// Insert the cookie in the current HttpResponse.
                //Response.Cookies.Add(cookieProductId);
                Response.Cookies.Add(new HttpCookie("CMS_PRODUCT_ID") { Value = productID.ToString() });

                if (trimID.HasValue)
                {
                    //// Create cookie.
                    //cookieTrimId = new HttpCookie("CMS_TRIM_ID");
                    //// No expiration datetime is set making this a browser session cookie
                    //// Set value of cookie to product id.
                    //cookieTrimId.Value = trimID.Value.ToString();
                    //// Insert the cookie in the current HttpResponse.
                    //Response.Cookies.Add(cookieTrimId);

                    Response.Cookies.Add(new HttpCookie("CMS_TRIM_ID") { Value = trimID.Value.ToString() });
                }

                //add the MainUrl cookie
                //cookieMainUrl = new HttpCookie("CMS_MAIN_URL");
                //cookieMainUrl.Value = returnURL;
                //Response.Cookies.Add(cookieMainUrl);
                Response.Cookies.Add(new HttpCookie("CMS_MAIN_URL") { Value = returnURL });

                //add the Language cookie
                Response.Cookies.Add(new HttpCookie("CMS_LANGUAGE") { Value = language });
            }
            else
            {
                // No expiration datetime is set making this a browser session cookie
                cookieApplication.Value = application;
                Response.Cookies.Set(cookieApplication);

                // Set value of cookie to product id.
                cookieProductId.Value = productID.ToString();
                // Insert the cookie in the current HttpResponse.
                Response.Cookies.Set(cookieProductId);

                cookieTrimId.Value = (trimID.HasValue) ? trimID.Value.ToString() : string.Empty;
                Response.Cookies.Set(cookieTrimId);

                cookieMainUrl.Value = returnURL;
                Response.Cookies.Set(cookieMainUrl);

                cookieLanguage.Value = language;
                Response.Cookies.Set(cookieLanguage);
            }
        }

        #endregion

    }
}