﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ElementControl.ascx.cs" Inherits="Navantis.Honda.CMS.Demo.Cms.ElementControl" %>
<%@ Register Assembly="Navantis.Honda.CMS.Core" Namespace="Navantis.Honda.CMS.Core" TagPrefix="cms" %>
<%@ Import Namespace="Navantis.Honda.CMS.Demo" %>
<%@ Import Namespace="Navantis.Honda.CMS.Core.Service" %>
<%@ Import Namespace="Navantis.Honda.CMS.Core.Unity" %>
<%@ Import Namespace="Navantis.Honda.CMS.Core" %>

<%     
    CmsBasePage page = this.Page as CmsBasePage;
    string userName = page.Username;
    bool isCMSSystemAdmin = page.IsCMSSystemAdmin;
    bool isEditorOrAdmin = isCMSSystemAdmin || page.IsEditor;
    bool isEditorAuthorOrAdmin = isEditorOrAdmin || page.IsAuthor;
    
    var id = this.ContentElement.ID;
    var controller = this.ContentElement.MvcController;
    var url = this.ContentElement.Parent.MainURL;

    bool isOldVersion = this.ContentElement.ContentHistoryID > 0;   
     
    var status = this.ContentElement.Status;
    bool isSubmitted = status == ContentStatus.Submitted;
    bool isSaved = !isSubmitted && status == ContentStatus.Saved;
    bool isRejected = !isSaved && status == ContentStatus.Rejected;

    bool lastSavedContent = userName.Equals(this.ContentElement.LastSavedBy);
    bool lastSubmittedContent = !lastSavedContent && userName.Equals(this.ContentElement.LastSubmittedBy);

    string enableCmsPublishConfig = ConfigurationManager.AppSettings["EnableCmsPublish"];
    bool enableCmsPublish = !string.IsNullOrWhiteSpace(enableCmsPublishConfig) && enableCmsPublishConfig.ToLower().Equals("true");
%>

<div id="cmsElementControl"  class="editable-pane">
    <div style="background-color: #fff;">
        <b><%=this.ContentElement.ElementName%></b> 
        <%if (isOldVersion)
        {%>
            <div style="background-color:Aqua;">
                Old Version <%=this.ContentElement.LastUpdated%>
            </div>            
        <%}
        else
        {%>
            <%=string.Format("Status: {0} by {1} on {2} Eastern Time", this.ContentElement.Status, this.ContentElement.OriginatorUser, this.ContentElement.LastUpdated)%>
        <%}%>
    </div>

    <%if (isOldVersion)
    {                      
        if (isEditorAuthorOrAdmin)
        {%>       
            <%--Clear History Link--%>     
            <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                border-left-width: 0px; height: 16px; width: 16px;" title="Clear History" alt="Clear History"
                src="/Cms/Images/Console/ViewVersion.gif" type="image" name="viewVersions" />&nbsp;
            <a class="CommandButton" title="Clear History" href="#" onclick="editCmsElement('ClearVersionHistory','<%=controller %>','<%=this.ContentElement.ContentHistoryID %>','<%=url %>');">
                Clear History</a>
        <%}
    }
    else
    {%>
        <div style="background-color: #fff;">       
            <%--View Link--%>
            <%if (isSubmitted)
            {
                if (isEditorAuthorOrAdmin)
                {%>
                    <%--View Link--%>
                    <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                        border-left-width: 0px; height: 16px; width: 16px;" title="View Content" alt="View Content"
                        src="/Cms/Images/Console/view.gif" type="image" name="viewElement" />&nbsp;
                    <a class="CommandButton" title="View Content" href="#" onclick="editCmsElement('Edit', '<%=controller %>','<%=id %>','<%=url %>');">
                        View</a>
                <%}
            }
            else
            {
                if ((isSaved && lastSavedContent) || status == ContentStatus.New || status == ContentStatus.Approved || (isRejected && lastSubmittedContent || isCMSSystemAdmin))
                {
                    if (isEditorAuthorOrAdmin)
                    {%>
                        <%--Edit Link--%>
                        <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                            border-left-width: 0px; height: 16px; width: 16px;" title="Edit Content" alt="Edit Content"
                            src="/Cms/Images/Console/edit.gif" type="image" name="editElement" />&nbsp;
                        <a class="CommandButton" title="Edit Content" href="#" onclick="editCmsElement('Edit', '<%=controller %>','<%=id %>','<%=url %>');">
                            Edit</a>
                    <%}
                      
                    // add option to see shedule log when content is in publisjInProgress state
                    if ( status.Equals("PublishInProgress") || status.Equals("6"))
                    {
                        if (isEditorAuthorOrAdmin)
                        {%>
                            <%--Edit Link--%>
                        <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                            border-left-width: 0px; height: 16px; width: 16px;" title="Check Publish Status" alt="Check Publish Status"
                            src="/Cms/Images/Console/view.gif" type="image" name="CheckPublishStatus" />&nbsp;
                        <a class="CommandButton" title="Check Publish Status" href="#" onclick="editCmsElement('ListScheduleLog', '<%=controller %>','<%=id %>','<%=url %>');">
                            Check Publish Status</a>
                        <%}
                    }
                }
            }%>

            <%--Undo Link--%>
            <%if ((isSaved && (isCMSSystemAdmin || lastSavedContent)) || (isRejected && (isCMSSystemAdmin || lastSubmittedContent)))
            {
                if (isEditorAuthorOrAdmin)
                {%>
                    <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                        border-left-width: 0px; height: 16px; width: 16px;" title="Undo Change" alt="Undo Change"
                        src="/Cms/Images/Console/Cmd_Undo.gif" type="image" name="undoElement" />&nbsp;
                    <a class="CommandButton" title="Undo Change" href="#" onclick="editCmsElement('UndoContent','<%=controller %>','<%=id %>','<%=url %>');">
                        Undo</a>
                <%}
            }%>

            <%if (isSaved)
            {
                if (lastSavedContent)
                {
                    if (!enableCmsPublish)
                    {
                        if (isEditorAuthorOrAdmin)
                        {%>
                        <%--Submit Link--%>
                        <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                            border-left-width: 0px; height: 16px; width: 16px;" title="Submit Content" alt="Submit Content"
                            src="/Cms/Images/Console/Submit.gif" type="image" name="submitElement" />&nbsp;
                        <a class="CommandButton" title="Submit Content" href="#" onclick="editCmsElement('SubmitContent','<%=controller %>','<%=id %>','<%=url %>');">
                            Submit
                        </a>
                    <%}
                    }
                    else
                    {%>
                        <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                            border-left-width: 0px; height: 16px; width: 16px;" title="Approve Content" alt="Publish"
                            src="/Cms/Images/Console/grant.gif" type="image" name="Publish" />&nbsp;
                        <a class="CommandButton" title="Publish" href="#" onclick="editCmsElement('PublishContent','<%=controller %>','<%=id %>','<%=url %>');">
                            Publish</a>
                    <%}
                }
                else
                {
                    if (isCMSSystemAdmin)
                    {%>
                        <%--Take Over Link--%>
                        <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                            border-left-width: 0px; height: 16px; width: 16px;" title="Take Over" alt="Take Over"
                            src="/Cms/Images/Console/Submit.gif" type="image" name="takeOverElement" />&nbsp;
                        <a class="CommandButton" title="Take Over" href="#" onclick="editCmsElement('TakeOverContent','<%=controller %>','<%=id %>','<%=url %>');">
                            Take Over
                        </a>
                    <%}
                }
            }
            else if (isSubmitted)
            {
                if (!enableCmsPublish)
                {
                    if (isEditorOrAdmin)
                    {%>
                    <%--Approve/Reject Link--%>
                    <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                        border-left-width: 0px; height: 16px; width: 16px;" title="Approve Content" alt="Approve Content"
                        src="/Cms/Images/Console/grant.gif" type="image" name="approveElement" />&nbsp;
                    <a class="CommandButton" title="Approve Content" href="#" onclick="editCmsElement('ApproveRejectContent','<%=controller %>','<%=id %>','<%=url %>');">
                        Approve/Reject</a>
                <%}
                }
                else
                {%>
                    <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                        border-left-width: 0px; height: 16px; width: 16px;" title="Approve Content" alt="Publish"
                        src="/Cms/Images/Console/grant.gif" type="image" name="Publish" />&nbsp;
                    <a class="CommandButton" title="Publish" href="#" onclick="editCmsElement('PublishContent','<%=controller %>','<%=id %>','<%=url %>');">
                        Publish</a>
                <%}
            }%>

            <%if (isEditorAuthorOrAdmin)
            {%>     
                <%--View Versions Link--%>
                <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                    border-left-width: 0px; height: 16px; width: 16px;" title="View Versions" alt="View Versions"
                    src="/Cms/Images/Console/ViewVersion.gif" type="image" name="viewVersions" />&nbsp;
                <a class="CommandButton" title="View Versions" href="#" onclick="editCmsElement('ViewVersionContent','<%=controller %>','<%=id %>','<%=url %>');">
                    View Versions
                </a>

                <%--View Text Difference Link--%>           
                <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                    border-left-width: 0px; height: 16px; width: 16px;" title="View Text Difference" alt="View Text Difference"
                    src="/Cms/Images/Console/ViewDifference.gif" type="image" name="viewTextDifference" />&nbsp;
                <a class="CommandButton" title="View Text Difference" href="#" onclick="viewTextDifference('<%=id %>', '<%=controller %>');">
                    View Text Difference
                </a>
            <%}%>
        </div>
        <%}%>
</div>
