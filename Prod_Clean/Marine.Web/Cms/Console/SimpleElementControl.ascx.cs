﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Client;

namespace Navantis.Honda.CMS.Demo.Cms.Console
{
    public partial class SimpleElementControl : System.Web.UI.UserControl
    {
        public ContentElement ContentElement
        {
            get;
            set;
        }

        public string ElementName
        {
            get;
            set;
        }

        public string MainUrl
        {
            get
            {
                return ((this.Page as CmsBasePage).MainUrl);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //get the ContentElement from the parent page by matching the ID
            CmsBasePage cmsPage = this.Page as CmsBasePage;
            if (cmsPage != null)
            {
                this.ContentElement = (this.Page as CmsBasePage).ContentPage.Elements[this.ElementName];
                if (!cmsPage.IsStaging)
                    this.Visible = false;
            }
        }
    }
}