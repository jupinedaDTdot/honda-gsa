﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Navantis.Honda.CMS.Core.Service;
using Navantis.Honda.CMS.Core.Unity;
using Navantis.Honda.CMS.Core.Model;
using Navantis.Honda.CMS.Client;
using Navantis.Honda.CMS.Client.Elements;
using Navantis.Honda.CMS.Core;
using Navantis.Honda.CMS.Core.Security;
using Navantis.Honda.CMS.Core.Common;
using Navantis.Honda.CMS.Demo;
using Microsoft.Practices.Unity;


namespace Navantis.Honda.CMS.Demo.Cms
{
    public partial class AuthoringConsole : UserControl
    {
        public ContentElement ContentElement { get; set; }
        public bool IsDisplay { get; set; }
        public string MainUrl { get { return page.MainUrl; } }
        public int? TrimID { get { return page.ContentPage.TrimID; } }
        private CmsBasePage page { get { return this.Page as CmsBasePage; } }

		protected void Page_Load(object sender, EventArgs e)
		{
			bool isStaging = Convert.ToBoolean( System.Configuration.ConfigurationManager.AppSettings["IsStaging"] );
			if( !isStaging )
			{
				//for live site, this should not be visible
				this.Visible = false;
			}
			else
			{
				CmsBasePage cmsPage = page;
				if( cmsPage != null )
				{
					//Set up Log Out link to allow quick re-entry to same page with different credentials. 
					LoginStatus loginStatus = (LoginStatus)HeadLoginView.FindControl( "HeadLoginStatus" );
					loginStatus.LogoutPageUrl = String.Concat( "/Asimo.Security/Logout.aspx?returnUrl=", Request.Path );

					this.ContentElement = cmsPage.ContentPage.Elements["PageSetting"];
					IsDisplay = ContentPage.IsDisplay( cmsPage.ContentPage );

					if( cmsPage.IsStaging )
					{
						//this is the staging site                    
						//initialize the model year dropdownlist, when this is a product page and not postback
						if( cmsPage.ContentPage.ProductID.HasValue && cmsPage.ContentPage.ProductID.Value > 0 )
						{
							if( !IsPostBack )
							{
								//cms_ddlModelYearsList.Items.Clear();
								IHondaCarsService hondaCarsService = Bootstrapper.Container.Resolve<IHondaCarsService>();

								// Get ActiveModeYears to populate dropdown
								Dictionary<int, int> activeModelYears = hondaCarsService.GetActiveModelYearsAsDictionary( cmsPage.ContentPage.ProductID.Value );

								if( activeModelYears.Count > 0 )
								{
									cms_ddlModelYearsList.DataTextField = "Key";
									cms_ddlModelYearsList.DataValueField = "Value";
									cms_ddlModelYearsList.DataSource = activeModelYears;
									cms_ddlModelYearsList.DataBind();
									cms_ddlModelYearsList.SelectedValue = Convert.ToString( cmsPage.ContentPage.ProductID.Value );
								}
								else
								{
									cms_ddlModelYearsList.Visible = false;
								}
							}
						}
						else
						{
							cms_ddlModelYearsList.Visible = false;
						}
					}
					else
					{
						this.Visible = false;
					}
				}
			}
		}

       public void LoginStatus_LoggedOut(Object sender, System.EventArgs e)
        {
           HttpContext.Current.Items.Remove(Navantis.Honda.CMS.Core.Common.Constants.SessionKeys.CmsHttpContext);
        }
    }
}