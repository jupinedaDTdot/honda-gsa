﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

namespace Navantis.Honda.CMS.Demo.Cms
{
    public partial class SetPageMode : System.Web.UI.Page
    {
        //private const string QueryStringKey = "Mode";

        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        [WebMethod]
        public static PageMode GetPageViewMode()
        {
            PageMode pageMode = PageMode.Edit;

            if (HttpContext.Current.Session["PageMode"] != null)
            {
                pageMode = (PageMode)Enum.Parse(typeof(PageMode), HttpContext.Current.Session["PageMode"].ToString()); 
            }

            return pageMode;
        }

        [WebMethod]
        public static void SetPageViewMode(string pageMode)
        {
            if (!string.IsNullOrEmpty(pageMode))
            {
                HttpContext.Current.Session["PageMode"] = (PageMode)Enum.Parse(typeof(PageMode), pageMode);
            }
        }

    }
}