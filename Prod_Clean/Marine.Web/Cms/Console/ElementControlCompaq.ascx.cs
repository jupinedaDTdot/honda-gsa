﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client;

namespace HondaCA.Web.Cms.Console
{
    public partial class ElementControlCompaq : System.Web.UI.UserControl
    {
        //private int uniqueNo;
        //public string uniqueName;
        
        public ContentElement ContentElement
        {
            get;
            set;
        }

        public string ElementName
        {
            get;
            set;
        }


        public string ReturnUrl
        {
            get;
            set;
        }


        public string MainUrl
        {
            get
            {
                return ((this.Page as CmsBasePage).MainUrl);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //get the ContentElement from the parent page by matching the ID
            CmsBasePage cmsPage = this.Page as CmsBasePage;
            if (cmsPage != null)
            {
                ContentPage contentPage = (this.Page as CmsBasePage).ContentPage;
                //if (contentPage.Elements.ContainsKey(this.ElementName))
                if (contentPage != null && this.ElementName != null && contentPage.Elements.ContainsKey(this.ElementName))
                {
                    this.ContentElement = contentPage.Elements[this.ElementName];
                }
                else
                    this.Visible = false;

                if (!cmsPage.IsStaging)
                    this.Visible = false;

                CmsBasePage basePage = this.Page as CmsBasePage;

                if (basePage.IsViewOnly)
                {
                    this.Visible = false;
                }

                //set Return url if page requested is diffreent from MainUrl ie ModelNav pages   
                if (ReturnUrl == null)
                    ReturnUrl = (basePage.MainUrl);
                else if (ReturnUrl.Trim() == string.Empty)
                    ReturnUrl = (basePage.MainUrl);
            }
        }
    }
}