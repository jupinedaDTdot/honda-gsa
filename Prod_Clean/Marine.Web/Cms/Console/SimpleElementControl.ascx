﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimpleElementControl.ascx.cs" 
Inherits="Navantis.Honda.CMS.Demo.Cms.Console.SimpleElementControl" %>

<%@ Register Assembly="Navantis.Honda.CMS.Core" Namespace="Navantis.Honda.CMS.Core"
    TagPrefix="cms" %>


    <% Navantis.Honda.CMS.Core.Service.IContentService contentService = Navantis.Honda.CMS.Core.Unity.Bootstrapper.Container.Resolve<Navantis.Honda.CMS.Core.Service.IContentService>(); %>
    <% string lastSavedContent = this.ContentElement.LastSavedBy; %> 
    <% string lastSubmittedContent = this.ContentElement.LastSubmittedBy; %>
    <%string currentUserName = (this.Page as Navantis.Honda.CMS.Demo.CmsBasePage).Username;  %>

    <% bool isEditor = (this.Page as Navantis.Honda.CMS.Demo.CmsBasePage).IsEditor; %>
    <% bool isAuthor = (this.Page as Navantis.Honda.CMS.Demo.CmsBasePage).IsAuthor; %>
    <% bool isCMSSystemAdmin = (this.Page as Navantis.Honda.CMS.Demo.CmsBasePage).IsCMSSystemAdmin; %>

<div id="cmsElementControl"  class="editable-pane">
    <div style="background-color: #fff;">
        <b>
            <%=this.ContentElement.ElementName%>
        </b> 

        <%if (ContentElement.ContentHistoryID <= 0)
        { %>
            Status:<%=this.ContentElement.Status%> by <%= this.ContentElement.OriginatorUser%> on <%=this.ContentElement.LastUpdated %>
        <%} %>
        <%else
            {%>
             <div style="background-color:Aqua;">
                Old Verion <%=this.ContentElement.LastUpdated%>
            </div>
        <%} %>
    </div>

    <%if (ContentElement.ContentHistoryID <= 0)
      { %>
    <div style="background-color: #fff;">
        
         <%--Edi Link--%>
         <%if (!this.ContentElement.Status.ToString().Equals(Navantis.Honda.CMS.Core.ContentStatus.Submitted.ToString()))
           {
               if ((this.ContentElement.Status.ToString().Equals(Navantis.Honda.CMS.Core.ContentStatus.Rejected.ToString()) &&
                   (currentUserName.Equals(lastSubmittedContent) || isCMSSystemAdmin))
            ||
                (this.ContentElement.Status.ToString().Equals(Navantis.Honda.CMS.Core.ContentStatus.Saved.ToString()) &&
                currentUserName.Equals(lastSavedContent))
                   ||
                   this.ContentElement.Status.ToString().Equals(Navantis.Honda.CMS.Core.ContentStatus.New.ToString())
                   ||
                   this.ContentElement.Status.ToString().Equals(Navantis.Honda.CMS.Core.ContentStatus.Approved.ToString())
                   )
               {
                   if (isEditor || isAuthor || isCMSSystemAdmin)
                   {%>
                       <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                            border-left-width: 0px; height: 16px; width: 16px;" title="Edit Content" alt="Edit Content"
                            src="/Cms/Images/Console/edit.gif" type="image" name="editElement" />&nbsp;
                        <a class="CommandButton" title="Edit Content" href="#" onclick="editCmsElement('Edit', '<%=this.ContentElement.MvcController %>','<%=this.ContentElement.ID %>','<%=this.ContentElement.Parent.MainURL %>');">
                            Edit</a>
                    <%}
               }
           }%>

          <%--View Link--%>
          <%if (this.ContentElement.Status.ToString().Equals(Navantis.Honda.CMS.Core.ContentStatus.Submitted.ToString()))
            {
                if (isEditor || isAuthor || isCMSSystemAdmin)
                {%>
                   <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
                        border-left-width: 0px; height: 16px; width: 16px;" title="View Content" alt="View Content"
                        src="/Cms/Images/Console/view.gif" type="image" name="viewElement" />&nbsp;
                    <a class="CommandButton" title="View Content" href="#" onclick="editCmsElement('Edit', '<%=this.ContentElement.MvcController %>','<%=this.ContentElement.ID %>','<%=this.ContentElement.Parent.MainURL %>');">
                        View</a>
                <%}

            }%>

    </div>
    <%} else {%>

   <%--Clear History Link--%>
        <% if (isEditor || isAuthor || isCMSSystemAdmin)
           { %>
            
            <input style="border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px;
            border-left-width: 0px; height: 16px; width: 16px;" title="Clear History" alt="Clear History"
            src="/Cms/Images/Console/ViewVersion.gif" type="image" name="viewVersions" />&nbsp;
            <a class="CommandButton" title="Clear History" href="#" onclick="editCmsElement('ClearVersionHistory','<%=this.ContentElement.MvcController %>','<%=ContentElement.ContentHistoryID %>','<%=this.ContentElement.Parent.MainURL %>');">
            Clear History</a>

         <%} %>
       <%} %>
</div>
