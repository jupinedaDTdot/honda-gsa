﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuthoringConsole.ascx.cs" Inherits="Navantis.Honda.CMS.Demo.Cms.AuthoringConsole" %>
<%@ Import Namespace="Navantis.Honda.CMS.Core.Common" %>
<%@ Import Namespace="Navantis.Honda.CMS.Core.Security" %>
<%@ Import Namespace="Navantis.Honda.CMS.Core.Service" %>
<%@ Import Namespace="Navantis.Honda.CMS.Demo" %>
<%@ Import Namespace="System.Configuration" %>

<%@ Register Assembly="Navantis.Honda.CMS.Core" Namespace="Navantis.Honda.CMS.Core" TagPrefix="cms" %>

<!-- common javasript functions have been moved to Site.Master -->

<% 
	CmsBasePage basePage = this.Page as CmsBasePage; 
	bool isEditor = basePage.IsEditor; 
	bool isAuthor = basePage.IsAuthor; 
	bool isCMSSystemAdmin = basePage.IsCMSSystemAdmin; 
	bool isViewOnly = basePage.IsViewOnly; 
	bool isAssociate = basePage.IsAssociate; 
	bool isMemberUserAdmin = basePage.IsMemberUserAdmin; 
	bool hasPermissionsToManageDET = basePage.HasPermissionsToManageDET; 
	bool hasPermissionsToManageHAC = basePage.HasPermissionsToManageHAC; 
	bool hasPermissionsToManageUsers = basePage.HasPermissionsToManageUsers; 
	bool commandButtonsVisible = ((!isViewOnly || hasPermissionsToManageDET || hasPermissionsToManageHAC || hasPermissionsToManageUsers) && !isAssociate);

    string enableCmsPublishConfig = ConfigurationManager.AppSettings["EnableCmsPublish"];
    bool enableCmsPublish = !string.IsNullOrWhiteSpace(enableCmsPublishConfig) && enableCmsPublishConfig.ToLower().Equals("true");

	PageMode pageViewMode = Navantis.Honda.CMS.Demo.Cms.SetPageMode.GetPageViewMode();
	String application = ConfigurationManager.AppSettings[Constants.ApplicationSettings.CmsApplication];
	//String cmsBuildNumber = Navantis.Honda.CMS.Core.Common.Constants.ApplicationSettings.CmsBuildNumber;
    //Frank: this could be different with constant value, Climax Media could upgrade the CMS /Content but keep using the older /Cms version
    //String cmsBuildNumber = "7.16";
    String cmsBuildNumber = Navantis.Honda.CMS.Core.Common.Constants.ApplicationSettings.CmsBuildNumber;
	String pageFunctionsTitle = (isEditor || isAuthor || isCMSSystemAdmin)? "Page Functions" : "&nbsp;";
	String urlPage = Server.UrlEncode( basePage.MainUrl );
	String urlCreateTrimPage = "/Content/cmsPage/CreateNewTrimPage/" + basePage.ContentPage.ID;
	String urlList = String.Concat( "/Content/cmsPage/Edit/", basePage.ContentPage.ID,"?",Constants.QueryStrings.returnUrl,"=",Server.UrlDecode(urlPage));
	String urlDisplay = String.Concat("/Content/cmsPage/Display/", basePage.ContentPage.ID, "?", Constants.QueryStrings.returnUrl, "=", Server.UrlDecode( urlPage ) );
	String urlDET = ConfigurationManager.AppSettings[Constants.ApplicationSettings.DETURL];
	String urlHAC = ConfigurationManager.AppSettings[Constants.ApplicationSettings.HACURL];
	String urlSurvey = ConfigurationManager.AppSettings[Constants.ApplicationSettings.SurveyURL];
	String urlUsers = ConfigurationManager.AppSettings[Constants.ApplicationSettings.UsersURL];
	String urlReports = String.Concat( ConfigurationManager.AppSettings[Constants.ApplicationSettings.ReportsURL],"?",Constants.QueryStrings.returnUrl,"=",Server.UrlDecode(urlPage));
	String urlPassword = String.Concat(ConfigurationManager.AppSettings[Constants.ApplicationSettings.PasswordURL], "?returnUrl=", this.ContentElement.Parent.MainURL );
	String settingsOnClick = String.Concat("editCmsElement('Edit', '",this.ContentElement.MvcController,"','",this.ContentElement.ID,"','",this.ContentElement.Parent.MainURL,"');");
	String newPageOnClick = String.Concat("newPage('",this.ContentElement.ID,"','",this.ContentElement.Parent.MainURL,"');");
	String membersOnClick = String.Concat("memberAdminPage('",ConfigurationManager.AppSettings[Constants.ApplicationSettings.OnlineUsersURL],"','", this.ContentElement.ID,"','",this.ContentElement.Parent.MainURL,"');");
	String submitOnClick = (this.ContentElement != null ? String.Concat("bulkSubmit('", this.ContentElement.Parent.ID,"','",this.ContentElement.Parent.MainURL,"');") : "");
	String approveOnClick = (this.ContentElement != null ? String.Concat("bulkApprove('", this.ContentElement.Parent.ID,"','",this.ContentElement.Parent.MainURL,"');") : "");
    String publishOnClick = (this.ContentElement != null ? String.Concat("bulkPublish('", this.ContentElement.Parent.ID, "','", this.ContentElement.Parent.MainURL, "');") : "");
    String cssSubmitApproveClass = (this.ContentElement != null ? "CommandButton" : "CommandButtonDisabled");
%>
 

<div id="cms_divControlPanel" class="ControlPanel">
    <div id="cms_divHeader">
        <div id="cms_divLogo">
			<img id="img_Logo" src="/Cms/Images/Console/Honda_logo_small.png" alt="" />
		</div>
		<div id="cms_divHeaderCommands" class="buttonContainer">
		<%if(isEditor || isAuthor || isCMSSystemAdmin) 
        {%>			
  			<span>
				<a id="cms_btnToggleViewMode" href="#" class="CommandButton" onclick="toggleViewMode();" title="Toggle Display Mode"><%=(pageViewMode == PageMode.Edit) ? "View" : "Edit"%> Mode</a> 
 			</span>                                                                               
			<span id="cms_ddlModelYears" class="SubHead">
				<asp:DropDownList ID="cms_ddlModelYearsList" runat="server" ToolTip="Select the model year for editing." />
			</span>
		<% }%>
		<%if((isEditor || isAuthor || isCMSSystemAdmin) && (application.Equals(Constants.ApplicationName.PowerEquipment) || application.Equals(Constants.ApplicationName.ATV) || application.Equals(Constants.ApplicationName.MotorCycle)))
        {%>
			<a id="btn_CreateTrimPage" class="CommandButton" href="<%=urlCreateTrimPage %>">
				Create New Trim Page
			</a>
        <% }%>
		<%if (this.IsDisplay )
		{%>
			<a id="btn_LivePage" class="CommandButton" href="#" onclick="loadLivePage();" title="View the Live page">Live Page</a>
		<%}
		else
		{%>
			<span id="notAccessible">Not publicly accessible</span>
		<%}%>
		</div>	
		<%if( commandButtonsVisible )
		{%>
		<div id="cms_divVisibility" class="buttonContainer">
			<a id="cms_cmdVisibility" href="" class="CommandButton">
				<img style="" id="cms_imgVisibility" title="Hide Command Buttons" alt="Minimize" src="/Cms/Images/Console/collapse.gif"  />
			</a>
        </div>
		<%}%>
		<div id="cms_divTitle">
            Honda CMS Authoring Console
		</div>
       <br class="clear"/>       
	</div>	
	<div id="cms_divHeaderInfo">
		<span id="buildNumber">
			<label>CMS Build #:</label>
			<%=cmsBuildNumber%>
		</span>
		<span id="mainUrl">
			<%=this.ContentElement.Parent.MainURL %> 
		</span>
		<span id="loginInfo">
		    <asp:LoginView ID="HeadLoginView" runat="server" EnableViewState="false">
                <AnonymousTemplate>
                    [ <a href="~/Account/Login.aspx" id="HeadLoginStatus" runat="server">Log In</a> ]
                </AnonymousTemplate>
                <LoggedInTemplate>
                    <span id="userName">
                        <%=(this.Page as CmsBasePage).Username%>
                    </span>
					<span id="logOut">
					[
                    <asp:LoginStatus ID="HeadLoginStatus" runat="server" LogoutAction="Redirect" LogoutText="Log Out"  OnLoggedOut="LoginStatus_LoggedOut" Visible="false"/>
                    <a href="/Asimo.Security/Logout.aspx?ReturnUrl=<%= Page.Server.UrlEncode(System.Web.HttpContext.Current.Request.RawUrl) %>" title="Log Off the Site">Log Out</a>
                    ]
					</span>
                    <%if ((this.Page as CmsBasePage).IsViewOnly || (this.Page as CmsBasePage).IsAssociate)
                    {%>
					<span id="changePassword">
                        [
                        <a href="<%=ConfigurationManager.AppSettings[Constants.ApplicationSettings.PasswordURL] %>" title="Change Password">
                            Change Password 
                        </a>
                        ]
					</span>
                    <%}%>
                </LoggedInTemplate>
            </asp:LoginView>
		</span>
		<br class="clear" />
	</div>
    <%if( commandButtonsVisible )
    {%>
        <div id="cms_divCommandButtons" class="fc buttonContainer">
			<div id="cms_divPageFunctions">
				<span id="cms_lblPageFunctions" class="SubHead"><%=pageFunctionsTitle %></span>
				<div id="cms_divPageFunctionButtons">
                <% if (isEditor || isAuthor || isCMSSystemAdmin)
                          {%>
					        <a id="btn_Edit" class="CommandButton" href="#" 
						        onclick="<%=settingsOnClick %>"  title="Current Page Settings">
						        <img id="imgEdit" alt="Settings" src="/Cms/Images/Console/iconbar_settings.png" title="Current Page Settings" />
						        <span>Settings</span>
					        </a>
				        <%} %>

				<%if (!enableCmsPublish)
                      {
                          if (isEditor || isAuthor || isCMSSystemAdmin)
                          {%>

					        <a id="btn_Submit" class="<%=cssSubmitApproveClass %>" href="#" onclick="<%=submitOnClick %>"  title="Bulk Submit">
						        <img id="img_Submit" alt="Submit" src="/Cms/Images/Console/iconbar_submit.png" title="Bulk Submit"  />
						        <span>Submit</span>
					        </a>
				        <%}
                          if (isEditor || isCMSSystemAdmin)
                          {%>
					        <a id="btn_Approve" class="<%=cssSubmitApproveClass %>" href="#" onclick="<%=approveOnClick %>"  title="Bulk Approve">
						        <img id="img_Approve" title="Bulk Approve" alt="Approve" src="/Cms/Images/Console/iconbar_approve.png" />
						        <span>Approve</span>
					        </a>
				        <%}
                      }
                      else { %>
                        <a id="A1" class="<%=cssSubmitApproveClass %>" href="#" onclick="<%=publishOnClick %>"  title="Bulk Publish">
						        <img id="img_Approve" alt="Publish" src="/Cms/Images/Console/iconbar_approve.png" title="Bulk Publish"  />
						        <span>Publish</span>
					        </a>
                      <%}%>
				<%if( (isEditor || isAuthor || isCMSSystemAdmin) && application.Equals( Constants.ApplicationName.HondaNews ) )
				{%>
					<a id="btn_New" class="CommandButton" href="#" onclick="<%=newPageOnClick %>" title="Create new CMS basePage" >
						<img  id="img_New" alt="New Page" src="/Cms/Images/Console/iconbar_newpage.png" title="Create new CMS basePage" />
						<span>New</span>
					</a>
				<%}%>
				<%if( isCMSSystemAdmin )
				{%>					
					<a id="btn_List" class="CommandButton" href="<%=urlList %>"  title="Site Current List" >
						<img  id="img_List" alt="List" src="/Cms/Images/Console/iconbar_list.png"  title="Site Current List"/>
						<span>List</span>
					</a>	
					<a id="btn_Display" class="CommandButton" href="<%=urlDisplay %>" title="Site Current Display">
						<img  id="img_Display" alt="Display" src="/Cms/Images/Console/iconbar_display.png" title="Site Current Display" />
						<span>Display</span>
					</a>			
				<%}%>		
				</div>
			</div>
			<div id="cms_divCommonTasks">
				<span id="cms_lblCommonTasks" class="SubHead">Common Tasks</span>
				<div id="cms_divCommonTaskButtons">
                <%if( hasPermissionsToManageDET )
                {%>
                    <a id="btn_DET" class="CommandButton" href="<%=urlDET %>" title="Manage DET" >
						<img  id="cms_imgDET" alt="DET" src="/Cms/Images/Console/iconbar_DET.png" title="Manage DET" />
						<span>DET</span>
					</a>
				<%}%> 
				<%if (hasPermissionsToManageHAC )
                {%>
                    <a id="btn_HAC" class="CommandButton" href="<%=urlHAC %>" title="Manage HAC" >
                        <img id="img_HAC" alt="HAC" src="/Cms/Images/Console/iconbar_HAC.png"  title="Manage HAC" />
						<span>HAC</span>
                    </a>
                <%}%>
                <%if( isCMSSystemAdmin )
				{%> 					
					<a id="btn_Surveys" class="CommandButton" href="<%=urlSurvey %>"  title="Manage Surveys"  >
						<img id="img_Surveys" alt="Surveys" src="/Cms/Images/Console/iconbar_survey.png" title="Manage Surveys"  />
						<span>Surveys</span>
					</a>
				<%}%>
                <%if( hasPermissionsToManageUsers )
				{%>
					<a id="btn_Users" class="CommandButton" href="<%=urlUsers%>" title="Manage Users">
						<img id="img_Users" alt="Users" src="/Cms/Images/Console/iconbar_users.png" title="Manage Users" />
						<span>Users</span>
					</a>
				<%}%>
                <%if( (isMemberUserAdmin || isCMSSystemAdmin) && application.Equals( Constants.ApplicationName.HondaNews ) )
				{%> 					
					<a id="btn_Members" class="CommandButton" href="#" onclick="<%=membersOnClick %>" title="Manage Online Users" >
						<img id="img_Members" alt="Members" src="/Cms/Images/Console/iconbar_users.png" title="Manage Online Users"/>
						<span>Members</span>
					</a>
				<%}%>
				<%if( isCMSSystemAdmin )
				{%> 					
					<a id="btn_Reports" class="CommandButton" href="<%=urlReports %>"  title="View Reports" >
						<img  id="img_Reports" alt="Reports" src="/Cms/Images/Console/iconbar_reports.png" title="View Reports" />
						<span>Reports</span>
					</a>	
				<%}%>		
					<a id="btn_Password" class="CommandButton" href="<%=urlPassword %>"  title="Change Password" >
						<img  id="img_Password" alt="Password" src="/Cms/Images/Console/iconbar_changepswd.png" title="Change Password" />
						<span>Password</span>
					</a>			
			  </div>
			</div>
			<br class="clear" />
		</div>			
    <%}%>

        <div id="publishTicker" style="display: none;">
        <h1>
            Publish in progress<span id="loadingDots"></span></h1>
        <div id="tickerDetailsPopup" style="display: none;">
            <span id="recentPublish" style="display: none;" class="clearboth">No active publish processes right now, recently published schedules are listed below.</span>
            <div id="tickerDetails" style="display: none;">
            </div>
            <div id="pt_loading">
                <span id="pt_loadingGif">
                    <img src="/Cms/Images/Console/loading.gif" alt=""/></span></div>
        </div>
    </div>

 </div>
  <script type="text/javascript">
      function bulkPublish(pageID, pageUrl) {
          return modalDialog('/Content/Content/BulkPublish?pageID=' + pageID + '&type=ByPage&math=' + Math.random(), pageUrl);
      }
 </script>
