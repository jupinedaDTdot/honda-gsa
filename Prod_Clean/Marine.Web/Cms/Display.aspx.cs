﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Navantis.Honda.CMS.Client;
using Navantis.Honda.CMS.Core;
using Navantis.Honda.CMS.Client.Elements;
using Navantis.Honda.CMS.Core.Service;
using Navantis.Honda.CMS.Core.Unity;
using Navantis.Honda.CMS.Core.Model;

using Microsoft.Practices.Unity;


namespace Navantis.Honda.CMS.Demo
{
    /// <summary>
    /// This page will be used in production to verify CMS pages
    /// </summary>
    public partial class Display : CmsBasePage
    {
        private MediaService mediaService;

        protected override void OnInit(EventArgs e)
        {
            string url = this.Request.QueryString["url"];
            if (!string.IsNullOrEmpty(url))
            {
                this.MainUrl = url;
            }
            else
            {
                this.MainUrl = this.Request.RawUrl; // /Cms/Display.aspx
            }

            mediaService = Bootstrapper.Container.Resolve<MediaService>();

            base.OnInit(e);
        }

        protected string GenerateMediaRenderingHtml(string imagePath)
        {
            string html = string.Empty;

            if (!string.IsNullOrEmpty(imagePath))
            {
                string[] imageTypeExtensions = { ".jpg", ".png", ".bmp", ".gif", ".jpeg" };
                string ext = System.IO.Path.GetExtension(imagePath).ToLower();
                if(imageTypeExtensions.Contains(ext))                    
                    html = string.Format("<img src='{0}' />", imagePath); //display
                else
                    html = string.Format("<a href='{0}' >{0}</a>", imagePath); //download 
            }

            return (html);
        }

        //<div><i>Display:</i><%=contentElement.ContentElement.Display %>&nbsp;|&nbsp;<i>Width:</i><%=contentElement.ContentElement.Width%>&nbsp;|&nbsp;<i>Height:</i><%=contentElement.ContentElement.Height %>&nbsp;|&nbsp;<i>ElementType:</i><%=contentElement.ContentElement.ElementTypeName %>&nbsp;|&nbsp;<i>ID:</i><%=contentElement.ContentElement.ID %><hr /></div>
        protected string GenerateContentElementRenderingHtml(ContentElement contentElement)
        {

            return(
                string.Format("<div><i>Display:</i>{0}&nbsp;|&nbsp;<i>Width:</i>{1}&nbsp;|&nbsp;<i>Height:</i>{2}&nbsp;|&nbsp;<i>ElementType:</i>{3}&nbsp;|&nbsp;<i>ID:</i>{4}<hr /></div>", 
            contentElement.Display,
            contentElement.Width,
            contentElement.Height,
            contentElement.ElementTypeName,
            contentElement.ID
            ));
        }

        protected List<string> GetCmsMediaPaths(ContentElement contentElement)
        {
            var mediaPaths = mediaService.GetMediaPaths(contentElement.ID);

            for (int i = 0; i < mediaPaths.Count; i++)
                mediaPaths[i].Replace("~", "/Content");

            return mediaPaths;
        }
    }
}
