﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Navantis.Honda.CMS.Client;
using Navantis.Honda.CMS.Core;
using Navantis.Honda.CMS.Client.Elements;

namespace Navantis.Honda.CMS.Demo
{
    public partial class TestF : CmsBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            //this is to test all the element types
            string url = this.Request.QueryString["url"];
            if (!string.IsNullOrEmpty(url))
            {
                this.MainUrl = url;
            }
            else
            {
                this.MainUrl = "/Cms/TestF.aspx";
            }

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                //submitGenericForm
                string whichButton = this.Request.Form["submitGenericForm"];
                if (!string.IsNullOrEmpty(whichButton))
                {
                    //add a generic form response
                    if (this.ContentPage.Elements.ContainsKey("GenericForm"))
                    {
                        GenericForm genericForm = this.ContentPage.Elements["GenericForm"].DeserializeElementObject() as GenericForm;
                        if (genericForm != null && genericForm.Items != null)
                        {
                            //get the value from the Request form
                            foreach (GenericFormField field in genericForm.Items)
                            {
                                field.Value = this.Request.Form[field.Name];
                            }
                            //insert into cmsGenericFormResponse and cmsGenericFormResponseItem table by calling the helper method
                            Navantis.Honda.CMS.Client.GenericFormHelper.CreateGenericFormResponse(genericForm.ContentElement.ID, genericForm.Fields);
                        }
                    }
                }
            }
        }
    }

}