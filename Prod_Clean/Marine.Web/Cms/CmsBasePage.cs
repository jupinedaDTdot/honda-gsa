﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using HondaCA.Common;
using HondaCA.Service.Common;
using Navantis.Honda.CMS.Client;
using Navantis.Honda.CMS.Core;
using Navantis.Honda.CMS.Client.Elements;
using Navantis.Honda.CMS.Core.Service;
using Navantis.Honda.CMS.Core.Common;
using Navantis.Honda.CMS.Core.Security;
using Constants = Navantis.Honda.CMS.Core.Common.Constants;

namespace Navantis.Honda.CMS.Demo
{
	public enum PageMode
	{
		Edit = 0,
		View = 1
	}

	public class CmsBasePage : System.Web.UI.Page
	{
		PageSetting _pageSetting = null;
		ContentPage _globalPage = null;
		bool _isEditor = false;
		bool _isAuthor = false;
		bool _isCMSSystemAdmin = false;
		bool _isViewer = false;
		bool _isAssociate = false;
		bool _isMemberUserAdmin = false;
		string _username = string.Empty;
		Dictionary<string, List<ContentPage>> _relatedPages = null;

		public ContentPage ContentPage { get; set; }
		public string MainUrl { get; set; }
		public bool IsStaging { get; set; }
		public bool IsMasterAdmin { get; set; }
		public bool HasPermissionsToManageDET { get; set; }
		public bool HasPermissionsToManageHAC { get; set; }
		public bool HasPermissionsToManageUsers { get; set; }
		public List<string> GroupFilter { get; set; }
		public bool IsNotCMSPage { get; set; }

		public PageSetting PageSetting
		{
			get
			{
				if (!IsNotCMSPage && _pageSetting == null)
					_pageSetting = (this.ContentPage.Elements["PageSetting"] as ContentElement).DeserializeElementObject() as PageSetting;

				return _pageSetting;
			}
		}

		public ContentPage GlobalPage
		{
			get
			{
				if (!IsNotCMSPage && _globalPage == null)
				{
					string globalPageUrl = ConfigurationManager.AppSettings[CommonService.IsEnglish(this.ContentPage.Language)
																				? Constants.ApplicationSettings.CmsApplicationGlobalPageEn
																				: Constants.ApplicationSettings.CmsApplicationGlobalPageFr];

					string cmsApplication = ConfigurationManager.AppSettings[Constants.ApplicationSettings.CmsApplication];

					if (this.IsStaging)
					{
						_globalPage = ContentPage.GetPage(globalPageUrl, cmsApplication, this.IsStaging, Username, null, -1);
					}
					else
					{
						//get from cache if in there, or build the cache and cache for an hour
						// generate cache keys
						string cacheKey = string.Concat(globalPageUrl, cmsApplication);
						if (HttpContext.Current.Cache[cacheKey] == null)
						{
							_globalPage = ContentPage.GetPage(globalPageUrl, cmsApplication, this.IsStaging, Username, null, -1);

							// add to cache
							//cache for 1 hours
							int cacheDuration = Convert.ToInt32(ConfigurationManager.AppSettings[Constants.ApplicationSettings.CmsApplicationGlobalPageCacheDuration]);
							HttpContext.Current.Cache.Insert(cacheKey, _globalPage, null, DateTime.Now.AddSeconds(cacheDuration), System.Web.Caching.Cache.NoSlidingExpiration);
						}
						else
						{
							// get from cache
							_globalPage = HttpContext.Current.Cache[cacheKey] as ContentPage;
						}
					}
				}
				return _globalPage;
			}
		}

		public bool IsEditor
		{
			get { return _isEditor; }
			set { _isEditor = value; }
		}

		public bool IsAuthor
		{
			get { return _isAuthor; }
			set { _isAuthor = value; }
		}

		public bool IsAssociate
		{
			get { return _isAssociate; }
			set { _isAssociate = value; }
		}

		public bool IsCMSSystemAdmin
		{
			get { return _isCMSSystemAdmin; }
			set { _isCMSSystemAdmin = value; }
		}

		public bool IsMemberUserAdmin
		{
			get { return _isMemberUserAdmin; }
			set { _isMemberUserAdmin = value; }
		}

		public bool IsViewOnly
		{
			get { return _isViewer && !(_isCMSSystemAdmin || _isEditor || _isAuthor); }
		}

		public bool IsAssociateOnly
		{
			get { return _isAssociate && !(_isCMSSystemAdmin || _isEditor || _isAuthor); }
		}

		public bool IsViewer
		{
			get { return _isViewer; }
			set { _isViewer = value; }
		}

		public string Username
		{
			get { return _username; }
			set { _username = value; }
		}

		public string UserIP
		{
			get
			{
				if (IsStaging)
					return ConfigurationManager.AppSettings["TestUserIP"] ?? Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
				else
					return Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
			}
		}

		public bool IsQuebecPricing
		{
			get
			{
				bool IsQuebec = false;
				var currentQubec = Request.Cookies.Get("quebecregioncookie");
				string quebecCookieValue = string.Empty;
				if (currentQubec != null)
					quebecCookieValue = currentQubec.Value;

				if (string.IsNullOrEmpty(quebecCookieValue))
				{
					var ls = new LocationService();
					string provCode = ls.getProvinceCodeByIP(UserIP, (this.ContentPage == null || this.ContentPage.Language == "en") ? Language.English : Language.French);
					var h = new HttpCookie("quebecregioncookie");
					h.Value = (provCode == "QC") ? "true" : "false";
					quebecCookieValue = h.Value;
					Response.Cookies.Add(h);
				}
				bool.TryParse(quebecCookieValue, out IsQuebec);
				return IsQuebec;
			}
		}

		// BRAD: this needs to be refactored as it's checking the same two conditions twice
		public Dictionary<string, List<ContentPage>> RelatedPages
		{
			get
			{
				if (!IsNotCMSPage)
				{
					if (_relatedPages == null)
					{
						if (!IsNotCMSPage)
						{
							//this is CMS page
							//get the related pages based on PageSetting.RelatedPages
							if (this.PageSetting.RelatedPages != null && this.PageSetting.RelatedPages.Count > 0)
								_relatedPages = this.ContentPage.GetRelatedPages(this.PageSetting, this.IsStaging);
						}
						//mark this as initialized by setting it to empty
						if (_relatedPages == null)
							_relatedPages = new Dictionary<string, List<ContentPage>>();
					}
				}
				return _relatedPages;
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			//check whether this is staging by reading a web.config setting
			this.IsStaging = Convert.ToBoolean(ConfigurationManager.AppSettings["IsStaging"]);

			if (!IsNotCMSPage)
			{
				string cmsApplication = ConfigurationManager.AppSettings[Constants.ApplicationSettings.CmsApplication];

				//Check if user has "viewer" role as minimum requirement to acces the page if in staging.
				if (this.IsStaging)
				{
					//this needs to be recoded for mobile and other new sites
					ApplicationType appType = Utility.MapApplicationNameToApplicationType(cmsApplication);
					CMSAuthorization cmsAuthorization = new CMSAuthorization();
					_isCMSSystemAdmin = cmsAuthorization.IsUserInRole(appType, this.Page.User, this.MainUrl, CmsRoleType.CMSSystemAdmin);

					//if _isCMSSystemAdmin then all the roles are applied
					_isEditor = _isCMSSystemAdmin || cmsAuthorization.IsUserInRole(appType, this.Page.User, this.MainUrl, CmsRoleType.Editor);
					_isAuthor = _isCMSSystemAdmin || cmsAuthorization.IsUserInRole(appType, this.Page.User, this.MainUrl, CmsRoleType.Author);
					_isViewer = _isCMSSystemAdmin || cmsAuthorization.IsUserInRole(appType, this.Page.User, this.MainUrl, CmsRoleType.Viewer);
					_isMemberUserAdmin = _isCMSSystemAdmin || cmsAuthorization.IsUserInRole(appType, this.Page.User, this.MainUrl, CmsRoleType.MemberUserAdmin);

					// set _isAssociate for any user except CMSsystemAdmin
					_isAssociate = cmsAuthorization.IsUserInRole(appType, this.Page.User, this.MainUrl, CmsRoleType.Associate);
					if (_isCMSSystemAdmin)
						_isAssociate = false;

					if (!(IsCMSSystemAdmin || IsEditor || IsAuthor || IsViewer || IsAssociate))
						Response.Redirect("~/Cms/AccessDenied.aspx", true);

					//set view mode for Viewer
					if (this.IsViewOnly)
						HttpContext.Current.Session["PageMode"] = PageMode.View; //it must be viewer then

					this.IsMasterAdmin = cmsAuthorization.IsMasterAdmin(this.Page.User);
					if (this.IsMasterAdmin)
					{
						this.HasPermissionsToManageDET = true;
						this.HasPermissionsToManageHAC = true;
						this.HasPermissionsToManageUsers = true;
					}
					else
					{
						this.HasPermissionsToManageDET = cmsAuthorization.HasAccessToManageDET(appType, this.Page.User);
						this.HasPermissionsToManageHAC = cmsAuthorization.HasAccessToManageHAC(appType, this.Page.User);
						this.HasPermissionsToManageUsers = cmsAuthorization.HasAccessToManageUsers(appType, this.Page.User);
					}
				}

				if (this.IsStaging)
					_username = CmsHttpContext.Current.CmsUser.User_Name;

				// Get selected model year from  cookie
				int productID = -1;
				int? trimID = null;
				string language = null;
				string mainUrlInCookie = null;
				if (Request.Cookies["CMS_APPLICATION"] != null && Request.Cookies["CMS_APPLICATION"].Value.Equals(cmsApplication, StringComparison.OrdinalIgnoreCase))
				{
					if (Request.Cookies["CMS_PRODUCT_ID"] != null)
					{
						productID = Int32.Parse(Request.Cookies["CMS_PRODUCT_ID"].Value);

						if (Request.Cookies["CMS_TRIM_ID"] != null && !string.IsNullOrEmpty(Request.Cookies["CMS_TRIM_ID"].Value))
							trimID = Int32.Parse(Request.Cookies["CMS_TRIM_ID"].Value);
						if (Request.Cookies["CMS_MAIN_URL"] != null)
							mainUrlInCookie = Request.Cookies["CMS_MAIN_URL"].Value;
						if (Request.Cookies["CMS_LANGUAGE"] != null)
							language = Request.Cookies["CMS_LANGUAGE"].Value;

						//ensure the mainUrlInCookie matches with the this.MainUrl
						if (!this.MainUrl.Equals(mainUrlInCookie, StringComparison.OrdinalIgnoreCase))
						{
							//this product id in cookie is not for this URL
							productID = -1;
							trimID = null;
						}
					}
				}
				//use the following to filter it
				this.ContentPage = ContentPage.GetPage(this.MainUrl, cmsApplication, this.IsStaging, Username, this.GroupFilter, GetVersionHistoryIDs(), productID, trimID);
			}
		}

		private List<string> GetVersionHistoryIDs()
		{
			HttpCookie cookie = Request.Cookies.Get(Constants.CookieKeys.VersionHistoryID);

			if (cookie == null || string.IsNullOrEmpty(cookie.Value))
				return null;

			List<string> versionHistoryIDs = new List<string>();
			versionHistoryIDs.AddRange(cookie.Value.Split(','));

			return versionHistoryIDs;
		}
	}
}