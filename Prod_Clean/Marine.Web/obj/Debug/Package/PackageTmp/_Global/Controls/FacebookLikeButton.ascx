﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FacebookLikeButton.ascx.cs" Inherits="Marine.Web._Global.Controls.FacebookLikeButton" %>

<div class="fr facebook-container">
    <iframe src="<%= HttpContext.Current.Request.Url.Scheme %>://www.facebook.com/plugins/like.php?href=<%= HttpUtility.UrlEncode("http://" + HondaCA.Common.Global.ProductionDomains[0] + Request.RawUrl.ToString()) %>&amp;layout=button_count&amp;show_faces=false&amp;width=90&amp;action=like&amp;colorscheme=light&amp;height=40&amp;font=lucida+grande" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:26px;" allowTransparency="true"></iframe>
</div>