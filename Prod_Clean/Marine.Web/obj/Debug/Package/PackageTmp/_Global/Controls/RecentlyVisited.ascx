﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecentlyVisited.ascx.cs" Inherits="Marine.Web._Global.Controls.RecentlyVisited" %>

<asp:Panel ID="PanelRecentlyVisited" runat="server">
<div class="content_section nb recently_viewed">
  <h2><img src="/_Global/img/content/products/icon_recent.gif" alt="" /><asp:Literal ID="LiteralRecentlyViewd" runat="server"></asp:Literal></h2>
  <div class="fc bottom_feature_inner">
    <ul class="fc fl recently_viewed_product">
      <asp:Literal ID="LiteralRecentlyVisited" runat="server"></asp:Literal>
      </ul>
    <div class="fr recently_viewed_compare">
      <p class="np compare-these"><strong><asp:Literal ID="LiteralCompareThesePE" runat="server"></asp:Literal></strong></p>
      <asp:HyperLink ID="HyperLinkCompare" runat="server" CssClass="btn primary"></asp:HyperLink>
    </div>
  </div>
</div>

</asp:Panel>