﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddToCompare.ascx.cs" Inherits="Marine.Web._Global.Controls.AddToCompare" %>
<div class="nb content_section compare" id="CompareWidget">
    <h2><%=ResourceManager.GetString("txtCompareModel2")%></h2>
    <div class="fc bottom_feature_inner">
        <ul class="fc fl product" runat="server" id="compareProductList">
            <li class="fc fl pf first product-item">
                <div class="fl list-steps">1</div>

                <div class="fl product-item-inner">
                    <div class="selector">
                        <div class="add-model"><%=ResourceManager.GetString("txtAddCompareModel")%></div>
                        <div class="buttons">
                        <a href="javascript:void(0)" class="btn secondary"
                                        data-popup-class="model-selector-right"
                                        data-popup-content-url="<%= ModelSelectorUrl %>"
                                        data-position-my="left top"
                                        data-position-at="left bottom"
                                        data-position-offset="10 4"
                                        data-popup-hanger="true"
                                        data-compare-behavior="add"
                                        property="act:popup"><span><%=ResourceManager.GetString("txtSelectModel1")%></span></a>
                        </div>
                    </div>
                    <div>
                        <h3 class="price-heading"></h3>
                        <a href="javascript:void(0)" class="fl pf btn-remove dn"></a>
                        <div class="clr"></div>
                    </div>
					<div class="tag-coming-soon dn">
                        <div class="fc final price">
                            <strong class="coming-soon"><%= ResourceManager.GetString("txtComingSoon") %></strong>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                    </div>
                    <div class="tag-has-discount dn">
                        <div class="fc original price">
                            <del><span class="strike"></span><strong class="tag-price"></strong></del>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                        <div class="fc special price">
                            <strong class="tag-discount"></strong>
                            <span class="tag-text-discount"><%=ResourceManager.GetString("Discount")%></span>
                        </div>
                        <div class="fc final price">
                            <strong class="tag-final-price"></strong>
                            <span class="tag-text-final-price"><%=ResourceManager.GetString("YourPrice")%></span>
                        </div>
                    </div>
                    <div class="tag-no-discount dn">
                        <div class="fc final price">
                            <strong class="tag-price"></strong>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                    </div>
                </div>
            </li>

            <li class="fc fl pf product-item">
                <div class="fl list-steps">2</div>

                <div class="fl product-item-inner">
                    <div class="selector">
                        <div class="add-model"><%=ResourceManager.GetString("txtAddCompareModel")%></div>
                        <div class="buttons">
                        <a href="javascript:void(0)" class="btn secondary"
                                        data-popup-class="model-selector-right"
                                        data-popup-content-url="<%= ModelSelectorUrl %>"
                                        data-position-my="left top"
                                        data-position-at="left bottom"
                                        data-position-offset="10 4"
                                        data-popup-hanger="true"
                                        data-compare-behavior="add"
                                        property="act:popup"><span><%=ResourceManager.GetString("txtSelectModel1")%></span></a>
                        </div>
                    </div>
                    <div>
                        <h3 class="price-heading"></h3>
                        <a href="javascript:void(0)" class="fl pf btn-remove dn"></a>
                        <div class="clr"></div>
                    </div>
					<div class="tag-coming-soon dn">
                        <div class="fc final price">
                            <strong class="coming-soon"><%= ResourceManager.GetString("txtComingSoon") %></strong>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                    </div>
                    <div class="tag-has-discount dn">
                        <div class="fc original price">
                            <del><span class="strike"></span><strong class="tag-price"></strong></del>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                        <div class="fc special price">
                            <strong class="tag-discount"></strong>
                            <span class="tag-text-discount"><%=ResourceManager.GetString("Discount")%></span>
                        </div>
                        <div class="fc final price">
                            <strong class="tag-final-price"></strong>
                            <span class="tag-text-final-price"><%=ResourceManager.GetString("YourPrice")%></span>
                        </div>
                    </div>
                    <div class="tag-no-discount dn">
                        <div class="fc final price">
                            <strong class="tag-price"></strong>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                    </div>
                </div>
            </li>

            <li class="fc fl pf product-item">
                <div class="fl list-steps">3</div>

                <div class="fl product-item-inner">
                    <div class="selector">
                        <div class="add-model"><%=ResourceManager.GetString("txtAddCompareModel")%></div>
                        <div class="buttons">
                        <a href="javascript:void(0)" class="btn secondary"
                                        data-popup-class="model-selector"
                                        data-popup-content-url="<%= ModelSelectorUrl %>"
                                        data-position-my="right top"
                                        data-position-at="right bottom"
                                        data-position-offset="10 4"
                                        data-popup-hanger="true"
                                        data-compare-behavior="add"
                                        property="act:popup"><span><%=ResourceManager.GetString("txtSelectModel1")%></span></a>
                        </div>
                    </div>
                    <div>
                        <h3 class="price-heading"></h3>
                        <a href="javascript:void(0)" class="fl pf btn-remove dn"></a>
                        <div class="clr"></div>
                    </div>
					<div class="tag-coming-soon dn">
                        <div class="fc final price">
                            <strong class="coming-soon"><%= ResourceManager.GetString("txtComingSoon") %></strong>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                    </div>
                    <div class="tag-has-discount dn">
                        <div class="fc original price">
                            <del><span class="strike"></span><strong class="tag-price"></strong></del>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                        <div class="fc special price">
                            <strong class="tag-discount"></strong>
                            <span class="tag-text-discount"><%=ResourceManager.GetString("Discount")%></span>
                        </div>
                        <div class="fc final price">
                            <strong class="tag-final-price"></strong>
                            <span class="tag-text-final-price"><%=ResourceManager.GetString("YourPrice")%></span>
                        </div>
                    </div>
                    <div class="tag-no-discount dn">
                        <div class="fc final price">
                            <strong class="tag-price"></strong>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                    </div>
                </div>
            </li>

            <li class="fc fl pf product-item">
                <div class="fl pf list-steps">4</div>

                <div class="fl product-item-inner">
                    <div class="selector">
                        <div class="add-model"><%=ResourceManager.GetString("txtAddCompareModel")%></div>
                        <div class="buttons">
                        <a href="javascript:void(0)" class="btn secondary"
                                        data-popup-class="model-selector"
                                        data-popup-content-url="<%= ModelSelectorUrl %>"
                                        data-position-my="right top"
                                        data-position-at="right bottom"
                                        data-position-offset="10 4"
                                        data-popup-hanger="true"
                                        data-compare-behavior="add"
                                        property="act:popup"><span><%=ResourceManager.GetString("txtSelectModel1")%></span></a>
                        </div>
                    </div>
                    <div>
                        <h3 class="price-heading"></h3>
                        <a href="javascript:void(0)" class="fl pf btn-remove dn"></a>
                        <div class="clr"></div>
                    </div>
					<div class="tag-coming-soon dn">
                        <div class="fc final price">
                            <strong class="coming-soon"><%= ResourceManager.GetString("txtComingSoon") %></strong>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                    </div>
                    <div class="tag-has-discount dn">
                        <div class="fc original price">
                            <del><span class="strike"></span><strong class="tag-price"></strong></del>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                        <div class="fc special price">
                            <strong class="tag-discount"></strong>
                            <span class="tag-text-discount"><%=ResourceManager.GetString("Discount")%></span>
                        </div>
                        <div class="fc final price">
                            <strong class="tag-final-price"></strong>
                            <span class="tag-text-final-price"><%=ResourceManager.GetString("YourPrice")%></span>
                        </div>
                    </div>
                    <div class="tag-no-discount dn">
                        <div class="fc final price">
                            <strong class="tag-price"></strong>
                            <span class="tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                        </div>
                    </div>
                </div>
            </li>

        </ul>
        <div class="fl recently_viewed_compare">
			<p class="np nm compare-these"><strong><%=ResourceManager.GetString("txtCompareModel3")%></strong></p>
			<a href="<%=CompareUrl %>" class="btn primary"><span><%=ResourceManager.GetString("Compare")%></span></a>
		</div>
    </div>
</div>     
<div id="compare-add-error" class="dn">
	<div>
		<%= ResourceManager.GetString("CompareWidgetError") %>
	</div>
</div>
<div class="error-message-add dn"><%= ResourceManager.GetString("CompareErrorMessage") %></div>