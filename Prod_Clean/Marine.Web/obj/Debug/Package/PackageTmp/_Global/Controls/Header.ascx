﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="Marine.Web._Global.Controls.Header" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<div class="wrapper nmb nmt">
    <div id="header">
        <ul id="language" class="fc">
    	    <li class="fr first"><a runat="server" id="LanguageUrl" href=""> <asp:Literal runat ="server" ID="LanguageName" /> </a></li>
          <li class="fr"><a runat="server" id="SecondLanguageURL" href=""> <asp:Literal runat ="server" ID="SecondLanguageName" /> </a></li>
    	    <li class="fr"><a href="<%=ResourceManager.GetString("urlBackToHonda")%>"><%=ResourceManager.GetString("txtBackToHonda")%></a></li>
	        <li class="fr"><a href="<%=ResourceManager.GetString("urlHondaCanada")%>" target="_blank"><%=ResourceManager.GetString("txtHondaCanada")%></a></li>
	    </ul>
	    <div class="fl">
		    <asp:HyperLink ID="hplLogo" runat="server" CssClass="header_logo" />
            <%--<a id="logo" href="#"></a> May need to change the style class defined after #logo since that id cannot be kept in above control--%>
	    </div>
	    <div class="fr" id="sub_header">
		    <div id="search" class="dd_button fr">
			    <input type="text" name="search" value="" placeholder="<%=ResourceManager.GetString("txtSearch")%>" />
			    <a href="#" class="clear dn"></a>
		    </div>
		    <ul id="secondary_nav" class="fr">
			    <asp:Literal ID="litDealerLocatorLink" runat="server" />
			    <asp:Literal ID="litLeaseAndFinanceLink" runat="server" />
                <asp:Literal ID="litAccessoriesLink" runat="server" Visible="false" />
                <asp:Literal ID="litPartsServicesLink" runat="server" Visible="false" />
                <asp:Literal ID="litSafetyLink" runat="server" />
                <asp:Literal ID="litNewsEventsLink" runat="server" />
		    </ul>
		    <div class="clrfix"></div>
	    </div>
	    <div class="clrfix"></div>
	    <div id="main_menu">
		    <ul id="primary_nav">
                <asp:Literal ID="ltPrimaryMenu" runat="server"></asp:Literal>			    
		    </ul>
		    <div class="clrfix"></div>
	    </div>
        <div class="landing-heading">
            <span class="commercial-products"><a href="<%=ResourceManager.GetString("txtCommercialProductsURL")%>"><%=ResourceManager.GetString("txtCommercialProducts")%></a></span>        
        </div>
    </div>
</div>
<asp:Label runat="server" ID="listModel"></asp:Label>
<!-- begin: dropdown of car box -->

<%--<div id="cars_list" class="dn">
	<div class="cars_list_container">
		<div class="vehicle_list">
			<asp:Repeater ID="repModelCars"  runat="server" >OnItemDataBound="repModels_ItemDataBound"
				<ItemTemplate>
				<span class="vehicle_btn">
					<asp:HyperLink ID="hypModelImage" runat="server" CssClass="vehicle_tout">
                        <asp:Literal ID="litModelImage" runat="server"></asp:Literal>
                    </asp:HyperLink>
                    <asp:HyperLink ID="hypModel" runat="server">
                        <span class="h3"><asp:Literal ID="litModelName" runat="server"></asp:Literal></span>
                        <span class="price"><asp:Literal ID="litModeldescription" runat="server"></asp:Literal></span>
                    </asp:HyperLink>
                </span>
				</ItemTemplate>
			</asp:Repeater>
			<asp:Literal ID="litFutureVehicle" runat="server" > </asp:Literal>
			<span id="Span1" class="vehicle_btn" runat="server" visible="true">				            
				   <a class="vehicle_tout" href="<%//= (this.isAbsolute ? this.domain : "") + this.ResourceManager.GetString("FutureVehicleUrl")%>">
				    <img src="/_Global/img/layout/btn_future_vehicle.png"  />
                    <%--<img src="<%=(this.isAbsolute ? this.domain : "") +this.ResourceManager.GetString("FutureVehicleImage")%>" alt="" width="160" height="200" />--%>
               <%-- </a>
                <a href="<%//=(this.isAbsolute ? this.domain : "") +this.ResourceManager.GetString("FutureVehicleUrl")%>">
                    <span class="h3"><%//=this.ResourceManager.GetString("FutureVehicles")%></span>
                    <span class="price">&nbsp;</span>
                </a>
            </span>
		</div>
		<div class="buttons fr">                        
			<asp:Literal ID="litCarCompareLink" runat="server" />
			<asp:Literal ID="litCarViewAlModelsLink" runat="server" />
			<asp:Literal ID="litCarCertifiedUsedLink" runat="server" />
		</div>
		<div class="clrfix"></div>
	</div>
</div>--%>

<!-- end: dropdown of car box -->
<!-- begin: dropdown of truck box -->
<!--
<div id="trucks_list" class="dn">
  <div class="trucks_list_container"> Testing 2 </div>
</div>
-->
<!-- end: dropdown of truck box -->
<!-- begin: dropdown of hybrid box -->
<%--
<div id="hybrids_list" class="dn">
	<div class="hybrids_list_container">
		<div class="vehicle_list">
		<asp:Repeater ID="repModelHybrids"  runat="server" OnItemDataBound="repModels_ItemDataBound">
			<ItemTemplate>
			<span class="vehicle_btn">
					<asp:HyperLink ID="hypModelImage" runat="server" CssClass="vehicle_tout">
                        <asp:Literal ID="litModelImage" runat="server"></asp:Literal>
                    </asp:HyperLink>
                    <asp:HyperLink ID="hypModel" runat="server">
                        <span class="h3"><asp:Literal ID="litModelName" runat="server"></asp:Literal></span>
                        <span class="price"><asp:Literal ID="litModeldescription" runat="server"></asp:Literal></span>
                    </asp:HyperLink>
                </span>
			</ItemTemplate>
		</asp:Repeater>
			<span id="Span2" class="vehicle_btn" runat="server" visible="true">				            
				    <a class="vehicle_tout" href="<%//= (this.isAbsolute ? this.domain : "") + this.ResourceManager.GetString("FutureVehicleUrl")%>">		            
				    <img src="/_Global/img/layout/btn_future_vehicle.png"  />
                    <%--<img src="<%=(this.isAbsolute ? this.domain : "") +this.ResourceManager.GetString("FutureVehicleImage")%>" alt="" width="160" height="200" />--%>
               <%-- </a>
                <a href="<%//=(this.isAbsolute ? this.domain : "") +this.ResourceManager.GetString("FutureVehicleUrl")%>">
                    <span class="h3"><%//=this.ResourceManager.GetString("FutureVehicles")%></span>
                    <span class="price">&nbsp;</span>
                </a>
            </span>
		</div>
		<div class="buttons fr">					    
			<asp:Literal ID="litHybridCompareLink" runat="server" />
			<asp:Literal ID="litHybridViewAllModels" runat="server" />
			<asp:Literal ID="litHybridCertifiedUsedLink" runat="server" />
		</div>
		<div class="clrfix"></div>
	</div>
</div>	--%>

<!-- end: dropdown of truck box -->
<!-- begin: dropdown of shopping box -->
<!--
<div id="shopping_list" class="dn">
  <div class="shopping_list_container fc">
    <dl class="fc fl">
      <dd class="compare-models first"><asp:HyperLink ID="HyperLinkCompareModels" runat="server"><span><%=ResourceManager.GetString("txtCompareModels")%></span></asp:HyperLink></dd>
      <dd class="help-me-choose"><asp:HyperLink ID="HyperLinkHelpMeChoose" runat="server"><span><%=ResourceManager.GetString("txtHelpMeChoose")%></span></asp:HyperLink></dd>
      <dd class="news-events"><asp:HyperLink ID="HyperLinkNewsAndEvents" runat="server"><span><%=ResourceManager.GetString("txtNewsAndEvents")%></span></asp:HyperLink></dd>
      <dd class="accessories"><asp:HyperLink ID="HyperLinkAccessories" runat="server"><span><%=ResourceManager.GetString("txtAccessories")%></span></asp:HyperLink></dd>
      <dd class="parts-service"><asp:HyperLink ID="HyperLinkPartsAndService" runat="server"><span><%=ResourceManager.GetString("txtPartsAndService")%></span></asp:HyperLink></dd>
    </dl>
    <dl class="fc fl">
      <dd class="find_a_dealer first"><asp:HyperLink ID="HyperLinkFindADealer" runat="server"><span><%=ResourceManager.GetString("txtFindADealer")%></span></asp:HyperLink></dd>
      <dd class="lease-finance"><asp:HyperLink ID="HyperLinkFinance" runat="server"><span><%=ResourceManager.GetString("txtFinance")%></span></asp:HyperLink></dd>
      <dd class="special-offers"><asp:HyperLink ID="HyperLinkSpecialOffers" runat="server"><span><%=ResourceManager.GetString("txtSpecialOffers")%></span></asp:HyperLink></dd>
      <dd class="commercial-products"><asp:HyperLink ID="HyperLinkCommercialProducts" runat="server"><span><%=ResourceManager.GetString("txtCommercialProducts")%></span></asp:HyperLink></dd>
      <dd class=""></dd>
    </dl>
  </div>
</div>
-->
<!-- end: dropdown of shopping box -->