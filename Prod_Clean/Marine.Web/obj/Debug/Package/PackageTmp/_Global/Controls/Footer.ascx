﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="Marine.Web._Global.Controls.Footer" %>

 <div class="container">
	<div id="footer" class="<%=white_text_class%>">
		<ul class="fc">
			<li class="fl first">
				<dl>
					<dt><%=ResourceManager.GetString("txtHondaFamily")%></dt>
                    <dd><a href="<%=ResourceManager.GetString("urlPowerEquipment")%>"><%=ResourceManager.GetString("txtPowerEquipment")%></a></dd>
                    <dd><a href="<%=ResourceManager.GetString("urlATV")%>"><%=ResourceManager.GetString("txtATVs")%></a></dd>
                    <dd><a href="<%=ResourceManager.GetString("urlSXS")%>"><%=ResourceManager.GetString("txtSXS")%></a></dd>
                    <dd><a href="<%=ResourceManager.GetString("urlMotorcycle")%>"><%=ResourceManager.GetString("txtMotorcycles")%></a></dd>
					<dd><a href="<%=ResourceManager.GetString("urlHondaAuto")%>"><%=ResourceManager.GetString("txtHondaAuto")%></a></dd>
                    <dd><a href="<%=ResourceManager.GetString("urlAcura")%>"><%=ResourceManager.GetString("txtAcura")%></a></dd>
                    <dd><a href="<%=ResourceManager.GetString("urlEngines")%>"><%=ResourceManager.GetString("txtEngines")%></a></dd>                    
				</dl>
			</li>
            <li class="fl">
				<dl>
					<dt><%=ResourceManager.GetString("txtHondaAutomotive")%></dt>
				        <asp:Repeater ID="repModels"  runat="server" OnItemDataBound="repModels_ItemDataBound">
					        <ItemTemplate>					        
					        <dd><asp:Literal ID="LitModelLink" runat="server" > </asp:Literal></dd>					                    					        
					        </ItemTemplate>
					    </asp:Repeater>
					</dl>
				</li>
				<li class="fl" id="liModelList2" runat="server" Visible="false">
				<dl>
					<dt>&nbsp;</dt>
				        <asp:Repeater ID="repModels2"  runat="server" OnItemDataBound="repModels2_ItemDataBound">
					        <ItemTemplate>					        
					        <dd><asp:Literal ID="LitModelLink2" runat="server" > </asp:Literal></dd>					                    					        
					        </ItemTemplate>
					    </asp:Repeater>
					</dl>
				</li>
			<li class="fl">
				<dl>
					<dt><%=ResourceManager.GetString("txtShoppingTools")%></dt>
                    <dd><asp:Literal ID="litCompareLink" runat="server" /></dd>
                    <dd><asp:Literal ID="litAccessories" runat="server" /></dd>
                    <dd><asp:Literal ID="litPartsService" runat="server" /></dd>
                    <dd><asp:Literal ID="litDealerLocatorLink" runat="server" /></dd>
                    <dd><asp:Literal ID="litLeaseAndFinanceLink" runat="server" /></dd>                    
                    <dd><asp:Literal ID="litSpecialOffersLink" runat="server" /></dd>                    
                    <dd><asp:Literal ID="litCommercialProd" runat="server" /></dd>
				</dl>
			</li>
			<%--<li class="fl">
				<dl>
					<dt><%=ResourceManager.GetString("txtOwnersMenu")%></dt>
					<dd><asp:Literal ID="litExpressServiceLink" runat="server" /></dd>
					<dd><asp:Literal ID="litFAQLink" runat="server" /></dd>
					<dd><asp:Literal ID="litHondaServiceLink" runat="server" /></dd>
					<dd><asp:Literal ID="litOwndersLink" runat="server" /></dd>
					<dd><asp:Literal ID="litApparelLink" runat="server" /></dd>
					<dd><asp:Literal ID="litWarrantyLink" runat="server" /></dd>
					<dd><asp:Literal ID="litVehicleRecallLink" runat="server"/></dd>
					<dd><asp:Literal ID="litSecurity" runat="server" /></dd>
					<dd><asp:Literal ID="litWarranty" runat="server" /></dd>						    
				</dl>
			</li>--%>
			<li class="fr">
				<dl>
					<dt><%=ResourceManager.GetString("txtOurCompany")%></dt>
					<dd><asp:Literal ID="litInnovationLink" runat="server" /></dd>
					<dd><asp:Literal ID="litEnvironmentLink" runat="server" /></dd>
					<dd><asp:Literal ID="litHondaInCanadaLink" runat="server" /></dd>
					<dd><asp:Literal ID="litFoundationLink" runat="server" /></dd>
					<dd><asp:Literal ID="litSponsorshipLink" runat="server" /></dd>
                    <dd><asp:Literal ID="litNewsLink" runat="server" /></dd>
					<dd><asp:Literal ID="litEventsLink" runat="server"></asp:Literal></dd>
					<dd><asp:Literal ID="litHondaWorldWideLink" runat="server" /></dd>
					<dd><asp:Literal ID="litCareersLink" runat="server" /></dd>
					<dd><asp:Literal ID="litContactLink" runat="server" /></dd>
				</dl>
			</li>
		</ul>
		<div class="sub_footer_nav">
			<div class="fl copyright"><%= string.Format(ResourceManager.GetString("txtCopyRightText"), DateTime.UtcNow.Year)%></div>
			<ul class="fr">
				<li class="first"><asp:Literal ID="litTermsLink" runat="server" /></li>
				<li><asp:Literal ID="litPrivacyBottomLink" runat="server" /></li>
				<li runat="server"><asp:Literal ID="litSiteMapLink" runat="server" /></li>
			</ul>
			<div class="clr"></div>
		</div>
	</div>
</div>

	<!-- begin: dropdown of generators box -->

	<div id="27_list" class="dn">
		<div class="nav-ddctr">
			<div class="target-ctr generator"></div>
			<div class="fc content_section footer_section">
				<div class="fr fc buttons">
					<a href="<%=ResourceManager.GetString("urlSeeAllOutBoardMotor")%>" class="btn delayed primary"><span><%=ResourceManager.GetString("txtdrpSeeOutBoardMotor")%></span></a>
					<div class="divider"></div>
					<a href="<%=ResourceManager.GetString("urlCompareOutBoardMotor")%>" class="btn delayed primary"><span><%=ResourceManager.GetString("txtdrpCompareOutBoardMotor")%></span></a>

					<%--<div class="divider"></div>
					<a href="<%=ResourceManager.GetString("urlHelpMeChooseGenerators")%>" class="btn delayed primary"><span><%=ResourceManager.GetString("txtdrpHelpMeChoose")%></span></a>--%>
				</div>
				<%--<div class="fr copy">
					<p><%=ResourceManager.GetString("txtdrpGeneratorDesc")%></p>
				</div>--%>
			</div>
		</div>

	</div>
	<!-- end: dropdown of generators box -->
	
	<!-- begin: dropdown of lawnmowers box -->
	<div id="28_list" class="dn">
		<div class="nav-ddctr">
            <div class="target-ctr lawnmower"></div>
			<div class="fc content_section footer_section">
				<div class="fr fc buttons">
					<a href="<%=ResourceManager.GetString("urlSeeAllOutBoardMotor")%>" class="btn delayed primary"><span><%=ResourceManager.GetString("txtdrpSeeOutBoardMotor")%></span></a>
					<div class="divider"></div>
					<a href="<%=ResourceManager.GetString("urlCompareOutBoardMotor")%>" class="btn delayed primary"><span><%=ResourceManager.GetString("txtdrpCompareOutBoardMotor")%></span></a>
					<%--<div class="divider"></div>
					<a href="<%=ResourceManager.GetString("urlHelpMeChooseLawnmowers")%>" class="btn delayed primary"><span><%=ResourceManager.GetString("txtdrpHelpMeChoose")%></span></a>--%>
				</div>
				<%--<div class="fr copy">
					<p><%=ResourceManager.GetString("txtdrpLawnmoverDesc")%></p>

				</div>--%>
			</div>
		</div>
	</div>
	<!-- end: dropdown of lawnmowers box -->
	
	<!-- begin: dropdown of waterpumps box -->
	<div id="29_list" class="dn">
		<div class="nav-ddctr">
            <div class="target-ctr waterpump"></div>

			<div class="fc content_section footer_section">
				<div class="fr fc buttons">
					<a href="<%=ResourceManager.GetString("urlSeeAllOutBoardMotor")%>" class="btn delayed primary"><span><%=ResourceManager.GetString("txtdrpSeeOutBoardMotor")%></span></a>
					<div class="divider"></div>
					<a href="<%=ResourceManager.GetString("urlCompareOutBoardMotor")%>" class="btn delayed primary"><span><%=ResourceManager.GetString("txtdrpCompareOutBoardMotor")%></span></a>
					<%--<div class="divider"></div>
					<a href="<%=ResourceManager.GetString("urlHelpMeChooseWaterpumps")%>" class="btn delayed primary"><span><%=ResourceManager.GetString("txtdrpHelpMeChoose")%></span></a>--%>

				</div>
				<%--<div class="fr copy">
					<p><%=ResourceManager.GetString("txtdrpWaterpumpDesc")%></p>
				</div>--%>
			</div>
		</div>
	</div>
	<!-- end: dropdown of waterpumps box -->

	


    <!-- begin: dropdown of shopping box -->
    <!-- TODO replace copy here with string resource -->
    <div id="shopping_list" class="dn">
        <div class="shopping_list_container fc">
            <dl class="fc fl">
                <dd class="compare-models first"><a href="<%=ResourceManager.GetString("CompareRootFolder")%>"><span><%=ResourceManager.GetString("txtCompareModels")%></span></a></dd>
                <%--<dd class="help-me-choose"><a href="<%=ResourceManager.GetString("HelpMeChooseUrl")%>"><span><%=ResourceManager.GetString("txtHelpMeChoose")%></span></a></dd>--%>
                <%--<dd class="news-events"><a href="<%=ResourceManager.GetString("NewsEventsURL") %>"><span><%=ResourceManager.GetString("txtNewsAndEvents")%></span></a></dd>--%>
                <dd class="accessories"><a href="<%=ResourceManager.GetString("AccessoriesUrl") %>"><span><%=ResourceManager.GetString("txtAccessories")%></span></a></dd>
                <dd class="parts-service"><a href="<%= ResourceManager.GetString("PartsServicesUrl") %>"><span><%=ResourceManager.GetString("txtPartsAndService")%></span></a></dd>
            </dl>
            <dl class="fc fl">
                <dd class="find_a_dealer first"><a href="<%=ResourceManager.GetString("DealerLocatorRootFolder") %>"><span><%=ResourceManager.GetString("txtFindADealer")%></span></a></dd>
                <dd class="lease-finance"><a href="<%=ResourceManager.GetString("LeaseAndFinanceUrl") %>"><span><%=ResourceManager.GetString("txtFinance")%></span></a></dd>
                <dd class="special-offers"><a href="<%=ResourceManager.GetString("txtSpecialOffersURL") %>"><span><%=ResourceManager.GetString("txtSpecialOffers")%></span></a></dd>
                <%--<dd class="commercial-products"><a href="<%=ResourceManager.GetString("txtCommercialProductsURL")%>"><span><%=ResourceManager.GetString("txtCommercialProducts")%></span></a></dd>--%>
                <%--<dd class=""></dd>--%>
            </dl>
        </div>
    </div>
