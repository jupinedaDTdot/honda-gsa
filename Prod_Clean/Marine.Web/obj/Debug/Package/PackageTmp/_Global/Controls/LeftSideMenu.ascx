﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftSideMenu.ascx.cs" Inherits="Marine.Web._Global.Controls.LeftSideMenu" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>

<li runat="server" id="mainLi">
	<div class="top_cap pf"></div>
    <uc:ElementControl id="ElementControl1" runat="server"  Visible="true" />
	<div class="content pf">
		<h4 id="ctrlH4" runat="server"><asp:Literal ID="litMenuTitle" runat="server"></asp:Literal></h4>
        <asp:PlaceHolder ID="plMenu" runat="server" />		
	</div>
	<div class="bottom_cap pf"></div>
</li>