var lang = 'fr';
define({
    'tracker.account': 'UA-5158555-29'
    , 'system.product_line': 'V'

    , 'url.survey': 'http://www.hondasurvey.ca/atv/?lang=fr&sid='
    , 'url.survey_window': '/survey/survey_fre.html'

    , 'popup.mask': {
        color: '#3f3832'
        , opacity: 0.9
    }
    , 'build_it.accessory.default_image': '/assets/Accessories/fr_NA_thumbnail.png'
    , 'build_it.details.url': '/construisezvotreatv/{category_key}/{model_key}/{model_year_key}'
    , 'build_it.details.overrides': {
        'recreatifs': {
            'big_red': 'utilitaire'
            , 'trx500pg_cte': 'utilitaire'
            , 'trx500_cte_se': 'utilitaire'
            , 'trx420pg_cte': 'utilitaire'
            , 'trx420pg_cte_se': 'utilitaire'
        }
    }
});
