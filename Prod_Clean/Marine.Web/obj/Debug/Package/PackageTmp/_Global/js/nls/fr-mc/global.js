var lang = 'fr';
define({
    'tracker.account': 'UA-5158555-30'
    , 'system.product_line': 'M'
    
    , 'url.survey': 'http://www.hondasurvey.ca/motorcycle/?lang=fr&sid='
    , 'url.survey_window': '/survey/survey_fre.html'

    , 'build_it.accessory.default_image': '/assets/Accessories/fr_NA_thumbnail.png'
    , 'build_it.details.url': '/construisezvotremotocyclette/{category_key}/{model_key}/{model_year_key}'
    , 'build_it.details.overrides': {
    }
});
