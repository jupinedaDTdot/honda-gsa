;(function($){


// define the WebTracker class
window.Honda = window['Honda'] ? window.Honda : {}; // define the Honda namespace if not exists

// constructor
Honda.WebTracker = function(tracker, gat, account) {
	if (tracker) {
		this.pageTracker = tracker;
	}
	else {
		this.pageTracker = gat._createTracker(account);
	}
	if (!this.pageTracker) {
		throw new Exception('gat');
	}
	
	if ($._DEBUG) {
		this.enableDebug();
	}
};
// prototype
$.extend(Honda.WebTracker.prototype, {
	trackPrimaryNavigation: function(label) {
		this.pageTracker._trackEvent('Primary Navigation', 'Drop Down', label);
	},
	trackWidget: function(action, label) {
		this.pageTracker._trackEvent('Widget', action, label);
	},
	trackDownload: function(label) {
		this.pageTracker._trackEvent('File Request', 'Download', label);
	},
	trackTout: function(vehicle, tout) {
		var url = '/{vehicle-key}/tout/{tout-control-name}'.replace('{vehicle-key}', vehicle);
		url = url.replace('{tout-control-name}', tout);
		this.pageTracker._trackPageview(url);
		this.pageTracker._trackEvent('Tout', 'Select Tout', tout);
	},
	trackTrimLevel: function(level) {
		this.pageTracker._trackEvent('Trim Selector', 'Select Trim Level', level);
	},
	trackTrimColor: function(color) {
		this.pageTracker._trackEvent('Trim Selector', 'Select Colour Swatch', color);
	},
	trackTrimCompare: function() {
		this.pageTracker._trackEvent('Trim Selector', 'Compare');
	},
	trackBookTestDrive: function(vehicle) {
		var url = '/{vehicle-key}/book-a-test-drive/'.replace('{vehicle-key}', vehicle);
		this.pageTracker._trackPageview(url);
		this.pageTracker._trackEvent('Popup', 'Book Test Drive', vehicle);
	},
	trackBookTestDriveComplete: function(vehicle) {
		var url = '/{vehicle-key}/book-a-test-drive/complete'.replace('{vehicle-key}', vehicle);
		this.pageTracker._trackPageview(url);
	},
	trackMicrositeTout: function(vehicle) {
		var url = '/{vehicle-key}/microsite-tout/'.replace('{vehicle-key}', vehicle);
		this.pageTracker._trackPageview(url);
		this.pageTracker._trackEvent('Tout', 'Select Microsite Tout', vehicle);
	},
	trackPage: function(url) {
		this.pageTracker._trackPageview(url);
	},
	trackDealerContactEmail: function(dealer) {
		this.pageTracker._trackEvent('Find a Dealer', 'Email Dealer', dealer);
	},
	trackDealerWebsite: function(dealer) {
		var url = '/dealer-website/{dealer-name-url-friendly}'.replace('{dealer-name-url-friendly}', dealer);
		this.pageTracker._trackPageview(url);
		this.pageTracker._trackEvent('Find a Dealer', 'Visit Website', dealer);
	},
	trackEvent: function(category, action, opt_label, opt_value, opt_noninteraction) {
	  this.pageTracker._trackEvent(category, action, opt_label, opt_value, opt_noninteraction);
	},
    trackVideo: function(path) {
        return this.trackUrlOrPath(path);
    },
    trackUrlOrPath: function(path) {
        var url;
        if (/^https?:/.test(path)) {
            url = '/videos/' + path.replace(/^https?:\/\//, '');
        }
        else if (path) {
            url= '/videos/' + path.replace(/^\//, '');
        }
        this.pageTracker._trackPageview(url);
    		this.pageTracker._trackEvent('Tout', 'Watch Video', path);
    },
	enableDebug: function() {
		var old_trackpage = this.pageTracker._trackPageview;
		var old_trackevent = this.pageTracker._trackEvent;
		
		this.pageTracker._trackPageview = function() {
			console.log(arguments.caller, arguments.callee, arguments);
			old_trackpage.apply(this, arguments);
		};
		this.pageTracker._trackEvent = function() {
			console.log(arguments.caller, arguments.callee, arguments);
			old_trackevent.apply(this, arguments);
		};
	}
});

var uid = Math.random().toString().substring(2);
window['tracker_' + uid] = function() {};

// global functions to access the tracker
window.tracker_get = function() {
	return window['tracker_' + uid];
};

window.tracker_set = function(tracker) {
	window['tracker_' + uid] = tracker;
};

})(typeof jQuery == 'undefined' ? null : jQuery);