/* $Id$ */

;(function($) {
var env = global_get('environment');
var _defaults = {
	url: '/buildittool/' + env + '/data/JSON/{lang}/{year}/{vehicle}/{trim}/{transmission}/warranty.json',

	vehicle: '',
	trim: '',
	transmission: '',

	lang: '',
	year: '',

	add_category: function(category) {},
	add_warranty: function(warranty) {},

	_last: null
};	

$.widget('honda.match_warranty', {
	widgetEventPrefix: 'match_warranty_',
	options: $.extend(true, {}, _defaults, (function(){
		try {
			return global_get('honda.match_warranty.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
	// initializer
	_create: function() {
	}
    , _ensureOptions: function() {
        var w = this.widget()
            , o = this.options;

        o.url = w.data('matchWarrantyUrl') || o.url;
    }
	// load vehicle
	, loadWarrantyWithTrimAndTransmission: function(vehicle, trim, transmission, year) {
		var self = this,
			o = this.options;
		
    if (year) {
      o.year = year;
    }

		if (vehicle) {
			o.vehicle = vehicle;
		}
		
		if (trim) {
			o.trim = trim;
		}
		
		if (transmission) {
			o.transmission = transmission;
		}
		
		this.reloadWarranty();
	},
	reloadWarranty: function() {
	  var self = this,
	      o = this.options,
	      url = $.isFunction(o.url) ? o.url() : o.url;
		
		url = url
	    .replace('{year}', o.year)
			.replace('{vehicle}', o.vehicle)
			.replace('{trim}', o.trim)
			.replace('{transmission}', o.transmission)
			.replace('{lang}', o.lang);

		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'json',
			cache: false,
			global: false,
			success: function(data, status, xhr) {
				console.log('match warranty data loaded', data);

			    self._data = data;

                if (null == data) {
                    self._success(data, status, xhr);
                    return;
                }
				
				$.each(data.keys, function(index, category_key) {
					var category = data[category_key];
					self._will_add_category(null, {
						category_key: category_key,
						category: category,
						data: data
					});
					o.add_category.call(self, {
						category_key: category_key,
						category: category,
						data: data
					});
					
					$.each(category.keys, function(index, warranty_key){
						var warranty = category[warranty_key];
						
						self._will_add_warranty(null, {
							category_key: category_key,
							warranty_key: warranty_key,
							warranty: warranty,
							data: data
						});
						o.add_warranty.call(self, {
							category_key: category_key,
							warranty_key: warranty_key,
							warranty: warranty,
							data: data
						});
						self._did_add_warranty(null, {
							category_key: category_key,
							warranty_key: warranty_key,
							warranty: warranty,
							data: data
						});
					});
					self._did_add_category(null, {
						category_key: category_key,
						category: category,
						data: data
					});
				});
				self._success(data, status, xhr);
			},
			error: function(xhr, err) {
				self._error(xhr, err);
			}
		});
	},
    	// filter data
	filterWarranties: function(selected) {
		var data = $.extend(true, {}, this._data);

		if (selected == null) {
			return null;
		}
		if (selected.keys == 0) {
			return null;
		}
		// first round, remove those accessories that are not selected
		$.each(this._data.keys, function(index, key) {
			if (data[key]) {
			    var items = data[key];
				var keys_to_delete = [];
				//collect accessories kyes to remove (not selected accessories)
				$.each(items.keys, function(index2, acc_key) {
					if (selected.keys.indexOf(acc_key) == -1) {
						keys_to_delete.push(acc_key)
					}
				});

				//if keys will be removed in a previous loop then it will glitch and won't remove all accessories
				$.each(keys_to_delete, function(index3, acc_key) {
					items.keys.remove(items.keys.indexOf(acc_key));
					delete items[acc_key];
				});
			    
			    // delete extra keys
			    delete items['allowMultipleSelection'];
			    delete items['isExclusive'];
			    delete items['title'];
			}
		});

		// second round, converts category to null if empty
		$.each(this._data.keys, function(index, key) {
			if (data[key] && data[key].keys.length == 0) {
    			data.keys.remove(data.keys.indexOf(key));
			    delete data[key];
			}
		});

		return data;
	},
	_error: function(xhr, err) {
		this._trigger('load_error', null, {xhr: xhr, err: err});
	},
	_success: function(data, status, xhr) {
		this._trigger('load_success', null, {data: data, status: status, xhr: xhr});
	},
	_will_add_category: function(event, ui) {
		this._trigger('will_add_category', event, ui);
	},
	_did_add_category: function(event, ui) {
		this._trigger('did_add_category', event, ui);
	},
	_will_add_warranty: function(event, ui) {
		this._trigger('will_add_category', event, ui);
	},
	_did_add_warranty: function(event, ui) {
		this._trigger('did_add_warranty', event, ui);
	}
});

})(jQuery);