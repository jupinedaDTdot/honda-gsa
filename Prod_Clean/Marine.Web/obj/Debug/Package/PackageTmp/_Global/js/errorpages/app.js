﻿(function () {

  window.__load__ = function (name, callback) {
    if ($.behaviors && $.behaviors.hasOwnProperty(name)) {
      return;
    }
    $.behaviors = $.behaviors || {};
    $.behaviors[name] = function (context, ajax) {
      callback(context, ajax);
    };
    callback(document, false);
  };

  require(['modernizr-1.6'], function() {
    var group = ['cufon-init', 'jquery.synchronised', 'ext/common', 'jquery.json-2.2.min', 'jquery.swfobject-1.1.1.min', 'modal_window', 'jquery.cookie'];

    if (Modernizr.history) {
      group.push('advanced-history');
    }
    require(group, function () {
      require(['jquery.format', 'jquery.hashchange', 'ui/jquery.ui.core', 'ui/jquery.ui.widget', 'ext/jquery.jobqueue'], function () {
        require(['ui/jquery.ui.mouse'], function () {
          require(['ui/jquery.ui.position', 'ui/jquery.ui.draggable', 'commonlib/jquery.commonlib.widget'], function () {
            require(['functions'], function () {
              var r = ['crossdomain'];
              if (window.__edit) {
                r.push('edit');
                require(r);
              }
            });
          });
        });
      });
    });
  });

} .call(this));