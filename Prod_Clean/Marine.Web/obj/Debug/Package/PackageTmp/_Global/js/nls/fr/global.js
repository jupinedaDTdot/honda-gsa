var lang = 'fr';
define({
    lang: 'fr',
    'format.price': '#,###.00$',
    'format.price_rounded': '#,###$',
    // urls
    'url.province_dealers': '/modelpages/BookTestDriveDealers.aspx?lang='+lang+'&province=',
    'url.finance_calculator_msrp': function() {
        // build the msrp url based on vehicle and province selector
        var province = $('#province', this).val();
        var vehicle = eval('(' + $('#vehicle', this).val() + ')');
        var included = $('#include_tax', this).attr('checked');
        var url = '/buildittool/'+global_get('environment')+'/data/JSON/fr/' + province + '/' + vehicle.model_key
            + '/' + vehicle.trim_level_key + '/financial_presets_' + vehicle.transmission_key + '.json?include_fees='
            + (included ? 'true' : 'false');
        return url;
    },
    // honda accessories
    'url.honda_accessories': function() {
        var province = $('#accessory_province').val();
        province = province ? province : 'ON';
        var uri = $('#accessory_trim').val();
        uri = uri.split('/');
        uri.splice(uri.indexOf('JSON')+2, 0, province); // insert province into the URI
        var transmission = uri.pop();
        uri.pop();
        uri.pop();
        uri.push(transmission);
        return uri.join('/') + '/accessories.json';
    },
    'url.honda_accessories.color_thumbnail_prefix': function(key) {
//        /buildittool/LIVE/data/JSON/assets/accessories/civic_coupe/dx/alabaster_silver_metallic/748-Manual/protection_package_thumbnail.jpg
        var uri = $('#accessory_trim').val();
        uri = uri.split('/');
        var transmission = uri.pop();
        var color = uri.pop(); // color
        var trim = uri.pop();
        var model = uri.pop();
        var acc = uri.pop();
        uri.pop(); // lang
        uri.push('assets');
        uri.push(acc);
        uri.push(model);
        uri.push(trim);
        uri.push(color);
        uri.push(transmission);
        return uri.join('/');
    },
    'url.honda_accessories.thumbnail_prefix': function(key) {
        var uri = $('#accessory_trim').val();
        uri = uri.split('/');
        var transmission = uri.pop();
        uri.pop(); // color
        var trim = uri.pop();
        var model = uri.pop();
        var acc = uri.pop();
        uri.pop(); // lang
        uri.push('assets');
        uri.push(acc);
        uri.push(model);
//        uri.push(trim);
//        uri.push(color);
        uri.push(transmission);
        return uri.join('/') + '/' + key;
    },
    'honda_accessories.section_titles': {
        packages: {
            title: 'Packages'
        },
        entertainment: {
            title: 'Entertainment'
        },
        exterior: {
            title: 'Exterior'
        },
        interior: {
            title: 'Interior'
        }
    },
    'accessory.slidedown_open' : true,
    // honda match
    'url.honda_match.models': function() {
        return '/buildittool/'+global_get('environment')+'/data/JSON/fr/models.json';
    },
    'url.honda_match.models_financial': function() {
        var province = $.cookie('hondaprovince');
        province = province ? province : 'ON';
        return '/buildittool/'+global_get('environment')+'/data/JSON/fr/'+province+'/models_financial.json?include_fees=false';
    },
    'url.honda_match.thumbnail_path': function() {
        return '/buildittool/'+global_get('environment')+'/data/json/assets/{model_key}/hondaca_modelNav.png';
        // return '/_Global/img/layout/btn_{model_key}.png';
    },
    // city suggest search url
    'url.search_city': '/dealerlocator/GetCityService.aspx?sCity=',
    'url.search_suggest': '/_Global/service/GSAService.svc/SuggestiveSearch',
    'search_suggest.query_type': {
        type: 'field',
        field: 'GSA_Keywords'
    },
    'search_suggest.data': {
        client: 'honda_suggestive',
        collections: 'honda_' + (__hsite ? __hsite : ''),
        lang: 'lang_' + lang,
        perGroup: 5,
        requiredMetaFields: '',
        partialMetaFields: '',
        filter: 0,
        _last: null
    },
    'url.search_link_fallback': {
        url: '/searchfre/',
        copy: 'Voir Tout Les Resultants'
    },
    'url.search_no_result_copy': "Désolé, nous ne trouvons pas de raccourci. Recherchez sur l'ensemble du site Honda.ca.",
    // survey urls
    'url.survey': 'http://www.hondasurvey.ca/powerequipment/?lang=fr&sid=',
   	'url.survey_window': '/survey/survey_fre.html',
    // french version
    // 'url.survey': 'http://hondasurvey.ca/?sid=97273&lang=fr',
    // 'url.survey_window': 'survey_fre.html',
    // 'url.survey_close_button': '/_Global/img/survey/Fre/close.gif',
    // arguments
    'autocomplete.search_city.min_length': 2,

    // google analytics account
    'tracker.account': 'UA-5158555-1',

    // recaptcha
    'recaptcha.public_key' : '6LdBLcESAAAAANjZQggpl7gk3QyauYh3INzDctO0',

    // CompareWidget
    'honda.CompareWidget.defaults': {
        url: '/comparer/'
    }
    // Compare App
    , 'honda.CompareApp': {
        modelSpecsUrl: '/service/trimListing.aspx?modelfamily={modelFamily}&L=fr'
    }
    , 'build_it.format.price': '#,###.00 <span>$</span>'
    , 'build_it.format.price_rounded': '#,### <span>$</span>'
    , 'build_it.url.dealerLocator': '/trouver-concessionaire'
    , 'text.decimal.separator': ','
    , 'text.manual': 'Manuelle'
    , 'text.automatic': 'Automatique'
    , 'text.cvt': 'CVT'
    , 'text.invalid_postal': 'Code postal invalide.'
    , 'text.msrp': 'PDSF'
    , 'text.vehicle_name': '%(model)s %(year)s'
    , 'text.lease': 'Location'
    , 'text.finance': 'Financement'
    , 'text.accessories': 'Accessoires'
    , 'text.comprehensive': 'Global'
    , 'text.partNumber': 'No de pièce'
    , 'text.cost': 'Coût'
    // FIXME brochure app should be updated to handle empty accessory results
    , 'build_it.brochure_accessory.defaults': {
        "interior": null,
        "labels": {
            "interior": "Interior",
            "exterior": "Exterior",
            "packages": "Packages",
            "entertainment": "Entertainment"
        },
        "keys": [
            "packages",
            "entertainment",
            "exterior",
            "interior"
        ],
        "exterior": null,
        "packages": null,
        "entertainment": null
    }
});

