var lang = 'en';
define({
    'tracker.account': 'UA-5158555-29'
    , 'system.product_line': 'V'

    , 'url.survey': 'http://www.hondasurvey.ca/atv/?lang=en&sid='
    , 'url.survey_window': '/survey/survey_eng.html'

    , 'popup.mask': {
        color: '#3f3832'
        , opacity: 0.9
    }
    , 'build_it.accessory.default_image': '/assets/Accessories/en_NA_thumbnail.png'
    , 'build_it.details.url': '/buildyouratv/{category_key}/{model_key}/{model_year_key}'
    , 'build_it.details.overrides': {
        'recreation': {
            'big_red': 'utility'
            , 'trx500pg_cte': 'utility'
            , 'trx500_cte_se': 'utility'
            , 'trx420pg_cte': 'utility'
            , 'trx420pg_cte_se': 'utility'
        }
    }
});
