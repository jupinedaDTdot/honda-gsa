// enable edit button
;(function($, F, W, undefined) {
    F.edit_mode = true;

    W.show_edit_buttons = function() {
        $('a.edit-button').each(function() {
            var self = $(this);
            self.show().parent().addClass('has-edit-button');
        });

        $('.edit-container-should-hide').show();

      F.edit_mode = true;
    };

    W.hide_edit_buttons = function() {
        $('a.edit-button').each(function() {
            var self = $(this);
            self.hide().parent().removeClass('has-edit-button');
        });

        $('.edit-container-should-hide').hide();

      F.edit_mode = false;
    };
})(jQuery, window, window);
