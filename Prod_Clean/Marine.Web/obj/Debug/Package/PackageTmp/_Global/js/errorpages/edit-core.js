﻿window.show_edit_buttons = function () {
  window.__edit = true;
  $('a.edit-button').each(function () {
    var self = $(this);
    self.show().parent().addClass('has-edit-button');
    $('.edit-container-should-hide').show();
  });
};

window.hide_edit_buttons = function () {
  window.__edit = false;
  $('a.edit-button').each(function () {
    var self = $(this);
    self.hide().parent().removeClass('has-edit-button');
    $('.edit-container-should-hide').hide();
  });
};
