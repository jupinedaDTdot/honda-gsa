/* $Id$ */

;(function($) {
var env = global_get('environment');
var _defaults = {
	url: '/api/FinancialCalculator/summary',
	msrp_url: '/api/financialcalculator/calculator/{lang}/{province_key}/{year}/{model_key}/{trim_level_key}/{transmission_key}/{exterior_key}/{include_fees}',    
	lang: 'en',
	province_key: 'ON',
	include_fees: false,
	
	lease_threshold: 7,
	financial_threshold: 7,

	add_cash_purchase: function(data) {
	},
	add_msrp: function(data) {
	},
	add_lease: function(data) {
	},
	add_finance: function(data) {
	},
	set_include_fees: function(include_fees) {
	},
	_last: null
};

$.widget('honda.match_financial', {
	widgetEventPrefix: 'match_financial_',
	options: $.extend(true, {}, _defaults, (function(){
		try {
			return global_get('honda.match_financial.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
	// initializer
	_create: function() {
		this._requestData = null;
		this._isRequestDataAvailable = false;
	},
	loadFinancial: function(selected_keys, requestData, includeFees, lang) {
		var o = this.options;

        this._trigger('before_load', null, {});

		requestData.selected_keys = selected_keys;

		if (requestData) {
			this._requestData = requestData;
			this._isRequestDataAvailable = true;
		}

		if (typeof includeFees != undefined) {
			o.include_fees = includeFees;
		}

		if (lang) {
			o.lang = lang;
		}

		this.reloadData();
	},
	loadVehicle: function(model_key, trim_level_key, transmission_key, include_fees, exterior_key) {
		var self = this,
			o = this.options;

        this._trigger('before_load', null, {});

		var url = o.msrp_url.replace('{lang}', o.lang)
			.replace('{province_key}', o.province_key)
			.replace('{model_key}', model_key)
			.replace('{trim_level_key}', trim_level_key)
			.replace('{transmission_key}', transmission_key)
			.replace('{include_fees}', include_fees ? 'true' : 'false');

        if (exterior_key) {
            url = url.replace('{exterior_key}', exterior_key);
        }

		// ajax call
		$.ajax({
			type: 'GET',
			url: url,
			dataType: 'json',
			global: false,
			cache: false,
			success: function(data, status, xhr) {
				console.log('match financial data loaded', data);

				o.add_finance.call(self, data.finance, data);
				o.add_lease.call(self, data.lease, data);
				o.add_msrp.call(self, data.msrp);
				o.add_cash_purchase.call(self, data.cash_purchase, data);
				o.set_include_fees.call(self, data.include_fees, data);

				self._success(data, status, xhr);
			},
			error: function(xhr, err) {
				self._error(xhr, err);
			}
		});
	},
    updateIncludeFees: function(data, value) {
        if (typeof data['include_fees'] != 'object') {
            throw new Error('Object does not meet precondition');
        }

        data.include_fees.value = value;
    }
	// reload data
	, reloadData: function() {
		var self = this,
			o = this.options;

		// pre-condition
		if (!this._isRequestDataAvailable) {
			throw new Error('Request data is not set.');
		}

		var url = o.url + '?include_fees=' + (o.include_fees ? 'true' : 'false') + '&Lang=' + o.lang
            ,  exterior_key = self._requestData.selected_keys.exterior_key || null;

		$.ajax({
			url: url,
			type: 'POST',
			data: {
				Lang: o.lang,
				Data: $.toJSON(self._requestData)
                , exterior_key: exterior_key
			},
			dataType: 'json',
			cache: false,
			success: function(data, status, xhr) {
				console.log('match financial data loaded', data);

                o.add_finance.call(self, data.finance, data);
                o.add_lease.call(self, data.lease, data);
                o.add_msrp.call(self, data.msrp);
                o.add_cash_purchase.call(self, data.cash_purchase, data);
                o.set_include_fees.call(self, data.include_fees, data);

				self._success(data, status, xhr);

				// if exception is thrown, call exception
				if (data.exceptions && data.exceptions.keys.length > 0) {
					self._exception(data.exceptions);
				}
			},
			error: function(xhr, err) {
				self._error(xhr, err);
			}
		});
	},
	_error: function(xhr, err) {
		this._trigger('load_error', null, {xhr: xhr, err: err});
	},
	_success: function(data, status, xhr) {
		this._trigger('load_success', null, {data: data, status: status, xhr: xhr});
	},
	_exception: function(exceptions) {
		this._trigger('load_exception', null, {exceptions: exceptions});
	}
});

})(jQuery);