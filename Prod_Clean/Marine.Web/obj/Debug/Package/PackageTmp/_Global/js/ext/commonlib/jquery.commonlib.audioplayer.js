; (function ($, W, undefined) {

  var _defaults = {
    url: '',
    player: 'flash', // or html
    playerUrl: '/_Global/swf/jplayer.swf',
    playClass: 'commonlib-audio-play',
    playingClass: 'commonlib-audio-playing',
    stopClass: 'commonlib-audio-stop',
    _last: null
  };

  var FlashPlayer = function (id, url, swfUrl) {

    var playerId = 'audio_player_' + Math.round(Math.random() * 100000),
            player = $('<div>').appendTo('body');

    player.flash({
      id: playerId,
      swf: swfUrl,
      width: 1,
      height: 1,
      allowScriptAccess: 'always',
      allowNetworking: 'all',
      wmode: 'opaque',
      quality: 'high',
      play: 'true',
      loop: 'false',
      menu: 'false',
      scale: 'showall',
      bgcolor: '#4c4c4c',
      flashvars: {
        id: id,
        vol: '1',
        muted: 'false'
      }
    });

    var flash = document[playerId];

    this._hidePlayer = function () {
      player.css({
        width: '0',
        height: '0',
        overflow: 'hidden'
      });
    };

    this.play = function () {
      if (url.match(/\.m4a$/)) {
        flash.fl_setAudio_m4a(url);
      }
      else if (url.match(/\.mp3$/)) {
        flash.fl_setAudio_mp3(url);
      }

      flash.fl_play(0);
    };

    this.stop = function () {
      flash.fl_pause(0);
    };

    this.destroy = function () {
      this.stop();
      player.remove();
    };
  };

  var HtmlPlayer = function (url) {
    var playerId = 'audio_player_' + Math.round(Math.random() * 100000),
        player = new Audio();


    player.src = url;

    this.play = function () {
      player.play();
    };

    this.stop = function () {
      player.pause();
      player.currentTme = 0;
    };

    this.destroy = function () {
      this.stop();
      delete player;
    };
  };

  $.widget('commonlib.audioplayer', {
    widgetEventPrefix: 'audioplayer',
    options: $.extend(true, {}, _defaults, (function () {
      try {
        return global_get('commonlib.audioplayer.defaults');
      }
      catch (e) {
        return {};
      }
    })()),
    _create: function () {
      var self = this,
            o = this.options,
            w = this.widget(),
            id;

      if (w.attr('id')) {
        id = w.attr('id');
      }
      else {
        id = 'commonlib_audioplayer_' + Math.round(Math.random() * 100000);
        w.attr('id', id);
      }

      this.watchId = null;
      this.isPlaying = false;
      this.playerReady = false;
      this._ensureOptions();

      // initialize the player
      if (o.player == 'html') {
        this.player = new HtmlPlayer(o.url);
      }
      else {
        this.player = new FlashPlayer(id, o.url, o.playerUrl);
      }

      w.click(function () {
        if (self.isPlaying) {
          self.stop();
        }
        else {
          self.play();
        }
      });
    },
    _ensureOptions: function () {
      var self = this,
            w = this.widget(),
            o = this.options;

      o.url = w.data('audio-url') || o.url;
      o.player = w.data('audio-player') || o.player;
      o.playerUrl = w.data('audio-player-url') || o.playerUrl;
      o.type = w.data('audio-player') || o.player;
      o.playClass = w.data('audio-play-class') || o.playClass;
      o.playingClass = w.data('audio-playing-class') || o.playingClass;
      o.stopClass = w.data('audio-stop-class') || o.stopClass;
    },
    play: function () {
      if (this.playerReady) {
        this.player.play();
      }
    },
    stop: function () {
      if (this.playerReady && this.isPlaying) {
        this.player.stop();
      }
    },
    destroy: function () {
      this.player.destroy();
    },
    triggerEvent: function (event, status) {
      var self = this;
      switch (event) {
        case 'ready':
          self.playerReady = true;
          if (self.player._hidePlayer) {
            self.player._hidePlayer();
          }
          break;
        case 'play':
          self.isPlaying = true;
          self._play();
          break;
        case 'pause':
          self.isPlaying = false;
          self._stop();
          break;
        case 'ended':
          self.isPlaying = false;
          self._stop();
          break;
      }
    },
    _play: function () {
      var w = this.widget(),
            o = this.options;
      this.isPlaying = true;
      w.removeClass(o.playClass);
      w.addClass(o.stopClass);
      w.addClass(o.playingClass);
    },
    _stop: function () {
      var w = this.widget(),
            o = this.options;
      this.isPlaying = false;
      w.removeClass(o.stopClass);
      w.addClass(o.playClass);
      w.removeClass(o.playingClass);
    }
  });

  $.commonlib.audioplayer.EventHandler = function (id, event, status) {
    $('#' + id).audioplayer('triggerEvent', event, status);
  };

})(jQuery, window);