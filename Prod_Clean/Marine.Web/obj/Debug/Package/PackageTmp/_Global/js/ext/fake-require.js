;(function(W, undefined) {

var require = function() {
    for (var index = 0; index < arguments.length; index ++) {
        if (typeof arguments[index] == 'function') {
            arguments[index].call(this);
        }
    }
};

W.require = W.define = require;

})(window);