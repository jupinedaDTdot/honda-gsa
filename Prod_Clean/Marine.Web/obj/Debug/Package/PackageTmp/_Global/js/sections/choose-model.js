define('sections/choose-model', ['require'], function (require) {

    var runChooseModel = function (_queue) {
        var localQueue = _queue || new $.jobqueue(10);

        // FIXME dirty hack, we should put this into NLS.
        var rowCount = __hsite == 'powerequipment' ? 6 : 5;

        function adjustItemClasses() {

            // change the first classes
            localQueue.add(function () {
                $('.product .product-item', '#ChooseModel').removeClass('first').filter('.active-trim').each(function (index) {
                    var self = $(this);
                    if (index % rowCount == 0) {
                        self.addClass('first');
                    }

                    if ($('.product-image', self).data('popup')) {
                        var frame = $('.product-image', self).popup('frame');
                        if (index % rowCount <= 3) {
                            $('.product-image', self).data({
                                position: 'right top'
                        , popupClass: 'toolbox-top-alt toolbox-left'
                            });
                            try {
                                frame.removeClass('toolbox-right').addClass('toolbox-left');
                            } catch (e) { }
                            try {
                                $('.product-image', self).dropdown('option', 'additionalClass', 'toolbox-top-alt toolbox-left commonlib-popup');
                            } catch (e) { }
                        }
                        else {
                            $('.product-image', self).data({
                                position: 'left top'
                        , popupClass: 'toolbox-top-alt toolbox-right'
                            });
                            try {
                                frame.removeClass('toolbox-left').addClass('toolbox-right');
                            } catch (e) { }
                            try {
                                $('.product-image', self).dropdown('option', 'additionalClass', 'toolbox-top-alt toolbox-right commonlib-popup');
                            } catch (e) { }
                        }
                    }
                });
            });
        }

        var loadingCoverCount = 0;

        // initialize the ChooseModel app
        require(['ext/honda/honda.ChooseModelApp', 'ui/jquery.ui.mouse'], function () {

            $('#ChooseModel').ChooseModelApp('destroy');
            $('#ChooseModel').ChooseModelApp({
                change: function (event, ui) {
                    // the main show/hide logic should be here.
                    // hide all active trims
                    $('.product .product-item', '#ChooseModel').removeClass('first');
                    $('.product .product-item', '#ChooseModel').each(function () {
                        if ($.inArray($('> .product-image', this).data('id'), ui.active) >= 0) {
                            $(this).removeClass('inactive-trim').addClass('active-trim');
                        }
                        else {
                            $(this).removeClass('active-trim').addClass('inactive-trim');
                        }
                    });
                    $('.product .product-item.inactive-trim', '#ChooseModel').hide('slow');
                    $('.product .product-item.active-trim', '#ChooseModel').show('slow', function () {
                        adjustItemClasses();
                    });

                    // if no active is found
                    if (ui.active.length == 0) {
                        $('#selector-message').show();
                    }
                    else {
                        $('#selector-message').hide();
                    }
                },
                reset: function (event, ui) {
                    // reset the filter area
                    $('[property~="honda:model-filter"]', this).each(function () {
                        var self = $(this),
                        filter_id = self.data('filter'),
                        filter_type = self.data('filter-type');

                        if (filter_type == 'range') {
                            $('.slider-controls-inner', self).slider('values', 0, self.data('filter-min'));
                            $('.slider-controls-inner', self).slider('values', 1, self.data('filter-max'));
                        }
                        else if (filter_type == 'radio') {
                            $('input:radio', self).removeAttr('checked').filter('[value="' + self.data('filter-default-value') + '"]').attr('checked', 'checked');
                        }
                        else if (filter_type == 'checkbox') {
                            $('input:checkbox', self).removeAttr('checked').filter('[value="' + self.data('filter-default-value') + '"]').attr('checked', 'checked');
                        }
                    });
                }
            });

            $('a.filter-reset').click(function (event) {
                event.preventDefault();
                $('#selector-message .aside', '#ChooseModel').empty();
                $('#ChooseModel').ChooseModelApp('resetCriteria');
            });

            // get all filters
            var filterIDs = [];
            $('[property~="honda:model-filter"]', '#ChooseModel').each(function () {
                var self = $(this),
                filter_id = self.data('filter');
                filterIDs.push(filter_id);

                localQueue.add(function () {
                    self.disableSelection();

                    if (self.data('filter-type') == 'range') {
                        // initialize the range slider filters
                        require(['ui/jquery.ui.slider'], function () {
                            $('.slider-controls-inner', self).slider({
                                range: true,
                                min: self.data('filter-min'),
                                max: self.data('filter-max'),
                                values: [self.data('filter-min'), self.data('filter-max')],
                                change: function (event, ui) {
                                    // don't fire it if it's programmatically called
                                    if (!event.originalEvent) {
                                        return;
                                    }
                                    // update choose model criteria
                                    var min = $(this).slider('values', 0),
                                    max = $(this).slider('values', 1);

                                    $('#ChooseModel').ChooseModelApp('changeCriteria', filter_id, [min, max]);
                                }
                            });
                        });

                        // add filter
                        $('#ChooseModel').ChooseModelApp('addFilter', filter_id, {
                            type: 'range',
                            default_value: [self.data('filter-min'), self.data('filter-max')]
                        });
                    }
                    else if (self.data('filter-type') == 'radio') {
                        // initialize the checkbox/radio filters
                        $('input:radio', self).bind($.browser.msie && $.browser.version.match(/^(6|7|8)\./) ? 'click' : 'click', function () {
                            // update choose model criteria
                            $('#ChooseModel').ChooseModelApp('changeCriteria', filter_id, $(this).val());

                            var msg = $(this).data('message');
                            if (msg) {
                                $('#selector-message .aside').html($(msg).html());
                            }
                            else {
                                $('#selector-message .aside').empty();
                            }
                        });

                        // add filter
                        $('#ChooseModel').ChooseModelApp('addFilter', filter_id, {
                            type: 'radio',
                            default_value: self.data('filter-default-value')
                        })
                    }
                    else if (self.data('filter-type') == 'checkbox') {
                        // initialize the checkbox/radio filters
                        $('input:checkbox', self).bind($.browser.msie && $.browser.version.match(/^(6|7|8)\./) ? 'click' : 'click', function () {
                            // update choose model criteria
                            // get all checkboxes
                            var checkboxes = $('input:checked', self),
                            values = [];

                            if (checkboxes.size() > 0) {
                                values = ["True"];
                            }
                            else {
                                values = ["False"];
                            }

                            // update criteria
                            $('#ChooseModel').ChooseModelApp('changeCriteria', filter_id, values);

                            var msg = $(this).data('message');
                            if (msg) {
                                $('#selector-message .aside').html($(msg).html());
                            }
                            else {
                                $('#selector-message .aside').empty();
                            }
                        });

                        // add filter
                        $('#ChooseModel').ChooseModelApp('addFilter', filter_id, {
                            type: 'checkbox',
                            default_value: self.data('filter-default-value')
                        });
                    }

                });

            });


            // iterate through all product items and initialize the trims
            $('.product .product-item', '#ChooseModel').each(function () {
                var self = $(this),
                trim_id = $('> .product-image', self).data('id');

                var values = {};

                localQueue.add(function () {
                    $.each(filterIDs, function (index, filter_id) {
                        values[filter_id] = self.data('filter-' + filter_id);
                    });

                    $('#ChooseModel').ChooseModelApp('add', trim_id, {
                        id: trim_id,
                        values: values
                    });
                });
            });

            localQueue.add(function () {
                $('#ChooseModel').ChooseModelApp('activateTrims');
            });
        });
    };

    if (!window['page_init']) {
        window.page_init = function () {
            runChooseModel()
        };
    }

    return runChooseModel;
});
