;(function($) {
	$.fn.synchronised = function(fn, event) {
		return this.each(function() {
			var self = $(this);

			var task = function() {
				if (self.data('_synchronized_lock')) {
					setTimeout(function() {
						task();
					}, 100);
					return;
				}
				self.data('_synchronized_lock', true);
				if (!!event) {
					self.one(event, function() {
						self.data('_synchronized_lock', false);
					});
				}
				fn.apply(self);
				if (typeof event == 'undefined') {
					self.data('_synchronized_lock', false);
				}

			};

			task();
		});
	};
})(jQuery);