; (function ($) {

  $.fn.enable_video_player = function () {
    return this.each(function () {
      var self = $(this);
      var options = $.extend(true, {}, $.fn.linkrel.default_options, global_get('player.standard'));
      options.swf = options.swf + '?_=' + Math.random();
      // set the width and height according to the container
      var width = self.data('width') || parseInt(self.css('width'));
      var height = self.data('height') || parseInt(self.css('height'));
      if (!self.hasClass('hero') && width > 0 && height > 0) {
        options.width = width;
        options.height = height;
      }
      else if (!self.hasClass('hero')) {
        options.width = 645; // quick fix
        options.height = 395; // quick fix
      }
      var url = self.attr('title');
      var share_url = self.find('a.share').attr('href');

      if (url.match(/\.(flv|mp4|f4v|m4v)$/i)) {
        options.flashvars.xml_location = '';
        options.flashvars.video_location = url;
        if (share_url) {
          options.flashvars.share_link = share_url;
        } else {
          options.flashvars.share_link = document.location.href;
        }
      }
      else {
        options.flashvars.xml_location = url;
        options.flashvars.video_location = '';
        if (share_url != '') {
          options.flashvars.share_link = share_url;
        } else {
          options.flashvars.share_link = document.location.href;
        }
      }
      if ($.flash.available) {
        self.empty().flash(options);
        //    			self.removeAttr('title');
      }
      else {
        requires_flash_message(self);
      }
    });
  };

})(jQuery);