﻿window.page_init = function () {

    // override post back
    var oldPostBack = window.__doPostBack;

    function updateContent(content, target) {
        $(target).html(content);
    }

    window.__doPostBack = function (eventTarget, eventArgument) {
        if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
            theForm.__EVENTTARGET.value = eventTarget;
            theForm.__EVENTARGUMENT.value = eventArgument;
            // theForm.submit();

            // do ajax call
            $.ajax({
                type: 'POST'
                , dataType: 'html'
                , data: $(theForm).serialize()
                , success: function (data, status, xhr) {
                    // update view states
                    var viewState = $('input[name=__VIEWSTATE]', data).val()
                        , viewValidation = $('input[name=__EVENTVALIDATION]', data).val();

                    $('input[name=__VIEWSTATE]').val(viewState);
                    $('input[name=__EVENTVALIDATION]').val(viewValidation);

                    // update content

                    if (eventTarget.match(/DropDownListYear$/)) {
                        updateContent($('.shcedule-category', data).html(), '.shcedule-category');
                        updateContent($('.schedule-trim', data).html(), '.schedule-trim');
                    }else if (eventTarget.match(/DropDownListCategory$/)) {
                        updateContent($('.schedule-trim', data).html(), '.schedule-trim');
                    }
                    else if (eventTarget.match(/DropDownListTrims$/)) {
                        updateContent($('.schedule-schedule', data).html(), '.schedule-schedule');
                        $.behaviors.cufon_refresh('.schedule-schedule', true);
                    }
                }
            });
        }
    };

};