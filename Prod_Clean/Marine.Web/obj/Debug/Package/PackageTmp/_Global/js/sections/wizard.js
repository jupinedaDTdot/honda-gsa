window.section_init = function () {
  var height = $('.question-panel .center-container').height() + $('.question-panel .bottom-container').height();
  $('.question-panel .loader_container').height(height);
  $('.question-panel .center-container, .question-panel .bottom-container').css('visibility', 'visible');

  var category = location.pathname;
  try {
    category = category.split('/')[2];
  }
  catch (e) { }
  category = 'Wizard: ' + category;
  var action = 'Step: ' + ($('.question-number').text() || 'Results');

  $('.next a, .prev a', '.pagination-controls').click(function () {
    var label = $(this).text();
    var tracker = tracker_get();
    if (tracker) {
      tracker.trackEvent(category, action, label);
    }
  });

  $('.results .result').each(function (index) {
    var model = $('.h1', this).text();
    $('a', this).click(function (event) {
      var label = 'Select: ' + model;
      var tracker = tracker_get();
      if (tracker) {
        tracker.trackEvent(category, action, label);
      }
    });
  });

  $('.questions a.item', '.container').each(function (index) {
    // hide the radio button
    $('input:radio', this).hide();

    // check the default value
    if ($('input:radio', this).is(':checked')) {
      $(this).addClass('hover');
    }

    // hook up to the click event
    $(this).click(function (event) {
      event.preventDefault();
      // remove all siblings status
      $(this).siblings().removeClass('hover').each(function () {
        $('input:radio', this).removeAttr('checked');
      });
      $(this).addClass('hover');
      $('input:radio', this).attr('checked', 'checked');

      var label = 'Answer: ' + parseInt(index + 1) + ' ' + $('.heading', this).text();

      var tracker = tracker_get();

      if (tracker) {
        tracker.trackEvent(category, action, label);
      }

      setTimeout(function () {
        // click the next button
        window.location.href = $('.next a', '.pagination-controls').attr('href');
      }, 500);
    });

    // error checking
    $('.next a', '.pagination-controls').click(function (event) {
      if ($('input:checked', '.questions').size() == 0) {
        var msg = $('.error-message', '.container').text();
        alert(msg);
        event.stopImmediatePropagation();
        event.preventDefault();
        event.stopPropagation();
      }
    });

    setTimeout(function() {
      $('.question-panel .loader_container').fadeOut('slow', function () {
        $(this).remove();
      });
    }, 500);
  });
};