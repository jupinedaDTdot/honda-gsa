﻿<%@ Page Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true" CodeBehind="SpecialOfferDetail.aspx.cs" Inherits="Marine.Web.SpecialOffer.SpecialOfferDetail" EnableViewStateMac="false" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/current-offers.css" rel="stylesheet" type="text/css" /> 
    <link href="/_Global/css/marine/sections/current-offers.css" rel="stylesheet" type="text/css" />   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="cap_top_default pf"></div>
	<div class="container pf">
		<div class="content_container">			
			<div class="content_section first_section">
                <div class="fc">
				    <h1 class="fl"><%=ResourceManager.GetString("SpecialOffers")%></h1>
				    <a href="#" runat="server" id="aViewAll" class="fr btn secondary"><span><%=ResourceManager.GetString("txtViewAllSpecialOffer") %></span></a>
                </div>				
				<asp:Literal runat="server" ID="topImage"></asp:Literal>				
			</div>
			<div class="fc content_section primary_section">
                <h2><asp:Literal runat="server" ID="litTitle"></asp:Literal></h2>
				<asp:Literal runat="server" ID="litSummery"></asp:Literal>
			</div>
		</div>		
	</div>
	<div class="cap_bottom_default pf"></div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BottomCap" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>