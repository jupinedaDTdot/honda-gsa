﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="ProvincialOffers.aspx.cs" Inherits="Marine.Web.SpecialOffer.ProvincialOffers" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="Menu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/current-offers.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    <uc:Menu runat="server" ID="userMenu" menuMainLiCSSClass="special-offers" menuCMSControlName="GenericContentPointer_CP"/>
        <li class="find-a-dealer">
	    <uc:FindADealer ID="FindADealer1" runat="server" />
    </li>    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="content_container">
        <div class="content_section first_section">
            <h1><%=ResourceManager.GetString("txtSpecialOffers") %></h1>
        </div>
        <asp:Literal ID="litNonProvOffer" runat="server" Visible="false"></asp:Literal>

        
        <div runat="server" id="divProvincial">
            <uc:ElementControl ElementName="GenericContent_GC1" runat="server" ID="ElementControl1" />
            <div runat="server" id="divProvincial1">
            <div class="np content_section">
                <h2><%=string.Format(ResourceManager.GetString("txtProvinceOffer"), provinceName)%></h2>
            </div>            
            <input type="hidden" name="OfferID" id= "OfferID" />
            <input type="hidden" name="OfferTitle" id= "OfferTitle" />
            <input type="hidden" name="OfferProv" id= "OfferProv" />
            <asp:Repeater runat="server" ID="rptProvOffer" OnItemDataBound="rptProvOffer_ItemDataBound">
                <ItemTemplate>
                    <div class="np" id="mainDiv" runat="server">
                        <asp:Literal ID="litProvHeading" runat="server"></asp:Literal>
                        <div class="fc offer-container">
                            <div class="fl offer-image" runat="server">
                                <asp:Image ID="imgOffer" runat="server" />
                            </div>
                            <div class="fl offer-content">
                                <p class="heading_large"><asp:Literal runat="server" ID="litTitle"></asp:Literal></p>
                                <p><asp:Literal runat="server" ID="litSummery"></asp:Literal></p>
                                <a href="#" class="btn primary" runat="server" id="lnkFindMore"><span><asp:Literal runat="server" ID="litMoreText"></asp:Literal></span></a>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            </div>
        </div>
        
        <div runat="server" id="divNational">
            <uc:ElementControl runat="server" ID="ElementControl2" ElementName="GenericContentPointer_CP2" />
            <div runat="server" id="divNational1">
        <%--<asp:Literal ID="LitNonNationalOffer" runat="server"></asp:Literal>--%>
            <div class="np content_section">
                <h2><%=string.Format(ResourceManager.GetString("txtNationalOffers"), provinceName)%></h2>
            </div>            
            <asp:Repeater runat="server" ID="rptNationalOffer" OnItemDataBound="rptNationalOffer_ItemDataBound">
                <ItemTemplate>                    
                    <div class="np content_section" id="mainDiv" runat="server">
                        <asp:Literal runat="server" ID="litProvHeading"></asp:Literal>
                        <div class="fc offer-container">
                            <div class="fl offer-image">
                                <asp:Image runat="server" ID="imgOffer"/>
                            </div>
                            <div class="fl offer-content">
                                <p class="heading_large"><asp:Literal runat="server" ID="litTitle"></asp:Literal></p>
                                <p><asp:Literal runat="server" ID="litSummery"></asp:Literal></p>
                                <a href="#" class="btn primary" runat="server" id="lnkFindMore"><span><asp:Literal runat="server" ID="litMoreText"></asp:Literal></span></a>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
<script>
  function ShowOfferDetail(id, title, url, Prov) {
        var form = document.getElementsByTagName('form')[0];

        $('input[name="OfferID"]').val(id);
        $('input[name="OfferTitle"]').val(title);
        $('input[name="OfferProv"]').val(Prov);
        form.action = url;
        form.submit();
    }
    </script>
</asp:Content>
