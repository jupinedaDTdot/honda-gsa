﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="PartAndService.aspx.cs" Inherits="Marine.Web.ContentPages.PartAndService" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="Menu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>
<%@ Register Src="~/_Global/Controls/tabMenu.ascx" TagPrefix="uc" TagName="TabMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/parts-service.css" rel="stylesheet" type="text/css" />
    <link href="/_Global/css/marine/sections/parts-service.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    <uc:Menu ID="userMenu" runat="server" menuMainLiCSSClass="menu menu-parts-service" menuCMSControlName="GenericContentPointer_CP"></uc:Menu>			                   
	<li class="powerhouse-dealers-alternate">
	    <div class="top_cap pf"></div>
	    <div class="content pf">
		    <a href="<%=ResourceManager.GetString("urlPowerhouseDealers")%>">
			    <img src="<%=ResourceManager.GetString("pathPowerHouseImage") %>" alt="" />
		    </a>
		    <p class="heading"><%=ResourceManager.GetString("HondaPowerHouseDealer")%></p>
	    </div>
	    <div class="bottom_cap pf"></div>
    </li>
	<li class="accessories" runat="server">
        <div class="top_cap pf"></div>
	    <div class="content pf">
		    <a href="<%=ResourceManager.GetString("urlOilsAndChemicals")%>">
			    <img src="/_Global/img/layout/accessories-sidebar-img.gif" alt="" />
		    </a>
		    <p class="heading"><%=ResourceManager.GetString("txtGenuineHondaOilsAndChemicals")%></p>
	    </div>
	    <div class="bottom_cap pf"></div>
    </li>
    <li class="find-a-dealer">
	    <uc:FindADealer ID="FindADealer1" runat="server" />
    </li>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentArea" runat="server">
    <asp:Literal ID="litContentTopDiv" runat="server"></asp:Literal>
    <div id="divTout" runat="server" visible="false">
        <uc:ElementControl ID="ElementControl1" runat="server" ElementName="GenericContent_Tout" />
        <p>
            <asp:PlaceHolder ID="plhTopTout" runat="server"></asp:PlaceHolder>
        </p>
    </div>        
    <div>
        <uc:ElementControl id="eleControlMiddle1" runat="server"  ElementName="GenericContent_FFH" Visible="true" />         
        <asp:PlaceHolder ID="plhMiddleTopContent" runat="server" > </asp:PlaceHolder>
    </div>    
    <asp:Literal ID="litContentBottomDiv" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>
