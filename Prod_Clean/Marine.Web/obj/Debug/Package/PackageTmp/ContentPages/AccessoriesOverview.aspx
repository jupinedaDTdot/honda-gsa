﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="AccessoriesOverview.aspx.cs" Inherits="Marine.Web.ContentPages.AccessoriesOverview" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="Menu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="/_Global/css/sections/accessories.css" />    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    <uc:Menu ID="userMenu" runat="server" menuMainLiCSSClass="menu-accessories" menuCMSControlName="GenericContentPointer_CP"></uc:Menu>
    <li class="find-a-dealer">
	    <uc:FindADealer ID="FindADealer" runat="server" />
    </li>
    <li id="Li1" class="accessories" runat="server">
        <div class="top_cap pf"></div>
	    <div class="content pf">
		    <a href="<%=ResourceManager.GetString("PartsServicesUrl")%>">
			    <img src="/_Global/img/layout/accessories-sidebar-img.gif" alt="" />
		    </a>
		    <p class="heading"><%=ResourceManager.GetString("PartsServices")%></p>
	    </div>
	    <div class="bottom_cap pf"></div>
    </li>  
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentArea" runat="server">
<div class="overview">
	<div class="content_container">
    	<div class="content_section first_section">
			<uc:ElementControl runat="server" ID="ElementControl1" ElementName="GenericContent_Tout" />
            <asp:PlaceHolder runat="server" ID="plhMainTout"></asp:PlaceHolder>
		</div>
		<div class="content_section">			
            <uc:ElementControl runat="server" ID="ElementControl3" ElementName="GenericContent_FFH2" />
            <asp:PlaceHolder runat="server" ID="plhMainFFH2"></asp:PlaceHolder>            
		</div>
        <div class="content_section_dotted">
            <uc:ElementControl runat="server" ID="ElementControl2" ElementName="GenericContent_FFH" />
            <asp:PlaceHolder runat="server" ID="plhMainFFH"></asp:PlaceHolder>            
        </div>
	</div>
</div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>