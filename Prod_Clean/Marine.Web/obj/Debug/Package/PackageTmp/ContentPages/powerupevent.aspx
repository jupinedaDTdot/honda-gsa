﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true" CodeBehind="powerupevent.aspx.cs" Inherits="Marine.Web.ContentPages.powerupevent" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagName="ElementControl" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/marine/sections/power-up.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
    <div class="container">
        <div class="content_container" id="powerup-banner">
            <uc:ElementControl runat="server" ID="ElementControl1" ElementName="GenericContent_Tout" />
            <asp:Literal ID="TopTout" runat="server"></asp:Literal>
        </div>

        <div class="content_container" id="powerup-container">
            <uc:ElementControl runat="server" ID="ElementControl2" ElementName="GenericContent_FFH" />
            <asp:Literal ID="MiddleContent" runat="server"></asp:Literal>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BottomCap" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>
