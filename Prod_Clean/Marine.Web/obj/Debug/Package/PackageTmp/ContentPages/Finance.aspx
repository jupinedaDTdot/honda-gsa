﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="Finance.aspx.cs" Inherits="Marine.Web.ContentPages.Finance" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="Menu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
    <link href="/_Global/css/sections/finance.css" rel="stylesheet" type="text/css" />
    <link href="/_Global/css/marine/sections/finance.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    <uc:Menu ID="userMenu" runat="server" menuMainLiCSSClass="menu-finance" menuCMSControlName="GenericContentPointer_CP"></uc:Menu>
    <li class="find-a-dealer">
	    <uc:FindADealer ID="FindADealer" runat="server" />
    </li>    

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="content_container">
        <div class="<%=cssClassContainer %>">
            <div>
                <uc:ElementControl ID="ElementControl1" runat="server" ElementName="GenericContent_Tout" />
                <p>
                    <asp:PlaceHolder ID="plhTopTout" runat="server"></asp:PlaceHolder>
                </p>
            </div>        
            <div>
                <uc:ElementControl id="eleControlMiddle1" runat="server"  ElementName="GenericContent_FFH" Visible="true" />         
                <asp:PlaceHolder ID="plhMiddleTopContent" runat="server" > </asp:PlaceHolder>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
  <script type="text/javascript" src="/_Global/js/sections/marine/finance.js"></script>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>

