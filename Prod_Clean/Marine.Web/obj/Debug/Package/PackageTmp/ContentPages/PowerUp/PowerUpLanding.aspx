﻿<%@ Page Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true"
    CodeBehind="PowerUpLanding.aspx.cs" Inherits="Marine.Web.ContentPages.PowerUp.PowerUpLanding" %>

<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <link rel="stylesheet" href="/_Global/css/marine/sections/spring-sales.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="cap_top_default pf"></div>
	<div class="container pf">
		<div class="content_container spring-sales wide_splash">
			<div class="content_section splash" runat="server" id="splashContainer">
				<div class="banner">
                    <uc:ElementControl runat="server" ID="Element1" ElementName="GenericContent_Tout" />
                    <asp:PlaceHolder ID="PlaceHolderTout" runat="server"></asp:PlaceHolder>
                </div>
                <div>
                    <uc:ElementControl runat="server" ID="Element2" ElementName="GenericContent_FFH" />
                    <asp:Literal ID="LiteralTopText" runat="server"></asp:Literal>
                </div>
				<div class="fc spring-disclaimer">
                    <uc:ElementControl runat="server" ID="ElementControl1" ElementName="GenericContent_FFH2" />
                    <asp:Literal ID="LiteralBottomText" runat="server"></asp:Literal>
				</div>
			</div>
			<div class="bottom_cap pf"></div>
		</div>
	</div>
	<div class="cap_bottom_default pf"></div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BottomCap" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd"
    runat="server">
</asp:Content>
