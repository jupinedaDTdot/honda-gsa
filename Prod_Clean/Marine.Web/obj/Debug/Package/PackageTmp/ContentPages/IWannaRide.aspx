﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsTwoColMaster.master" AutoEventWireup="true" CodeBehind="IWannaRide.aspx.cs" Inherits="Marine.Web.ContentPages.IWannaRide" %>

<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/LeftSideMenu.ascx" TagPrefix="uc" TagName="LeftSideMenu" %>
<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagPrefix="uc" TagName="FindADealer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/news-events.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/_Global/css/pe/news-events.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server"></asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server"></asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Widgets" runat="server">
    <uc:LeftSideMenu ID="leftsidemenu1" runat="server" menuMainLiCSSClass="menu-news-events" menuCMSControlName="GenericContent_Menu" />
    <li class="find-a-dealer">
        <uc:FindADealer ID="findadealer1" runat="server" />
    </li>    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BgIllustration" runat="server"></asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ContentArea" runat="server">
<div class="content_container">
    <div class="content_section first_section">
        <div class="tout_slideshow">
            <asp:PlaceHolder runat="server" ID="plhMainTout"></asp:PlaceHolder>
        </div>
        <div>
            <asp:Literal ID="litFFH" runat="server"></asp:Literal> 
        </div>
	</div>
	<div class="content_section">
        <asp:PlaceHolder ID="plhGC" runat="server"></asp:PlaceHolder>
	</div>
</div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="insertDivatBottom" runat="server"></asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="includeModelVarJS" runat="server"></asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server"></asp:Content>
