﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComparePopUpSelector.aspx.cs" Inherits="Marine.Web.Service.ComparePopUpSelector" %>

<div runat="server" id="divMain">
    <asp:placeholder runat="server" id="seriesSelector" visible="True">                
        <h3 runat="server" id="labelSelect"><%=ResourceManager.GetString("txtSelectCategory")%></h3>
        <asp:Repeater runat="server" ID="rptSeries" OnItemDataBound="rptSeries_ItemDataBound" > 
        <HeaderTemplate>
            <select property="compare:series-selector">
        </HeaderTemplate>
        <ItemTemplate>
            <asp:Literal runat="server" ID="OptionItem" > </asp:Literal>            
        </ItemTemplate>
        <FooterTemplate>
            </select>
        </FooterTemplate>
        </asp:Repeater>
    <div class="line"></div>
 </asp:placeholder>
    
    <div class="selection_wrap">
        <div class="scrollview">
            <div class="subview">
                <asp:repeater runat="server" id="rptTrims" onitemdatabound="rptTrims_ItemDataBound"> 
                    <ItemTemplate>            
                        <div class="item"
                            data-id="<%# Eval("Trim.TrimExportKey")%>-<%#Eval("Trim.ModelYearYear")%>"
                            data-trim-id="<%# Eval("Trim.TrimExportKey")%>-<%#Eval("Trim.ModelYearYear")%>"
                            data-model-category="<%#Eval("Trim.ModelCategoryURL")%>"
                            data-item-text-msrp="<%= ResourceManager.GetString("MSRP") %>"
                            data-item-text-final-price="<%= ResourceManager.GetString("YourPrice") %>"
                            data-item-text-discount="<%= ResourceManager.GetString("Discount") %>"
							              data-item-coming-soon="<%# Eval("IsComingSoon").ToString().ToLower() %>"
                            data-item-has-discount="<%# Eval("HasDiscount").ToString().ToLower() %>"
                            data-item-final-price="<%# string.Format("{0:C0}", Eval("Price")) %>"
                            data-item-discount="<%# string.Format("{0:C0}", Eval("Discount")) %>"
                            data-item-price="<%# String.Format("{0:C0}", Eval("Trim.MSRP")) %>"
                            data-item-title="<%# Eval("Trim.TrimName") %>"
                            >
                            <div class="product-image" runat="server" id="div_image">
                                <img src="<%# Eval("Thumbnail") %>?Crop=auto&Width=90&Height=125"/>
                            </div>
                            <h3 class="price-heading"><%# Eval("Trim.TrimName") %></h3>
							<asp:PlaceHolder runat="server" id="phComingSoon" visible="false">
                            <div class="tag-no-discount">
                                <span class="price tag-price coming-soon"><%= ResourceManager.GetString("txtComingSoon") %></span>
                                <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                            </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" id="phNoDiscount" visible="false">
                            <div class="tag-no-discount">
                                <span class="price tag-price"><%# String.Format("{0:C0}", Eval("MSRP"))%></span>
                                <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                            </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" id="phHasDiscount" visible="false">
                            <div class="tag-has-discount">
                                <div class="fc original price">
                                    <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP")%></span>
                                    <del><span class="strike"></span><strong class="tag-price"><%# String.Format("{0:C0}", Eval("MSRP"))%></strong></del>                                    
                                </div>
                                <div class="fc special price">
                                    <span class="tag-text-discount msrp"><%=ResourceManager.GetString("Discount")%></span>
                                    <strong class="tag-discount"><%# String.Format("{0:C0}", Eval("Discount"))%></strong>                                        
                                </div>
                                <div class="fc final price">
                                    <span class="tag-text-final-price msrp"><%=ResourceManager.GetString("YourPrice")%></span>
                                    <strong class="tag-final-price"><%# String.Format("{0:C0}", Eval("Price"))%></strong>                                    
                                </div>
                            </div>
                            </asp:PlaceHolder>
                        </div>
                    </ItemTemplate>
                </asp:repeater>
            </div>
        </div>
    </div>
</div>
