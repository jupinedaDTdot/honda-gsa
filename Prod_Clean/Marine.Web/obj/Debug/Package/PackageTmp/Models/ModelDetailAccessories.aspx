﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/ModelPage.master" AutoEventWireup="true" CodeBehind="ModelDetailAccessories.aspx.cs" Inherits="Marine.Web.Models.ModelDetailAccessories" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentArea" runat="server">
<div class="accessories">
    <div class="fc">
	<div class="fl content_section first_section">
        <div class="h1"><%=ResourceManager.GetString("Accessories")%></div>
        <asp:HyperLink ID="HyperLinkDownloadPdf" CssClass="fr download" runat="server" Visible="False"></asp:HyperLink>
	</div>
    <div class="fc fr accordion-controls">
        <a property="act:collapsible-collapse" class="fl collapse" href="javascript:void(0)"><%=ResourceManager.GetString("txtCollapseAll")%></a>
        <a property="act:collapsible-expand" class="fl expand" href="javascript:void(0)"><%=ResourceManager.GetString("txtExpandAll")%></a>
    </div>

    <div id="Div1" class="fc" runat="server" visible="false">
        <div id="Div2" class="province-selector fr" runat="server" visible="false">
            <select id="accessory_province"><option value="BC" class="95.00">British Columbia</option><option value="AB" class="95.00">Alberta</option><option value="SK" class="95.00">Saskatchewan</option><option value="MB" class="95.00">Manitoba</option><option value="ON" class="95.00">Ontario</option><option value="QC" class="95.00">Quebec</option><option value="NB" class="95.00">New Brunswick</option><option value="NS" class="95.00">Nova Scotia</option><option value="PE" class="95.00">Prince Edward Island</option><option value="NL" class="95.00">Newfoundland and Labrador</option><option value="YT" class="95.00">Yukon</option> </select>
        </div>
    </div>
    </div>
    <asp:Panel ID="PanelAccessoriesResult" runat="server" Visible="true">
	    <div class="nb nm content_section honda_accessories_enabled">
		    <div class="flex_bar" runat="server" visible="false">
			    <span>
                    <a href="javascript:void(0);"
                                property="act:collapsible"
                                data-collapsible-target="#accessories_list"
                                data-collapsible-active="true"
                                ><asp:Literal ID="LiteralAccessoryTitle" runat="server"></asp:Literal></a>
                </span>
		    </div>
            <div id="accessories_list" class="sub_content">
                <%--<ul>
                    <asp:Literal ID="LiteralAccessoryItems" runat="server"></asp:Literal>
			    </ul>--%>
            </div>
            <asp:Literal ID="LiteralAccessoryItems" runat="server"></asp:Literal>
	    </div>
        <div class="disclaimer"><%=ResourceManager.GetString("txtAccessoriesDisclaimer") %></div>
        <asp:Literal ID="LiteralAccessoryToolTips" runat="server"></asp:Literal>
    </asp:Panel>
    <%--<div class="nb content_section honda_accessories_enabled">
    </div>--%>
</div>
</asp:Content>