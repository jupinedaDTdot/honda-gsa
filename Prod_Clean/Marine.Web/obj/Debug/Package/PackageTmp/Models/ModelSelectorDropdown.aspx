﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="ModelSelectorDropdown.aspx.cs" Inherits="Marine.Web.Models.ModelSelectorDropdown" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:act="http://honda.ca/ns#">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="en-us" />
    <title>Honda Canada</title>
	<link rel="stylesheet" href="/_Global/css/common/reset.css" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="/_Global/css/common/layout.css" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="/_Global/css/common/fr/layout.css" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="/_Global/css/light/layout.css" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="/_Global/css/sections/landing.css"  type="text/css" media="screen" charset="utf-8" />

	<!--[if IE]>
	<link rel="stylesheet" href="/_Global/css/common/ie.css" type="text/css" media="screen" charset="utf-8" />
	<![EndIf]-->
</head>
<body class="page-landing">
<div class="wrapper dropdown_container navigation-dropdown">
	<div class="cap_top_default pf"></div>
	<div runat="server" id="divCssChange">
		<div class="content_container">
			<div class="fc content_section first_section" id="navigation_content">
				<div class="fl why-buy">
                    <uc:ElementControl runat="server" ID="ElementControl" ElementName="GenericContentPointer_CP" />
				    <asp:PlaceHolder runat="server" ID="plmenuFFH"></asp:PlaceHolder>
				</div>
				<div class="fl right-boxes" runat="server" id="oneColumn">
                    <div class="fc subview">
                        <div class="fl series" runat="server" id="divSeries">
                            <div class="see-all">
                                <p>
                                    <a href="<%=ResourceManager.GetString("urlSeeAllOutBoardMotor") %>"><%=ResourceManager.GetString("txtHondaModelAllModels")%></a>
                                </p>
                            </div>
                            <p class="heading"><asp:Literal ID="litHeading" runat="server"></asp:Literal></p>
							<p><asp:Literal ID="litHeadDescription" runat="server"></asp:Literal></p>                            							
							<asp:Literal ID="litUsage" runat="server"></asp:Literal>
                        </div>
                        <div class="products">
                            <div class="subview">
                                <asp:PlaceHolder ID="plhtrimlist" runat="server"></asp:PlaceHolder>
                            </div>
                        </div>
                    </div>
				</div>

			</div>
		</div>
	</div>
	<div class="cap_bottom_default pf"></div>
</div>

<script src="/_Global/js/core/selectivizr-1.0.3b.js" type="text/javascript" charset="utf-8"></script>
<!--[if IE 6]>
<script src="/_Global/js/core/libpngfix.js" type="text/javascript" charset="utf-8"></script>
<![endif]-->
<script type="text/javascript">
    var __hsite = 'marine';
    var require = {
        baseUrl: '/_Global/js',
        paths: {
            'i18n': 'ext/require/i18n',
            'order': 'ext/require/order',
            'text': 'ext/require/text',
            'commonlib': 'ext/commonlib',
            'ui': 'ext/ui',
            'tools': 'ext/tools'
        }
    };
</script>
<script src="/_Global/js/core/require-jquery.js" type="text/javascript" charset="utf-8"></script>

<script src="/_Global/js/core/cufon.js"></script>
<script src="/_Global/js/app.js" type="text/javascript" charset="utf-8"></script>

<!-- BEGIN: PAGE SPECIFIC SCRIPTS -->

<!-- END  : page SPECIFIC SCRIPTS -->
</body>
</html>