﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModelPopup.aspx.cs" Inherits="Marine.Web.Models.ModelPopup" %>
<%@ Import Namespace="Marine.Service.Extensions" %>
<div>
	<div class="fc popup-top">
		<div class="fl product-title-price">
			  <div class="h1"><%=trim.TrimName%></div>			        
        <div class="prices">			
			    <asp:PlaceHolder runat='server' id='phComingSoon' visible='false'>
                                <div class="fc small-price msrp">
					<span class="price coming-soon"><%=ResourceManager.GetString("txtComingSoon")%></span>
                    <span class="price-heading"><%= ResourceManager.GetString("MSRP") %></span>
				</div>
                </asp:PlaceHolder>

			    <asp:PlaceHolder runat='server' id='phCurrentModel'>
          <%if (trim.HasDiscount()) { %>
          <div class="fc small-price msrp">
						<del>
							<span class="strike"></span>
							<span class="price"><%=string.Format(ResourceManager.GetString("valMSRP4"), MSRP)%></span>
						</del>
						<span class="price-heading"><%=ResourceManager.GetString("MSRP")%></span>
					</div>
					<div class="fc small-price discount">
						<span class="price"><%=string.Format(ResourceManager.GetString("valMSRP4"), DiscountAmount)%></span>
						<span class="price-heading"><%=ResourceManager.GetString("Discount")%></span>
					</div>
					<div class="fc large-price your-price">
						<span class="price"><%=string.Format(ResourceManager.GetString("valMsrp"), "$", DiscountedPrice)%></span>

						<span class="price-heading"><%=ResourceManager.GetString("YourPrice")%></span>
					</div>
          <% }
             else {%>
          <div class="fc large-price mdp">
					  <span class="price"><%=string.Format(ResourceManager.GetString("valMsrp"), "$", MSRP)%></span>
					  <%--<span class="price-heading"><%=ResourceManager.GetString("ManufacturerDiscountedPrice") %></span>--%>
					  <span class="price-heading"><%=ResourceManager.GetString("MSRP")%></span>
				  </div>
          <%} %>
			    </asp:PlaceHolder>
            </div>
        </div>
        <div class="fr product-image"><img src="<%= imageURL %>?Crop=auto&amp;Width=90&amp;Height=125" alt="<%= trim.TrimName%>" /></div>    
    </div>
		
	<div class="popup-content">
		<h3><%=ResourceManager.GetString("Features")%></h3>
		<ul>
            <%=features %>
		</ul>
	</div>
		
	<div class="fc popup-bottom compare-buttons">
		<div class="fl view-details">
			<a href="<%= baseURL %>" class="btn primary"><span><%=ResourceManager.GetString("ViewDetails")%></span></a>
		</div>
		<div class="fl pf compare-add-remove add">
            <a href="javascript:void(0)"
                data-id="<%=trim.TrimExportKey %>-<%= trim.ModelYearYear %>"
                data-item-title="<%=trim.TrimName%>"
                data-item-price="<%= String.Format("{0:C0}", trim.MSRP) %>"
                data-item-discount="<%= String.Format("{0:C0}", DiscountAmount) %>"
				        data-item-coming-soon="<%= IsComingSoon.ToString().ToLower() %>"
                data-item-final-price="<%= String.Format("{0:C0}", DiscountedPrice) %>"
                data-item-has-discount="<%= HasDiscount.ToString().ToLower() %>"
                data-item-text-discount="<%= ResourceManager.GetString("Discount") %>"
                data-item-text-final-price="<%= ResourceManager.GetString("YourPrice") %>"
                data-item-text-msrp="<%=ResourceManager.GetString("MSRP") %>"><%=ResourceManager.GetString("AddtoCompare") %></a>
		</div>
        <div class="fl pf compare-add-remove remove dn">
			<a href="javascript:void(0)" data-id="<%=trim.TrimExportKey %>-<%= trim.ModelYearYear %>"><%=ResourceManager.GetString("RemoveFromCompare") %></a>
		</div>
	</div>
</div>
	