﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintModelSpecs.aspx.cs" Inherits="Marine.Web.Models.PrintModelSpecs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"  xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://ogp.me/ns#" class="no-js">
<head runat="server">
    <base href="<%=domain%>"/>
    <asp:Literal ID="litHeaderLink" runat="server"/>
    <title></title>
</head>
<body runat="server" id="body" class="page-product-specs-print">
    <div class="page-product-specs-content-container">
        <div class="fc product-header">
            <div class="fl product-left-col">
            <%if (this.PageLanguage == HondaCA.Common.Language.French) { %>
                <div class="honda-logo-french"></div>
                <% }else { %>
                <div class="honda-logo"></div>
                <%} %>
                <div class="product-name"><asp:Literal ID="LiteralPgeHeading" runat="server"></asp:Literal></div>
            </div>
            <div class="fr product-image"><asp:Literal ID="LiteralModelImage" runat="server"></asp:Literal></div>
        </div>
 
        <table class="specs_chart">    	   
        <asp:Repeater ID="RepeaterSpecs" runat="server" OnItemDataBound="RepeaterSpecs_ItemDataBound">
            <ItemTemplate>
                <tr><th colspan="2" class="category"><%#Eval("SpecCategoryDescription")%></th></tr>            
                <asp:Repeater ID="RepCatItem" runat="server">
                    <ItemTemplate>
                        <tr class="<%# Container.ItemIndex == 0 ? " first" : Container.ItemIndex  == SpecCount -1  ? " last" : "" %>">
	                        <th><a><%# Eval("SpecLabel")%></a></th>
                            <td><%# Eval("SpecValue")%></td>
	                    </tr> 
                    </ItemTemplate>
                </asp:Repeater>            
            </ItemTemplate>
        </asp:Repeater>
        </table>
        <div class="specs">
          <div class="content_section first_section">
             <div class="content_section">
                <asp:Literal ID="litDisclaimer" runat="server"></asp:Literal> 
             </div>
             <div class="content_section">
                <asp:Literal ID="litWarranty" runat="server"></asp:Literal> 
             </div>
          </div>
      </div>
  </div>  
</body>
</html>
