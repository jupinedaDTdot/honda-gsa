﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="Marine.Web.ErrorPages" EnableViewState="false"  MasterPageFile="~/_Global/master/TwoColMaster.master" %>

<asp:Content ID="contentHead" runat="server" ContentPlaceHolderID="head"></asp:Content>

<asp:Content ID="LeftColContent" ContentPlaceHolderID="Widgets" runat="server">
    <li class="find-a-dealer">
    </li>
</asp:Content>

<asp:Content ID="RightColContent" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="content_container">
        <div class="content_section first_section">
            <asp:Literal ID="litErrorText" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>