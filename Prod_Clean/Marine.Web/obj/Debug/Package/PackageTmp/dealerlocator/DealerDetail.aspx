﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DealerDetail.aspx.cs" Inherits="Marine.Web.dealerlocator.DealerDetail" EnableViewState="false" MasterPageFile="~/_Global/master/TwoColMaster.master" %>

<%@Register  Src="~/_Global/Controls/FindADealer.ascx"  TagName="FindADealer" TagPrefix="uc2"%>

<asp:Content ID="contentHead" runat="server" ContentPlaceHolderID="head">
	<link rel="stylesheet" href="/_Global/css/sections/find-a-dealer.css" type="text/css" media="screen" charset="utf-8" />
	<style type="text/css">
		v\:* { behavior: url(#default#VML); }
	</style>
</asp:Content>

<%--<asp:Content runat="server" ID="extenedwraper" ContentPlaceHolderID="wrapper_extended_placeholder_feature_area">
    <div class="wrapper_extended">
		<div id="feature_area">
			<div class="hero">
				<img alt="" src="/_Global/img/layout/placeholder_feature_area.png">
			</div>
		</div>
		<div class="cap_bottom_default"></div>
	</div>
</asp:Content>--%>

<asp:Content ID="LeftColContent" ContentPlaceHolderID="Widgets" runat="server">
	<li class="find-a-dealer"><uc2:FindADealer ID="FindADealer1" runat="server" /></li>
	<li class="honda-dealerships">
		<div class="top_cap pf"></div>
		<div class="content pf">
			<h4><asp:Literal runat="server" ID="LitCities_Near_You"></asp:Literal></h4>
			<ul id="left_nav">
				<asp:Repeater runat="server" ID="rptCityList" OnItemDataBound="rptCityList_ItemDataBound"  >               
					<ItemTemplate>	
						<li runat="server" id="li"><asp:HyperLink ID="hypCity" runat="server" /></li>
					</ItemTemplate>
				</asp:Repeater>	
			</ul>
		</div>
		<div class="bottom_cap pf"></div>
	</li>
	<li class="powerhouse_dealers">
		<div class="top_cap pf"></div>
		<div class="content pf">
			<img src="<%=ResourceManager.GetString("pathPowerHouseImage2") %>" alt="" />
			<p class="heading"><asp:Literal ID="LiteralPowerHouseDealerHeader" runat="server"></asp:Literal></p>
			<asp:Literal ID="LiteralPowerHouseDealerBlurb" runat="server"></asp:Literal>
			<a href="<%=ResourceManager.GetString("urlPowerhouseDealers")%>" class="btn secondary"><span><%=ResourceManager.GetString("LearnMore")%></span></a>
		</div>
		<div class="bottom_cap pf"></div>
	</li>
</asp:Content>

<asp:Content ID="RightColContent" ContentPlaceHolderID="ContentArea" runat="server">
	<div class="content_container">
		<div class="content_section first_section">
			<h1><asp:Literal runat="server" ID="LitDealerName"></asp:Literal></h1>
			<div class="sub_section address">
				<div class="address-info">
					<div class="img_rounded_corners pf fl"><img class="fl db photo" runat="server" id="imgDealerImage" alt="" /></div>
					<div class="fl copy">
						<div class="powerhouse-logo" id="divPowerhouseLogo" runat="server" visible="false"></div>
						<p class="heading"><asp:Literal runat="server" ID="LitAddress"></asp:Literal></p>
						<p class="adr">
							<span class="street-address"><asp:Literal runat="server" ID="LitStreet"></asp:Literal></span><br />
							<span class="locality"><asp:Literal runat="server" ID="LitLocality"></asp:Literal>,</span>
							<on class="region"><asp:Literal runat="server" ID="LitRegionCode"></asp:Literal></on><br />
							<span class="postal-code"><asp:Literal runat="server" ID="LitPostalCode"></asp:Literal></span><br />
						</p>
						<p>
							<asp:Literal runat="server" ID="LitPhone"></asp:Literal>
							<asp:Literal runat="server" ID="LitFax"></asp:Literal>
							<asp:Literal runat="server" ID="LitEmail"></asp:Literal>
							<asp:Literal runat="server" ID="LitWeb"></asp:Literal>
						</p>
					</div>
					<div class="clr"></div>
				</div>
				<div class="fc sub_section directions">
					<div class="gmap_result_detail fl">
						<div class="bg"></div>
						<%--<cc1:GMap ID="GMap1" runat="server" Width="340px" Height="195px" />--%>
						<div id="map-canvas" style="width: 340px; height: 195px;"></div>
						<%--center, zoom--%>
						<input type="hidden" id="MapDataView" runat="server" class="view-json-data" />
						<%--locations to pin--%>
						<div id="MapDataLocations" runat="server" class="dealers-json-data dn" />
					</div>
					<div class="fl directions-copy">
						<p class="heading"><asp:Literal runat="server" ID="LitDirection"></asp:Literal></p>
						<p><asp:Literal runat="server" ID="LitDirectionText"></asp:Literal><p> 
					</div>
				</div>
			</div>
		</div>
		<div id="divProductLine" class="content_section" runat="server" visible="true">
			<h2><%=ResourceManager.GetString("txtProductLinesCarried") %></h2>
			<dl class="product-lines">
				<asp:Repeater ID="RepeaterProductLine" runat="server" OnItemDataBound="RepeaterProductLine_ItemDataBound">
					<ItemTemplate>
						<dd id="ddProductLine" runat="server">
							<p class="heading"><asp:Literal ID="LiteralProdutLineName" runat="server"></asp:Literal></p>
							<p><asp:Literal ID="LiteralProductLineDescription" runat="server"></asp:Literal></p>
						</dd>
					</ItemTemplate>
				</asp:Repeater>
				<asp:Literal ID="LiteralBigRedDealer" runat="server" Visible="false"></asp:Literal>
			</dl>
		</div>
		<div class="fc content_section about-this-dealer" id="divDealerBlurb" runat="server" visible="false">
			<h2><asp:Literal runat="server" id="LitaboutLabel"></asp:Literal></h2>
			<div class="fl image"><img runat="server" id ="imgQualityDealer" alt="" class="fl" src=""/></div>
			<div class="fl content">
				<p class="heading"><%=ResourceManager.GetString("txtSpeedTrustConvenience") %></p>
				<asp:Literal runat="server" ID="LitQualityDealerText" ></asp:Literal>
			</div>
		</div>
		<div class="content_section hours_section" id="divHoursSection" runat="server">
			<h2><%=ResourceManager.GetString("txtHours") %></h2>
			<div class="subsection fl nm">
				<p class="heading"><asp:Literal ID="littxtSales" runat="server"></asp:Literal></p>
				<table class="tbl hours first">
					<tbody>
						<asp:Repeater runat="server" ID="rptDealerSalesHour" Visible="true" EnableViewState="true" OnItemDataBound="Repeater_ItemDataBound">
							<ItemTemplate>
								<tr runat="server" id="tr_rpt">
									<td class="first odd"><div><span><asp:Literal ID="LitDay" runat="server" /></span></div></td>
									<td class="last even"><div><span><asp:Literal ID="LitHours" runat="server" /></span></div></td>
								</tr>
							</ItemTemplate>
						</asp:Repeater>			
					</tbody>
				</table>
				<p class="contact_info"><asp:Literal ID="litSalesPhoneFax" runat="server"></asp:Literal></p>
				<br />
				<p>
					<asp:Literal ID="litSalesNotes" runat="server"></asp:Literal>
				</p>
			</div>
			<div class="subsection fl">
				<p class="heading"><asp:Literal ID="littxtParts" runat="server"></asp:Literal></p>
				<table class="tbl hours">
					<tbody>
						<asp:Repeater runat="server" ID="rptDealerPartsHour" Visible="true" EnableViewState="true" OnItemDataBound="Repeater_ItemDataBound">
							<ItemTemplate>
								<tr runat="server" id="tr_rpt">
									<td class="first odd"><div><span><asp:Literal ID="LitDay" runat="server" /></span></div></td>
									<td class="last even"><div><span><asp:Literal ID="LitHours" runat="server" /></span></div></td>
								</tr>
							</ItemTemplate>
						</asp:Repeater>
					</tbody>
				</table>
				<p class="contact_info"><asp:Literal ID="litPartsPhoneFax" runat="server"></asp:Literal></p>
				<br />
				<p>
					<asp:Literal ID="litPartsNotes" runat="server"></asp:Literal>
				</p>
			</div>
			<div class="subsection fl">
				<p class="heading"><asp:Literal ID="littxtService" runat="server"></asp:Literal></p>
				<table class="tbl hours last">
					<tbody>
						<asp:Repeater runat="server" ID="rptDealerServiceHour" Visible="true" EnableViewState="true" OnItemDataBound="Repeater_ItemDataBound">
							<ItemTemplate>
								<tr runat="server" id="tr_rpt">
									<td class="first odd"><div><span><asp:Literal ID="LitDay" runat="server" /></span></div></td>
									<td class="last even"><div><span><asp:Literal ID="LitHours" runat="server" /></span></div></td>
								</tr>
							</ItemTemplate>
						</asp:Repeater>	
					</tbody>
				</table>
				<p class="contact_info"><asp:Literal ID="litServicePhoneFax" runat="server"></asp:Literal></p>
				<br/>
				<p>
					<asp:Literal ID="litServiceNotes" runat="server"></asp:Literal>
				</p>
			</div>
			<div class="clr"></div>			
		</div>          
	</div>       		
</asp:Content>

<asp:Content ID="JavaScript" ContentPlaceHolderID="IncludeJavaScripatEnd" runat="server">
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<%=ConfigurationManager.AppSettings["GoogleMapKey:Honda"]%>&sensor=false"> </script>
	<script type="text/javascript" src="/_Global/js/maps/googlemaps.js"></script>
	<script type="text/javascript">
		window.page_init = function () {
			var config = $("input.view-json-data").attr('value');
			var data = $("div.dealers-json-data").text();
			var areaId = 'map-canvas';
			ShowMap(config, data, areaId, '/_Global/img/Honda_pin/honda_pin.png');
		};
	</script>
</asp:Content>
