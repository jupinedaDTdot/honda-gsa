﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FindDealer.aspx.cs" Inherits="Marine.Web.FindDealer" EnableViewState="false"  MasterPageFile="~/_Global/master/TwoColMaster.master" %>

<%@ Register Src="~/_Global/Controls/FindADealer.ascx" TagName="FindADealer" TagPrefix="uc2" %>

<asp:Content ID="contentHead" runat="server" ContentPlaceHolderID="head">
	<link rel="stylesheet" href="/_Global/css/sections/find-a-dealer.css" type="text/css" media="screen" charset="utf-8" />
	<link href="/_Global/css/marine/sections/find-a-dealer.css" rel="stylesheet" type="text/css" />
	<style type="text/css">v\:* { behavior: url(#default#VML); }</style>
</asp:Content>

<asp:Content ID="LeftColContent" ContentPlaceHolderID="Widgets" runat="server">
	<li class="find-a-dealer">
		<uc2:FindADealer ID="FindADealer1" runat="server" />
	</li>
	<li class="cities_nearby" runat="server" id="liCitieNearYou">
		<div class="top_cap pf"></div>
		<div class="content pf">
			<h4><%=ResourceManager.GetString("txtCities_Near_You")%></h4>
			<ul id="left_nav">
				<asp:Repeater runat="server" ID="rptCityList" OnItemDataBound="rptCityList_ItemDataBound">
					<ItemTemplate>
						<li runat="server" id="li"><asp:HyperLink ID="hypCity" runat="server" /></li>
					</ItemTemplate>
				</asp:Repeater>							
			</ul>
		</div>
		<div class="bottom_cap pf"></div>
	</li>
	<li class="powerhouse_dealers">
		<div class="top_cap pf"></div>
		<div class="content pf">
			<img src="<%=ResourceManager.GetString("pathPowerHouseImage2") %>" alt="" />
			<p class="heading"><asp:Literal ID="LiteralPowerHouseDealerHeader" runat="server"></asp:Literal></p>
			<asp:Literal ID="LiteralPowerHouseDealerBlurb" runat="server"></asp:Literal>
			<a href="<%=ResourceManager.GetString("urlPowerhouseDealers")%>" class="btn secondary"><span><%=ResourceManager.GetString("LearnMore")%></span></a>
		</div>
		<div class="bottom_cap pf"></div>
	</li>
</asp:Content>

<asp:Content ID="RightColContent" ContentPlaceHolderID="ContentArea" runat="server">
	<script type="text/javascript" language="javascript">
		function validate(valby, valtxt, defaultText) {
			if ((valtxt.type) == "text") {
				if (valtxt.value == "" || valtxt.value == defaultText) { //set Label for error if required
					valtxt.focus();
					return false;
				}
			}
		}

		function handleFocus(element) {
			if (element.value == element.defaultValue) {
				element.value = '';
			}
		}

		function handleBlur(element) {
			if (element.value == '') {
				element.value = element.defaultValue;
			}
		}
	</script>
	<div class="content_container">
		<div class="content_section first_section">
			<h1><asp:Literal runat="server" ID="LitFindYourHondaDealer"></asp:Literal></h1>
			<div class="gmap_result_list" runat="server" id="divGMAP">
				<div class="bg bg_t"></div>
				<div class="bg bg_l"></div>
				<div class="bg bg_r"></div>
				<div class="bg bg_bl"></div>
				<div class="bg bg_br"></div>
				<div id="gmap">
					<%--<cc1:GMap ID="GMap1" runat="server" Width="645" Height="355" />--%>
					<div id="map-canvas" style="width: 645px; height: 355px;"></div>
					<%--center, zoom--%>
					<input type="hidden" id="MapDataView" runat="server" class="view-json-data" />
					<%--locations to pin--%>
					<div id="MapDataLocations" runat="server" class="dealers-json-data dn" />
			
					<div class="dealer_popup_wrapper dn" >
						<div class="fc dealer_popup" style="width:320px; height:110px;">
							<div class="h6">{8}</div> 
							<div class="fr image">
								<a href="{6}"><%--/dealerlocator/ontario/toronto/lakeshore_honda--%>
									<img style="width:90px; height:68px;" src="{7}" alt="{0}"/><%--/content/honda.ca/dealerimages/H_dealer_default_image.png?width=90&amp;--%>
								</a> 
							</div> 
							<div class="fl copy">
								<div class="address">{1}<br/>{2}, {3} {4}</div> 
								<div class="phone">{5}</div> 
							</div> 
						</div>
					</div>
				</div>
			</div>
			<p class='sorry' id="SorryLabel" runat="server">
				<asp:Literal runat="server" ID="LitSorry"></asp:Literal>
			</p>
			<div class="bar" id="ucSubHeader" runat="server">
				<h2><asp:Literal runat="server" ID="LitSearchResult"></asp:Literal></h2>
			</div>
			<div class="show_phd_only" ID="PhdCheckBoxDiv" runat="server">
				<label for="phd_only" class="phd_only">
					<asp:CheckBox ID="CheckBoxPhd" runat="server" AutoPostBack="true" />
				</label>
			</div>
			<asp:Repeater runat="server" ID="rptDealersList" Visible="true" EnableViewState="true" OnItemDataBound="rptDealersList_ItemDataBound">
				<HeaderTemplate>
					<table id="sort_province" class="tbl province">
					<thead>
						<tr class="first top" >
							<th class="first odd name sort_asc" colspan="2"><div><span><a href="javascript:void(0)"><%=ResourceManager.GetString("txtDealerName")%></a></span></div></th>
							<th class="even address"><div><span><%=ResourceManager.GetString("txtAddress")%></span></div></th>
							<th class="odd phone"><div><span><%=ResourceManager.GetString("txtPhone")%></span></div></th>
							<th class="last even city sort"><div><span><a href="javascript:void(0)"><%=ResourceManager.GetString("txtCity")%></a></span></div></th>
						</tr>
					</thead>
					<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr id="trDealerList" runat="server">
						<td>
							<input src="/_Global/img/layout/honda_pin.gif" type="image" id="OpenInfoWindow" runat="server" alt="Honda" class="location-icon" style="cursor: pointer" width="12" />
						</td>
						<td class="odd first" valign="middle"><asp:Literal ID="LitDealerName" runat="server" /></td>
						<td class="even"><div><span><asp:Literal ID="LitStreat" runat="server" /></span></div></td>
						<td class="odd"><div><span><asp:Literal ID="LitMainPhone" runat="server" /></span></div></td>
						<td class="last even"><div><span><asp:Literal ID="LitCity" runat="server" /></span></div></td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
				</tbody>
				</table>
				</FooterTemplate>
			</asp:Repeater>	
		</div>
	</div>
</asp:Content>

<asp:Content ID="additional" runat="server" ContentPlaceHolderID="insertDivatBottom">
</asp:Content>

<asp:Content ID="JavaScript" ContentPlaceHolderID="IncludeJavaScripatEnd" runat="server">
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<%=ConfigurationManager.AppSettings["GoogleMapKey:Honda"]%>&sensor=false"> </script>
	<script type="text/javascript" src="/_Global/js/maps/googlemaps.js"></script>
    <script type="text/javascript">
    	window.page_init = function () {
    		var config = $("input.view-json-data").attr('value');
    		var data = $("div.dealers-json-data").text();
    		var areaId = 'map-canvas';
    		ShowMap(config, data, areaId, '/_Global/img/Honda_pin/honda_pin.png');
    	}
	</script>
</asp:Content>