﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ElementControlCompaq.ascx.cs"
    Inherits="HondaCA.Web.Cms.Console.ElementControlCompaq" %>
<%@ Register Assembly="Navantis.Honda.CMS.Core" Namespace="Navantis.Honda.CMS.Core" TagPrefix="cms" %>
<%@ Import Namespace="Navantis.Honda.CMS.Core.Service" %>
<%@ Import Namespace="Navantis.Honda.CMS.Core.Unity" %>
<%@ Import Namespace="Navantis.Honda.CMS.Core" %>
<%@ Import Namespace="Navantis.Honda.CMS.Demo" %>

<%     
    CmsBasePage page = this.Page as CmsBasePage;
    string userName = page.Username;
    bool isCMSSystemAdmin = page.IsCMSSystemAdmin;
    bool isEditorOrAdmin = isCMSSystemAdmin || page.IsEditor;
    bool isEditorAuthorOrAdmin = isEditorOrAdmin || page.IsAuthor;
    
    var contentElement = this.ContentElement;
    var id = contentElement.ID;
    var controller = contentElement.MvcController;
    var url = contentElement.Parent.MainURL;
    string lastSavedContent = contentElement.LastSavedBy;
    string lastSubmittedContent = contentElement.LastSubmittedBy;
    bool isOldVersion = contentElement.ContentHistoryID > 0;    
    string status = contentElement.Status.ToString();
    bool isSubmitted = status.Equals(ContentStatus.Submitted.ToString());
    bool isSaved = !isSubmitted && status.Equals(ContentStatus.Saved.ToString());
%>

<a href="#" class="edit-button editable-pane edit-button-<%=isOldVersion ? "oldversion" : status.ToLower() %>" rel="#<%=id%>" href="#" style="position: absolute; z-index: 20000;"></a>

<div id="<%=id%>" class="dn">
    <ul class="operations">
        <%if (isOldVersion)
        {
            if (isEditorAuthorOrAdmin)
            {%>
                <%--Clear History Link--%>
                <li>        
                    <a title="Go back to current version" href="#" onclick="editCmsElement('ClearVersionHistory','<%=controller%>','<%=ContentElement.ContentHistoryID%>','<%=url%>');">
                    Return to current version
                    </a>
                </li>
            <%}
        }
        else
        {
            if (isSubmitted)
            {
                if (isEditorAuthorOrAdmin)
                {%>
                    <%--View Link--%>
                    <li>
                        <a title="View Content" href="#" onclick="editCmsElement('Edit', '<%=controller%>','<%=id%>','<%=url%>');">
                        View
                        </a>
                    </li>
                <%}
            }
            else
            {
                if ((status.Equals(ContentStatus.Rejected.ToString()) && (userName.Equals(lastSubmittedContent) || isCMSSystemAdmin))
                    || (isSaved && userName.Equals(lastSavedContent))
                    || status.Equals(ContentStatus.New.ToString())
                    || status.Equals(ContentStatus.Approved.ToString()))
                {
                    if (isEditorAuthorOrAdmin)
                    {%>
                        <%--Edit Link--%>
                        <li>
                            <a title="Edit Content" href="#" onclick="editCmsElement('Edit', '<%=controller%>','<%=id%>','<%=url%>','<%=ReturnUrl%>');">
                            Edit
                            </a>
                        </li>
                    <%}
                }
            }%>

            <%--Undo Link--%>
            <%if ((isSaved && (isCMSSystemAdmin || userName.Equals(lastSavedContent)))
                  || (status.Equals(ContentStatus.Rejected.ToString()) && (isCMSSystemAdmin || userName.Equals(lastSubmittedContent))))
             {
                 if (isEditorAuthorOrAdmin)
                 {%>
                    <li>
                        <a title="Undo Change" href="#" onclick="editCmsElement('UndoContent','<%=controller%>','<%=id%>','<%=url%>');">
                        Undo
                        </a>
                    </li>
                <%}
             }%>
             
            <%if (isSaved)
            {
                if (userName.Equals(lastSavedContent))
                {
                    if (isEditorAuthorOrAdmin)
                    {%>
                        <%--Submit Link--%>
                        <li>
                            <a title="Submit Content" href="#" onclick="editCmsElement('SubmitContent','<%=controller%>','<%=id%>','<%=url%>');">
                            Submit
                            </a>
                        </li>
                    <%}
                }
                else
                {
                    if (isCMSSystemAdmin)
                    {%>
                        <%--Take Over Link--%>
                        <li>
                            <a title="Take Over" href="#" onclick="editCmsElement('TakeOverContent','<%=controller%>','<%=id%>','<%=url%>');">
                            Take Over
                            </a>
                        </li>
                    <%}
                }
            }
            else if (isSubmitted)
            {
                if (isEditorOrAdmin)
                {%>
                    <%--Approve/Reject Link--%>
                    <li>
                        <a title="Approve Content" href="#" onclick="editCmsElement('ApproveRejectContent','<%=controller%>','<%=id%>','<%=url%>');">
                        Approve/Reject
                        </a>
                    </li>
                <%}
            }%>

            <%if (isEditorAuthorOrAdmin)
            {%>
                <%--View Versions Link--%>
                <li>
                    <a title="View Versions" href="#" onclick="editCmsElement('ViewVersionContent','<%=controller%>','<%=id%>','<%=url%>');">
                    View Versions
                    </a>
                </li>
                
                <%--View Text Difference Link--%>
                <li>
                    <a title="View Text Difference" href="#" onclick="viewTextDifference('<%=id%>', '<%=controller%>');">
                    View Text Difference
                    </a>
                </li>
            <%}                    
        }%>
    </ul>
    <div class="status">
        <%--
        <b>
            this.ContentElement.ElementName
        </b>
         --%>
        <%if (isOldVersion)
        {%>
            Old Version
        <%}
        else
        {%>
            Status:<%=status%> by <%=ContentElement.OriginatorUser%> on
        <%}%>
        <%=ContentElement.LastUpdated%>
    </div>    
</div>
