﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetPidCookie.aspx.cs" Inherits="Navantis.Honda.CMS.Demo.Cms.Console.SetPidCookie" %>
<%@ Import Namespace="MvcContrib.UI.Grid" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Navantis.Honda.CMS.Core.Model" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form  runat="server">

    <div class="page">
        <div class="header">
            <div class="title">
                <h1>
                    Honda.ca CMS - ASP.NET Application
                </h1>
            </div>
            <div class="clear hideSkiplink">
            </div>
        </div>
        <div class="main" >
            <font style="color:Black;font-weight:bold">
                A new cmsPage is about to be created for the model, are you sure you want to continue?
            </font>
            <table border="0" style="width: 400px">
                <tr>
                    <td style="width:120px" >
                        Model Name
                    </td>
                    <td>
                        <%=this.ModelName%>
                    </td>
                </tr>
                <%if (!string.IsNullOrEmpty(this.TrimName))
                  {%>
                    <tr>
                        <td style="width:120px" >
                            Trim Name
                        </td>
                        <td>
                            <%=this.TrimName%>
                        </td>
                    </tr>                   
                <%}%>
                <tr>
                    <td>
                        Model Year
                    </td>
                    <td>
                        <%=this.ModelYear %>
                    </td>
                </tr>
                <%if (PagesRelatedTo != null && PagesRelatedTo.Count > 0)
                {%>
                    <tr>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <th colspan="3">Related Pages</th>
                                </tr>
                                <tr>
                                    <th>MainURL</th>
                                    <th>Product ID</th>
                                    <th>Trim ID</th>
                                </tr>
                                <%foreach (cmsPage page in PagesRelatedTo)
                                  {%>
                                    <tr>
                                        <td><%=page.MainURL%></td>
                                        <td><%=page.Product_ID.ToString()%></td>
                                        <td><%=page.Trim_ID.ToString()%></td>
                                    </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <p>
                                <input id="Update" name="Update" type="checkbox" value="true" />                                <input name="Update" type="hidden" value="false" />
                                Update related page reference of the current page to the new page
                            </p>
                        </td>
                    </tr>
                <%}%>
                <tr>
                    <td >
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnYes" Text="Yes" runat="server" Width="60px" />
                        <asp:Button ID="btnNo" Text="No" runat="server" Width="60px" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="footer">
        
    </div>
    </form>
</body>
</html>
