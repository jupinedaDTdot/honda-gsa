﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Display.aspx.cs" Inherits="Navantis.Honda.CMS.Demo.Display" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client" %>
<%@ Import Namespace="System.Collections.Generic" %>

<html>
    <head>
        <title>CMS Display Page</title>
    </head>
    <body>
        <table border="1" cellpadding="0" cellspacing="0" width="100%">
            <%if (this.ContentPage.ProductID != null)
            {
                string contentPageDisplay = string.Format("This is a model page for model with Product_ID = {0}", this.ContentPage.ProductID);
                if (this.ContentPage.TrimID != null)
                    contentPageDisplay += string.Format(" and Trim_ID = {0}", this.ContentPage.TrimID);%>
                <tr>
                    <td colspan="2">
                        <strong><%=contentPageDisplay%></strong>
                        <br />
                        <br />
                    </td>
                </tr>
            <%}%>
            <tr>
                <td>Related Pages</td>
                <td>
                    <div>
                        <%if (this.RelatedPages.Count > 0)
                        {
                            foreach (string relatedPageName in this.RelatedPages.Keys)
                            {
                                foreach (ContentPage cp in this.RelatedPages[relatedPageName])
                                {%>
                                    <strong>Related Page Name:</strong> <%=relatedPageName%>
                                    <strong>Related Page URL:</strong> <%=cp.MainURL%>  
                                    <strong>Related content elements:</strong>
                                    <%foreach (string elementName in cp.Elements.Keys)    
                                    {%>
                                        <%=elementName%>;
                                    <%}%>
                                    <br />
                                <%}
                            }
                        }
                        else
                        {%>
                            No related pages to display.
                        <%}%>
                    </div>
                </td>
            </tr>
            <%foreach (string elementName in this.ContentPage.Elements.Keys)
            {%>
                <tr>
                    <td><%=elementName %></td>
                    <td>
                        <%
                        //display the element properly
                        //we have to code like this since the front-end cannot use the MVC views
                        IElementContent contentElement = this.ContentPage.Elements[elementName].DeserializeElementObject();
                        %>
    
                        <%=GenerateContentElementRenderingHtml(contentElement.ContentElement)%>               
                        
                       <%if( contentElement is PromoTout && (contentElement as PromoTout).Items != null )
                        {
							foreach( PromoToutItem item in (contentElement as PromoTout).Items )
							{%>                
                                <div>
                                    <i>DisplayOrder:</i> <%=item.DisplayOrder%>
                                </div>
                                <div>
                                    <i>Title:</i> <%=item.Title%>
                                </div>
                               <div>
                                    <i>Description:</i> <%=item.Description%>
                                </div>
								<div>
                                    <i>Image:</i>
									<%=GenerateMediaRenderingHtml(item.Image)%>
                                </div>
								<div>
									<i>Start Date</i>
									<%=Navantis.Honda.CMS.Core.Service.CommonService.DateTimeToString(item.StartDate)%> 
								</div>
								<div>
									<i>End Date</i>
									<%=Navantis.Honda.CMS.Core.Service.CommonService.DateTimeToString(item.EndDate)%> 
								</div>
								<div>
                                    <i>Duration:</i>
									<%=item.DurationString%>
                                </div>
								<div>
									<i>Detail Page URL</i>
									<a href="<%=item.URL%>"  target="_blank"><%=item.URL%></a>
								</div>
								<hr />
                       <%	}
						}
						else if (contentElement is AdBuilderAsset && (contentElement as AdBuilderAsset).Items != null)
                        {
                            foreach (AdBuilderAssetItem abai in (contentElement as AdBuilderAsset).Items)
                            {%>                
                                <div>
                                    <i>DisplayOrder:</i> <%=abai.DisplayOrder%>
                                </div>
                                <div>
                                    <i>Title:</i> <%=abai.Title%>
                                </div>
                                <div>
                                    <i>Notes:</i>
                                    <%=abai.Notes%>                           
                                </div>
                                <div>
                                    <i>Preview Thumbnail:</i>
                                    <%=GenerateMediaRenderingHtml(abai.PreviewThumbnail)%>
                                </div>
                                <div>
                                    <i>Preview Thumbnail Link URL:</i>
                                    <%=abai.PreviewThumbnailLinkURL%>
                                </div>
                                <div>
                                    <i>Associated Files:</i>
                                    <%if(abai.AssociatedFiles != null && abai.AssociatedFiles.Count > 0)
                                    {%>
                                        <br/>
                                        <%foreach(var aft in abai.AssociatedFiles)
                                        {%>
                                            <%=aft.FileURL%> (<%=aft.Type%>)
                                            <br/>
                                        <%}
                                    }%>
                                </div>
                                <div>
                                    <i>Featured:</i>
                                    <%=abai.Featured.ToString()%>
                                </div>
                                <div>
                                    <i>Group:</i>
                                    <%=abai.Group%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is AdBuilderFeaturedAsset && (contentElement as AdBuilderFeaturedAsset).Items != null)
                        {
                            foreach (AdBuilderFeaturedAssetItem abfai in (contentElement as AdBuilderFeaturedAsset).Items)
                            {%>                
                                <div>
                                    <i>DisplayOrder:</i>
                                    <%=abfai.DisplayOrder%>
                                </div>
                                <div>
                                    <i>Title:</i>
                                    <%=abfai.AssetItem.Title%>
                                </div>
                                <div>
                                    <i>Notes:</i>
                                    <%=abfai.AssetItem.Notes%>                           
                                </div>
                                <div>
                                    <i>Preview Thumbnail:</i>
                                    <%=GenerateMediaRenderingHtml(abfai.AssetItem.PreviewThumbnail)%>
                                </div>
                                <div>
                                    <i>Preview Thumbnail Link URL:</i>
                                    <%=abfai.AssetItem.PreviewThumbnailLinkURL%>
                                </div>
                                <div>
                                    <i>Associated Files:</i>
                                    <%if (abfai.AssetItem.AssociatedFiles != null && abfai.AssetItem.AssociatedFiles.Count > 0)
                                    {%>
                                        <br/>
                                        <%foreach (var aft in abfai.AssetItem.AssociatedFiles)
                                        {%>
                                            <%=aft.FileURL%> (<%=aft.Type%>)
                                            <br/>
                                        <%}
                                    }%>
                                </div>
                                <div>
                                    <i>Group:</i>
                                    <%=abfai.AssetItem.Group%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is AdBuilderNewAsset && (contentElement as AdBuilderNewAsset).Items != null)
                        {
                            foreach (AdBuilderNewAssetItem abnai in (contentElement as AdBuilderNewAsset).Items)
                            {%>                
                                <div>
                                    <i>DisplayOrder:</i>
                                    <%=abnai.DisplayOrder%>
                                </div>
                                <div>
                                    <i>Title:</i>
                                    <%=abnai.AssetItem.Title%>
                                </div>
                                <div>
                                    <i>Notes:</i>
                                    <%=abnai.AssetItem.Notes%>                           
                                </div>
                                <div>
                                    <i>Preview Thumbnail:</i>
                                    <%=GenerateMediaRenderingHtml(abnai.AssetItem.PreviewThumbnail)%>
                                </div>
                                <div>
                                    <i>Preview Thumbnail Link URL:</i>
                                    <%=abnai.AssetItem.PreviewThumbnailLinkURL%>
                                </div>
                                <div>
                                    <i>Associated Files:</i>
                                    <%if (abnai.AssetItem.AssociatedFiles != null && abnai.AssetItem.AssociatedFiles.Count > 0)
                                    {%>
                                        <br/>
                                        <%foreach (var aft in abnai.AssetItem.AssociatedFiles)
                                        {%>
                                            <%=aft.FileURL%> (<%=aft.Type%>)
                                            <br/>
                                        <%}
                                    }%>
                                </div>
                                <div>
                                    <i>New:</i>
                                    <%=abnai.AssetItem.Featured.ToString()%>
                                </div>
                                <div>
                                    <i>Group:</i>
                                    <%=abnai.AssetItem.Group%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is Brochure)
                        {
                            Brochure brochure = contentElement as Brochure;%>
                            <div>
                                <i>PDF</i>
                                <%=brochure.PDF%>
                            </div>
                            <div>
                                <i>URL</i>
                                <%=brochure.URL%>
                            </div>
                        <%}
                        else if (contentElement is ContentPointer)
                        {
                            ContentPointer contentPointer = contentElement as ContentPointer;%>
                            <div>
                                <i>ContentID</i>
                                <%=contentPointer.ContentID%>
                            </div>
                            <div>
                                <i>PageID</i>
                                <%=contentPointer.PageID%>
                            </div>
                        <%}
                        else if (contentElement is FreeFormHtml)
                        {
                            FreeFormHtml ffh = contentElement as FreeFormHtml;%>
                            <div>
                                <i>Html</i>
                                <hr />
                            </div>
                            <div>
                                <%=ffh.Html%>
                                <hr />
                            </div>
                            <%List<string> mediaPaths = GetCmsMediaPaths(ffh.ContentElement);
                            if(mediaPaths != null && mediaPaths.Count > 0)
                            {%>
                                <div>
                                    <i>Related Media</i>
                                    <hr />
                                </div>
                                <%foreach (string path in mediaPaths)
                                {%>
                                    <div>
                                        <%=GenerateMediaRenderingHtml(path)%>
                                        <%="***"+path+"***"%>
                                    </div>
                                <%}
                            }
                        }
                        else if (contentElement is Gallery && (contentElement as Gallery).Items != null)
                        {
                            foreach (GalleryItem gi in (contentElement as Gallery).Items)
                            {%>                
                                <div>
                                    <i>DisplayOrder:</i>
                                    <%=gi.DisplayOrder%>
                                </div>
                                <div>
                                    <i>Caption:</i>
                                    <%=gi.Caption%>
                                </div>
                                <div>
                                    <i>ThumbAsset:</i>
                                    <%=GenerateMediaRenderingHtml(gi.ThumbAsset)%>
                                </div>
                                <div>
                                    <i>LargeAsset:</i>
                                    <%=GenerateMediaRenderingHtml(gi.LargeAsset)%>
                                </div>
                                <div>
                                    <i>WallpaperSmall:</i>
                                    <%=GenerateMediaRenderingHtml(gi.WallpaperSmall)%>
                                </div>
                                <div>
                                    <i>WallpaperMedium:</i>
                                    <%=GenerateMediaRenderingHtml(gi.WallpaperMedium)%>
                                </div>
                                <div>
                                    <i>WallpaperLarge:</i>
                                    <%=GenerateMediaRenderingHtml(gi.WallpaperLarge)%>
                                </div>
                                <div>
                                    <i>Duration:</i>
                                    <%=gi.Duration%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is GenericContent && (contentElement as GenericContent).Items != null)
                        {
                            foreach (GenericContentItem gci in (contentElement as GenericContent).Items)
                            {%>                
                                <div>
                                    <i>Title:</i>
                                    <%=gci.Title%>
                                </div>
                                <div>
                                    <i>Title URL:</i>
                                    <%=gci.TitleURL%>
                                </div>
                                <div>
                                    <i>Summary:</i>
                                    <%=gci.Summary%>
                                </div>
                                <div>
                                    <i>Summary Link URL:</i>
                                    <%=gci.SummaryLinkURL%>
                                </div>
                                <div>
                                    <i>Image:</i>
                                    <%=GenerateMediaRenderingHtml(gci.Image)%>
                                </div>
                                <div>
                                    <i>Image Link URL:</i>
                                    <%=gci.ImageLinkURL%>
                                </div>
                                <div>
                                    <i>MoreText:</i>
                                    <%=gci.MoreText%>
                                </div>
                                <div>
                                    <i>MoreText Link URL:</i>
                                    <%=gci.MoreTextLinkURL%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is GenericForm)
                        {
                            GenericForm gf = contentElement as GenericForm;%>
                            <div>
                                <i>Form Name:</i>
                                <%=gf.FormName%>
                            </div>
                            <div>
                                <i>Form Identifier:</i>
                                <%=gf.FormIdentifier%>
                            </div>
                            <div>
                                <i>Form Title:</i>
                                <%=gf.Title%>
                            </div>
                            <div>
                                <i>Summary:</i>
                                <%=gf.Summary%>
                            </div>
                            <div>
                                <i>Rows:</i>
                                <%=gf.Rows.ToString()%>
                            </div>
                            <div>
                                <i>Columns:</i>
                                <%=gf.Columns.ToString()%>
                            </div>
                            <div>
                                <i>Fields:</i>
                                <%if (gf.Fields == null)
                                {%>
                                    No fields to display.
                                <%}
                                else
                                {%>
                                    <table>
                                        <tr>
                                            <th>Display Order</th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Label</th>
                                            <th>IsRequired</th>
                                        </tr>
                                        <%foreach(var gff in gf.Fields)
                                        {%>
                                            <tr>
                                                <td><%=gff.DisplayOrder%></td>
                                                <td><%=gff.Name%></td>
                                                <td><%=gff.Type%></td>
                                                <td><%=gff.Label%></td>
                                                <td><%=gff.IsRequired%></td>
                                            </tr>
                                        <%}%>
                                    </table>
                                <%}%>
                                <hr />
                            </div>                            
                        <%}
                        else if (contentElement is GenericLink && (contentElement as GenericLink).Items != null)
                        {
                            foreach (GenericLinkItem gli in (contentElement as GenericLink).Items)
                            {%>                
                                <div>
                                    <i>Title:</i>
                                    <%=gli.Title%>
                                </div>
                                <div>
                                    <i>File:</i>
                                    <%=GenerateMediaRenderingHtml(gli.File)%>
                                </div>
                                <div>
                                    <i>Link Text:</i>
                                    <%=gli.LinkText%>
                                </div>
                                <div>
                                    <i>Link URL:</i>
                                    <%=gli.LinkURL%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is GenericUrl)
                        {
                            GenericUrl genericURL = contentElement as GenericUrl;%>
                            <div>
                                <i>Title</i>
                                <%=genericURL.Title%>
                            </div>
                            <div>
                                <i>URL</i>
                                <%=genericURL.URL%>
                            </div>
                        <%}
                        else if (contentElement is Navantis.Honda.CMS.Client.Elements.Menu && (contentElement as Navantis.Honda.CMS.Client.Elements.Menu).Items != null)
                        {
                            foreach (Navantis.Honda.CMS.Client.Elements.MenuItem mi in (contentElement as Navantis.Honda.CMS.Client.Elements.Menu).Items)
                            {%>                
                                <div>
                                    <i>Name:</i>
                                    <%=mi.Name%>
                                </div>
                                <div>
                                    <i>Text:</i>
                                    <%=mi.Text%>
                                </div>
                                <div>
                                    <i>URL:</i>
                                    <%=mi.Url%>
                                </div>
                                <div>
                                    <i>Group:</i>
                                    <%=mi.Group%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is ModelNav && (contentElement as ModelNav).Items != null)
                        {
                            foreach (ModelNavItem mni in (contentElement as ModelNav).Items)
                            {%>                
                                <div>
                                    <i>Name:</i>
                                    <%=mni.Name%>
                                </div>
                                <div>
                                    <i>Title:</i>
                                    <%=mni.Title%>
                                </div>
                                <div>
                                    <i>URL:</i>
                                    <%=mni.URL%>
                                </div>
                                <div>
                                    <i>Page Title:</i>
                                    <%=mni.PageTitle%>
                                </div>
                                <div>
                                    <i>Is Visible:</i>
                                    <%=mni.IsVisible.ToString()%>
                                </div>
                                <div>
                                    <i>Is Optional:</i>
                                    <%=mni.IsOptional%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is PartsAndAccessories)
                        {
                            PartsAndAccessories paa = contentElement as PartsAndAccessories;%>
                            <div>
                                <i>Title:</i>
                                <%=paa.Title%>
                            </div>
                            <div>
                                <i>Items:</i>
                                <%if (paa.Items == null)
                                {%>
                                    No items to display.
                                <%}
                                else
                                {%>
                                    <table>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Product Detail</th>
                                            <th>Price</th>
                                            <th>Link</th>
                                        </tr>
                                        <%foreach (var paai in paa.Items)
                                        {%>
                                            <tr>
                                                <td><%=paai.ProductName%></td>
                                                <td><%=paai.ProductDetail%></td>
                                                <td><%=paai.Price%></td>
                                                <td><%=paai.Link%></td>
                                            </tr>
                                        <%}%>
                                    </table>
                                <%}%>
                                <hr />
                            </div>                            
                        <%}
                        else if (contentElement is RelatedProduct && (contentElement as RelatedProduct).Items != null)
                        {
                            foreach (RelatedProductItem rpi in (contentElement as RelatedProduct).Items)
                            {%>                
                                <div>
                                    <i>ModelID:</i>
                                    <%=rpi.ModelID%>
                                </div>
                                <div>
                                    <i>TrimID:</i>
                                    <%=rpi.TrimID%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is Review && (contentElement as Review).Items != null)
                        {
                            foreach (ReviewItem ri in (contentElement as Review).Items)
                            {%>                
                                <div>
                                    <i>Title:</i>
                                    <%=ri.Title%>
                                </div>
                                <div>
                                    <i>Summary:</i>
                                    <%=ri.Summary%>
                                </div>
                                <div>
                                    <i>Link URL:</i>
                                    <%=ri.LinkURL%>
                                </div>
                                <div>
                                    <i>Source:</i>
                                    <%=ri.Source%>
                                    <hr />
                                </div>
                            <%}
                        }                
                        else if (contentElement is Specification)
                        {%>
                            <div>
                                <i>Html</i><hr />
                            </div>
                            <div>
                                <%=(contentElement as Specification).Html%>
                            </div>
                        <%}
                        else if (contentElement is TopAccessories && (contentElement as TopAccessories).Items != null)
                        {
                            foreach (TopAccessoriesItem tai in (contentElement as TopAccessories).Items)
                            {%>                
                                <div>
                                    <i>Accessory Item ID:</i>
                                    <%=tai.AccessoryItemID%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is Tout && (contentElement as Tout).Items != null)
                        {
                            foreach (ToutItem ti in (contentElement as Tout).Items)
                            {%>                
                                <div>
                                    <i>Name:</i>
                                    <%=ti.Name%>
                                </div>
                                <div>
                                    <i>Title:</i>
                                    <%=ti.Title%>
                                </div>
                                <div>
                                    <i>URL:</i>
                                    <%=ti.URL%>
                                </div>
                                <div>
                                    <i>Path Reference:</i>
                                    <%=ti.PathReference%>
                                </div>
                                <div>
                                    <i>Support Files:</i>
                                    <%if (ti.SupportFiles != null && ti.SupportFiles.Count > 0)
                                    {
                                        foreach (var file in ti.SupportFiles)
                                        {%>
                                            <%=GenerateMediaRenderingHtml(file)%>
                                        <%}
                                    }%>
                                </div>
                                <div>
                                    <i>Duration:</i>
                                    <%=ti.Duration%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is ToutHighlightPoint && (contentElement as ToutHighlightPoint).Items != null)
                        {
                            foreach (ToutHighlightPointItem thpi in (contentElement as ToutHighlightPoint).Items)
                            {%>                
                                <div>
                                    <i>Title:</i>
                                    <%=thpi.Title%>
                                </div>
                                <div>
                                    <i>Summary:</i>
                                    <%=thpi.Summary%>
                                </div>
                                <div>
                                    <i>Image:</i>
                                    <%=GenerateMediaRenderingHtml(thpi.Image)%>
                                </div>
                                <div>
                                    <i>MoreText:</i>
                                    <%=thpi.MoreText%>
                                </div>
                                <div>
                                    <i>MoreText Link URL:</i>
                                    <%=thpi.MoreTextLinkURL%>
                                </div>
                                <div>
                                    <i>X:</i>
                                    <%=thpi.X.ToString()%>
                                </div>
                                <div>
                                    <i>Y:</i>
                                    <%=thpi.Y.ToString()%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is VehicleLife && (contentElement as VehicleLife).Items != null)
                        {
                            foreach (VehicleLifeItem vli in (contentElement as VehicleLife).Items)
                            {%>                
                                <div>
                                    <i>Title:</i>
                                    <%=vli.Title%>
                                </div>
                                <div>
                                    <i>Content:</i>
                                    <%=vli.Content%>
                                </div>
                                <div>
                                    <i>URL:</i>
                                    <%=vli.URL%>
                                </div>
                                <div>
                                    <i>ImageURL:</i>
                                    <%=GenerateMediaRenderingHtml(vli.ImageURL)%>
                                    <hr />
                                </div>
                            <%}
                        }
                        else if (contentElement is PageSetting)
                        {
                            PageSetting ps = contentElement as PageSetting;%>
                            <div>
                                <i>Title:</i>
                                <%=ps.Title%>
                            </div>
                            <div>
                                <i>Language:</i>
                                <%if (ps.Language == 1)
                                {%>
                                    English
                                <%}
                                else if (ps.Language == 2)
                                {%>
                                    French
                                <%}
                                else
                                {%>
                                    null
                                <%}%>
                            </div>
                            <div>
                                <i>LanguageToggleURL:</i>
                                <%=ps.LanguageToggleURL%>
                            </div>
                            <div>
                                <i>Source URLs:</i>
                                <%if (ps.SourceURLs == null || ps.SourceURLs.Count == 0)
                                {%>
                                    No items to display.
                                <%}
                                else
                                {%>
                                    <table>
                                        <%foreach (var su in ps.SourceURLs)
                                        {%>
                                            <tr>
                                                <td><%=su%></td>
                                            </tr>
                                        <%}%>
                                    </table>
                                <%}%>
                            </div>
                            <div>
                                <i>GSA Meta Data:</i>
                                <%if (ps.GsaMetaData != null)
                                {%>
                                    <table>
                                        <tr>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Content</th>
                                        </tr>
                                        <%foreach (var gmd in ps.GsaMetaData.Items)
                                        {
                                            var gtv = gmd.TypeValue;%>
                                            <tr>
                                                <td><%=gmd.Name%></td>
                                                <%if (gtv == GsaTypeValue.Image)
                                                {%>
                                                    <td>Image</td>
                                                    <td><%=GenerateMediaRenderingHtml(gmd.Content)%></td>
                                                <%}
                                                else
                                                {%>
                                                    <td>
                                                        <%if (gtv == GsaTypeValue.Text)
                                                        {%>
                                                            Text
                                                        <%}
                                                        else
                                                        {%>
                                                            Keyword
                                                        <%}%>
                                                    </td>
                                                    <td><%=gmd.Content%></td>
                                                <%}%>
                                            </tr>
                                        <%}%>
                                    </table>
                                <%}
                                else
                                {%>
                                      No GSA Meta Data to display.
                                <%}%>
                            </div>
                            <div>
                                <i>Page Meta Data:</i>
                                <%if (ps.MetaData != null)
                                {%>
                                    <table>
                                        <tr>
                                            <th>Name</th>
                                            <th>Content</th>
                                        </tr>
                                        <%foreach (var pmd in ps.MetaData)
                                        {%>
                                            <tr>
                                                <td><%=pmd.Name%></td>
                                                <td><%=pmd.Content%></td>
                                            </tr>
                                        <%}%>
                                    </table>
                                <%}
                                else
                                {%>
                                      No Page Meta Data to display.
                                <%}%>
                            </div>
                            <div>
                                <i>Survey Settings:</i>
                                <table>
                                    <tr>
                                        <td>Survey ID</td>
                                        <td><%=ps.SurveyID %></td>
                                    </tr>
                                    <tr>
                                        <td>Survey Start Date</td>
                                        <td><%=ps.SurveyStartDate.ToString() %></td>
                                    </tr>
                                    <tr>
                                        <td>Survey End Date</td>
                                        <td><%=ps.SurveyEndDate.ToString() %></td>
                                    </tr>
                                    <tr>
                                        <td>Survey Enforced</td>
                                        <td><%=ps.SurveyEnforced %></td>
                                    </tr>
                                </table>
                            </div>
                            <div>
                                <i>Associated Files:</i>
                                <%if (ps.AssociatedFilePaths != null && ps.AssociatedFilePaths.Count > 0)
                                {%>
                                    <br/>
                                    <%foreach (var afp in ps.AssociatedFilePaths)
                                    {%>
                                        <%=afp%>
                                        <br/>
                                    <%}
                                }
                                else
                                {%>
                                      No Associated Files to display.
                                <%}%>
                            </div>
                        <%}
                        else if (contentElement is ProductSelectionWizard && (contentElement as ProductSelectionWizard).Items != null)
                        {
                            foreach (ProductSelectionQuestion psq in (contentElement as ProductSelectionWizard).Items)
                            {%>                
                                <div>
                                    <i>Title:</i>
                                    <%=psq.Title%>
                                </div>
                                <div>
                                    <i>QuestionText:</i>
                                    <%=psq.QuestionText%>
                                </div>
                                <div>
                                    <i>QuestionSubText:</i>
                                    <%=psq.QuestionSubText%>
                                    <hr />
                                </div>
                                <%if (psq.QuestionItems != null && psq.QuestionItems.Count > 0)
                                {%>
                                    <table>
                                        <%foreach (ProductSelectionQuestionItem psqi in psq.QuestionItems)
                                        {%>
                                            <tr>
                                                <td rowspan="2"><%=psqi.DisplayOrder%></td>
                                                <td>
                                                    <b>Title:</b> <%=psqi.Title%>
                                                    <br />
                                                    <b>SubTitle:</b> <%=psqi.SubTitle%>
                                                    <br />
                                                    <b>Image:</b> <%=psqi.Image%>
                                                    <br />
                                                    <b>JumpToEndIfSelected:</b> <%=psqi.JumpToEndIfSelected%>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <%if (psqi.Products != null && psqi.Products.Count > 0)
                                                    {%>
                                                        <br />
                                                        <table width="100%">
                                                            <tr>
                                                                <th>Order</th>
                                                                <th>Model Name</th>
                                                                <th>Trim Name</th>
                                                            </tr>
                                                            <%foreach (ProductSelectionQuestionItemProductItem psqipi in psqi.Products)
                                                            {%>
                                                                <tr>
                                                                    <td><%=psqipi.DisplayOrder%></td>
                                                                    <td><%=psqipi.ModelName%></td>
                                                                    <td><%=psqipi.TrimName%></td>
                                                                </tr>
                                                            <%}%>
                                                        </table>
                                                    <%}%>
                                                </td>
                                            </tr>
                                        <%}%>
                                    </table>
                                <%}
                            }
                        }%>
                    </td>
                </tr>
            <%}%>
        </table>
    </body>
</html>