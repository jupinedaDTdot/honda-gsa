﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/TwoColMaster.master" AutoEventWireup="true" EnableViewState="false" CodeBehind="results.aspx.cs" Inherits="Marine.Web.Search.Results" %>
<%@ Import Namespace="Honda.Google.Common.Contracts" %>
<%@Register  Src="~/_Global/Controls/FindADealer.ascx"  TagName="FindADealer" TagPrefix="uc2"%>
<%@ Register TagPrefix="honda" TagName="SearchPager" Src="Pager.ascx" %>

<asp:Content ID="content1" runat="server" ContentPlaceHolderID="head">
    <link rel="stylesheet" href="/_Global/css/sections/search.css" type="text/css" media="screen" charset="utf-8" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Widgets" runat="server">
    <li class="find-a-dealer">
      <uc2:FindADealer ID="FindADealer2" runat="server" />
    </li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentArea" runat="server">
    				<div class="content_container search_container">
				<h1><%=ResourceManager.GetString("txtSearchResult")%></h1>
				<div class="section first">
					
					    <div class="rel zi search_input_container">
						    <div class="search_input gray_wall">
						        <span class="top_left"></span><span class="top_right"></span><span class="bottom_left"></span><span class="bottom_right"></span>
						        <div class="fc">
							    <div class="fl input_box">
							        <asp:TextBox runat="server" CssClass="input_xxxlarge" ID="SearchTextBox" />
							    </div>
							    <asp:LinkButton runat="server" CssClass="fl btn primary" ID="SearchLinkButton" OnClick="SearchLinkButton_Click"><span><%=ResourceManager.GetString("txtSearch")%></span></asp:LinkButton>
							    </div>
						    </div>
					    </div>
					
					<div class="leading"><asp:Literal runat="server" ID="SearchSummaryText" /></div>
				</div>
				<div class="section featured_search_results">
					<ul class="inner_three_column">
						<asp:Repeater runat="server" ID="FeaturedSearchResultsRepeater">
				            <ItemTemplate>
						        <li class='fl <%# GetItemCssClass(Container.ItemIndex, FeaturedSearchItems) %>'>
							        <div>
								        <img class="db" src='<%# ((Item)Container.DataItem).Image %>' width="190" height="120" alt='<%# ((Item)Container.DataItem).Title %>' />
								        <div class="heading"><a href='<%# ((Item)Container.DataItem).Url %>'><%# (Server.HtmlDecode(((Item)Container.DataItem).Title)) %></a></div>
								        <p class="excerpt">
									        <%# (Server.HtmlDecode(((Item)Container.DataItem).Copy)) %>
								        </p>
								        <div class="read_more"><a href='<%# ((Item)Container.DataItem).Url %>'>Learn More</a></div>
							        </div>
						        </li>
						    </ItemTemplate>
					    </asp:Repeater>
						<li class="fl clr"></li>
					</ul>
					<div class="clrfix"></div>
				</div>
				<div class="section search_results">
					<ul>
				        <asp:Repeater runat="server" ID="PagesSearchResultsRepeater">
				            <ItemTemplate>
						        <li class='<%# GetItemCssClass(Container.ItemIndex, PagesSearchItems) %>'>
							        <div>
								        <div class="heading"><a href="<%# ((Item)Container.DataItem).Url %>"><%# (Server.HtmlDecode(((Item)Container.DataItem).Title_Abbr))%></a></div>
								        <p><%# (Server.HtmlDecode(((Item)Container.DataItem).Copy_Abbr))%></p>
								        <div class="url"><a href="<%# ((Item)Container.DataItem).Url %>"><%# ((Item)Container.DataItem).Url %></a></div>
							        </div>
						        </li>
						    </ItemTemplate>
					    </asp:Repeater>
					</ul>
				</div>
				<div class="section pagination_section">
				    <honda:SearchPager runat="server" ID="Pager" />
				</div>
			</div>
				   
</asp:Content>			