﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Marine.Web.Default" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" href="/_Global/css/sections/landing.css" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
    <div id="feature_area">
	    <uc:ElementControl id="ElementControl1" runat="server"  ElementName="GenericContent_Tout" Visible="true" />         
        <asp:PlaceHolder ID="plTopBanner" runat="server" > </asp:PlaceHolder>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
    <div class="wrapper" id="divThreeGeneric" runat="server">
		<div class="cap_top_default pf"></div>
		<div class="container">
			<div class="content_container">
                <uc:ElementControl id="eleControlMiddle1" runat="server"  ElementName="GenericContent_GC1" Visible="true" />         
                <asp:PlaceHolder ID="plGenericThreeColumns" runat="server" > </asp:PlaceHolder>				
            </div>
		</div>
		<div class="cap_bottom_default pf"></div>
	</div>    
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="cap_top_default pf"></div>
	<div class="container pf">
   		<ul class="fc web_properties">
            <li class="fl honda"><a href="<%=ResourceManager.GetString("urlHondaAuto")%>"></a></li>
			<li class="fl acura"><a href="<%=ResourceManager.GetString("urlAcura")%>"></a></li>
            <li class="fl mc"><a href="<%=ResourceManager.GetString("urlMotorcycle")%>"></a></li>
			<li class="fl atvs"><a href="<%=ResourceManager.GetString("urlATV")%>"></a></li>
			<li class="fl honda_power_equipment"><a href="<%=ResourceManager.GetString("urlPowerEquipment")%>"></a></li>
			<li class="fl last honda_engines"><a href="<%=ResourceManager.GetString("urlEngines")%>"></a></li>
		</ul>
    </div>
	<div class="cap_bottom_default pf"></div>			
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>

