﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

using HondaCA.Common;
using System.Globalization;
using System.Threading;
using System.Web.UI.HtmlControls;


using Navantis.Honda.CMS.Demo;
namespace Marine.Web
{
    public partial class ErrorPages : CmsContentBasePage//BasePage
    {
       protected override void OnInit(EventArgs e)
       {
           IsNotCMSPage = true;          
           base.OnInit(e);
       }

       protected void Page_Load(object sender, EventArgs e)
        {
           string lang = Request.QueryString["L"] != null ? Request.QueryString["L"] : "";
           lang = lang.Length > 2 ? lang.Substring(0, 2) : lang;
           if (lang.ToUpper() == Language.French.GetCultureStringValue().Substring(0, 2).ToUpper())
                this.PageLanguage = Language.French;
            else
                this.PageLanguage = Language.English;

           string url = string.IsNullOrWhiteSpace(Request.QueryString["url"]) ? "" : "?url=" + Request.QueryString["url"];
           if (Request.QueryString["err"] == "404")
           {
               if (this.PageLanguage == Language.English)
                   Response.Redirect("/errorpages/404.html" + url);
               else
                   Response.Redirect("/errorpages/404_fr.html" + url);
           }
           else
           {
               if (this.PageLanguage == Language.English)
                   Response.Redirect("/errorpages/error.html" + url);
               else
                   Response.Redirect("/errorpages/error_fr.html" + url);
           }
           
           //base.InitializeCulture();

           //if (Request.QueryString["err"] == "404") 
           //{
           //    Response.Status = "404 Not Found";
           //    Response.StatusCode = 404;

           //    this.ToggleURL = ResourceManager.GetString("Error404ToggleURL");
           //    litErrorText.Text = ResourceManager.GetString("Error404HTML");
           //}
           //else if (Request.QueryString["err"] == "500")
           //{
           //    this.ToggleURL = ResourceManager.GetString("Error500ToggleURL");
           //    litErrorText.Text = ResourceManager.GetString("Error500HTML");
           //}
           //this.Title = "Honda";

        }
    }
}