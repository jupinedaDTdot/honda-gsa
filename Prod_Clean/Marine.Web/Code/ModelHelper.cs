﻿using System.Text;

namespace Marine.Web.Code
{
	public static class ModelHelper
	{
		public static string ProcessModelCategoryTooltip(string tooltip)
		{
			StringBuilder sb = new StringBuilder();

			//litHeading.Text = CommonFunctions.ucFirst(modelCategory.CategoryName) + " " + ResourceManager.GetString("txtMarineOutboards");
			string[] ArrayDetails = System.Text.RegularExpressions.Regex.Split(tooltip, "\r\n");

			if (ArrayDetails.Length > 0)
			{
				foreach (string aItem in ArrayDetails)
				{
					if (aItem.Trim().ToLower().StartsWith("<uses>") || aItem.Trim().ToLower().StartsWith("<utilisations>"))
					{
						continue;
					}
					sb.Append(aItem);
				}
			}
			return sb.ToString();
		}
	}
}