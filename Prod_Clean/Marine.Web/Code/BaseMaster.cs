﻿using System;
using System.Web;
using HondaCA.Common;
using System.Text.RegularExpressions;
using HondaCA.Service.Common;

namespace Marine.Web
{
	public class BaseMaster : System.Web.UI.MasterPage
	{
		public HttpCookie currentCookie;

		protected virtual void Page_Load(object sender, EventArgs e)
		{
			this.Page.Form.Action = Request.RawUrl;

			currentCookie = Request.Cookies.Get("hondaprovince");
			string cookieValue = string.Empty;
			if (currentCookie != null) { cookieValue = currentCookie.Value; }

			string provCode = string.Empty;
			LocationService ls = new LocationService();

			if (string.IsNullOrEmpty(cookieValue))
			{
				string ip = Request.ServerVariables[HondaCA.Common.Global.getRemotAddressConst].ToString();
				provCode = ls.getProvinceCodeByIP(ip, PageLanguage);
			}

			if (string.IsNullOrEmpty(cookieValue))
			{
				if (string.IsNullOrEmpty(provCode)) provCode = "ON";

				if (!string.IsNullOrEmpty(provCode))
				{
					HttpCookie h = new HttpCookie("hondaprovince");
					h.Value = provCode;
					Response.Cookies.Add(h);
				}
			}
		}

		public Language PageLanguage
		{
			get;
			set;
		}

		private static readonly Regex REGEX_BETWEEN_TAGS = new Regex(@">\s+<", RegexOptions.Compiled);
		private static readonly Regex REGEX_LINE_BREAKS = new Regex(@"\n\s+", RegexOptions.Compiled);

		static readonly Regex viewStateRegex = new Regex(@"(<input type=""hidden"" name=""__VIEWSTATE"" id=""__VIEWSTATE"" value=""[a-zA-Z0-9\+=\\/]+"" />)", RegexOptions.Multiline | RegexOptions.Compiled);
		static readonly Regex endFormRegex = new Regex(@"</form>", RegexOptions.Multiline | RegexOptions.Compiled);

		// Commented out white space stripper code
		//protected override void Render(HtmlTextWriter writer)
		//{
		//    using (HtmlTextWriter htmlwriter = new HtmlTextWriter(new System.IO.StringWriter()))
		//    {
		//            base.Render(htmlwriter);
		//            string html = htmlwriter.InnerWriter.ToString();
		//            try
		//            {
		//                //move viewstate to bottom of page.
		//                Match viewStateMatch = viewStateRegex.Match(html);
		//                string viewStateString = viewStateMatch.Captures[0].Value;
		//                html = html.Remove(viewStateMatch.Index, viewStateMatch.Length);
		//                //This will only work if you have only one </form> on the page
		//                Match endFormMatch = endFormRegex.Match(html, viewStateMatch.Index);
		//                html = html.Insert(endFormMatch.Index, viewStateString);
		//                //strip out html whitespace
		//                html = REGEX_BETWEEN_TAGS.Replace(html, "> <");
		//                html = REGEX_LINE_BREAKS.Replace(html, string.Empty);
		//            }
		//            catch
		//            {
		//            }
		//            finally
		//            {
		//                writer.Write(html.Trim());
		//            }
		//    }
		//} 
	}
}
