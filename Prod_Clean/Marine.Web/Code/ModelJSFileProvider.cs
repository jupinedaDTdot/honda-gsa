﻿using System;
using System.Web;
using System.IO;
using System.Collections;
using Navantis.Honda.CMS.Client.Elements;
using System.Configuration;
using System.Resources;

namespace Marine.Web
{
	public class ModelJSFileProvider
	{
		public string[] ToutNames { get; set; }
		public Hashtable HTouts { get; set; }
		public string Language;
		public string newFileName;
		string physicalPath;
		public string ModelExportKey;
		public string specialOfferUrl;
		public HondaCA.Entity.Model.HondaModel ObjModel;
		public ResourceManager ResourceManager;

		public string getPath(string modelUrl)
		{
			string physicalPath = HttpContext.Current.Request.MapPath(HttpContext.Current.Request.ApplicationPath);
			physicalPath = Path.Combine(physicalPath, @"_Global\js\models");
			string physicalPath1 = HttpContext.Current.Request.Url.AbsoluteUri;//Directory.CreateDirectory(newPath);// Create the subfolder
			newFileName = System.IO.Path.GetFileName(modelUrl + "_model_vars_" + (String.IsNullOrEmpty(Language) ? "" : Language) + ".js");
			physicalPath = System.IO.Path.Combine(physicalPath, newFileName);

			return physicalPath;
		}

		public string CreateFile(string modelUrl)
		{
			physicalPath = getPath(modelUrl);

			bool needToUpdate = false;
			if (!System.IO.File.Exists(physicalPath))
			{
				needToUpdate = true;
			}
			else
			{
				DateTime fileUpdatedt = File.GetLastWriteTime(physicalPath);
				int compareResult;
				if (HTouts.Count > 0)
				{
					//Tout objTout=null;
					//Gallery objGallery=null;
					int i = 0;
					IElementContent objElement = HTouts[ToutNames[i]] as IElementContent;

					for (i = 0; i < ToutNames.Length; i++)
					{
						if (HTouts.ContainsKey(ToutNames[i]))
						{
							objElement = HTouts[ToutNames[i]] as IElementContent;
							if (objElement != null)
							{
								compareResult = fileUpdatedt.CompareTo(objElement.ContentElement.LastUpdated);
								if (compareResult < 0)
								{
									needToUpdate = true;
									break;
								}
							}
						}
					}
				}
				else
				{
					needToUpdate = true;
				}
			}

			if (needToUpdate)
			{
				using (StreamWriter outfile = new StreamWriter(physicalPath))
				{
					string tmp = formatJSFile();
					outfile.Write(tmp);
					outfile.Write(" /* New file created on:" + File.GetLastWriteTime(physicalPath).ToString() + "*/ ");
				}
			}
			return physicalPath;
		}

		public string formatJSFile()
		{
			string JSString1 = string.Empty, JSString2 = string.Empty, JSString3 = string.Empty, JSString4 = string.Empty, JSString5 = string.Empty, JSString6 = string.Empty, tmp, defaultpath, defaultSwf;
			defaultpath = HttpContext.Current.Request.Url.Scheme + "://" + HondaCA.Common.Global.CurrentDomain;//HttpContext.Current.Server.UrlEncode("http://" + HondaCA.Common.Global.CurrentDomain);//"http%3a%2f%2fbuildyourhonda.ca";

			if (HTouts.Count > 0)
			{
				Tout objTout;
				string sBuildItTargetFolder = HondaCA.Common.Global.BuildItTargetFolder;
				defaultSwf = "/_Global/swf/buildit.swf";

				objTout = null;

				if (HTouts.ContainsKey(ToutNames[0]))//DefaultGroup_Video_Tout
				{
					Gallery objGallery = HTouts[ToutNames[0]] as Gallery; ;
					if (objGallery != null && objGallery.Items != null)
					{
						if (objGallery.Items.Count > 0)
						{
							VideoFile vf = new VideoFile();
							vf.gallery = objGallery;
							vf.ResourceManager = ResourceManager;
							vf.mode = VideoFile.Mode.VideoGallery;
							string xmlfile = vf.CreateFile(ObjModel.BMUrlName);
							//todo:set width and height from gallery element
							tmp = "window._globals['hero.video_showcase'] = {'type':'flash','share_url':'" + "http://" + HondaCA.Common.Global.CurrentDomain + "/" + ObjModel.BMUrlName + "','width':'" + (String.IsNullOrEmpty(objGallery.ContentElement.Width) ? "700" : objGallery.ContentElement.Width) + "','height':'" + (String.IsNullOrEmpty(objGallery.ContentElement.Height) ? "390" : objGallery.ContentElement.Height) + "','hashVersion':'9','swf':'/_Global/swf/honda_videoPlayer.swf','flashvars':{show_controls:'true', xml_location:'" + "/_Global/xml/gallery/models/video_showcase/" + HttpUtility.UrlEncode(vf.newFileName) + "'},'params':{'allowScriptAccess':'sameDomain','allowNetworking':'all','wmode':'opaque','quality':'high','play':'true','loop':'false','menu':'false','scale':'showall','bgcolor':'#4c4c4c'}};";
							JSString1 = tmp;
						}
					}
				}

				if (HTouts.ContainsKey(ToutNames[1]))//DefaultGroup_BuildandPrice_Tout
				{
					defaultSwf = "/_Global/swf/buildit.swf";

					Placeholder objPH = HTouts[ToutNames[1]] as Placeholder;
					// do not check for Buildit as it does not req items : if (objPH.Items.Count > 0)
					if (objPH != null)
					{
						tmp = string.Format((@"window._globals['hero.build_price'] =       {""type"":""flash"",""width"":""[[0]]"",""height"":""[[1]]"",""hashVersion"":""9"",""swf"":""[[2]]"",""flashvars"":{""lang"":""[[3]]"",""ga_key"":""[[9]]"",""ga_baseURI"":""[[10]]"",""dataRoot"":""/buildittool/[[8]]/data/JSON"",""assetsRoot"":""/buildittool/[[8]]/data/JSON/assets"",""mediaRoot"":""/buildittool/[[8]]/data/JSON/assets/media_gallery_root"",""dataDomain"":""[[4]]"",""assetsDomain"":""[[5]]"",""mediaDomain"":""[[6]]"",""selectedKeys"":""{\""zip-code\"":null,\""province_key\"":null,\""model_key\"":\""[[7]]\"",\""trim_level_key\"":null,\""transmission_key\"":null}""},
                                    ""params"":{""allowScriptAccess"":""sameDomain"",""allowNetworking"":""all"",""wmode"":""transparent"",""quality"":""high"",""play"":""true"",""loop"":""false"",""menu"":""false"",""scale"":""showall"",""bgcolor"":""#4c4c4c""}};")
								.Replace("{", "$$$").Replace("}", "***").Replace("[[", "{").Replace("]]", "}"),
								 String.IsNullOrEmpty(objPH.ContentElement.Width) ? "950" : objPH.ContentElement.Width,
								 String.IsNullOrEmpty(objPH.ContentElement.Height) ? "650" : objPH.ContentElement.Height,
								 defaultSwf,
								 Language,
								 defaultpath,
								 defaultpath,
								 defaultpath,
								 ModelExportKey,
								 sBuildItTargetFolder,
								 ConfigurationManager.AppSettings.Get("Google_Analytics_Account"),
								 System.Web.Configuration.WebConfigurationManager.AppSettings["ga_baseURI"]
								 ).Replace("$$$", "{").Replace("***", "}"); //ModelExportKey //,""selectedkeys"":""%7b%22zip-code%22%3a+null%2c%22province_key%22%3a+null%2c%22model_key%22%3a%22[[7]]%22%2c%22trim_level_key%22%3a+null%2c%22transmission_key%22%3a+null%7d""
						JSString2 = tmp;
					}
				}

				objTout = null;
				if (HTouts.ContainsKey(ToutNames[2]))//DefaultGroup_360_Tout
				{
					defaultSwf = "/_Global/swf/Hondaca_360Viewer.swf";
					defaultpath = "/_Global/swf/car360_01.swf";
					objTout = HTouts[ToutNames[2]] as Tout;

					if (objTout != null && objTout.Items != null)
					{
						if (objTout.Items.Count > 0)
						{
							tmp = string.Format((@"window._globals['hero.360_view'] = { ""type"": ""flash"", ""width"": ""[[0]]"", ""height"": ""[[1]]"", ""hashVersion"": ""9"", ""swf"": ""[[2]]"", ""flashvars"": { car360: ""[[3]]"" }, ""params"": { ""allowScriptAccess"": ""sameDomain"", ""allowNetworking"": ""all"", ""wmode"": ""transparent"", ""quality"": ""high"", ""play"": ""true"", ""loop"": ""false"", ""menu"": ""false"", ""scale"": ""showall"", ""bgcolor"": ""#4c4c4c""} };")
								.Replace("{", "$$$").Replace("}", "***").Replace("[[", "{").Replace("]]", "}"),
								String.IsNullOrEmpty(objTout.ContentElement.Width) ? "950" : objTout.ContentElement.Width,
								String.IsNullOrEmpty(objTout.ContentElement.Height) ? "402" : objTout.ContentElement.Height,
								defaultSwf,
								(objTout.Items[0].PathReference ?? defaultpath) == string.Empty ? defaultpath : objTout.Items[0].PathReference
								).Replace("$$$", "{").Replace("***", "}");

							JSString3 = tmp;
						}
					}
				}

				//Santino
				if (HTouts.ContainsKey(ToutNames[3]))
				{
					defaultSwf = "/_Global/swf/FeaturesAndBenefits.swf";
					defaultpath = "";
					objTout = HTouts[ToutNames[3]] as Tout;

					if (objTout != null && objTout.Items != null)
					{
						if (objTout.Items.Count > 0)
						{
							//string file = objTout.Items[0].PathReference ?? defaultpath;
							//JSString4 = BuildScript(objTout, file, "Option1", defaultpath, defaultSwf);
							tmp = string.Format((@"window._globals['hero.key_features'] = {""type"": ""flash"", ""width"": ""[[0]]"", ""height"": ""[[1]]"", ""hashVersion"": ""9"", ""swf"": ""[[2]]"", ""flashvars"": { lang: ""language"", pkg:""[[3]]"" }, ""params"": { ""allowScriptAccess"": ""sameDomain"", ""allowNetworking"": ""all"", ""wmode"": ""transparent"", ""quality"": ""high"", ""play"": ""true"", ""loop"": ""false"", ""menu"": ""false"", ""scale"": ""showall"", ""bgcolor"": ""#4c4c4c""} };")
									.Replace("{", "$$$").Replace("}", "***").Replace("[[", "{").Replace("]]", "}"),
									String.IsNullOrEmpty(objTout.ContentElement.Width) ? "950" : objTout.ContentElement.Width,
									String.IsNullOrEmpty(objTout.ContentElement.Height) ? "470" : objTout.ContentElement.Height,
									defaultSwf,
									(objTout.Items[0].PathReference ?? defaultpath) == string.Empty ? defaultpath : objTout.Items[0].PathReference
									).Replace("$$$", "{").Replace("***", "}").Replace("language", Language);

							JSString4 = tmp;
						}
					}
				}

				if (HTouts.ContainsKey(ToutNames[4]))
				{
					defaultSwf = "/_Global/swf/Hondaca_360Viewer.swf";
					defaultpath = "/_Global/swf/car360_01.swf";
					objTout = HTouts[ToutNames[4]] as Tout;

					if (objTout != null && objTout.Items != null)
					{
						if (objTout.Items.Count > 0)
						{
							string file = objTout.Items[0].PathReference ?? defaultpath;
							JSString5 = BuildScript(objTout, file, "Option2", defaultpath, defaultSwf);
						}
					}
				}
				//Santino
			}
			if (!string.IsNullOrEmpty(specialOfferUrl))
			{
				JSString6 = string.Format(@"window._globals['model.current_offers'] = '{0}';", specialOfferUrl);
			}
			string javascript = string.Format((@";(function() {
            
                                [[0]]
            
                                [[1]]
            
                                [[2]]      

                                [[3]]    

                                [[4]]  

                                [[5]]  
            
                                window._globals['model.book_test_drive'] = {""file"":""modal_window.html"", ""closeBtnSelector"":""#test_drive_close_btn""};
                                window._globals['model.co_model_id'] = 1; // model id of the current page (when we're on model pages)
                                window._globals['url.trim_selector'] = 'model_overview.html'; // location of trim_selector in model pages (other than model_overview)
            
                                })();").Replace("{", "$$$").Replace("}", "***").Replace("[[", "{").Replace("]]", "}"), JSString1, JSString2, JSString3, JSString4, JSString5, JSString6).Replace("$$$", "{").Replace("***", "}");

			#region oldcode

			//            string javascript= string.Format( @";(function() {
			//
			//                    window._globals['hero.video_showcase'] = {""type"":""flash"",""width"":""950"",""height"":""650"",""hashVersion"":""9"",""swf"":""http://www.buildyourhonda.ca/main.swf"",""flashvars"":{""lang"":""en"",""dataRoot"":""data%2fJSON"",""assetsRoot"":""data%2fJSON%2fassets"",""dataDomain"":""http%3a%2f%2fbuildyourhonda.ca"",""assetsDomain"":""http%3a%2f%2fbuildyourhonda.ca"",""mediaDomain"":""http%3a%2f%2fbuildyourhonda.ca""},""params"":{""allowScriptAccess"":""sameDomain"",""allowNetworking"":""all"",""wmode"":""transparent"",""quality"":""high"",""play"":""true"",""loop"":""false"",""menu"":""false"",""scale"":""showall"",""bgcolor"":""#4c4c4c""}};
			//
			//                    window._globals['hero.build_price'] = {""type"":""flash"",""width"":""950"",""height"":""650"",""hashVersion"":""9"",""swf"":""/buildit/main.swf"",""flashvars"":{""lang"":""[[en]]"",""dataRoot"":""data%2fJSON"",""assetsRoot"":""data%2fJSON%2fassets"",""dataDomain"":""[[http%3a%2f%2fbuildyourhonda.ca]]"",""assetsDomain"":""[[http%3a%2f%2fbuildyourhonda.ca]]"",""mediaDomain"":""[[http%3a%2f%2fbuildyourhonda.ca]]""},""params"":{""allowScriptAccess"":""sameDomain"",""allowNetworking"":""all"",""wmode"":""transparent"",""quality"":""high"",""play"":""true"",""loop"":""false"",""menu"":""false"",""scale"":""showall"",""bgcolor"":""#4c4c4c""}};
			//
			//                    //window._globals['hero.360_view'] = {type:'flash', src:'/_Global/swf/videoshowcase.swf', height: 400, image_width: 950, image_height: 400};
			//                    window._globals['hero.360_view'] = { ""type"": ""flash"", ""width"": ""950"", ""height"": ""402"", ""hashVersion"": ""9"", ""swf"": ""/_Global/swf/Hondaca_360Viewer.swf"", ""flashvars"": { car360: ""/_Global/assets/car360_01.swf"" }, ""params"": { ""allowScriptAccess"": ""sameDomain"", ""allowNetworking"": ""all"", ""wmode"": ""transparent"", ""quality"": ""high"", ""play"": ""true"", ""loop"": ""false"", ""menu"": ""false"", ""scale"": ""showall"", ""bgcolor"": ""#4c4c4c""} };
			//
			//
			//                    window._globals['model.book_test_drive'] = {""file"":""modal_window.html"", ""closeBtnSelector"":""#test_drive_close_btn""};
			//                    window._globals['model.co_model_id'] = 1; // model id of the current page (when we're on model pages)
			//                    window._globals['url.trim_selector'] = 'model_overview.html'; // location of trim_selector in model pages (other than model_overview)
			//
			//                    })();" ,"");

			#endregion

			return javascript;
		}

		//Santino
		public string BuildScript(Tout objTout, string filepath, string relName, string defaultPATH, string defaultSWF)
		{
			string tmp = string.Empty;

			if (objTout != null && objTout.Items != null)
			{
				string fExt = Path.GetExtension(filepath);
				switch (fExt)
				{
					case ".swf":
						tmp = BuildSWF(objTout, filepath, relName, defaultPATH, defaultSWF);
						break;
					case ".flv":
						tmp = BuildFLV();
						break;
					case ".jpeg":
						tmp = BuildJPG();
						break;
					case ".jpg":
						tmp = BuildJPG();
						break;
					case ".gif":
						tmp = BuildGIF();
						break;
				}
			}
			return tmp;
		}

		public string BuildSWF(Tout objTout, string filepath, string relName, string defaultpath, string defaultSwf)
		{
			string SWFstring = string.Empty;

			SWFstring = string.Format((@"window._globals['hero.[[4]]'] = { ""type"": ""flash"", ""width"": ""[[0]]"", ""height"": ""[[1]]"", ""hashVersion"": ""9"", ""swf"": ""[[2]]"", ""flashvars"": { car360: ""[[3]]"" }, ""params"": { ""allowScriptAccess"": ""sameDomain"", ""allowNetworking"": ""all"", ""wmode"": ""transparent"", ""quality"": ""high"", ""play"": ""true"", ""loop"": ""false"", ""menu"": ""false"", ""scale"": ""showall"", ""bgcolor"": ""#4c4c4c""} };")
								.Replace("{", "$$$")
								.Replace("}", "***")
								.Replace("[[", "{")
								.Replace("]]", "}"),
								((objTout.ContentElement.Width ?? "950") == string.Empty) ? "950" : objTout.ContentElement.Width,
								(objTout.ContentElement.Height ?? "402") == string.Empty ? "402" : objTout.ContentElement.Height,
								defaultSwf,
								(objTout.Items[0].PathReference ?? defaultpath) == string.Empty ? defaultpath : objTout.Items[0].PathReference, relName).Replace("$$$", "{").Replace("***", "}");
			return SWFstring;
		}
		public string BuildFLV()
		{
			string FLVstring = string.Empty;
			FLVstring = "";
			return FLVstring;
		}
		public string BuildJPG()
		{
			string JPGstring = string.Empty;
			JPGstring = "";
			return JPGstring;
		}
		public string BuildGIF()
		{
			string GIFstring = string.Empty;
			GIFstring = "";
			return GIFstring;
		}

		//Santino

		public string GetBuildYourHondaScript(Tout objTout)
		{
			string sBuidYourHonda = "", defaultpath, defaultSwf;
			string sBuildItTargetFolder = HondaCA.Common.Global.BuildItTargetFolder;
			defaultSwf = "/_Global/swf/buildit.swf";
			defaultpath = HondaCA.Common.Global.CurrentDomain;

			if (HTouts.ContainsKey(ToutNames[1]))
			{
				objTout = HTouts[ToutNames[1]] as Tout;
				if (objTout != null && objTout.Items != null)
				{
					sBuidYourHonda = string.Format((@"window._globals['hero.build_price'] =       {""type"":""flash"",""width"":""[[0]]"",""height"":""[[1]]"",""hashVersion"":""9"",""swf"":""[[2]]"",""flashvars"":{""lang"":""[[3]]"",""dataRoot"":""buildittool/[[8]]/data/JSON"",""assetsRoot"":""buildittool/[[8]]/data/JSON/assets"",""dataDomain"":""[[4]]"",""assetsDomain"":""[[5]]"",""mediaDomain"":""[[6]]""},
                                    ""params"":{""allowScriptAccess"":""sameDomain"",""allowNetworking"":""all"",""wmode"":""transparent"",""quality"":""high"",""play"":""true"",""loop"":""false"",""menu"":""false"",""scale"":""showall"",""bgcolor"":""#4c4c4c"",""selectedKeys"":""%7b%22zip-code%22%3a+null%2c%22province_key%22%3a+null%2c%22model_key%22%3a%22[[7]]%22%2c%22trim_level_key%22%3a+null%2c%22transmission_key%22%3a+null%7d"" }};")
									.Replace("{", "$$$").Replace("}", "***").Replace("[[", "{").Replace("]]", "}"),
									((objTout.ContentElement.Width ?? "950") == string.Empty) ? "950" : objTout.ContentElement.Width, (objTout.ContentElement.Height ?? "600") == string.Empty ? "650" : objTout.ContentElement.Height,
									defaultSwf, Language, defaultpath, defaultpath, defaultpath, ModelExportKey, sBuildItTargetFolder).Replace("$$$", "{").Replace("***", "}"); //ModelExportKey //,""selectedkeys"":""%7b%22zip-code%22%3a+null%2c%22province_key%22%3a+null%2c%22model_key%22%3a%22[[7]]%22%2c%22trim_level_key%22%3a+null%2c%22transmission_key%22%3a+null%7d""                       
				}
			}
			return sBuidYourHonda;
		}
	}
}
