﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Honda.Google.Common;

namespace Honda.Google.Web.GSA
{
	public static class GSAClientConfig
	{
		public static string GetServiceUrl()
		{
			return ConfigurationManager.AppSettings["GSAServiceUrl"];
		}

		public static string GetClient(string setting)
		{
			return ConfigurationManager.AppSettings["GSAClient_" + setting + "_Client"];
		}

		public static IList<string> GetCollections(string setting)
		{
			return ConfigurationManager.AppSettings["GSAClient_" + setting + "_Collections"].Split('|').ToList();
		}

		public static IDictionary<string, string> GetRequiredMetaFields(string setting)
		{
			return ConversionHelpers.StringToDictionary(ConfigurationManager.AppSettings["GSAClient_" + setting + "_RequiredMetaFields"]);
		}

		public static IDictionary<string, string> GetPartialMetaFields(string setting)
		{
			return ConversionHelpers.StringToDictionary(ConfigurationManager.AppSettings["GSAClient_" + setting + "_PartialMetaFields"]);
		}

		public static bool GetFilter(string setting)
		{
			return Convert.ToBoolean(ConfigurationManager.AppSettings["GSAClient_" + setting + "_Filter"]);
		}

		public static int GetPerPage(string setting)
		{
			return Convert.ToInt32(ConfigurationManager.AppSettings["GSAClient_" + setting + "_PerPage"]);
		}
	}
}