﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Honda.Google.Common.Contracts;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Text;
using Honda.Google.Common;

namespace Honda.Google.Web.GSA
{
	public class GSAClient
	{
		public string ServiceUrl { get; set; }
		public string Query { get; set; }
		public string Client { get; set; }
		public IList<string> Collections { get; set; }
		public string Language { get; set; }
		public int Start { get; set; }
		public int MaxResults { get; set; }
		public IDictionary<string, string> RequiredMetaFields { get; set; }
		public IDictionary<string, string> PartialMetaFields { get; set; }
		public bool Filter { get; set; }

		private void ValidateParameters()
		{
			if (string.IsNullOrEmpty(Query))
				throw new Exception("Query not defined.");
			if (string.IsNullOrEmpty(Client))
				throw new Exception("Client not defined.");
			if (Collections == null)
				throw new Exception("Collections not defined.");
			if (string.IsNullOrEmpty(Language))
				throw new Exception("Language not defined.");
			if (RequiredMetaFields == null)
				throw new Exception("RequiredMetaFields not defined.");
			if (PartialMetaFields == null)
				throw new Exception("PartialMetaFields not defined.");
		}

		public WebResult WebSearch()
		{
			ValidateParameters();

			NameValueCollection values = new NameValueCollection();
			values["query"] = Query;
			values["client"] = Client;
			values["collections"] = string.Join("|", Collections.ToArray());
			values["lang"] = Language;
			values["start"] = Start.ToString();
			values["perPage"] = MaxResults.ToString();
			values["requiredMetaFields"] = ConversionHelpers.DictionaryToString(RequiredMetaFields);
			values["partialMetaFields"] = ConversionHelpers.DictionaryToString(PartialMetaFields);
			values["filter"] = Convert.ToInt32(Filter).ToString();

			WebRequest request = WebRequest.Create(ServiceUrl);
			request.Method = "POST";
			request.ContentType = "application/x-www-form-urlencoded";

			string valuesString = ConversionHelpers.NameValueCollectionToString(values);
			byte[] postData = Encoding.UTF8.GetBytes(valuesString);

			request.ContentLength = postData.Length;
			using (Stream requestStream = request.GetRequestStream())
			{
				requestStream.Write(postData, 0, postData.Length);
			}

			WebResponse response = request.GetResponse();

			using (Stream stream = response.GetResponseStream())
			using (StreamReader reader = new StreamReader(stream))
			{
				string json = reader.ReadToEnd();

				return JsonConvert.DeserializeObject<WebResult>(json);
			}
		}
	}
}