﻿using System;
using System.Collections.Generic;
using System.Linq;
using HondaCA.Entity.Model;
using System.Configuration;
using System.IO;
using System.Text;

namespace Marine.Web.Code
{
	public class CommonFunctions
	{
		public static string ucWords(string _string)
		{
			_string = _string.Trim();
			try
			{
				string[] tmpString = _string.Split(new char[] { ' ' });

				for (int i = 0; i < tmpString.Length; i++)
				{
					tmpString[i] = tmpString[i].Substring(0, 1).ToUpper() + tmpString[i].Substring(1).ToLower();
				}
				return String.Join(" ", tmpString);
			}
			catch (Exception)
			{
				return _string;
			}
		}

		public static string ucFirst(string _string)
		{
			_string = _string.Trim();
			try
			{
				return _string.Substring(0, 1).ToUpper() + _string.Substring(1).ToLower();
			}
			catch (Exception)
			{
				return _string;
			}
		}

		public static string BuildModelImageUrl(Trim objTrim)
		{
			string ModelImage = string.Format("{0}_{1}_{2}", objTrim.TrimExportKey, objTrim.ColorExportKey, ConfigurationManager.AppSettings["ApplicationImageSuffix"]);
			string ImagePath = string.Format("{0}/{1}/{2}/{3}"
													, ConfigurationManager.AppSettings["AssetPathModels"]
													, (objTrim.ModelYearYear > 0 ? objTrim.ModelYearYear.ToString() : "9999")
													, objTrim.BaseModelExportKey
													, ModelImage
												 );
			if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(ImagePath)))
			{
				return ImagePath;
			}
			return ImagePath;
		}

		public static string BuildModelImageUrl(HondaModel objModel)
		{
			string ModelImage = string.Format("{0}_{1}_{2}", objModel.TrimExportKey, "", ConfigurationManager.AppSettings["ApplicationImageSuffix"]);
			string ImagePath = string.Format("{0}/{1}/{2}/{3}"
													, ConfigurationManager.AppSettings["AssetPathModels"]
													, (objModel.ModelYear > 0 ? objModel.ModelYear.ToString() : "9999")
													, objModel.ModelExportKey
													, ModelImage
												 );
			if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(ImagePath)))
			{
				return ImagePath;
			}
			return ImagePath;
		}

		public static string StringToHtmlEntities(string SourceString)
		{
			SourceString = SourceString.Replace("<", "&lt;");
			SourceString = SourceString.Replace(">", "&gt;");

			return SourceString;
		}

		public static IList<string> getDescriptionItems(string description)
		{
			string[] ArrayDetails = System.Text.RegularExpressions.Regex.Split(description, "\r\n|\n");

			IList<string> sList = ArrayDetails.ToList();
			return sList;
		}

		public static string getDefaultImagesofOverlayProd(HondaCA.Common.Language language)
		{
			string img = string.Empty;
			if (language == HondaCA.Common.Language.French)
				img = "/_Global/img/layout/accessories_default_en.jpg?maxheight=222&Crop=auto";
			else
				img = "/_Global/img/layout/accessories_default_fr.jpg?maxheight=222&Crop=auto";
			return img;
		}

		public static string getDefaultImagesofProd(HondaCA.Common.Language language)
		{
			string img = string.Empty;
			if (language == HondaCA.Common.Language.French)
				img = "/_Global/img/layout/accessories_default_en.jpg?maxheight=94&Crop=auto";
			else
				img = "/_Global/img/layout/accessories_default_fr.jpg?maxheight=94&Crop=auto";
			return img;
		}

		public static string getStringListBySeperater(IList<string> stringList, string seperator)
		{
			StringBuilder sb = new StringBuilder();
			if (stringList != null && stringList.Count > 0)
			{
				int cnt = 1;
				foreach (string s in stringList)
				{
					sb.Append(s + (stringList.Count == cnt ? string.Empty : seperator));
					cnt++;
				}
			}
			return sb.ToString();
		}

		public static IList<string> getStringItems(string description, char seperator)
		{
			string[] ArrayDetails = description.Split(seperator); //System.Text.RegularExpressions.Regex.Split(description, seperator);

			IList<string> sList = ArrayDetails.ToList();
			return sList;
		}

		public static IList<string> getEventDivisions(HondaCA.Common.Language lang)
		{
			string s = string.Empty;
			if (lang == HondaCA.Common.Language.French)
				s = ConfigurationManager.AppSettings["CMS_Event_Divisions_Fr"] ?? string.Empty;
			else
				s = ConfigurationManager.AppSettings["CMS_Event_Divisions_En"] ?? string.Empty;

			IList<string> sList = getStringItems(s, '|');
			return sList;
		}

		public static IList<string> getEventTypes(HondaCA.Common.Language lang)
		{
			string s = string.Empty;
			if (lang == HondaCA.Common.Language.French)
				s = ConfigurationManager.AppSettings["CMS_Event_EventTypes_Fr"] ?? string.Empty;
			else
				s = ConfigurationManager.AppSettings["CMS_Event_EventTypes_En"] ?? string.Empty;

			IList<string> sList = getStringItems(s, '|');
			return sList;
		}

		public static bool isStringInList(string str, List<string> stringList)
		{
			foreach (string s in stringList)
			{
				if (s.ToLower() == str.ToLower() || (s.ToLower() == "Powerequipments".ToLower() && str.ToLower() == "Power Equipment".ToLower()))
					return true;
			}
			return false;
		}

		public static string GetMainUrl(string sCategoryUrl, string sTrimUrl)
		{
			if (!string.IsNullOrEmpty(sCategoryUrl) && !string.IsNullOrEmpty(sTrimUrl))
				return string.Format("/{0}/{1}", sCategoryUrl, sTrimUrl);
			else
				return string.Empty;
		}
	}
}