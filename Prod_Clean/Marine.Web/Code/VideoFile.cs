﻿using System;
using System.Web;
using System.IO;
using System.Text;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.Common;
using System.Resources;

namespace Marine.Web
{
	public class VideoFile
	{
		public Gallery gallery;
		//public string Language;
		public string newFileName;
		public Tout tout;
		IElementContent elecontent;
		public ResourceManager ResourceManager;
		string physicalPath;
		const string ModelPath = @"_Global\xml\gallery\models\videos";
		const string GalleryPath = @"_Global\xml\gallery";
		const string VideoGalleryPath = @"_Global\xml\gallery\models\video_showcase";
		const string FuturevehicleVideoPath = @"_Global\xml\gallery\futurevehicles\videos";
		const string FuturevehiclePhotoPath = @"_Global\xml\gallery\futurevehicles\photo";
		const string BlowPath = @"_Global\xml\gallery\blow";
		public string lang;

		public enum Mode
		{
			Models = 1,
			Automotive = 2,
			VideoGallery = 3,
			CorporateLanding = 4,
			FuturevehiclesVideo = 5,
			FuturevehiclesPhoto = 6,
			Accessories = 7,
			Advntages = 8,
			Blow = 9
		}
		public Mode mode { get; set; }

		public string getPath(string modelUrl)
		{
			string physicalPath = HttpContext.Current.Request.MapPath(HttpContext.Current.Request.ApplicationPath);
			string modePath = string.Empty;
			switch (mode)
			{
				case Mode.Automotive:
				case Mode.CorporateLanding:
				case Mode.Accessories:
				case Mode.Advntages:
					modePath = GalleryPath;
					break;
				case Mode.Models:
					modePath = ModelPath;
					break;
				case Mode.VideoGallery:
					modePath = VideoGalleryPath;
					break;
				case Mode.FuturevehiclesVideo:
					modePath = FuturevehicleVideoPath;
					break;
				case Mode.FuturevehiclesPhoto:
					modePath = FuturevehiclePhotoPath;
					break;
				case Mode.Blow:
					modePath = BlowPath;
					break;
				default:
					modePath = ModelPath;
					break;
			}
			physicalPath = Path.Combine(physicalPath, modePath);
			string physicalPath1 = HttpContext.Current.Request.Url.AbsoluteUri;//Directory.CreateDirectory(newPath);// Create the subfolder
			string fName = string.Empty;

			switch (mode)
			{
				case Mode.Automotive:
					fName = "automotive_landing.xml";
					break;
				case Mode.CorporateLanding:
					if (lang.ToLower() == "fr")
						fName = "corporate_landing_fr.xml";
					else
						fName = "corporate_landing.xml";
					break;
				case Mode.Models:
					fName = modelUrl + "_videogallery.xml";
					break;
				case Mode.VideoGallery:
					fName = modelUrl + "_videoshowcase.xml";
					break;
				case Mode.FuturevehiclesVideo:
					fName = modelUrl + "_future.xml";
					break;
				case Mode.FuturevehiclesPhoto:
					fName = modelUrl + "_future.xml";
					break;
				case Mode.Advntages:
					if (lang.ToLower() == "fr")
						fName = modelUrl + "Advantagefre.xml";
					else
						fName = modelUrl + "Advantage.xml";
					break;
				case Mode.Accessories:
					if (lang.ToLower() == "fr")
						fName = modelUrl + "Accessoriesfre.xml";
					else
						fName = modelUrl + "Accessories.xml";
					break;
				case Mode.Blow:
					if (lang.ToLower() == "fr")
						fName = modelUrl + "Blowfre.xml";
					else
						fName = modelUrl + "Blow.xml";
					break;

				//default:
				//    fName = modelUrl + "_videogallery.xml";
				//    break;
			}
			newFileName = System.IO.Path.GetFileName(fName);
			physicalPath = System.IO.Path.Combine(physicalPath, newFileName);
			return physicalPath;
		}

		public string CreateFile(string modelUrl)
		{
			physicalPath = getPath(modelUrl);

			bool needToUpdate = false;

			if (mode == Mode.CorporateLanding || mode == Mode.Automotive) elecontent = tout;
			else elecontent = gallery;

			if (!System.IO.File.Exists(physicalPath))
			{
				needToUpdate = true;
			}
			else
			{
				DateTime fileUpdatedt = File.GetLastWriteTime(physicalPath);
				int compareResult;

				if (elecontent != null)
				{
					compareResult = fileUpdatedt.CompareTo(elecontent.ContentElement.LastUpdated);
					if (compareResult < 0)
					{
						needToUpdate = true;
					}
				}
			}

			if (needToUpdate)
			{
				using (StreamWriter outfile = new StreamWriter(physicalPath))
				{
					string tmp;

					switch (mode)
					{
						case Mode.Automotive:
						case Mode.CorporateLanding:
							tmp = generateXMLAutomative_Tout();
							break;
						case Mode.FuturevehiclesPhoto:
							tmp = generateXMLAutomative_gallery();
							break;
						case Mode.Models:
						case Mode.VideoGallery:
						case Mode.FuturevehiclesVideo:
							tmp = generateXMLModels();
							break;
						default:
							tmp = generateXMLModels();
							break;
					}
					outfile.Write(tmp);
				}
			}
			return physicalPath;
		}

		public string createAutomotiveFile(Language pageLanguage, VideoFile.Mode md)
		{
			string physicalPath = HttpContext.Current.Request.MapPath(HttpContext.Current.Request.ApplicationPath);
			physicalPath = Path.Combine(physicalPath, GalleryPath);

			if (md == Mode.Accessories)
			{
				if (pageLanguage == HondaCA.Common.Language.English)
				{
					newFileName = System.IO.Path.GetFileName("Accessories.xml");
				}
				else
				{
					newFileName = System.IO.Path.GetFileName("Accessoriesfre.xml");
				}
			}
			else if (md == Mode.Advntages)
			{
				if (pageLanguage == HondaCA.Common.Language.English)
				{
					newFileName = System.IO.Path.GetFileName("Advantage.xml");
				}
				else
				{
					newFileName = System.IO.Path.GetFileName("Advantagefre.xml");
				}
			}
			else
			{
				if (pageLanguage == HondaCA.Common.Language.English)
				{
					newFileName = System.IO.Path.GetFileName("Automotive.xml");
				}
				else
				{
					newFileName = System.IO.Path.GetFileName("Automotivefre.xml");
				}
			}
			physicalPath = System.IO.Path.Combine(physicalPath, newFileName);

			bool needToUpdate = false;
			if (!System.IO.File.Exists(physicalPath))
			{
				needToUpdate = true;
			}
			else
			{
				DateTime fileUpdatedt = File.GetLastWriteTime(physicalPath);
				int compareResult;

				if (tout != null)
				{
					compareResult = fileUpdatedt.CompareTo(tout.ContentElement.LastUpdated);
					if (compareResult < 0)
					{
						needToUpdate = true;
					}
				}
			}
			if (needToUpdate)
			{
				using (StreamWriter outfile = new StreamWriter(physicalPath))
				{
					outfile.Write(generateXMLAutomative_Tout());
				}
			}
			return physicalPath;
		}

		protected string generateXMLAutomative_Tout()
		{
			StringBuilder sb = new StringBuilder();

			if (tout.Items.Count > 0)
			{
				sb.Append("<?xml version='1.0'?>");
				sb.Append(Environment.NewLine);
				sb.Append("<touts>");
				sb.Append(Environment.NewLine);

				int index = 1;
				int duration = 0;
				string rootFolderPath = string.Empty;
				int pathindex = 0;

				foreach (ToutItem vlItem in tout.Items)
				{
					rootFolderPath = string.Empty;
					if (vlItem.SupportFiles != null)
					{
						if (vlItem.SupportFiles.Count > 0)
						{
							rootFolderPath = (vlItem.PathReference ?? string.Empty);
							pathindex = rootFolderPath.LastIndexOf('/');
							if (pathindex > 0) rootFolderPath = rootFolderPath.Substring(0, pathindex);
						}
					}
					if (vlItem.Duration == 0) { duration = HondaCA.Common.Global.Duration; } else { duration = vlItem.Duration; }
					sb.Append(string.Format("<tout activeTime='{0}' {1}>", duration, string.IsNullOrEmpty(rootFolderPath) ? "" : "rootfolder='" + rootFolderPath + "'"));
					sb.Append(Environment.NewLine);
					sb.Append(string.Format(@"<url><![CDATA[{0}]]></url>", (vlItem.PathReference)));
					sb.Append(Environment.NewLine);
					sb.Append("</tout>");
					index++;
				}
				sb.Append(Environment.NewLine);
				sb.Append("</touts>");
			}
			return sb.ToString();
		}

		protected string generateXMLAutomative_gallery()
		{
			StringBuilder sb = new StringBuilder();

			if (gallery.Items.Count > 0)
			{
				sb.Append("<?xml version='1.0'?>");
				sb.Append(Environment.NewLine);
				sb.Append("<touts>");
				sb.Append(Environment.NewLine);

				int index = 1;
				int duration = 0;
				foreach (GalleryItem vlItem in gallery.Items)
				{
					if (vlItem.Duration == 0) { duration = HondaCA.Common.Global.Duration; } else { duration = vlItem.Duration; }

					sb.Append(string.Format("<tout activeTime='{0}'>", duration));
					sb.Append(Environment.NewLine);
					sb.Append(string.Format(@"<url><![CDATA[{0}]]></url>", (vlItem.LargeAsset)));
					if (mode == Mode.FuturevehiclesPhoto && vlItem.Caption != null)
						sb.Append(string.Format(@"<caption>{0}</caption>", vlItem.Caption));
					sb.Append(Environment.NewLine);
					sb.Append("</tout>");
					index++;
				}
				sb.Append(Environment.NewLine);
				sb.Append("</touts>");
			}
			return sb.ToString();
		}

		protected string generateXMLModels()
		{
			StringBuilder sb = new StringBuilder();

			if (gallery.Items.Count > 0)
			{
				sb.Append("<?xml version='1.0' encoding='UTF-8'?>");

				sb.Append(Environment.NewLine);
				sb.Append(string.Format("<vidplayer>"));

				sb.Append(Environment.NewLine);
				sb.Append(string.Format("<labels>"));

				sb.Append(Environment.NewLine);
				sb.Append(string.Format(@"<label name=""replay"">{0}</label>", ResourceManager.GetString("Replay")));

				sb.Append(Environment.NewLine);
				sb.Append(string.Format(@"<label name=""share"">{0}</label>", ResourceManager.GetString("Share")));

				sb.Append(Environment.NewLine);
				sb.Append(string.Format(@"<label name=""shareBoxTitle"">{0}</label>", ResourceManager.GetString("ShareBoxTitle")));

				sb.Append(Environment.NewLine);
				sb.Append(string.Format(@"<label name=""shareBoxSubtitle"">{0}</label>", ResourceManager.GetString("ShareBoxSubTitle")));

				sb.Append(Environment.NewLine);
				sb.Append(string.Format("</labels>"));

				sb.Append(Environment.NewLine);
				sb.Append("<videos>");

				int index = 1;
				foreach (GalleryItem vlItem in gallery.Items)
				{
					sb.Append(Environment.NewLine);
					sb.Append(string.Format(@"<video thumb=""{0}"" url=""{1}"" desc=""{2}"" />"
						, (vlItem.ThumbAsset), (vlItem.LargeAsset), vlItem.Caption ?? string.Empty));
					index++;
				}
				sb.Append(Environment.NewLine);
				sb.Append("</videos>");

				sb.Append(Environment.NewLine);
				sb.Append(string.Format("</vidplayer>"));
			}
			return sb.ToString();
		}
	}
}
