﻿using System.Resources;
using System.Reflection;
using System.Threading;
using System.Globalization;
using HondaCA.Common;

namespace Marine.Web
{
	public abstract class BasePage : System.Web.UI.Page
	{
		public abstract Language PageLanguage { get; set; }

		public BasePage()
		{
		}

		public ResourceManager ResourceManager
		{
			get
			{
				Assembly assembly = Assembly.GetExecutingAssembly();
				ResourceManager resourceManager = new ResourceManager("Marine.Web.Resources.Strings", assembly);
				return resourceManager;
			}
		}

		protected new virtual void InitializeCulture()
		{
			if (PageLanguage == 0)
				PageLanguage = Language.English;

			SetCulture(PageLanguage);
		}

		private void SetCulture(Language language)
		{
			string cultureString = Cultures.GetCultureStringValue(language);
			UICulture = cultureString;
			Culture = cultureString;

			Thread.CurrentThread.CurrentCulture =
				CultureInfo.CreateSpecificCulture(cultureString);
			Thread.CurrentThread.CurrentUICulture = new
				CultureInfo(cultureString);
			base.InitializeCulture();
		}
	}
}
