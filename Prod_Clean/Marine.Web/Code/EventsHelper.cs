﻿using System;
using System.Collections.Generic;
using System.Linq;
using Navantis.Honda.CMS.Client.Elements;

namespace Marine.Web.Code
{
	public static class EventsHelper
	{
		public static Event FilterandSortEventsForDate(Event eventElement, bool filterbyEndDate)
		{
			List<EventItem> tmpEventList = new List<EventItem>();

			if (eventElement != null)
			{
				if (eventElement.ContentElement.Display)
				{
					if (eventElement.Items.Count > 0)
					{
						try
						{
							if (filterbyEndDate)
								tmpEventList = eventElement.Items.Where(x => x.EndDate >= DateTime.Today).OrderBy(x => x.StartDate).ToList<EventItem>();
							else
								tmpEventList = eventElement.Items.OrderBy(x => x.StartDate).ToList<EventItem>();
						}
						catch { }
						eventElement.Items = tmpEventList;
					}
				}
			}
			return eventElement;
		}

		public static Event FilterEvents(string sDivision, string sProvince, string sEventType, Event eventElement)
		{
			List<EventItem> tmpEventList = new List<EventItem>();
			bool checkDivision = true, checkProvince = true, checkEventType = true;

			if (string.IsNullOrEmpty(sDivision)) checkDivision = false;
			if (string.IsNullOrEmpty(sProvince)) checkProvince = false;
			if (string.IsNullOrEmpty(sEventType)) checkEventType = false;

			if (checkDivision || checkProvince || checkEventType)
			{
				if (eventElement != null)
				{
					if (eventElement.ContentElement.Display)
					{
						if (eventElement.Items.Count > 0)
						{
							foreach (EventItem eventItem in eventElement.Items)
							{
								bool isitemInDivision = false, isitemInProvince = false, isitemInEventSize = false;
								if (checkDivision)
									isitemInDivision = CommonFunctions.isStringInList(sDivision, eventItem.Divisions);
								else
									isitemInDivision = true;
								if (checkProvince)
									isitemInProvince = eventItem.Province.ToLower() == sProvince.ToLower() ? true : false;
								else
									isitemInProvince = true;
								if (checkEventType)
									isitemInEventSize = CommonFunctions.isStringInList(sEventType, eventItem.EventTypes);
								else
									isitemInEventSize = true;

								if (isitemInDivision && isitemInProvince && isitemInEventSize)
								{
									tmpEventList.Add(eventItem);
								}
							}
							eventElement.Items = tmpEventList;
						}
					}
				}
			}
			// setEvents(eventElement);
			return eventElement;
		}
	}
}