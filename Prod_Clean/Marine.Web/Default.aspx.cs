﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using Marine.Web.ContentPages;
using HondaCA.Web;

namespace Marine.Web
{
    public partial class Default : ContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-landing");
                this.IsMasterBodyClassUpdate = true;
            }

            //this.IsStaging = false;
            IElementContent element = this.ContentPage.Elements["GenericContent_Tout"].DeserializeElementObject() as IElementContent;
            plTopBanner.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;

            if (plTopBanner.Visible)
              plTopBanner.Controls.Add(new LiteralControl(GetToutWithXml("GenericContent_Tout", 950, 400)));

            GenericContent gContent = this.ContentPage.Elements["GenericContent_GC1"].DeserializeElementObject() as GenericContent;
            plGenericThreeColumns.Visible = gContent.ContentElement.Display ? true : this.IsStaging ? true : false;
            divThreeGeneric.Visible = plGenericThreeColumns.Visible ? true : false;

            if(plGenericThreeColumns.Visible == true)
                plGenericThreeColumns.Controls.Add(new LiteralControl(setGenericContent(gContent)));
        }
                
        protected string setGenericContent(GenericContent content)
        {
            StringBuilder sb = new StringBuilder();
            if (content != null)
            {
                if (content.ContentElement.Display || IsStaging == true)
                {
                    if (content.Items != null)
                    {
                        sb.Append(@"<dl class=""fc landing-touts"">");
                        //int firstElement = 0;
                        string strtarget = string.Empty;
                        foreach(Navantis.Honda.CMS.Client.Elements.GenericContentItem item in content.Items)
                        {
                            strtarget = string.IsNullOrWhiteSpace(item.ImageLinkURL) ? "" : item.ImageLinkURL.StartsWith("http") ? "target='_blank'" : "";
                            sb.Append(string.Format(@"<dd class=""fl""><a href=""{0}"" class=""trackable"" {2}><div><img class='round-corners-small' src='_Global\img\layout\round_corners_230_131.png'/><img src=""{1}?crop=auto&height=131&width=230"" alt="""" /></div></a>", item.ImageLinkURL, item.Image, strtarget));
                            if ((item.Title ?? string.Empty).Trim() != string.Empty)
                            {
                                strtarget = string.IsNullOrWhiteSpace(item.TitleURL) ? "" : item.TitleURL.StartsWith("http") ? "target='_blank'" : "";
                                sb.Append(string.Format(@"<p class=""heading""><a title=""{0}"" href=""{1}"" {3}>{2}</a></p>", item.Title, item.TitleURL, item.Title, strtarget));
                            }
                            sb.Append(string.Format(@"{0}</dd>", item.Summary));
                            
                        }
                    }
                }
            }
            sb.Append(@"</dl>");
            return sb.ToString();
        }
    }
}


