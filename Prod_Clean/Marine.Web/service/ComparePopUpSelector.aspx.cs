﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using HondaCA.Service.Model;
using Marine.Service.Extensions;
using Marine.Web.ContentPages;
using System.Web.UI.HtmlControls;
using HondaCA.Common;
using Marine.Web.Code;
using HondaATV.Service.Model;


namespace Marine.Web.Service
{
    public partial class ComparePopUpSelector : ContentBasePage
    {
        string CategoryUrl;
        int ModelYear=0;
                
        private int CategoryID=0;

        protected override void OnInit(EventArgs e)
        {
            this.IsNotCMSPage = true;
            base.OnInit(e);
        }

        class TrimExt
        {
            public Trim Trim { get; set; }
            public decimal Discount { get; set; }
            public decimal Price { get; set; }
            public decimal MSRP { get; set; }
			public bool IsComingSoon { get; set; }
            public bool HasDiscount { get; set; }
            public string Thumbnail { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string lang = Request.QueryString["L"] != null ? Request.QueryString["L"] : "";
            string sModelYear = Request.QueryString["ModelYear"] != null ? Request.QueryString["ModelYear"] : "";
            lang = lang.Length > 2 ? lang.Substring(0, 2) : lang;
            if (lang.ToUpper() == Language.French.GetCultureStringValue().Substring(0, 2).ToUpper())
                this.PageLanguage = Language.French;
            else
                this.PageLanguage = Language.English;

            base.InitializeCulture();

           

            Int32.TryParse(sModelYear, out ModelYear);
            CategoryUrl = Request.QueryString["CategoryUrl"];


            setTrimSelector();
        }

        protected void setTrimSelector()
        {
            
            EntityCollection<ModelCategory> listModelCat = new EntityCollection<ModelCategory>();
            HondaModelService ModService = new HondaModelService();
            listModelCat = ModService.GetModelCategory(PageLanguage, this.TargetID);

            if (listModelCat.Count > 0)
            {
                rptSeries.DataSource = listModelCat;
                rptSeries.DataBind();

                if (string.IsNullOrEmpty(CategoryUrl))
                {
                    CategoryUrl = listModelCat[0].CategoryUrl;
                }

                PETrimService ts = new PETrimService();
                IList<Trim> oTrims = ts.GetTrimsModelCategoryUrl(this.TargetID, CategoryUrl, PageLanguage);
                oTrims = oTrims.Select(trim => trim.GetPriceAdjustedTrim(TargetID, PageLanguage)).ToList();
                try
                {
                    if (oTrims.Count > 0)
                    {
                        List<TrimExt> trimList = new List<TrimExt>();                        

                        foreach (Trim trim in oTrims)
                        {							
                            TrimExt t = new TrimExt();
                            decimal MSRP;
                            if (!this.IsQuebecPricing)
                              MSRP = trim.MSRP;
                            else
                              MSRP = trim.MSRP + trim.FreightPDI;                   
                            
							Colour oColour = null;
                            t.Trim = trim;
                            t.Discount = PETrimService.getDiscountAmount(MSRP, trim.PriceDiscountAmount, trim.PriceDiscountPercentage);
                            t.Price = PETrimService.getDiscountedPrice(MSRP, trim.PriceDiscountAmount, trim.PriceDiscountPercentage);
                            t.HasDiscount = t.Discount > 0;
                            t.MSRP = MSRP;
                            t.Thumbnail = CommonFunctions.BuildModelImageUrl(trim);
                            t.IsComingSoon = ts.IsComingSoonWithColor(trim, oColour);
                            trimList.Add(t);
                        }
                        rptTrims.DataSource = trimList;
                        rptTrims.DataBind();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        protected int getModelCategoryID(string CategoryUrl, EntityCollection<ModelCategory> listModelCat) 
        {
            foreach (ModelCategory category in listModelCat)
            {
                if (category.CategoryUrl.ToLower() == CategoryUrl.ToLower())
                    return category.CategoryID;
            }

            return 0;
        }

        protected void rptSeries_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            string optiontxt = @"<option value=""{0}"">{1}</option>";
            ModelCategory category =  (ModelCategory)e.Item.DataItem;
            Literal OptionItem = (Literal)e.Item.FindControl("OptionItem");
            if (OptionItem != null)
                OptionItem.Text = string.Format(optiontxt, string.Format(@"/service/ComparePopUpSelector.aspx?CategoryUrl={0}&L={1}", category.CategoryUrl, this.PageLanguage.GetCultureStringValue().Length > 0 ? this.PageLanguage.GetCultureStringValue().Substring(0,2):"en"), category.CategoryName );
        }

        protected void rptTrims_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            
            TrimExt trim = (TrimExt)e.Item.DataItem;
            
            PlaceHolder phNoDiscount = (PlaceHolder)e.Item.FindControl("phNoDiscount");
            PlaceHolder phHasDiscount = (PlaceHolder)e.Item.FindControl("phHasDiscount");
			PlaceHolder phComingSoon = (PlaceHolder)e.Item.FindControl("phComingSoon");

			if (trim.IsComingSoon)
			{
				phComingSoon.Visible = true;
			}
			else
			{
				if (trim.Discount > 0)
				{
					phHasDiscount.Visible = true;
				}
				else
				{
					phNoDiscount.Visible = true;
				}
			}
        }
    }
}