﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Honda.JSON.MCPE;
using Newtonsoft.Json;
using Honda.JSON.Convertors;
using HondaCA.Entity;
using Newtonsoft.Json.Serialization;
using Navantis.Honda.CMS.Demo;
using HondaCA.Common;

namespace Marine.Web.service
{
    
        public partial class TrimListing : CmsContentBasePage
        {
          bool quebec = false;
          protected override void OnInit(EventArgs e)
            {
                this.IsNotCMSPage = true;
            }
            protected void Page_Load(object sender, EventArgs e)
            {
                string lang = Request.QueryString["L"] != null ? Request.QueryString["L"] : "";
                lang = lang.Length > 2 ? lang.Substring(0, 2) : lang;
                if (lang.ToUpper() == Language.French.GetCultureStringValue().Substring(0, 2).ToUpper())
                    this.PageLanguage = Language.French;
                else
                    this.PageLanguage = Language.English;

                base.InitializeCulture();

               string ModelFamily, TrimExportKeyList = string.Empty;
                //string lang = Request.Form["lang"];
                Response.ContentType = "application/json";
                Honda.JSON.MCPE.TrimList trimList = new Honda.JSON.MCPE.TrimList();

                ModelFamily = Request.QueryString["modelfamily"] ?? string.Empty;
                TrimExportKeyList = Request.QueryString["trimexportkeylist"] ?? string.Empty;

                //if (!string.IsNullOrEmpty(ModelFamily) && !string.IsNullOrEmpty(TrimExportKeyList))
                //{
                //    trimList = getTrimListByTrimExportKeyList(ModelFamily, TrimExportKeyList);
                //}
                //else
                //    trimList = getDummyTrimItems();//todo:return null object

                if (!string.IsNullOrEmpty(TrimExportKeyList))
                {
                    trimList = getTrimListByTrimExportKeyList(string.Empty, TrimExportKeyList);
                }
                else
                    trimList = getDummyTrimItems();//todo:return null object




                string json = @"{""value:"":""null"";}";


                try
                {
                    json = JsonConvert.SerializeObject(trimList, Formatting.Indented, new KeyedListConverter());
                    //json = JsonConvert.SerializeObject(trimList.TrimItems[0]   , Formatting.Indented, new KeyedListConverter());
                }
                catch (Exception ex)
                {
                    json = "{exception:" + ex.Message + "}";
                }

                Response.Write(json);

            }


            public string GetPropertyValue(Object value)
            {

                Type type = value.GetType();
                System.Reflection.FieldInfo fieldInfo = type.GetField(value.ToString());

                JsonProperty[] attribs = fieldInfo.GetCustomAttributes(
                    typeof(JsonProperty), false) as JsonProperty[];
                return attribs.Length > 0 ? attribs[0].PropertyName : null;

            }


            protected TrimList getTrimListByTrimExportKeyList(string ModelFamily, string TrimExportKeyList)
            {

                TrimList trimList = new TrimList();
                Json_TrimList trimlistservice = new Json_TrimList(this.PageLanguage, this.TargetID, this.ResourceManager);
                trimList = trimlistservice.GetTrimList(ModelFamily, TrimExportKeyList, this.IsQuebecPricing);
                return trimList;
            }

            protected TrimList getDummyTrimItems()
            {
                TrimList trimList = new TrimList();
                //trimList.Primary = "xyz";
                //trimList.SpecsItems= createDummySpec();
                //IList<Trim> trims = new List<Trim>();

                //trims.Add(createDummyTrim("TrimAA"));
                //trims.Add(createDummyTrim("TrimAB"));
                //trimList.TrimItems = trims;
                Json_TrimList trimlistservice = new Json_TrimList(this.PageLanguage, this.TargetID, this.ResourceManager);
                //trimList = trimlistservice.GetTrimListDummy("generators", "eb3800xc,eb5000ic,eb5000xk2c,eb6500xc");
                trimList = trimlistservice.GetTrimList("generators", "eu1000ic,eb5000ic,eb5000xk2c,eb6500xc");
                return trimList;
            }


        }
    
}