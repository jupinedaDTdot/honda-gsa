﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Marine.Service.Extensions;
using Newtonsoft.Json;
//using Honda.JSON.Convertors;
using HondaCA.Entity;
using Newtonsoft.Json.Serialization;
using HondaATV.Service.Model;
using HondaCA.Common;
using System.Resources;

namespace Marine.Web.service
{
    public class Json_TrimList
    {
        public Language PageLanguage { set; get; }
        public int TargetID { set; get; }
        public ResourceManager ResourceManager;

        public Json_TrimList(Language lang, int targetID, ResourceManager resourceManager)
        {
            PageLanguage = lang;
            TargetID = targetID;
            ResourceManager = resourceManager;
        }

        public Honda.JSON.MCPE.TrimList GetTrimListDummy(string ModelFamilyUrl, string CommaSeperatedTrimCodeList)
        {
            Honda.JSON.MCPE.TrimList trimList = new Honda.JSON.MCPE.TrimList();


            trimList.Primary = "PrimaryKey";
            trimList.SpecsItems = createDummySpec();
            IList<Honda.JSON.MCPE.Trim> trims = new List<Honda.JSON.MCPE.Trim>();

            trims.Add(createDummyTrim("TrimAA"));
            trims.Add(createDummyTrim("TrimAB"));
            trimList.TrimItems = trims;


            return trimList;

            //HondaPE.Entity.Specs
        }


        public Honda.JSON.MCPE.TrimList GetTrimList(string ModelFamilyUrl, string CommaSeperatedTrimCodeList, bool bQc = false)
        {
            Honda.JSON.MCPE.TrimList trimList = new Honda.JSON.MCPE.TrimList();


            trimList.Primary = "PrimaryKey";
            IList<Honda.JSON.MCPE.Trim> trims = new List<Honda.JSON.MCPE.Trim>();
            trimList.TrimItems = this.mapTrimList(ModelFamilyUrl, CommaSeperatedTrimCodeList, bQc);
            trimList.SpecsItems = GenerateParentSpecItemList(trimList.TrimItems);

            return trimList;

            //HondaPE.Entity.Specs
        }

        private IList<Honda.JSON.MCPE.SpecItem> GenerateParentSpecItemList(IList<Honda.JSON.MCPE.Trim> trimList)
        {

            IList<Honda.JSON.MCPE.SpecItem> specitemList = new List<Honda.JSON.MCPE.SpecItem>();

            foreach (Honda.JSON.MCPE.Trim trim in trimList)
            {
                if (trim.SpecsItems != null)
                {

                    foreach (Honda.JSON.MCPE.SpecItem specItem in trim.SpecsItems)
                    {
                        bool isadded = false;

                        foreach (Honda.JSON.MCPE.SpecItem tmpspecitem in specitemList)
                        {
                            if (tmpspecitem.Key == specItem.Key)
                            {
                                isadded = true;
                                break;
                            }
                        }

                        if (!isadded)
                            specitemList.Add(specItem);
                    }
                }

            }

            return specitemList;

        }

        public IList<Honda.JSON.MCPE.Trim> mapTrimList(string ModelFamilyUrl, string CommaSeperatedTrimCodeList, bool bQc = false)
        {
            IList<Honda.JSON.MCPE.Trim> trimList = new List<Honda.JSON.MCPE.Trim>();

            PETrimService trimService = new PETrimService();
            HondaCA.Entity.Model.Trim oTrim;

            string[] TrimExportList;


            if (!string.IsNullOrEmpty(CommaSeperatedTrimCodeList))
            {
                if (CommaSeperatedTrimCodeList.Contains(","))
                    TrimExportList = CommaSeperatedTrimCodeList.Split(',');
                else
                {
                    TrimExportList = new string[1];
                    TrimExportList[0] = CommaSeperatedTrimCodeList;
                }
                foreach (string str in TrimExportList)
                {
                    //oTrim = trimService.GetTrimByTrimExportKey(ModelFamilyUrl, TargetID, str, PageLanguage.ToString());
                    oTrim = trimService.GetTrimsByTrimExportKey_ModelYear(TargetID, str, PageLanguage);
                    if (oTrim != null)
                    {
                      oTrim = oTrim.GetPriceAdjustedTrim(TargetID, PageLanguage);
                      trimList.Add(mapTrim(oTrim, bQc));
                    }

                }
            }



            return trimList;
        }

        protected Honda.JSON.MCPE.Trim mapTrim(HondaCA.Entity.Model.Trim trim, bool isQuebecProvince = false)
        {
			PETrimService ts = new PETrimService();
			HondaCA.Entity.Model.Colour oColour = null;

            Honda.JSON.MCPE.Trim jsonTrim = new Honda.JSON.MCPE.Trim();
            jsonTrim.Key = trim.TrimExportKey + "-" + trim.ModelYearYear;
            jsonTrim.Title = trim.TrimName;
            jsonTrim.Text_MSRP = ResourceManager.GetString("MSRP*");
            jsonTrim.Text_Discount = ResourceManager.GetString("Discount");
            jsonTrim.Text_Final_Price = ResourceManager.GetString("YourPrice*");
            if (!isQuebecProvince)
              jsonTrim.MSRP = trim.MSRP;
            else
              jsonTrim.MSRP = trim.MSRP + trim.FreightPDI;
            //Code chage for removing Discount
            jsonTrim.IsComingSoon = ts.IsComingSoonWithoutColor(trim);//ts.IsComingSoonWithColor(trim, oColour);
            jsonTrim.Discount = trim.GetDiscount(isQuebecProvince);
            jsonTrim.Price = trim.GetFinalPrice(isQuebecProvince);
            jsonTrim.Thumbnail = Marine.Web.Code.CommonFunctions.BuildModelImageUrl(trim) + "?maxwidth=90"; // "?Crop=auto&Width=150&Height=150";
            jsonTrim.SpecsItems = GetSpecItems(trim.TrimID);


            return jsonTrim;
        }

        protected IList<Honda.JSON.MCPE.SpecItem> GetSpecItems(int trimID)
        {
            SpecsService ss = new SpecsService();
            EntityCollection<HondaATV.Entity.Specs> TrimSpecs = ss.GetSpecsByTrimId(trimID, PageLanguage.GetCultureStringValue(), this.TargetID);
            IList<Honda.JSON.MCPE.SpecItem> JsonSpecsItems = new List<Honda.JSON.MCPE.SpecItem>();
            foreach (HondaATV.Entity.Specs specItem in TrimSpecs)
            {
                if (specItem != null)
                {
                    bool isadded = false;

                    foreach (Honda.JSON.MCPE.SpecItem tmpspecitem in JsonSpecsItems)
                    {
                        if (tmpspecitem.Key == specItem.SpecName)
                        {
                            isadded = true;
                            break;
                        }
                    }

                    if (!isadded)
                        JsonSpecsItems.Add(MapSpecItem(specItem));
                }
            }
            //todo:remove createDummySpec 
            //if(JsonSpecsItems.Count <= 0)
            //    JsonSpecsItems = createDummySpec();

            return JsonSpecsItems;
        }

        protected Honda.JSON.MCPE.SpecItem MapSpecItem(HondaATV.Entity.Specs specItem)
        {
            Honda.JSON.MCPE.SpecItem s = new Honda.JSON.MCPE.SpecItem();
            if (specItem.SpecName != null)
            {
                s.Key = specItem.SpecName;
                s.SpecName = specItem.SpecName;
                s.SpecLabel = specItem.SpecLabel ?? string.Empty;
                s.SpecType = specItem.SpecType ?? string.Empty;
                s.SpecValue = specItem.SpecValue ?? string.Empty;
                s.SpecTip = specItem.SpecTip ?? string.Empty;
                s.IsSlider = specItem.IsSlider;
            }
            return s;
        }

        protected IList<Honda.JSON.MCPE.SpecItem> createDummySpec()
        {

            IList<Honda.JSON.MCPE.SpecItem> items = new List<Honda.JSON.MCPE.SpecItem>();
            Honda.JSON.MCPE.SpecItem s = new Honda.JSON.MCPE.SpecItem();

            s.Key = "engine_type";
            s.SpecName = "engine_type";
            s.SpecValue = "engineType1";
            s.SpecLabel = "LabelEngineType dummy";
            s.SpecTip = "tip";
            s.SpecType = "type";
            items.Add(s);

            s = new Honda.JSON.MCPE.SpecItem();
            s.Key = "new_type";
            s.SpecName = "new_type";
            s.SpecValue = "120.00";
            s.SpecLabel = "New Type dummy";
            items.Add(s);

            s = new Honda.JSON.MCPE.SpecItem();
            s.Key = "honda_engine_type";
            s.SpecName = "honda_engine_type";
            s.SpecValue = "type2";
            s.SpecLabel = "Honda Engine dummy";
            items.Add(s);

            s = new Honda.JSON.MCPE.SpecItem();
            s.Key = "displacement";
            s.SpecName = "displacement";
            s.SpecValue = "25";
            s.SpecLabel = "Displacement dummy";
            items.Add(s);

            s = new Honda.JSON.MCPE.SpecItem();
            s.Key = "ignition_system";
            s.SpecName = "ignition_system";
            s.SpecValue = "IgnitionSystem1";
            s.SpecLabel = "Ignition dummy";
            items.Add(s);

            s = new Honda.JSON.MCPE.SpecItem();
            s.Key = "zone_start_safety_system";
            s.SpecName = "zone_start_safety_system";
            s.SpecValue = "zone_start_safety_system1";
            s.SpecLabel = "Zone start dummy";
            items.Add(s);

            return items;
        }

        protected Honda.JSON.MCPE.Trim createDummyTrim(string TrimCode)
        {
            Honda.JSON.MCPE.Trim trim = new Honda.JSON.MCPE.Trim();
            trim.Key = TrimCode;
            trim.Discount = 1.10M;
            trim.MSRP = 4444;
            trim.Text_MSRP = "msrp";
            trim.Title = TrimCode;
            trim.SpecsItems = createDummySpec();
            trim.Price = 4444;
            trim.Thumbnail = "swatch_11";

            return trim;
        }


    }
}