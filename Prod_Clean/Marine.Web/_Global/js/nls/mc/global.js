var lang = 'en';
define({
    'tracker.account': 'UA-5158555-30'
    , 'system.product_line': 'M'
    
    , 'url.survey': 'http://www.hondasurvey.ca/motorcycle/?lang=en&sid='
    , 'url.survey_window': '/survey/survey_eng.html'

    , 'build_it.accessory.default_image': '/assets/Accessories/en_NA_thumbnail.png'
    , 'build_it.details.url': '/buildyourmotorcycle/{category_key}/{model_key}/{model_year_key}'
    , 'build_it.details.overrides': {
    }
});
