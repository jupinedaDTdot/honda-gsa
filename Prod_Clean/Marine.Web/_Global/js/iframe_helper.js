;(function(){
window._cross_domain_helpers = {
    resize: function(width, height) {
    	try {
	        if (!isNaN(parseInt(width))) {
	            document.getElementsByTagName('iframe')[0].width = width;
	        }
	        if (!isNaN(parseInt(height))) {
	            document.getElementsByTagName('iframe')[0].height = height + 60;
	        }
    	} catch (e) {
    		if (typeof console != 'undefined') {
    			console.log('unable to find the iframe element.');
    		}
    	}
    }
};
})();