// $Id: jquery.honda.match_vehicle.js 959 2011-01-21 22:22:06Z yuxhuang $

;
(function($) {

  var SLIDER_MIN = 0;
  var SLIDER_MAX = 30;

  function format(type) {
    switch (type) {
    case 'lease':
    case 'finance':
      return '#,###.00';
    case 'msrp':
      return '#,###';
    case 'seats':
      return '#';
    case 'fuel_efficiency':
      return '#.0';
    default:
      return '#';
    }
  }

  var Condition = function(min, max) {

    var _min, _max, _start, _end;

    $.extend(this, {
      min: function(value) {
        if (value) {
          _min = value;
        }
        return _min;
      },
      max: function(value) {
        if (value) {
          _max = value;
        }
        return _max;
      },
      range: function(start, end) {
        if (start && end) {
          _start = start;
          _end = end;
        }

        return {
          start: _start,
          end: _end
        };
      },
      to_slider: function(min, max) {
        var actual_start = 0, actual_end = 0;
        try {
          var scale = (max - min) / (this.max() - this.min());
          var range = this.range();
          actual_start = min + (range.start - this.min()) * scale;
          actual_end = min + (range.end - this.min()) * scale;
        } finally {
          return {
            start: actual_start,
            end: actual_end
          };
        }
      },
      intersects: function(value, precision) {
        var range = this.range();
        if (value == null) {
          return false;
        }
        // convert to precision
        range = {
          start: Math.floor(range.start / precision),
          end: Math.ceil(range.end / precision)
        };
        if (typeof value == 'object') {
          value = {
            base_trim: Math.round(value.base_trim / precision),
            top_trim: Math.round(value.top_trim / precision)
          };
          if (value.base_trim <= range.end && value.top_trim >= range.start) {
            return true;
          } else {
            return false;
          }
        } else {
          value = Math.round(value / precision);
          if (value <= range.end && value >= range.start) {
            return true;
          } else {
            return false;
          }
        }
      }
    });

    this.min(min);
    this.max(max);
    this.range(min, max);
  };


  var defaults = {
    navigationController: null,
    sliderSelector: '.honda_match_slider',
    typeSelector: '.honda_match_type',
    modelSelector: '.honda_match_model',
    modelContainerSelector: '.honda_match_model_container',
    defaultType: 'msrp',
    format: global_get('honda_match.format'),
    reload: true,
    simple: false,
    include_fees: false,
    loyalty_offer: '',
    modelsUrl: function() {
    },
    modelsFinancialUrl: function() {
    },
    // event
    slider_complete: function(event, ui) {
      $('.ui-slider-handle', ui.element).each(function() {
        $(this).empty().append($('<span>'));
        if (typeof DD_belatedPNG != 'undefined') {
          DD_belatedPNG.fixPng($(this).find('span').get(0));
        }

        var zindex = $(this).css('z-index');
        $(this).mouseenter(function() {
          $(this).siblings().css('z-index', 0);
          $(this).css('z-index', 10);
        });
      });
    },
    // set the handle value
    handle_set_values: function(start, end) {
      start = isNaN(start) ? 0 : start;
      end = isNaN(end) ? 0 : end;

      var data = $(this).data('honda_match');
      var type = data.type;
      var opts = data.options;
      var ui = $(data.options.sliderSelector, this).data('slider');

      try {
        var precision = data.options.format.precision[type];
        start = Math.floor(start / precision) * precision;
        end = Math.ceil(end / precision) * precision;
      } catch(e) {
      }

      $('span', ui.handles[0]).text(start).format({ format: format(type) });
      $('span', ui.handles[1]).text(end).format({ format: format(type) });

      try {
        $('span', ui.handles[0]).prepend(opts.format.prefix[type]);
      } catch(e) {
      }
      try {
        $('span', ui.handles[0]).append(opts.format.suffix[type]);
      } catch(e) {
      }
      try {
        $('span', ui.handles[1]).prepend(opts.format.prefix[type]);
      } catch(e) {
      }
      try {
        $('span', ui.handles[1]).append(opts.format.suffix[type]);
      } catch(e) {
      }
    },
    // type events
    type_change: function(event) {
      Cufon.refresh('.match_buttons a');
    },
    // callback functions for create model
    /*
    <span class="match_vehicle">
    <a href="#" class="pf match_btn btn_civic_coupe" rel="civic_coupe"></a>
    <a href="#">
    <span class="h3">Civic Coupe</span>
    <span class="price">5.4L/100km</span>
    </a>
    </span>
    */
    create_model: function(model_key, model) {
      // get current type
      var type = $(this).data('honda_match').type;
      var img_path = global_get('url.honda_match.thumbnail_path');
      img_path = $.isFunction(img_path) ? img_path.apply(this) : img_path;
      img_path = img_path.replace('{model_key}', model_key).replace('{year}', model.year);

      var span = $('<span>').addClass('match_vehicle honda_match_model').append(
        $('<a>').addClass('pf match_btn').attr({
          href: model['hondaca_url']
        }).append(
          $('<img>').attr({
            src: img_path,
            height: global_get('url.honda_match.thumbnail_height'),
            width: global_get('url.honda_match.thumbnail_width'),
            alt: model.name
          }).addClass('pf')
        )
      ).append(
        $('<a>').append(
          $('<span>').addClass('honda_match_model_title h3').text(model.name)
        ).append(
          $('<span>').addClass('honda_match_model_content price')
        ).attr({
          href: model['hondaca_url']
        })
      )
        .hover(function() {
          // if (!$(this).is('.honda_match_model_disabled')) {
          $(this).addClass('hover');
          Cufon.replace($('.h3', this));
          // }
        }, function() {
          $(this).removeClass('hover');
          Cufon.replace($('.h3', this));
        });
      span.bind('honda_match_type_change', function() {
        if ($(this).hasClass('honda_match_model_disabled')) {
          $('img', this).css({ opacity: 0.25 });
        } else {
          $('img', this).css({ opacity: 1 });
        }
      });
      return span;
    },
    append_models: function(models) {
      var group = models.splice(0, 5);
      var div = $('<div>').addClass('fc match_row nmt');
      $.each(group, function(index, element) {
        div.append(element);
      });
      div.appendTo($('.match_cars', this));

      group = models.splice(0, 4);
      div = $('<div>').addClass('fc match_row');
      $.each(group, function(index, element) {
        div.append(element);
      });
      div.appendTo($('.match_cars', this));

      group = models;
      div = $('<div>').addClass('fc match_row');
      $.each(group, function(index, element) {
        div.append(element);
      });
      div.appendTo($('.match_cars', this));

      // png fix
      if (typeof DD_belatedPNG != 'undefined') {
        $('.pf', this).each(function() {
          DD_belatedPNG.fixPng(this);
        });
      }

      Cufon.replace($('.h3', this));
    },
    set_thresholds: function(lease, finance) {
    },
    _last: null
  };

  $.widget('honda.match_vehicle', {
    widgetEventPrefix: 'match_vehicle_',
    options: $.extend(true, { }, defaults, (function() {
      try {
        return global_get('honda.match_vehicle.defaults');
      } catch(e) {
        return { };
      }
    })()),
    _create: function() {
      var self = $(this.element);
      // var opts = $.extend(true, {}, $.fn.honda_match.defaults, options);
      var opts = this.options, o = this.options;

      if (o.reload) {
        this.reloadData();
      }

      // hook up to type switching anchors
      $('.honda_match_type', self).click(function() {
        $('.honda_match_type', self).removeClass('active selected');
        var type = $(this).attr('rel');
        $(this).addClass('active selected');

        self.data('honda_match').type = type;

        // slider
        $('.honda_match_slider', self).trigger('honda_match_type_change');
        $('.honda_match_model', self).trigger('honda_match_model_change');
        $('.honda_match_model', self).trigger('honda_match_type_change');

        self.trigger('honda_match_type_change');

        // Cufon.replace($('.honda_match_type', this), {fontFamily: 'National', hover: true});
        // Cufon.refresh();
        $('.match_disclaimer', self).find('> div, > span').hide().end()
          .find('#disclaimer_' + type).show();
        return false;
      });

      self.bind('honda_match_type_change', opts.type_change);

      $('.previous', self).click(function() {
        o.navigationController.expectViewController({
          returnValue: 'previous'
        });
        return false;
      });
    },
    reloadData: function() {
      var self = $(this.element),
          opts = this.options,
          o = this.options,
          root = this;

      var url = $.isFunction(opts.modelsUrl) ? opts.modelsUrl.apply(this) : opts.modelsUrl;
      var financeUrl = $.isFunction(opts.modelsFinancialUrl) ? opts.modelsFinancialUrl.apply(this) : opts.modelsFinancialUrl;

      // when the slider slides

      function do_slide(event, ui) {
        var match_start = 0, match_end = 0;
        try {
          var honda_match = self.data('honda_match');
          var type = honda_match.type;
          var condition = honda_match.conditions[type];

          // update the match range
          var scale = (condition.max() - condition.min()) / (SLIDER_MAX - SLIDER_MIN);
          var range = ui.values;
          match_start = condition.min() + (range[0] - SLIDER_MIN) * scale;
          match_end = condition.min() + (range[1] - SLIDER_MIN) * scale;
          condition.range(match_start, match_end);
        } finally {
          opts.handle_set_values.apply(self, [match_start, match_end]);
          do_change.apply(self, [event, ui]);
        }
      }

      function do_change(event, ui) {
        var honda_match = self.data('honda_match');
        var type = honda_match.type;
        var condition = honda_match.conditions[type];

        // assign values to the models
        $(opts.modelSelector, self).each(function() {
          var self = $(this);

          // check the value
          var model = self.data('honda_match_model');
          if (!model) {
            return;
          }

          self.trigger('honda_match_model_change');
          self.trigger('honda_match_type_change');
        });
      }

      // generate models

      function generate_models(data) {
        $(opts.modelContainerSelector, this).empty();
        var models = [];
        for (var index = 0; index < data.keys.length; index++) {
          var model_key = data.keys[index];
          var element = opts.create_model.apply(this, [model_key, data[model_key]]);
          // if no element is created
          if (element == null) {
            continue;
          }
          element.data('honda_match_model', data[model_key]);

          // handle events
          element.bind('honda_match_type_change', function(event) {
            var type = self.data('honda_match').type;
            var model = $(this).data('honda_match_model');

            if (model[type]) {
              switch (type) {
              case 'msrp':
                $('.honda_match_model_content', this).text(model[type].base_trim).format({ format: format(type) });
                break;
              case 'lease':
              case 'finance':
                $('.honda_match_model_content', this).text(model[type].base_trim).format({ format: format(type) });
                break;
              case 'seats':
                $('.honda_match_model_content', this).text(model[type]).format({ format: format(type) });
                break;
              case 'fuel_efficiency':
                $('.honda_match_model_content', this).text(model[type]).format({ format: format(type) });
                break;
              }
              if (opts['format'] && opts['format']['prefix'] && opts['format']['prefix'][type]) {
                $('.honda_match_model_content', this).prepend(opts.format.prefix[type]);
              }
              if (opts['format'] && opts['format']['suffix'] && opts['format']['suffix'][type]) {
                $('.honda_match_model_content', this).append(opts.format.suffix[type]);
              }
            } else {
              $('.honda_match_model_content', this).text('*');
            }
          });

          element.bind('honda_match_model_change', function(event) {
            var data = self.data('honda_match');
            var type = data.type;
            var condition = data.conditions[type];
            var model = $(this).data('honda_match_model');

            var precision;
            try {
              precision = data.options.format.precision[type];
            } catch(e) {
              precision = 1;
            }

            if (model[type] && condition.intersects(model[type], precision)) {
              // remove the disabled class
              $(this).removeClass('honda_match_model_disabled');
            } else {
              // add the disabled class
              $(this).addClass('honda_match_model_disabled');
            }
          });

          // element.click(function(event) {
          // 	if ($(this).hasClass('honda_match_model_disabled')) {
          // 		return false;
          // 	}
          // });
          models.push(element);
        }
        opts.append_models.apply(this, [models]);

        $('.honda_match_model', this).trigger('honda_match_type_change');
        $(opts.sliderSelector, self).trigger('honda_match_type_change');
      }

      // ajax calls are successful

      function did_succeed() {
        // parse data

        var data = self.data('honda_match').data;
        root.data = data;

        // sort key for ranges
        var ranges = { };
        // get sort keys
        for (var index = 0; index < data.sort_keys.length; index++) {
          ranges[data.sort_keys[index].key] = {
            min: 999999,
            max: -1
          };
        }

        // get model information, and generate the max range list
        var models = data.keys;
        for (var index = 0; index < models.length; index++) {
          var model_key = models[index];

          for (var key in ranges) {
            if (data[model_key][key] == null) {
              continue;
            }
            switch (key) {
            case 'msrp':
            case 'lease':
            case 'finance':
              ranges[key].min = Math.min(ranges[key].min, data[model_key][key].base_trim);
              ranges[key].max = Math.max(ranges[key].max, data[model_key][key].top_trim);
              break;
            default:
              ranges[key].min = Math.min(ranges[key].min, data[model_key][key]);
              ranges[key].max = Math.max(ranges[key].max, data[model_key][key]);
              break;
            }
          }
        }

        // generate the Match objects
        var conditions = { };
        for (var key in ranges) {
          // reset the minimum and maximum values to zero if they are not set from the vehicles
          ranges[key].min = ranges[key].min == 999999 ? 0 : ranges[key].min;
          ranges[key].max = ranges[key].max == -1 ? 0 : ranges[key].max;

          conditions[key] = new Condition(ranges[key].min, ranges[key].max);
        }

        self.data('honda_match').conditions = conditions;

        // initialize the ui slider
        if (!opts.simple) {
          var widget = $(opts.sliderSelector, self).slider({
            range: true,
            min: SLIDER_MIN,
            max: SLIDER_MAX,
            values: [SLIDER_MIN, SLIDER_MAX],
            // change: do_change,
            slide: do_slide
          }).bind('honda_match_type_change', function() {
            var data = self.data('honda_match');
            var type = data.type;
            var condition = data.conditions[type];

            if (condition.range().start && condition.range().end) {
              $(this).css('visibility', 'visible');
              var range = condition.to_slider(SLIDER_MIN, SLIDER_MAX);

              $(this).slider('values', 0, range.start);
              $(this).slider('values', 1, range.end);

              range = condition.range();
              opts.handle_set_values.apply(self, [range.start, range.end]);
            } else {
              $(this).css('visibility', 'hidden');
            }
          });
        }


        // mobile safari intialization
        if ($.browser.mobilesafari) {
          var handles = $('a.ui-slider-handle', $(opts.sliderSelector, self));
          $('a.ui-slider-handle', $(opts.sliderSelector, self)).bind('touchstart', function(event) {
            event.preventDefault();


            $(this).data('startX', null);
            $(this).data('startY', null);
            $(this).data('ui', null);

            var e = event.originalEvent;
            var touch = e.touches[0];
            var values = $(opts.sliderSelector, self).slider('option', 'values');

            $(this).data('startX', touch.pageX);
            $(this).data('startY', touch.pageY);

            var base;

            if ($(this).next('a.ui-slider-handle').size() > 0) {
              base = values[0];
            } else {
              base = values[1];
            }

            $(this).data('startBase', base);

          }).bind('touchmove', function(event) {
            event.preventDefault();

            var e = event.originalEvent;
            var touch = e.targetTouches[0],
                startX = $(this).data('startX'),
                startY = $(this).data('startY'),
                base = $(this).data('startBase');

            var curX = touch.pageX - startX,
                curY = touch.pageY - startY;

            // check the x-axis movement
            var width = $(opts.sliderSelector, self).width();

            var values = $(opts.sliderSelector, self).slider('option', 'values'),
                handle = null,
                value = null;

            //					console.log(curX / width)
            var new_value = base + Math.ceil(curX / width * (SLIDER_MAX - SLIDER_MIN));

            // check which slider handle we are at
            // the left handle

            new_value = new_value < SLIDER_MIN ? SLIDER_MIN : new_value;
            new_value = new_value > SLIDER_MAX ? SLIDER_MAX : new_value;


            //					console.log(touch.pageX)
            //					console.log(new_value)
            if ($(this).next('a.ui-slider-handle').size() > 0) {
              new_value = new_value > values[1] - 2 ? values[1] - 2 : new_value;
              values[0] = new_value;
              value = new_value;
              handle = this;
            }

              // the right handle
            else {
              new_value = new_value < values[0] + 2 ? values[0] + 2 : new_value;
              values[1] = new_value;
              value = new_value;
              handle = this;
            }

            $(opts.sliderSelector, self).data('ui', {
              handle: handle,
              value: value,
              values: values
            });
            $(opts.sliderSelector, self).slider('option', 'values', values);

          }).bind('touchend', function(event) {
            event.preventDefault();

            var ev = event.originalEvent;

            var ui = $(opts.sliderSelector, self).data('ui');

            do_slide(event, ui);

            $(this).data('startX', null);
            $(this).data('startY', null);
            $(this).data('ui', null);
          });
        }

        $(opts.sliderSelector, self)
          .bind('slidercomplete', opts.slider_complete)
          .trigger('slidercomplete', [widget]);

        generate_models.apply(self, [data]);
      }

      // error handler

      function did_error() {

      }

      $(o.modelContainerSelector, this).empty();

      function do_refactor(data) {
        var result = $.extend(true, { }, data);
        $.each(data.keys, function(index, key) {
          var item = data[key];
          var year_key = item.keys && item.keys.length ? item.keys[0] : null;
          if (!year_key) return;
          result[key] = item[year_key];
          result[key].year = year_key;
        });
        return result;
      }

      // run ajax call to grab all model information
      $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        global: false,
        success: function(data, status, xhr) {
          // store the data
          self.data('honda_match', {
            data: do_refactor(data),
            type: opts.defaultType,
            options: opts,
            conditions: { }
          });
          $.ajax({
            type: 'GET',
            url: financeUrl.replace('{include_fees}', o.include_fees).replace('{loyalty_offer}', o.loyalty_offer),
            dataType: 'json',
            global: false,
            success: function(data, status, xhr) {
                console.log('match vehicle data loaded', data);
					    var result = do_refactor(data);
						  // store the data
						  result = $.extend(true, self.data('honda_match').data, result);
					    self.data('honda_match').data = result;
              o.set_thresholds(data.lease_threshold, data.financial_threshold);
              did_succeed();
              self.trigger('match_vehicle_load_success', { data: data, status: status, xhr: xhr });
            },
            error: function(data, status, xhr) {
              // $(o.modelContainerSelector, this).find('> *').fadeIn();
              did_error(data, status, xhr);
            }
          });
        },
        error: function(data, status, xhr) {
          did_error(data, status, xhr);
        }
      });
    },

    getModel: function(model_key) {
      return this.data[model_key];
    }
  });

})(jQuery);
