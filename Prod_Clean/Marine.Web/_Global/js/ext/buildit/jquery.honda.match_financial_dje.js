﻿/* $Id$ */

; (function ($) {
  var env = global_get('environment');
  var _defaults = {
    url: '/api/FinancialCalculator/calculator',
    msrp_url: '/api/FinancialCalculator/calculator/{lang}/{province_key}/{year}/{model_key}/{trim_level_key}/{transmission_key}/{include_fees}{loyalty_offer}',
    lang: 'en',
    province_key: 'ON',
    include_fees: false,

    lease_threshold: 7,
    financial_threshold: 7,
    loyalty_offer: '',
    add_cash_purchase: function (data) {
    },
    add_msrp: function (data) {
    },
    add_lease: function (data) {
    },
    add_finance: function (data) {
    },
    set_include_fees: function (include_fees) {
    },
    set_accessories_pricing: function (data) {
    },
    _last: null
  };

  $.widget('honda.match_financial', {
    widgetEventPrefix: 'match_financial_',
    options: $.extend(true, {}, _defaults, (function () {
      try {
        return global_get('honda.match_financial.defaults');
      }
      catch (e) {
        return {};
      }
    })()),
    // initializer
    _create: function () {
      this._requestData = null;
      this._isRequestDataAvailable = false;
    },
    loadFinancial: function (selected_keys, requestData, includeFees, lang) {
      var o = this.options;

      this._trigger('before_load', null, {});

      requestData.selected_keys = selected_keys;

      if (requestData) {
        this._requestData = requestData;
        this._isRequestDataAvailable = true;
      }

      if (typeof includeFees != undefined) {
        o.include_fees = includeFees;
      }

      if (lang) {
        o.lang = lang;
      }

      this.reloadData();
    },
    loadVehicle: function (model_year, model_key, trim_level_key, transmission_key, include_fees, exterior_key, callback) {
      var self = this,
			o = this.options;

      this._trigger('before_load', null, {});

      var url = o.msrp_url.replace('{lang}', o.lang)
	    .replace('{year}', model_year)
			.replace('{province_key}', o.province_key)
			.replace('{model_key}', model_key)
			.replace('{trim_level_key}', trim_level_key)
			.replace('{transmission_key}', transmission_key)
      .replace('{include_fees}', include_fees ? 'true' : 'false')
          .replace('{loyalty_offer}', o.loyalty_offer);
      url = url.replace('{exterior_key}', exterior_key || '');
      // ajax call
      $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        global: false,
        cache: false,
        success: function (data, status, xhr) {
            console.log('match financial data loaded', data);

          o.add_finance.call(self, data.finance, data);
          o.add_lease.call(self, data.lease, data);
          o.add_msrp.call(self, data.msrp);
          o.add_cash_purchase.call(self, data.cash_purchase, data);
          o.set_include_fees.call(self, data.include_fees, data);          
          o.set_accessories_pricing.call(self, data);
          self._success(data, status, xhr);
	        
		  if (typeof callback != 'undefined') 
		    callback();
        },
        error: function (xhr, err) {
          self._error(xhr, err);
        }
      });
    },
    updateIncludeFees: function (data, value) {
      if (typeof data['include_fees'] != 'object') {
        throw new Error('Object does not meet precondition');
      }

      data.include_fees.value = value;
    }
    // reload data
	, reloadData: function () {
	  var self = this,
			o = this.options;

	  // pre-condition
	  if (!this._isRequestDataAvailable) {
	    throw new Error('Request data is not set.');
	  }

	  var url = o.url + '?include_fees=' + (o.include_fees ? 'true' : 'false') + '&Lang=' + o.lang
            , exterior_key = self._requestData.selected_keys.exterior_key || null;

	  $.ajax({
	    url: url,
	    type: 'POST',
	    data: {
	      Lang: o.lang,
	      Data: $.toJSON(self._requestData)
                , exterior_key: exterior_key
	    },
	    dataType: 'json',
	    cache: false,
	    success: function (data, status, xhr) {
	      console.log('match financial data loaded', data);
          //data = $.parseJSON('{ "keys": [ "finance", "lease", "accessories", "msrp", "cash_purchase", "include_fees", "offers", "selected_keys", "exceptions" ], "finance": { "keys": [ "monthly", "biweekly", "weekly" ], "monthly": { "Key": "monthly", "value": 514.22, "fees": 39.92, "total": 554.14, "down_payment": 0, "term_options": [ 24, 30, 36, 42, 48, 54, 60, 72, 84 ], "term_options_default": 60, "est_balance": 30163.15, "apr": 5.9, "informational_apr": 5.9, "freight_pdi": 1995, "levies": { "keys": [ "Federal Air Conditioner Fee", "Tire Duty", "OMVIC", "Registering Agent Fee", "PPSA Registration Fee", "Environmental Fees" ], "Federal Air Conditioner Fee": { "Key": "Federal Air Conditioner Fee", "value": 100 }, "Tire Duty": { "Key": "Tire Duty", "value": 27.15 }, "OMVIC": { "Key": "OMVIC", "value": 5 }, "Registering Agent Fee": { "Key": "Registering Agent Fee", "value": 5 }, "PPSA Registration Fee": { "Key": "PPSA Registration Fee", "value": 40 }, "Environmental Fees": { "Key": "Environmental Fees", "value": 1 } }, "est_annual_km": 0, "est_monthly_payment": 554.14, "amount_on_delivery": 0, "total_cost_of_borrowing": 3008.95, "no_of_biweekly_payments": 130, "rate_period_start": "", "rate_period_end": "", "offer": { "value": 2, "unit": "percent", "yousave": 27.59 } }, "biweekly": { "Key": "biweekly", "value": 236.79, "fees": 18.38, "total": 255.17, "down_payment": 0, "term_options": [ 24, 30, 36, 42, 48, 54, 60, 72, 84 ], "term_options_default": 60, "est_balance": 30163.15, "apr": 5.9, "informational_apr": 5.9, "freight_pdi": 1995, "levies": { "keys": [ "Federal Air Conditioner Fee", "Tire Duty", "OMVIC", "Registering Agent Fee", "PPSA Registration Fee", "Environmental Fees" ], "Federal Air Conditioner Fee": { "Key": "Federal Air Conditioner Fee", "value": 100 }, "Tire Duty": { "Key": "Tire Duty", "value": 27.15 }, "OMVIC": { "Key": "OMVIC", "value": 5 }, "Registering Agent Fee": { "Key": "Registering Agent Fee", "value": 5 }, "PPSA Registration Fee": { "Key": "PPSA Registration Fee", "value": 40 }, "Environmental Fees": { "Key": "Environmental Fees", "value": 1 } }, "est_annual_km": 0, "est_monthly_payment": 255.17, "amount_on_delivery": 0, "total_cost_of_borrowing": 3008.95, "no_of_biweekly_payments": 130, "rate_period_start": "", "rate_period_end": "", "offer": { "value": 2, "unit": "percent", "yousave": 12.41 } }, "weekly": { "Key": "weekly", "value": 236.79, "fees": 18.38, "total": 255.17, "down_payment": 0, "term_options": [ 24, 30, 36, 42, 48, 54, 60, 72, 84 ], "term_options_default": 60, "est_balance": 30163.15, "apr": 5.9, "informational_apr": 5.9, "freight_pdi": 1995, "levies": { "keys": [ "Federal Air Conditioner Fee", "Tire Duty", "OMVIC", "Registering Agent Fee", "PPSA Registration Fee", "Environmental Fees" ], "Federal Air Conditioner Fee": { "Key": "Federal Air Conditioner Fee", "value": 100 }, "Tire Duty": { "Key": "Tire Duty", "value": 27.15 }, "OMVIC": { "Key": "OMVIC", "value": 5 }, "Registering Agent Fee": { "Key": "Registering Agent Fee", "value": 5 }, "PPSA Registration Fee": { "Key": "PPSA Registration Fee", "value": 40 }, "Environmental Fees": { "Key": "Environmental Fees", "value": 1 } }, "est_annual_km": 0, "est_monthly_payment": 255.17, "amount_on_delivery": 0, "total_cost_of_borrowing": 3008.95, "no_of_biweekly_payments": 130, "rate_period_start": "", "rate_period_end": "", "offer": { "value": 2, "unit": "percent", "yousave": 12.41 } } }, "lease": { "keys": [ "monthly", "biweekly", "weekly" ], "monthly": { "Key": "monthly", "value": 368.77, "fees": 49.26, "total": 416.2, "down_payment": 0, "term_options": [ 36, 48, 54, 60 ], "term_options_default": 48, "est_balance": 30118.15, "apr": 5.49, "informational_apr": 5.49, "freight_pdi": 1995, "levies": { "keys": [ "Federal Air Conditioner Fee", "Tire Duty", "OMVIC", "Registering Agent Fee", "PPSA Registration Fee", "Environmental Fees" ], "Federal Air Conditioner Fee": { "Key": "Federal Air Conditioner Fee", "value": 100 }, "Tire Duty": { "Key": "Tire Duty", "value": 27.15 }, "OMVIC": { "Key": "OMVIC", "value": 5 }, "Registering Agent Fee": { "Key": "Registering Agent Fee", "value": 5 }, "PPSA Registration Fee": { "Key": "PPSA Registration Fee", "value": 32 }, "Environmental Fees": { "Key": "Environmental Fees", "value": 1 } }, "est_annual_km": 24000, "additional_annual_km": 0, "additional_cost_per_km": 0.1, "additional_km_term_exclude": [ 25 ], "annual_km_allowances": [ { "est_annual_km": 12000, "allow_additional_km": false, "additional_cost_per_km": 0.1 }, { "est_annual_km": 16000, "allow_additional_km": false, "additional_cost_per_km": 0.1 }, { "est_annual_km": 20000, "allow_additional_km": false, "additional_cost_per_km": 0.1 }, { "est_annual_km": 24000, "allow_additional_km": true, "additional_cost_per_km": 0.1 }, { "est_annual_km": 10000, "allow_additional_km": false, "additional_cost_per_km": 0.1 } ], "buyout": 13155.3, "est_monthly_payment": 416.2, "amount_on_delivery": 453.2, "total_cost_of_borrowing": 3030.11, "no_of_biweekly_payments": 104, "rate_period_start": "", "rate_period_end": "", "offer": { "value": 2, "unit": "percent", "yousave": 36.34 } }, "biweekly": { "Key": "biweekly", "value": 170.35, "fees": 22.75, "total": 192.24, "down_payment": 0, "term_options": [ 36, 48, 54, 60 ], "term_options_default": 48, "est_balance": 30118.15, "apr": 5.49, "informational_apr": 5.49, "freight_pdi": 1995, "levies": { "keys": [ "Federal Air Conditioner Fee", "Tire Duty", "OMVIC", "Registering Agent Fee", "PPSA Registration Fee", "Environmental Fees" ], "Federal Air Conditioner Fee": { "Key": "Federal Air Conditioner Fee", "value": 100 }, "Tire Duty": { "Key": "Tire Duty", "value": 27.15 }, "OMVIC": { "Key": "OMVIC", "value": 5 }, "Registering Agent Fee": { "Key": "Registering Agent Fee", "value": 5 }, "PPSA Registration Fee": { "Key": "PPSA Registration Fee", "value": 32 }, "Environmental Fees": { "Key": "Environmental Fees", "value": 1 } }, "est_annual_km": 24000, "additional_annual_km": 0, "additional_cost_per_km": 0.1, "additional_km_term_exclude": [ 25 ], "annual_km_allowances": [ { "est_annual_km": 12000, "allow_additional_km": false, "additional_cost_per_km": 0.1 }, { "est_annual_km": 16000, "allow_additional_km": false, "additional_cost_per_km": 0.1 }, { "est_annual_km": 20000, "allow_additional_km": false, "additional_cost_per_km": 0.1 }, { "est_annual_km": 24000, "allow_additional_km": true, "additional_cost_per_km": 0.1 }, { "est_annual_km": 10000, "allow_additional_km": false, "additional_cost_per_km": 0.1 } ], "buyout": 13155.3, "est_monthly_payment": 192.24, "amount_on_delivery": 229.24, "total_cost_of_borrowing": 3030.11, "no_of_biweekly_payments": 104, "rate_period_start": "", "rate_period_end": "", "offer": { "value": 2, "unit": "percent", "yousave": 16.92 } }, "weekly": { "Key": "weekly", "value": 170.35, "fees": 22.75, "total": 192.24, "down_payment": 0, "term_options": [ 36, 48, 54, 60 ], "term_options_default": 48, "est_balance": 30118.15, "apr": 5.49, "informational_apr": 5.49, "freight_pdi": 1995, "levies": { "keys": [ "Federal Air Conditioner Fee", "Tire Duty", "OMVIC", "Registering Agent Fee", "PPSA Registration Fee", "Environmental Fees" ], "Federal Air Conditioner Fee": { "Key": "Federal Air Conditioner Fee", "value": 100 }, "Tire Duty": { "Key": "Tire Duty", "value": 27.15 }, "OMVIC": { "Key": "OMVIC", "value": 5 }, "Registering Agent Fee": { "Key": "Registering Agent Fee", "value": 5 }, "PPSA Registration Fee": { "Key": "PPSA Registration Fee", "value": 32 }, "Environmental Fees": { "Key": "Environmental Fees", "value": 1 } }, "est_annual_km": 24000, "additional_annual_km": 0, "additional_cost_per_km": 0.1, "additional_km_term_exclude": [ 25 ], "annual_km_allowances": [ { "est_annual_km": 12000, "allow_additional_km": false, "additional_cost_per_km": 0.1 }, { "est_annual_km": 16000, "allow_additional_km": false, "additional_cost_per_km": 0.1 }, { "est_annual_km": 20000, "allow_additional_km": false, "additional_cost_per_km": 0.1 }, { "est_annual_km": 24000, "allow_additional_km": true, "additional_cost_per_km": 0.1 }, { "est_annual_km": 10000, "allow_additional_km": false, "additional_cost_per_km": 0.1 } ], "buyout": 13155.3, "est_monthly_payment": 192.24, "amount_on_delivery": 229.24, "total_cost_of_borrowing": 3030.11, "no_of_biweekly_payments": 104, "rate_period_start": "", "rate_period_end": "", "offer": { "value": 2, "unit": "percent", "yousave": 16.92 } } }, "accessories": { "keys": [ "62017_aero_kit_package", "62016_ilx_fundamental_package" ], "62017_aero_kit_package": { "price": 2744.27, "residual": 2720, "name": "Aero Kit Package", "finance_monthly": 52.93, "lease_monthly": 39.54, "finance_biweekly": 24.4, "lease_biweekly": 18.28, "finance_weekly": 24.4, "lease_weekly": 18.28 }, "62016_ilx_fundamental_package": { "price": 193.82, "residual": null, "name": "ILX Fundamental Package", "finance_monthly": 3.74, "lease_monthly": 4.49, "finance_biweekly": 1.72, "lease_biweekly": 2.07, "finance_weekly": 1.72, "lease_weekly": 2.07 } }, "msrp": { "value": 27990, "fees": 2173.15, "total": 30163.15, "include_fees": false, "MSRP_Include_Freight_PDI_fees": false }, "cash_purchase": { "value": 27990, "fees": 2128.15, "total": 30118.15 }, "include_fees": { "value": false }, "offers": null, "selected_keys": { "transmission_key": "10049-Automatic", "model_key": "ilx", "model_year": "2014", "trim_level_key": "ilx_10082", "exterior_key": null, "province_key": "ON", "term_options_lease": 48, "est_annual_km_lease": 24000, "term_options_finance": 60, "accessories": null, "warranty": null, "payment_type": "weekly" }, "exceptions": null }');

	      o.add_finance.call(self, data.finance, data);
	      o.add_lease.call(self, data.lease, data);
	      o.add_msrp.call(self, data.msrp);
	      o.add_cash_purchase.call(self, data.cash_purchase, data);
	      o.set_include_fees.call(self, data.include_fees, data);          
	      o.set_accessories_pricing.call(self, data);
	      self._success(data, status, xhr);

	      // if exception is thrown, call exception
	      if (data.exceptions && data.exceptions.keys.length > 0) {
	        self._exception(data.exceptions);
	      }
	    },
	    error: function (xhr, err) {
	      self._error(xhr, err);
	    }
	  });
	},
    _error: function (xhr, err) {
      this._trigger('load_error', null, { xhr: xhr, err: err });
    },
    _success: function (data, status, xhr) {
      this._trigger('load_success', null, { data: data, status: status, xhr: xhr });
    },
    _exception: function (exceptions) {
      this._trigger('load_exception', null, { exceptions: exceptions });
    }
  });

})(jQuery);