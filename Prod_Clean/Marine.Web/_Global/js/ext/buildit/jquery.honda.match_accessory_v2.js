﻿/* $Id$ */

; (function ($) {

    var in_degraded_browser = $.browser.msie && $.browser.version.match(/^(6|7)\./);
    var DEGRADED_LEVEL = in_degraded_browser ? 10 : 100;

    var _defaults = {        
        url: '/api/buildit/accessories/{province}/{modelyear}/{modekkey}/{transmission}/{colorkey}/{lang}',
        default_thumbnail: '',
        vehicle: '',
        transmission: '',
        current_lang: '',
        province_key: '',

        all_accessories: {},
        selected_accessories: {},
        disabled_accessories: {},
        accessory_relationships: {},
        reverse_relationships: {},

        add_category: function (category) { },
        add_accessory: function (accessory) { },

        select_accessory: function (accessory_key) { },
        deselect_accessory: function (accessory_key) { },

        enable_accessory: function (accessory_key) { },
        disable_accessory: function (accessory_key) { },

        _last: null
    };

    // local functions to avoid IE6 recursive function stack overflow issue
    var local_selectAccessory = function (event, accessory_key, with_includes, recur) {
        if (typeof recur == 'undefined') {
            recur = 0;
        }

        if (recur > DEGRADED_LEVEL) {
            return;
        }

        var self = this,
			o = this.options;

        try {
            if (typeof with_includes == 'undefined') {
                with_includes = true;
            }
            this._will_select_accessory(event, accessory_key);
            if (this._should_select_accessory(accessory_key)) {
                // disable excluded accesssories
                var r = o.accessory_relationships;
                var accessory_rkey = accessory_key + ':on';
                if (typeof r[accessory_rkey] != 'undefined') {
                    $.each(r[accessory_rkey], function (target_rkey, target_key) {
                        if (accessory_rkey != target_rkey && target_rkey.match(/:off$/) && r[accessory_rkey][target_rkey]) {
                            local_disableAccessory.call(self, event, target_key, recur + 1);
                        }
                    });
                }
                o.selected_accessories[accessory_key] = o.selected_accessories[accessory_key] ? o.selected_accessories[accessory_key] + 1 : 1;
                o.select_accessory.call(self, accessory_key, o.selected_accessories[accessory_key]);
                // include sub accessories
                if (with_includes) {
                    local_select_sub_accessories.call(self, event, accessory_key, recur + 1);
                }
                this._did_select_accessory(event, accessory_key);
            }
        }
        finally {
        }

    };

    var local_deselectAccessory = function (event, accessory_key, with_includes, release_one, direction, recur) {

        if (typeof recur == 'undefined') {
            recur = 0;
        }

        if (recur > DEGRADED_LEVEL) {
            return;
        }

        var self = this,
			o = this.options;

        try {
            if (typeof release_one == 'undefined') {
                release_one = false;
            }

            if (typeof with_includes == 'undefined') {
                with_includes = true;
            }

            if (typeof direction == 'undefined') {
                direction = 'up';
            }

            self._will_deselect_accessory(event, accessory_key);

            while (self._should_deselect_accessory(accessory_key)) {
                try {
                    // deselect all sub accessories if not releasing one
                    if (!release_one) {
                        local_deselect_sub_accessories.call(self, event, accessory_key, with_includes, 'down', recur);
                    }

                    // enable accessories
                    // disable excluded accesssories
                    var r = o.accessory_relationships;
                    accessory_rkey = accessory_key + ':on';
                    if (typeof r[accessory_rkey] != 'undefined') {
                        $.each(r[accessory_rkey], function (target_rkey, target_key) {
                            if (accessory_rkey != target_rkey && target_rkey.match(/:off$/) && r[accessory_rkey][target_rkey]) {
                                local_enableAccessory.call(self, event, target_key, recur + 1);
                            }
                        });
                    }

                    // deselect this accessory
                    o.selected_accessories[accessory_key]--;
                    o.deselect_accessory.call(self, accessory_key, o.selected_accessories[accessory_key]);
                    self._did_deselect_accessory(event, accessory_key);
                    if (release_one) {
                        break;
                    }
                }
                finally {
                    r = null;
                }
            }

            // deselect parents (one step)
            var v = o.reverse_relationships;
            var accessory_rkey = accessory_key + ':on';
            if (direction == 'up' && typeof v[accessory_rkey] != 'undefined') {
                $.each(v[accessory_rkey], function (parent_rkey, parent_key) {
                    if (accessory_rkey != parent_rkey && parent_rkey.match(/:on$/) && v[accessory_rkey][parent_rkey]) {
                        local_deselectAccessory.call(self, event, parent_key, false, true, recur + 1);
                    }
                });
            }

        }
        finally {
            v = null;
            accessory_rkey = null;
        }

    };

    var local_enableAccessory = function (event, accessory_key, recur) {
        if (recur > DEGRADED_LEVEL) {
            return;
        }

        var self = this,
			o = this.options;

        try {
            if (typeof o.disabled_accessories[accessory_key] == 'undefined' &&
					(!o.disabled_accessories[accessory_key] || o.selected_accessories[accessory_key])) {
                return;
            }

            self._will_enable_accessory(event, accessory_key);

            // disable this accessory
            o.disabled_accessories[accessory_key]--;
            o.enable_accessory.call(self, accessory_key, o.disabled_accessories[accessory_key]);
            self._did_enable_accessory(event, accessory_key);

            // enable parents (one step)
            var v = o.reverse_relationships;
            var accessory_rkey = accessory_key + ':on';
            if (typeof v[accessory_rkey] != 'undefined') {
                $.each(v[accessory_rkey], function (parent_rkey, parent_key) {
                    if (accessory_rkey != parent_rkey && parent_rkey.match(/:on$/) && v[accessory_rkey][parent_rkey]) {
                        local_enableAccessory.call(self, event, parent_key, recur + 1);
                    }
                });
            }
        }
        finally {
            v = null;
            accessory_rkey = null;
        }

    };

    var local_disableAccessory = function (event, accessory_key, recur) {
        if (recur > DEGRADED_LEVEL) {
            return;
        }

        var self = this,
			o = this.options;

        try {
            self._will_disable_accessory(event, accessory_key);

            self._allow_disability(accessory_key);

            // disable this accessory
            o.disabled_accessories[accessory_key] = o.disabled_accessories[accessory_key] ? o.disabled_accessories[accessory_key] + 1 : 1;
            o.disable_accessory.call(self, accessory_key, o.disabled_accessories[accessory_key]);
            self._did_disable_accessory(event, accessory_key);

            // disable parents (one step)
            var v = o.reverse_relationships;
            var accessory_rkey = accessory_key + ':on';
            if (typeof v[accessory_rkey] != 'undefined') {
                $.each(v[accessory_rkey], function (parent_rkey, parent_key) {
                    if (accessory_rkey != parent_rkey && parent_rkey.match(/:on$/) && v[accessory_rkey][parent_rkey]) {
                        local_disableAccessory.call(self, event, parent_key, recur + 1);
                    }
                });
            }
        }
        finally {
            v = null;
            accessory_rkey = null;
        }
    };

    var local_select_sub_accessories = function (event, accessory_key, recur) {
        if (recur > DEGRADED_LEVEL) {
            return;
        }

        var self = this,
			o = self.options,
			r = o.accessory_relationships;

        var first = accessory_key + ':on';

        try {
            // iterate in the transitive closure
            if (!r[first]) {
                return;
            }
            $.each(r[first], function (second, target) {
                // select the accessory if it's not the reflexive relationship
                if (first != second && second.match(/:on$/)) {
                    local_selectAccessory.call(self, event, r[first][second], true, recur + 1);
                }
            });
        }
        finally {
            r = null;
            first = null;
        }

    };

    var local_deselect_sub_accessories = function (event, accessory_key, with_includes, direction, recur) {
        if (recur > DEGRADED_LEVEL) {
            return;
        }

        var self = this,
			o = self.options,
			r = o.accessory_relationships;

        try {
            var first = accessory_key + ':on';
            // iterate in the transitive closure
            if (!r[first]) {
                return;
            }
            $.each(r[first], function (second, target) {
                // select the accessory if it's not the reflexive relationship
                if (first != second) {
                    local_deselectAccessory.call(self, event, r[first][second], with_includes, true, direction, recur + 1);
                }
            });
        }
        finally {
            first = null;
        }
    };



    $.widget('honda.match_accessory', {
        widgetEventPrefix: 'match_accessory_',
        options: $.extend(true, {}, _defaults, (function () {
            try {
                return global_get('honda.match_accessory.defaults');
            }
            catch (e) {
                return {};
            }
        })()),
        _resetData: function () {
            var o = this.options;
            o.all_accessories = {};
            o.selected_accessories = {};
            o.disabled_accessories = {};
            o.accessory_relationships = {};
            o.reverse_relationships = {};

            this._data = null;
        },
        // initializer
        _create: function () {
            this._resetData();
        }
    , _ensureOptions: function () {
        var o = this.options
            , w = this.widget();

        o.url = (w.data('hondaMatchAccessoryUrl') || o.url).replace('{0}',o.province_key).replace('{province}',o.province_key);        
    }
        // load trim and transmission data from the data source
	, loadVehicleAndTransmission: function (vehicle, transmission) {
	    var self = this,
			o = this.options;

	    if (vehicle) {
	        o.vehicle = vehicle;
	    }

	    if (transmission) {
	        o.transmission = transmission;
	    }

	    this.reloadData();
	},

        selectAccessory: local_selectAccessory,
        deselectAccessory: local_deselectAccessory,
        enableAccessory: local_enableAccessory,
        disableAccessory: local_disableAccessory,

        _allow_disability: function (accessory_key) {
            var self = this,
			o = this.options;

            self.deselectAccessory(null, accessory_key);
        },
        hasRelation: function (first, second) {
            var r = this.options.accessory_relationships;
            return r[first] && r[first][second];
        },
        hasReverseRelation: function (first, second) {
            var v = this.options.reverse_relationships;
            return v[second] && v[second][first];
        },
        // check exclusion rules here
        _should_select_accessory: function (accessory_key) {
            var self = this,
			o = this.options;

            if (typeof (o.all_accessories[accessory_key]) == 'undefined' || o.disabled_accessories[accessory_key]) {
                throw new Error('accessory cannot be selected: ' + accessory_key);
            }

            return true;
        },
        _should_deselect_accessory: function (accessory_key) {
            var self = this,
			o = this.options;

            if (typeof (o.all_accessories[accessory_key]) == 'undefined') {
                return false;
                // throw new Error('accessory does not exist: ' + accessory_key);
            }
            return typeof (o.selected_accessories[accessory_key]) != 'undefined' && o.selected_accessories[accessory_key] > 0;
        },
        _select_sub_accessories: local_select_sub_accessories,
        _deselect_sub_accessories: local_deselect_sub_accessories,
        // calculate relationship
        _build_simple_relationship: function (accessory_key) {
            var self = this,
			o = this.options;

            var accessory = o.all_accessories[accessory_key];
            var r = o.accessory_relationships,
			v = o.reverse_relationships;

            if (!accessory) {
                throw new Error('accessory does not exist: ' + accessory_key);
            }

            // build reflexive relationship
            var first = accessory_key + ':off';
            if (typeof r[first] == 'undefined') {
                r[first] = {};
            }
            if (typeof v[first] == 'undefined') {
                v[first] = {};
            }
            r[first][first] = accessory_key;
            v[first][first] = accessory_key;
            first = accessory_key + ':on';
            if (typeof r[first] == 'undefined') {
                r[first] = {};
            }
            if (typeof v[first] == 'undefined') {
                v[first] = {};
            }
            r[first][first] = accessory_key;
            v[first][first] = accessory_key;

            // find all includes
            var includes = accessory.includes;
            if (includes) {
                // category
                $.each(includes.keys, function (index, category_key) {
                    var target = includes[category_key];
                    $.each(target, function (index, target_accessory) {
                        // we need to make sure the target accessory exists
                        if (!o.all_accessories[target_accessory]) {
                            throw new Error('target accessory does not exist: ' + target_accessory);
                        }
                        // build the relationship
                        // second
                        var second = target_accessory + ':on';
                        r[first][second] = target_accessory;

                        // build the reverse relationship
                        if (typeof v[second] == 'undefined') {
                            v[second] = {};
                        }
                        v[second][first] = accessory_key;
                    });
                });
            }

            // find all excludes
            var excludes = accessory.excludes;
            if (excludes) {
                // category
                $.each(excludes.keys, function (index, category_key) {
                    var target = excludes[category_key];
                    $.each(target, function (index, target_accessory) {
                        // we need to make sure the target accessory exists
                        if (!o.all_accessories[target_accessory]) {
                            throw new Error('target accessory does not exist: ' + target_accessory);
                        }
                        // build the relationship
                        // second
                        var second = target_accessory + ':off';
                        r[first][second] = target_accessory;

                        // build the reverse relationship
                        if (typeof v[second] == 'undefined') {
                            v[second] = {};
                        }
                        v[second][first] = accessory_key;
                    });
                });
            }
        },
        _build_transitive_relationships: function () {
            var self = this,
			o = this.options,
			r = o.accessory_relationships,
			v = o.reverse_relationships,
			expandable = true;

            while (expandable) {
                // make it unexpandable
                expandable = false;
                var n = $.extend(true, {}, r);
                var nv = $.extend(true, {}, v);
                $.each(r, function (accessory_key, accessory) {
                    $.each(r[accessory_key], function (target_key) {
                        $.each(r[target_key], function (sub_target_key) {
                            if (!r[accessory_key][sub_target_key]) {
                                n[accessory_key][sub_target_key] = sub_target_key;
                                if (typeof nv[sub_target_key] == 'undefined') {
                                    nv[sub_target_key] = {};
                                }
                                nv[sub_target_key][accessory_key] = accessory_key;
                                expandable = true;
                            }
                        });
                    });
                });
                r = n;
                v = nv;
            }

            o.accessory_relationships = r;
            o.reverse_relationships = v;

        },
        // reload data
        reloadData: function () {
            var self = this,
			o = this.options;

            self._ensureOptions();
            $.ajax({
                
                url: o.url.replace('{vehicle}', o.vehicle).replace('{transmission}', o.transmission).replace('{lang}', o.current_lang).replace('{province}', o.province_key),
                type: 'GET',
                dataType: 'json',
                cache: false,
                global: false,
                success: function (data, status, xhr) {
                    // clear all accessories and selected accessories data
                    self._resetData();
                    self._data = data;

                    console.log('match accessory data load', data);

                    // guard
                    if (data) {

                        // iterate all trims
                        $.each(data.keys, function (index, category_key) {
                            var category = data[category_key];

                            self._will_add_category(null, {
                                category_key: category_key,
                                category: category,
                                data: self._data
                            });

                            o.add_category.call(self, {
                                category_key: category_key,
                                category: category,
                                data: self._data
                            });

                            if (category) {
                                // iterate all transmissions in a trim
                                $.each(category.items.keys, function (index, accessory_key) {
                                    var accessory = category.items[accessory_key];

                                    // mark down all accessories
                                    o.all_accessories[accessory_key] = accessory;

                                    self._will_add_accessory(null, {
                                        accessory_key: accessory_key,
                                        accessory: accessory,
                                        category_key: category_key,
                                        data: self._data
                                    });
                                    o.add_accessory.call(self, {
                                        accessory_key: accessory_key,
                                        accessory: accessory,
                                        category_key: category_key,
                                        data: self._data
                                    });
                                    self._did_add_accessory(null, {
                                        accessory_key: accessory_key,
                                        accessory: accessory,
                                        category_key: category_key,
                                        data: self._data
                                    });
                                });
                            }

                            self._did_add_category(null, {
                                category_key: category_key,
                                category: category,
                                data: self._data
                            });
                        });

                        // build relationship graph
                        $.each(o.all_accessories, function (accessory_key, accessory) {
                            self._build_simple_relationship(accessory_key);
                        });

                        self._build_transitive_relationships();

                    }
                    self._success(data, status, xhr);
                },
                error: function (xhr, err) {
                    self._error(xhr, err);
                }
            });
        },
        // local function to build accessory thumbnail's path
        build_accessory_thumbnail_path: function (accessory_key, data, opt) {
            var o = this.options;
            var exterior_path = '', interior_path = '';
            var thumbnail = opt.thumbnail;

            if (!thumbnail) {
                return o.default_thumbnail;
            }

            if (data.exterior && accessory_key in data.exterior ||
				data.packages && accessory_key in data.packages) {
                exterior_path = opt.trim_level_key + '/' + opt.exterior_key + '/';
            }
            else {
                if (data.interior && accessory_key in data.interior) {
                    interior_path = 'interior/';
                }
                else if (data.entertainment && accessory_key in data.entertainment) {
                    interior_path = 'entertainment/';
                }
            }
            return global_get('build_it.assets_path') + 'accessories/' +
				opt.model_key + '/' +
				exterior_path +
				opt.transmission_key + '/' +
				interior_path +
				thumbnail;
        },
        build_accessory_thumbnail_path: function(accessory_key, data, opt) {
            if (opt.thumbnail) {
                return '/assets/accessories/' + opt.model_year + '/' + opt.thumbnail;
            }
            else {
                return global_get('build_it.accessory.default_image');
            }
        },
	    // filter data
	    filterAccessories: function(selected) {
		    var data = $.extend(true, {}, this._data);

		    if (selected == null) {
			    return null;
		    }
		    if (selected.keys == 0) {
			    return null;
		    }
		    // first round, remove those accessories that are not selected
		    $.each(this._data.keys, function(index, key) {
			    if (data[key]) {
			        var items = data[key].items;
				    var keys_to_delete = [];
					//collect accessories kyes to remove (not selected accessories)
					$.each(items.keys, function(index2, acc_key) {
						if (selected.keys.indexOf(acc_key) == -1) {
							keys_to_delete.push(acc_key)
						}
					});

					//if keys will be removed in a previous loop then it will glitch and won't remove all accessories
					$.each(keys_to_delete, function(index3, acc_key) {
						items.keys.remove(items.keys.indexOf(acc_key));
						delete items[acc_key];
					});
			    }
		    });

		    // second round, converts category to null if empty
		    $.each(this._data.keys, function(index, key) {
			    if (data[key] && (data[key].items == null || data[key].items.keys.length == 0)) {
    				data.keys.remove(data.keys.indexOf(key));
			        delete data[key];
			    }
		    });

		    return data;
	    },
        // events
        _will_add_category: function (event, ui) {
            this._trigger('will_add_category', event, ui);
        },
        _did_add_category: function (event, ui) {
            this._trigger('did_add_category', event, ui);
        },
        _will_add_accessory: function (event, ui) {
            this._trigger('will_add_accessory', event, ui);
        },
        _did_add_accessory: function (event, ui) {
            this._trigger('did_add_accessory', event, ui);
        },
        _will_select_accessory: function (event, ui) {
            this._trigger('will_select_accessory', event, ui);
        },
        _did_select_accessory: function (event, ui) {
            this._trigger('did_select_accessory', event, ui);
        },
        _will_deselect_accessory: function (event, ui) {
            this._trigger('will_deselect_accessory', event, ui);
        },
        _did_deselect_accessory: function (event, ui) {
            this._trigger('did_deselect_accessory', event, ui);
        },
        _will_disable_accessory: function (event, ui) {
            this._trigger('will_disable_accessory', event, ui);
        },
        _did_disable_accessory: function (event, ui) {
            this._trigger('did_disable_accessory', event, ui);
        },
        _will_enable_accessory: function (event, ui) {
            this._trigger('will_enable_accessory', event, ui);
        },
        _did_enable_accessory: function (event, ui) {
            this._trigger('did_enable_accessory', event, ui);
        },
        _error: function (xhr, err) {
            this._trigger('load_error', null, { xhr: xhr, err: err });
        },
        _success: function (data, status, xhr) {
            this._trigger('load_success', null, { data: data, status: status, xhr: xhr });
        }
    });

})(jQuery);
