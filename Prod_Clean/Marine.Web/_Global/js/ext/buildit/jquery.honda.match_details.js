/* $Id$ */

;(function($){

var _defaults = {
	url: '/'+(global_get('lang') == 'en' ? 'buildyoursxs' : 'construisezvotresxs')+'/{category_key}/{model_key}/{model_year_key}/{model_year_key}/{province}/{include_fees}'
    , contentSelector: false
    , containerSelector: false
    , categoryKey: null
    , modelKey: null
    , modelYearKey: 'NONE'
    , province: null
    , include_fees: false
};

$.widget('honda.match_details', {
	widgetEventPrefix: 'match_details_'
	, options: $.extend(true, {}, _defaults, (function(){
		try {
			return global_get('honda.match_match_details_.defaults');
		}
		catch(e) {
			return {};
		}
	})())
    , _create: function() {
        // store the default options
        this.defaults = $.extend(true, {}, this.options);
        this._resetData();
    }
    , _resetData: function() {
        this.options = $.extend(true, {}, this.defaults);
    }
    , _ensureOptions: function() {
        var o = this.options
            , w = this.widget();

        o.url = w.data('honda-match-details-url') || o.url;
        o.contentSelector = w.data('honda-match-details-content-selector') || o.contentSelector;
        o.containerSelector = w.data('honda-match-details-container-selector') || o.containerSelector;
        o.modelKey = w.data('honda-match-details-model-key') || o.modelKey;
        o.modelYearKey = w.data('honda-match-details-model-year-key') || o.modelYearKey;
    }
    , _replaceUrl: function(url) {
        var o = this.options;

        url = url.replace('{category_key}', o.categoryKey)
                  .replace('{model_key}', o.modelKey)
                  .replace('{model_year_key}', o.modelYearKey || '')
				  .replace('{province}', o.province)
	              .replace('{include_fees}', o.include_fees);
        if (!o.modelYearKey) {
            url = url.replace(/\/$/, '');
        }
        return url;

    }
    , loadModel: function(category_key, model_key, model_year_key, province, include_fees) {
        this._ensureOptions();

        var self = this
            , o = this.options
            , w = this.widget()
            , ctr = o.containerSelector ? $(o.containerSelector, w) : w;


        // we always maintain the current category key
        if (category_key) {
            o.categoryKey = category_key;
        }

        if (model_key) {
            o.modelKey = model_key;
        }
		o.modelYearKey = model_year_key;
	    
		if (province) {
			o.province = province;
		}
	    if (include_fees) {
		    o.include_fees = include_fees;
	    }
        var url = self._replaceUrl(o.url);

        self._beforeAjaxLoad();
        $.ajax({
            type: 'POST'
            , dataType: 'text'
            , url: url
            , global: false
//            , complete: function(xhr, status) {
//                alert(xhr.status);
//            }
            , success: function(data, status, xhr) {
                // flll whatever data back
                ctr.empty();

                if ($.browser.msie) {
                    var doc = document.createElement('div');
                    doc.innerHTML = data;
                    data = doc;
                }

                if (o.contentSelector) {
                    ctr.append($(o.contentSelector, data).html());
                }
                else {
                    ctr.append(data);
                }

                self._success(data, status, xhr);
            }
            , error: function(xhr, err) {
                self._error(xhr, err);
            }
        });
    }
    , _beforeAjaxLoad: function() {
        this._trigger('beforeAjaxLoad');
    }
    , _success: function(data, status, xhr) {
        this._trigger('load_success', null, {
            data: data
            , status: status
            , xhr: xhr
            , widget: this.widget()
        });
    }
    , _error: function(xhr, err) {
        this._trigger('load_error', null, {
            xhr: xhr
            , err: err
            , widget: this.widget()
        });
    }
});

})(jQuery);