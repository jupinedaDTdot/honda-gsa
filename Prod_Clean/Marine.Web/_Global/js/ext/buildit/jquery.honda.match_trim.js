/* $Id$ */

;(function($){
var env = global_get('environment');
var _defaults = {
	finance_url: '/buildittool/' + env + '/data/JSON/{lang}/{year}/{vehicle}/trim_levels.json',
	url: '/buildittool/' + env + '/data/JSON/{lang}/{province}/{year}/{vehicle}/trim_levels_financial.json?include_fees={include_fees}',
	vehicle: '',

	current_trim: null,
	current_transmission: null,
	current_lang: null,
	province_key: null,
	include_fees: false,
	
	add_trim: function(trim) {},
	add_transmission: function(transmission) {},
	switch_trim: function(trim_key) {},
	switch_transmission: function(transmission_key) {},

	_last: null
};

$.widget('honda.match_trim', {
	widgetEventPrefix: 'match_trim_',
	options: $.extend(true, {}, _defaults, (function(){
		try {
			return global_get('honda.match_trim.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
	// initializer
	_resetData: function() {
		this._trims = [];
		this._transmissions = [];
		this._data = null;	
	},
	_create: function() {
		this._resetData();
	},
	// load trim and transmission data from the data source
	loadVehicle: function(vehicle, priceOnly) {
        priceOnly = priceOnly || false;
        
		var self = this,
			o = this.options;
		
		if (vehicle) {
			o.vehicle = vehicle;
		}
		
		this.reloadData(priceOnly);
	},
	switchTrim: function(event, trim_key) {
		var self = this,
			o = this.options;
		
		this._will_switch_trim(event, {trim_key: trim_key, trim: self._data[trim_key], data: self._data});
		if (self._trims.indexOf(trim_key) != -1) {
			o.current_trim = trim_key;
			o.switch_trim.call(self, {trim_key: trim_key, trim: self._data[trim_key], data: self._data});
			this._did_switch_trim(event, {trim_key: trim_key, trim: self._data[trim_key], data: self._data});
		}
		else {
			throw new Error('trim_key does not exist');
		}
	},
	switchTransmission: function(event, transmission_key) {
		var self = this,
			o = this.options;

        // find out trim key
        var trim = null;
        $.each(self._data.keys, function(index, trim_key) {
            if (self._data[trim_key] && self._data[trim_key].hasOwnProperty(transmission_key)) {
                trim = trim_key;
                return false;
            }
        });
		
		this._will_switch_transmission(event, {trim_key: trim, transmission_key: transmission_key, data: self._data});
		if (self._transmissions.indexOf(transmission_key) != -1) {
			o.current_transmission = transmission_key;
			o.switch_transmission({trim_key: trim, transmission_key: transmission_key, data: self._data});
			this._did_switch_transmission(event, {trim_key: trim, transmission_key: transmission_key, data: self._data});
		}
		else {
			throw new Error('transmission_key does not exist.');
		}
	},
	getTrim: function(trim_key) {
		return this._data[trim_key];
	},
	// reload data
	reloadData: function(priceOnly) {
		var self = this,
			o = this.options;

		$.ajax({
			url: o.url.replace('{vehicle}', o.vehicle).replace('{lang}', o.current_lang).replace('{province}', o.province_key).replace('{year}', o.model_year).replace('{include_fees}', o.include_fees ? 'true' : 'false'),
			type: 'GET',
			dataType: 'json',
			global: false,
			cache: false,
			success: function(data, status, xhr) {
				$.ajax({
					url: o.finance_url.replace('{vehicle}', o.vehicle).replace('{lang}', o.current_lang).replace('{province}', o.province_key).replace('{year}', o.model_year),
					type: 'GET',
					dataType: 'json',
					success: function(data2, status, xhr) {
						var data3 = $.extend(true, {}, data, data2);
						self._trims = data3.keys;
						self._transmissions = data3.transmissions_keys;
						self._data = data3;
						
						console.log('match trim data loaded', data3);
						// iterate all trims
						$.each(data3.keys, function(index, trim_key) {
							var trim = data3[trim_key];
							
							self._will_add_trim(null, {
								trim_key: trim_key
								, trim: trim
								, data: data3
                                , priceOnly: priceOnly
							});
							o.add_trim.call(self, {
                                trim_key: trim_key
                                , trim: trim
                                , data: data3
                                , priceOnly: priceOnly
							});
		
							// iterate all transmissions in a trim
							$.each(data3[trim_key].transmissions, function(index, transmission_key) {
								var transmission = trim[transmission_key];
								self._will_add_transmission(null, {
									transmission_key: transmission_key
									, transmission: transmission
									, trim_key: trim_key
									, data: data3
                                    , priceOnly: priceOnly
								});
								o.add_transmission.call(self, {
                                    transmission_key: transmission_key
                                    , transmission: transmission
                                    , trim_key: trim_key
                                    , data: data3
                                    , priceOnly: priceOnly
								});
								self._did_add_transmission(null, {
                                    transmission_key: transmission_key
                                    , transmission: transmission
                                    , trim_key: trim_key
                                    , data: data3
                                    , priceOnly: priceOnly
								});
							});
							
							self._did_add_trim(null, {
                                trim_key: trim_key
                                , trim: trim
                                , data: data3
                                , priceOnly: priceOnly
							});
						});
						
						self._success(data3, status, xhr, priceOnly);
					},
					error: function(xhr2, err) {
						self._error(xhr2, err, priceOnly);
					}
				})

			},
			error: function(xhr, err) {
				self._error(xhr, err);
			}
		});
	},
	
	// events
	_will_add_trim: function(event, ui) {
		this._trigger('will_add_trim', event, ui);
	},
	_did_add_trim: function(event, ui) {
		this._trigger('did_add_trim', event, ui);
	},
	_will_add_transmission: function(event, ui) {
		this._trigger('will_add_transmission', event ,ui);
	},
	_did_add_transmission: function(event, ui) {
		this._trigger('did_add_transmission', event, ui);
	},
	_will_switch_trim: function(event, ui) {
		this._trigger('will_switch_trim', event, ui);
	},
	_did_switch_trim: function(event, ui) {
		this._trigger('did_switch_trim', event, ui);
	},
	_will_switch_transmission: function(event, ui) {
		this._trigger('will_switch_transmission', event, ui);
	},
	_did_switch_transmission: function(event, ui) {
		this._trigger('did_switch_transmission', event, ui);
	},
	_error: function(xhr, err, priceOnly) {
		this._trigger('load_error', null, {
			xhr: xhr,
			err: err
            , priceOnly: priceOnly
		});
	},
	_success: function(data, status, xhr, priceOnly) {
		this._trigger('load_success', null, {
			data: data,
			status: status,
			xhr: xhr
            , priceOnly: priceOnly
		});
	}
});
	
})(jQuery);
