/* $Id$ */

;(function($){

var _defaults = {
	url: '/{category_key}/' + (global_get('lang') == 'en' ? 'choose-a-model' : 'choisir-un-modele') + '?province={province}&include_fees={include_fees}' 
    , contentSelector: false
    , containerSelector: false
    , categoryKey: null
    , province: null
    , include_fees: false
};

$.widget('honda.match_choose_model', {
	widgetEventPrefix: 'match_choose_model_'
	, options: $.extend(true, {}, _defaults, (function(){
		try {
			return global_get('honda.match_choose_model.defaults');
		}
		catch(e) {
			return {};
		}
	})())
    , _create: function() {
        // store the default options
        this.defaults = $.extend(true, {}, this.options);
        this._resetData();
    }
    , _resetData: function() {
        this.options = $.extend(true, {}, this.defaults);
    }
    , _ensureOptions: function() {
        var o = this.options
            , w = this.widget();

        o.url = w.data('hondaMatchChooseModelUrl') || o.url;
        o.categoryKey = w.data('hondaMatchChooseModelCategoryKey') || o.categoryKey;
        o.contentSelector = w.data('hondaMatchChooseModelContentSelector') || o.contentSelector;
        o.containerSelector = w.data('hondaMatchChooseModelContainerSelector') || o.containerSelector;
    }
    , _replaceUrl: function(url) {
        var o = this.options;

        return url.replace('{category_key}', o.categoryKey).replace('{province}', o.province).replace('{include_fees}', o.include_fees);
    }
    , loadCategory: function(category_key, province, include_fees) {
        var self = this
            , o = this.options
            , w = this.widget()
            , ctr = o.containerSelector ? $(o.containerSelector, w) : w;

        self._ensureOptions();

        // we always maintain the current category key
        if (category_key) {
            o.categoryKey = category_key;
        }

        if (province) {
			o.province = province;
		}
	    if (include_fees) {
		    o.include_fees = include_fees;
	    }
	    
        var url = self._replaceUrl(o.url);

        self._beforeAjaxLoad();
        $.ajax({
            type: 'POST'
            , dataType: 'text'
            , url: url
            , global: false
            , success: function(data, status, xhr) {

                if ($.browser.msie) {
                    var doc = document.createElement('div');
                    doc.innerHTML = data;
                    data = doc;
                }

                // flll whatever data back
                ctr.empty();

                if (o.contentSelector) {
                    ctr.append($(o.contentSelector, data).html());
                }
                else {
                    ctr.append(data);
                }

                self._success(data, status, xhr);
            }
            , error: function(xhr, err) {
                self._error(xhr, err);
            }
        });
    }
    , _beforeAjaxLoad: function() {
        this._trigger('beforeAjaxLoad');
    }
    , _success: function(data, status, xhr) {
        this._trigger('load_success', null, {
            data: data
            , status: status
            , xhr: xhr
            , widget: this.widget()
        });
    }
    , _error: function(xhr, err) {
        this._trigger('load_error', null, {
            xhr: xhr
            , err: err
            , widget: this.widget()
        });
    }
});
	
})(jQuery);