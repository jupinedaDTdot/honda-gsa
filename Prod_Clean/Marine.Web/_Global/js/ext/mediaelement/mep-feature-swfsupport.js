(function ($) {

  $.extend(mejs.MepDefaults, {});

  // Play list Selector
  $.extend(MediaElementPlayer.prototype, {
    buildswfsupport: function (player, controls, layers, media) {
      var t = this;
      t.createSwfContainer();
    },

    createSwfContainer: function () {
      var t = this;

      t.$SwfElement = $('<div></div>')
				.addClass("mejs-swfelement")
				.css('left', -10000)
				.insertBefore($('.mejs-mediaelement', t.container));
      t.$MediaElement = $('.mejs-mediaelement', t.container);
      t.$OverlayPlay = $('.mejs-overlay-play', t.container);

      t._resetSwfContainer();
    },


    //playMedia function should be called by other plugins to change current video to other video or swf
    playMedia: function (filePathName, calledOnInit) {
      var t = this;

      // reset status.
      $('.mejs-xml-description', t.$MediaElement).remove();
      t.container.removeClass('playing-xml-mp4');

      if (filePathName.match(/\.swf$/i)) {
        t._playSwf(filePathName);
      }
      if (filePathName.match(/\.mp4$/i)) {
        t._playMp4(filePathName, calledOnInit);
      }
      if (/^https?:\/\//.test(filePathName)) {
        t._playYoutube(filePathName, calledOnInit);
      }
      if (filePathName.match(/config\.xml$/i)) {
        t._playXml(filePathName, calledOnInit);
      }
    },
    pauseMedia: function () {
      this.pause();
    },
    _playXml: function (filePathName, calledOnInit) {
      // load the xml file.
      var t = this, pathPrefix = filePathName.replace(/\/config\.xml/gi, '');
      var tmpl = $('#xml-description-template').clone();
      tmpl.removeAttr('id').appendTo(t.$MediaElement).show();

      $.ajax({
        type: 'GET',
        dataType: 'xml',
        url: filePathName,
        success: function (data) {
          var url = $('video', data).attr('url'),
              disclaimer = $('disclaimer', data).text(),
              description = $('description', data).text(),
              thumb = $('video', data).attr('thumb'),
              bg = $('video', data).attr('bg'),
              link = $('video', data).attr('link'),
              learn = $('video', data).attr('learn'),
              title = $('video', data).attr('desc');

          var abspat = /^(https?:\/\/|\/)/i;
          if (url && !abspat.test(url)) {
            url = pathPrefix + '/' + url;
          }
          if (thumb && !abspat.test(thumb)) {
            thumb = pathPrefix + '/' + thumb;
          }
          if (link && !abspat.test(link)) {
            link = pathPrefix + '/' + link;
          }
          if (bg && !abspat.test(bg)) {
            bg = pathPrefix + '/' + bg;
          }
          $('.description', tmpl).html(description);
          $('.disclaimer', tmpl).html(disclaimer);
          // hook up events
          $('.buttons a', tmpl).click(function (event) {
            event.preventDefault();
            $('.description', tmpl).toggle();
            $('.disclaimer', tmpl).toggle();
          });

          t._playMp4(url, calledOnInit, true);
        },
        error: function (xhr, err) {
          console.log('Unable to load ' + filePathName);
        }
      });
    },
    //playMediaOnInit is called first time when plugin is loaded to check autostart option
    playMediaOnInit: function (filePathName) {
      var t = this;

      t.playMedia(filePathName, true);
    },

    //embed SWF file to player area
    _playSwf: function (filePathName) {
      var t = this;
      t._prepareSwfContainer();


      var swf_install = "/_Global/swf/expressInstall.swf";
      var flashvars = {};
      var params = {
        "menu": "false",
        "scale": "noScale",
        "quality": "high",
        "wmode": "opaque",
        "allowScriptAccess": "always"
      };
      var attributes = {};

      swfobject.embedSWF(
				'/old/_Global/swf/preloader.swf?swf=' + filePathName,
				t._getSwfContainerId(),
				t.width,
				t.height,
				"10.0.0",
				swf_install,
				flashvars,
				params,
				attributes
			);
    },

    //set new video for media element
    _playMp4: function (filePathName, calledOnInit, xmlMode) {
      var t = this;

      t._prepareMediaContainer(xmlMode);

      t.setSrc([{ src: filePathName, type: 'video/mp4'}]);

      if (calledOnInit == true && t.options.autostart == false) {
        //do not start video if player just initialized and autostart is disabled
        return;
      }

      //start video with timeout, otherwise won't work in chrome
      //https://github.com/johndyer/mediaelement/issues/328
      setTimeout(function () { t.play() }, 100);
    },

    //set new video for media element
    _playYoutube: function (filePathName, calledOnInit, xmlMode) {
      var t = this;

      t._prepareMediaContainer(xmlMode);

      t.setSrc([{ src: filePathName, type: 'video/youtube'}]);

      if (calledOnInit == true && t.options.autostart == false) {
        //do not start video if player just initialized and autostart is disabled
        return;
      }

      //start video with timeout, otherwise won't work in chrome
      //https://github.com/johndyer/mediaelement/issues/328
      setTimeout(function () { t.play() }, 100);
    },

    //move video controls outside of the screen; bring swf container to visible area
    _prepareSwfContainer: function () {
      var t = this;
      t.pause();
      t._resetSwfContainer();
      t.container.removeClass('playing-mp4').addClass('playing-swf');
      t.controls.css('left', -10000);
      t.$MediaElement.css('left', -10000);
      t.$OverlayPlay.css('left', -10000);
      t.$SwfElement.css('left', 0);
    },

    //move video controls back to screen; bring swf container outside of visible area
    _prepareMediaContainer: function (xmlMode) {
      var t = this;
      t._resetSwfContainer();
      t.container.addClass('playing-mp4').removeClass('playing-swf');
      if (xmlMode) {
        t.container.addClass('playing-xml-mp4');
        t.media.setVideoSize(t.options.xmlWidth, t.options.xmlHeight);
      }
      else {
        t.media.setVideoSize(t.width, t.height);
      }
      t.$SwfElement.css('left', -10000);
      t.$MediaElement.css('left', 0);
      t.controls.css('left', 0);
      t.$OverlayPlay.css('left', 0);
    },

    //generating unique id for flash element container, based on video id to be able to have multiple instances of video widget on a page
    _getSwfContainerId: function () {
      var t = this;
      return "mejs-swf-" + t.id
    },

    //used to clean flash container from any flash elements, so it can be just moved outside of screen area
    _resetSwfContainer: function () {
      var t = this;
      t.$SwfElement.empty().append('<div id="' + t._getSwfContainerId() + '"></div>');
    }


  });

})(mejs.$);