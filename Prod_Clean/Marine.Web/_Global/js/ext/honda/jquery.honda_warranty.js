;(function($){

$.fn.honda_warranty = function(options) {
	this.each(function() {
		var self = $(this);
		var opts = $.extend(true, {}, $.fn.honda_warranty.defaults, options);
		
		var _load = function() {
			var callback = function() {
				$.ajax({
					url: $.isFunction(opts.url) ? opts.url.apply(self) : opts.url,
					dataType: 'json',
					type: 'GET',
					success: function(data, status, xhr) {
						// iterate all keys
						var sections = [];
						$.each(data.keys, function(index, section_key) {
							// collect items
							var items = [];
							var section = data[section_key];
							
							// an empty section is possible. so let's ignore them.
							if (null == section) {
								return;
							}
							
							$.each(section.keys, function(index, key) {
								var item = {
									label: section[key].label,
									price: section[key].price
								};
								items.push(opts.create_item.apply(self, [item]));
							});
							
							sections.push(opts.create_section.apply(self, [section.title, items]));
						});
						
						opts.append_to_page.apply(self, [sections]);
						opts.success.apply(self, [data, status, xhr]);
					},
					error: function(data, status, xhr) {
						opts.error.apply(self, [data, status, xhr]);
					},
					complete: function(data, status, xhr) {
						opts.complete.apply(this, [data, status, xhr]);
					}
				});
			};
			var run_ = opts.before_send.call(self, callback);
			if (typeof run_ == 'undefined' || run_ !== true) {
				callback.apply(this);
			}
		};

		self.data('honda_warranty', {
			load: _load
		});
		
		_load.apply(this);
	});
};

$.fn.honda_warranty.defaults = {
	url: function() {},
	create_section: function(title, items) {},
	create_item: function(item) {},
	append_to_page: function(sections) {},
	before_send: function() {},
	success: function(data, status, xhr) {},
	error: function(data, status, xhr) {},
	complete: function(data, status, xhr) {},
	// last
	_last: null
};


})(jQuery);
