;(function($, D, W, F, undefined) {

var _defaults = {
    size: 4,
    modelSpecsUrl: '/shared/json/compare.json',
    beforeAdd: function(event, ui) {},
    add: function(event, ui) {},
    beforeReplace: function(event, ui) {},
    replace: function(event, ui) {},
    remove: function(event, ui) {},
    highlight: function(event, ui) {},
    modelIdKey: 'trimexportkeylist',
    modelIdEncode: function(id) {
        if ($.isArray(id)) {
            return id.join(',');
        }
        else {
            return id;
        }
    }
};

var ReleasePool = $.commonlib.collections.ReleasePool;

$.widget('honda.CompareApp', {
    widgetEventPrefix: 'CompareApp_',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('honda.CompareApp.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        this.dataModel = {};
        this.dataIndexes = {};
        this.dataModelSize = 0;
        this.highlights = {};
        this.dataModelSpecPool = new ReleasePool();
        this.dataModelSpecs = {};

        this._queue = new jQuery.jobqueue();
    },
    add: function(model_id, model_family) {
        var self = this;
        // check is array
        if (!$.isArray(model_id)) {
            model_id = [model_id];
        }

        $.each(model_id, function(index, e) {
            if (self.dataModel.hasOwnProperty(e)) {
                throw new Error(e + " exists.");
            }
        });

        self._beforeAdd(model_id);
        self._loadModelSpecs(model_id, model_family, function(data) {
            self._add(model_id, data);
        });
    },
    replace: function(model_id, replacement_id, model_family) {
        var self = this;
        if (self.dataModel.hasOwnProperty(replacement_id)) {
            throw new Error(replacement_id + " exists.");
        }
        if (!self.dataModel.hasOwnProperty(model_id)) {
            throw new Error(model_id + " does not exist.");
        }
        self._beforeReplace(model_id, replacement_id);
        this._loadModelSpecs(replacement_id, model_family, function(data) {
            self._replace(model_id, replacement_id, data);
        });
    },
    remove: function(model_id) {
        var self = this;
        if (self.dataModel.hasOwnProperty(model_id)) {
            self._trigger('beforeRemove', null, {
                id: model_id
            });

            this._remove(model_id);
        }
    },
    containsSpec: function(spec_key) {
        return this.dataModelSpecPool.contains(spec_key);
    },
    getSpec: function(spec_key) {
        return this.dataModelSpecs[spec_key];
    },
    _loadModelSpecs: function(model_id, model_family, callback) {
        var self = this
            , o = this.options
            , url = o.modelSpecsUrl
            , data = {};

        data[o.modelIdKey] = o.modelIdEncode(model_id);

        if (model_family) {
            url = url.replace('{modelFamily}', model_family);
        }

        $.ajax({
            url: url,
            type: 'GET',
            global: false,
            cache: false,
            dataType: 'json',
            data: data,
            success: function(data, status, xhr) {
                callback(data);
            },
            error: function(xhr, err) {
                self._error(xhr, err);
            }
        });
    },
    _error: function(xhr, err) {
        this._trigger('error', null, {
            xhr: xhr,
            err: err
        });
    },
    _modelsLoaded: function(models) {
        this._trigger('modelsLoaded', null, {
            models: models
        });
    },
    _beforeAdd: function(model_id) {
        this._trigger('beforeAdd', null, {
            id: model_id
        });
    },
    _add: function(model_id, data) {
        var self = this;

        $.extend(true, self.dataModelSpecs, data.specs);

        $.each(model_id, function(index, model_id) {
            if (!data.trims[model_id]) {
                throw new Error(model_id + ' does not exist.');
            }
            self.dataModel[model_id] = data.trims[model_id];
            self.dataIndexes[model_id] = self.dataModelSize ++;
            self.dataModelSpecPool.retainAll(data.trims[model_id].specs.keys);

            self._trigger('add', null, {
                id: model_id,
                data: self.dataModel[model_id]
            });
        });

        this._queue.add(function() {
            self._highlight();
        });
    },
    _beforeReplace: function(model_id, replacement_id) {
        this._trigger('beforeReplace', null, {
            id: model_id,
            replacement_id: replacement_id
        });
    },
    _replace: function(model_id, replacement_id, data) {
        var self = this;

        if (!data.trims[replacement_id]) {
            throw new Error(replacement_id + ' does not exist.');
        }

        $.extend(true, self.dataModelSpecs, data.specs);
        self.dataIndexes[replacement_id] = self.dataIndexes[model_id];
        self.dataModelSpecPool.releaseAll(self.dataModel[model_id].specs.keys);
        delete self.dataModel[model_id];
        delete self.dataIndexes[model_id];
        self.dataModelSpecPool.retainAll(data.trims[replacement_id].specs.keys);
        self.dataModel[replacement_id] = data.trims[replacement_id];

        this._trigger('replace', null, {
            id: model_id,
            replacement_id: replacement_id,
            data: self.dataModel[replacement_id]
        });

        this._queue.add(function() {
            self._highlight();
        });
    },
    _remove: function(model_id) {
        var self = this;

        self.dataModelSpecPool.releaseAll(self.dataModel[model_id].specs.keys);
        delete self.dataModel[model_id];
        delete self.dataIndexes[model_id];
        self.dataModelSize --;

        this._trigger('remove', null, {
            id: model_id
        });

        this._queue.add(function() {
            self._highlight();
        });
    },
    _highlight: function() {
        var self = this;
        // don't highlight if number of data models is less than or equal to 1
        if (self.dataModelSize < 1) {
            return;
        }

        // find unmatched specs
        for (var key in self.dataModelSpecPool) {
            if (self.dataModelSpecPool.contains(key)) {
                // not every spec has this item value
                if (self.dataModelSpecPool[key] < self.dataModelSize) {
                    self._trigger('highlight', null, {
                        key: key,
                        highlight: true
                    });
                }
                else {
                    // check specs in all models
                    var same = $.reduce(self.dataModel, {eq: true, val: null}, function(last) {
                        if (last.eq === false) {
                            return {eq: false, val: null};
                        }
                        else if (last.val === null) {
                            return {eq: true, val: this.specs[key].SpecValue};
                        }
                        else  {
                            return {eq: this.specs[key].SpecValue == last.val, val: this.specs[key].SpecValue};
                        }
                    });

                    if (!same.eq) {
                        self._trigger('highlight', null, {
                            key: key,
                            highlight: true
                        });
                    }
                    else {
                        self._trigger('highlight', null, {
                            key: key,
                            highlight: false
                        });
                    }
                }
            }
        }

    }
});

})(jQuery, document, window, window);