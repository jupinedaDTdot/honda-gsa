;(function($, D, W, F, undefined) {

var _defaults = {
    add: function(event, ui) {},
    remove: function(event, ui) {},
    change: function(event, ui) {},
    reset: function(event, ui) {}
};

var ReleasePool = $.commonlib.collections.ReleasePool;

$.widget('honda.ChooseModelApp', {
    widgetEventPrefix: 'ChooseModelApp_',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('honda.ChooseModelApp.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        this.trims = {}; // a dictionary keyed by trim export key => trim object
        this.trimCount = 0;
        this.filters = {}; // a dictionary keyed by filter ids
        this.filterCriteria = {}; // a dictionary of criteria keyed by filter ids
        this.activeTrims = []; // a list of trim export keys
        this.inactiveTrims = [];
        this.criteriaChanging = false;
    },
    /**
     *
     * @param filter_id
     * @param filter Filter structure:
     * radio: filter = {
     *   type: 'radio',
     *   default_value: 'a value', empty, or undefined
     * }
     * checkbox: filter = {
     *   type: 'checkbox',
     *   default_value: [] or ['a','b','c'], or 'a value'
     * }
     * range: filter = {
     *   type: 'range',
     *   default_value: [min, max]
     * }
     */
    addFilter: function(filter_id, filter) {
        var self = this,
            o = this.options,
            filters = this.filters,
            filterCriteria = this.filterCriteria;

        if (filters.hasOwnProperty(filter_id)) {
            throw new Error(filter_id + ' does exist.');
        }

        // assign the filter
        filters[filter_id] = filter;

        self._setupFilterDefaultValues(filter_id, filter);
    },
    _setupFilterDefaultValues: function(filter_id, filter) {
        var self = this,
            filterCriteria = self.filterCriteria;
        // set up the default criteria
        switch (filter.type) {
            case 'radio':
                filterCriteria[filter_id] = filter.default_value || undefined;
                break;
            case 'checkbox':
                filterCriteria[filter_id] = ["True", "False"];
                break;
            case 'range':
                filterCriteria[filter_id] = filter.default_value;
                break;
            default:
                throw new Error(filter_id + ' has unrecognized type: ' + filter.type);
        }
    },
    add: function(trim_id, trim) {
        var self = this,
            o = this.options,
            values = {};

        if (self.trims.hasOwnProperty(trim_id)) {
            throw new Error(trim_id + ' already exists.');
        }

        self.trimCount ++;

        for (var key in trim.values) {
            if (typeof trim.values[key] == 'number' || typeof trim.values[key] == 'string') {
                values[key] = trim.values[key].toString();
            }
        }

        self.trims[trim_id] = {
            id: trim.id,
            values: values
        };

        self._add(null, {
            widget: self.widget(),
            id: trim_id,
            trim: trim
        });
    },
    remove: function(trim_id) {
        var self = this,
            o = this.options;

        if (!self.trims.hasOwnProperty(trim_id)) {
            throw new Error(trim_id + ' does not exist.');
        }

        delete self.trims[trim_id];

        self.trimCount --;

        self._remove(null, {
            widget: self.widget(),
            id: trim_id
        });
    },
    changeCriteria: function(filter_id, filter_value) {
        var self = this;
        if (!self.filters.hasOwnProperty(filter_id)) {
            throw new Error(filter_id + ' does not exist.');
        }

        // update the filter
        self.filterCriteria[filter_id] = filter_value;

        self.activateTrims();
    },
    resetCriteria: function() {
        var self = this;

        // update all filter criteria to default values
        $.each(self.filters, function(filter_id, filter) {
            self._setupFilterDefaultValues(filter_id, filter);
        });

        self._reset(null, {
            widget: this.widget()
        });
        self.activateTrims();
    },
    activateTrims: function() {
        var self = this;
        
        // go through all the trims and find activated trims according to criteria
        self.activeTrims = []; // we now have a new active trims array
        self.inactiveTrims = [];
        $.each(self.trims, function(trim_id, trim) {
            var matched = self._matchCriteria(trim);
            // if matched, put the trim_id into the activated trims
            if (matched) {
                self.activeTrims.push(trim_id);
            }
            else {
                self.inactiveTrims.push(trim_id);
            }
        });

        self._change(null, {
            active: self.activeTrims,
            inactive: self.inactiveTrims,
            widget: self.widget()
        });
    },
    _matchCriteria: function(trim) {
        var self = this,
            matched = true;

        $.each(self.filterCriteria, function(f_id, f_value) {
            matched = matched && self._matchOneFilter(trim, f_id, f_value);
        });

        return matched;
    },
    _matchOneFilter: function(trim, filter_id, filter_value) {
        var self = this;

        if (!self.filters.hasOwnProperty(filter_id)) {
            throw new Error('filter: ' + filter_id + ' does not exist.');
        }

        // TODO new criteria: if filter is empty, we should return true
        if (!trim.values.hasOwnProperty(filter_id)) {
            return true;
        }

        switch (self.filters[filter_id].type) {
            case 'radio':
                return filter_value === undefined || filter_value == trim.values[filter_id];
            case 'checkbox':
                return filter_value.length == 0 || $.inArray(trim.values[filter_id], filter_value) >= 0;
            case 'range':
                return trim.values[filter_id] >= filter_value[0] && trim.values[filter_id] <= filter_value[1];
            default:
                return false;
        }
    },
    isActive: function(trim_id) {
        return $.inArray(trim_id, this.activeTrims) >= 0;
    },
    getActiveTrims: function() {
        return this.activeTrims;
    },
    _add: function(event, ui) {
        this._trigger('add', event, ui);
    },
    _remove: function(event, ui) {
        this._trigger('remove', event, ui);
    },
    _change: function(event, ui) {
        this._trigger('change', event, ui);
    },
    _reset: function(event, ui) {
        this._trigger('reset', event, ui);
    }
});

})(jQuery, document, window, window);