﻿var optionSetObject;
window.page_init = pageInitHandler;
window.onunload = windowUnloadHandler;

function pageInitHandler() {
    // Instantiate the phone-selector object
    var optionsDiv = $(".PhoneSetupOptionsDiv").get();
    var language = global_get('lang');
    var brand = global_get('brand');
    var timeoutInMilliSecond = 20000;
    optionSetObject = new Climax.Bluetooth.VehiclePhoneSelector(optionsDiv, brand, language, timeoutInMilliSecond);
}

function windowUnloadHandler() {
    // Dispose objects
    if (optionSetObject) optionSetObject.dispose();
}

function validateAndSubmit() {
    if ($('#vehicleModelSelector').val() != "-1" && $('#vehicleYearSelector').val() != "-1" && $('#vehicleTrimSelector').val() != "-1" && $('#phoneManufacturerSelector').val() != "-1" && $('#phoneModelSelector').val() != "-1")
        window.location.href = global_get('bluetooth.pairing.instructions')
            .replace('{modelId}', $('#vehicleModelSelector').val())
            .replace('{modelYearId}', $('#vehicleYearSelector').val())
            .replace('{trimId}', $('#vehicleTrimSelector').val())
            .replace('{phoneId}', $('#phoneModelSelector').val());
    //'/bluetooth/instructions/200ad24e-230b-4740-82d9-64dd834e2a2d/ca4e0ae0-5d49-40d0-9f33-58274e15f849/9d50aaa1-fc14-43d9-adcb-fee5e04b1e2d/b1ee61b8-65ec-4e6a-8fc5-80fbb2c89c70';
}