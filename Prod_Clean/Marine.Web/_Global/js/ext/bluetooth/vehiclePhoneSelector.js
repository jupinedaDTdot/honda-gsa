﻿///<reference path="~/_Global/js/jquery.js"/>
/// <reference path="~/_Global/js/knockout-3.0.0.js"/>

var Climax;
(function(climax) {
    (function(bluetooth) {
        // Constructor
        var vehiclePhoneSelector = function(element, brand, language, timeoutInMilliSecond, filterPhoneManufacturersByVehicle) {
            // Create instance fields
            this._apiRootUrl = "/api/Bluetooth";
            this._elementQuery = $(element);
            this._brand = brand;
            this._language = language;
            this._timeoutInMilliSecond = timeoutInMilliSecond  || 20000;
            this._filterPhoneManufacturersByVehicle = filterPhoneManufacturersByVehicle === true ? true : false;
            this._vehicleModelSelectorQuery = this._elementQuery.find("#vehicleModelSelector");
            this._vehicleYearSelectorQuery = this._elementQuery.find("#vehicleYearSelector");
            this._vehicleTrimSelectorQuery = this._elementQuery.find("#vehicleTrimSelector");
            this._phoneManufacturerSelectorQuery = this._elementQuery.find("#phoneManufacturerSelector");
            this._phoneModelSelectorQuery = this._elementQuery.find("#phoneModelSelector");
            
            // Create proxies for the event handlers
            this._vehicleModelChangeHandlerProxy = $.proxy(this._vehicleModelChangeHandler, this);
            this._vehicleYearChangeHandlerProxy = $.proxy(this._vehicleYearChangeHandler, this);
            this._vehicleTrimChangeHandlerProxy = $.proxy(this._vehicleTrimChangeHandler, this);
            this._phoneManufacturerChangeHandlerProxy = $.proxy(this._phoneManufacturerChangeHandler, this);

            // Bind event handlers
            this._vehicleModelSelectorQuery.bind("change", this._vehicleModelChangeHandlerProxy);
            this._vehicleYearSelectorQuery.bind("change", this._vehicleYearChangeHandlerProxy);
            this._vehicleTrimSelectorQuery.bind("change", this._vehicleTrimChangeHandlerProxy);
            this._phoneManufacturerSelectorQuery.bind("change", this._phoneManufacturerChangeHandlerProxy);

            // Initialize the control
            this._initialize();
        };

        // Disposer
        vehiclePhoneSelector.prototype.dispose = function() {
            // Unbind event handlers
            this._vehicleModelSelectorQuery.unbind("change", this._vehicleModelChangeHandlerProxy);
            this._vehicleYearSelectorQuery.unbind("change", this._vehicleYearChangeHandlerProxy);
            this._vehicleTrimSelectorQuery.unbind("change", this._vehicleTrimSelectorQuery);
            this._phoneManufacturerSelectorQuery.unbind("change", this._phoneManufacturerChangeHandlerProxy);

            // Clear proxies
            this._vehicleModelChangeHandlerProxy = null;
            this._vehicleYearChangeHandlerProxy = null;
            this._vehicleTrimChangeHandlerProxy = null;
            this._phoneManufacturerChangeHandlerProxy = null;

            // Clear instance fields
            this._apiRootUrl = null;
            this._elementQuery = null;
            this._brand = null;
            this._language = null;
            this._timeoutInMilliSecond = null;
            this._filterPhoneManufacturersByVehicle = null;
            this._vehicleModelSelectorQuery = null;
            this._vehicleYearSelectorQuery = null;
            this._vehicleTrimSelectorQuery = null;
            this._phoneManufacturerSelectorQuery = null;
            this._phoneModelSelectorQuery = null;
        };

        // Initializer
        vehiclePhoneSelector.prototype._initialize = function() {
            // Load vehicle models
            this._bindVehicleModels();

            // Disable empty drop-downs
            this._vehicleYearSelectorQuery.attr("disabled", "disabled");
            this._vehicleTrimSelectorQuery.attr("disabled", "disabled");
            this._phoneManufacturerSelectorQuery.attr("disabled", "disabled");
            this._phoneModelSelectorQuery.attr("disabled", "disabled");
        };

        vehiclePhoneSelector.prototype._vehicleModelChangeHandler = function() {
            var selectedVehicleModelId = this._vehicleModelSelectorQuery.val();
            if (selectedVehicleModelId == -1) {
                // Clear non-default options of the consequent drop-down
                vehiclePhoneSelector._removeNondefaultOptions(this._vehicleYearSelectorQuery);
                vehiclePhoneSelector._removeNondefaultOptions(this._vehicleTrimSelectorQuery);
                vehiclePhoneSelector._removeNondefaultOptions(this._phoneManufacturerSelectorQuery);
                vehiclePhoneSelector._removeNondefaultOptions(this._phoneModelSelectorQuery);
            } else {
                // Bind the vehicle years based on the selected model
                this._bindVehicleYears(selectedVehicleModelId);
            }

            // Disable the consequent drop-downs
            this._vehicleYearSelectorQuery.attr("disabled", "disabled");
            this._vehicleTrimSelectorQuery.attr("disabled", "disabled");
            this._phoneManufacturerSelectorQuery.attr("disabled", "disabled");
            this._phoneModelSelectorQuery.attr("disabled", "disabled");
        };

        vehiclePhoneSelector.prototype._vehicleYearChangeHandler = function() {
            var selectedVehicleModelId = this._vehicleModelSelectorQuery.val();
            var selectedVehicleYear = this._vehicleYearSelectorQuery.val();
            if (selectedVehicleModelId == -1 || selectedVehicleYear == -1) {
                // Clear non-default options of the consequent drop-down
                vehiclePhoneSelector._removeNondefaultOptions(this._vehicleTrimSelectorQuery);
                vehiclePhoneSelector._removeNondefaultOptions(this._phoneManufacturerSelectorQuery);
                vehiclePhoneSelector._removeNondefaultOptions(this._phoneModelSelectorQuery);
            } else {
                // Bind the vehicle trims based on the selected model and year
                this._bindVehicleTrims(selectedVehicleModelId, selectedVehicleYear);
            }

            // Disable the consequent drop-downs
            this._vehicleTrimSelectorQuery.attr("disabled", "disabled");
            this._phoneManufacturerSelectorQuery.attr("disabled", "disabled");
            this._phoneModelSelectorQuery.attr("disabled", "disabled");
        };

        vehiclePhoneSelector.prototype._vehicleTrimChangeHandler = function() {
            var selectedVehicleModelId = this._vehicleModelSelectorQuery.val();
            var selectedVehicleYear = this._vehicleYearSelectorQuery.val();
            var selectedVehicleTrim = this._vehicleTrimSelectorQuery.val();
            if (selectedVehicleModelId == -1 || selectedVehicleYear == -1 || selectedVehicleTrim == -1) {
                // Clear non-default options of the consequent drop-down
                vehiclePhoneSelector._removeNondefaultOptions(this._phoneManufacturerSelectorQuery);
                vehiclePhoneSelector._removeNondefaultOptions(this._phoneModelSelectorQuery);
            } else {
                // Bind the phone manufacturers
                this._bindPhoneManufacturers(this._filterPhoneManufacturersByVehicle, selectedVehicleModelId, selectedVehicleYear, selectedVehicleTrim);
            }

            // Disable the consequent drop-downs
            this._phoneManufacturerSelectorQuery.attr("disabled", "disabled");
            this._phoneModelSelectorQuery.attr("disabled", "disabled");
        };

        vehiclePhoneSelector.prototype._phoneManufacturerChangeHandler = function() {
            var selectedVehicleModelId = this._vehicleModelSelectorQuery.val();
            var selectedVehicleYear = this._vehicleYearSelectorQuery.val();
            var selectedVehicleTrim = this._vehicleTrimSelectorQuery.val();
            var selectedPhoneManufacturer = this._phoneManufacturerSelectorQuery.val();
            if (selectedVehicleModelId == -1 || selectedVehicleYear == -1 || selectedVehicleTrim == -1 || selectedPhoneManufacturer == -1) {
                // Clear non-default options of the consequent drop-down
                vehiclePhoneSelector._removeNondefaultOptions(this._phoneModelSelectorQuery);

                // Disable the consequent drop-down
                this._phoneModelSelectorQuery.attr("disabled", "disabled");
            } else {
                // Bind the phone models
                this._bindPhoneModels(selectedPhoneManufacturer, selectedVehicleModelId, selectedVehicleYear, selectedVehicleTrim);
            }
        };

        vehiclePhoneSelector.prototype._bindVehicleModels = function() {
	        var apiUrlTemplate = "{apiRootUrl}/vehicle/{brandId}/models/{language}";
            var apiUrl = apiUrlTemplate
                .replace("{apiRootUrl}", this._apiRootUrl)
                .replace("{brandId}", this._brand)
                .replace("{language}", this._language);

            var thisObj = this;
            vehiclePhoneSelector._showLoader("#loader_models");
            $.ajax({
                type: "GET",
                url: apiUrl,
                timeout: thisObj._timeoutInMilliSecond,
                dataType: "json",
                success: function(data) {
                    vehiclePhoneSelector._populateDropDown(data, thisObj._vehicleModelSelectorQuery);
                }
            });
        };

        vehiclePhoneSelector.prototype._bindVehicleYears = function(selectedVehicleModelId) {
            var apiUrlTemplate = "{apiRootUrl}/vehicle/{brandId}/{modelId}/modelyears/{language}";
            var apiUrl = apiUrlTemplate
                .replace("{apiRootUrl}", this._apiRootUrl)
                .replace("{brandId}", this._brand)
                .replace("{modelId}", selectedVehicleModelId)
                .replace("{language}", this._language);
            
            var thisObj = this;
            vehiclePhoneSelector._showLoader("#loader_years");
            $.ajax({
                type: "GET",
                url: apiUrl,
                timeout: thisObj._timeoutInMilliSecond,
                dataType: "json",
                success: function(data) {
                    vehiclePhoneSelector._populateDropDown(data, thisObj._vehicleYearSelectorQuery);
                }
            });
        };

        vehiclePhoneSelector.prototype._bindVehicleTrims = function(selectedVehicleModelId, selectedVehicleYear) {
            var apiUrlTemplate = "{apiRootUrl}/vehicle/{brandId}/{modelId}/{modelYearId}/trims/{language}";
            var apiUrl = apiUrlTemplate
                .replace("{apiRootUrl}", this._apiRootUrl)
                .replace("{brandId}", this._brand)
                .replace("{modelId}", selectedVehicleModelId)
                .replace("{modelYearId}", selectedVehicleYear)
                .replace("{language}", this._language);
            
            var thisObj = this;
            vehiclePhoneSelector._showLoader("#loader_trims");
            $.ajax({
                type: "GET",
                url: apiUrl,
                timeout: thisObj._timeoutInMilliSecond,
                dataType: "json",
                success: function(data) {
                    vehiclePhoneSelector._populateDropDown(data, thisObj._vehicleTrimSelectorQuery);
                }
            });
        };

        vehiclePhoneSelector.prototype._bindPhoneManufacturers = function(filterManufacturersByVehicle, selectedVehicleModelId, selectedVehicleYear, selectedtrimId) {
            var apiUrlTemplate =
                filterManufacturersByVehicle ?
                    "{apiRootUrl}/phone/manufacturers/{brandId}/{modelId}/{modelYearId}/{trimId}/{language}" :
                    "{apiRootUrl}/phone/{brandId}/manufacturers/{language}";
            var apiUrl = apiUrlTemplate
                .replace("{apiRootUrl}", this._apiRootUrl)
                .replace("{brandId}", this._brand)
                .replace("{modelId}", selectedVehicleModelId)
                .replace("{modelYearId}", selectedVehicleYear)
                .replace("{trimId}", selectedtrimId)
                .replace("{language}", this._language);

            var thisObj = this;
            vehiclePhoneSelector._showLoader("#loader_manufacturers");
            $.ajax({
                type: "GET",
                url: apiUrl,
                timeout: thisObj._timeoutInMilliSecond,
                dataType: "json",
                success: function(data) {
                    vehiclePhoneSelector._populateDropDown(data, thisObj._phoneManufacturerSelectorQuery);
                }
            });
        };

        vehiclePhoneSelector.prototype._bindPhoneModels = function(selectedPhoneManufacturer, selectedVehicleModelId, selectedVehicleYear, selectedtrimId) {
            var apiUrlTemplate = "{apiRootUrl}/phone/{brandId}/{modelId}/{modelYearId}/{trimId}/{manufacturerId}/phones/{language}";
            var apiUrl = apiUrlTemplate
                .replace("{apiRootUrl}", this._apiRootUrl)
                .replace("{brandId}", this._brand)
				.replace("{modelId}", selectedVehicleModelId)
                .replace("{modelYearId}", selectedVehicleYear)
                .replace("{trimId}", selectedtrimId)
                .replace("{manufacturerId}", selectedPhoneManufacturer)
                .replace("{language}", this._language);
            
            var thisObj = this;
            vehiclePhoneSelector._showLoader("#loader_phones");
            $.ajax({
                type: "GET",
                url: apiUrl,
                timeout: thisObj._timeoutInMilliSecond,
                dataType: "json",
                success: function(data) {
                    vehiclePhoneSelector._populateDropDown(data, thisObj._phoneModelSelectorQuery);
                }
            });
        };

        vehiclePhoneSelector._populateDropDown = function(data, dropDwonQuery) {
            vehiclePhoneSelector._removeNondefaultOptions(dropDwonQuery);
            $.each(data, function(index, item) {
                dropDwonQuery.append('<option value="' + item.Id + '">' + item.DisplayName + '</option>');
            });
            dropDwonQuery.removeAttr("disabled");
	        vehiclePhoneSelector._hideLoader();
        };

        vehiclePhoneSelector._removeNondefaultOptions = function(dropDwonQuery) {
            dropDwonQuery.find("option[value!='-1']").remove();
        };

	    vehiclePhoneSelector._showLoader = function(loaderId) {
		    $(loaderId + '.loader').append($('<div>').addClass('loader_container').css({ 'text-align': 'center'}).append(
			    $('<span>').addClass('bg_loader loader_small_gray').width(32).height(32).css({ margin: 'auto', display: 'inline-block' })
		    ));
	    };
	    vehiclePhoneSelector._hideLoader = function() {
		    $('.loader_container').remove();
	    };
    
        bluetooth.VehiclePhoneSelector = vehiclePhoneSelector;
    })(climax.Bluetooth || (climax.Bluetooth = { }));
})(Climax || (Climax = { }));