﻿/// <reference path="~/_Global/js/jquery-1.7.js"/>
/// <reference path="~/_Global/js/knockout-3.0.0.js"/>

var Climax;
(function(climax) {
    (function(bluetooth) {
        // Constructor
        var faqDisplay = function(faqList, selectedOptionsDiv, timeoutInMilliSecond) {
            // Create instance fields
            this._apiRootUrl = "/api/Bluetooth";
            this.faqListQuery = $(faqList);
            this._selectedOptionsQuery = selectedOptionsDiv && selectedOptionsDiv.length ? $(selectedOptionsDiv) : null;
            this._timeoutInMilliSecond = timeoutInMilliSecond || 15000;
        };

        faqDisplay.prototype.dispose = function() {
            // Clear instance fields
            this._apiRootUrl = null;
            this.faqListQuery = null;
            this._selectedOptionsQuery = null;
            this._timeoutInMilliSecond = null;
        };

        faqDisplay.prototype.getFaq = function(options) {
            var thisObj = this;
            
            // Bind selected options
            if (this._selectedOptionsQuery) {
                ko.applyBindings(options, this._selectedOptionsQuery[0]);
            }

            // Get and bind FAQ
            var apiUrlTemplate = "{apiRootUrl}/vehicle/{brandId}/{modelId}/{modelYearId}/{trimId}/platform/{language}";
            var apiUrl = apiUrlTemplate
                .replace("{apiRootUrl}", thisObj._apiRootUrl)
                .replace("{brandId}", options.brand)
                .replace("{modelId}", options.vehicleModelId)
                .replace("{modelYearId}", options.vehicleYearId)
                .replace("{trimId}", options.vehicleTrimId)
                .replace("{language}", options.language);
            $.ajax({
                type: "GET",
                url: apiUrl,
                timeout: thisObj._timeoutInMilliSecond,
                dataType: "json",
                success: function(platform) {
                    var displayModel = faqDisplay.prepareFaqDisplayModel(platform);
                    ko.applyBindings(displayModel, thisObj.faqListQuery[0]);
                }
            });
        };

        faqDisplay.prepareFaqDisplayModel = function(platform) {
            var result = new Array();

            result.isAnswer = function(question, answerIndex) {
                    return question.answerIndex == answerIndex;
            };

            var isX75 = faqDisplay._isX75(platform);
            var isGenY = faqDisplay._isGenY(platform);
            var isGenYplus = faqDisplay._isGenYplus(platform);
            

            // Q1, Q2, Q3, Q4, Q9, Q10
            var alwaysDisplayedQuestionIndices = new Array(1, 2, 3, 4, 9, 10);
            $.each(alwaysDisplayedQuestionIndices, function(index, item) {
                result[item] = {
                    isVisible: true,
                    answerIndex: 0
                };
            });

            // Q11, Q12, Q13, Q14, Q15, Q16
            var x75GenYGenYplusIndices = new Array(11, 12, 13, 14, 15, 16);
            $.each(x75GenYGenYplusIndices, function(index, item) {
                result[item] = {
                    isVisible: isX75 || isGenY || isGenYplus,
                    answerIndex: 0
                };
            });

            // Q5
            result[5] = {
                isVisible: true,
                answerIndex: isX75 || isGenY || isGenYplus ? 0 : 1,
            };

            // Q6
            result[6] = {
                isVisible: isGenY || isGenYplus,
                answerIndex: 0
            };

            // Q7
            result[7] = {
                isVisible: isGenY || isGenYplus,
                answerIndex: 0
            };

            // Q8
            result[8] = {
                isVisible: isGenYplus,
                answerIndex: 0
            };
            return result;
        };

        faqDisplay._isX75 = function(platform) {
            if (platform && platform.Code)
                return platform.Code.toLowerCase() == "honda-na-11-x75-";
            return false;
        };

        faqDisplay._isGenY = function(platform) {
            if (platform && platform.Code)
                return platform.Code.toLowerCase() == "honda-na-12-geny-";
            return false;
        };

        faqDisplay._isGenYplus = function(platform) {
            if (platform && platform.Code)
                return platform.Code.toLowerCase() == "honda-na-13-geny-plus-";
            return false;
        };

        bluetooth.FaqDisplay = faqDisplay;
    })(climax.Bluetooth || (climax.Bluetooth = { }));
}(Climax || (Climax = { })));