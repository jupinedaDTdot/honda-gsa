;(function($, M, D, F, require, undefined){

var _defaults = {
    type: 'flash',
    width: 0,
    height: 0,
    player: '/old/_Global/swf/honda_slideShow.swf',
    flashvars: {'xml_location':'','debug': 'browser'},
    params:{'allowScriptAccess':'sameDomain','allowNetworking':'all','wmode':'transparent','quality':'high','play':'true','loop':'false','menu':'false','scale':'showall','bgcolor':'#ffffff'}
};
 if ($('.slideshow_player_support:not(.slideshow_player_processed)').size() > 0) {
        require(['includes/jquery.linkrel'], function() {
            $('.slideshow_player_support:not(.slideshow_player_processed)').each(function() {
                var self = $(this);
                var options = $.extend(true, {}, $.fn.linkrel.default_options, F.global_get('player.slideshow'));
                // set the width and height according to the container
                var width = self.width();
                var height = self.height();
                if (width > 0 && height > 0) {
                    // options.width = width;
                    // options.height = height;
                }
                var url = self.attr('title');
                options.flashvars.xml_location = url;
                if ($.flash.available) {
                    self.empty().flash(options);
                    self.removeAttr('title');
                }
                else {
                    requires_flash_message(self);
                }
            });
        });
    }

$.widget('commonlib.slideshow', $.commonlib.widget, {
    widgetEventPrefix: 'slideshow',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.slideshow.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        this._ensureOptions();
    },
    _ensureOptions: function() {
        var self = this,
            o = this.options,
            widget = this.widget();

        o.type = widget.data('slideshow-type') || o.type;
        o.width = widget.data('slideshow-width') || o.width;
        o.height = widget.data('slideshow-height') || o.height;
        o.player = widget.data('slideshow-player') || o.player;
        o.flashvars = widget.data('slideshow-flashvars') || o.flashvars;
        o.params = widget.data('slideshow-params') || o.params;
    }
}); // of $.widget

})(jQuery, Modernizr, document, window, require);