;(function($, undefined) {

var _defaults = {
    swipeSensitivity: 18,
    swipeDirection: 'horizontal',
    mouse: true, // NOT SUPPORTED YET
    swipe: function(event, ui) {}
};

if (!Math['sgn']) {
    Math.sgn = function(e) {
        return e > 0 ? 1 : (e < 0 ? -1 : 0);
    };
}

$.widget('commonlib.touch', {
    widgetEventPrefix: 'touch',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.touch.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        var self = this,
            o = this.options,
            widget = this.widget();


        // event detect
        var touch = {}, mousedown = false;

        if (o.mouse) {
//            widget.disableSelection();
            widget.css('-webkit-user-select', 'none'); // webkit
            widget.css('-moz-user-select', 'none'); // 
            widget.bind('selectstart', function(event) { event.preventDefault(); });

            widget.bind('mousedown', function(event) {
                mousedown = true;
                touch.x = event.pageX;
                touch.y = event.pageY;
            });

            widget.bind('mousemove', function(event) {
                if (!mousedown) {
                    return;
                }

                var deltaX = touch.x - event.pageX,
                    deltaY = touch.y - event.pageY;

                // sensitivity support
                if (Math.abs(deltaX) > o.swipeSensitivity && Math.abs(deltaX) > Math.abs(deltaY)) {
                    self._swipe(event,
                            Math.abs(deltaX) > o.swipeSensitivity && (o.swipeDirection == 'both' || o.swipeDirection == 'horizontal') ? Math.sgn(deltaX) : false,
                            Math.abs(deltaY) > o.swipeSensitivity && (o.swipeDirection == 'both' || o.swipeDirection == 'vertical') ? Math.sgn(deltaY) : false);
                    mousedown = false;
                    event.preventDefault();
                }
            });
        }

        widget[0].ontouchstart = function(event) {
            var t = event.touches[0];
            touch.x = t.clientX;
            touch.y = t.clientY;
        };

        widget[0].ontouchmove = function(event) {
            // one finger events only, and make sure myself is not animated to enroll
            if (event.touches.length == 1 && !widget.is(':animated')) {
                var t = event.touches[0],
                    deltaX = touch.x - t.clientX,
                    deltaY = touch.y - t.clientY;

                // sensitivity support
                if (Math.abs(deltaX) > o.swipeSensitivity && Math.abs(deltaX) > Math.abs(deltaY)) {
                    self._swipe(event,
                            Math.abs(deltaX) > o.swipeSensitivity && (o.swipeDirection == 'both' || o.swipeDirection == 'horizontal') ? Math.sgn(deltaX) : false,
                            Math.abs(deltaY) > o.swipeSensitivity && (o.swipeDirection == 'both' || o.swipeDirection == 'vertical') ? Math.sgn(deltaY) : false);
                    event.preventDefault();
                }
            }
        };
    },
    _swipe: function(event, horizontal, vertical) {
        this._trigger('swipe', event, {
            widget: this.widget(),
            horizontal: horizontal,
            vertical: vertical
        });
    }
});

})(jQuery);