;
(function($, undefined) {

	'use strict';

	function requires_flash_message(context) {
		var noscript = $('.flash_disabled', context);
		if (noscript.size() > 0) {
			noscript.show();
		} else {
			if (window.global_get && global_get('lang') == 'fr') {
				$(context).html('<div class="flash_disabled"><div class="flash_logo"><p class="message"><span class="fnat">Vous devez mettre à jour Adobe Flash Player pour voir ce contenu.</span></p><p><a class="btn primary" href="http://get.adobe.com/flashplayer/" rel="external" target="_blank"><span>Téléchargez-le sur le site d’Adobe</span></a></p></div></div>');
			} else {
				$(context).html('<div class="flash_disabled"><div class="flash_logo"><p class="message"><span class="fnat">You need to upgrade your Adobe Flash Player to view this content.</span></p><p><a class="btn primary" href="http://get.adobe.com/flashplayer/" rel="external" target="_blank"><span>Download it from Adobe</span></a></p></div></div>');
			}
			$('.flash_disabled', context).show();
			if (window.cufon_refresh_replace) {
				cufon_refresh_replace(context);
			}
		}
	}

	;

	function __defer__(callback) {
		setTimeout(callback, 50);
	}

	function __once__(callback) {
		var run = false;
		return function() {
			if (run) {
				return;
			}

			callback();
			run = true;
		};
	}

	var console = window.console || {
		log: function() {
		}
	};


	function translate3D(node, o, callback) {
		o.x = o.x || 0;
		o.y = o.y || 0;
		o.z = o.z || 0;
		o.duration = o.duration || 0;
		if (callback) {
			$(node).one('webkitTransitionEnd mozTransitionEnd oTransitionEnd msTransitionEnd transitionend', function() {
				callback();
			});
		}
		var prefixes = ['webkit', 'moz', 'ms', 'o'];
		$.each(['webkitTransform', 'mozTransform', 'msTransform', 'oTransform', 'transform'], function(idx, prop) {
			var x = o.x == 0 ? '0' : o.x + 'px', y = o.y == 0 ? '0' : o.y + 'px', z = o.z == 0 ? '0' : o.z + 'px';
			node.style[prop] = 'translate3d(' + x + ',' + y + ',' + z + ')';
		});
		if (o.duration) {
			node.style.webkitTransitionProperty = '-webkit-transform';
			node.style.mozTransitionProperty = '-moz-transform';
			node.style.oTransitionProperty = '-o-transform';
			node.style.msTransitionProperty = '-ms-transform';
			node.style.transitionProperty = 'transform';
			$.each(['webkitTransitionDuration', 'mozTransitionDuration', 'msTransitionDuration', 'oTransitionDuration', 'transitionDuration'], function(idx, prop) {
				node.style[prop] = o.duration + 'ms';
			});
		} else {
			$.each(['webkitTransitionDuration', 'mozTransitionDuration', 'msTransitionDuration', 'oTransitionDuration', 'transitionDuration'], function(idx, prop) {
				node.style[prop] = '';
			});
		}
	}

	function Has3DSupport() {
		var sTranslate3D = "translate3d(0px, 0px, 0px)";

		var eTemp = document.createElement("div");

		eTemp.style.cssText = "-ms-transform:" + sTranslate3D +
			"; -o-transform:" + sTranslate3D +
			"; -webkit-transform:" + sTranslate3D +
			"; transform:" + sTranslate3D;
		var rxTranslate = /translate3d\(0px, 0px, 0px\)/g;
		var asSupport = eTemp.style.cssText.match(rxTranslate);
		var bHasSupport = (asSupport !== null && asSupport.length == 1);

		return bHasSupport;
	}

// Has3DSupport
	var _has3d = Has3DSupport();

	$.widget('commonlib.toutrotator', {
		widgetEventPrefix: 'toutrotator',
		options: {
			flashPreloaderPrefix: null,
			stage: null,
			controller: null,
			touchSimulation: false,
			navigationController: null,
			width: null,
			height: null,
			duration: 6000, // default duration
			autoloop: true,
			transition: 'default', //not implemented yet
			alwaysShowControls: false, //true: show controls on tour with 1 item, false: show controls only when more touts
			preload: false,
			touchStageZIndex: 20,
			/// function with callback, or just an array of touts
			/// each tout item has a type and a URL or path which points to the widget
			/// { type: 'iframe', poster: '', url: 'path/to/widget', duration: 6000 } or
			/// { type: 'html', contenthtml: '', duration: 6000 } or
			/// { type: 'flash', poster: '', url: 'path/to/flash', duration: 6000, mode: 'transparent' }
			/// { type: 'image', url: 'path/to/image', link: 'path/to/link', target: '_blank', duration: 6000 }
			touts: function(callback) { callback(null, []); }
		},
		_create: function() {
			var self = this,
			    o = this.options,
			    widget = this.widget();

			o.stage = o.stage || widget;
			self._touts = o.touts;
			self._inTransition = false;

			// transition downgrade for IE
			//      if ($.browser.msie && /^[678]\./.test($.browser.version)) {
			//        o.transition = 'default';
			//      }

			this._bindEvents();
			this._trigger('create');
		},
		isInTransition: function() {
			return this._inTransition;
		},
		_bindEvents: function() {
			var self = this,
			    o = this.options,
			    widget = this.widget(),
			    stage = $(o.stage),
			    count = 0,
			    func = ['play', 'pause'];

			if ('ontouchend' in document || o.touchSimulation) {
				$('.toutrotator-progress-outer', widget).bind('click', function() {
					self[func[(count++) % 2]](); // play or pause depending on the count
					// should not overflow (unless somebody taps it for n_int64 times)
				});
				// check transition handler
				var transitions = $.commonlib.toutrotator.transitions, tkey = o.transition + '.touch';
				if (transitions[tkey]) {
					// bind transition events
					if ('ontouchend' in document) {
						$.each(['touchstart', 'touchmove', 'touchend', 'touchcancel'], function(index, evkey) {
							if (transitions[tkey][evkey]) {
								widget.bind(evkey, function(event) {
									transitions[tkey][evkey].call(self, event);
								});
							}
						});
					}
					if (o.touchSimulation && !('ontouchend' in document)) {
						if (transitions[tkey]['touchstart']) {
							widget.bind('mousedown', function(event) {
								if (event.which != 1) return;
								if (event.originalEvent) {
									event.originalEvent.touches = [
										{ pageX: event.pageX, pageY: event.pageY }
									];
								}
								transitions[tkey].touchstart.call(self, event);
							});
						}
						if (transitions[tkey]['touchmove']) {
							widget.bind('mousemove', function(event) {
								if (event.originalEvent) {
									event.originalEvent.touches = [
										{ pageX: event.pageX, pageY: event.pageY }
									];
								}
								transitions[tkey].touchmove.call(self, event);
							});
						}
						if (transitions[tkey]['touchend']) {
							widget.bind('mouseup', function(event) {
								if (event.originalEvent) {
									event.originalEvent.touches = [
										{ pageX: event.pageX, pageY: event.pageY }
									];
								}
								transitions[tkey].touchend.call(self, event);
							});
						}
						if (transitions[tkey]['touchcancel']) {
							widget.bind('mouseout', function(event) {
								if (event.originalEvent) {
									event.originalEvent.touches = [
										{ pageX: event.pageX, pageY: event.pageY }
									];
								}
								transitions[tkey].touchcancel.call(self, event);
							});
						}
					}
				}
			} else {
				widget.bind('mouseover mouseenter', function() {
					self.pause();
				}).bind('mouseout mouseleave', function() {
					self.play();
				});
			}
		},
		_requestAnimFrame: function(cb) {
			// shim layer for requestAnimationFrame, for convenience taken from
			// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
			// thanks Paul.
			var _requestAnimFrame = (function() {
				return window.requestAnimationFrame ||
					window.webkitRequestAnimationFrame ||
					window.mozRequestAnimationFrame ||
					window.oRequestAnimationFrame ||
					window.msRequestAnimationFrame ||
					function(callback) {
						window.setTimeout(callback, 1000 / 60);
					};
			})();
			return _requestAnimFrame(cb);
		},
		_setIndex: function(x) {
			this._prevIndex = this._currentIndex;
			this._currentIndex = x;
		},
		getIndex: function() {
			return this._currentIndex;
		},
		_setDirection: function(x) {
			this._currentDirection = x;
		},
		getDirection: function() {
			return this._currentDirection;
		},
		_bindController: function() {
			var self = this,
			    o = self.options,
			    widget = self.widget();
			// try to initialize the first widget for testing
			this._controller = o.navigationController || $.commonlib.toutrotator.controller;

			// bind the events with the controller
			if (o.controller) {
				$.each(['create', 'prev', 'next', 'seek', 'play', 'pause', 'load', 'progress', 'init'], function(index, event) {
					if (self._controller[event] && $.isFunction(self._controller[event])) {
						widget.bind(self.widgetEventPrefix + event, self._controller[event]);
					}
				});
			}
		},
		_preload: function() {
			var self = this,
			    o = this.options,
			    widget = this.widget(),
			    touts = this._touts,
			    transitions = $.commonlib.toutrotator.transitions,
			    tkey = o.transition + '.touch';

			if (!o.preload) return;

			// init all touts
			$(o.stage).hide();
			$.each(touts, function(index, tout) {
				self._initTout(tout);
			});
			$(o.stage).show();

			if ('ontouchend' in document && transitions[tkey] && transitions[tkey].touchinit) {
				transitions[tkey].touchinit.call(self);
			}

		},
		_init: function() {
			var self = this,
			    o = this.options,
			    widget = this.widget();
			var targetLength = 1;
			if (o.alwaysShowControls) targetLength = 0;

			// set the index
			self._setIndex(0);
			self._setDirection("");
			if (jQuery.isArray(o.touts)) {
				self._touts = o.touts;
				self._count = o.touts.length;
				if (self._touts.length > targetLength) {
					self._bindController();
				} else {
					$('.toutrotator-controls', widget).remove();
				}
				self._preload();
				self._trigger('init', null, {
					touts: o.touts,
					total: o.touts.length
				});
				self._load();
			} else if (jQuery.isFunction(o.touts)) {
				o.touts(function(err, touts) {
					if (err) {
						return;
					}
					self._touts = touts;
					self._count = touts.length;
					if (self._touts.length > targetLength) {
						self._bindController();
					} else {
						$('.toutrotator-controls', widget).remove();
						//$('.fc nav', widget).remove();
					}

					__defer__(function() {
						self._preload();
						self._trigger('init', null, {
							touts: touts,
							total: touts.length
						});
						self._seek();
						self._load();
					});
				});
			} else {
				throw new Error('Unknown format of touts.');
			}
		},
		_initTout: function(toutItem, callback, stage) {
			var self = this, o = this.options;
			if (toutItem.stage) {
				(function() {
					var s = stage || $(o.stage);
					s.empty().append(toutItem.stage.clone().children());
					if (callback) callback(null, toutItem);
				})();
			} else {
				self.getTout(toutItem, function(err, item) {
					if (err || !item) {
						// deal with error
						alert(err);
						return;
					}

					var cb = function(err, tout) {
						var s = stage || $(o.stage);
						toutItem.stage = s.clone().detach();
						if (callback) callback(err, tout);
					};

					switch (toutItem.type) {
					case 'flash':
						self._initToutWithFlashStage(item, cb, stage);
						break;
					case 'iframe':
						self._initToutWithIframeStage(item, cb, stage);
						break;
					case 'html':
						self._initToutWithHtmlStage(item, cb, stage);
						break;
					case 'image':
						self._initToutWithImageStage(item, cb, stage);
						break;
					}
				});
			}
		},
		_initToutWithIframeStage: function(tout, callback, aStage) {
			// create the new iframe
			var self = this,
			    o = this.options,
			    element = this.widget(),
			    stage = aStage || $(o.stage),
			    iframe = $('<iframe>');

			// set the width and height and styles of the iframe
			iframe.css({
				width: o.width,
				height: o.height,
				overflow: 'hidden',
				border: 'none'
			});
			iframe.attr({
				src: tout.url,
				frameborder: 'no',
				scrolling: 'no'
			});
			// append to iframe to element and hide it until it's ready
			stage.empty().append(iframe.hide());
			iframe.load(function() {
				iframe.show();
				// enforce the margin/padding of the iframe document
				try {
					var d = iframe[0].contentWindow.document;
					$('body', d).css({
						padding: 0,
						margin: 0
					});
				} catch(e) {
					console.log(e);
				}
				// the iframe shows up, run the callback
				if (callback) {
					callback(null, tout);
				}
			});
		},
		_initToutWithFlashStage: function(tout, callback, aStage) {
			// check flash support
			if (!jQuery.fn.flash) {
				throw new Error('jQuery.swfobject is required for Flash support.');
			}

			// create the new iframe
			var self = this,
			    o = this.options,
			    element = this.widget(),
			    stage = aStage || $(o.stage),
			    obj = $('<div>');

			obj.css({
				width: o.width,
				height: o.height,
				overflow: 'hidden',
				border: 'none'
			});

			var runCallback = __once__(callback);

			// initialize the flash
			stage.empty().append(obj);

			if ($.flash.available) {
				obj.flash({
					swf: o.flashPreloaderPrefix === null || tout.standalone ? tout.url : (o.flashPreloaderPrefix + tout.url),
					width: tout.width || o.width,
					height: tout.height || o.height,
					wmode: tout.wmode || o.wmode || 'transparent',
					bgcolor: tout.bgcolor || o.bgcolor || '#ffffff',
					align: tout.align || o.align || 't',
					salign: tout.salign || o.salign || 't'
				});

				// make sure the movie plays
				var movie = $('object', obj)[0];
				var playMovie = __once__(function() {
					runCallback();
					movie.Play();
					//console.log('play movie');
				});
				if (o.flashPreloaderPrefix !== null || tout.standalone) {
					try {
						playMovie();
					} catch(e) {
					}
				} else {
					var startTime = new Date().getTime();
					var checkMovieLoaded = setInterval(function() {
						var currentTime = new Date().getTime();
						if (currentTime - startTime > 10000) {
							clearInterval(checkMovieLoaded);
							return;
						}
						try {
							playMovie();
							clearInterval(checkMovieLoaded);
						} catch(e) {
						}
					}, 200);
				}
			} else {
				if (tout.fallback) {
					self._initTout(tout.fallback, callback);
				} else {
					requires_flash_message(obj);
					callback('flasherror', null);
				}
			}

		},
		_initToutWithHtmlStage: function(tout, callback, aStage) {
			var self = this,
			    o = this.options,
			    element = this.widget(),
			    stage = aStage || $(o.stage),
			    div = $('<div>');

			stage.empty().append(div.html(tout.htmlcontent));

			if (callback) {
				callback(null, tout);
			}
		},
		_initToutWithImageStage: function(tout, callback, aStage) {
			// create the new iframe
			var self = this,
			    o = this.options,
			    element = this.widget(),
			    stage = aStage || $(o.stage),
			    image = $('<img>');

			stage.empty().append(image.hide());

			// ie quirk: 'load' event should be bound before 'src' is changed.
			image.load(function() {
				image.show();
				if (callback) callback(null, tout);
			});

			// set the width and height of the image
			image.attr({
				width: o.width,
				height: o.height,
				src: tout.url
			}).css({
				display: 'block'
			});


			// if the link exists
			if (tout.link) {
				image.wrap($('<a>').attr({
						href: tout.link,
						target: tout.target
					}).css({
						display: 'block'
					})
				);
			}
		},
		getTout: function(tout, callback) {
			switch (tout.type) {
			case 'flash':
				callback(null, tout);
				break;
			case 'image':
				callback(null, tout);
				break;
			case 'html':
				callback(null, tout);
				break;
			case 'iframe':
				// load config.xml and return the preferences
				$.ajax({
					type: 'GET',
					url: tout.url + '/config.xml',
					dataType: 'xml',
					cache: false,
					global: false,
					success: function(data, status) {
						// feature object
						// var features = {};
						// $('feature param', data).each(function() {
						// 	var name = $(this).attr('name'),
						// 		value = $(this).attr('value');
						// 	features[name] = value;
						// });
						var preferences = { };
						$('preference', data).each(function() {
							var name = $(this).attr('name'),
							    value = $(this).attr('value'),
							    readonly = $(this).attr('readonly');
							preferences[name] = {
								name: name,
								value: value,
								readonly: readonly && readonly.toLowerCase() == 'true' ? true : false
							};
						});
						var icons = [];
						$('icon', data).each(function() {
							icons.push($(this).attr('src'));
						});

						var contentPath = $('content', data).attr('src') || 'index.html';

						var result = {
							url: tout.url + '/' + contentPath,
							name: $('name', data).text(),
							shortName: $('name', data).attr('short'),
							description: $('description', data).text(),
							license: $('license', data).text(),
							author: {
								name: $('author', data).text(),
								href: $('author', data).attr('href'),
								email: $('author', data).attr('email')
							},
							icons: icons,
							// features: features,
							preferences: preferences
						};

						callback(null, result);
					},
					error: function(xhr, err) {
						callback(err, null);
					}
				});
				break;
			}
		},
		_navigationLoop: function() {
			var self = this,
			    o = this.options,
			    element = this.widget(),
			    index = this.getIndex(),
			    duration = self._touts && self._touts[index] && self._touts[index].duration ? self._touts[index].duration : o.duration,
			    autoloop = o.autoloop;

			//do nothing if autoloop is set to false
			if (!autoloop) {
				return;
			}
			// we don't do anything if paused
			if (!self._isPlaying) {
				return;
			}

			// request the next frame
			self._requestAnimFrame(function() {
				self._navigationLoop();
			});

			// calculate the time elapsed
			var now = new Date().getTime();

			self._timeElapsed += now - self._lastCycle;
			self._lastCycle = now;

			// emit the progress
			self._progress(self._timeElapsed, duration);

			if (self._timeElapsed > duration) {
				// go to next tout
				self._isPlaying = false;
				self.next();
			}
		},
		play: function() {
			var self = this,
			    o = this.options,
			    element = this.widget(),
			    index = this.getIndex();

			self._isPlaying = true;
			self._lastCycle = new Date().getTime();

			self._requestAnimFrame(function() {
				self._navigationLoop();
			});

			self._play();
		},
		pause: function() {
			var self = this,
			    o = this.options,
			    element = this.widget(),
			    index = this.getIndex();

			this._isPlaying = false;
			self._pause();
		},
		seek: function(index) {
			if (this.getDirection() == "") {
				if (index < this.getIndex())
					this._setDirection("right");
				else if (index > this.getIndex())
					this._setDirection("left");
				else {
					return;
				}
			}
			var self = this,
			    o = this.options,
			    transitions = $.commonlib.toutrotator.transitions,
			    element = this.widget();

			this.pause();
			index = index % self._touts.length;
			this._setIndex(index);

			this._seek();

			var transition_func = transitions[o.transition] || transitions['default'];
			if ($.isFunction(transition_func)) {
				transition_func.call(this, function(cb) {
					__defer__(function() {
						self._load(cb);
					});
				});
			} else if (typeof transition_func == 'object') {
				if (!transition_func.begin || !transition_func.end) {
					throw new Error('Both begin() and end() have to be defined for a transition');
				}

				// if touch device do something else

				transition_func.begin.call(self, function(cb) {
					__defer__(function() {
						self._load(cb);
					});
				}, function(tempStage, translate, loadCallback) {
				    var args = Array.prototype.slice.apply(arguments);
				    transition_func.end.apply(self, args);
				});
			}
		},
		next: function() {
			var self = this,
			    o = this.options,
			    element = this.widget(),
			    index = this.getIndex(),
			    prevIndex = (index + self._touts.length - 1) % self._touts.length,
			    nextIndex = (index + 1) % self._touts.length;
			if (index == self._touts.length - 1) this._setDirection("left");
			else this._setDirection("");
			this.seek(nextIndex);
		},
		prev: function() {
			var self = this,
			    o = this.options,
			    element = this.widget(),
			    index = this.getIndex(),
			    prevIndex = (index + self._touts.length - 1) % self._touts.length,
			    nextIndex = (index + 1) % self._touts.length;
			if (index == 0) this._setDirection("right");
			else this._setDirection("");
			this.seek(prevIndex);
		},
		_load: function(cb) {

			var self = this,
			    o = this.options,
			    element = this.widget(),
			    touts = self._touts,
			    index = this.getIndex();

			self._timeElapsed = 0;

			self._initTout(touts[index], function() {
				self._trigger('load', null, {
					index: self.getIndex(),
					tout: self._touts[self.getIndex()]
				});
				//if (cb && $.isFunction(cb)) cb();
				if (cb) cb();
				self.play();
			});
		},
		_progress: function(elapsed, duration) {
			this._trigger('progress', null, {
				elapsed: elapsed,
				duration: duration,
				index: this.getIndex(),
				total: this._touts.length,
				tout: this._touts[this.getIndex()],
				percentage: Math.min(1, elapsed / duration)
			});
		},
		_seek: function() {
			this._trigger('seek', null, {
				index: this._touts ? this.getIndex() : -1,
				total: this._touts ? this._touts.length : -1,
				tout: this._touts ? this._touts[this.getIndex()] : null
			});
		},
		_transitionstart: function(ui) {
			this._inTransition = true;
			this._trigger('transitionstart', null, {
				index: this._touts ? this.getIndex() : -1,
				total: this._touts ? this._touts.length : -1,
				tout: this._touts ? this._touts[this.getIndex()] : null,
				tempStage: ui.tempStage
			});
		},
		_transitioncomplete: function() {
			this._inTransition = false;
			this._trigger('transitioncomplete', null, {
				index: this._touts ? this.getIndex() : -1,
				total: this._touts ? this._touts.length : -1,
				tout: this._touts ? this._touts[this.getIndex()] : null
			});
		},
		_next: function() {
			this._trigger('next', null, {
				index: this._touts ? this.getIndex() : -1,
				total: this._touts ? this._touts.length : -1,
				tout: this._touts ? this._touts[this.getIndex()] : null
			});
		},
		_prev: function() {
			this._trigger('prev', null, {
				index: this._touts ? this.getIndex() : -1,
				total: this._touts ? this._touts.length : -1,
				tout: this._touts ? this._touts[this.getIndex()] : null
			});
		},
		_play: function() {
			this._trigger('play', null, {
				index: this._touts ? this.getIndex() : -1,
				total: this._touts ? this._touts.length : -1,
				tout: this._touts ? this._touts[this.getIndex()] : null
			});
		},
		_pause: function() {
			this._trigger('pause', null, {
				index: this._touts ? this.getIndex() : -1,
				total: this._touts ? this._touts.length : -1,
				tout: this._touts ? this._touts[this.getIndex()] : null
			});
		}
	});


	$.commonlib.toutrotator.controller = {
		init: function(event, ui) {
			// construct controls
			var self = $(this),
			    controller = self.toutrotator('option', 'controller');
			controller.empty();
			var nav = $('<span>').addClass('toutrotator-nav-outer').append(
				$('<span>').addClass('toutrotator-nav').append(
					$('<span>').addClass('toutrotator-nav-inner')
				)
			).appendTo(controller);

			$.each(ui.touts, function(index, tout) {
				var item = $("<span>").addClass('toutrotator-nav-item').append(
					$('<span>').addClass('toutrotator-nav-item-inner')
				);

				item.appendTo($('.toutrotator-nav-inner', nav));

				item.click(function(event) {
					event.preventDefault();
					self.toutrotator('seek', index);
				});
			});

			var progressBar = $('<span>').addClass('toutrotator-progress-outer').append(
				$('<span>').addClass('toutrotator-progress').append(
					$('<span>').addClass('toutrotator-progress-inner')
				)
			).appendTo(controller);

		},
		seek: function(event, ui) {
			var self = $(this),
			    controller = self.toutrotator('option', 'controller');

			$('.toutrotator-nav .toutrotator-nav-item', controller).removeClass('toutrotator-nav-item-active');
			$('.toutrotator-nav .toutrotator-nav-item', controller).eq(ui.index).addClass('toutrotator-nav-item-active');

		},
		progress: function(event, ui) {
			var self = $(this),
			    controller = self.toutrotator('option', 'controller'),
			    width = $(".toutrotator-progress", controller).innerWidth();

			width = width * ui.percentage;

			$('.toutrotator-progress-inner', controller).width(width);
		},
		load: function(event, ui) {
		}
	};


	$.commonlib.toutrotator.transitions = {
		'default': function(loadCallback) {
			loadCallback();
		},
		'fade': function(loadCallback) {
			var self = this,
			    o = this.options,
			    stage = $(o.stage),
			    vstage = stage.clone(),
			    astage = stage.clone().empty(),
			    index = this.getIndex();

			this._initTout(self._touts[index], null, astage);
			this._transitionstart();
			// prepare
			stage.hide();
			vstage.css({
				position: 'absolute',
				opacity: 1
			});
			stage.after(vstage);
			astage.css({
				position: 'absolute',
				opacity: 0
			});
			// begins
			vstage.animate({
				opacity: 0
			});
			astage.animate({
					opacity: 1
				}, function() {
					// clean up
					loadCallback(function() {
						stage.show();
						vstage.remove();
						astage.remove();
						self._transitioncomplete();
					});
				});
		},
		'slide.touch': {
			touchinit: function() {
				var self = this,
				    o = this.options,
				    touts = self._touts;

				// init all touch stages
				$.each(touts, function(index, tout) {
					var stage = $(o.stage);
					tout.touchStage = $('<div>').hide();
					tout.touchStage[0].style.webkitUserSelect = 'none';
					translate3D(tout.touchStage[0], { x: -o.width });
					tout.touchStage.width(o.width * 3 + 5).height(o.height).css({
						'z-index': o.touchStageZIndex,
						position: 'absolute',
						top: 0,
						left: 0
					});

					// initialize the touch stage
					var prevIndex = (index - 1 + self._touts.length) % self._touts.length,
					    nextIndex = (index + 1) % self._touts.length,
					    curStage = stage.clone().empty(),
					    prevStage = stage.clone().empty(),
					    nextStage = stage.clone().empty();

					self._initTout(touts[index], function() {
						self._initTout(self._touts[prevIndex], function() {
							self._initTout(self._touts[nextIndex], function() {
								curStage.css({ 'float': 'left', 'position': 'relative' });
								prevStage.css({ 'float': 'left', 'position': 'relative' });
								nextStage.css({ 'float': 'left', 'position': 'relative' });
								// the order is very important here.
								tout.touchStage.append(prevStage)
									.append(curStage)
									.append(nextStage)
									.disableSelection();
								stage.after(tout.touchStage);
							}, nextStage);
						}, prevStage);
					}, curStage);
				});
			},
			touchstart: function(event) {
				if (this.isInTransition()) return;
				if (this._inTouchTransition) return;

				var self = this,
				    o = this.options,
				    stage = $(o.stage),
				    index = this.getIndex(),
				    touts = self._touts,
				    touch = this._touch || { startX: 0, startY: 0, x: 0, y: 0, ended: false };

				if (self._touts.length <= 1) return;
				if (o.controller[0] !== event.target) return;


				this._touch = touch;

				touch.x = event.originalEvent.touches[0].pageX;
				touch.y = event.originalEvent.touches[0].pageY;
				touch.startX = touch.x;
				touch.startY = touch.y;
				touch.startDate = new Date();

				// if cached
				if (touts[index].touchStage) {
					touch.stage = touts[index].touchStage;
					touch.stage.show();
//          stage.hide();
					translate3D(touch.stage[0], { x: -o.width });
					self._transitionstart({ tempStage: touch.stage });
				} else {
					// non cached

					touch.stage = $('<div>');

					var zIndex = parseInt(stage.css('z-index')) || 0;
					touch.stage[0].style.webkitUserSelect = 'none';
					translate3D(touch.stage[0], { x: -o.width });
					touch.stage.width(o.width * 3 + 5).height(o.height).css({
						'z-index': o.touchStageZIndex,
						position: 'absolute',
						top: 0,
						left: 0
					});

					// initialize the touch stage
					var prevIndex = (index - 1 + self._touts.length) % self._touts.length,
					    nextIndex = (index + 1) % self._touts.length,
					    curStage = stage.clone().show(),
					    prevStage = stage.clone().empty(),
					    nextStage = stage.clone().empty();

					self._initTout(self._touts[prevIndex], function(err, tout) {
						self._initTout(self._touts[nextIndex], function(err2, tout2) {
							curStage.css({ 'float': 'left', 'position': 'relative' });
							prevStage.css({ 'float': 'left', 'position': 'relative' });
							nextStage.css({ 'float': 'left', 'position': 'relative' });
							// the order is very important here.
							touch.stage.append(prevStage)
								.append(curStage)
								.append(nextStage)
								.disableSelection();
							stage.after(touch.stage);
							touts[index].touchStage = touch.stage; // cache the stage
							self._transitionstart({ tempStage: touch.stage });
						}, nextStage);
					}, prevStage);
				}
			},
			touchmove: function(event) {
				if (this._inTouchTransition) return;

				var self = this,
				    touch = this._touch,
				    o = this.options;

				if (!touch || !touch.stage || touch.ended) return;

				try {
					touch.x = event.originalEvent.touches[0].pageX;
					touch.y = event.originalEvent.touches[0].pageY;

					var translate = -o.width + touch.x - touch.startX;
					// touch.stage.css({
					//    startX: (-o.width + touch.x - touch.startX) + 'px'
					//  });
					translate3D(touch.stage[0], { x: translate });
				} catch(e) {
					alert(e);
				}
			},
			touchend: function(event) {
				if (this._inTouchTransition) return;

				var self = this,
				    touch = this._touch,
				    o = this.options,
				    stage = $(o.stage);

				if (!touch || !touch.stage) return;

				function clearStage() {
					var duration = 1000 * Math.abs(touch.x - touch.startX) / o.width * 0.618;
					self._inTouchTransition = true;
					if (duration) {
						translate3D(touch.stage[0], { x: -o.width, duration: duration }, function(event) {
							$(o.stage).show();
							touch.stage.hide();
							self._transitioncomplete();
							self._touch = null;
							self._inTouchTransition = false;
						});
					} else {
						translate3D(touch.stage[0], { x: -o.width });
						$(o.stage).show();
						touch.stage.hide();
						self._transitioncomplete();
						self._touch = null;
						self._inTouchTransition = false;
					}
				}

				touch.ended = true;

				var index = this.getIndex(),
				    endDate = new Date(),
				    offset = (endDate - touch.startDate) / 1000,
				    speed = offset < 0.00001 ? 10000 : Math.abs(touch.x - touch.startX) / offset;

				// velocity is greater than 512px/s or displacement is greater than 1/3 of the width
				if (speed > 512 || Math.abs(touch.x - touch.startX) > o.width / 3) {

					var translate = 0;

					if (touch.x > touch.startX) {
						translate = 0;
						index = (index - 1 + self._touts.length) % self._touts.length;
					} else if (touch.x < touch.startX) {
						translate = -2 * o.width;
						index = (index + 1) % self._touts.length;
					} else {
						clearStage();
						return;
					}

					var duration = 1000 - 1000 * Math.abs(touch.x - touch.startX) / o.width;
					event.preventDefault();
					self._setIndex(index);
					self._seek();
					self._inTouchTransition = true;
					translate3D(touch.stage[0], { x: translate, duration: duration }, function(event) {
						self._load(function() {
							$(o.stage).show();
							touch.stage.hide();
							self._transitioncomplete();
							self._touch = null;
							self._inTouchTransition = false;
						});
					});
				} else {
					clearStage();
				}
			},
			touchcancel: function(event) {
				if (this.isInTransition()) return;

				var self = this,
				    touch = this._touch,
				    o = this.options,
				    stage = $(o.stage);

				if (touch) {
					stage.show();
					if (touch.stage) touch.stage.hide();
					self._transitioncomplete();
					this._touch = null;
				}
			}
		},
		'slide': {
			begin: function(loadCallback, cb) {
				var self = this,
				    o = this.options,
				    stage = $(o.stage),
				    vstage = stage.clone(),
				    astage = stage.clone().empty(),
				    tempStage = $('<div>'),
				    index = this.getIndex(),
				    translate;


				function prepare() {
				    // prepare
				    stage.hide();
					stage.after(tempStage);
					vstage.css({ 'float': 'left', position: 'relative' });
					astage.css({ 'float': 'left', position: 'relative' });
					tempStage.css({ position: 'absolute' });
				}

				function work() {
				    // calculate the width of the tempstage
				    
				    if (self.getDirection() == "left") {
						translate = -o.width;
						tempStage.append(vstage).append(astage);
						if (_has3d) {
							translate3D(tempStage[0], { x: 0 });
						} else {
							tempStage.css({
								translateX: 0
							});
						}
					} else {
						translate = 0;
						tempStage.append(astage).append(vstage);
						if (_has3d) {
							translate3D(tempStage[0], { x: -o.width });
						} else {
							tempStage.css({
								translateX: (-o.width) + 'px'
							});
						}
					}

					tempStage.width(o.width * 3 + 5);
					self._transitionstart({ tempStage: tempStage });
					if ($.isFunction(cb)) cb(tempStage, translate, loadCallback);
				    
				}

				self._initTout(self._touts[index], function(err, tout) {
					prepare();
					work();
				}, astage);
			},
			end: function(tempStage, translate, loadCallback) {
				var self = this,
				    o = this.options,
				    stage = $(o.stage);

                if (_has3d) {
					__defer__(function() {
						translate3D(tempStage[0], { x: translate, duration: 1000 }, function(event) {
							// clean up & load the stage
							loadCallback(function() {
								stage.show();
								__defer__(function() {
									tempStage.remove();
									self._transitioncomplete();
								});
							});
						});
					});
				} else {
				    //this is instead of translateX animation - translateX is erroring with jquery 1.8.3 
                    if (this.getDirection() == "right") {
				        tempStage.animate({
				                left: this.options.width + 'px'
				            }, {
				                duration: 1000,
				                complete: function() {
				                    loadCallback(function() {
				                        stage.show();
				                        __defer__(function() {
				                            tempStage.remove();
				                            self._transitioncomplete();
				                        });
				                    });
				                }
				            });
				    } else {
				        tempStage.animate({
				            left: translate + 'px'
				        }, {
				            duration: 1000,
				            complete: function () {
				                loadCallback(function () {
				                    stage.show();
				                    __defer__(function () {
				                        tempStage.remove();
				                        self._transitioncomplete();
				                    });
				                });
				            }
				        });
				    }
				        
				    //tempStage.animate({
				    //		translateX: translate + 'px'
				    //	}, {
				    //		duration: 1000,
				    //		complete: function () {
				    //		    console.log('complete');
				    //			loadCallback(function() {
				    //				stage.show();
				    //				__defer__(function() {
				    //					tempStage.remove();
				    //					self._transitioncomplete();
				    //				});
				    //			});
				    //		}
				    //	});
				}
				self._setDirection("");
			}
		}
	};

}.call(this, jQuery));