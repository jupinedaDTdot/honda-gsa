;(function($, W, D, undefined) {

var _defaults = {
    form: 'form',
    name: undefined,
    button: '[data-role="ajaxform-button"]',
    buttonEvent: 'click',
    validate: function() { return true },
    success: function(event, ui){},
    error: function(event, ui){},
    formsubmit: function(event, ui) {}
};

$.widget('commonlib.ajaxform', {
    widgetEventPrefix: 'ajaxform',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.ajaxform.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        var self = this,
            o = this.options,
            w = this.widget();

        self._ensureOptions();

        // bind button events
        if (o.button) {
            $(o.button, o.form).bind(o.buttonEvent, function(event) {
                var button = this;
                
                try {
                    // tinkering the form name
                    $('input[name="formName"]', o.form).remove();
                    $(o.form).append(
                        $('<input>').attr({
                            type: 'hidden',
                            name: 'formName',
                            value: $(this).attr('name')
                        })
                    );

                    // submit the form
                    if (o.validate.apply(this, [o.form])) {

                        self._formsubmit(event);

                        $.ajax({
                            url: $(o.form).attr('action') || D.location.href,
                            global: false,
                            cache: false,
                            type: $(o.form).attr('method') || 'POST',
                            data: $(o.form).serialize(),
                            success: function(data, status, xhr) {
                                self._success(event, data, status, xhr);
                            },
                            error: function(xhr, err) {
                                self._error(event, xhr, err);
                            }
                        });
                    }
                } catch(e) {
                }

                event.preventDefault();
            });
        }
    },
    _ensureOptions: function() {
        var self = this,
            o = this.options,
            w = this.widget();

        o.name = w.data('ajaxform-name') || o.name;
        o.button = w.data('ajaxform-button') || o.button;
        o.buttonEvent = w.data('ajaxform-button-event') || o.buttonEvent;
    },
    _success: function(event, data, status, xhr) {
        this._trigger('success', event, {
            data: data,
            status: status,
            xhr: xhr,
            widget: this.widget()
        });
    },
    _error: function(event, xhr, err) {
        this._trigger('error', event, {
            xhr: xhr,
            err: err,
            widget: this.widget()
        });
    },
    _formsubmit: function(event) {
        this._trigger('formsubmit', event, {
            widget: this.widget(),
            form: $(this.options.form)
        });
    }
});  

})(jQuery, window, document);