/* $Id$ */
/*
* scroll down to the bottom to see what options can be sent in.
*/
;(function($, M, D, F, require, undefined){

var _defaults = {
    immediate: false,
	additionalClass: '',
	top: 0, // offset related to the button
	left: 0,
	flexibleWidth: false,
	relation: 'relative',
	contentTarget: false, // dom element or jquery object of the container content element
	debug: false, // if set true, the dropdown would keep open
	handler: 'click', // click, rollover, or a function($container, $btn) to handle the events.
					  // the handler does not implement onshow and onhide events.
	multiple: false, // if set false, showing this dropdown would close all others
    sibling: false, // put the dropdown container as the next sbiling of the button (to solve relative position-ed bugs)
					// normally should be false

    show: function(event, ui) {
        ui.frame.show();
    },
    hide: function(event, ui) {
        ui.frame.hide();
    },
	body: true // append to body directly
};

require(['ui/jquery.ui.position'], function() {

var dropdownWidgets = [];

$.widget('commonlib.dropdown', $.commonlib.widget, {
    widgetEventPrefix: 'dropdown',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.dropdown.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _attachClick: function() {
        var self = this,
            widget = this.widget(),
            frame = self.frameContainer;

        // when the widget is clicked
        widget.one('click', function(event) {
            self.show(event);
            event.stopPropagation();
            event.preventDefault();
        });

    },
    _attachClickDelayed: function() {
        var self = this,
            widget = this.widget(),
            frame = self.frameContainer;

        // when the widget is clicked
        widget.click(function(event) {
            if (frame.is(':hidden')) {
                self.show(event);
            }
            else {
                self.hide(event);
            }
            event.stopPropagation();
            event.preventDefault();
        });

        // stop event propagation in the container
        frame.click(function(event) {
            event.stopPropagation();
        });

        // allow hidedropdown
        $('.dropdown_button_right .dd_button', frame).click(function(ev) {
            // ev.stopPropagation();
            self.hide(ev);
            ev.preventDefault();
        });

        // allow clicks of anchors within the frame
        $('a', frame).click(function(ev){
            ev.stopPropagation();
        });

        // when the document is clicked
        $(D).click(function(ev) {
            self.hide(ev);
        });
    },
    _attachRollover: function() {
        var self = this,
            widget = this.widget(),
            frame = self.frameContainer;

        // attach events
        widget.one('mouseenter', function(ev) {
            self.show(ev);
        });
    },
    _attachRolloverDelayed: function() {
        var self = this,
            widget = this.widget(),
            frame = self.frameContainer;

        widget.mouseenter(function(ev) {
            self.show(ev);
        });

        frame.mouseleave(function(ev) {
            self.hide(ev);
        });
    },
    _attachTouch: function() {
        var self = this,
            widget = this.widget(),
            frame = self.frameContainer;

        // when the widget is clicked
        widget.one('touchstart', function(event) {
            self.show(event);
            event.stopPropagation();
            event.preventDefault();
        });
    },
    _attachTouchDelayed: function() {
        var self = this,
            widget = this.widget(),
            frame = self.frameContainer;

        // when the widget is clicked
        widget.bind('touchstart', function(event) {
            if (frame.is(':hidden')) {
                self.show(event);
            }
            else {
                self.hide(event);
            }
            event.stopPropagation();
            event.preventDefault();
        });

        // stop event propagation in the container
        frame.bind('touchstart', function(event) {
            event.stopPropagation();
            event.preventDefault();
        });

        // allow clicks of anchors within the frame
        $('a', frame).bind('touchstart', function(event){
            event.stopPropagation();
        });

        // allow hidedropdown
        $('.dropdown_button_right .dd_button', frame).bind('touchstart', function(event) {
            self.hide();
            event.preventDefault();
        });
    },
    _attachCustom: function() {
    },
    _attachCustomDelayed: function() {
    },
    _rePosition: function() {
        var self = this,
            o = this.options,
            frame = this.frameContainer,
            width = o['width'] || $('.dropdown_button', frame).outerWidth(true);
            
        frame.position(o.position);
        
        // reset frame width to button-width if it's flexible
        if (o.flexibleWidth) {
        	frame.width(width);
        }
    },
    _attachEventHandlers: function() {
        var self = this,
            o = this.options,
            method = o.immediate ? ('_attach' + o.handler.toTitleCase() + 'Delayed') : ('_attach' + o.handler.toTitleCase());

        self[method]();
    },
    _discoverHandler: function() {
        var self = this,
            o = this.options,
            widget = this.widget();

        if (o.handler != 'custom') {
            if (M.touch) {
                self.showEvent = 'touchstart';
            }
            else {
                if (o.handler === 'rollover') {
                    self.showEvent = 'mouseenter';
                }
                else {
                    self.showEvent = 'click';
                }
            }
        }
        else {
            self.showEvent = 'dropdownshow';
            self.hideEvent = 'dropdownhide';
        }
    },
    show: function(event) {
        var self = this,
            o = this.options,
            widget = this.widget();

        if (self.frameContainer === null) {
            self._createDelayed();
        }

        self._ensureMultipleState();
        self._ensurePosition();

        var ui = {
            widget: widget,
            frame: this.frameContainer,
            placeholder: self.placeholder,
            call_Notify: function() {
                self._rePosition();
            }
        };

        self._trigger('show', event, ui);

        self._rePosition();
        self._rePosition(); // positioning fix for a couple browsers including IEs

    },
    updateFrame: function(callback) {
        callback.apply(this, [this.frameContainer]);
    },
    hide: function(event) {
        var self = this;
        
        if (this.frameContainer === null) {
            return;
        }
        this._trigger('hide', event, {
            widget: this.widget(),
            frame: this.frameContainer,
            placeholder: self.placeholder,
            call_Notify: function() {
                self._rePosition();
            }
        });
    },
    _complete: function(event) {
        var self = this,
            o = this.options,
            method = '_attach' + o.handler.toTitleCase() + 'Delayed';

        if (self[method] && !o.immediate) {
            self[method]();
        }

        var reposition = function() {
            self._rePosition();
        };

        // hook up to window resize
        $(window).resize((function() {
            var timeout = null;
            return function() {
                if (timeout) clearTimeout(timeout);
                timeout = setTimeout(reposition, 250);
            };
        })());

//        self.frameContainer.css('z-index', o.containerZIndex);
        self._trigger('complete', event, {
            widget: self.widget(),
            frame: self.frameContainer,
            placeholder: self.placeholder,
            call_Notify: function() {
                self._rePosition();
            }
        });
    },
    destroy: function() {
        if (dropdownWidgets) {
            var index = dropdownWidgets.indexOf(this);
            if (index >= 0) {
                dropdownWidgets.splice(index, 1);
            }
        }
        $.Widget.prototype.destroy.apply(this, arguments); // default destroy
    },
    _ensureMultipleState: function() {
        var self = this,
            o = this.options;

        if (o.multiple) {
            return;
        }

        $.each(dropdownWidgets, function(index, dd) {
            if (dd !== self) {
                dd.hide();
            }
        });
    },
    _create: function() {
        var self = this,
            o = this.options,
            widget = this.widget();

        // we don't have a frame container yet.
        self.frameContainer = null;
        self._ensurePosition();

        // enforce position collision to none
        o.position.collision = 'none';
        o.contentTarget = widget.data('dropdown-content-target') || o.contentTarget;
        o.additionalClass = widget.data('dropdownClass') ? (widget.data('dropdownClass') + ' ' + o.additionalClass) : o.additionalClass;

        self._discoverHandler();

        if (o.immediate) {
            self._createDelayed();
        }

        self._attachEventHandlers();

        dropdownWidgets.push(self);
    },
    _createDelayed: function() {
        /** variables */
        var self = this,
            o = this.options,
            widget = this.widget(),
            widgetCopy = widget.clone().addClass('dropdown-widget-clone dd_button');

        var $outer, $container, $content = $(o.contentTarget).clone();

        if (o.sibling) {
            // wrap the button with a parent element
            var $parent = $('<span>');

            if (widget.css('display') == 'inline' || widget.css('display') == 'inline-block') {
                $parent.css('display', 'inline-block');
            }
            else {
                $parent.css('display', 'block');
            }
            widget.wrap($parent);

            $outer = widget.parent();
        }
        else if (o.body) {
            $outer = $('body');
        }
        else {
            $outer = $('#dropdown_outer').size() > 0
                ? $('#dropdown_outer') : $('<div>')
                    .attr('id', 'dropdown_outer')
                    .css({
                        top: 0,
                        left: 0,
                        width: 0,
                        height: 0
                    })
                    .prependTo('body');
        }

        /** end */

        /** initializations */

        // if width is supplied
        if (o.flexibleWidth) {
            $container = $('<div>').addClass('dropdown_container pf').addClass(o.additionalClass)
                .append(
                    $('<div>').addClass('dropdown_button pf').append(
                        $('<span>').addClass('dropdown_button_right pf').append(
                            widgetCopy
                        )
                    )
                ).append(
                    $('<div>').addClass('dropdown_content pf').append(
                        $('<div>').addClass('dropdown_content_top_l pf').append(
                            $('<div>').addClass('dropdown_content_top_r pf')
                        )
                    ).append(
                        $('<div>').addClass('dropdown_content_mid_l pf').append(                            
                            $('<div>').addClass('dropdown_content_mid_r pf').append(                           
                                $content.children()
                            )
                        )
                    ).append(
                        $('<div>').addClass('dropdown_content_bottom_l pf').append(
                            $('<div>').addClass('dropdown_content_bottom_r pf')
                        )
                    )
                ).append(
                    $('<div>').addClass('clrfix')
                )
                .appendTo($outer).hide();
        }
        else {
            // create the container
            $container = $('<div>').addClass('dropdown_container pf').addClass(o.additionalClass)
                .append(
                    $('<div>').addClass('dropdown_button pf').append(
                        $('<span>').addClass('dropdown_button_right pf').append(
                            widgetCopy
                        )
                    )
                ).append(
                    $('<div>').addClass('dropdown_content pf').append(
                        $('<div>').addClass('dropdown_content_top pf')
                    ).append(
                        $('<div>').addClass('dropdown_content_mid pf').append(
                            $content.children()
                        )
                    ).append(
                        $('<div>').addClass('dropdown_content_bottom pf')
                    )
                )
                .appendTo($outer).hide();
                // .insertAfter($btn).hide();
        }

        self.frameContainer = $container;
        self.placeholder = widgetCopy;
        // complete
        self._complete();
    }
});  

});

})(jQuery, Modernizr, document, window, require);