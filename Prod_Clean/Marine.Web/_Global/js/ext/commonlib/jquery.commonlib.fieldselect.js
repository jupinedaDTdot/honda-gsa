/* $Id: jquery.commonlib.fieldselect.js 15528 2011-08-04 14:49:21Z yuxhuang $ */
;(function($, D, W, M, F, require, undefined) {

var _defaults = {
    additionalClass: '',
    hoverClass: '',
    flexibleWidth: false,
    contentTarget: null
};

require(['commonlib/jquery.commonlib.dropdown'], function() {


var fieldSelectWidgets = [];

$.widget('commonlib.fieldselect',  $.commonlib.widget, {
    widgetEventPrefix: 'fieldselect',
    options: $.extend(true, {}, _defaults, (function(){
		try {
			return F.global_get('commonlib.fieldselect.defaults');
		}
		catch(e) {
			return {};
		}
	})()),
    _create: function() {
        var self = this,
            o = this.options,
            widget = this.widget(),
            id = 'select_' + Math.round(Math.random() * 10000),
            paneClass = widget.data('fieldselect-pane-class'),
            buttonClass = widget.data('fieldselect-button-class'),
            contentTarget = widget.data('fieldselect-content-target') || o.contentTarget;

        o.hoverClass = widget.data('fieldselect-button-hover-class') || o.hoverClass;
        o.flexibleWidth = widget.data('fieldselect-flexible-width') || o.flexibleWidth;

        // we have to be sure the widget is a select box
        if (!widget.is('select')) {
            self.destroy();
        }

        var choose = widget.find('option[value=""]').text();

        // create the anchor
        var anchor = $('<a>');
        anchor.addClass(o.additionalClass).addClass(buttonClass).addClass('field-select-wrapper pf').append(
            $('<span>').addClass('field-select-inner pf').text(choose)
        );

        // create the select drop down content
        var dl = $('<dl>').append($('<dt>').text(choose));
		$('option', widget).each(function() {
			if ($(this).attr('value') == '') {
				return;
			}
			dl.append($('<dd>').append(
				$('<a>').text($(this).text()).attr({
					'data-value': $(this).attr('value'),
					href: '#'
				})
			));
		});
		dl.find('dd:first').addClass('first');
		dl.find('dd:last').addClass('last');

        // create the select dropdown div
		var div = $('<div>').attr({
			id: id
		}).append(dl).hide();
		widget.hide().after(div).after(anchor);

		var addClass = 'field-select-list';

        // create the select dropdown style
        anchor.dropdown({
			additionalClass: addClass + ' ' + paneClass,
            contentTarget: '#' + id,
            flexibleWidth: o.flexibleWidth,
            position: {
                my: 'left top',
                at: 'left top',
                of: anchor
            },
			complete: function(event, ui) {
                self.pane = ui.frame;
                self.placeholder = ui.placeholder;

				$('dt', ui.frame).click(function(ev) {
					ui.widget.dropdown('hide', ev);
				});

				$('a', ui.frame).click(function(ev) {
					widget.val($(this).attr('data-value'));
                    if (contentTarget) {
                        $(contentTarget).text($(this).text());
                    }
                    else {
                        ui.widget.find('.field-select-inner').text($(this).text());
                        ui.placeholder.find('.field-select-inner').text($(this).text());
                    }
                    ui.widget.dropdown('hide', ev);
                    self._change(ev);
                    widget.change();
					ev.preventDefault();
				});

                F.forBrowser('msie', function() {
                    self.placeholder.mouseenter(function() {
                        $(this).addClass(o.hoverClass).addClass('field-select-wrapper-hover');
                    }).mouseleave(function() {
                        if (!self.isVisible) {
                            $(this).removeClass(o.hoverClass).removeClass('field-select-wrapper-hover');
                        }
                    });
                });

			}
		}).bind('dropdownshow', function(event, ui) {
            self._show(event);
        }).bind('dropdownhide', function(event, ui) {
            self._hide(event);
        });

        self.button = anchor;

        F.forBrowser('msie', function() {
            self.button.mouseenter(function() {
                $(this).addClass(o.hoverClass).addClass('field-select-wrapper-hover');
            }).mouseleave(function() {
                if (!self.isVisible) {
                    $(this).removeClass(o.hoverClass).removeClass('field-select-wrapper-hover');
                }
            });
        });

        self._complete();

    },
    destroy: function() {
        var index = fieldSelectWidgets.indexOf(this);
        if (index >= 0) {
            fieldSelectWidgets.splice(index, 1);
        }
        $.Widget.prototype.destroy.apply(this, arguments); // default destroy
    },
    _complete: function(event) {
        this._trigger('complete', event, {
            widget: this.widget(),
            button: this.button,
            placeholder: this.placeholder,
            pane: this.pane
        });
    },
    _show: function(event) {
        this.isVisible = true;

        var o = this.options;
        this.button.addClass(o.hoverClass).addClass('field-select-wrapper-hover');
        this.placeholder.addClass(o.hoverClass).addClass('field-select-wrapper-hover');
        this._trigger('show', event, {
            widget: this.widget(),
            button: this.button,
            placeholder: this.placeholder,
            pane: this.pane
        });
    },
    _hide: function(event) {
        var o = this.options;
        this.button.removeClass(o.hoverClass).removeClass('field-select-wrapper-hover');
        this.placeholder.removeClass(o.hoverClass).removeClass('field-select-wrapper-hover');

        this._trigger('hide', event, {
            widget: this.widget(),
            button: this.button,
            placeholder: this.placeholder,
            pane: this.pane
        });

        this.isVisible = false;
    },
    _change: function(event) {
        this._trigger('change', event, {
            widget: this.widget(),
            button: this.button,
            placeholder: this.placeholder,
            pane: this.pane
        });
    }
});

});
})(jQuery, document, window, Modernizr, window, require);