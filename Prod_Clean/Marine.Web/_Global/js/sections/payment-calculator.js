﻿
window.page_init = function () {

    var soon_text = global_get('lang') == 'en' ? 'COMING SOON<sup>MSRP</sup>' : 'ARRIVERA BIENTÔT<sup>PDSF</sup>';

    function _show(model) {
        $('.select-lists').nextAll().hide();

        var div = $('<div>').addClass('clr custom-hybrid-page').append(
            $('<div>').addClass('hybrid-header-box').append(
                $('<div>').addClass('hybrid-header').text(model)
                ).append(
                $('<div>').addClass('hybrid-sub-header').html(soon_text)
                )
            );

        $('.select-lists').after(
                $('<div>').addClass('payment-calc-placeholder').append(
                    div
                    )
                );

        Cufon.replace($('.hybrid-header, .hybrid-sub-header', div), { fontFamily: 'National' }, true);
    }

    function _hide() {
        $('.payment-calc-placeholder').remove();
        $('.select-lists').nextUntil('#lease_summary').show();
    }

    $('#vehicle, #province').change(function (event) {
        var val = $('#vehicle').val(),
            selectedIndex = $('#vehicle')[0].selectedIndex,
            options = $('#vehicle')[0].options;

        var model = $(options[selectedIndex]).text();

        try {
            val = $.parseJSON(val);
        }
        catch (e) {
            val = null;
        }

        if (null === val) {
            _show(model);
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
            return;
        }
        else {
            // temporary condition
            if (val && val.model_key && val.model_key == 'civic_hybrid') {
                _show(model);
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
                return;
            }
            else {
                _hide();
            }
        }
    });

    $('#vehicle').change();


    $('[property~="act:collapsible"]').bind('collapsibleshow', function () {
        Cufon.replace($('a', '#finance-calculator .tabs_horizontal_fixed'), { fontFamily: FONT_NATIONAL }, true);
    });

    function fill_in_blanks(type) {

        if (type != 'finance') {
            // lease down payment
            $('#lease_down_payment').attr('placeholder', '------');
            // lease est balance
            $('#lease_est_balance').text('------');
            // lease apr
            $('#lease_apr').text('------');
            // lease annual km
            $('#lease_est_annual_km').text('------');
            // lease buyout
            $('#lease_buyout').text('------');
            // lease monthly payment
            $('#lease_est_monthly_payment').text('------');
            // lease amount on delivery
            $('#lease_amount_on_delivery').text('------');
            $('.lease_amount').text('------');
            $('#lease_summary tbody').empty();
            $('#lease_rate_period').text('');
        }

        if (type != 'lease') {
            // finance down payment
            $('#finance_down_payment').attr('placeholder', '------');
            // additional km
            $('#lease_additional_annual_km').attr('placeholder', '------');
            // finance est balance
            $('#finance_est_balance').text('------');
            // finance apr
            $('#finance_apr').text('------');
            // finance monthly payment
            $('#finance_est_monthly_payment').text('------');
            // finance amount on delivery
            $('#finance_amount_on_delivery').text('------');
            // msrp
            $('.msrp_amount').text('------');
            $('.cash_purchase_amount').text('------');
            $('.finance_amount').text('------');
            $('#finance_summary tbody').empty();
            $('#finance_rate_period').text('');
        }

    }

    function clear_summary() {
    }

    require(['ext/jquery.format', 'ext/jquery.json.min', 'ext/honda/jquery.hondafinance', 'ui/jquery.ui.datepicker'], function () {

        $('#finance-calculator').hondafinance({
            vehicleSelector: '#vehicle',
            provinceSelector: '#province',
            financeUrl: global_get('url.finance_calculator'),
            msrpUrl: global_get('url.finance_calculator_msrp'),
            summaryUrl: global_get('url.finance_calculator_summary'),
            lang: global_get('lang'),
            threshold: 100,
            create_summary: function (data, opts, types) {
                function create_item(item) {
                    if (item.title === null) {
                        return '';
                    }
                    var $tr = $('<tr>');
                    item.value = item.value === null ? '' : item.value;
                    var $value = item.unit == 'dollar' ? $('<span>').text(item.value).format({ format: global_get('format.price') }) :
						$('<span>').text(item.value);
                    $tr.append(
					$('<td>').addClass('first odd').append(
						$('<div>').append(
							$('<span>').text(item.title)
						)
					)
				).append(
					$('<td>').addClass('even').append(
						$('<div>').append(
							$value
						)
					)
				);

                    return $tr;
                }

                // lease or finance
                $.each(types, function (index, type) {
                    var $tbody = $('<tbody>');
                    var group = data[type];

                    if (group) {
                        $.each(group.keys, function (index, key) {
                            var item = group[key];
                            // deal with group items
                            if ('grouped_items' in item && $.isArray(item.grouped_items)) {
                                var $tr = create_item(item);
                                $tr = $tr ? $tr.addClass('group_title') : '';
                                $tbody.append($tr);
                                $.each(item.grouped_items, function (index, subkey) {
                                    var $tr = create_item(item[subkey]);
                                    $tr = $tr ? $tr.addClass('grouped') : '';
                                    $tbody.append($tr);
                                    $.each(item[subkey].items, function (index, item) {
                                        var $tr = create_item(item).addClass('grouped_item');
                                        $tbody.append($tr);
                                    });
                                });
                            }
                            else {
                                $tbody.append(create_item(item));
                            }
                        });

                        $tbody.find('tr:last').addClass('last bottom');

                        if (type == 'lease') {
                            $(opts.leaseSummaryContainer).find('tbody').replaceWith($tbody);
                        }
                        else {
                            $(opts.financeSummaryContainer).find('tbody').replaceWith($tbody);
                        }
                    }
                });
            },
            populate: function (data, opts) {
                // check exceptions
                $('.exception').empty();
                if ('exceptions' in data && data.exceptions.keys.indexOf('lease') != -1) {
                    $.each(data.exceptions.lease, function (index, element) {
                        $('#exception_' + element.field).append($('<div>').text(element.message)).show();
                    });
                }
                if ('exceptions' in data && data.exceptions.keys.indexOf('finance') != -1) {
                    $.each(data.exceptions.finance, function (index, element) {
                        $('#exception_' + element.field).append($('<div>').text(element.message)).show();
                    });
                }
                if (data.lease) {
                    $('table tr th:nth-child(3), table tr td:nth-child(3)', '#payment_options').show();
                    $('table tr th:nth-child(4), table tr td:nth-child(4)', '#payment_options').addClass('odd').removeClass('even');

                    $('#finance-calculator').removeClass('finance-only');

                    // lease term
                    $('#lease_term').empty();
                    $.each(data.lease.term_options, function (index, element) {
                        $('#lease_term').append($('<option>').attr('value', element).text(element));
                    });
                    $('#lease_term').val(data.selected_keys.term_options_lease);

                    $('.lease_amount', this).text(data.lease.est_monthly_payment).format({ format: global_get('format.price').replace('.00', '') });
                    // threshold
                    if (data.lease.apr >= opts.threshold) {
                        fill_in_blanks('lease');
                        $('#lease_term').next('.error').remove().end().after(
				        $('<span class="error">').text(global_get('finance_calculator.lease_contact_dealer'))
			        );
                    }
                    else {
                        $('#lease_term').next('.error').remove();
                        // lease down payment
                        $('#lease_down_payment').val(data.lease.down_payment).format();
                        // lease est balance
                        $('#lease_est_balance').text(data.lease.est_balance).format();
                        // lease apr
                        $('#lease_apr').text(data.lease.apr);
                        // lease annual km
                        $('#lease_est_annual_km').text(data.lease.est_annual_km).format();
                        // additional km
                        $('#lease_additional_annual_km').val(data.lease.additional_annual_km).format({ format: '####' });
                        // additioanl cost per km
                        $('#lease_additional_cost_per_km').text(data.lease.additional_cost_per_km).format();
                        // lease buyout
                        $('#lease_buyout').text(data.lease.buyout).format();
                        // lease monthly payment
                        $('#lease_est_monthly_payment').text(data.lease.est_monthly_payment).format();
                        // lease amount on delivery
                        $('#lease_amount_on_delivery').text(data.lease.amount_on_delivery).format();
                        // rate period
                        try {
                            if (data.lease.rate_period_start && data.lease.rate_period_end) {
                                var s = new Date(data.lease.rate_period_start);
                                var e = new Date(data.lease.rate_period_end);
                                s = $.datepicker.formatDate($.datepicker.RFC_2822, s, opts.dateSettings);
                                e = $.datepicker.formatDate($.datepicker.RFC_2822, e, opts.dateSettings);
                                $('#lease_rate_period').text(s + ' - ' + e);
                            }
                        }
                        catch (e) {
                            $('#lease_rate_period').text('');
                        }
                    }
                }
                else {
                    fill_in_blanks('lease');
                    $('table tr th:nth-child(3), table tr td:nth-child(3)', '#payment_options').hide();
                    $('table tr th:nth-child(4), table tr td:nth-child(4)', '#payment_options').removeClass('odd').addClass('even');
                    $('#finance-calculator').addClass('finance-only');
                }


                if (data.finance) {
                    // finance term
                    $('#finance_term').empty();
                    $.each(data.finance.term_options, function (index, element) {
                        $('#finance_term').append($('<option>').attr('value', element).text(element));
                    });
                    $('#finance_term').val(data.selected_keys.term_options_finance);
                    $('.finance_amount', this).text(data.finance.est_monthly_payment).format({ format: global_get('format.price').replace('.00', '') });

                    if (data.finance.apr >= opts.threshold) {
                        fill_in_blanks('finance');
                        $('#finance_term').next('.error').remove().end().after(
				        $('<span class="error">').text(global_get('finance_calculator.finance_contact_dealer'))
			        );
                    }
                    else {
                        $('#finance_term').next('.error').remove();
                        // finance down payment
                        $('#finance_down_payment').val(data.finance.down_payment).format();
                        // finance est balance
                        $('#finance_est_balance').text(data.finance.est_balance).format();
                        // finance apr
                        $('#finance_apr').text(data.finance.apr).format();
                        // finance monthly payment
                        $('#finance_est_monthly_payment').text(data.finance.est_monthly_payment).format();
                        // finance amount on delivery
                        $('#finance_amount_on_delivery').text(data.finance.amount_on_delivery).format();
                        // rate period
                        try {
                            if (data.finance.rate_period_start && data.finance.rate_period_end) {
                                var s = new Date(data.finance.rate_period_start);
                                var e = new Date(data.finance.rate_period_end);
                                s = $.datepicker.formatDate($.datepicker.RFC_2822, s, opts.dateSettings);
                                e = $.datepicker.formatDate($.datepicker.RFC_2822, e, opts.dateSettings);
                                $('#finance_rate_period').text(s + ' - ' + e);
                            }
                        }
                        catch (e) {
                            $('#finance_rate_period').text('');
                        }
                    }
                }
                else {
                    fill_in_blanks('finance');
                }
                // msrp
                $('.msrp_amount', this).text(data.msrp.value).format({ format: global_get('format.price').replace('.00', '') });
                $('.cash_purchase_amount', this).text(data.cash_purchase.value).format({ format: global_get('format.price').replace('.00', '') });
                // $('#freight_pda', this).text(data.msrp.value).format({format:global_get('format.price').replace('.00', '')});
                // $('#tax_levis').text(data.msrp.value).format({format:global_get('format.price').replace('.00', '')});
                // $('#total_price').text(data.msrp.value).format({format:global_get('format.price')});


            },
            _last: null
        });
        var province = $.cookie('hondaprovince') ? $.cookie('hondaprovince') : 'ON';
        $('#finance_calculator #province').change(function () {
            $.cookie('hondaprovince', $(this).val());
        }).val(province);


    });

};