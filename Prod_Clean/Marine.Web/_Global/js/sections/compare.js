window.section_init = function () {

    var error_add = $('.error-message-add').text()
        , localQueue = new $.jobqueue(100)
        , modelFamily = $('#ModelFamilyUrl').val();

    // User Interface part of the compare app
    function attachCompareBehaviors(behavior, trim, ui, frame) {
        $('.item', frame).click(function () {
            var t, family = $(this).data('model-category') || modelFamily;
            try {
                if (behavior == 'add') {
                    t = findTrim($(this).data('trim-id'));
                    if (t.size() == 0) {
                        $('#CompareApp').CompareApp('add', $(this).data('trim-id'), family);
                    }
                    else {
                        alert(error_add.replace('{0}', $(this).data('trim-id')));
                    }
                }
                else if (behavior == 'replace') {
                    t = findTrim($(this).data('trim-id'));
                    if (trim.data('trim-id') != $(this).data('trim-id') && t.size() == 0) {
                        $('#CompareApp').CompareApp('replace', trim.data('trim-id'), $(this).data('trim-id'), family);
                    }
                    else {
                        alert(error_add.replace('{0}', $(this).data('trim-id')));
                    }
                }
                ui.widget.popup('hide');
            } catch (e) {
                // error
                alert(error_add.replace('{0}', $(this).data('trim-id')));
            }
        });
    }

    function attachScrollViews(frame, popup) {
        require(['commonlib/jquery.commonlib.scrollview'], function () {
            if ($('.subview > .item', frame).size() < 4) {
                return;
            }
            var one = 125, //$('.subview > .item', frame).first().outerWidth(),
                width = $('.subview > .item', frame).size() * one;
            $('.subview', frame).width(width);
            $('.scrollview', frame).scrollview({
                delayed: false,
                type: 'horizontal'
            }).css('overflow', 'hidden');
        });
    }

    function attachSelectorBehaviors(frame, popup, ui) {
        // attach select behavior
        $('[property~="compare:series-selector"], [property~="compare:category-selector"]', frame).change(function () {
            var select = $(this),
                            url = select.val();
            // load the selection area
            $.ajax({
                global: false,
                dataType: 'html',
                type: 'GET',
                url: url,
                success: function (data) {
                    if ($('.compare-selector', frame).size() > 0) {
                        $('.compare-selector', frame).empty().append(
                                $('.compare-selector > *', data)
                            );
                        attachSelectorBehaviors(frame, popup, ui);
                    }

                    $('.selection_wrap', frame).empty().append(
                                        $('.selection_wrap > *', data)
                                        );

                    // Update the fonts within the dropdown
                    select.blur();
                    attachScrollViews(frame, popup);
                    attachCompareBehaviors(popup.data('compare-behavior') || 'add', popup.parents('.tag-trim-details'), ui, ui.frame);
                }
            });
        });
    }
    // Select model popup
    function createComparePopup(obj) {
        $(obj).each(function () {
            var self = $(this);
            self.popup({
                immediate: true,
                position: {
                    my: 'right top',
                    at: 'right bottom',
                    offset: '10 4'
                }
                , complete: function (event, ui) {
                    $('.dropdown_button a', ui.frame).addClass('btn-hover');
                    Cufon.replace($('.dropdown_button a', ui.frame), { fontFamily: FONT_NATIONAL_EXTRABOLD }, true);

                    // attach select behavior
                    attachSelectorBehaviors(ui.frame, self, ui);

                    if (typeof DD_belatedPNG != 'undefined') {
                        $('.pf', ui.frame).each(function () {
                            DD_belatedPNG.fixPng(this);
                        });
                    }

                    attachCompareBehaviors($(this).data('compare-behavior') || 'add', self.parents('.tag-trim-details'), ui, ui.frame);
                    attachScrollViews(ui.frame);
                    // Update the fonts within the dropdown
                    //                    cufon_refresh_replace(ui.frame);
                }
                , show: function (event, ui) {
                    // try fix font issue
                    //                    Cufon.replace($('.dropdown_button a.btn', ui.frame), {fontFamily: F.FONT_NATIONAL_EXTRABOLD}, true);
                }
            });

            Cufon.replace(self, { fontFamily: FONT_NATIONAL_EXTRABOLD }, true);
        });
    }

    function clearAllSpecs() {
        $('th .tag-trim-details', '#CompareApp').hide().data('trim-id', null);
        $('th .popup_wrap', '#CompareApp').show();
        $('tbody tr:not(.note):not(.tmpl)', '#CompareApp').remove();
    }

    function adjustTableClasses() {
        var stack = $('tbody tr:not(.note):not(.tmpl)', '#CompareApp');
        stack.filter(':first').addClass('first');
        stack.filter(':last').addClass('last bottom');
    }

    function findFirstUnused() {
        return $('th .tag-trim-details', '#CompareApp').filter(function () {
            return !$(this).data('trim-id');
        }).first();
    }

    function findLastUnusedAndUnmarked() {
        return $('th .tag-trim-details:not(.col-marked)', '#CompareApp').filter(function () {
            return !$(this).data('trim-id');
        }).last();
    }

    function findNextColumn(col) {
        var nextTHs = $(col).parent().nextAll('th:has(.tag-trim-details)');
        if (nextTHs.size() == 0) {
            return false;
        }

        return $('.tag-trim-details', nextTHs.first());
    }

    function swapColumns(col, dol) {
        var colTH = col.parent(), dolTH = dol.parent(),
            colIndex = colTH.data('index') - 1, dolIndex = dolTH.data('index') - 1;
        // swap the elements
        $('tr:not(.tmpl):not(.note)', '#CompareApp').each(function () {
            var c = $('th, td', this),
                colChildren = c.eq(colIndex).children().detach(),
                dolChildren = c.eq(dolIndex).children().detach();

            c.eq(colIndex).append(dolChildren);
            c.eq(dolIndex).append(colChildren);
        });
    }


    function adjustTableColumns() {
        /* Algorithm to adjust table columns:
        0. Clear all column marks.
        1. Find the first unused column and the last unmarked unused column.
        2. If the last unmarked column is not found, algorithm stops.
        3. If the last unused column is the same as the first unused column,
        and the next column of the last unused column is also unused, algorithm stops.
        If the last unused column does not have a next column, and it's not the same
        as the first unused column, mark the column and go to 1.
        4. Swap the last unused column with the column to the right.
        5. Go to 1.
        */

        // clear all marks
        $('.tag-trim-details', '#CompareApp').removeClass('col-marked');

        while (true) {
            // 1
            var first = findFirstUnused(), last = findLastUnusedAndUnmarked();
            var next = findNextColumn(last);

            // 2
            if (last.size() == 0) {
                break;
            }

            // 3.1
            if (first[0] == last[0] && (false === next || !next.data('trim-id'))) {
                break;
            }

            // 3.2
            if (first[0] != last[0] && (false === next || !next.data('trim-id'))) {
                last.addClass('col-marked');
                continue;
            }

            // 4
            swapColumns(last, next);

            // 5
        }
    }

    function findTrim(trimId) {
        return $('th .tag-trim-details', '#CompareApp').filter(function () {
            return $(this).data('trim-id') == trimId;
        }).first();
    }

    var specRows = {};

    function findSpec(spec_key) {

        if (specRows[spec_key]) {
            return specRows[spec_key];
        }
        else {
            var spec = $('tbody tr:not(.note):not(.tmpl)', '#CompareApp').filter(function () {
                return $(this).data('trim-spec-key') == spec_key;
            }).first();
            if (spec.size() > 0) {
                specRows[spec_key] = spec;
                return specRows[spec_key];
            }
            else {
                return spec;
            }
        }
    }

    function addTrimSpec(nth, key, label, value) {
        var spec = findSpec(key);

        if (spec.size() == 0) {
            spec = $('tbody tr.tmpl', '#CompareApp').clone().removeClass('tmpl dn').show().appendTo($('tbody', '#CompareApp'));
            spec.data('trim-spec-key', key);
            $('.tag-spec-label', spec).html(label);
            specRows[key] = spec;
        }

        // update value
        $('td:nth-child(' + nth + ') span', spec).html(value);
    }

    function removeTrimSpecs(nth) {
        $('tbody tr:not(.note):not(.tmpl) td:nth-child(' + nth + ') span', '#CompareApp').empty();

        for (var key in specRows) {
            if (!$('#CompareApp').CompareApp('containsSpec', key)) {
                specRows[key].remove();
                delete specRows[key];
            }
        }
    }

    function addTrim(id, data, trim) {
        trim = trim || findFirstUnused();
        var nth = trim.parent().data('index');

        if (trim.size() == 0) {
            return;
        }

        // update the copy
        Cufon.replace($('.tag-trim-year', trim).text(data.year), { fontFamily: window.FONT_NATIONAL }, true);
        Cufon.replace($('.tag-trim-name', trim).text(data.title), { fontFamily: window.FONT_NATIONAL_SEMIBOLD }, true);
        Cufon.replace($('.tag-price', trim).text(data.msrp).format({ format: global_get('format.price_rounded') }), { fontFamily: window.FONT_NATIONAL_SEMIBOLD }, true);
        Cufon.replace($('.tag-discount', trim).text(data.discount).format({ format: global_get('format.price_rounded') }), { fontFamily: window.FONT_NATIONAL_SEMIBOLD }, true);
        Cufon.replace($('.tag-final-price', trim).text(data.price).format({ format: global_get('format.price_rounded') }), { fontFamily: window.FONT_NATIONAL_SEMIBOLD }, true);

        $('.tag-text-msrp', trim).text(data.text_msrp);
        $('.tag-text-discount', trim).text(data.text_discount);
        $('.tag-text-final-price', trim).text(data.text_final_price);
        $('.tag-thumbnail', trim).attr('src', data.thumbnail);

        // logic appears if discount is here.
        if (data.isComingSoon) {
            $('.tag-coming-soon', trim).show();
            $('.tag-no-discount', trim).hide();
            $('.tag-has-discount', trim).hide();
        }
        else {
            if (data.discount > 0) {
                $('.tag-coming-soon', trim).hide();
                $('.tag-no-discount', trim).hide();
                $('.tag-has-discount', trim).show();
            }
            else {
                $('.tag-coming-soon', trim).hide();
                $('.tag-has-discount', trim).hide();
                $('.tag-no-discount', trim).show();
            }
        }
        // set the trim's data id
        trim.data('trim-id', id);
        $('a.close', trim).data('trim-id', id);

        // add trim specs
        $.each(data.specs.keys, function (index, spec_key) {
            var label = $('#CompareApp').CompareApp('getSpec', spec_key),
                value = data.specs[spec_key].SpecValue;

            if (label['SpecLabel']) {
                label = label.SpecLabel;
            }
            else {
                label = '';
            }

            addTrimSpec(nth, spec_key, label, value);
        });

        // display/hide
        trim.siblings('.popup_wrap').hide();
        trim.show();

        // show disclaimer
        $('.disclaimer-content').show();

        adjustTableClasses();
    }

    function removeTrim(id) {
        var trim = findTrim(id);
        if (trim.size() == 0) {
            return;
        }
        var index = trim.parent().data('index');

        // update trim id
        trim.data('trim-id', null);

        // display/hide
        trim.siblings('.popup_wrap').show();
        trim.hide();

        // clear trim specs
        removeTrimSpecs(index);
        adjustTableColumns();
        adjustTableClasses();
    }

    require(['commonlib/jquery.commonlib.popup', 'ext/honda/honda.CompareApp', 'includes/jquery.format'], function () {
        createComparePopup('[property~="compare:model-selector"]');

        // load compare app
        $('#CompareApp').CompareApp({
            modelSpecsUrl: global_get('honda.CompareApp')['modelSpecsUrl'],
            beforeAdd: function (event, ui) {
            },
            add: function (event, ui) {
                addTrim(ui.id, ui.data);
            },
            beforeRemove: function (event, ui) {
            },
            remove: function (event, ui) {
                removeTrim(ui.id);
            },
            beforeReplace: function (event, ui) {
            },
            replace: function (event, ui) {
                var trim = findTrim(ui.id);
                addTrim(ui.replacement_id, ui.data, trim);
            },
            highlight: function (event, ui) {
                if (specRows.hasOwnProperty(ui.key)) {
                    if (ui.highlight) {
                        specRows[ui.key].addClass('differences');
                    }
                    else {
                        specRows[ui.key].removeClass('differences');
                    }
                }
            }
        });

        // close buttons
        $('a.close', '#CompareApp').click(function (event) {
            var trimId = $(this).data('trim-id');
            if (!trimId) {
                event.preventDefault();
                return undefined;
            }

            $('#CompareApp').CompareApp('remove', trimId);
            event.preventDefault();
        });

        // initial load
        var trimIds = $('#TrimIDs').val();
        if (trimIds) {
            trimIds = trimIds.split(/,/);
        }
        else {
            trimIds = [];
        }

        clearAllSpecs();

        // filter trim ids
        var index = 0;
        while (index < trimIds.length) {
            if (trimIds[index] == '') {
                trimIds.splice(index, 1);
            }
            else {
                index++;
            }
        }

        if (trimIds.length > 0) {
            $.each(trimIds, function (index, trimId) {
                localQueue.add(function () {
                    $('#CompareApp').CompareApp('add', trimId, modelFamily);
                });
            });
        }
    });
};