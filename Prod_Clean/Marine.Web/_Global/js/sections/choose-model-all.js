﻿define('sections/choose-model-all', ['require'], function (require) {

    var chooseModelAllFunc = function () {

        // attach events
        $('.all-products-steps .categories li a').each(function () {
            var self = $(this);


            self.click(function (event) {
                event.preventDefault();

                if ($('.all-products-steps').data('isLoading')) {
                    return;
                }
                $('.bottom-container').show();
                $('.wrapper-grey.cap_bottom_default').addClass('cap_bottom_grey');

                $('.all-products-steps').data('isLoading', true);

                var localQueue = new jQuery.JobQueue();

                $('.all-products-steps > li').show();

                self.closest('li').addClass('active').siblings().removeClass('active');
                jQuery.behaviors.cufon_refresh('.all-products-steps', true);

                var loader = $('<div>').addClass('loader-overlay').appendTo($('.top-container').css('position', 'relative'));


                loader.position({
                    my: 'center top'
                    , at: 'center top'
                    , offset: '0 250px'
                    , of: '.top-container'
                    , collision: 'none'
                });

                $.ajax({
                    type: 'GET'
                    , dataType: 'text'
                    , url: self.attr('href')
                    , global: false
                    , success: function (data, status, xhr) {
                        if ($.browser.msie) {
                            var div = document.createElement('div');
                            div.innerHTML = data;
                            data = div;
                        }

                        $('.choose-model-placeholder').remove();

                        var cnt = $('#ChooseModel', data).html();

                        $('#ChooseModel').html(cnt);

                        // run ajax behaviors
                        execute_behaviors('ajax', '#ChooseModel', true, null, localQueue);

                        // run product listing and choose model
                        require(['sections/product-listing', 'sections/choose-model'], function (runProductListing, runChooseModel) {
                            runProductListing(localQueue);
                            runChooseModel(localQueue);
                        });
                    }
                    , error: function (xhr, err) {
                        console.log(err)
                    }
                    , complete: function () {
                        localQueue.add(function () {
                            loader.remove();
                            $('.all-products-steps').data('isLoading', false);
                        });
                    }
                });
                setTimeout(function () {
                    if (loader)
                        loader.remove();
                }, 5000);
            });
        });

    };


    if (!window['section_init']) {
        window.section_init = chooseModelAllFunc;
    }

    return chooseModelAllFunc;
});