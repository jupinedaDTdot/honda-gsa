window.section_init = function () {

    $('.colors .option-items a', '.product_heading').click(function (event) {
        event.preventDefault();

        var self = $(this);
        // take the large image url from the anchor
        var url = self.attr('rel');

        if (url) {
            $('.hero-image', '.product_heading').show();
            $('.hero-image img', '.product_heading').attr('src', url);
            $('.viewer-360', '.product_heading').hide();

            // update popup image as well
            var popup_url = url.replace(/(width=\d+)/i, 'width=960').replace(/(height=\d+)/i, 'height=565');
            $('#popup-image .popup-image-large img').attr('src', popup_url);

            // update the colour active state
            self.addClass('active').siblings('a').removeClass('active');
        }


        // update pricing according to color
        if ($('.tag-msrp', '.product-price-container').size() > 0) {
            $('.tag-msrp', '.product-price-container').html(self.data('trimMsrp'));
        }
        if ($('.tag-discount', '.product-price-container').size() > 0) {
            $('.tag-discount', '.product-price-container').html(self.data('trimDiscount'));
        }
        if ($('.tag-final-price', '.product-price-container').size() > 0) {
            $('.tag-final-price', '.product-price-container').html(self.data('trimPrice'));
        }
        if ($('.tag-image-disclaimer', '.product_heading').size() > 0) {
            $('.tag-image-disclaimer', '.product_heading').html(self.data('disclaimer'));
        }

        if (self.data('discountDisclaimer')) {
            $('a.btn-discounted-price').show();
            $('.tag-discounted-price-legal').html(self.data('discountDisclaimer'));
        }
        else {
            $('a.btn-discounted-price').hide();
        }

        if ($('.tag-support-discount').size() > 0) {
            if ($('.tag-coming-soon').is(':hidden')) {
                // reset all container status
                $('.tag-has-discount').hide();
                $('.tag-no-discount').hide();

                if (self.data('trimHasDiscount')) {
                    $('.tag-has-discount').show();
                }
                else {
                    $('.tag-no-discount').show();
                }
            }
        }

        // update build and price button
        if ($('a.btn-build-price').size() > 0) {
            (function () {
                var btn = $('a.btn-build-price'),
                    href = btn.attr('href');
                href = href.replace(/color\/[a-z0-9_-]+/i, 'color/' + self.data('trimColor'));
                btn.attr('href', href);
            })();
        }

        $.behaviors.cufon_refresh('.product-price-container', true);
    });

    $('.view-360', '.product_heading').click(function (event) {
        event.preventDefault();

		var $container = $('[property~="honda:360"]');

		//clean and recreate target container.
		$('.tout-container img', $container).first().trigger('teardown'); // tear down the jquery reel
		var $targetContainer = $('<div></div>').addClass('tout-container').appendTo($container.empty());

		$('.hero-image, .viewer-video-showcase, span.view-360', '.product_heading').hide();
        $('.viewer-360', '.product_heading').show();

		options360 = $.extend({}, global_get('player.360'), options360);

		if (options360.type == "flash") {
			$targetContainer.flash(options360);
		} else {
			require(['includes/jquery.honda.widget360'], function () {
				$targetContainer.widget360(options360);
			});
		}
    });

	$('a[property~="act:video-showcase"]', '.product_heading').click(function(event) {
		event.preventDefault();

		var $container = $('[property~="honda:video-showcase"]');
		var $targetContainer = $('<div></div>').addClass('tout-container').appendTo($container.empty());

		$('.hero-image, .viewer-360', '.product_heading').hide();
		$('.viewer-video-showcase, span.view-360', '.product_heading').show();

		optionsVideoShowcase = $.extend({}, global_get('player.video'), optionsVideoShowcase);

		require(['includes/jquery.honda.videoplayer'], function () {
			$targetContainer.videoplayer(optionsVideoShowcase);
		});

	})



    // track gallery links so we can update wallpaper link as well.
    $('a.gallery_link', '#inner_page_content').live('click', function (event) {
        var url = $(this).data('wallpaper-path');
        $('a.download-wallpaper', '#inner_page_content').attr('href', url);
    });

    // sign up form
    $('a[data-ajaxform-name="SignUpForm"]').bind('popupshow', function () {
        $('form').data('CurrentForm', 'SignUpForm');
    }).bind('popuphide', function () {
        $('form').data('CurrentForm', null);
    });


    $('a[property~="act:ajaxload"]', '.tabs_horizontal').bind('ajaxloadbeforeload', function () {
        var div = $('<div>').addClass('loader_container').append(
            $('<div>').addClass('loader_default bg_loader')
        );
        $('.content_box .content').append(div);
    }).bind('ajaxloadload', function () {
        $('.loader_container', '.content_box .content').remove();
    }).bind('ajaxloaderror', function () {
        $('.loader_container', '.content_box .content').remove();
    });

    $('.view-all-family a.view-all').bind('popupcomplete', function (event, ui) {
        var popup = $(this);
        $('.dropdown_button', ui.frame).click(function (e) {
            e.preventDefault();
            popup.popup('close');
        });
    });
};

