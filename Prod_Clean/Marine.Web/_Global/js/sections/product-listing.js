define('sections/product-listing', ['require'], function (require) {
  var error_add = $('.error-message-add').text();

  var productListingFunc = function () {
    function attachScrollViews(frame, popup) {
      require(['commonlib/jquery.commonlib.scrollview'], function () {
        if ($('.subview > .item', frame).size() < 4) {
          return;
        }
        var one = 125, //$('.subview > .item', frame).first().outerWidth(),
                width = $('.subview > .item', frame).size() * one;
        $('.subview', frame).width(width);
        $('.scrollview', frame).scrollview({
          delayed: false,
          type: 'horizontal'
        }).css('overflow', 'hidden');
      });
    }
    function attachCompareWidgetBehaviors(frame, popup) {
      $('.subview .item', frame).click(function (event) {
        event.preventDefault();
        try {
          $('#CompareWidget').CompareWidget('add', $(this).data('trimId'), $(this));
        }
        catch (e) {
          alert(error_add.replace('{0}', $(this).data('trim-id')));
        }
        popup.popup('close');
      });
    }

    function attachSelectorBehaviors(frame, popup) {
      // attach select behavior
      $('[property~="compare:series-selector"], [property~="compare:category-selector"]', frame).change(function () {
        var select = $(this),
                            url = select.val();
        // load the selection area
        $.ajax({
          global: false,
          dataType: 'html',
          type: 'GET',
          url: url,
          success: function (data) {
            if ($('.compare-selector', frame).size() > 0) {
              $('.compare-selector', frame).empty().append(
                                $('.compare-selector > *', data)
                            );
              attachSelectorBehaviors(frame, popup);
            }

            $('.selection_wrap', frame).empty().append(
                                        $('.selection_wrap > *', data)
                                        );

            // Update the fonts within the dropdown
            select.blur();
            attachScrollViews(frame, popup);
            attachCompareWidgetBehaviors(frame, popup);
          }
        });
      });
    }

    // add popup support
    $('#CompareWidget .product-item .selector .buttons a').each(function () {
      var self = $(this);

      self.bind('popupcomplete', function (event, ui) {
        ui.placeholder.addClass('btn-hover');

        attachSelectorBehaviors(ui.frame, self);
        attachScrollViews(ui.frame, self);
        attachCompareWidgetBehaviors(ui.frame, self);
      });

      require(['commonlib/jquery.commonlib.popup'], function() {
        self.popup('destroy');
        self.popup();
      });
    });

    // initialize the compare widget
    require(['ext/honda/honda.CompareWidget'], function () {

      var reorderItems = function () {
        // update order
        $('.product-item:not(.active)', '#CompareWidget').each(function () {
          $(this).appendTo($(this).parent());
        });
        // update number
        $('.product-item', '#CompareWidget').each(function (index) {
          if (index) {
            $(this).removeClass('first');
          }
          else {
            $(this).addClass('first');
          }
          $('.list-steps', this).text(index + 1);
          Cufon.replace($('.list-steps', this), { fontFamily: window.FONT_NATIONAL }, true);
        });
      };

      $('#CompareWidget').CompareWidget({
        url: $('#CompareWidget .recently_viewed_compare a.primary').attr('href'),
        name: 'compareIDs',
        type: 'GET',
        submitHandler: function (ids) {
          var o = this.options;
          document.location.href = o.url + '/' + ids.join(',');
        },
        add: function (event, ui) {
          // add item to the compare widget
          var obj = $(ui.object),
                        widget = $('.product-item:not(.active)', '#CompareWidget').first();
          // update title
          $('.selector, .add-model', widget).hide();
          $('.price-heading', widget).show().text(obj.data('item-title'));
          $('.price', widget).show();
          $('.tag-price', widget).text(obj.data('item-price'));
          $('.tag-discount', widget).text(obj.data('item-discount'));
          $('.tag-final-price', widget).text(obj.data('item-final-price'));
          $('.tag-text-msrp', widget).text(obj.data('item-text-msrp'));
          $('.tag-text-discount', widget).text(obj.data('item-text-discount'));
          $('.tag-text-final-price', widget).text(obj.data('item-text-final-price'));
          $('.btn-remove', widget).show().data('id', ui.id);

          if (obj.data('item-coming-soon')) {
            $('.tag-coming-soon', widget).show();
            $('.tag-no-discount', widget).hide();
            $('.tag-has-discount', widget).hide();
          }
          else {
            if (obj.data('item-has-discount')) {
              $('.tag-no-discount', widget).hide();
              $('.tag-has-discount', widget).show();
              $('.tag-coming-soon', widget).hide();
            }
            else {
              $('.tag-has-discount', widget).hide();
              $('.tag-no-discount', widget).show();
              $('.tag-coming-soon', widget).hide();
            }
          }

          Cufon.replace($('.price strong, .price-heading', widget), { fontFamily: window.FONT_NATIONAL_SEMIBOLD }, true);

          widget.data('id', ui.id);
          widget.addClass('active');
        },
        remove: function (event, ui) {
          var obj = $(ui.object),
                        widget = $('.product-item.active', '#CompareWidget').filter(function () {
                          return $(this).data('id') && $(this).data('id') == ui.id;
                        }).first();

          // hide button
          $('.product .product-item .product-image').filter(function () {
            var self = $(this)
                            , pattern = new RegExp('^' + self.data('id') + '(-|$)');

            return self.data('popup') && pattern.test(ui.id);
          }).first().each(function () {
            var frame = $(this).popup('frame');
            $('.compare-buttons .remove', frame).hide().siblings('.add').show();
          });

          // remove item from compare widget
          $('.selector, .add-model', widget).show();
          $('.price-heading', widget).hide();
          $('.price', widget).hide();
          $('.btn-remove', widget).hide().data('id', null);
          widget.data('id', null);
          widget.removeClass('active');

          // reorder the items
          reorderItems();
        }
      });

      $('#CompareWidget .recently_viewed_compare a.primary').click(function (event) {
        event.preventDefault();
        $('#CompareWidget').CompareWidget('submit');
      });

      $('#CompareWidget .btn-remove').click(function (event) {
        $('#CompareWidget').CompareWidget('remove', $(this).data('id'));
        event.preventDefault();
      });

      $('#CompareWidget .btn-submit').click(function (event) {
        $('#CompareWidget').CompareWidget('submit');
        event.preventDefault();
      });
    });

    var firstTime = true;

    var updatePopupPosition = function (ui) {
      var e = $.browser.msie || $.browser.mozilla ? 'html' : 'body',
                    top = ui.frame.offset().top + ($.browser.msie && $.browser.version.match(/^(8)\./) ? document.documentElement.scrollTop : 0);
      if ($(e).scrollTop() > top && !$(e).is(':animated')) {
        $(e).stop().animate({
          scrollTop: top
        }, Math.abs($(e).scrollTop() - top), 'swing');
      }
    };

    $('.content_section.compare .selector .buttons a.btn').bind('popupcomplete', function (event, ui) {
      Cufon.replace($(this), { fontFamily: FONT_NATIONAL_EXTRABOLD, hover: true }, true);
      Cufon.replace(ui.placeholder, { fontFamily: FONT_NATIONAL_EXTRABOLD, hover: true }, true);
    });

    $('.product-item .product-image, .applications_table .model-name').bind('popupshow', function (event, ui) {
      // move and show after the content is displayed (so with priority 9)
      if (ui.queue) {
        ui.queue.add(function () { updatePopupPosition(ui); });
      }
      else {
        updatePopupPosition(ui);
      }
    }).bind('popupcomplete', function (event, ui) {
      var popup = $(this).data('popup');

      if (typeof DD_belatedPNG != 'undefined') {
        $('.pf', ui.frame).each(function () {
          DD_belatedPNG.fixPng(this);
        });
      }

      // initialize the buttons
      $('.compare-buttons .add a', ui.frame).click(function (event) {
        var button = $(this);
        event.preventDefault();

        try {
          $('#CompareWidget').CompareWidget('add', button.data('id'), button);

          // we have only one ADD button so the triggered action is located here.
          button.parent().hide().siblings('.remove').show();

          if (firstTime) {
            popup.close();
          }
          setTimeout(function () {
            var e = $.browser.msie || $.browser.mozilla ? 'html' : 'body',
                            top = $('#CompareWidget').offset().top + ($.browser.msie && $.browser.version.match(/^(8)\./) ? document.documentElement.scrollTop : 0);
            $(e).stop().animate({
              scrollTop: top
            }, Math.abs($(e).scrollTop() - top) / 2, 'swing');
          }, 100);

          firstTime = false;
          // }
        }
        catch (e) {
          require(['commonlib/jquery.commonlib.tooltip'], function () {
            // show error tooltip
            button.parent().data({
              position: 'right top'
            });
            button.parent().tooltip({
              additionalClass: 'tooltip-top-error',
              contentTarget: '#compare-add-error'
            });

            button.parent().tooltip('show');
            setTimeout(function () {
              button.parent().tooltip('hide');
              button.parent().tooltip('destroy');
            }, 2000);
          });
        }
        finally {
        }
      });

      $('.compare-buttons .remove a', ui.frame).click(function (event) {
        event.preventDefault();
        $('#CompareWidget').CompareWidget('remove', $(this).data('id'), $(this));
      });

      $('.compare-buttons a', ui.frame).mouseenter(function () {
        $(this).addClass('hover');
      }).mouseleave(function () {
        $(this).removeClass('hover');
      });

      $('.compare-buttons .compare-add-remove.add', ui.frame).mouseenter(function () {
        $(this).addClass('add-hover');
      }).mouseleave(function () {
        $(this).removeClass('add-hover');
      });

      $('.compare-buttons .compare-add-remove.remove', ui.frame).mouseenter(function () {
        $(this).addClass('remove-hover');
      }).mouseleave(function () {
        $(this).removeClass('remove-hover');
      });


      $('a.btn', ui.frame).mouseenter(function () {
        $(this).addClass('btn-hover');
      }).mouseleave(function () {
        $(this).removeClass('btn-hover');
      });

      // FIXME update popup position
      if (ui.queue) {
        // FIXME dirty hack to make sure scroll up works
        setTimeout(function () {
          ui.queue.add(function () {
            updatePopupPosition(ui);
          });
        }, 200);
        setTimeout(function () {
          ui.queue.add(function () {
            updatePopupPosition(ui);
          });
        }, 400);
        setTimeout(function () {
          ui.queue.add(function () {
            updatePopupPosition(ui);
          });
        }, 600);
        setTimeout(function () {
          ui.queue.add(function () {
            updatePopupPosition(ui);
          });
        }, 800);
      }
      else {
        setTimeout(function () {
          updatePopupPosition(ui);
        }, 200);
      }

    }).bind('popupbeforeajaxload', function (event, ui) {
      $('.dropdown_content_mid', ui.frame).append(
                $('<div>').addClass('loader_container').append(
                    $('<div>').addClass('bg_loader loader_gray')
                )
            );
    });
  };


  if (!window['section_init']) {
    window.section_init = function () {
      productListingFunc();
    };
  }

  return productListingFunc;
});