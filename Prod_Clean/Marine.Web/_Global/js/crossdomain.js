;(function($){
// remove test frame

// only do this when we're in honda.ca/acura.ca
var domain = window.location.hostname;
if (domain.match(/(honda|acura)\.ca$/)) {
	return;
}

$('#test').remove(); // this is the bluetooth iframe

var SUPPORT_FRAME_ID = '__cross_domain_support_frame';
var SUPPORT_JS_REGEX = /^(https?:\/\/(.+?\.)?(honda|acura)\.ca)\/_Global\/js\/bottom\.min\.js/;

$.behaviors.iframe_helper_initialization = function(ctx, ajax) {
	// find all iframes in the document and check whether its source is what we want
	if (ajax) {
		return;
	}
	
	// try to find the support frame in the document
	var support_frame = null;
	// $('iframe', ctx).each(function() {
	// 	var iframe = $(this);
	// 	var src = iframe.attr('src');
	// 	
	// 	if (src.match(/\/helper\.html\??/)) {
	// 		support_frame = iframe;
	// 		support_frame.attr('id', SUPPORT_FRAME_ID);
	// 		return false;
	// 	}
	// });
	// create the support frame if not found
	if (!support_frame) {
		$('<iframe>').attr({
			id: SUPPORT_FRAME_ID,
			height: 0,
			width: 0,
			border: 0
		}).appendTo('body');
	}
	
	$('html').css({'background-color': '#fff'});
};

var _base_url = null;

 // Helper function, parse param from request string
function get_param(name) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null )
        return null;
    else
        return results[1];
}

function base_url() {
	if (_base_url) {
		return _base_url;
	}
	$('script').each(function() {
		var src = $(this).attr('src');
		// we expect src to be undefined sometimes, if the script does not have a src.
		if (src) {
			var matches = src.match(SUPPORT_JS_REGEX);
			if (matches) {
				_base_url = matches[1];
				console.log('base url found: ' + _base_url);
				return false;
			}
		}
	});
	
	var domain = get_param('domain');
	if (domain == '') {
		_base_url = _base_url.replace(/www\./, '');
	}
	console.log('base url found: ' + _base_url);
	return _base_url;
}

function remote_call(func, params) {
	try {
		var q = '?call=' + encodeURIComponent(func);
		for (var key in params) {
			q += '&' + encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
		}
		q += '&_rand=' + Math.random();
		
		var iframe = document.getElementById(SUPPORT_FRAME_ID);
		var base = base_url();
		iframe.src = base + '/helper.html' + q;
	} catch (e) {
		console.log(e);
	}
}

function remote_resize_height(height) {
	console.log('height changed to: ' + height);
	remote_call('resizeHeight', {
		height: height
	});
}

$.behaviors.list_faq_resize = function(ctx, ajax) {
	if (ajax) {
		return;
	}
	$('.section dt.collapsed', ctx).click(function() {
		var self = $(this);
		// hook up to the answer's animation queue. When the animation is done, resize the frame.
		self.next('dd').queue(function() {
			// call the resize function here
			// do whatever... to get the height of the document
			var height = $(document).height();
			remote_resize_height(height);
			// then unshift the fx queue
			$(this).dequeue();
		});
	});
};

$.behaviors.iframe_document_resize = function(ctx, ajax) {
	if (ajax) {
		return;
	}
	setTimeout(function() {
		remote_resize_height($(document).height());
	}, 100);
};

})(jQuery);