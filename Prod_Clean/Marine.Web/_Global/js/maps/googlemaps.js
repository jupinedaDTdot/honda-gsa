﻿function ShowMap(config,data,areaId,pin)
{
	//loads viewer center and zoom level
    var viewerData = config;
    var dim = $("#" + areaId);
	var center, zoom, mini;
	//var bounds;
	try {
		viewerData = $.parseJSON(viewerData);
		var ne = new google.maps.LatLng(viewerData.nelat, viewerData.nelng);
		var sw = new google.maps.LatLng(viewerData.swlat, viewerData.swlng);
		var bounds = new google.maps.LatLngBounds(sw, ne);
		center = bounds.getCenter();
		zoom = getBoundsZoomLevel(viewerData.nelat, viewerData.nelng, viewerData.swlat, viewerData.swlng, dim.height() * 0.9, dim.width() * 0.9);
		mini = viewerData.miniView;
	}
	catch (e) {
		center = null;
	}
	var mapOptions = {
		center: center, //new google.maps.LatLng(-34.397, 150.644),
		zoom: zoom,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		overviewMapControl: mini,
		overviewMapControlOptions: { opened: true }
	};
    var map = new google.maps.Map(document.getElementById(areaId), mapOptions);

	//creates generic (and global) location bubble object
	var infowindow = new google.maps.InfoWindow({
		//maxWidth: 295,
		content: 'empty'
	});

	function showBubble(dealerPopupWrapper, feature, map, marker) {
		var content = $(dealerPopupWrapper).html()
				.replace('{0}', feature.DealerName)
				.replace('{1}', feature.Street)
				.replace('{2}', feature.City)
				.replace('{3}', feature.Province)
				.replace('{4}', feature.PostalCode)
				.replace('{5}', feature.MainPhone)
				.replace('{6}', feature.DealerUrl)
				.replace('{7}', feature.DealerImage)
				.replace('{8}', feature.DealerName);
		infowindow.setContent(content);
		infowindow.open(map, marker);
	}
	
	//function to add a pin on the map
	function addMarker(feature) {
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(feature.Latitude, feature.Longitude), //feature.position,
			icon: pin, //iconBase + 'schools_maps.png',//icons[feature.type].icon,
			map: map,
			//flat: false,
			title: feature.DealerName
		});
		return marker;
	}

	//clicking on the map, but outside the bubble, should close the bubble
	google.maps.event.addListener(map, 'click', function () {
		infowindow.close();
	});

	//add a pin for each dealer location
    var dealers = data;  
	try {
		dealers = $.parseJSON(dealers);
	}
	catch (e) {
		dealers = null;
	}
	$(dealers).each(function () {
		var feature = this;
		var marker = addMarker(feature);
		if (this.Clickable) {
			//clicking the location should open the bubble
			google.maps.event.addListener(marker, 'click', function () {
				showBubble(".dealer_popup_wrapper", feature, map, marker);
			});
			var locationIcon = $("[dealerid='" + feature.DealerID + "']");
			locationIcon.click(function () {
				showBubble(".dealer_popup_wrapper", feature, map, marker);
				return false;
			});
		}
	});

	function getBoundsZoomLevel(nelat, nelng, swlat, swlng, mapHeight, mapWidth) {
		var WORLD_DIM = { height: 256, width: 256 };
		var ZOOM_MAX = 21;

		function latRad(lat) {
			var sin = Math.sin(lat * Math.PI / 180);
			var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
			return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
		}

		function zoom(mapPx, worldPx, fraction) {
			return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
		}
		    
		var latFraction = (latRad(nelat) - latRad(swlat)) / Math.PI;

		var lngDiff = nelng - swlng;
		var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

		var latZoom = zoom(mapHeight, WORLD_DIM.height, latFraction);
		var lngZoom = zoom(mapWidth, WORLD_DIM.width, lngFraction);

		if (lngFraction == 0)
			latZoom = 14;

		return Math.min(latZoom, lngZoom, ZOOM_MAX);
	}
}
function ShowMapInFacebookPage(lat,lng,mapCanvasId,title,pinUrl) {
    var latdealer = lat;
    var longdealer = lng;
    var myLatlng = new google.maps.LatLng(latdealer, longdealer);
    var mapOptions = {
        zoom: 15,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById(mapCanvasId), mapOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: title
    });
    marker.setIcon(pinUrl);
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
}