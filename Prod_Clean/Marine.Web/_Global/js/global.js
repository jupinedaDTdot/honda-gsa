﻿var popup = null;
var result = 0;
var cms_url = null;
var cms_redirect_pageUrl = null;
var cms_returnUrl = null;

function isSet(param) {
    return (typeof param !== 'undefined' && param != null);
}

function dialogSettings(width, height) {
    var w = isSet(width) ? width : 950;
    var h = isSet(height) ? height : 750;
    var t = (screen.height - h) / 2;
    var l = (screen.width - w) / 2;
    if ($.browser.msie) {
        w += 20;
    }
    return "width=" + w + ",height=" + h + ",top=" + t + ",left=" + l + ",resizable=1,scrollbars=yes";
}

function modalDialog(url, pageUrl, returnUrl) {
    cms_url = url;
    cms_redirect_pageUrl = pageUrl;
    cms_returnUrl = returnUrl;

    if (popup != null) {
        alert('Please close the already opened popup window to open another window.');
        popup.focus();
    }
    else {
        popup = window.open(url, 'Marine', dialogSettings());
        popup.focus();
    }

    return false;
}

function popupClosed() {
    popup = null;
}

function setReturnValue(val) {
    result = val;
    if (result == 1 || result == null) {
        if (isSet(cms_returnUrl)) {
            window.location.href = cms_returnUrl;
        }
        else if (isSet(cms_redirect_pageUrl)) {
            window.location.href = cms_redirect_pageUrl;
        }
        else {
            window.location.href = cms_url;
        }
    }
    else if (result == -1) {
        window.location.href = '/Asimo.Security/Logout.aspx?ReturnUrl=' + GetReturnUrl();
    }

    popup = null;
}

function editCmsElement(action, mvcController, contentId, pageUrl, returningUrl) {
    return modalDialog('/Content/' + mvcController + '/' + action + '/' + contentId + '?math=' + Math.random(), pageUrl, returningUrl);
}

function bulkSubmit(pageID, pageUrl) {
    return modalDialog('/Content/Content/BulkSubmit?pageID=' + pageID + '&type=ByPage&math=' + Math.random(), pageUrl);
}

function bulkApprove(pageID, pageUrl) {
    return modalDialog('/Content/Content/BulkApprove?pageID=' + pageID + '&type=ByPage&math=' + Math.random(), pageUrl);
}

function newPage(pageSettingID, pageUrl) {
    return modalDialog('/Content/cmsPage/NewPage/' + pageSettingID + '?math=' + Math.random(), pageUrl);
}

function memberAdminPage(url, pageID, pageUrl) {
    return modalDialog(url + '?pageID=' + pageID, pageUrl);
}

function viewTextDifference(contentId, mvcController) {
    return modalDialog('/Content/' + mvcController + '/ViewTextDifference/' + contentId);
}

function GetReturnUrl() {
    urlhref = window.location.href;
    urlhost = window.location.host;
    urlprotocol = window.location.protocol;
    urlprefix = urlprotocol + '//' + urlhost;
    urlreturn = urlhref.substr(urlprefix.length);
    urlreturn = encodeURIComponent(urlreturn);
    return urlreturn;
}