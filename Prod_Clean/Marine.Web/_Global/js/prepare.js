;(function($, W, D, F, require, define, undefined) {
var lang = /\bfrench\b/.test(D.body.className) ? 'fr' : 'en';


// CONSTANTS
F.FONT_FOUNDERS_CONDENSED = 'Founders Grotesk Condensed';
F.FONT_NATIONAL           = 'National';
F.FONT_NATIONAL_SEMIBOLD  = 'National Semibold';
F.FONT_NATIONAL_EXTRABOLD = 'National Extrabold';

// FUNCTIONS
F.forBrowser = function(browser, version, callback) {
    if (typeof version === 'function') {
        callback = version;
    }
    
    if (typeof version === 'string') {
        version = new RegExp(version);
    }

    var _versionCheck = typeof version === 'object' && version.test($.browser.version) || typeof version === 'function';

    if ($.browser[browser] && _versionCheck) {
        callback();
    }
};


F.get_typefaces = function() {
	return [
        // founders condensed
		{
			elements:'.ffgc, #primary_nav li > a, #feature_area .hero_menu .hero_menu_info .video_showcase, #primary_nav li > span > a, #feature_area .buttons > li > a, #feature_area .buttons_expanded > li a.future_hero_link, .find_dealer_subheading, #book_test_drive .h1, .editor .h3, .find_dealer_subheading, h2.top5, .model_specs .h2, .page-landing .landing-heading, #header .landing-heading .commercial-products a',
			options: {fontFamily: F.FONT_FOUNDERS_CONDENSED, hover: true}
		},
        // national extra bold
		{
			elements: '.input_special a, .input_box a, a.secondary_large, .page-compare-results .tbl_compare_result tr.first.top th.name span',
			options: {fontFamily: F.FONT_NATIONAL_EXTRABOLD}
		},
        // national semibold
		{
			elements: '.search_options li:not(.search_sub_header) label, .finance-dropout .finance-heading dd p.title, #trim_selector .content .preview .colour_title , .page-build-price .single-col-4 p.heading, .page-build-price .single-col-5 p.heading, .page-build-price .single-col-4 p.heading, #trim_selector .price .disclaimer, #trim_selector .preview .colour dt, #trim_selector .number, .offers .rate, .offers .amount, .page-product-details .product-price-container .price-heading, .page-product-specs .product-price-container .price-heading, .page-product-accessories .product-price-container .price-heading, .page-product-details .product-price-container .small-price .price, .page-product-specs .product-price-container .small-price .price, .page-product-accessories .product-price-container .small-price .price, .page-product-details .product-price-container .large-price .price, .page-product-specs .product-price-container .large-price .price, .page-product-accessories .product-price-container .large-price .price, .product-specs .product-price-container .price-heading, .product-specs .product-price-container .small-price .price, .product-specs .product-price-container .large-price .price, .product-accessories .product-price-container .price-heading, .product-accessories .product-price-container .small-price .price, .product-accessories .product-price-container .large-price .price, .page-product-listing .product .price-heading, .sliders-box-group .labels span, .sliders-box-group .field label, .page-help-me-choose-results .center-container .result .price strong, .page-help-me-choose-results .center-container .result .price span, .page-product-listing .product .price strong, .wrapper_columns .product .price strong, .page-product-listing .toolbox-top-alt .price-heading, .page-product-listing .toolbox-top-alt .small-price .price, .page-product-listing .toolbox-top-alt .large-price .price, .page-safety ul.circled-number .counter, .page-accessories .find .product .product_name .price, .page-accessories .find .product .product_name .price-label, .navigation-dropdown .product-item .product-pricing .price-heading, .navigation-dropdown .product-item .product-pricing .price, .page-product-details .product_wrapper .product-options-container .years .year span, .page-product-details .product_wrapper .product-options-container .years .year a, .page-product-details .product_wrapper .product-options-container .option .options-heading, .page-compare-results div.selection_wrap div.item .fc.final.price strong, .page-compare-results table.tbl.tbl_compare_result tr.first.top th.compare div.model span, .page-compare-results table.tbl.tbl_compare_result tr.first.top span.price, .page-compare-results div.selection_wrap div.item .fc.final.price strong, .page-compare-results .special strong, .page-compare-results .final strong, .page-compare-results table.tbl.tbl_compare_result tr.top th.compare .original strong, .page-parts-service .maintenance .recommended .model .model_detail .large-price, .page-news-events .event-type, .page-product-listing .choose .all-products-steps li .counter',
			options: {fontFamily: F.FONT_NATIONAL_SEMIBOLD}
		},
		{
			elements: '.fnat, h1:not(.head_compare_links), .h1, #popup-form-updates h2, .page-build-price .accessories-slider h2, .large-inner-menu li a, .page-build-price .choice, .page-product-listing #feature_area .feature_header .h2, .powerhouse_dealer, .exclusive_honda_dealer, .page-find-a-dealer-details-1 h2, .h4, h5, .h5, h2.detail, li.consider .product .product_name, .page-product-details .product_name h2, .page-product-specs .product_name h2, .page-product-accessories .product_name h2, .page-product-specs .content_container h2:not(.small), .page-product-details .product_heading h3, .page-product-specs .product_heading h3, .page-product-accessories .product_heading h3, .page-product-listing .compare .product .list-steps, .page-product-listing .compare .product .add-model, .page-product-listing .content_section h2, .sliders-box-group h3, .page-product-details .recently_viewed h2, .page-product-details .content_container .recently_viewed .recently_viewed_product .product .description h3, .page-product-listing #feature_area .feature_box .heading, .page-help-me-choose-question .top-container .subheading-box .question-number, .page-help-me-choose-question .wizard-progress-count .wizard-progress-number, .page-help-me-choose-question .pagination-controls .btn a, .page-accessories .find .content_section .h2, .bar h2, .page-finance #finance-calculator .tbl_payment_calculator_recalculate .currency div, .navigation-dropdown .content_section .why-buy .heading, .navigation-dropdown .product-item .heading, .page-compare-results div.selection_wrap div.item .fc.final.price span, ul.sub-menu-alt li a, .page-parts-maintenance .content_container .recommended .model .model_detail h2, .page-compare-results div.selection_wrap div.item h3.price-heading, .page-honda-advantage h2.white, .page-safety .sub_content .conversion-chart td, .page-product-specs-print .product-header .product-name, .page-parts-service .maintenance .recommended .model .model_detail h2, .page-current-offers-provincial-landing h2, .page-honda-advantage .h4_sun_icon, .page-product-listing .choose .all-products-steps li .list-copy, .page-product-details .content_container .specs dl.inner-two-col dd h2, .page-commerical-product ul.commercial-touts li h3',
			options: {fontFamily: F.FONT_NATIONAL}
		},
		{
			elements: '.fnats, .small-sub-heading, .page-build-price .model-year-heading',
			options: {fontFamily: F.FONT_NATIONAL_SEMIBOLD}
		},
		{
			elements: '.page-product-details .content_container .download, a.btn_large > span, a.btn_download_pdf, .page-product-listing .categories li',
			options: {fontFamily: F.FONT_NATIONAL, hover:true}
		},
		{
			elements: '.flex_bar:not(.alternate) span, .page-build-price .popup-form-content .h2, #book_test_drive .h1, .editor .h3, .page-build-price .model-years .dd, .editor h2, #match_landing h3, .page-build-price .product-details h2, .finance-dropout .finance-body .bottom .disclaimer p, h4, h2.search-options, .finance-dropout .finance-heading dd p.price, .finance-dropout .finance-body .top table th',
			options: {fontFamily: F.FONT_NATIONAL}
		},
		{
			elements: 'a.btn:not(.delayed)',
			options: {fontFamily: F.FONT_NATIONAL_EXTRABOLD, hover:true}
		},
		{
			elements: '.tabs_horizontal li > a, .tabs_horizontal_fixed li > a, #language a',
			options: {
				fontFamily: F.FONT_NATIONAL,
				hover: true
			}
		},
		{
			elements: '.tabs_horizontal .dd a, .tabs_horizontal_fixed .dd a',
			options: {
				fontFamily: F.FONT_NATIONAL,
				hover: true
			}
		},
		{
			elements: '#secondary_nav a',
			options: {
				fontFamily: F.FONT_FOUNDERS_CONDENSED,
				hover: true
			}
		},
		{
			elements: '.page-parts-service .genuine-oils-chemicals .category a, .page-parts-service .genuine-oils-chemicals .category h3',
			options: {
				fontFamily: F.FONT_NATIONAL,
				hover: true
			}
		}
	];
};

define(['i18n!nls/global', 'ext/jquery.jobqueue', 'ext/common'], function(G) {

F.global_get = function(key) {
    if (!(key in G)) {
        return null;
    }
    return G[key];
};



Cufon.refresh = $.browser.msie && $.browser.version.match(/^(6|7)\./) ? function() {} : Cufon.refresh;

F.cufon_refresh_replace = function(ctx) {
	var typefaces = F.get_typefaces();
	
	$.each(typefaces, function(index, face) {
		if ($(face.elements, ctx).size() > 0) {
			Cufon.refresh(face.elements);
		}
	});
};
	
F.requires_flash_message = function(context) {
	var noscript = $('.flash_disabled', context);
	if (noscript.size() > 0) {
		noscript.show();
	}
	else {
		if (global_get('lang') == 'fr') {
			$(context).html('<div class="flash_disabled"><div class="flash_logo"><p class="message"><span class="fnat">Vous devez mettre à jour Adobe Flash Player pour voir ce contenu.</span></p><p><a class="btn primary" href="http://get.adobe.com/flashplayer/" rel="external" target="_blank"><span>Téléchargez-le sur le site d’Adobe</span></a></p></div></div>');
		}
		else {
			$(context).html('<div class="flash_disabled"><div class="flash_logo"><p class="message"><span class="fnat">You need to upgrade your Adobe Flash Player to view this content.</span></p><p><a class="btn primary" href="http://get.adobe.com/flashplayer/" rel="external" target="_blank"><span>Download it from Adobe</span></a></p></div></div>');
		}
		$('.flash_disabled', context).show();
		cufon_refresh_replace(context);
	}
};
	
// disable Cufon for Googlebot, IE 6 & 7
if (W.navigator.userAgent.match(/Googlebot\/[23456789]\./) || $.browser.msie && $.browser.version.match(/^(6|7)\./)) {
	Cufon.replace = function() { return Cufon; };
}
	

// don't process for the second time
if ($('body').hasClass('cufon_processed')) {
	return;
}
	
if ($.browser.msie && $.browser.version.match(/^9\./)) {
	Cufon.set('engine', 'canvas');
}
	
// Cufons
Cufon.now();

var typefaces = F.get_typefaces();
for (var index = 0; index < typefaces.length; index++) {
//        $(typefaces[index].elements).each(function() {
//            $(this).data('cufonText', $(this).text());
//        });
	Cufon.replace($(typefaces[index].elements), typefaces[index].options, true);
}

}); // of define()
})(jQuery, window, document, window, require, define);
