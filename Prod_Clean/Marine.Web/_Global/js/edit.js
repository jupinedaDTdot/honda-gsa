// enable edit button
;(function($, F, W, undefined) {
    F.edit_mode = true;

    W.show_edit_buttons = function() {
        $('a.edit-button').each(function() {
            var self = $(this);
            self.show().parent().addClass('has-edit-button');
        });

        $('.edit-container-should-hide').show();

      F.edit_mode = true;
    };

    W.hide_edit_buttons = function() {
        $('a.edit-button').each(function() {
            var self = $(this);
            self.hide().parent().removeClass('has-edit-button');
        });

        $('.edit-container-should-hide').hide();

      F.edit_mode = false;
    };
})(jQuery, window, window);

if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
            ;
        });
    };
}

var publishTicker = function () {

    var tmplAbort = "{0} CMS element publish cancelled.";
    var tmplCompleted = "Data of CMS element {0} has been published to {1} environment.";
    var tmplProcessing = "Data of CMS element {0} publish in progress to {1} environment.";
    var tmplFileCopying = "{0} copied.";

    var lastLog = {};
    var tickerTimer = null;
    var tickerDetailTimer = null;
    var loadingDots = null;

    var TickerUpdate = function () {
        $.ajax({
            url: "/Content/Utility/TickerUpdate",
            dataType: "json",
            success: function (data) {
                if (data) {
                    lastLog = {};
                    for (var i = 0; i < data.length; i++) {
                        lastLog['lastLog[' + i + '].Key'] = data[i];
                        lastLog['lastLog[' + i + '].Value'] = -1;
                    }

                    $('#publishTicker').slideDown('slow');
                    // StartLoadingDots();
                } else {
                    lastLog = {};
                    $('#publishTicker').slideUp('slow');
                    // StopLoadingDots();
                }
            }
        });
    },

    StartTickerUpdate = function () {
        TickerUpdate();

        StopTickerUpdate();
        tickerTimer = setInterval(TickerUpdate, 1000 * 20)
    },

    StopTickerUpdate = function () {
        if (tickerTimer && tickerTimer != null) {
            clearInterval(tickerTimer);
            tickerTimer = null;
        }

        // StopLoadingDots();
    },

    TickerDetailsPopup = function () {

        $('#publishTicker').click(function () {
            StopTickerUpdate();

            require(['ui/jquery.ui.core', 'ui/jquery.ui.widget', 'ui/jquery.ui.position', 'ui/jquery.ui.button', 'ui/jquery.ui.dialog', 'ui/jquery.ui.draggable'], function () {

                $("#tickerDetailsPopup").dialog({
                    open: function (event, ui) {
                        $('#tickerDetails').html('');
                        StartTickerDetailsUpdate();
                    },
                    close: function (event, ui) {
                        StopTickerDetailsUpdate();
                        StartTickerUpdate();
                    },
                    position: { my: "center", at: "center top", of: window },
                    width: 695,
                    dialogClass: "tickerDialog",
                    resizable: false,
                    modal: true
                });
            });
        });
    },

    TickerDetailsUpdate = function () {
        $.ajax({
            url: "/Content/Utility/TickerDetails",
            dataType: "json",
            type: 'POST',
            data: NextIndex(lastLog) > 0 ? lastLog : { lastLog: null },
            success: function (response) {
                if (response && response.Data) {

                    var data = response.Data;
                    if (response.isRecent) {
                        $('#recentPublish').show('slow');
                        $('#publishTicker').slideUp('slow');
                        $('#pt_loading').hide();
                    } else {
                        $('#recentPublish').hide('slow');
                        $('#publishTicker').slideDown('slow');
                    }

                    var $tickerDetails = $('#tickerDetails');
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];

                        // update the log collection
                        if (!response.isRecent && response.realLogCount > i) {
                            var idx = getKeyByValue(lastLog, item.scheduleID);
                            if (idx != undefined) {
                                var currLog = lastLog[idx];
                                if (currLog < item.ID) {
                                    lastLog[idx] = item.ID;
                                }
                            } else {
                                var nextIndex = NextIndex(lastLog);
                                lastLog['lastLog[' + nextIndex + '].Key'] = item.scheduleID;
                                lastLog['lastLog[' + nextIndex + '].Value'] = item.ID;
                            }
                        }

                        // update view
                        var scheduleDivID = '#sch_' + item.scheduleID;
                        var titleDivID = '#ttl_' + item.scheduleID;
                        var mediaDivID = '#media_' + item.scheduleID;

                        var elementName = '';
                        var pageUrl = '';
                        if (item.StepTypeID == 1 && item.StepData && item.StepData != '') {
                            var splits = item.StepData.split('$');
                            if (splits.length == 2) {
                                elementName = splits[0];
                                pageUrl = splits[1];
                            }
                        }

                        var $divschedule = $(scheduleDivID);
                        if ($divschedule.length == 0) {

                            $divschedule = $('<div class="schedule" scheduleID="' + item.scheduleID + '" id="sch_' + item.scheduleID + '" cmsElement="' + elementName + '" pageUrl="' + pageUrl + '">' +
                                                    '<div class="title" id="ttl_' + item.scheduleID + '"><span class="text"/></div>' +
                                                    '<a href="' + pageUrl + '" class="gotoPage" title="Click here to go to the page">Go to page</a>' +
                                                    '<a href="#" class="btn_abortPublish abortPublsih" title="Abort publish">Abort Publish</a>' +
                                                    '<div class="media" id="media_' + item.scheduleID + '"></div><br class="clearfloat"/></div>');

                            $('#pt_loading').hide();
                            $tickerDetails.show().prepend($divschedule);
                        }

                        //scheduleStatus
                        //1 = Pending
                        //2 = Completed
                        //3 = Cancelled
                        //4 = Failed
                        //5 = Processing

                        if (item.StepTypeID == 1) {
                            var $divTitle = $(titleDivID);

                            //publish started
                            if (item.scheduleStatus == 5 || item.StepName == "Started") {
                                $divTitle.find('span.text').text(tmplProcessing.format(elementName, item.StepTarget));
                                $divschedule.find('.abortPublsih').show();
                                //$divschedule.find('.gotoPage').hide();
                            }

                            //publish completed
                            if (item.scheduleStatus == 2 || item.StepName == "Completed") {
                                $divTitle.find('span.text').text(tmplCompleted.format(elementName, item.StepTarget));
                                $divschedule.find('.abortPublsih').hide();
                                //$divschedule.find('.gotoPage').show();
                            }

                            //publish aborted
                            if (item.scheduleStatus == 3 || item.StepName == "Aborted") {
                                $divTitle.find('span.text').text(tmplAbort.format($divschedule.attr('cmsElement')));
                                $divschedule.find('.abortPublsih').hide();
                                //$divschedule.find('.gotoPage').show();
                            }
                        }

                        // publishing media files
                        if (item.StepTypeID == 2) {
                            var $divMedia = $(mediaDivID);
                            var mediaItem = $('#mediaItem_' + item.ID);
                            if (mediaItem.length == 0 && item.StepData) {
                                var shortPath = item.StepData.split();
                                $divMedia.append('<span class="mediaItem" id="mediaItem_' + item.ID + '"><a href="' + item.StepData + '" title=' + item.StepData + '" target="_blank">' +
                                                    tmplFileCopying.format(item.StepData.replace(/^.*[\\\/]/, '')) + '</a></span>');
                            }
                        }
                    }
                }
            }
        });
    },

    StartTickerDetailsUpdate = function () {
        TickerDetailsUpdate();

        StopTickerDetailsUpdate();
        tickerDetailTimer = setInterval(TickerDetailsUpdate, 5000);
    },

    StopTickerDetailsUpdate = function () {
        if (tickerDetailTimer && tickerDetailTimer != null) {
            clearInterval(tickerDetailTimer);
            tickerDetailTimer = null;
        }
    },

    BindAbortPublishEvent = function () {
        $(document).on("click", ".abortPublsih", function () {
            var abortSchedule = $(this).parents('.schedule').attr('scheduleID');
            $.ajax({
                url: "/Content/Utility/AbortPublish?scheduleID=" + abortSchedule,
                dataType: "json",
                success: function (data) {
                    if (data) {
                        Alert("aborted");
                    } else {
                        Alert("abort failed");
                    }
                }
            });
        });
    },

    getKeyByValue = function (arr, value) {
        for (var prop in arr) {
            if (arr.hasOwnProperty(prop)) {
                if (arr[prop] === value &&
                    prop.indexOf('.Key', prop.length - '.Key'.length) !== -1) {
                    return prop.replace('.Key', '.Value');
                }
            }
        }
        return undefined;
    },

    NextIndex = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }

        return size / 2;
    },

    StartLoadingDots = function (obj) {
        var loadingDotSpan = document.getElementById('loadingDots');

        StopLoadingDots();
        //        loadingDots = setInterval(function () {
        //            if ((loadingDotSpan.innerHTML += '.').length == 5)
        //                loadingDotSpan.innerHTML = '.';
        //        }, 500);
    },

    StopLoadingDots = function () {
        if (loadingDots && loadingDots != null) {
            clearInterval(loadingDots);
            loadingDots = null;
        }
    };

    TickerDetailsPopup();
    BindAbortPublishEvent();
    StartTickerUpdate();
}

$(document).ready(publishTicker);



