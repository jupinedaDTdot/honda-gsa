;(function($){

var defaults = {
	labelViewAllKeyFeatures: 'View All Key Features',
	labelPlayVideo: 'PLAY VIDEO',
	labelKeyFeatures: 'Key Features',
	features: [], //array of feature objects with following keys: title, description, thumb, image, videoId
	video_xml_location: '', //location of XML with video playlist, so video poster and src can be referenced by video if from key features.
	scale: 121 / 375.0,
	skin: ''
};

$.widget('honda.keyfeatures', {

	options: defaults,

	_init: function() {
		var self = this, o = this.options, element = this.element;
		self.currentFeatureIdx = -1;
		require(['includes/jquery.honda.videoplayer', 'ui/jquery.effects.core', 'ext/jquery.transform'], function () {
			self.getVideoDetails();
			self.buildHTML();
			self.bindEvents();
			self._created();
		});
	},

	//as feature reference video by id we need to find out video src and video poster for each feature with video id
	getVideoDetails: function() {
		var self = this, o = this.options, element = this.element;

		$.ajax({
			type: "GET",
			url: o.video_xml_location,
			dataType: "xml",
			success: function(xml) {
				if ($(xml).find('videos').length) {
					var videos = [];

					//build all videos to self.videos array with number as a key
					$(xml).find('video').each(function(idx, video_tag) {
						videos[idx + 1] = {
							src: $(video_tag).attr('url'),
							poster: $(video_tag).attr('toutImage')
						};
					});

					//extend each feature with video src and poster
					$.each(o.features, function(idx, feature) {
						if (feature.videoId && feature.videoId in videos) {
							feature.videoPoster = videos[feature.videoId].poster;
							feature.videoSrc =videos[feature.videoId].src;
						}
					});

					//if currently displayed feature had an videoid then we show video icon
					if (o.features[self.currentFeatureIdx].videoId) {
						element.find('.info .video').show();
					}
				}
			}
		});
	},

	buildHTML: function () {
		var self = this, o = this.options, element = this.element;

		element.empty();
		element.html(
			'<div class="keyfeatures"><div class="layer images"></div>\
				<div class="layer selector">\
					<a class="close" href="#"></a>\
					<h2 class="fnatb">'+o.labelKeyFeatures+'</h2>\
					<div class="features"></div>\
				</div>\
				<div class="layer shade"></div>\
				<div class="layer arrows">\
					<a class="prev" href="#"></a>\
					<a class="next" href="#"></a>\
				</div>\
				<div class="layer info">\
					<div class="column copy">\
						<h3 id="key_feature_title" class="fnatb"></h3>\
						<div id="key_feature_copy" class="fnat"></div>\
					</div>\
					<div class="column all-key-features">\
						<a href="#" class="fnatb">'+o.labelViewAllKeyFeatures+'</a>\
					</div>\
					<div class="column video">\
						<a href="#">\
							<img src="/_Global/img/keyfeatures/keyfeatures-video-btn.png" alt="">\
							<span>'+o.labelPlayVideo+'</span>\
						</a>\
					</div>\
				</div>\
				<div class="layer video-shade">\
					<a class="close" href="#"></a>\
				</div>\
				<div class="layer video-container"></div></div>');

		var $images = element.find('.images'),
			$features = element.find('.features');

		$.each(o.features, function(idx, feature){

			var lastInRow = ((idx + 1) % 5 == 0) ? 'last-in-row' : '';

			$('<img>')
				.attr('src', feature.image)
				.css('display', 'none')
				.appendTo($images);

			var divFeature = $('<div>')
				.addClass('feature '+lastInRow);

			var a = $('<a>')
				.attr('href', 'javascript:void(0)')
				.addClass('thumb');
		  
      if (/\?/.test(feature.thumb)) {
        feature.thumb += "&_r=" + Math.random();
      }
		  else {
        feature.thumb += '?_r=' + Math.random();
      }

			var img = $('<img>')
				.attr('src', feature.thumb)
		    .load(function () {
		      
		      img.css({
		        marginLeft: (121 - 375) / 2,
		        marginTop: (66 - 222) / 2
		      })

          // BUG rotate 0.01deg here to circumvent a Gecko bug,
		      // @see https://bugzilla.mozilla.org/show_bug.cgi?id=663776
		      if ($.browser.mozilla) {
		        img.transform({ scale: o.scale, origin: ['50%', '50%'], rotate: '0.01deg' });
		      }
		      else {
  		      img.transform({ scale: o.scale, origin: ['50%', '50%'] });
		      }

		      a.mouseenter(function(e) {
			      self.featureOver($(this));
		      });

		      a.mouseleave(function(e) {
			      self.featureOut($(this));
		      })

		      a.click(function(e) {
			      e.preventDefault();
			      self.selectFeature(this);
		      })

		    })
        .appendTo(a);

			a.appendTo(divFeature);

			var divTitle = $('<div>')
				.addClass('title fnatb')
				.html(feature.title)
				.appendTo(divFeature);

			divFeature.appendTo($features);
		})

		self.transitionToFeature(0, true);
	},

	bindEvents: function(){
		var self = this, o = this.options, element = this.element;

		element.find('.prev').click(function(e) {
			e.preventDefault();
			self.transitionToFeature(self.getPrevIdx());
		})

		element.find('.next').click(function(e) {
			e.preventDefault();
			self.transitionToFeature(self.getNextIdx());
		})

		element.find('.selector .close').click(function(e) {
			e.preventDefault();
			self.hideSelector();
		})

		element.find('.all-key-features a').click(function(e) {
			e.preventDefault();
			self.showSelector();
		})

		element.find('.video a').click(function(e){
			e.preventDefault();
			self.launchVideo();
		})

		element.find('.video-shade, .video-shade a.close').click(function(e) {
			e.preventDefault();
			self.hideVideo();
		})

	},

	getPrevIdx: function() {
		var self = this,
			o = this.options,
			prevIdx = self.currentFeatureIdx - 1;
		return (prevIdx < 0) ? o.features.length - 1 : prevIdx;
	},

	getNextIdx: function() {
		var self = this,
			o = this.options,
			nextIdx = self.currentFeatureIdx + 1;
		return (nextIdx >= o.features.length) ? 0 : nextIdx;
	},


	transitionToFeature: function(idx, skipShade) {
		var self = this, o = this.options, element = this.element;
		var $images = element.find('.images'),
			$info = element.find('.info');

		if (self.currentFeatureIdx != idx) {
			self.currentFeatureIdx = idx;

			$images.find('img').not(':eq('+idx+')').fadeOut();
			$images.find('img:eq('+idx+')').fadeIn();

			$info.find('#key_feature_title').html(o.features[idx].title);
			$info.find('#key_feature_copy').html(o.features[idx].description);

			if (o.features[idx].videoSrc) {
				$info.find('.video').show();
			} else {
				$info.find('.video').hide();
			}

			self.ensureInfoColumnsHeight();
			if (!skipShade) {
				self.ensureShadeHeight();
			}

			self._change(null, {idx: idx, skipShade: skipShade});
		}
	},


	ensureInfoColumnsHeight: function() {
		var self = this, o = this.options, element = this.element;
		var $info = element.find('.info');

		var copy_height = $info.find('.copy').height() - 15; //.copy has smaller vertical paddings than .video and .all-key-features, that's why we adjust it

		//padding for all key features column is large when video icon is visible as it should be cebtered relatively to video icon
		if ($info.find('.video').is(':visible')) {
			var video_height = $info.find('.video').height()
			$info.find('.all-key-features').css({paddingTop: 18, paddingBottom: 12})
		} else {
			var video_height = 0;
			$info.find('.all-key-features').css({paddingTop: 10, paddingBottom: 4})
		};
		var max_height = Math.max(28, copy_height, video_height);
/*
		console.log("copy_height");
		console.log(copy_height);

		console.log("video_height");
		console.log(video_height);

		console.log("max");
		console.log(max_height);*/

		$info.find('.all-key-features').height(max_height);
		$info.find('.video').height(max_height);
	},

	ensureShadeHeight: function(){
		var self = this, o = this.options, element = this.element;
		var $shade = element.find('.shade');
		$shade.height(self.getInfoHeight());
	},

	selectFeature: function(a){
		var self = this,
			$a = $(a),
			featureIdx = $a.closest('.feature').prevAll().length;

		self.transitionToFeature(featureIdx);
		self.hideSelector();
	},

	showSelector: function() {
		var self = this, o = this.options, element = this.element;
		var featureHideTimeout = 0,
			featureHideDelay = 30,
			$info = element.find('.info'),
			$arrows = element.find('.arrows'),
			$title = element.find('.selector h2'),
			$close = element.find('.selector a.close'),
			$shade = element.find('.shade'),
			$featuresContainer = element.find('.features'),
			$featuresCollection = element.find('.feature');

		$arrows.fadeOut('fast');
		$info.fadeOut('fast', function(){
			$shade.animate({height: 445}, 300);

			$featuresContainer.show()
			$.each($featuresCollection.get().reverse(), function(idx, feature){
				setTimeout(function(){
					$(feature).animate({opacity: 1}, 200)},
					featureHideTimeout
				);
				featureHideTimeout += featureHideDelay;
			})

			setTimeout(function(){
				$title.css('visibility', 'visible'); //need for IE8
				$title.animate({opacity: 1}, 200);
				$close.animate({opacity: 1}, 200);
				self._expand();
			}, 300);
		});
	},

	hideSelector: function(){
		var self = this, o = this.options, element = this.element;
		var featureHideTimeout = 0,
			featureHideDelay = 30,
			$info = element.find('.info'),
			$arrows = element.find('.arrows'),
			$title = element.find('.selector h2'),
			$close = element.find('.selector a.close'),
			$shade = element.find('.shade'),
			$featuresContainer = element.find('.features'),
			$featuresCollection = element.find('.feature'),
			shadeTargetHeight = self.getInfoHeight();

		$title.animate({opacity: 0}, 100, function(){$title.css('visibility', 'hidden');}); //need visibility hidden for IE8
		$close.animate({opacity: 0}, 100);

		$.each($featuresCollection, function(idx, feature){
			setTimeout(function(){
				$(feature).animate({opacity: 0}, 200)},
				featureHideTimeout
			);
			featureHideTimeout += featureHideDelay;
		})

		setTimeout(function(){
			$shade.animate(
				{height: shadeTargetHeight},
				400,
				function(){
					if (self.isArrowsNeeded()) {
					$arrows.fadeIn();
					}
					$featuresContainer.hide();
					self._collapse();
				}
			);

			$info.fadeIn();
			self.ensureInfoColumnsHeight();
		}, 100);
	},

	isArrowsNeeded: function(){		
		return this.options.features.length > 1;
	},

	getInfoHeight: function(){
	  var info = this.element.find('.info');
		return info.height() + 25;
	},

	featureOver: function($a){
		var self = this;
		$a.stop().css({margin: 0, border: '2px solid red'});

		$a.find('img').stop().animate({
		  scale: 1
		}, 6180, 'swing');
		self._over();
	},

	featureOut: function($a){
	  var self = this, o = this.options;
		$a.stop().animate({
			borderTopColor: "#333",
			borderLeftColor: "#333",
			borderRightColor: "#333",
			borderBottomColor: "#333"
		}, 500, function(){
			$a.css({margin: '2px', border: 'none'});
		} );

		$a.find('img').stop().animate({
		  scale: o.scale
		}, 1000)
		self._out();
	},


	launchVideo: function(){
		var self = this, o = this.options, element = this.element;
		var $videoShade = element.find('.video-shade'),
			$videoConainer = element.find('.video-container');

		$videoShade.fadeIn('fast');
		$videoConainer.fadeIn('fast');
		$videoConainer.append('<div class="video-player"></div>');

	  var opts = $.extend({ }, global_get('player.standard'), {
			width: 600,
			height: 342,
			videos: [{
				src : o.features[self.currentFeatureIdx].videoSrc,
				poster: o.features[self.currentFeatureIdx].videoPoster
			}],
			overlay_play_button_visible: true,
			skin: o.skin
		});

		$videoConainer.find('.video-player').videoplayer(opts);
	},

	hideVideo: function(){
		var self = this, o = this.options, element = this.element;
		var $videoShade = element.find('.video-shade'),
			$videoConainer = element.find('.video-container');

		$videoShade.fadeOut('fast');
		$videoConainer.fadeOut('fast');

		$videoConainer.find('.video-player').remove();

	},

	_created: function(event, ui) {
		this._trigger('created', event, ui)
	},

	_change: function(event, ui) {
		this._trigger('change', event, ui)
	},

	_expand: function(event, ui) {
		this._trigger('expand', event, ui)
	},

	_collapse: function(event, ui) {
		this._trigger('collapse', event, ui)
	},

	_over: function(event, ui) {
		this._trigger('over', event, ui)
	},

	_out: function(event, ui) {
		this._trigger('out', event, ui)
	}


});

})(jQuery);

