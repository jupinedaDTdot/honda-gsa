;(function() {
window._globals = {
	base_url: 'http://www.honda.ca',
	lang: 'fr',
	'format.price': '#,###.00 $',
	'format.price_rounded': '#,### $',
	'format.dateSettings': {
		closeText: 'Fermer',
		prevText: '&#x3c;Préc',
		nextText: 'Suiv&#x3e;',
		currentText: 'Courant',
		monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin',
		'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		monthNamesShort: ['Jan','Fév','Mar','Avr','Mai','Jun',
		'Jul','Aoû','Sep','Oct','Nov','Déc'],
		dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
		dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
		dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	},
	environment: (window.__staging ? 'STAGING' : 'LIVE'),
	// urls
	'url.base': '',
	'url.province_dealers': '/modelpages/BookTestDriveDealers.aspx?lang=fr&province=',
	'url.finance_calculator': '/tools/buildit/financialcalculator/financial_panel.aspx',
	'url.finance_calculator_summary': '/tools/buildit/financialcalculator/summary.aspx',
	'url.finance_calculator_msrp': function() {
		// build the msrp url based on vehicle and province selector
		var province = $('#province', this).val();
		var vehicle = eval('(' + $('#vehicle', this).val() + ')');
		var included = $('#include_tax', this).attr('checked');
		var url = '/buildittool/'+global_get('environment')+'/data/JSON/fr/' + province + '/' + vehicle.model_year + '/' + vehicle.model_key
			+ '/' + vehicle.trim_level_key + '/financial_presets_' + vehicle.transmission_key + '.json?include_fees='
			+ (included ? 'true' : 'false');
		return url;
	},
	'finance_calculator.lease_contact_dealer': "*Voir le concessionnaire pour plus d'infomations.",
	'finance_calculator.finance_contact_dealer': "*Voir le concessionnaire pour plus d'infomations.",
	// honda accessories
	'url.honda_accessories': function() {
		var province = $('#accessory_province').val();
		province = province ? province : 'ON';
		var uri = $('#accessory_trim').val();
		uri = uri.split('/');
		// temporarily commented out for civic hybrid launch without accessories
        uri.splice(uri.indexOf('JSON')+2, 0, province); // insert province into the URI
        var transmission = uri.pop();
	  uri.pop(); // colour
	  uri.pop(); // trim
        uri.push(transmission);
		return uri.join('/') + '/accessories.json';
	},
	'url.honda_accessories.color_thumbnail_prefix': function(key) {
	  // /buildittool/LIVE/data/JSON/en/accessories/2012/fit/dx/milano_red/895-Manual
//        \LIVE\Data\JSON\assets\accessories\2012\fit\dx\milano_red\895-Manual/protection_package_thumbnail.jpg
		var uri = $('#accessory_trim').val();
        uri = uri.split('/');
        var transmission = uri.pop();
        var color = uri.pop(); // color
        var trim = uri.pop();
        var model = uri.pop();
	      var year = uri.pop();
        var acc = uri.pop();
        uri.pop(); // lang
        uri.push('assets');
        uri.push(acc);
	      uri.push(year);
        uri.push(model);
        uri.push(trim);
        uri.push(color);
        uri.push(transmission);
		return uri.join('/');
	},
	'url.honda_accessories.thumbnail_prefix': function(key) {
	  // /buildittool/LIVE/data/JSON/en/accessories/2012/fit/dx/milano_red/895-Manual
	  // /buildittool/LIVE/data/JSON/assets/accessories/2012/fit/895-Manual/interior/all_weather_floor_mats_thumbnail.jpg
        var uri = $('#accessory_trim').val();
        uri = uri.split('/');
        var transmission = uri.pop();
        var color = uri.pop(); // color
        var trim = uri.pop();
        var model = uri.pop();
	      var year = uri.pop();
        var acc = uri.pop();
        uri.pop(); // lang
        uri.push('assets');
        uri.push(acc);
	      uri.push(year);
        uri.push(model);
        uri.push(transmission);
		return uri.join('/') + '/' + key;
	},
	'honda_accessories.section_titles': {
		packages: {
			title: 'Ensembles'
		},
		entertainment: {
			title: 'Divertissement'
		},
		exterior: {
			title: 'Extérieur'
		},
		interior: {
			title: 'Intérieur'
		}
	},
	'accessory.slidedown_open' : true,
	// honda warranty
	'url.honda_warranty': function() {
		var uri = $('#warranty_trim').val();
		return uri + '/warranty.json';
	},
	// honda match
	'url.honda_match.models': function() {
		return '/buildittool/'+global_get('environment')+'/data/JSON/fr/models.json';;
	},
	'url.honda_match.models_financial': function() {
		var province = $.cookie('hondaprovince');
		province = province ? province : 'ON';
		return '/buildittool/'+global_get('environment')+'/data/JSON/fr/'+province+'/models_financial.json?include_fees=false';
	},
	'url.honda_match.thumbnail_path': '/buildittool/LIVE/data/json/assets/{model_key}/hondaca_modelNav.png',
	'url.honda_match.thumbnail_width': 140,
	'url.honda_match.thumbnail_height': 175,
	'honda_match.format': {
		suffix: {
			msrp: '$',
			lease: '$/m',
			finance: '$/m',
			seats: ' sièges',
			fuel_efficiency: ' L/100km'
		},
		precision: {
			msrp: 1,
			lease: 0.01,
			finance: 0.01,
			seats: 1,
			fuel_efficiency: 0.1
		}
	},
	// city suggest search url
	'url.search_city': '/dealerlocator/GetCityService.aspx?sCity=',
	'url.search_suggest': '/_Global/service/GSAService.svc/SuggestiveSearch',
	'search_suggest.query_type': {
		type: 'field',
		field: 'GSA_Keywords'
	},
	'search_suggest.data': {
	    client: 'honda_suggestive',
		collections: 'honda_automotive|honda_corporate',
		lang: 'lang_fr',
		perGroup: 5,
		requiredMetaFields: '',
		partialMetaFields: '',
        filter: 0,
		_last: null
	},
	'url.search_link_fallback': {
		url: '/searchfre/',
		copy: 'Voir Tout Les Resultants'
	},
	'url.search_no_result_copy': "Désolé, nous ne trouvons pas de raccourci. Recherchez sur l'ensemble du site Honda.ca.",
	// survey urls
//	'url.survey': 'http://hondasurvey.ca/?sid=97273&lang=en',
//	'url.survey_window': 'survey_eng.html',
//	'url.survey_close_button': '/_Global/img/survey/Eng/close.gif',
	// french version
	'survey.enabled': false,
	'url.survey': 'http://hondasurvey.ca/?lang=fr&sid=',
	'url.survey_window': '/survey_fre.html',
	'url.survey_close_button': '/_Global/img/survey/Fre/close.gif',
	// arguments
	'autocomplete.search_city.min_length': 2,

    // html video player
    'player.standard': {'width':'680','height':'379', fullscreen_enabled: false, skin: 'mejs-honda', timeline_handle_shift_right: 13},

	// slideshow player
    'player.slideshow': {'type':'flash','width':'950','height':'425','hashVersion':'9','swf':'/_Global/swf/honda_slideShow.swf','flashvars':{'xml_location':'','debug': 'browser'},'params':{'allowScriptAccess':'sameDomain','allowNetworking':'all','wmode':'transparent','quality':'high','play':'true','loop':'false','menu':'false','scale':'showall','bgcolor':'#ffffff'}},
    
    // single playlist prefix
    'player.single_suffix': '/tools/3rdparty/VideoPlaylist.aspx?L=fr-CA&url=',
	
    // canada map
    'url.current_offers_map': '/_Global/svg/canada.xml',

    // google analytics account
    'tracker.account': 'UA-5158555-1',

	// recaptcha
	'recaptcha.public_key' : '6LdBLcESAAAAANjZQggpl7gk3QyauYh3INzDctO0',

    //photos popup
	'popup.mask': { color: '#000', opacity: 0.8 },
	//
	_last: null
};


})();
