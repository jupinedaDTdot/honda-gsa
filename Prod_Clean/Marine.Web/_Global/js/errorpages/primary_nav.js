$.nav = function(selector) {
	$(selector + ' li a').hover(
		function() {
			if ($(this).parents('li').hasClass('first')) {
				$(this).parents('li').attr('style','background:url(images/bg_primary_navigation.gif) 0 -40px;');
			} else if($(this).parents('li').hasClass('last')) {
				$(this).parents('li').attr('style','background:url(images/bg_primary_navigation.gif) right -40px;');
			} else {
				$(this).parents('li').attr('style','background:url(images/bg_primary_navigation.gif) -30px -40px;');
			}
		},
		function(){	$(this).parents('li').attr('style','background:none');}
	);
	$(selector + ' li.fr').attr('style','background:url(images/bg_primary_navigation.gif) right -40px;');
};