;(function($, F, W, undefined) {

    var prioritySortLow = function(a, b) {
        return b.priority - a.priority;
    };

    var prioritySortHigh = function(a, b) {
        return a.priority - b.priority;
    };

    var PriorityQueue = function(options) {
        var contents = [];

        var sorted = false;
        var sortStyle;

        if (options && options.low) {
            sortStyle = prioritySortLow;
        } else {
            sortStyle = prioritySortHigh;
        }

        var sort = function() {
            contents.sort(sortStyle);
            sorted = true;
        };

        var self = {
            pop: function() {
                if (!sorted) {
                    sort();
                }

                var element = contents.shift();

                if (element) {
                    return element.object;
                } else {
                    return undefined;
                }
            },
            queue: function() {
                return contents;
            },
            top: function() {
                if (!sorted) {
                    sort();
                }

                var element = contents[0];

                if (element) {
                    return element.object;
                } else {
                    return undefined;
                }
            },
            size: function() {
                return contents.length;
            },
            empty: function() {
                return contents.length === 0;
            },
            push: function(object, priority) {
                if (priority === undefined) {
                    priority = 0;
                }
                contents.push({object: object, priority: priority});
                sorted = false;
            }
        };

        return self;
    };

/*
 Behaviors management code
 */

$.jobqueue = function(delay) {
    this._timer = null;
    this._delay = delay || 1;
    this._queue = PriorityQueue();
};

$.JobQueue = $.jobqueue;

$.extend($.jobqueue.prototype, {
    add: function(fn, context, time, priority, queue) {
        var self = this;
        if (priority === undefined) {
            priority = 0;
        }
        var setTimer = function(time) {
            self._timer = setTimeout(function() {
                time = self.add();
                if (self._queue.size()) {
                    setTimer(time);
                }
            }, time);
        };

        if (fn) {
            self._queue.push([fn, context || document, time || self._delay, queue || self], priority);
            if (self._queue.size() == 1) {
                setTimer(time);
            }
            return undefined;
        }

        var next = self._queue.pop();
        if (!next) {
            return 0;
        }
        next[0].call(next[1] || window, next[4]);
        return next[2];
    }
    , clear: function() {
        clearTimeout(self._timer);
        this._queue = null;
        this._queue = PriorityQueue();
    }
});

$.extend($.jobqueue, {
    _timer: null
    , _delay: 2
    , _queue: PriorityQueue()
    , async: function(obj, callback) {
        setTimeout(function() {
            callback.apply(obj, arguments.slice(2));
        }, 1);
    }
}, $.jobqueue.prototype);

function try_execute(func, ctx, ajax, queue) {
    var args = Array.prototype.slice.call(arguments);
    args.shift();
    try {
        return func.apply(this, args);
    } catch (e) {
        if ($._DEBUG && typeof(W.console) != 'undefined') {
            W.console.log(e);
        }
        return undefined;
    }
}


// attach the behaviors to document ready and ajaxSuccess events
F.execute_behaviors = function(behavior_case, ctx, ajax, time, queue) {
    var e = this;
    queue = queue || $.jobqueue;

    var run_single_behavior = function(func) {
        if (time > 0) {
            queue.add(function() {
                try_execute(func, ctx, ajax);
            }, e, time, undefined, queue);
        }
        else {
            try_execute(func, ctx, ajax);
        }
    };

    $.each($.behaviors, function(behavior, func) {
        // all behaviors
        if ($.behavior_cases[behavior_case] == 'all') {
            run_single_behavior(func);
        }
        // behavior case is an array and contains the behavior
        else if (typeof($.behavior_cases[behavior_case]) == 'object' && $.behavior_cases[behavior_case].indexOf(behavior) !== -1) {
            run_single_behavior(func);
        }
        // behavior case is a regular expression
        else if (typeof($.behavior_cases[behavior_case]) == 'function' &&
            $.behavior_cases[behavior_case].constructor.toString().match(/regexp/i) != null) {
            if (behavior.match($.behavior_cases[behavior_case])) {
                run_single_behavior(func);
            }
        }
    });
};

})(jQuery, window, window);

