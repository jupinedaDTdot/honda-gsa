;(function($, D, W, F, require, undefined) {
$.reduce = function(arr, valueInitial, fnReduce)
{
	$.each( arr, function(i, value)
	{
		valueInitial = fnReduce.apply(value, [valueInitial, i, value]);
	});
	return valueInitial;
};
    
require(['ui/jquery.ui.core', 'ui/jquery.ui.widget'], function() {

$.widget('commonlib.widget', {
    options:  {
        position: {
            my: 'right bottom',
            at: 'left top',
            collision: 'fit'
        }
    },
    setNeedsDisplay: function () {
      var w = this.widget()[0], n = document.createTextNode('');
      w.appendChild(n);
      setTimeout(function() {
        n.parentNode.removeChild(n);
      }, 0);
    },
    _ensurePosition: function() {
        var o = this.options,
            widget = this.widget();

        // ensure position 'of' is set
        if (o.position['of'] === undefined) {
            o.position.of = widget;
        }

        if (widget.data('position')) {
            var p = widget.data('position');
            if (p.toLocaleLowerCase() === 'left top') {
                o.position.my = 'right bottom';
                o.position.at = 'left top';
            }
            else if (p.toLocaleLowerCase() === 'left bottom') {
                o.position.my = 'right top';
                o.position.at = 'left bottom';
            }
            else if (p.toLocaleLowerCase() === 'right top') {
                o.position.my = 'left bottom';
                o.position.at = 'right top';
            }
            else if (p.toLocaleLowerCase() === 'right bottom') {
                o.position.my = 'left top';
                o.position.at = 'right bottom';
            }
        }

        if (widget.data('position-my')) {
            o.position.my = widget.data('position-my');
        }

        if (widget.data('position-at')) {
            o.position.at = widget.data('position-at');
        }

        if (widget.data('position-offset')) {
            o.position.offset = widget.data('position-offset');
        }

        o.position.collision = widget.data('position-collision') || o.position.collision;
    }
});

$.commonlib.collections = $.commonlib.collections || {};

$.commonlib.collections.ReleasePool = function(arr) {
    if (arr) {
        for (var i = 0; i < arr.length; i++) {
            this.add(arr[i]);
        }
    }
};

var ReleasePool = $.commonlib.collections.ReleasePool;

ReleasePool.prototype.retain = function() {
    for (var index = 0; index < arguments.length; index ++) {
        if (!this.hasOwnProperty(arguments[index])) {
            this[arguments[index]] = 1;
        }
        else {
            this[arguments[index]] ++;
        }
    }
};

ReleasePool.prototype.retainAll = function() {
    for (var index = 0; index < arguments.length; index ++) {
        ReleasePool.prototype.retain.apply(this, arguments[index]);
    }
};

ReleasePool.prototype.release = function() {
    for (var index = 0; index < arguments.length; index ++) {
        if (this.hasOwnProperty(arguments[index]) && this[arguments[index]] > 0) {
            this[arguments[index]] --;
        }
        if (this.hasOwnProperty(arguments[index]) && 0 == this[arguments[index]]) {
            delete this[arguments[index]];
        }
    }
};

ReleasePool.prototype.releaseAll = function() {
    for (var index = 0; index < arguments.length; index ++) {
        ReleasePool.prototype.release.apply(this, arguments[index]);
    }
};

ReleasePool.prototype.contains = function(m) {
    return this.hasOwnProperty(m) && this[m] > 0;
};

ReleasePool.prototype.asArray = function() {
    var arr = [];
    for (var key in this) {
        if (this.hasOwnProperty(key)) {
            arr.push(key);
        }
    }
    return arr;
};


});
})(jQuery, document, window, window, require);