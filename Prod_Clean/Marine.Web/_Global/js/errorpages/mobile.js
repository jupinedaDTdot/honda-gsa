(function() {
    var cls = document.body.className,
        a = document.createElement('a');

    a.className = 'mobile-site-link';

    if (cls.match(/\bfrench\b/)) {
        a.innerHTML = 'Voir Honda.ca site mobile';
        a.href = 'http://m.honda.ca/automobiles';
    }
    else {
        a.innerHTML = 'View Honda.ca mobile site';
        a.href = 'http://m.honda.ca/';
    }

    document.body.insertBefore(a, document.body.firstChild);
})();

