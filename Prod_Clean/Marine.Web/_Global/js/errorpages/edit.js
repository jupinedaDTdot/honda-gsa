// enable edit button
;(function($) {
		__load__('edit_mode', function(ctx) {
			if ($('a.edit-button:not(.edit-button-processed)', ctx).size() > 0)
				require(['jquery.dropdown'], function() {
					$('a.edit-button:not(.edit-button-processed)', ctx).each(function() {
							var self = $(this);
							self.dropdown({
									sibling: false,
									body: true,
									width: 150,
									top: 23,
									btnZIndex: 20000,
									containerZIndex: 30000,
									relation: 'absolute',
									position: 'bottom',
									additionalClass: 'edit-menu'
							}).addClass('edit-button-processed');
							if (!window.__edit) {
								return;
							}
							self.parent().addClass('has-edit-button');
					});
				});
    });

})(window.jQuery);
