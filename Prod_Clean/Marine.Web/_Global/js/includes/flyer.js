﻿window.WebFlyer = function (flyerUrl, backTitle, backUrl, target, flyerElement) {
  this._flyerUrl = flyerUrl;
  this._backUrl = backUrl;
  this._backTitle = backTitle;
  this._target = target || '_self';
  this._flyerElement = flyerElement;
};
window.WebFlyer.prototype.open = function () {
  // create a form and post it locally
  var form = $('<form>').attr({
    action: this._flyerUrl,
    method: 'post',
    target: this._target
  });
  // append elements in the form
  $('<input>').attr({ type: 'hidden', name: 'backUrl', value: this._backUrl }).appendTo(form);
  $('<input>').attr({ type: 'hidden', name: 'backTitle', value: this._backTitle }).appendTo(form);
  $('<input>').attr({ type: 'hidden', name: 'element', value: this._flyerElement }).appendTo(form);
  // append the form and submit
  form.appendTo('body').submit();
};

console.log(window.WebFlyer);
