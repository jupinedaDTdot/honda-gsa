;(function($, F, undefined) {

$.behaviors.edit_mode = function(ctx, ajax) {

    var localQueue = new $.jobqueue(10);

    if ($('a.edit-button:not(.edit-button-processed)', ctx).size() > 0) {
        require(['commonlib/jquery.commonlib.dropdown'], function() {
            $('a.edit-button:not(.edit-button-processed)', ctx).each(function() {
                var self = $(this);
                localQueue.add(function() {
                    self.dropdown({
                        contentTarget: self.attr('rel'),
                        multiple: true,
                        position: {
                            my: 'right top',
                            at: 'right bottom'
                        },
                        additionalClass: 'edit-menu'
                    }).addClass('edit-button-processed');
                    if (!F.edit_mode) {
                        return;
                    }
                    self.parent().addClass('has-edit-button');
                }, ajax, 20);
            });
        });
    }
};

})(jQuery, window);