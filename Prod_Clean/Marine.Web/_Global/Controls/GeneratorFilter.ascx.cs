﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;
using Navantis.Honda.CMS.Demo;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using HondaCA.Service.Model;

namespace Marine.Web._Global.Controls
{
    public partial class GeneratorFilter : System.Web.UI.UserControl 
    {
        CmsContentBasePage cmsbasePage;
        private ResourceManager _ResourceManager;
        public string value1 = "maximum_continuous_ac_output__watts_";
        public string value2 = "vac_voltage_available";
        public string value3 = "approximate_running_time___tankful__hrs__";
        public string value4 = "generator_type";
        public string value5 = "ground_fault_circuit_interrupter__gfci_";

        public decimal minWatts { get; set; }
        public decimal maxWatts { get; set; }

        public decimal minRunTime { get; set; }
        public decimal maxRunTime { get; set; }
                
        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                this.ResourceManager = cmsbasePage.ResourceManager;
            }
            TrimSpecService trimSpectService = new TrimSpecService();
            EntityCollection<TrimSpect> trimSpect = trimSpectService.GetTrimSpectRange(cmsbasePage.TargetID, cmsbasePage.PageLanguage, value1);
            minWatts = trimSpect[0].minValue;
            maxWatts = trimSpect[0].maxValue;

            trimSpect = trimSpectService.GetTrimSpectRange(cmsbasePage.TargetID, cmsbasePage.PageLanguage, value3);
            minRunTime = trimSpect[0].minValue;
            maxRunTime = trimSpect[0].maxValue;
        }
    }
}