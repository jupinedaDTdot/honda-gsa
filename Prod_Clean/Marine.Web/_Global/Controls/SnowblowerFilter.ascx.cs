﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;
using Navantis.Honda.CMS.Demo;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using HondaCA.Service.Model;

namespace Marine.Web._Global.Controls
{
    public partial class SnowblowerFilter : System.Web.UI.UserControl
    {
        CmsContentBasePage cmsbasePage;
        private ResourceManager _ResourceManager;
        public string value1 = "discharge_type";
        public string value2 = "clearing_width";
        public string value3 = "displacement";
        public string value4 = "axle_type";
        public string value5 = "drive_type";
        public decimal mincc { get; set; }
        public decimal maxcc { get; set; }

        public decimal minClearWidth { get; set; }
        public decimal maxClearWidth { get; set; }

        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                this.ResourceManager = cmsbasePage.ResourceManager;
            }
            TrimSpecService trimSpectService = new TrimSpecService();
            EntityCollection<TrimSpect> trimSpect = trimSpectService.GetTrimSpectRange(cmsbasePage.TargetID, cmsbasePage.PageLanguage, value2);
            minClearWidth = trimSpect[0].minValue;
            maxClearWidth = trimSpect[0].maxValue;

            trimSpect = trimSpectService.GetTrimSpectRange(cmsbasePage.TargetID, cmsbasePage.PageLanguage, value3);
            mincc = trimSpect[0].minValue;
            maxcc = trimSpect[0].maxValue;
        }
    }
}