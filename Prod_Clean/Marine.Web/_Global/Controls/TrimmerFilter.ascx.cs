﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;
using Navantis.Honda.CMS.Demo;

namespace Marine.Web._Global.Controls
{
    public partial class Trimmer : System.Web.UI.UserControl
    {
        CmsContentBasePage cmsbasePage;
        private ResourceManager _ResourceManager;
        public string value1 = "handle_type";
        public string value2 = "blade";
        public string value3 = "line_type";
        public string value4 = "engine_type";
        

        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                this.ResourceManager = cmsbasePage.ResourceManager;
            }
        }
    }
}