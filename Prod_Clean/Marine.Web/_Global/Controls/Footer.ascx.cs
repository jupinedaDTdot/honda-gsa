﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;
using HondaCA.Common;
using HondaCA.Service.Model;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using Navantis.Honda.CMS.Demo;


namespace Marine.Web._Global.Controls
{
    public partial class Footer : System.Web.UI.UserControl
    {
        protected CmsContentBasePage basePage;
        private ResourceManager _ResourceManager;
        private Language _PageLanguage;
        private string domain = "http://" + HondaCA.Common.Global.CurrentDomain.ToLower();

        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }

        public Language PageLanguage
        {
            get { return _PageLanguage; }
            set { _PageLanguage = value; }
        }

        public bool isAbsolute { get; set; }

        public HondaModel CurrentModel { get; set; }
        public string white_text_class = "";
        
//        protected void Page_Load(object sender, EventArgs e)
//        {
//            basePage = (CmsContentBasePage)this.Parent.Page;
//            this.PageLanguage = basePage.PageLanguage;
//            this.ResourceManager = basePage.ResourceManager;

//            if (CurrentModel == null)
//            {
//                HondaModel ms = new HondaModel();
//                CurrentModel = ms;
//            }
//            else
//            {
//                white_text_class = "white_text";
//            }
                        
//            //Navantis.Honda.CMS.Demo.CmsContentBasePage.InitializeCulture();

//            // SetDefaulPage();
//            EntityCollection<ModelCategory> hmList = new EntityCollection<ModelCategory>();
//            //there are 2 cols for vehicle list
//            EntityCollection<ModelCategory> hmList1 = new EntityCollection<ModelCategory>();
//            EntityCollection<ModelCategory> hmList2 = new EntityCollection<ModelCategory>();

//            HondaModelService hm = new HondaModelService();
//            //hmList = hm.GetModels(2, PageLanguage);
//            hmList = hm.GetModelCategory(PageLanguage, basePage.TargetID);

//            int hmCount = 0;
//            foreach (ModelCategory hmListIntermediate in hmList)
//            {
//                if (hmCount < 8)
//                {
//                    hmList1.Add(hmListIntermediate);
//                }
//                else
//                {
//                    hmList2.Add(hmListIntermediate);
//                }
//                hmCount++;
//            }

//            repModels.DataSource = hmList1;
//            repModels.DataBind();

//            if (hmList2 != null && hmList2.Count > 0)
//            {
//                repModels2.DataSource = hmList2;
//                repModels2.DataBind();
//                liModelList2.Visible = true;
//            }
            
            
//            //string res = "";
//            //foreach (BaseModelFamily obj1 in hmList)
//            //{
//            //    res += obj1.ModelName + "  " + obj1.ModelYear.ToString() + "  " + obj1.MSRP.ToString();
//            //}


//            if (isAbsolute != true) { domain = ""; }

            
//            litCompareLink.Text = string.Format("<a href='{1}'>{2}</a>", domain, ResourceManager.GetString("CompareRootFolder"), ResourceManager.GetString("CompareVehicles"));
//            litSpecialOffersLink.Text = string.Format("<a href='{1}'>{2}</a>", domain, ResourceManager.GetString("SpecialOffersURL"), ResourceManager.GetString("SpecialOffers"));
//            litDealerLocatorLink.Text = string.Format("<a href='{1}'>{2}</a>", domain, ResourceManager.GetString("DealerLocatorRootFolder"), ResourceManager.GetString("FindADealer"));
//            litLeaseAndFinanceLink.Text = string.Format("<a href='{1}'>{2}</a>", domain, ResourceManager.GetString("FinanceUrl"), ResourceManager.GetString("LeaseAndFinance"));
//            litPartsService.Text = string.Format("<a href='{1}'>{2}</a>", domain, ResourceManager.GetString("PartsServicesUrl"), ResourceManager.GetString("PartsServices"));
//            litAccessories.Text = string.Format("<a href='{1}'>{2}</a>", domain, ResourceManager.GetString("AccessoriesUrl"), ResourceManager.GetString("txtAccessories"));
//            litCommercialProd.Text = string.Format("<a href='{1}'>{2}</a>", domain, ResourceManager.GetString("txtCommercialProductsURL"), ResourceManager.GetString("txtCommercialProducts"));
            

//            //litHelpMeChooseLink.Text = string.Format("<a href='{1}'>{2}</a>", domain, ResourceManager.GetString("HelpMeChooseUrl"), ResourceManager.GetString("HelpMeChoose"));
//            //litWarranty.Text = string.Format("<a href='{0}{1}'>{2}</a>", domain, ResourceManager.GetString("WarrantyUrl"), ResourceManager.GetString("Warranty"));
///*
//            litExpressServiceLink.Text = string.Format("<a href='{0}{1}'>{2}</a>", domain, ResourceManager.GetString("ExpressServiceUrl"), ResourceManager.GetString("ExpressService"));
//            litFAQLink.Text = string.Format("<a href='{0}{1}'>{2}</a>", domain, ResourceManager.GetString("FaqsUrl"), ResourceManager.GetString("FAQs"));
//            litHondaServiceLink.Text = string.Format("<a href='{0}{1}'>{2}</a>", domain, ResourceManager.GetString("HondaServiceUrl"), ResourceManager.GetString("GenuinePartsAndService"));
//            litOwndersLink.Text = string.Format("<a href='{0}{1}'>{2}</a>", domain, ResourceManager.GetString("OwnersWebsiteUrl"), ResourceManager.GetString("OwnersWebsite"));
//            litApparelLink.Text = string.Format("<a href='{0}{1}'>{2}</a>", domain, ResourceManager.GetString("ApparelUrl"), ResourceManager.GetString("Apparel"));
//            litVehicleRecallLink.Text = string.Format("<a href='{0}{1}'>{2}</a>", domain, ResourceManager.GetString("VehicleRecallUrl"), ResourceManager.GetString("VehicleRecall"));
//            litSecurity.Text = string.Format("<a href='{0}{1}'>{2}</a>", domain, ResourceManager.GetString("SecurityUrl"), ResourceManager.GetString("Security"));
//            litWarranty.Text = string.Format("<a href='{0}{1}'>{2}</a>", domain, ResourceManager.GetString("WarrantyUrl"), ResourceManager.GetString("Warranty"));

//            */
//            litInnovationLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("InnovationUrl"), ResourceManager.GetString("Innovation"));
//            litEnvironmentLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("EnvironmentUrl"), ResourceManager.GetString("Environment"));
//            litHondaInCanadaLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("HondaInCanadaUrl"), ResourceManager.GetString("HondaInCanada"));
//            litFoundationLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("FoundationUrl"), ResourceManager.GetString("Foundation"));
//            litSponsorshipLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("SponsorshipUrl"), ResourceManager.GetString("Sponsorship"));
//            litNewsLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("NewsUrl"), ResourceManager.GetString("News"));
//            litHondaWorldWideLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("HondaWorldwideUrl"), ResourceManager.GetString("HondaWorldwide"));
//            litContactLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("ContactUsDefUrl"), ResourceManager.GetString("ContactUs"));
//            litCareersLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("CareersUrl"), ResourceManager.GetString("Careers"));

            
//            litTermsLink.Text = string.Format("<a href='{0}' target='_blank'>{1}</a>", ResourceManager.GetString("TermsAndConditionsURL"), ResourceManager.GetString("TermsAndConditions"));
//            litPrivacyBottomLink.Text = string.Format("<a href='{0}' target='_blank'>{1}</a>", ResourceManager.GetString("PrivacyPolicyURL"), ResourceManager.GetString("PrivacyPolicy"));
//            litSiteMapLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("SiteMapURL"), ResourceManager.GetString("SiteMap"));
//            litSiteMapLink.Text = string.Format("<a href='{0}'>{1}</a>", ResourceManager.GetString("SiteMapURL"), ResourceManager.GetString("SiteMap"));
             
//        }

        protected void repModels_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            ModelCategory objCategory = (ModelCategory)e.Item.DataItem;
            string Url = "";
            if (objCategory != null)
            {

                Literal LitModelLink = ((Literal)e.Item.FindControl("LitModelLink"));
                //Url = HondaCA.Common.Global.ModelString + "/" + objModel.CategoryUrl;///Models
                Url = ResourceManager.GetString("urlSeeAllOutBoardMotor") + "#" + objCategory.ExportKey;
                if (isAbsolute != true) { domain = ""; }
                LitModelLink.Text += string.Format("<a href='{0}{1}'>{2}</a>", domain, Url, Marine.Web.Code.CommonFunctions.ucWords(objCategory.CategoryName));
                //if (e.Item.ItemIndex == 0) li.Attributes.Add("class", "fl first");
            }

        }

        protected void repModels2_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            HondaModel objModel = (HondaModel)e.Item.DataItem;
            string Url = "";
            if (objModel != null)
            {
                Literal LitModelLink2 = ((Literal)e.Item.FindControl("LitModelLink2"));
                Url = HondaCA.Common.Global.ModelString + "/" + objModel.BMUrlName;///Models
                                                                                   ///
                if (isAbsolute != true) { domain = ""; }
                LitModelLink2.Text += string.Format("<a href='{0}{1}'>{2}</a>", domain, Url, objModel.ModelName);
            }

        }
    }
}