﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;
using Navantis.Honda.CMS.Demo;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using HondaCA.Service.Model;

namespace Marine.Web._Global.Controls
{
    public partial class WaterpumpFilter : System.Web.UI.UserControl
    {
        CmsContentBasePage cmsbasePage;
        private ResourceManager _ResourceManager;
        public string value1 = "water_pump_type";
        public string value2 = "litres_per_minute";
        public string value3 = "total_head";
        public decimal minCapacity { get; set; }
        public decimal maxCapacity { get; set; }

        public decimal minTotalHead { get; set; }
        public decimal maxTotalHead { get; set; }
        

        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                this.ResourceManager = cmsbasePage.ResourceManager;
            }
            TrimSpecService trimSpectService = new TrimSpecService();
            EntityCollection<TrimSpect> trimSpect = trimSpectService.GetTrimSpectRange(cmsbasePage.TargetID, cmsbasePage.PageLanguage, value2);
            minCapacity = trimSpect[0].minValue;
            maxCapacity = trimSpect[0].maxValue;

            trimSpect = trimSpectService.GetTrimSpectRange(cmsbasePage.TargetID, cmsbasePage.PageLanguage, value3);
            minTotalHead = trimSpect[0].minValue;
            maxTotalHead = trimSpect[0].maxValue;

        }
    }
}