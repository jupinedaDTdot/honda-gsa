﻿#region

using System;
using System.Resources;
using System.Text;
using System.Web.UI;
using HondaCA.Common;
using HondaCA.Entity.Common;
using HondaCA.Entity.Model;
using HondaCA.Service.Common;
using HondaDealer.Entity;
using HondaDealer.Service;
using Navantis.Honda.CMS.Demo;
using Region = HondaDealer.Entity.Region;
using RegionService = HondaDealer.Service.RegionService;

#endregion

namespace Marine.Web._Global.Controls
{
    public partial class FindADealer : UserControl
    {
        private readonly string domain = "http://" + HondaCA.Common.Global.CurrentDomain.ToLower();

        private CmsContentBasePage cmsbasePage;

        public string locatorXmlfile;

        public HondaModel objModel;
        public ResourceManager ResourceManager { get; set; }

        public Language PageLanguage { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage) Parent.Page;
                PageLanguage = cmsbasePage.PageLanguage;
                ResourceManager = cmsbasePage.ResourceManager;
            }
            if (PageLanguage == Language.French)
            {
                locatorXmlfile = "locator_map_fr.xml";
            }
            else if (PageLanguage == Language.English)
            {
                locatorXmlfile = "locator_map.xml";
            }

            SetDefaulPage();
            CheckBoxPhd.Text = ResourceManager.GetString("txtCheckBoxrPHD");

            if (IsPostBack)
            {
                //check for region selector postback
                string selectedRegion = null;
                try
                {
                    selectedRegion = Request.Form["RegionSelectBoxSide"];
                }
                catch (Exception)
                {
                    selectedRegion = null;
                }
                if (!string.IsNullOrEmpty(selectedRegion))
                {
                    Response.Redirect(selectedRegion);
                }
            }
        }

        protected void SetDefaulPage()
        {
            LitFindADealer.Text = ResourceManager.GetString("txtFindADealer");
            LitSearchBy.Text = ResourceManager.GetString("txtSearchBy");
            LitZipCode.Text = ResourceManager.GetString("txtPostalCode");
            LitCity.Text = ResourceManager.GetString("txtCity");
            LitRegion.Text = ResourceManager.GetString("txtRegion");
            lnkBPostalCode.Text = ResourceManager.GetString("txtButtonGo");

            lnkBCitySearch1.Text = ResourceManager.GetString("txtButtonGo");

            String bookaTestDriveUrl;

            if (PageLanguage == Language.French)
            {
                bookaTestDriveUrl = domain + "/modelpages/BookTestDrive.aspx?L=fr";
            }
            else
            {
                bookaTestDriveUrl = domain + "/modelpages/BookTestDrive.aspx?L=en";
            }
            //var rs = new RegionService();
            //var regionList = new EntityCollection<Region>();
            //try
            //{
            //    regionList = rs.GetAllRegions(PageLanguage.GetCultureStringValue());
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("hondaPE:finadadealer:SetDefaulPage" + ex.Message, ex);
            //}

            string url;
            //var sb = new StringBuilder();
            //LiteralRegionSelectOptions.Text = string.Format(@"<option value=""{0}"">{1}</option>", "",
            //    ResourceManager.GetString("txtSelect"));
            //foreach (var region in regionList)
            //{
            //    LiteralRegionSelectOptions.Text += string.Format(@"<option value=""{0}"">{1}</option>",
            //        string.Format("{0}/{1}", ResourceManager.GetString("urlFindADealer"), region.ProvinceUrlName),
            //        region.Name);
            //    url = ResourceManager.GetString("DealerLocatorRootFolder") + "/" + region.ProvinceUrlName;
            //    sb.Append(string.Format("<a href='{0}' title='{1}'>{2}</a>", url,
            //        string.Format(ResourceManager.GetString("RegionToolTip"), region.Name), region.Name));
            //}
            //LiteralHidddenDealerList.Text = sb.ToString();
            var sos = new SpecialOffersRedirectService();
            var soList = new HondaCA.Entity.EntityCollection<SpecialOffersRedirect>();
            soList = sos.GetAllSpecialOffersInfo(PageLanguage.GetCultureStringValue());

            var sbSpecial = new StringBuilder();
            foreach (var specialoffersredirect in soList)
            {
                url = specialoffersredirect.URL;
                LitRegionList_SO.Text += string.Format(@"<option value=""{0}"">{1}</option>", url,
                    specialoffersredirect.Title);
                sbSpecial.Append(string.Format("<a href='{0}' title='{1}'>{2}</a>>", url,
                    string.Format(ResourceManager.GetString("RegionToolTip"), specialoffersredirect.Title),
                    specialoffersredirect.Title));
            }
            LiteralHidddenDealerList.Text += sbSpecial.ToString();
        }

        protected void lnkBCitySearch_OnClick(object sender, EventArgs e)
        {
            var url = RedirectCity(txtCity1.Text.Trim(), ResourceManager.GetString("urlFindADealer"), PageLanguage);
            if (url != string.Empty)
            {
                if (!(url.Contains("NoProvince")))
                    Response.Redirect(url);
                else
                    Response.Redirect(DealerLocatorCommon.getSearchErrorUrl(RepDealerSearch(txtCity1.Text.Trim()),
                        ResourceManager.GetString("urlFindADealer")));
            }
        }

        protected void lnkBPostalCode_OnClick(object sender, EventArgs e)
        {
            var url = DealerLocatorCommon.redirectPostalCode(txtPostalCode.Text.Trim(),
                ResourceManager.GetString("urlFindADealer"), false);
            if (url != string.Empty) Response.Redirect(url);
        }

        public static string RedirectCity(string sText, string RootPath, Language PageLanguage)
        {
            var url = "";
            var searchCity = sText;
            if (searchCity != string.Empty)
            {
                if (searchCity.Contains(","))
                {
                    var tmpcity = RepDealerSearch(searchCity.Substring(0, searchCity.IndexOf(",")));
                    var tmpProvince =
                        RepDealerSearch(searchCity.Substring(searchCity.IndexOf(",") + 1,
                            searchCity.Length - searchCity.IndexOf(",") - 1));
                    url = RootPath + "/" + tmpProvince.ToLower() + "/" + tmpcity.ToLower();
                }
                else
                {
                    url = RootPath + "/city/" + RepDealerSearch(searchCity).ToLower();
                }
            }
            return (url);
        }

        public static string RepDealerSearch(string str)
        {
            str = str.Trim().Replace(" ", "_").Replace("&", "AND").Replace("(", "").Replace(")", "").Replace(",", "");
            return str;
        }
    }
}