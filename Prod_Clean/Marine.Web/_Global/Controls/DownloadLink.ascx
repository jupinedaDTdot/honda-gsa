﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DownloadLink.ascx.cs" Inherits="Marine.Web._Global.Controls.DownloadLink" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>

<div>
    <asp:Panel runat="server" ID="pnlSingle">
    <div class="edit-container-should-hide"><uc:ElementControl runat="server" ID="elementControl1" /> <%=(isStaging ? "Model Specific DLs" : string.Empty)%></div>
    <div class="edit-container-should-hide"><uc:ElementControl runat="server" ID="elementControl2" ElementName="GenericContent_Link" /> <%=(isStaging ? "Category Specific DLs"  : string.Empty)%></div>
    <div class="edit-container-should-hide"><uc:ElementControl runat="server" ID="elementControl3" /> <%=(isStaging ? "Shared for all models DLs":string.Empty)%></div>
    </asp:Panel>
    <a class="btn_large first" property="act:menu" data-menu-type="brochure" data-menu-class="brochure-flyout" data-menu-content-target="#download-brochure">
        <span><img src="/_Global/img/layout/icon_downloads.png" class="pf" alt="" /></span>
	    <span class="downloads"><asp:Literal ID="LiteralDownloads" runat="server"></asp:Literal></span>
    </a>
</div>
<div id="download-brochure" class="dn">
	<div>
        <ul>
            <asp:PlaceHolder runat="server" ID="plDownloadLinks"></asp:PlaceHolder>	 
        </ul>
        <a href="http://get.adobe.com/reader/" target="_blank" class="get_adobe_reader"></a>
        <div class="clr"></div>
	</div>
</div>