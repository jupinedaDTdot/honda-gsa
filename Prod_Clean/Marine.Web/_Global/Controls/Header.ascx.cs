﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;
using System.IO;
using System.Text;
using HondaCA.Common;
using HondaCA.Service.Model;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using Navantis.Honda.CMS.Client;

namespace Marine.Web._Global.Controls
{
    public partial class Header : System.Web.UI.UserControl
    {
        CmsContentBasePage basePage;
        private ResourceManager _ResourceManager;
        private Language _PageLanguage;
        protected string domain = "http://" + HondaCA.Common.Global.CurrentDomain.ToLower();
        protected ContentPage globalPage;
        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }

        public Language PageLanguage
        {
            get { return _PageLanguage; }
            set { _PageLanguage = value; }
        }

        public bool isAbsolute { get; set; }
       
        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    basePage = (CmsContentBasePage)this.Parent.Page;
        //    this.PageLanguage = basePage.PageLanguage;
        //    this.ResourceManager = basePage.ResourceManager;
        //   // SetDefaulPage();

        //    EntityCollection<ModelCategory> hmList = new EntityCollection<ModelCategory>();
        //    HondaModelService hm = new HondaModelService();

        //    //hmList = hm.GetModelFamily(PageLanguage);
        //    hmList = hm.GetModelCategory(PageLanguage, basePage.TargetID);

        //    StringBuilder sb = new StringBuilder();
            

        //    int intStart=0;
        //    string strStart=string.Empty, strSeperator=string.Empty;
        //    foreach (ModelCategory mCategory in hmList)
        //    {
        //        string multilineClass = string.Empty;
        //        string MenuTitle = string.Empty;

        //        strStart= intStart==0 ? "<li class='fl first'>" : "<li class='fl'>";
        //        sb.Append(strStart);
        //        switch(mCategory.CategoryUrl)
        //        {
        //            case "trimmers":
        //                strSeperator = "class='full_seperator'";
        //                MenuTitle = mCategory.CategoryName;
        //                break;
        //            case "taille-borduresbroussailles":
        //                strSeperator = "class='full_seperator'";
        //                multilineClass = "multiline";
        //                MenuTitle = mCategory.CategoryName;
        //                break;
        //            case "souffleuses-a-neige":
        //                multilineClass = "multiline";
        //                MenuTitle = mCategory.CategoryName;
        //                break;
        //            case "waterpumps":
        //            case "motopompes-a-eau":
        //                multilineClass = "full_seperator";
        //                MenuTitle = mCategory.CategoryName;
        //                break;

        //            /*
        //             * case "generators":
        //                strSeperator = "data-dropdown-content-target='#cars_list'";
        //                break;
        //            case "lawnmowers":
        //                strSeperator = "data-dropdown-content-target='#hybrids_list'";
        //                break;
        //             * */
        //            default:
        //                strSeperator = string.Empty;
        //                MenuTitle = mCategory.CategoryName;
        //                break;
        //        }

        //        sb.AppendFormat(@"<a href='/{0}' data-url=""{3}"" data-dropdown-content-target=""{4}"" class=""{5}""  {1}>{2}</a></li>", mCategory.CategoryUrl, strSeperator, MenuTitle, "/dropdown/" + mCategory.CategoryUrl, "#" + mCategory.CategoryID.ToString() + "_list", multilineClass);
        //        intStart++;
        //    }
        //    //sb.Append(string.Format(@"<li class='fl alternate'><a href='{0}'>{1}</a></li>", ResourceManager.GetString("txtExperienceUrl"), ResourceManager.GetString("txtExperience")));
        //    sb.Append(string.Format(@"<li class='fl alternate'><a href='{0}' class='{2}'>{1}</a></li>", ResourceManager.GetString("txtWhyHondaURL"), ResourceManager.GetString("menuWhyHonda"), ""));
        //    sb.Append(string.Format(@"<li class='fl alternate'><a href='{0}' class='{2}'>{1}</a></li>", ResourceManager.GetString("AccessoriesUrl"), ResourceManager.GetString("txtAccessories"),""));
        //    sb.Append(string.Format(@"<li class='fl alternate'><a href='{0}' class='{2}'>{1}</a></li>", ResourceManager.GetString("PartsServicesUrl"), ResourceManager.GetString("PartsServices"),""));
        //    sb.Append(string.Format(@"<li class='fl alternate'><a href='{0}' class='last_seperator{2}'>{1}</a></li>", ResourceManager.GetString("txtSpecialOffersURL"), ResourceManager.GetString("menuSpecialOffers"),  ""));
        //    sb.Append(string.Format(@"<li class='last'><a id='shopping_dropdown' data-dropdown-content-target='#shopping_list'  href='{0}'>{1}</a></li>", "#", ResourceManager.GetString("menuShoppingTools")));          
           				
        //    ltPrimaryMenu.Text = sb.ToString();


        //    SetLanguageURL();

        //    //if (basePage.MainUrl == "/news-events/news-detail_fr")
        //    //{
        //    //    LanguageUrl.HRef = "javascript:changeCMSLanguage('/news-events/news-detail')";
        //    //}
        //    //if (basePage.MainUrl == "/news-events/news-detail")
        //    //{
        //    //    LanguageUrl.HRef = "javascript:changeCMSLanguage('/news-events/news-detail_fr')";
        //    //}


        //    //if (string.IsNullOrEmpty(LanguageUrl.HRef)) LanguageUrl.Visible = false;
        //    //if (string.IsNullOrEmpty(SecondLanguageURL.HRef)) SecondLanguageURL.Visible = false;

        //    if (isAbsolute != true) { domain = ""; }

        //    hplLogo.NavigateUrl = PageLanguage == Language.French ? domain + "/francais" : domain + "/";
        //    //hplAllModelsDropDown.NavigateUrl = domain + ResourceManager.GetString("AllModelsURL");

        //    litDealerLocatorLink.Text = string.Format(@"<li class=""fl first{3}""><a href='{1}'><span>{2}</span></a></li>", domain, ResourceManager.GetString("DealerLocatorRootFolder"), ResourceManager.GetString("FindADealer"), PageLanguage == Language.French ? " multiline" : "");
        //    // litOwnersLink.Text = string.Format("<a href='{0}{1}'><span>{2}</span></a>", domain, ResourceManager.GetString("OwnersUrl"), ResourceManager.GetString("OwnersMenu"));
        //    litLeaseAndFinanceLink.Text = string.Format(@"<li class=""fl""><a href='{1}'><span>{2}</span></a></li>", domain, ResourceManager.GetString("LeaseAndFinanceUrl"), ResourceManager.GetString("txtFinance").ToUpper());

        //    //litAccessoriesLink.Text = string.Format(@"<li class=""fl""><a href='{1}'><span>{2}</span></a></li>", domain, ResourceManager.GetString("AccessoriesUrl"), ResourceManager.GetString("txtAccessories"));
        //    //litPartsServicesLink.Text = string.Format(@"<li class=""fl{3}""><a href='{1}'><span>{2}</span></a></li>", domain, ResourceManager.GetString("PartsServicesUrl"), ResourceManager.GetString("PartsServices"), PageLanguage == Language.French ? " multiline" : "");
        //    litSafetyLink.Text = string.Format(@"<li class=""fl""><a href='{1}'><span>{2}</span></a></li>", domain, ResourceManager.GetString("SafetyUrl"), ResourceManager.GetString("Safety"));

        //    litNewsEventsLink.Text = string.Format(@"<li class=""fl{2}""><a href='{0}' class='btn_square'><span>{1}</span></a></li>", ResourceManager.GetString("NewsEventsURL"), ResourceManager.GetString("NewsEvents"), PageLanguage == Language.French ? " multiline" : "");
        //  //litCarCompareLink.Text = string.Format("<a href='{0}{1}' class='btn_square'><span>{2}</span></a>", domain, ResourceManager.GetString("MatchUrl"), ResourceManager.GetString("MatchAHondaToYourNeeds"));
        //  //litCarViewAlModelsLink.Text = string.Format("<a href='{0}{1}' class='btn_square'><span>{2}</span></a>", domain, ResourceManager.GetString("ViewAllModelsURL"), ResourceManager.GetString("ViewAllModels"));

        //  //litTrucksCertifiedUsedLink.Text = litCarCertifiedUsedLink.Text;
        //  //litTrucksCompareLink.Text = litCarCompareLink.Text;
        //  //litTrucksViewAllModels.Text = litCarViewAlModelsLink.Text;

        //}



        //private void SetLanguageURL()
        //{
        //  if (basePage.IsNotCMSPage)
        //  {
        //    string lang = string.Empty;
        //    lang = Request.QueryString["L"] ?? Request.QueryString["L"];
        //    if (string.IsNullOrEmpty(lang))
        //    {
        //        lang = Request.QueryString["Lang"] != null ? Request.QueryString["Lang"] : "en";
        //    }

        //    lang = lang.Length > 2 ? lang.Substring(0, 2) : lang;
        //    if (lang.ToUpper() == Language.French.GetCultureStringValue().Substring(0, 2).ToUpper())
        //      basePage.PageLanguage = Language.French;
        //    else
        //      basePage.PageLanguage = Language.English;
        //  }

        //  string toggoleURL = basePage.PageSetting != null
        //                          ? basePage.PageSetting.LanguageToggleURL
        //                          : !string.IsNullOrWhiteSpace(basePage.ToggleURL) ? basePage.ToggleURL : "";
        //  string mainURL = HttpContext.Current.Request.RawUrl;

        //  if (!basePage.IsQuebecPricing)
        //  {
        //    if (PageLanguage == Language.English)
        //    {
        //      SecondLanguageURL.HRef = toggoleURL;
        //      SecondLanguageName.Text = "Français(National)";
        //      SecondLanguageURL.Attributes.Add("class", "quebec_false");

        //      LanguageUrl.HRef = toggoleURL;
        //      LanguageName.Text = "Français(QC)";
        //      LanguageUrl.Attributes.Add("class", "quebec_true");
        //    }
        //    else
        //    {
        //      SecondLanguageURL.HRef = toggoleURL;
        //      SecondLanguageName.Text = "English";
        //      SecondLanguageURL.Attributes.Add("class", "quebec_false");

        //      LanguageUrl.HRef = mainURL;
        //      LanguageName.Text = "Français(QC)";
        //      LanguageUrl.Attributes.Add("class", "quebec_true");
        //    }
        //  }
        //  else
        //  {
        //    if (PageLanguage == Language.English)
        //    {
        //      SecondLanguageURL.HRef = mainURL;
        //      SecondLanguageName.Text = "English";
        //      SecondLanguageURL.Attributes.Add("class", "quebec_false");

        //      LanguageUrl.HRef = toggoleURL;
        //      LanguageName.Text = "Français(QC)";
        //      LanguageUrl.Attributes.Add("class", "quebec_true");
        //    }
        //    else
        //    {
        //      SecondLanguageURL.HRef = toggoleURL;
        //      SecondLanguageName.Text = "English(QC)";
        //      SecondLanguageURL.Attributes.Add("class", "quebec_true");

        //      LanguageUrl.HRef = mainURL;
        //      LanguageName.Text = "Français(National)";
        //      LanguageUrl.Attributes.Add("class", "quebec_false");
        //    }
        //  }
        //}

            
            
            /*if (isAbsolute != true) { domain = ""; }


          /*if (isAbsolute != true) { domain = ""; }

          hplLogo.NavigateUrl = PageLanguage == Language.French ? domain + "/francais" : domain + "/";
          //hplAllModelsDropDown.NavigateUrl = domain + ResourceManager.GetString("AllModelsURL");

          litDealerLocatorLink.Text = string.Format(@"<li class=""fl first{3}""><a href='{1}'><span>{2}</span></a></li>", domain, ResourceManager.GetString("DealerLocatorRootFolder"), ResourceManager.GetString("FindADealer"), "");
         // litOwnersLink.Text = string.Format("<a href='{0}{1}'><span>{2}</span></a>", domain, ResourceManager.GetString("OwnersUrl"), ResourceManager.GetString("OwnersMenu"));
          litLeaseAndFinanceLink.Text = string.Format(@"<li class=""fl""><a href='{1}'><span>{2}</span></a></li>", domain, ResourceManager.GetString("LeaseAndFinanceUrl"), ResourceManager.GetString("txtFinance").ToUpper());

          //litAccessoriesLink.Text = string.Format(@"<li class=""fl""><a href='{1}'><span>{2}</span></a></li>", domain, ResourceManager.GetString("AccessoriesUrl"), ResourceManager.GetString("txtAccessories"));
          //litPartsServicesLink.Text = string.Format(@"<li class=""fl{3}""><a href='{1}'><span>{2}</span></a></li>", domain, ResourceManager.GetString("PartsServicesUrl"), ResourceManager.GetString("PartsServices"), PageLanguage == Language.French ? " multiline" : "");
          litSafetyLink.Text = string.Format(@"<li class=""fl""><a href='{1}'><span>{2}</span></a></li>", domain, ResourceManager.GetString("SafetyUrl"), ResourceManager.GetString("Safety"));

          litNewsEventsLink.Text = string.Format(@"<li class=""fl{2}""><a href='{0}' class='btn_square'><span>{1}</span></a></li>", ResourceManager.GetString("NewsEventsURL"), ResourceManager.GetString("NewsEvents"),  "");
          //litCarCompareLink.Text = string.Format("<a href='{0}{1}' class='btn_square'><span>{2}</span></a>", domain, ResourceManager.GetString("MatchUrl"), ResourceManager.GetString("MatchAHondaToYourNeeds"));
          //litCarViewAlModelsLink.Text = string.Format("<a href='{0}{1}' class='btn_square'><span>{2}</span></a>", domain, ResourceManager.GetString("ViewAllModelsURL"), ResourceManager.GetString("ViewAllModels"));

          //litTrucksCertifiedUsedLink.Text = litCarCertifiedUsedLink.Text;
          //litTrucksCompareLink.Text = litCarCompareLink.Text;
          //litTrucksViewAllModels.Text = litCarViewAlModelsLink.Text;

          /*
           * TODO : Remove comment if required
          litHybridCertifiedUsedLink.Text = litCarCertifiedUsedLink.Text;
          litHybridCompareLink.Text = litCarCompareLink.Text;
          litHybridViewAllModels.Text = litCarViewAlModelsLink.Text;
          */


            //hplBuildYourHonda.NavigateUrl = domain + ResourceManager.GetString("BuildYourHondaURL");
            //hplCompare.NavigateUrl = domain + ResourceManager.GetString("CompareRootFolder");
            //hplCertifiedUsed.NavigateUrl = ResourceManager.GetString("CertifiedUsedURL");
            //hplSpecialOffers.NavigateUrl = domain + ResourceManager.GetString("SpecialOffersURL");
            //hplDealerLocator.NavigateUrl = domain + ResourceManager.GetString("DealerLocatorRootFolder");
            //hplLeaseAndFinance.NavigateUrl = domain + ResourceManager.GetString("LeaseAndFinanceUrl");
            //hplPartsAndAccessories.NavigateUrl = domain + ResourceManager.GetString("PartsAndAccessoriesURL");
            //hypMerchandise.NavigateUrl = domain + ResourceManager.GetString("ApparelUrl");
            //hplEvents.NavigateUrl = domain + ResourceManager.GetString("EventsUrl");
      //  }

      
        /*
         * TODO : Remove comment if required
         * 
        protected void repModels_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (isAbsolute != true) { domain = ""; }

            HondaModel objModel = (HondaModel)e.Item.DataItem;
            if (objModel != null)
            {
                HyperLink hypModel = (HyperLink)e.Item.FindControl("hypModel");
                HyperLink hypModelImage = (HyperLink)e.Item.FindControl("hypModelImage");
                Literal litModelImage = (Literal)e.Item.FindControl("litModelImage");
                Literal litModelName = (Literal)e.Item.FindControl("litModelName");
                Literal litModeldescription = (Literal)e.Item.FindControl("litModeldescription");

                string urlPath = ""; // Path.Combine(domain + "/", objModel.BMUrlName);

                hypModel.NavigateUrl = urlPath;
                hypModel.ToolTip = objModel.ModelName;

                hypModelImage.NavigateUrl = urlPath;
                hypModelImage.ToolTip = objModel.ModelName;

                litModelImage.Text = string.Format("<img src='{3}/buildittool/{0}/data/json/assets/{1}/hondaca_modelNav.png' width='160' height='200' alt='{2}' />",HondaCA.Common.Global.BuildItTargetFolder, objModel.ModelExportKey, objModel.ModelName, domain);

                litModelName.Text = objModel.ModelName;

                litModeldescription.Text = string.Format("{0:C0}<span class='dropdown_msrp'> {1}</span>", objModel.MSRP, ResourceManager.GetString("MSRP"));//&#8224;

            }
        }
         * */
    }
}
