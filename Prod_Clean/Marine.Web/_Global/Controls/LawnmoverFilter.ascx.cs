﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;
using Navantis.Honda.CMS.Demo;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using HondaCA.Service.Model;

namespace Marine.Web._Global.Controls
{
    public partial class Lawnmover : System.Web.UI.UserControl
    {
        CmsContentBasePage cmsbasePage;
        private ResourceManager _ResourceManager;
        public string value1 = "transmission";
        public string value2 = "mulching_system";
        public string value3 = "deck_type__standard_";
        public string value4 = "bag_capacity";
        public string value5 = "deck_material";

        public decimal minBagCapacity { get; set; }
        public decimal maxBagCapacity { get; set; }

        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                this.ResourceManager = cmsbasePage.ResourceManager;
            }
            TrimSpecService trimSpectService = new TrimSpecService();
            EntityCollection<TrimSpect> trimSpect = trimSpectService.GetTrimSpectRange(cmsbasePage.TargetID, cmsbasePage.PageLanguage, value4);
            minBagCapacity = trimSpect[0].minValue;
            maxBagCapacity = trimSpect[0].maxValue;           
        }
    }
}