﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using HondaPE.Service.Model;
using HondaCA.Common;
using Marine.Web.Code;
using System.IO;
using HondaCA.Service.Model;
using HondaCA.Entity;
using HondaCA.Entity.Model;

namespace Marine.Web._Global.Controls
{
    public partial class AddToCompare : BaseUserControl
    {
        CmsContentBasePage cmsbasePage;
        string BaseModelUrl = string.Empty;
        string SeriesUrl = string.Empty;
        string TrimExportKey = string.Empty;
        public string BaseModelFamilyUrl { get; set; }
        public string CompareUrl { get; set; }

        protected string ModelSelectorUrl = string.Empty;

        public string ModelFamilyName;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                this.PageLanguage = cmsbasePage.PageLanguage;
                this.ResourceManager = cmsbasePage.ResourceManager;

                HondaModelService ms = new HondaModelService();
                EntityCollection<BaseModelFamily> oBmfs = ms.GetModelFamilyByFamilyURL(PageLanguage, BaseModelFamilyUrl);
                BaseModelFamily oBmf = null;

                if (oBmfs != null && oBmfs.Count > 0)
                {
                    oBmf = oBmfs[0];
                }

                if (oBmf != null)
                {
                    ModelFamilyName = oBmf.ModelFamilyName;
                }

                ModelSelectorUrl = string.Format("/service/ComparePopUpSelector.aspx?L={0}&amp;ModelFamilyUrl={1}", cmsbasePage.PageLanguage == Language.English ? "en" : "fr", BaseModelFamilyUrl);
            }
        }
        /*
            try
            {
                BaseModelUrl = Request.QueryString["BaseModelUrl"] ?? string.Empty;
                SeriesUrl = Request.QueryString["BaseSeriesUrl"] ?? string.Empty;
                TrimExportKey = Request.QueryString["TrimExportKey"] ?? string.Empty;
                string CookieName = string.Format("addtocompare_{0}", BaseModelUrl.ToLower());

                string GetTrmListFromCookie("");

                PETrimService trimService = new PETrimService();
                //HondaCA.Entity.Model.Trim oTrim = trimService.GetTrimByTrimExportKey(BaseModelUrl, this.TargetID, TrimExportKey, this.PageLanguage.ToString());
                HondaCA.Entity.Model.Trim oTrim = trimService.GetTrimByTrimID(cmsbasePage.TargetID, (int)cmsbasePage.ContentPage.ProductID, (int)cmsbasePage.ContentPage.TrimID, cmsbasePage.PageLanguage.GetCultureStringValue());// GetTrimByTrimExportKey(BaseModelUrl, this.TargetID, TrimExportKey, this.PageLanguage.ToString());

                HttpCookie NewCookie = new HttpCookie(CookieName);

                try
                {
                    NewCookie.Values[oTrim.TrimExportKey] = String.Format("{0}@{1}@{2}", oTrim.MSRP, SeriesUrl, DateTime.Now.ToString());
                }

                catch (Exception ex)
                {

                }
                string html = null;
                if (Request.Cookies[CookieName] != null)
                {
                    for (int i = 0; i < Request.Cookies.Count; i++)
                    {
                        if (Request.Cookies[i].Name.Equals(CookieName))
                        {
                            HttpCookie CurrentCookie = Request.Cookies[i];
                            System.Collections.Specialized.NameValueCollection CookieValues = CurrentCookie.Values;
                            string[] CookieValueNames = CookieValues.AllKeys;
                            int cnt = 0;
                            for (int j = 0; j < CookieValues.Count; j++)
                            {
                                if (TrimExportKey != CookieValueNames[j])
                                {
                                    NewCookie.Values[CookieValueNames[j]] = CookieValues[j];
                                    PanelRecentlyVisited.Visible = true;
                                    html += DoHtml(cnt, CookieValueNames[j]);
                                    cnt++;
                                }
                            }
                        }
                    }
                    if (PanelRecentlyVisited.Visible)
                    {
                        LiteralRecentlyVisited.Text = html;
                        LiteralRecentlyViewd.Text = ResourceManager.GetString("txtRecentlyViewed");
                        LiteralCompare.Text = ResourceManager.GetString("txtCompare");
                        LiteralCompareThesePE.Text = string.Format(ResourceManager.GetString("txtCompareThesePE"), CommonFunctions.ucWords(oTrim.BaseModelName));
                    }
                }
                else
                {
                    PanelRecentlyVisited.Visible = false;
                }
                NewCookie.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(NewCookie);
            }
            catch (Exception ex)
            {
            }
         */

//        private void GetTrmListFromCookie(string p)
//        {
//            throw new NotImplementedException();
//        }


      

//         protected void repTrims_ItemDataBound(Object sender, RepeaterItemEventArgs e)
//        {
//            HondaCA.Entity.Model.Trim oTrim = (HondaCA.Entity.Model.Trim)e.Item.DataItem;
//            if (oTrim != null)
//            {
                
                
//                //HyperLink hypModel = (HyperLink)e.Item.FindControl("hypModel");
//                //HyperLink hypModelImage = (HyperLink)e.Item.FindControl("hypModelImage");
//                //Literal litModelImage = (Literal)e.Item.FindControl("litModelImage");
//                //Literal litModelName = (Literal)e.Item.FindControl("litModelName");
//                //Literal litModeldescription = (Literal)e.Item.FindControl("litModeldescription");

//                //string urlPath = Path.Combine(domain + "/", oTrim.BMUrlName);

//                //hypModel.NavigateUrl = urlPath;
//                //hypModel.ToolTip = oTrim.ModelName;

//                //hypModelImage.NavigateUrl = urlPath;
//                //hypModelImage.ToolTip = oTrim.ModelName;

//                //litModelImage.Text = string.Format("<img src='{3}/buildittool/{0}/data/json/assets/{1}/hondaca_modelNav.png' width='160' height='200' alt='{2}' />",HondaCA.Common.Global.BuildItTargetFolder, oTrim.ModelExportKey, oTrim.ModelName, domain);

//                //litModelName.Text = oTrim.ModelName;
                
//                //if (oTrim.MSRP == 0)
//                //    litModeldescription.Text = @"<span class=""coming-soon"">" + string.Format("{0:C0}", oTrim.MSRP.GetMSRPAsStringIfZero(this.PageLanguage.GetCultureStringValue())) + "</span>";
//                //else
//                //    litModeldescription.Text = string.Format("{0:C0}<span class='dropdown_msrp'> {1}</span>", oTrim.MSRP, ResourceManager.GetString("MSRP"));//&#8224;
                    

//            }
//        }

//        private string DoHtml(int cnt, string export_key)
//        {
//            PETrimService trimService = new PETrimService();
//            HondaCA.Entity.Model.Trim oTrim = trimService.GetTrimByTrimExportKey(BaseModelUrl, this.TargetID, export_key, this.PageLanguage.ToString());
//            string html = null;
//            html = string.Format(@"
//                <li class=""fl{0}product"">
//                    <div class=""image""><a href=""{4}""><img src=""{1}?crop=auto&amp;width=125&amp;height=125"" alt='' /></a></div>
//                    <div class=""description"">
//                        <h3 class=""nm product-item""><a href=""{4}"">{2}</a></h3>
//                        <div class=""price"">{3}</div>
//                    </div>
//                </li>", cnt == 0 ? " nm pf " : " ",
//                     string.Format(string.Format(ResourceManager.GetString("pathBuildItToolImagePath"), HondaCA.Common.Global.BuildItTargetFolder) + @"{0}/{1}/{2}",
//                           oTrim.BaseModelExportKey,
//                           oTrim.TrimExportKey,
//                           ResourceManager.GetString("imgDefaultThumbImage")),
//                     oTrim.TrimName,
//                     string.Format("{0:C0}", oTrim.MSRP),
//                     "/" + oTrim.BaseModelExportKey + "/" + oTrim.TrimExportKey);
//            return html;
//        }
    }
}