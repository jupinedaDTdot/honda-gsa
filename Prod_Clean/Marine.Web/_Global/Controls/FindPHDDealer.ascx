﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FindPHDDealer.ascx.cs" Inherits="Marine.Web._Global.Controls.FindPHDDealer" %>

<script  type="text/javascript" language=javascript>

    function ValidateCity() {
        var x = document.getElementById("<%=txtCity1.ClientID%>"); //ctl00_ContentArea_txtCity1   
        var val = x.value;
        if (val == "") {
            alert("<%=ResourceManager.GetString("EmptySearchBoxMessage")%>");
            x.focus();
            return false;
        }
        return true;
    }

    function ValidatePostalCode() {
        var x = document.getElementById("<%=txtPostalCode.ClientID%>"); //ctl00_ContentArea_txtPostalCode
        var val = x.value;
        if (val == "") {
            alert("<%=ResourceManager.GetString("EmptySearchBoxMessage")%>");
            x.focus();
            return false;
        }
        return true;
    }
</script>
    <h4 class="pf"><asp:Literal runat="server" ID="LitFindADealer"></asp:Literal></h4>
    <ul class="search_options">
	    <li class="search_sub_header"><label><asp:Literal runat="server" ID="LitSearchBy"></asp:Literal></label></li>
	    <li class="postal_code">

		    <label class="fl"><asp:Literal runat="server" ID="LitZipCode"></asp:Literal></label>
		    <div class="fl input_box">
                <asp:TextBox TabIndex="1" class="fl input_extra_small" id="txtPostalCode" pattern="[a-zA-Z][0-9][a-zA-Z]( ?[0-9][a-zA-Z][0-9])?" maxlength="7" runat="server" name="postal_code" ></asp:TextBox>										
                <asp:LinkButton TabIndex="2" class="fl btn" data-type="submit" data-role="button" ID="lnkBPostalCode" runat="server" OnClick="lnkBPostalCode_OnClick" OnClientClick=" return ValidatePostalCode()" ></asp:LinkButton> 					
			    <div class="clr"></div>
		    </div>
		    <div class="clr"></div>
	    </li>

	    <li class="city">
		    <label class="fl"><asp:Literal runat="server" ID="LitCity"></asp:Literal></label>
		    <div class="fl input_box">
                <asp:TextBox TabIndex="3" class="fl input_medium dropdown_autocomplete_city dropdown_autocomplete_city_sidebar" id="txtCity1"  runat="server" maxlength="20" name="city" ></asp:TextBox>
                <asp:LinkButton TabIndex="4" class="fl btn" ID="lnkBCitySearch1" runat="server" OnClick="lnkBCitySearch_OnClick" OnClientClick=" return ValidateCity()"></asp:LinkButton> 
			    <div class="clr"></div>
		    </div>
		    <div class="clr"></div>
	    </li>
    </ul>