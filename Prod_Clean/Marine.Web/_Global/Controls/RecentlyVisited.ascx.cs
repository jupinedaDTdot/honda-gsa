﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HondaPE.Entity;
using HondaPE.Service;
using HondaPE.Service.Model;
using Marine.Service.Extensions;
using Navantis.Honda.CMS.Demo;
using System.Resources;
using System.Configuration;
using HondaCA.Common;
using Marine.Web.Code;
using System.Text;
using HondaCA.Entity.Model;

namespace Marine.Web._Global.Controls
{
    public partial class RecentlyVisited : System.Web.UI.UserControl
    {
        protected CmsContentBasePage cmsbasePage;
        CmsContentBasePage basePage;

        private ResourceManager _ResourceManager;
        private Language _PageLanguage;
        private int _targetID;

        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }

        public Language PageLanguage
        {
            get { return _PageLanguage; }
            set { _PageLanguage = value; }
        }

        public int TargetID
        {
            get { return _targetID; }
            set { _targetID = value; }
        }

        string ModelFamilyUrl = string.Empty;
        string CategoryUrl = string.Empty;
        string TrimUrl = string.Empty;
        string CompareUrl = string.Empty;
        int ShowOnly = 3;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                this.PageLanguage = cmsbasePage.PageLanguage;
                this.ResourceManager = cmsbasePage.ResourceManager;
            }
            basePage = (CmsContentBasePage)this.Parent.Page;  
            BaseMaster basemaster_for_region = new BaseMaster();
                        
            try
            {
                //ModelFamilyUrl = Request.QueryString["ModelFamilyUrl"];
                CategoryUrl = Request.QueryString["CategoryUrl"];
                TrimUrl = Request.QueryString["TrimUrl"];
                string CookieName = string.Format("HondaMarine"); //{0}", Marine.Web.Code.CommonFunctions.ucWords(CategoryUrl.Replace("-", " ")));

                PETrimService trimService = new PETrimService();
                HondaCA.Entity.Model.Trim oTrim = trimService.GetTrimByTrimID(cmsbasePage.TargetID, (int)cmsbasePage.ContentPage.ProductID, (int)cmsbasePage.ContentPage.TrimID, cmsbasePage.PageLanguage.GetCultureStringValue());
                oTrim = oTrim.GetPriceAdjustedTrim(cmsbasePage.TargetID, cmsbasePage.PageLanguage);


                string TrimExportKey = oTrim.TrimExportKey;
                CompareUrl = string.Format("{0}/{1}-{2}", ResourceManager.GetString("CompareRootFolder"), oTrim.TrimExportKey, oTrim.ModelYearYear);

                HttpCookie NewCookie = new HttpCookie(CookieName);

                decimal MSRP;
                if (!cmsbasePage.IsQuebecPricing)
                  MSRP = oTrim.MSRP;
                else
                  MSRP = oTrim.MSRP + oTrim.FreightPDI;  
              
               try
                {  
                  NewCookie.Values[oTrim.TrimID.ToString()] = String.Format("{0}@{1}@{2}", MSRP, CategoryUrl, DateTime.Now.ToString());
                }
                catch (Exception ex)
                {
                }
                string html = null;
                if (Request.Cookies[CookieName] != null)
                {
                    for (int i = 0; i < Request.Cookies.Count; i++)
                    {
                        if (Request.Cookies[i].Name.Equals(CookieName))
                        {
                            HttpCookie CurrentCookie = Request.Cookies[i];
                            System.Collections.Specialized.NameValueCollection CookieValues = CurrentCookie.Values;
                            string[] CookieValueNames = CookieValues.AllKeys;
                            int cnt = 0;
                            for (int j = 0; j < CookieValues.Count; j++)
                            {
                                if (!string.IsNullOrWhiteSpace(CookieValueNames[j]) && !string.IsNullOrWhiteSpace(CookieValues[j]) && oTrim.TrimID.ToString() != CookieValueNames[j])
                                {
                                    NewCookie.Values[CookieValueNames[j]] = CookieValues[j];
                                    PanelRecentlyVisited.Visible = true;
                                    if (cnt >= ShowOnly) break;
                                    html += DoHtml(cnt, int.Parse(CookieValueNames[j]));
                                    cnt++;
                                }
                            }
                        }
                    }
                    if (PanelRecentlyVisited.Visible)
                    {
                        HondaCA.Service.Model.HondaModelService ms = new HondaCA.Service.Model.HondaModelService();
                        HondaCA.Entity.Model.ModelCategory mc = ms.GetCategoryByCategoryUrl(this.TargetID, PageLanguage, CategoryUrl); //ms.GetModelFamilyByFamilyURL(PageLanguage, ModelFamilyUrl)[0];

                        LiteralRecentlyVisited.Text = html;
                        LiteralRecentlyViewd.Text = ResourceManager.GetString("txtRecentlyViewed");
                        HyperLinkCompare.Text = string.Format("<span>{0}</span>", ResourceManager.GetString("txtCompare"));
                        HyperLinkCompare.NavigateUrl = CompareUrl;
                        // FIXME workaround for far-too-long trimmer name in French.
                        string name = CommonFunctions.ucWords(mc.CategoryName ?? string.Empty).Replace("/", " / ");
                        LiteralCompareThesePE.Text = string.Format(ResourceManager.GetString("txtCompareModel3"), name);
                    }
                }
                else
                {
                    PanelRecentlyVisited.Visible = false;
                }
                NewCookie.Expires = DateTime.Now.AddDays(30);
                Response.Cookies.Add(NewCookie);
            }
            catch (Exception ex)
            {
            }
        }

        private string DoHtml(int cnt, int TrimID)
        {
          
            string sUrl = string.Empty;
            PETrimService trimService = new PETrimService();
            HondaCA.Entity.Model.Trim oTrim = trimService.GetTrimByTrimID(this.TargetID, -1, TrimID, PageLanguage.GetCultureStringValue());
            oTrim = oTrim.GetPriceAdjustedTrim(TargetID, PageLanguage);

            CompareUrl += "," + oTrim.TrimExportKey + "-" + oTrim.ModelYearYear;

            HondaCA.Service.Model.HondaModelService ms = new HondaCA.Service.Model.HondaModelService();
            HondaCA.Entity.Model.ModelCategory oCategory = null;

            try
            {
                oCategory = ms.GetCategoryByTrimExportKey(this.TargetID, oTrim.TrimExportKey, PageLanguage)[0];
            }
            catch (Exception)
            {
            }

            string html = null;
            StringBuilder sb = new StringBuilder();

            Colour oColour = null;
            if (trimService.IsComingSoonWithColor(oTrim, oColour))
            {
                sb.AppendFormat(@"<div class=""fc coming-soon""><strong class=""final price"">{0}</strong><span>{1}</span></div>", ResourceManager.GetString("txtComingSoon"), ResourceManager.GetString("MSRP*")); //Price without any kind of discount
            }
            else
            {
              decimal MSRP;
              if (!cmsbasePage.IsQuebecPricing)
              {
                 MSRP = oTrim.MSRP;
              }
              else
              {
               MSRP = oTrim.MSRP + oTrim.FreightPDI;
              }
                //decimal MSRP = oTrim.MSRP + oTrim.FreightPDI;

                if (oTrim.HasDiscount())
                {
                    sb.AppendFormat(@"<div class=""fc original price""><del><span class=""strike""></span><strong>{0}</strong></del><span>{1}</span></div>", string.Format(ResourceManager.GetString("valMSRP4"), MSRP), ResourceManager.GetString("MSRP*"));
                    sb.AppendFormat(@"<div class=""fc special price""><strong>{0}</strong><span>{1}</span></div>", string.Format(ResourceManager.GetString("valMSRP4"), oTrim.GetDiscount(cmsbasePage.IsQuebecPricing)), ResourceManager.GetString("Discount"));
                    sb.AppendFormat(@"<div class=""fc final price""><strong>{0}</strong><span>{1}</span></div>", string.Format(ResourceManager.GetString("valMSRP4"), oTrim.GetFinalPrice(cmsbasePage.IsQuebecPricing)), ResourceManager.GetString("YourPrice*"));
                }
                else       //Without discount
                    sb.AppendFormat(@"<div class=""fc final price""><strong>{0}</strong><span>{1}</span></div>", string.Format(ResourceManager.GetString("valMSRP4"), MSRP), ResourceManager.GetString("MSRP*")); //Price without any kind of discount
            }

            html = string.Format(@"
                <li class=""fl{0}product"">
                    <div class=""image""><a href=""{4}""><img src=""{1}?crop=auto&amp;width=80&amp;height=115"" alt='' /></a></div>
                    <div class=""description"">
                        <h3 class=""nm product-item""><a href=""{4}"">{2}</a></h3>
                        {3}
                    </div>
                </li>"
                , cnt == 0 ? " nm pf " : " "
                , CommonFunctions.BuildModelImageUrl(oTrim)
                , oTrim.TrimName
                //,string.Format("{0:C0}",oTrim.MSRP)
        , sb.ToString()
                , string.Format("/{1}", oTrim.ModelFamilyURL, (oCategory == null ? "" : oCategory.CategoryUrl + "/") + oTrim.TrimUrl));
            return html;
        }
    }
}