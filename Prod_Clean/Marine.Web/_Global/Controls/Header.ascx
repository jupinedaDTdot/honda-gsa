﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="Marine.Web._Global.Controls.Header" %>
<%--<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>--%>
<div class="wrapper nmb nmt">
    <div id="header">
        <%--<ul id="language" class="fc">
    	    <li class="fr first"><a runat="server" id="LanguageUrl" href=""> <asp:Literal runat ="server" ID="LanguageName" /> </a></li>
          <li class="fr"><a runat="server" id="SecondLanguageURL" href=""> <asp:Literal runat ="server" ID="SecondLanguageName" /> </a></li>
    	    <li class="fr"><a href="<%=ResourceManager.GetString("urlBackToHonda")%>"><%=ResourceManager.GetString("txtBackToHonda")%></a></li>
	        <li class="fr"><a href="<%=ResourceManager.GetString("urlHondaCanada")%>" target="_blank"><%=ResourceManager.GetString("txtHondaCanada")%></a></li>
	    </ul>--%>
	    <div class="fl">
		    <asp:HyperLink ID="hplLogo" runat="server" CssClass="header_logo" />
            <%--<a id="logo" href="#"></a> May need to change the style class defined after #logo since that id cannot be kept in above control--%>
	    </div>
	    <div class="fr" id="sub_header">
		    <div id="search" class="dd_button fr">
			    <input type="text" name="search" value="" placeholder="Search" />
			    <a href="#" class="clear dn"></a>
		    </div>
		    <ul id="secondary_nav" class="fr">
			    <asp:Literal ID="litDealerLocatorLink" runat="server" />
			    <asp:Literal ID="litLeaseAndFinanceLink" runat="server" />
                <asp:Literal ID="litAccessoriesLink" runat="server" Visible="false" />
                <asp:Literal ID="litPartsServicesLink" runat="server" Visible="false" />
                <asp:Literal ID="litSafetyLink" runat="server" />
                <asp:Literal ID="litNewsEventsLink" runat="server" />
		    </ul>
		    <div class="clrfix"></div>
	    </div>
	    <div class="clrfix"></div>
	    <div id="main_menu">
		    <ul id="primary_nav">
                <asp:Literal ID="ltPrimaryMenu" runat="server"></asp:Literal>			    
		    </ul>
		    <div class="clrfix"></div>
	    </div>
        <%--<div class="landing-heading">
            <span class="commercial-products"><a href="<%=ResourceManager.GetString("txtCommercialProductsURL")%>"><%=ResourceManager.GetString("txtCommercialProducts")%></a></span>        
        </div>--%>
    </div>
</div>
<asp:Label runat="server" ID="listModel"></asp:Label>