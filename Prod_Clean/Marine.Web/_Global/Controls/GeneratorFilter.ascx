﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneratorFilter.ascx.cs" Inherits="Marine.Web._Global.Controls.GeneratorFilter" %>

<div class="fc pf sliders-box-group sliders-box-group-of-5">
    <div class="first sliders-box range-sliders-box"
            property="honda:model-filter"
            data-filter="value-1"
            data-filter-min="<%=minWatts %>"
            data-filter-type="range"
            data-filter-max="<%=maxWatts %>">

		<h3><%=ResourceManager.GetString("txtWattage")%></h3>
	<%--	<a class="pf btn-help"
			property="act:tooltip"
			data-position="right top"
			data-tooltip-class="tooltip-top-alt"
			data-tooltip-content-target="#product-sliders-help1"></a>--%>
		<div class="fc slider-controls">
			<div class="pf slider-controls-cap-left"></div>
			<div class="slider-controls-inner ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
			</div>
			<div class="pf slider-controls-cap-right"></div>
		</div>
		<div class="fc labels">
			<span class="fl"><%=minWatts%></span>
			<span class="fr"><%=maxWatts %></span>
		</div>
	</div>

	<div class="sliders-box filter-control"
            property="honda:model-filter"
            data-filter="value-2"
            data-filter-type="radio"
            data-filter-default-value="">
		<h3><%=ResourceManager.GetString("txtACOutput")%></h3>
<%--		<a class="pf btn-help"
			property="act:tooltip"
			data-position="right top"
			data-tooltip-class="tooltip-top-alt"
			data-tooltip-content-target="#product-sliders-help2"></a>--%>
							
		<div class="fc slider-controls">
            <div class="fc field">
                <input type="radio" name="value-2" value="<%=ResourceManager.GetString("txtACOutputOpt1") %>" />
                <label for="check1"><%=ResourceManager.GetString("txtACOutputOpt1") %></label>
            </div>

            <div class="fc field">
                <input type="radio" name="value-2" value="<%=ResourceManager.GetString("txtACOutputOpt2") %>" />
                <label for="check1"><%=ResourceManager.GetString("txtACOutputOpt2") %></label>
            </div>                                

            <div class="fc field">
                <input type="radio" name="value-2" value="<%=ResourceManager.GetString("txtACOutputOpt3") %>" />
                <label for="check1"><%=ResourceManager.GetString("txtACOutputOpt3") %></label>
            </div>                                

		</div>
	</div>
	<div class="sliders-box range-sliders-box"
            property="honda:model-filter"
            data-filter="value-3"
            data-filter-min="<%=minRunTime %>"
            data-filter-type="range"
            data-filter-max="<%=maxRunTime %>">
		<h3><%=ResourceManager.GetString("txtRunTime")%></h3>
<%--
		<a class="pf btn-help"
			property="act:tooltip"
			data-position="right top"
			data-tooltip-class="tooltip-top-alt"
			data-tooltip-content-target="#product-sliders-help3"></a>--%>
							
		<div class="fc slider-controls">
			<div class="pf slider-controls-cap-left"></div>
			<div class="slider-controls-inner ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
			</div>
			<div class="pf slider-controls-cap-right"></div>
		</div>

							
		<div class="fc labels">
			<span class="fl"><%=minRunTime%> <%=ResourceManager.GetString("txtRunTimeScale") %></span>
			<span class="fr"><%=maxRunTime%> <%=ResourceManager.GetString("txtRunTimeScale") %></span>
		</div>

	</div>
	<div class="sliders-box"
            property="honda:model-filter"
            data-filter="value-4"
            data-filter-default-value=""
            data-filter-type="radio">
		<h3><%=ResourceManager.GetString("txtAVR")%></h3>
	<%--	<a class="pf btn-help"
			property="act:tooltip"
			data-position="right top"
			data-tooltip-class="tooltip-top-alt"
			data-tooltip-content-target="#product-sliders-help4"></a>--%>
							
		<div class="fc slider-controls">
            <div class="fc field">
                <input type="radio" name="value-4" value="1" />
                <label for="check1"><%=ResourceManager.GetString("txtAVROpt1")%></label>
            </div>
            <div class="fc field">
                <input type="radio" name="value-4" value="2" />
                <label for="check1"><%=ResourceManager.GetString("txtAVROpt2")%></label>
            </div>
            <div class="fc field">
                <input type="radio" name="value-4" value="3" />
                <label for="check1"><%=ResourceManager.GetString("txtAVROpt3")%></label>
            </div>
		</div>
	</div>
	<div class="last sliders-box"
            property="honda:model-filter"
            data-filter="value-5"
            data-filter-default-value="False"
            data-filter-type="checkbox">

		<h3><%=ResourceManager.GetString("txtGFCI")%></h3>
<%--		<a class="pf btn-help"
			property="act:tooltip"
			data-position="right top"
			data-tooltip-class="tooltip-top-alt"
			data-tooltip-content-target="#product-sliders-help4"></a>--%>
							
		<div class="fc slider-controls">
			<div class="fc field">
                <input id="Checkbox1" type="checkbox" name="value-5" value="True" />
                <label for="Checkbox1"><%=ResourceManager.GetString("txtGFCIReq")%></label>
            </div>
		</div>							
	</div>						
</div>