﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;

using HondaCA.Common;
//using HondaCA.Service.Common;
using HondaDealer.Service;
//using HondaCA.Entity.Common;
using HondaDealer.Entity;
using System.Text;
using Navantis.Honda.CMS.Demo;
using System.Collections;

namespace Marine.Web._Global.Controls
{
    public partial class FindPHDDealer : System.Web.UI.UserControl //, ISetPlaceholder
    {

        private string domain = "http://" + HondaCA.Common.Global.CurrentDomain.ToLower();

        CmsContentBasePage cmsbasePage;

        private ResourceManager _ResourceManager;
        private Language _PageLanguage;

        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }

        public Language PageLanguage
        {
            get { return _PageLanguage; }
            set { _PageLanguage = value; }
        }
        public string locatorXmlfile;

        public HondaCA.Entity.Model.HondaModel objModel;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                this.PageLanguage = cmsbasePage.PageLanguage;
                this.ResourceManager = cmsbasePage.ResourceManager;
            }
            if (PageLanguage == Language.French)
            {
                locatorXmlfile = "locator_map_fr.xml";
            }
            else if (PageLanguage == Language.English)
            {
                locatorXmlfile = "locator_map.xml";
            }

            SetDefaulPage();

            if (this.IsPostBack)
            {
                //check for region selector postback
                string SelectedRegion = null;
                try
                {
                    SelectedRegion = Request.Form["RegionSelectBoxSide"];
                }
                catch (Exception)
                {
                    SelectedRegion = null;
                }
                if (!string.IsNullOrEmpty(SelectedRegion))
                {
                    Response.Redirect(SelectedRegion);
                }
            }
        }

        protected void SetDefaulPage()
        {

            LitFindADealer.Text = ResourceManager.GetString("txtFindPHD");
            LitSearchBy.Text = ResourceManager.GetString("txtSearchBy");
            LitZipCode.Text = ResourceManager.GetString("txtPostalCode");
            LitCity.Text = ResourceManager.GetString("txtCity");
            //LitSelect.Text = ResourceManager.GetString("txtSelect");
            // LitSelectYourRegion.Text = ResourceManager.GetString("txtSelectYourRegion");
            lnkBPostalCode.Text = ResourceManager.GetString("txtButtonGo");

            lnkBCitySearch1.Text = ResourceManager.GetString("txtButtonGo");

            // No need for this line as control is directly accessible  it return null all the time 
            //System.Web.UI.HtmlControls.HtmlControl ccFindADealerHeading = (System.Web.UI.HtmlControls.HtmlControl)Page.FindControl("ccFindADealerHeading");

            //if (ccFindADealerHeading != null)
            //{
            //    ccFindADealerHeading.Attributes.Add("class", ((PageLanguage == Language.French) ? "heading multiline heading_small" : "heading"));
            //}

            //hypBookaTestDrive.Text = "<span>" + ResourceManager.GetString("txtBookATestDrive") + "</span>";

            String bookaTestDriveUrl;

            if (this.PageLanguage == Language.French)
            {
                bookaTestDriveUrl = domain + "/modelpages/BookTestDrive.aspx?L=fr";
            }
            else
            {
                bookaTestDriveUrl = domain + "/modelpages/BookTestDrive.aspx?L=en";
            }
            ////hypBookaTestDrive.NavigateUrl = bookaTestDriveUrl;
            //hypBookaTestDrive.NavigateUrl = "#";
            //if (objModel != null)
            //{   
            //    hypBookaTestDrive.Attributes["onclick"] = "javascript:window.open('https://www.customerreach.ca/hondacm/scheduler/appointment2.aspx?txt_modelname_" + (PageLanguage == Language.French ? "fr" : "en") + "=" + objModel.ModelYear + " " + objModel.ModelName + "&txt_language=" + (PageLanguage == Language.French ? "fr" : "en") + "','mywindow','width=748, height=732');return false;";
            //}
            //else 
            //{
            //    hypBookaTestDrive.Attributes["onclick"] = "javascript:window.open('https://www.customerreach.ca/hondacm/scheduler/appointment2.aspx?txt_language=" + (PageLanguage == Language.French ? "fr" : "en") + "','mywindow','width=748, height=732');return false;";
            //}
            HondaDealer.Service.RegionService rs = new HondaDealer.Service.RegionService();
            EntityCollection<Region> regionList = new EntityCollection<Region>();
            try
            {
                regionList = rs.GetAllRegions(PageLanguage.GetCultureStringValue());
            }
            catch (Exception ex)
            {
                throw new Exception("hondaPE:finadadealer:SetDefaulPage" + ex.Message, ex);
            }
        }


        protected void lnkBCitySearch_OnClick(object sender, EventArgs e)
        {
            string url = DealerLocatorCommon.redirectCity(txtCity1.Text.Trim(), ResourceManager.GetString("urlDealerLocatorPhd"), (HondaDealer.Common.Language)PageLanguage);

            if (url != string.Empty)
            {
                if (!(url.Contains("NoProvince")))
                    Response.Redirect(url);
                else
                    Response.Redirect(DealerLocatorCommon.getSearchErrorUrl(DealerLocatorCommon.RepDealerSearch(txtCity1.Text.Trim()), ResourceManager.GetString("DealerLocatorRootFolder")));
            }
        }

        protected void lnkBPostalCode_OnClick(object sender, EventArgs e)
        {
            string url = DealerLocatorCommon.redirectPostalCode(txtPostalCode.Text.Trim(), ResourceManager.GetString("urlDealerLocatorPhd"), false);

            if (url != string.Empty) Response.Redirect(url);
        }
    }
}