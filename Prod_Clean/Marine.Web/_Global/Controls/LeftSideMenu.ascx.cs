﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using Marine.Web.ContentPages;

namespace Marine.Web._Global.Controls
{     
    public partial class LeftSideMenu :System.Web.UI.UserControl
    {
        public string menuTitle { get; set; }
        public string menuCMSControlName { get; set; }
        public string menuMainLiCSSClass { get; set; }
        public string menuCSSH4 { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            mainLi.Attributes.Add("class", menuMainLiCSSClass);
            litMenuTitle.Text = menuTitle;
            ElementControl1.ElementName = menuCMSControlName;
            if(menuCSSH4 != "" && menuCSSH4 != string.Empty)
                ctrlH4.Attributes.Add("class",menuCSSH4);
                        
        }

        public void LoadData(string loadText)
        {
            plMenu.Controls.Add(new LiteralControl(loadText));
        }
    }
}