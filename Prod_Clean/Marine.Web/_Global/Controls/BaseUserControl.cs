﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Navantis.Honda.CMS.Demo;
using System.Resources;
using HondaCA.Common;

namespace Marine.Web._Global.Controls
{
    public class BaseUserControl : System.Web.UI.UserControl
    {
        private ResourceManager _ResourceManager;
        private Language _PageLanguage;
        private int _targetID;

        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }

        public Language PageLanguage
        {
            get { return _PageLanguage; }
            set { _PageLanguage = value; }
        }

        public int TargetID
        {
            get { return _targetID; }
            set { _targetID = value; }
        }

    }
}