﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using Marine.Web.ContentPages;

namespace Marine.Web._Global.Controls
{
    public partial class tabMenu : System.Web.UI.UserControl
    {
        public string menuCSSClass { get; set; }
        public string menuCMSControlName { get; set; }
        //public string[] menuTitles { get; set; }
        //public string[] menuLinks { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            ElementControl1.ElementName = menuCMSControlName;

        }

        public void LoadData(Navantis.Honda.CMS.Client.Elements.Menu menu, Boolean IsStaging, Boolean IsModelPage)
        {
            StringBuilder sb = new StringBuilder();
            if(menu != null)
            {
                if(menu.ContentElement.Display || IsStaging==true)
                {

                    if(menu.Items != null)
                    {
                        sb.Append(string.Format(@"<ul class=""{0}"">",menuCSSClass));
                        int menuCnt = 0;
                        foreach(Navantis.Honda.CMS.Client.Elements.MenuItem item in menu.Items)
                        {
                            
                            string liCss = "";
                            string cssMultiline = "";

                            liCss = menuCnt==0 ? "fl first" : menuCnt == menu.Items.Count-1 ? "fl last" : "fl";
                            cssMultiline = IsMultiLine(item.Text) ? "multiline" : "";

                            try
                            {
                                if (IsModelPage)
                                {
                                    string BaseUrl = Request.QueryString["BaseUrl"];
                                    if (item.Url == null)
                                    {
                                        if (BaseUrl == Request.RawUrl.ToLower() && IsMultiLine(item.Text))
                                            cssMultiline = "selected multiline";
                                        if (BaseUrl == Request.RawUrl.ToLower() && !IsMultiLine(item.Text))
                                            cssMultiline = "selected";
                                        else if (BaseUrl != Request.RawUrl.ToLower() && IsMultiLine(item.Text))
                                            cssMultiline = "multiline";
                                    }
                                    else
                                    {
                                        if (BaseUrl + item.Url.ToLower() == Request.RawUrl.ToLower() && IsMultiLine(item.Text))
                                            cssMultiline = "selected multiline";
                                        if (BaseUrl + item.Url.ToLower() == Request.RawUrl.ToLower() && !IsMultiLine(item.Text))
                                            cssMultiline = "selected";
                                        else if (BaseUrl + item.Url.ToLower() != Request.RawUrl.ToLower() && IsMultiLine(item.Text))
                                            cssMultiline = "multiline";
                                    }
                                }
                                else
                                {
                                    if (item.Url.ToLower() == Request.RawUrl.ToLower() && IsMultiLine(item.Text))
                                        cssMultiline = "selected multiline";
                                    if (item.Url.ToLower() == Request.RawUrl.ToLower() && !IsMultiLine(item.Text))
                                        cssMultiline = "selected";
                                    else if (item.Url.ToLower() != Request.RawUrl.ToLower() && IsMultiLine(item.Text))
                                        cssMultiline = "multiline";
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            if(IsModelPage)
                                sb.Append(string.Format(@"<li class=""{0}""><a href=""{1}{2}"" class=""{3}"" data-ajaxload-group=""product"" property=""act:ajaxload"">{4}</a></li>", liCss, Request.QueryString["BaseUrl"],item.Url, cssMultiline, item.Text));
                            else
                                sb.Append(string.Format(@"<li class=""{0}""><a href=""{1}"" class=""{2}"">{3}</a></li>", liCss, item.Url, cssMultiline, item.Text));

                            menuCnt++;
                        }
                        
                        sb.Append("</ul>");
                    }
                }
            }
            pltabmenu.Controls.Add(new LiteralControl(sb.ToString()));            
        }

        public void LoadData(Navantis.Honda.CMS.Client.Elements.ModelNav menu, Boolean IsStaging, Boolean IsModelPage)
        {
            StringBuilder sb = new StringBuilder();
            if (menu != null)
            {
                if (menu.ContentElement.Display || IsStaging == true)
                {

                    if (menu.Items != null)
                    {
                        sb.Append(string.Format(@"<ul class=""{0}"">", menuCSSClass));
                        int menuCnt = 0;
                        foreach (Navantis.Honda.CMS.Client.Elements.ModelNavItem item in menu.Items)
                        {

                            string liCss = "";
                            string cssMultiline = "";

                            liCss = menuCnt == 0 ? "fl first" : menuCnt == menu.Items.Count - 1 ? "fl last" : "fl";
                            cssMultiline = IsMultiLine(item.Title) ? "multiline" : "";

                            try
                            {
                                if (IsModelPage)
                                {
                                    string BaseUrl = (Request.QueryString["BaseUrl"] ?? string.Empty ).ToLower();
                                    if (item.URL == null)
                                    {
                                        if (BaseUrl == Request.RawUrl.ToLower() && IsMultiLine(item.Title))
                                            cssMultiline = "selected multiline";
                                        if (BaseUrl == Request.RawUrl.ToLower() && !IsMultiLine(item.Title))
                                            cssMultiline = "selected";
                                        else if (BaseUrl != Request.RawUrl.ToLower() && IsMultiLine(item.Title))
                                            cssMultiline = "multiline";
                                    }
                                    else
                                    {
                                        if (BaseUrl + item.URL.ToLower() == Request.RawUrl.ToLower() && IsMultiLine(item.Title))
                                            cssMultiline = "selected multiline";
                                        if (BaseUrl + item.URL.ToLower() == Request.RawUrl.ToLower() && !IsMultiLine(item.Title))
                                            cssMultiline = "selected";
                                        else if (BaseUrl + item.URL.ToLower() != Request.RawUrl.ToLower() && IsMultiLine(item.Title))
                                            cssMultiline = "multiline";
                                    }
                                }
                                else
                                {
                                    if (item.URL.ToLower() == Request.RawUrl.ToLower() && IsMultiLine(item.Title))
                                        cssMultiline = "selected multiline";
                                    if (item.URL.ToLower() == Request.RawUrl.ToLower() && !IsMultiLine(item.Title))
                                        cssMultiline = "selected";
                                    else if (item.URL.ToLower() != Request.RawUrl.ToLower() && IsMultiLine(item.Title))
                                        cssMultiline = "multiline";
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                            if (IsModelPage)
                                sb.Append(string.Format(@"<li class=""{0}""><a href=""{1}{2}"" class=""{3}"" data-ajaxload-group=""product"" property=""act:ajaxload"">{4}</a></li>", liCss, Request.QueryString["BaseUrl"], item.URL, cssMultiline, item.Title));
                            else
                                sb.Append(string.Format(@"<li class=""{0}""><a href=""{1}"" class=""{2}"">{3}</a></li>", liCss, item.URL, cssMultiline, item.Title));

                            menuCnt++;
                        }

                        sb.Append("</ul>");
                    }
                }
            }
            pltabmenu.Controls.Add(new LiteralControl(sb.ToString()));
        }

        protected bool IsMultiLine(string s)
        {
            MatchCollection collection = Regex.Matches(s, @"[\s]+");
            if (collection.Count > 2)
                return true;
            else
                return false;
        }
    }

    
}
