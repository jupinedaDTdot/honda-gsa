﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using System.Resources;
using HondaCA.Common;
using Marine.Web.Code;
using HondaPE.Service.Model;
using HondaPE.Entity;
using ExpertPdf;
using ExpertPdf.HtmlToPdf;
using HondaCA.Service.Model;

namespace Marine.Web._Global.Controls
{

    public partial class DownloadLink : System.Web.UI.UserControl
    {
        public string elementName { get; set; }
        public string elementName2 { get; set; }
        public string elementName3 { get; set; }
        public string linkTitle { get; set; }
        public string btncssClass { get; set; }
        public string anchocssClass { get; set; }
        public bool isVisible { get; set; }
        public bool isStaging { get; set; }
        CmsContentBasePage cmsbasePage;

        private ResourceManager _ResourceManager;
        private Language _PageLanguage;
        private int _targetID;
        private string CategoryName;
        public HondaCA.Entity.Model.Trim objTrim;
        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }

        public Language PageLanguage
        {
            get { return _PageLanguage; }
            set { _PageLanguage = value; }
        }

        public int TargetID
        {
            get { return _targetID; }
            set { _targetID = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            isStaging = false;
            if (this.Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                this.PageLanguage = cmsbasePage.PageLanguage;
                this.ResourceManager = cmsbasePage.ResourceManager;
                this.TargetID = cmsbasePage.TargetID;
                isStaging = cmsbasePage.IsStaging;
            }
            elementControl1.ElementName = elementName;
            if (!string.IsNullOrEmpty(elementName2)) elementControl2.ElementName = elementName2;
            if( !string.IsNullOrEmpty(elementName3)) elementControl3.ElementName = elementName3;
            LiteralDownloads.Text = ResourceManager.GetString("Downloads");
            pnlSingle.Visible = isVisible;
        }

        public void LoadData(GenericLink genlinksModel, GenericLink genlinksCategory=null, GenericLink genlinksModelShared = null)
        {
            LoadPageData();
            string res = string.Empty;

            if (genlinksModel != null && genlinksModel.Items != null)
            {
                if (genlinksModel.ContentElement.Display)
                {
                    foreach (GenericLinkItem item in genlinksModel.Items)
                    {
                        res += string.Format(@"<li{0}><h3>{1}</h3><a href=""{2}"" target=""_blank"">{3}</a></li>", "", item.Title, item.File ?? item.LinkURL, item.LinkText);
                    }
                }
            }


            if (genlinksCategory != null && genlinksCategory.Items != null)
            {
                if (genlinksCategory.ContentElement.Display)
                {
                    int count = 1;
                    foreach (GenericLinkItem item in genlinksCategory.Items)
                    {
                        res += string.Format(@"<li{0}><h3>{1}</h3><a href=""{2}"" target=""_blank"">{3}</a></li>", (count == genlinksCategory.Items.Count ? " class='last'" : ""), item.Title, item.File ?? item.LinkURL, item.LinkText);
                        count++;
                    }
                    //res = @"</li>" + res;
                }
            }

            if (genlinksModelShared != null && genlinksModelShared.Items != null)
            {
                if (genlinksModelShared.ContentElement.Display)
                {
                    int count = 1;
                    foreach (GenericLinkItem item in genlinksModelShared.Items)
                    {
                        res += string.Format(@"<li{0}><h3>{1}</h3><a href=""{2}"" target=""_blank"">{3}</a></li>", (count == genlinksModelShared.Items.Count ? " class='last'" : ""), item.Title, item.File ?? item.LinkURL, item.LinkText);
                        count++;
                    }
                    //res = @"</li>" + res;
                }
            }

            if (!string.IsNullOrEmpty(CategoryName) && objTrim != null)
            {
                LinkButton lnkDownload = new LinkButton();

                lnkDownload.Text = ResourceManager.GetString("downloadpdf");
                lnkDownload.Click += lnk_Download_OnClick;
                plDownloadLinks.Controls.Add(new LiteralControl(string.Format(@"<li><h3  class=""first"">{0}</h3><h3 class=""first"">{1}</h3>", objTrim.TrimName, ResourceManager.GetString("txtFeature_Spec_Sheet_Title"))));
                plDownloadLinks.Controls.Add(lnkDownload);
                plDownloadLinks.Controls.Add(new LiteralControl("</li>"));
            }
            plDownloadLinks.Controls.Add(new LiteralControl(res));
        }

        private void lnk_Download_OnClick(object sender, EventArgs e)
        {
            // TODO generates PDF and push to client side


            string url = string.Format("/Models/SellSheetExporter.aspx?TrimID={0}&ProductID={1}&Lang={2}&TargetID={3}&BaseUrl={4}", objTrim.TrimID, cmsbasePage.ContentPage.ProductID, PageLanguage.GetCultureStringValue(), TargetID, Request.QueryString["BaseUrl"]);



            string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
            string headerAndFooterHtmlUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/footer.htm";



            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter;
            pdfConverter.PdfDocumentOptions.TopMargin = 30;
            pdfConverter.PdfDocumentOptions.LeftMargin = 30;
            pdfConverter.PdfDocumentOptions.RightMargin = 30;
            pdfConverter.PdfDocumentOptions.BottomMargin = 30;
            pdfConverter.PdfFooterOptions.FooterHeight = 0;
            pdfConverter.PdfFooterOptions.DrawFooterLine = false;


            Navantis.Honda.CMS.Core.Common.HtmlToPDF htmltopdf = new Navantis.Honda.CMS.Core.Common.HtmlToPDF();
            htmltopdf.Converter = pdfConverter;

            htmltopdf.WritePDF(this.Response, url, string.Format("{0}sellsheet{1}.pdf", objTrim.TrimUrl, PageLanguage.GetCultureStringValue().Substring(0, 2)));
            Response.Write(url);
            //Response.Redirect(url);


        }

        private void LoadPageData()
        {
            if (this.Parent.Page is CmsContentBasePage)
            {
                if (cmsbasePage == null)
                {
                    cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                    this.PageLanguage = cmsbasePage.PageLanguage;
                    this.ResourceManager = cmsbasePage.ResourceManager;
                    this.TargetID = cmsbasePage.TargetID;
                    HondaATV.Service.Model.PETrimService trimService = new HondaATV.Service.Model.PETrimService();
                    HondaModelService modelservice = new HondaModelService();

                    if (cmsbasePage.ContentPage.ProductID != null && cmsbasePage.ContentPage.TrimID != null)
                        objTrim = trimService.GetTrimByTrimID(this.TargetID, (int)cmsbasePage.ContentPage.ProductID, (int)cmsbasePage.ContentPage.TrimID, PageLanguage.GetCultureStringValue());
                    if (objTrim != null)
                        CategoryName = modelservice.GetModelFamilyByFamilyURL(PageLanguage, objTrim.ModelFamilyURL).FirstOrDefault().ModelFamilyName.TrimEnd('S');
                }


            }
        }
    }
}