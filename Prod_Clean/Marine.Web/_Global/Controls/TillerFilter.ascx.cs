﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Resources;
using Navantis.Honda.CMS.Demo;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using HondaCA.Service.Model;

namespace Marine.Web._Global.Controls
{
    public partial class TillerFilter : System.Web.UI.UserControl
    {
        CmsContentBasePage cmsbasePage;
        private ResourceManager _ResourceManager;
        public string value1 = "tiller_type";
        public string value2 = "displacement";
        public string value3 = "tilling_width";
        public decimal minEngineSize { get; set; }
        public decimal maxEngineSize { get; set; }

        public decimal minTillingWidth { get; set; }
        public decimal maxTillingWidth { get; set; }

        public ResourceManager ResourceManager
        {
            set { _ResourceManager = value; }
            get { return _ResourceManager; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Parent.Page is CmsContentBasePage)
            {
                cmsbasePage = (CmsContentBasePage)this.Parent.Page;
                this.ResourceManager = cmsbasePage.ResourceManager;
            }
            TrimSpecService trimSpectService = new TrimSpecService();
            EntityCollection<TrimSpect> trimSpect = trimSpectService.GetTrimSpectRange(cmsbasePage.TargetID, cmsbasePage.PageLanguage, value2);
            minEngineSize = trimSpect[0].minValue;
            maxEngineSize = trimSpect[0].maxValue;

            trimSpect = trimSpectService.GetTrimSpectRange(cmsbasePage.TargetID, cmsbasePage.PageLanguage, value3);
            minTillingWidth = trimSpect[0].minValue;
            maxTillingWidth = trimSpect[0].maxValue;
        }
    }
}