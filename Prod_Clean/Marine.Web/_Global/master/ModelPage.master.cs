﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Resources;
using Marine.Service.Extensions;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using Navantis.Honda.CMS.Client;
using System.Configuration;
using HondaATV.Service.Model;
using HondaCA.Entity.Model;
using System.Text;
using HondaDealer.Entity;
using Marine;
using System.IO;
using HondaCA.Common;
using Marine.Web.Code;
using HondaCA.WebUtils.CMS;

namespace Marine.Web._Global.master
{
    public partial class ModelPage : System.Web.UI.MasterPage
    {
        protected CmsContentBasePage basePage;
        public bool IsStaging;
        int modelID;
        HondaCA.Common.Language PageLanguage;
        private int reviewCount = 1;

        public HondaCA.Entity.Model.HondaModel objBaseModel;
        public HondaCA.Entity.Model.Trim objTrim;
        public HondaCA.Entity.Model.ModelCategory oCategory;
        public HondaCA.Entity.Model.BaseModelFamily objBaseModelFamily;
        
        string ModelFamilyUrl = string.Empty;
        string CategoryUrl = string.Empty;
        string TrimExportKey = string.Empty;
        string TrimUrl = string.Empty;
        protected decimal DiscountAmount;
        protected decimal DiscountedPrice;
        protected decimal MSRP;
        protected string strFeatureIdentifierStart;
        protected string strFeatureIdentifierEnd;
        protected bool ImplementNewFeatureCode = false;
        protected string DefaultImageDisclaimer = string.Empty;
        //string QuebecCookieValue = string.Empty;

        // Section Visibility
        protected bool FeaturedAccessoriesVisible = true;

        protected void Page_Load(object sender, EventArgs e)
        {            
            IsStaging = Convert.ToBoolean(ConfigurationManager.AppSettings["IsStaging"].ToString());
            basePage = (CmsContentBasePage) this.Parent.Page;
            PageLanguage = basePage.PageLanguage;

            ModelFamilyUrl = Request.QueryString["ModelFamilyUrl"] ?? string.Empty;
            CategoryUrl = Request.QueryString["CategoryUrl"] ?? string.Empty;
            TrimUrl = Request.QueryString["TrimUrl"] ?? string.Empty;

            HondaCA.Service.Model.HondaModelService ms = new HondaCA.Service.Model.HondaModelService();

            
            strFeatureIdentifierStart = ConfigurationManager.AppSettings["StrFeatureIdentifierStart"] ?? "<";
            strFeatureIdentifierEnd = ConfigurationManager.AppSettings["StrFeatureIdentifierEnd"] ?? ">";
            string tmp = ConfigurationManager.AppSettings["ImplementNewFeatureCode"] ?? "false";
            ImplementNewFeatureCode = (tmp.ToLower() == "true") ? true : false;            
            
            //Model
            if (basePage.ContentPage != null)
            {
                modelID = (int)basePage.ContentPage.ProductID;
                objBaseModel = ms.GetModelByModelID(basePage.TargetID, PageLanguage, modelID);
            }
            else
            {
                objBaseModel = ms.GetModelByUrl(2, PageLanguage, ModelFamilyUrl);
            }
            if (objBaseModel == null) Response.Redirect("~/error/404");// throw new Exception("Url Not Found");

            //Trim
            PETrimService trimService = new PETrimService();            
            objTrim = trimService.GetTrimByTrimID(basePage.TargetID, (int)basePage.ContentPage.ProductID, (int)basePage.ContentPage.TrimID, basePage.PageLanguage.GetCultureStringValue());//.GetTrimByTrimExportKey(BaseModelUrl, basePage.TargetID, TrimExportKey, PageLanguage.ToString());
            objTrim = objTrim.GetPriceAdjustedTrim(basePage.TargetID, basePage.PageLanguage);

            if (objTrim == null) Response.Redirect("~/error/404");
            //commented code to remove discount 
            BaseMaster basemaster_for_region = new BaseMaster();

            if (!basePage.IsQuebecPricing)
              MSRP = objTrim.MSRP;
            else
              MSRP = objTrim.MSRP + objTrim.FreightPDI;

            DiscountAmount = PETrimService.getDiscountAmount(MSRP, objTrim.PriceDiscountAmount, objTrim.PriceDiscountPercentage);
            DiscountedPrice = PETrimService.getDiscountedPrice(MSRP, objTrim.PriceDiscountAmount, objTrim.PriceDiscountPercentage);
            
            DefaultImageDisclaimer = GetColourDisclaimer(); //Add image descliamer code

            TrimExportKey = objTrim.TrimExportKey;

           // Legal Text

            if (!basePage.IsQuebecPricing)
            {
              litlegal.Text = basePage.ResourceManager.GetString("MSRPLegalTooltip");
            }
            else
            {
              litlegal.Text = basePage.ResourceManager.GetString("MSRPLegalTooltipQuebec");
            }

            
            //litlegal.Text = "text";
              //basePage.ResourceManager.GetString("MSRPLegalTooltip");

            //Model Family
            try
            {
                HondaCA.Entity.EntityCollection <BaseModelFamily> objModelFamilies = ms.GetModelFamilyByFamilyURL(PageLanguage,ModelFamilyUrl);
                if (objModelFamilies != null && objModelFamilies.Count > 0) 
                {
                    foreach (BaseModelFamily bmf in objModelFamilies) 
                    {
                        if (bmf.ModelFamilyURL == ModelFamilyUrl)
                        {
                            objBaseModelFamily = bmf;
                            break;
                        }
                     }
                    
                }
                
            }
            catch (Exception ex)
            {
                
                throw ex;
            }


			Colour oColour = null;

			if (trimService.IsComingSoonWithColor(objTrim, oColour))
			{
				phComingSoon.Visible = true;
			}
			else
			{
				if (DiscountAmount == 0)
				{
					phHasNoDiscount.Visible = true;
				}
				else
				{
					phHasDiscount.Visible = true;
				}
			}

            //Category
            //HondaCA.Entity.EntityCollection<ModelCategory> oCategories = ms.GetCategoryByTrimExportKey(basePage.TargetID, TrimExportKey, PageLanguage);
            HondaCA.Entity.EntityCollection<ModelCategory> oCategories = ms.GetModelCategory(PageLanguage, basePage.TargetID);
            try
            {
                oCategory = oCategories.FirstOrDefault(x => x.CategoryID == objTrim.BaseModelCategoryID);
            }
            catch (Exception)
            {
            }
            
            //CMS Product Image
            DoCMSProductImage();
            
            //Tout for enlarge
            DoTout();

            //Model Details Top Bar
            DoModelDetailsTopBar(ms);

            //Model Details Bottom Bar
            DoModelDetailsBottomBar();
                        
            //New Product
            DoNewProduct();


            //Top selling accessories
            DoTopSellingAccessories();

            //Compare Button
            DoComapareButton();

            //Downloads
            DoDownloads();

            //you may also consider
            DoRelatedProducts();

            //Sign up For Updates
            DoSignUpForUpdates();

            //Tabbed Menu
            DoTabbedMenu();

            //Review
            //DoReviews();
            
            //Parts & Services
            //DoPartsAndServices();
            
            //Safety
            DoSafety();

            this.DataBind();

            //sign up form submit
            if (this.IsPostBack)
            {
                bool responseofForm = false;

                if (Request.Form["formName"] == "SignUpForm") 
                {
                    responseofForm = GenericFormRequest("GenericContentPointer_CP3");
                    if (!responseofForm)
                    {
                        Response.StatusCode = 500;
                    }
                }
            }
        }

        private string GetColourDisclaimer()
        {
            PETrimService trimService = new PETrimService();
            HondaCA.Entity.EntityCollection<Colour> oColours = trimService.GetcolorsByTrimID(basePage.TargetID, objTrim.TrimID, this.PageLanguage);

            return oColours[0].TrimExteriorColorDisclaimer;
        }

       private void DoCMSProductImage()
        {
            Tout tout = CMSHelper.getCmsElementFromCmsPage<Tout>(basePage.ContentPage, "GenericContent_Tout");
            if(tout!=null && tout.Items != null && tout.Items.Count > 0  && tout.ContentElement.Display )
            {
                if (!string.IsNullOrEmpty(tout.Items[0].PathReference))
                {
                    cmsProductImage.Src = tout.Items[0].PathReference;
                    cmsProductImage.Visible = true;

                }
            }
            
        }

        //Enlarge
        protected void DoTout()
        {
            ImageTout.ImageUrl = string.Format("{0}?Crop=auto&maxwidth={1}&maxheight={2}", CommonFunctions.BuildModelImageUrl(objTrim), 740, 740);
            /*
            Tout tout = (basePage.ContentPage.Elements.ContainsKey("DefaultOverview_Tout") ? basePage.ContentPage.Elements["DefaultOverview_Tout"].DeserializeElementObject() : null) as Tout;
            try
            {
                ImageTout.ImageUrl = tout.Items[0].PathReference;
            }
            catch (Exception)
            {
            }
             */
        }

        //Model Details TOp bar
        protected void DoModelDetailsTopBar(HondaCA.Service.Model.HondaModelService ms)
        {
            LiteralModelName.Text = objTrim.TrimName;
            try
            {
                LiteralSeriesName.Text = string.Format("<h2>{0}</h2>", oCategory.CategoryName);
                LiteralTooltipSeriesHeader.Text = oCategory.CategoryName;
                LiteralTooltipSeriesDescription.Text = Marine.Web.Code.ModelHelper.ProcessModelCategoryTooltip(oCategory.CategoryDescription);
            }
            catch (Exception)
            {
                LiteralSeriesName.Visible = false;
                ucToolTipSeries.Visible = false;
            }
            LiteralMSRP1.Text = String.Format(basePage.ResourceManager.GetString("valMsrp6"), "$", MSRP);
         }

        //Model Details Bottom Bar
        protected void DoModelDetailsBottomBar() 
        {
            HyperLinkSeeAll.Text = string.Format(basePage.ResourceManager.GetString("txtSeeAllPE"), CommonFunctions.ucWords(oCategory.CategoryName.ToLower()));
            HyperLinkSeeAll.NavigateUrl = basePage.ResourceManager.GetString("urlSeeAllOutBoardMotor") + "#" + oCategory.ExportKey;
        }
                        
        //New Product
        protected void DoNewProduct()
        {
            FreeFormHtml ffh = (basePage.ContentPage.Elements.ContainsKey("GenericContent_FFH5") ? basePage.ContentPage.Elements["GenericContent_FFH5"].DeserializeElementObject() : null) as FreeFormHtml;
            if (ffh != null)
            {
                if (ffh.ContentElement.Display && ffh.Html != null)
                {
                    if (!string.IsNullOrEmpty(ffh.Html))
                    {
                        Literal lit = new Literal();
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat(@"<div class=""fc pf badge new-product"">
                                                <img src=""/_Global/img/content/products/icon_sun_invert.png"" alt="""" class=""fl pf"" />
                                                <p class=""fl heading"">{0}</p>
                                          </div>", ffh.Html);
                        lit.Text = sb.ToString();
                        PlaceHolderNew.Controls.Add(lit);
                    }
                }
            }
        }

    
        protected void DoTopSellingAccessories()
        {
            if (!basePage.getElementVisiblity("PartsAndAccessories_TA"))
            {
                this.FeaturedAccessoriesVisible = false;
                return;
            }

            try
            {
                TopAccessories topAccessories = CMSHelper.getCmsElementFromCmsPage<TopAccessories>(basePage.ContentPage, "PartsAndAccessories_TA");
                LiteralPartsAndAccessories.Text = getTopAccessories(topAccessories);//


                if (topAccessories == null || topAccessories.Items == null || topAccessories.Items.Count <= 0)
                    LiteralTopSellingAccessories.Text = string.Empty;
                else
                    LiteralTopSellingAccessories.Text = basePage.ResourceManager.GetString("txtTopSellingAccessories");
            }
            catch (Exception ex) { //Response.Write(ex.Message); 
                throw; }
        }

        //Get Top Accessories
        protected string getTopAccessories(TopAccessories topAccessories)
        {
            StringBuilder sb = new StringBuilder();

            int taCount = 0;
            int i = 0;

            if (topAccessories != null && topAccessories.Items != null)
            {
                taCount = topAccessories.Items.Count;
                if (topAccessories.ContentElement.Display)
                {
                    foreach (TopAccessoriesItem vlItem in topAccessories.Items)
                    {
                        sb.Append(string.Format("{0}", vlItem.AccessoryItemID));
                        if ((taCount - 1) != i) { sb.Append(", "); };
                        i++;
                    }
                }
            }
            return getAccesories(sb.ToString(), taCount);
        }

        //Compare Button
        protected void DoComapareButton()
        {
            HyperLinkCompare.Text = string.Format(@"<span>{0}</span>",basePage.ResourceManager.GetString("Compare"));
            HyperLinkCompare.NavigateUrl = string.Format("{0}/{1}-{2}", basePage.ResourceManager.GetString("CompareRootFolder"), objTrim.TrimExportKey, objTrim.ModelYearYear);
        }

        //Get Top Accessories
        protected string getTopAccessories(string sElementName)
        {
            StringBuilder sb = new StringBuilder();
            TopAccessories topAccessories = (basePage.ContentPage.Elements.ContainsKey(sElementName) ? basePage.ContentPage.Elements[sElementName].DeserializeElementObject() : null) as TopAccessories;
            int taCount = 0;
            int i = 0;

            if (topAccessories != null && topAccessories.Items != null)
            {
                taCount = topAccessories.Items.Count;
                if (topAccessories.ContentElement.Display)
                {
                    foreach (TopAccessoriesItem vlItem in topAccessories.Items)
                    {
                        sb.Append(string.Format("{0}", vlItem.AccessoryItemID));
                        if ((taCount - 1) != i) { sb.Append(", "); };
                        i++;
                    }
                }
            }
            return getAccesories(sb.ToString(), taCount);
        }

        //Get Accessories
        protected string getAccesories(string AccessoryItemIDList, int taCount)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbToolTip = new StringBuilder();

            string imgPath = string.Empty;

            string sBuildItTargetFolder = HondaCA.Common.Global.BuildItTargetFolder;
            string sBuildItPath = String.Format(basePage.ResourceManager.GetString("pathBuildItToolImagePath"), sBuildItTargetFolder);

            HondaAccessories.Service.PartsAndAccessoriesService accService = new HondaAccessories.Service.PartsAndAccessoriesService();
            HondaAccessories.Entity.EntityCollection<HondaAccessories.Entity.Accessory> accessories = accService.GetTopAccessories(( ConfigurationManager.AppSettings["ProductLineCode"] ?? "R").ToLower(), AccessoryItemIDList, this.PageLanguage.ToString());

            int i = 0;

            if (taCount != 0)
            {
                //sb.Append(string.Format(basePage.ResourceManager.GetString("txtTopSellingAccessories"), objBaseModel.ModelName));    //PartsAndAccessoriesTopAccessoriesTitle
                //"Les 5 accessoires les plus populaires pour la Fit "
            }
            sb.Append(@"<!--start--><a class=""btn-up btn-disabled prev""></a>");
            sb.Append(@"<div class=""accessory-carousel"" >");
            sb.Append(@"  <div class=""items"">");

            foreach (HondaAccessories.Entity.Accessory acc in accessories)
            {

                imgPath = string.Empty;
                if (i == 0)
                {
                    sb.Append(@"    <div>");
                    sb.Append(@"      <div class=""first accessory"">");
                }
                else if (i % 2 == 0)
                {
                    sb.Append(@"      </div>");
                    sb.Append(@"    </div>");
                    sb.Append(@"    <div>");
                    sb.Append(@"      <div class=""first accessory"">");
                }
                else
                {
                    sb.Append(@"      </div>");
                    //sb.Append(@"    <div>");
                    sb.Append(@"      <div class=""accessory"">");
                }

                imgPath = string.Format(@"{0}/{1}/{2}", ConfigurationManager.AppSettings["AssetPathAccessories"], 9999, acc.ItemVehicleID.ToString() + "_thumbnail.jpg");

                if (!File.Exists(Server.MapPath(imgPath))) imgPath = string.Format(@"{0}/{1}_{2}", ConfigurationManager.AppSettings["AssetPathAccessories"], basePage.PageLanguage.GetCultureStringValue().Substring(0,2), "NA_thumbnail.png");
                
                string AccPrice = string.Empty;
                string AccPriceWithMSRP = string.Empty;
                try
                {
                    if (acc.ListPrice > 0)
                    {
                        AccPrice = string.Format("{0:C2}", acc.ListPrice);
                        AccPriceWithMSRP = string.Format("{0:C2} <span>{1}</span>", acc.ListPrice, basePage.ResourceManager.GetString("MSRP"));
                    }
                }
                catch (Exception)
                {
                    AccPrice = string.Empty;
                }
                string AccItemName = CommonFunctions.StringToHtmlEntities(acc.Name);
                string AccItemDesc = CommonFunctions.StringToHtmlEntities(acc.DescriptionBody);

                sb.Append(string.Format(@"<a class=""image btn-tooltips"" 
                                            property=""act:tooltip"" 
                                            data-position-my=""right center"" 
                                            data-position-at=""left center"" 
                                            data-position-offset=""-20px 0""
                                            data-tooltip-class=""tooltip-left"" 
                                            data-tooltip-content-target=""#product-accessories-item{0}""><img src=""{1}?Crop=auto&maxWidth=105&maxHeight=70"" alt="""" /></a>", i.ToString(), imgPath));

                sb.Append(string.Format(@"<div class=""description"">
                                        <span class=""product-item"">{0}</span>
                                        <div class=""price"">{1}</div>
                                    </div>", AccItemName, AccPriceWithMSRP));

                sbToolTip.Append(string.Format(@"<div id=""product-accessories-item{0}"" class=""dn"">
	                                                <div class=""image""><img src=""{1}?maxwidth=250&maxHeight=199"" alt="""" /></div>
	                                                <div class=""tooltip_heading"">{2}</div>
	                                                <p>{5}</p><br />
                                                  <p>{4}</p>
                                                    </div>", i.ToString(), imgPath, AccItemName, AccPrice, basePage.ResourceManager.GetString("txtSeeDealerForPricing"), AccItemDesc));
                //<div class=""tooltip_price""><span>{4}</span></div>
                i++;
            }
            if (taCount != 0 && (accessories.Count > 0 ))
            {
                sb.Append("  </div>");
                sb.Append("  </div>");
            }
            sb.Append("  </div>");
            sb.Append("</div><!-- close-->");
            if (accessories.Count > 2) 
                sb.Append(@"<a class=""btn-down next""></a>");

            LiteralToolTip.Text = sbToolTip.ToString();

            return sb.ToString();
        }

        //Downloads
        protected void DoDownloads()
        {
            DownloadButton1.linkTitle = basePage.ResourceManager.GetString("Downloads");
            IElementContent element = basePage.ContentPage.Elements["GenericContentPointer_CP"].DeserializeElementObject();
            DownloadButton1.isVisible = element.ContentElement.Display ? true : IsStaging ? true : false;

            if (DownloadButton1.isVisible)
            {
                GenericLink genlinks = new GenericLink();
                //DownloadButton1.LoadData(basePage.getContentPointerElement("GenericContentPointer_CP", genlinks) as GenericLink);
                //DownloadButton1.LoadData(basePage.getContentPointerElement("GenericContentPointer_CP", genlinks) as GenericLink, basePage.ContentPage.Elements["GenericContent_Link"].DeserializeElementObject() as GenericLink);
                DownloadButton1.LoadData(CMSHelper.getCmsElementFromCmsPage<GenericLink>(basePage.ContentPage, "GenericContent_Link"), CMSHelper.getElementThroughContentPointerFromCmsPage<GenericLink>(basePage.ContentPage, "GenericContentPointer_CP") , CMSHelper.getElementThroughContentPointerFromCmsPage<GenericLink>(basePage.ContentPage, "GenericContentPointer_CP2"));
            }
        }

        //You May Also Consider    
        protected void  DoRelatedProducts() 
        {
            IElementContent elementReview = basePage.ContentPage.Elements["RelatedProduct"].DeserializeElementObject();
            plRelatedProduct.Visible = elementReview.ContentElement.Display ? true : IsStaging ? true : false;

            if (plRelatedProduct.Visible)
            {
                plRelatedProduct.Controls.Add(new LiteralControl(getRelatedProduct("RelatedProduct")));
            }
            LiteralYouMayConsider.Text = basePage.ResourceManager.GetString("txtYouMayAlsoConsider");
        }

        //Get Related Products
        protected string getRelatedProduct(string sElementName)
        {
            RelatedProduct relPro = basePage.ContentPage.Elements[sElementName].DeserializeElementObject() as RelatedProduct;
            if (relPro != null && relPro.Items != null)
            {
                if (relPro.ContentElement.Display)
                {
                    if (relPro.Items.Count <= 0 && !this.basePage.IsStaging)
                    {
                        YouMayConsiderDisclaimer.Visible = false;
                        LiYouMayConsider.Visible = false;
                        return string.Empty;
                    }
                    string res = string.Empty;
                    foreach (RelatedProductItem item in relPro.Items)
                    {
                        PETrimService trimService = new PETrimService();
                        Trim trim = trimService.GetTrimByTrimID(basePage.TargetID, item.ModelID, item.TrimID, basePage.PageLanguage.ToString());
                        trim = trim.GetPriceAdjustedTrim(basePage.TargetID, PageLanguage);
                        try
                        {
                          decimal MSRP;                         
                          if (!basePage.IsQuebecPricing)
                            MSRP = objTrim.MSRP;
                          else
                            MSRP = objTrim.MSRP + objTrim.FreightPDI;

                         
                            //MSRP = trim.MSRP + trim.FreightPDI;
                            HondaCA.Service.Model.HondaModelService ms = new HondaCA.Service.Model.HondaModelService();

                            HondaCA.Entity.EntityCollection<HondaCA.Entity.Model.ModelCategory> oCategories = ms.GetCategoryByTrimExportKey(basePage.TargetID, trim.TrimExportKey, PageLanguage);

                            HondaCA.Entity.Model.ModelCategory oCategory = null;

                            if (oCategories != null && oCategories.Count > 0)
                            {
                                oCategory = oCategories[0];
                            }

                            res += string.Format(@"<a href=""{0}""><img src=""{1}?maxwidth=125"" alt="""" /><span class=""product_name"">{2}</span>"
                                                    , (oCategory == null ? string.Empty : oCategory.CategoryUrl + "/") + trim.TrimUrl
                                                    ,CommonFunctions.BuildModelImageUrl(trim)
                                                    ,trim.TrimName);
                                                    //    <span class=""product_price"">{3:C0}<span>{4}</span></span></a>"
                                                    
                                                    //,trim.MSRP
                                                    //,basePage.ResourceManager.GetString("MSRP"));

                            decimal DiscountAmount = HondaPE.Service.Model.PETrimService.getDiscountAmount(MSRP, trim.PriceDiscountAmount, trim.PriceDiscountPercentage);
                            decimal DiscountedPrice = PETrimService.getDiscountedPrice(MSRP, trim.PriceDiscountAmount, trim.PriceDiscountPercentage);
                            
                            
                            StringBuilder sb = new StringBuilder();
							                Colour oColour = null;

							                if (trimService.IsComingSoonWithColor(trim, oColour))
							                {
								                sb.Append(string.Format(@"<div class=""fc final price coming-soon""><strong>{0}</strong><span>{1}</span></div>", basePage.ResourceManager.GetString("txtComingSoon"), basePage.ResourceManager.GetString("MSRP")));//Line goes with special discount
							                }
							                else
							                {							    
								                if (DiscountAmount != 0)
								                {
									                sb.Append(string.Format(@"<div class=""fc original price""><del><span class=""strike""></span><strong>{0}</strong></del><span>{1}</span></div>", string.Format(basePage.ResourceManager.GetString("valMSRP4"), MSRP), basePage.ResourceManager.GetString("MSRP*"))); //Line goes with special discount
									                sb.Append(string.Format(@"<div class=""fc special price""><strong>{0}</strong><span>{1}</span></div>", string.Format(basePage.ResourceManager.GetString("valMSRP4"), DiscountAmount), basePage.ResourceManager.GetString("Discount"))); //special discount
									                sb.Append(string.Format(@"<div class=""fc final price""><strong>{0}</strong><span>{1}</span></div>", string.Format(basePage.ResourceManager.GetString("valMSRP4"), DiscountedPrice), basePage.ResourceManager.GetString("YourPrice*"))); //your price after discount
								                }
								                else //Without discount
									                sb.AppendFormat(@"<span class=""product_price"">{0:C0}<span>{1}</span></span>", MSRP,
									                                basePage.ResourceManager.GetString("MSRP*")); //Price without any kind of discount
							                }
                        	sb.Append("</a>");
                            res += sb.ToString();
                        }
                        catch (Exception)
                        {
                            YouMayConsiderDisclaimer.Visible = false;
                        }
                    }
                    return res;
                }
            }
            return "";
        }

        //Handle Form Request
        protected bool GenericFormRequest(string sElementName)
        {
            bool response = false;
            if (this.IsPostBack)
            {
                //submitGenericForm
                string whichButton = this.Request.Form["formName"];
                if (!string.IsNullOrEmpty(whichButton))
                {
                    //add a generic form response
                    if (basePage.ContentPage.Elements.ContainsKey(sElementName))
                    {
                        //GenericForm genericForm = this.ContentPage.Elements[sElementName].DeserializeElementObject() as GenericForm;
                        GenericForm genericForm = getGenericFormFromContentPointer(sElementName);
                        if (genericForm != null && genericForm.Items != null)
                        {
                            //get the value from the Request form
                            foreach (GenericFormField field in genericForm.Items)
                            {
                                field.Value = this.Request.Form[field.Name];

                                if (field.Name == "DateSubmitted")
                                {
                                    field.Value = DateTime.Now.ToString();
                                }

                                switch (field.Name)
                                {
                                    case "ModelName":
                                        field.Value = objTrim.TrimName;
                                        break;
                                    case "ModelCategory":
                                        field.Value = oCategory.CategoryName;
                                        break;
                                    case "ModelYear":
                                        field.Value = objTrim.ModelYearYear.ToString();
                                        break;
                                }

                            }
                            response = GenericFormHelper.CreateGenericFormResponse(genericForm.ContentElement.ID, genericForm.Fields);
                        }
                    }
                }
            }
            return response;
        }

        //Get Form Content
        public GenericForm getGenericFormFromContentPointer(string sElementName)
        {
            Navantis.Honda.CMS.Client.Elements.GenericForm ffh = null;
            ffh = basePage.getContentPointerElement(sElementName, ffh) as Navantis.Honda.CMS.Client.Elements.GenericForm;
            return ffh;
        }

        //Sign up for updates
        protected void DoSignUpForUpdates()
        {
            LiteralSignUpButtonText.Text = string.Format(@"<span class=""{0}"">{1}</span>", PageLanguage == Language.French? "updates multiline":"updates", basePage.ResourceManager.GetString("txtSignUpForUpdates"));
            LiteralSignUpFormHeader.Text = basePage.ResourceManager.GetString("txtSignUpForUpdates");
            LiteralSignUpFormBlurb.Text = basePage.ResourceManager.GetString("txtSignUpFormBlurb");
            LiteralPleaseEnter.Text = basePage.ResourceManager.GetString("GenericFormEnterMessage");
            LiteralRequiredFields.Text = basePage.ResourceManager.GetString("txtRequiredFields");
            LiteralFirstName.Text = basePage.ResourceManager.GetString("txtFirstName");

            LiteralAddress.Text = basePage.ResourceManager.GetString("txtAddress");
            LiteralProvince.Text = basePage.ResourceManager.GetString("txtProvince");
            LiteralPleaseSelect.Text = basePage.ResourceManager.GetString("txtPleaseSelect");
            LiteralLastName.Text = basePage.ResourceManager.GetString("txtLastName");
            LiteralAddressContd.Text = basePage.ResourceManager.GetString("txtAddressContd");
            LiteralPostalCode.Text = basePage.ResourceManager.GetString("txtPostalCode");
            LiteralEmailAddress.Text = basePage.ResourceManager.GetString("txtEmailAddress");
            LiteralCity.Text = basePage.ResourceManager.GetString("txtCity");
            LiteralSubmitFormFooter.Text = basePage.ResourceManager.GetString("txtSubmitFormFooter");

            //LinkButtonSubmitform.Text = string.Format("<span>{0}</span>", basePage.ResourceManager.GetString("txtSubmit"));
            //LinkButtonSubmitform.CssClass = "btn primary submit";

            HondaDealer.Service.RegionService rs = new HondaDealer.Service.RegionService();
            EntityCollection<Region> regionList = new EntityCollection<Region>();
            regionList = rs.GetAllRegions((HondaDealer.Common.Language)this.PageLanguage);

            StringBuilder sb1 = new StringBuilder();
            foreach (Region region in regionList)
            {
                sb1.Append(string.Format(@"<option value=""{0}"">{1}</option>", region.Code, region.Name));
            }
            litProvince.Text = sb1.ToString();
        }
            
        //Tabbed Menu
        protected void DoTabbedMenu() 
        {
            //Navantis.Honda.CMS.Client.Elements.Menu menu = basePage.ContentPage.Elements["GenericContent_Menu"].DeserializeElementObject() as Navantis.Honda.CMS.Client.Elements.Menu;
            //TabMenu1.LoadData(menu, this.IsStaging,true);
            try
            {   ModelNav modelnav = CMSHelper.getCmsElementFromCmsPage<ModelNav>(basePage.ContentPage, "ModelNav");
                TabMenu1.LoadData(modelnav, this.IsStaging, true);
            }
            catch { }

        }

  

        //Parts And Services
        //protected void DoPartsAndServices()
        //{
        //    LiteralPartsAndServiceHeader.Text = basePage.ResourceManager.GetString("txtPartsAndService");
        //    LiteralPartsAndService.Text = string.Format(basePage.ResourceManager.GetString("blurbPartsAndServices"), CommonFunctions.ucWords(oCategory.CategoryName.ToLower()));
        //    HyperLinkPartsAndServices.Text = string.Format("<span>{0}</span>", basePage.ResourceManager.GetString("LearnMore"));
        //}

        //Safety
        protected void DoSafety()
        {
            /*
            FreeFormHtml ffh = null;
            ffh = (basePage.ContentPage.Elements.ContainsKey("GenericContentPointer_CP4") ? basePage.getContentPointerElement("GenericContentPointer_CP4", ffh) as Navantis.Honda.CMS.Client.Elements.FreeFormHtml : null) as FreeFormHtml;
            if (ffh != null) if (ffh.ContentElement.Display) LiteralSafety.Text = ffh.Html;
             */
        }
    }
}