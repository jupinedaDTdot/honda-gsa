﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using HondaCA.Common;
//using Navantis.Honda.CMS.Demo;
//using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils;

namespace Marine.Web._Global.master
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}
//{
//    public partial class Master : BaseMaster
//    {
//        public string cultureString;
//        //public CmsContentBasePage basePage;
//        public basePage;
//        HtmlMeta meta;
//        HtmlMeta metaPageSetting;
//        const string cssFilePath = @"<link rel=""Stylesheet"" href=""{0}"" type=""text/css"" media=""screen"" charset=""utf-8"" />";
//        const string jsFilePath = @"<script src=""{0}"" type=""text/javascript""></script>";
//        string body_Attributes_class = string.Empty;
//        protected string ScriptLocale = string.Empty;


//        private void prepareLanguageBase()
//        {
//            if (this.PageLanguage == Language.French)
//            {
//                this.prepareFrench();
//            }
//            else
//            {
//                this.prepareEnglish();
//            }


//            //if (body.Attributes["class"] != "staging page page-parts-service-overview-product" || body.Attributes["class"] != "page-landing")
//            if (String.IsNullOrEmpty(body.Attributes["class"]))
//            {
//                body.Attributes.Add("class", body_Attributes_class);
//            }
//            else
//            {
//                body.Attributes["class"] += " " + body_Attributes_class;
//            }
//        }

//        private void prepareEnglish()
//        {
//            if (basePage.IsStaging)
//                body_Attributes_class = "staging page";
//            else
//                body_Attributes_class = "page";

//            ScriptLocale = "marine";

//            litFonts.Text = String.Format(jsFilePath, "/_Global/js/fonts/en/fonts.js");
//        }

//        private void prepareFrench()
//        {
//            if (basePage.IsStaging)
//                body_Attributes_class = "french page";
//            else
//                body_Attributes_class = "staging french page";

//            Literal LiteralFrStyle = new Literal();
//            LiteralFrStyle.Text = @"<link rel=""stylesheet"" href=""/_Global/css/common/fr/layout.css"" type=""text/css"" media=""screen"" charset=""utf-8"" /> ";
//            head.Controls.Add(LiteralFrStyle);
//            litFonts.Text = String.Format(jsFilePath, "/_Global/js/fonts/fr/fonts.js");
//            ScriptLocale = "fr-marine";
//        }

//        protected override void Page_Load(object sender, EventArgs e)
//        {
//            basePage = (CmsContentBasePage)this.Parent.Page;
//            this.PageLanguage = basePage.PageLanguage;
//            cultureString = PageLanguage.GetCultureStringValue();
//            meta = new HtmlMeta();            
//            meta.Attributes.Add("http-equiv","Content-Language");
//            meta.Attributes.Add("content", cultureString);
//            Header.Controls.Add(meta);//"<meta http-equiv="Content-Language" content="PUT LANGUAGE CULTURE SETTING HERE">"

//            PageSetting pageSetting;
            
//            if (basePage != null)
//            {
//                pageSetting = basePage.PageSetting;
//                if (pageSetting != null)
//                {
//                    //Set the page title
//                    this.Page.Title = String.IsNullOrEmpty(this.Page.Title) ? (HttpUtility.HtmlEncode(pageSetting.Title ?? "Honda Marine")) : this.Page.Title;
//                    //Header.Controls.AddAt(0, new LiteralControl(string.Format("<title>{0}</title>", HttpUtility.HtmlEncode(pageSetting.Title ?? "Honda Canada"))));

//                    PageSettingUtils.AddPageMetaData(Header, pageSetting);

//                    //survey
//                    if (string.IsNullOrEmpty(pageSetting.SurveyID) == false && pageSetting.SurveyStartDate != null && pageSetting.SurveyEnforced != null)
//                    {
//                        if ((DateTime.Today >= pageSetting.SurveyStartDate) && (DateTime.Today <= ( pageSetting.SurveyEndDate ?? pageSetting.SurveyStartDate) ))
//                        {

//                            litSurvey.Text = string.Format(@"<input type=""hidden"" name=""{0}"" value=""{1}"" />", "SurveyID", pageSetting.SurveyID);
//                            //litSurvey.Text += string.Format(@"<input type=""hidden"" name=""{0}"" value=""{1}"" />", "SurveyStartDate", pageSetting.SurveyStartDate.ToString());
//                            //litSurvey.Text += string.Format(@"<input type=""hidden"" name=""{0}"" value=""{1}"" />", "SurveyEndDate", pageSetting.SurveyEndDate.ToString());
//                            litSurvey.Text += string.Format(@"<input type=""hidden"" name=""{0}"" value=""{1}"" />", "SurveyEnforced", pageSetting.SurveyEnforced);
//                        }
//                    }
//                    //Page title                                        

//                    //js and css file insert to header ( from pagesetting ) 
//                    if (pageSetting.AssociatedFilePaths != null )
//                    {
                  
//                        foreach (string filepath in pageSetting.AssociatedFilePaths) 
//                        {
//                            if (filepath.EndsWith(".css"))
//                                Header.Controls.Add(new LiteralControl(string.Format(cssFilePath, filepath)));
//                            else if (filepath.EndsWith(".js"))
//                                phJSAssociatedFile.Controls.Add(new LiteralControl(string.Format(jsFilePath, filepath)));
//                        }
//                    }

//                }

//            }

//            this.prepareLanguageBase();
            

//            base.Page_Load(sender, e);
//        }

        
//    }
//}


