﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true" CodeBehind="AllModels.aspx.cs" Inherits="Marine.Web.Models.AllModels" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagName="ElementControl" TagPrefix="uc" %>
<%@ Register Src="~/_Global/Controls/AddToCompare.ascx" TagName="AddToCompare" TagPrefix="uc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/product-listing.css" rel="stylesheet" type="text/css" />
    <link href="/_Global/css/marine/sections/model-selector.css" rel="stylesheet" type="text/css" />
    <link href="/_Global/css/marine/sections/product-listing.css" rel="stylesheet" type="text/css" />    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
<div id="feature_area">
    <div class="container">
        <div>
            <uc:ElementControl ID="ElementControl1" runat="server" ElementName="GenericContent_FFH" />
            <asp:PlaceHolder ID="plhTopFFH" runat="server"></asp:PlaceHolder>
        </div>
        <div>
            <uc:ElementControl ID="ElementControl2" runat="server" ElementName="GenericContent_GC1" />
            <asp:PlaceHolder ID="plhTopGC" runat="server"></asp:PlaceHolder>
        </div>
    </div>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="cap_top_default pf"></div>
		<div class="container top-container">
			<div class="content_container overview">
                <div class="fc content_section first_section">
					<h1 class="fl"><%=ResourceManager.GetString("strHondaModelAllModels")%></h1>
				</div>
				<div class="content_section primary_section">
                    <asp:PlaceHolder ID="plhModelDetail" runat="server"></asp:PlaceHolder>
                </div>
            </div>
		</div>
        <div class="container bottom-container">
            <div class="content_container">
                <uc:AddToCompare runat="server" id="CompareWidget" />
            </div>
        </div>
	<div class="wrapper-grey cap_bottom_default cap_bottom_grey pf"></div>
	<div class="clr"></div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BottomCap" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
<script src="/_Global/js/sections/product-listing.js"></script>
</asp:Content>
