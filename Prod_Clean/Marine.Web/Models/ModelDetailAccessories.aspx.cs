﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using HondaATV.Service.Model;
using HondaCA.Common;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using System.IO;
using System.Configuration;
using System.Text;
using HondaAccessories.Service;
using HondaAccessories.Entity;
using Marine.Web.Code;
using ExpertPdf.HtmlToPdf;
using ExpertPdf.HtmlToPdf.PdfDocument;

namespace Marine.Web.Models
{
    //public partial class ModelDetailAccessories : CmsContentBasePage
    //{
    //    public string sGroup;
    //    int modelID;
    //    public HondaCA.Entity.Model.HondaModel objBaseModel;
    //    public HondaCA.Entity.Model.Trim objTrim;
    //    //public HondaMC.Entity.Series oSeries;

    //    public HondaATV.Entity.Series oSeries;

    //    string CategoryUrl = string.Empty;
    //    string TrimUrl = string.Empty;

    //    private int SelectedTrimID;
    //    private string SelectedModelCategoryUrl;
    //    private string SelectedProvinceCode;
    //    private string ImageFolder = string.Empty;

    //    string BaseModelUrl = string.Empty;
    //    string TrimExportKey = string.Empty;

    //    bool IsPrintSpecs = false;

    //    protected override void OnInit(EventArgs e)
    //    {
    //        string BaseUrl = string.Empty;
    //        try
    //        {
    //            BaseUrl = Request.QueryString["BaseUrl"];
    //            if (Request.QueryString["Print"] == "1")
    //            {
    //                IsPrintSpecs = true;
    //            }
    //        }
    //        catch (Exception)
    //        {
    //            Response.Redirect("~/error/404");
    //        }
    //        //this.MainUrl = BaseUrl;
    //        this.MainUrl = BaseUrl;

    //        base.OnInit(e);

    //        //this is to add sectio specific class to the body tag
    //        System.Web.UI.HtmlControls.HtmlControl ctrlBody = (System.Web.UI.HtmlControls.HtmlControl)this.Page.Master.Master.Master.Master.FindControl("body");
    //        if (ctrlBody != null) ctrlBody.Attributes["class"] = ctrlBody.Attributes["class"] == null ? "page-product-details" : " page-product-details";
    //    }

    //    protected void Page_Load(object sender, EventArgs e)
    //    {
    //        CategoryUrl = Request.QueryString["CategoryUrl"];
    //        TrimUrl = Request.QueryString["TrimUrl"];

    //        this.PageLanguage = getLanguageForCMSPage();
    //        base.InitializeCulture();

    //        HondaCA.Service.Model.HondaModelService ms = new HondaCA.Service.Model.HondaModelService();
    //        if (this.ContentPage != null)
    //        {
    //            modelID = (int)this.ContentPage.ProductID;
    //            objBaseModel = ms.GetModelByModelID(this.TargetID, PageLanguage, modelID);
    //        }
    //        if (objBaseModel == null) Response.Redirect("~/error/404");// throw new Exception("Url Not Found");

    //        PETrimService trimService = new PETrimService();
    //        objTrim = trimService.GetTrimByTrimID(this.TargetID, (int)this.ContentPage.ProductID, (int)this.ContentPage.TrimID, this.PageLanguage.GetCultureStringValue());


    //        if (!this.IsPostBack)
    //        {
    //            //PopulateProvinceDropDown();
    //        }

    //        //Initialize selected trimid, selscted modelfamilyurl, selected provincecode and imagefolder
    //        //if (!string.IsNullOrEmpty(DropDownListProvince.SelectedValue))
    //        //{
    //        //    SelectedProvinceCode = DropDownListProvince.SelectedValue;
    //        //    PanelAccessoriesResult.Visible = true;
    //        //}

    //        SelectedModelCategoryUrl = CategoryUrl;
    //        SelectedTrimID = objTrim.TrimID;
    //        ImageFolder = string.Format(@"{0}/{1}/", ConfigurationManager.AppSettings["AssetPathAccessories"], objTrim.ModelYearYear.ToString());

    //        if (PanelAccessoriesResult.Visible)
    //        {
    //            //LiteralImage.Text = string.Empty;

    //            if (objTrim != null)
    //            {
    //                //  LiteralModelName.Text = oTrim.TrimName;
    //                // LiteralMsrpValue.Text = string.Format(ResourceManager.GetString("valMsrp"), "$", oTrim.MSRP);

    //                if (IsPrintSpecs)
    //                {
    //                    PrintSpecs();
    //                }
    //                else
    //                {
    //                    LiteralAccessoryTitle.Text = string.Format(ResourceManager.GetString("txtTrimAccessories"), objTrim.TrimName);
    //                    ShowHideResultsPanel();
    //                }

    //                //string IamgePath = string.Format(string.Format(ResourceManager.GetString("pathBuildItToolImagePath"), HondaCA.Common.Global.BuildItTargetFolder) + @"{0}/{1}/{2}",
    //                //        oTrim.BaseModelExportKey,
    //                //        oTrim.TrimExportKey,
    //                //        ResourceManager.GetString("imgDefaultThumbImage"));
    //                //string IamgePath = HondaPE.Web.Code.CommonFunctions.BuildModelImageUrl(oTrim);

    //                //if (File.Exists(Server.MapPath(IamgePath)))
    //                // {
    //                //    LiteralImage.Text = string.Format(@"<img src=""{0}?Crop=auto&Width={1}&Height={2}"" alt"""" />", IamgePath, 250, 199);
    //                // }

    //                //HondaCA.Service.Model.HondaModelService ms = new HondaModelService();
    //                //EntityCollection<ModelCategory> oCategories = ms.GetModelCategoryByModelFamilyUrl(this.TargetID, ModelFamilyUrl, PageLanguage);
    //                //ModelCategory oCateory = null;

    //                //try
    //                //{
    //                //    oCateory = oCategories[0];
    //                //    LiteralSeriesName.Text = oCateory.CategoryName;
    //                //}
    //                //catch (Exception)
    //                //{
    //                //}
    //            }
    //        }
    //        HyperLinkDownloadPdf.Text = ResourceManager.GetString("downloadpdf");

    //        if (Request.RawUrl.ToLower() == this.MainUrl.ToLower())
    //        {
    //            HyperLinkDownloadPdf.NavigateUrl = this.MainUrl + "/print";
    //        }
    //        else
    //        {
    //            HyperLinkDownloadPdf.NavigateUrl = this.MainUrl + Request.RawUrl.Substring(Request.RawUrl.LastIndexOf("/")) + "/print";
    //        }
    //    }

    //    //Province Dropdown
    //    //protected void PopulateProvinceDropDown()
    //    //{
    //    //    HondaCA.Service.Common.RegionService rs = new HondaCA.Service.Common.RegionService();
    //    //    EntityCollection<HondaCA.Entity.Common.Region> oRegions = rs.GetAllRegions(PageLanguage);

    //    //    if (oRegions != null)
    //    //    {
    //    //        try
    //    //        {
    //    //            if (oRegions.Count > 0)
    //    //            {
    //    //                foreach (HondaCA.Entity.Common.Region oRegion in oRegions)
    //    //                {
    //    //                    DropDownListProvince.Items.Add(new ListItem(oRegion.Name, oRegion.Code));
    //    //                }
    //    //                DropDownListProvince.Items.Insert(0, new ListItem(ResourceManager.GetString("txtSelectOne"), ""));
    //    //            }
    //    //        }
    //    //        catch (Exception)
    //    //        {
    //    //        }
    //    //    }
    //    //}

    //    //Show results
    //    protected void ShowHideResultsPanel()
    //    {
    //        //if (!string.IsNullOrEmpty(SelectedModelFamilyUrl) && SelectedTrimID > 0 && !string.IsNullOrEmpty(SelectedProvinceCode))
    //        if (!string.IsNullOrEmpty(SelectedModelCategoryUrl) && SelectedTrimID > 0)
    //        {
    //            //ImageFolder += "/" + objTrim.ModelYearYear.ToString();

    //            PartsAndAccessoriesService paService = new PartsAndAccessoriesService();
    //            PartsAndAccessory oPartsAndAccessories = paService.GetPartsAndAccessories(SelectedTrimID, this.TargetID, (HondaAccessories.Common.Language)PageLanguage, System.Configuration.ConfigurationManager.AppSettings["ProductLineCode"], ImageFolder);

    //            //Check if PAs are null
    //            if (oPartsAndAccessories != null)
    //            {
    //                //check if PAs have items
    //                if (oPartsAndAccessories.Category.Count > 0)
    //                {
    //                    StringBuilder sbAccItems = new StringBuilder();
    //                    StringBuilder sbAccToolTips = new StringBuilder();
    //                    int categoryIndex = 0;

    //                    foreach (Category oCategory in oPartsAndAccessories.Category)
    //                    {
    //                        IList<Accessory> oAccessories = oCategory.Accessories;

    //                        if (oAccessories.Count <= 0) continue;

    //                        sbAccItems.AppendFormat(@"<div class=""flex_bar"">
    //                                                    <span>
    //                                                        <a href=""javascript:void(0);""
    //                                                            property=""act:collapsible""
    //                                                            data-collapsible-target=""#accessories_list{1}""
    //                                                            data-collapsible-active=""{2}"">{0}</a>
    //                                                    </span>
    //                                                  </div>"
    //                                                , oCategory.CategoryName
    //                                                , oCategory.CategoryID
    //                                                , categoryIndex == 0 ? "true" : "false");
    //                        sbAccItems.AppendFormat(@"<div id=""accessories_list{0}"" class=""sub_content"">", oCategory.CategoryID);
    //                        sbAccItems.Append(@"<ul>");

    //                        if (oAccessories != null)
    //                        {
    //                            if (oAccessories.Count > 0)
    //                            {
    //                                int cnt = 0;

    //                                foreach (Accessory oAccessory in oAccessories)
    //                                {
    //                                    string AccPrice = string.Empty;
    //                                    try
    //                                    {
    //                                        if (oAccessory.Price > 0)
    //                                        {
    //                                            AccPrice = string.Format("{0:C2}", oAccessory.Price);
    //                                        }
    //                                    }
    //                                    catch (Exception)
    //                                    {
    //                                        AccPrice = string.Empty;
    //                                    }
    //                                    string AccName = CommonFunctions.StringToHtmlEntities(oAccessory.Name);
    //                                    string AccDesc = CommonFunctions.StringToHtmlEntities(oAccessory.DescriptionBody);

    //                                    sbAccItems.AppendFormat(@"<li class=""fc"">
    //                                                            <a class=""fl btn-tooltips""
				//	                                                property=""act:tooltip""
    //                                                                data-position-my=""left center""
    //                                                                data-position-at=""right center""
    //                                                                data-position-offset=""20px 0""
				//	                                                data-position-collision=""fit""
				//	                                                data-tooltip-class=""tooltip-right""
				//	                                                data-tooltip-content-target=""#accessories-item{2}"">{0}{3}
    //                                                            </a>
				//	                                            <span class=""fr"">{1}</span>
				//                                              </li>"
    //                                        , AccName
    //                                        , string.Empty // AccPrice -- updated to remove accessory price.
    //                                        , oAccessory.ItemID
    //                                        , string.IsNullOrWhiteSpace(oAccessory.ItemNumber) ? string.Empty : string.Format(@"<br /><q class=""part-number"">{0}: {1}</q>", ResourceManager.GetString("txtPartNumberLabel"), oAccessory.ItemNumber)
    //                                        );

    //                                    string image = string.Empty;

    //                                    if (!string.IsNullOrEmpty(oAccessory.Assets.Thumbnail))
    //                                    {
    //                                        image = string.Format("{0}/{1}", ImageFolder, oAccessory.Assets.Thumbnail);

    //                                        if (File.Exists(Server.MapPath(image)))
    //                                        {
    //                                            image = string.Format(@"<div class=""image""><img src=""{0}/{1}?Crop=auto&maxWidth=250&maxHeight=199"", alt="""" /></div>", ImageFolder, oAccessory.Assets.Thumbnail);
    //                                        }
    //                                    }

    //                                    if (string.IsNullOrEmpty(image))
    //                                    {
    //                                      image = string.Format(@"<div class=""image""><img src=""{0}?Crop=auto&maxWidth=250&maxHeight=199"", alt"""" /></div>", string.Format(@"{0}/{1}_{2}",
    //                                                ConfigurationManager.AppSettings["AssetPathAccessories"],
    //                                                PageLanguage.GetCultureStringValue().Substring(0, 2),
    //                                                "NA_thumbnail.png"));
    //                                    }

    //                                    sbAccToolTips.AppendFormat(@"<div id=""accessories-item{0}"" class=""dn"">
	   //                                                             {1}
	   //                                                             <div class=""tooltip_heading""><strong>{2}</strong></div>
	   //                                                             <p>{5}</p>
    //                                                              <p>{4}</p>
    //                                                            </div>", oAccessory.ItemID, image, AccName, string.Empty /*AccPrice -- updated to remove accessory pricing */, ResourceManager.GetString("txtSeeDealerForPricing"), AccDesc);
    //                                    // <div class=""tooltip_price""><span>{4}</span></div>

    //                                    cnt++;
    //                                }
    //                            }
    //                        }
    //                        sbAccItems.Append("</ul>");
    //                        sbAccItems.Append("</div>");

    //                        categoryIndex++;
    //                    }
    //                    LiteralAccessoryItems.Text = sbAccItems.ToString();
    //                    LiteralAccessoryToolTips.Text = sbAccToolTips.ToString();
    //                }
    //            }
    //            PanelAccessoriesResult.Visible = true;
    //        }
    //        else
    //        {
    //            PanelAccessoriesResult.Visible = false;
    //        }
    //    }
    //    protected void PrintSpecs()
    //    {
    //        string url = string.Format(Request.RawUrl);
    //        url = string.Format("/Models/PrintModelAccessories.aspx?TrimID={0}&ProductID={1}&Lang={2}&TargetID={3}&BaseUrl={4}", objTrim.TrimID, this.ContentPage.ProductID, PageLanguage.GetCultureStringValue(), TargetID, Request.QueryString["BaseUrl"]);

    //        PdfConverter pdfConverter = new PdfConverter();

    //        string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
    //        //string headerAndFooterHtmlUrl = thisPageURL.Substring(0, thisPageURL.LastIndexOf('/')) + "/footer.htm";

    //        //enable footer
    //        pdfConverter.PdfDocumentOptions.ShowFooter = true;
    //        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
    //        pdfConverter.PdfDocumentOptions.TopMargin = 20;
    //        // set the footer height in points
    //        pdfConverter.PdfFooterOptions.FooterHeight = 67;
    //        pdfConverter.PdfFooterOptions.DrawFooterLine = false;
    //        //pdfConverter.PdfFooterOptions.HtmlToPdfArea = new HtmlToPdfArea(headerAndFooterHtmlUrl);

    //        Navantis.Honda.CMS.Core.Common.HtmlToPDF htmltopdf = new Navantis.Honda.CMS.Core.Common.HtmlToPDF();
    //        htmltopdf.Converter = pdfConverter;

    //        htmltopdf.WritePDF(this.Response, url, string.Format("{0}_accessories_{1}.pdf", objTrim.TrimUrl, PageLanguage.GetCultureStringValue().Substring(0, 2)));
    //        Response.Write(url);
    //    }
    //}
}