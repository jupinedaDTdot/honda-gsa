﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Navantis.Honda.CMS.Demo;
using HondaCA.Common;
using Navantis.Honda.CMS.Client.Elements;
using System.Text;
using HondaCA.WebUtils.CMS;
using HondaPE.Service.Model;
//using HondaATV.Service.Model;

namespace Marine.Web.Models
{
    public partial class ModelDetailOverview : CmsContentBasePage
    {
        public string sGroup, ModelUrl;
        int trimID;        
        public HondaCA.Entity.Model.Trim objTrim;
                 
        
        protected override void OnInit(EventArgs e)
        {
            string requestNoQueryString = Request.RawUrl.Split('?').FirstOrDefault();
            this.MainUrl = requestNoQueryString;// removed BaseUrl for passing complte url to cms, used from PageSetting:SourceUrls
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Cultures.GetCultureStringValue(this.PageLanguage);
            RecentlyVisited.TargetID = this.TargetID;
            
            Marine.Web.Models.ModelDetailOverview _Test = new Marine.Web.Models.ModelDetailOverview();
            
            ModelUrl = Request.QueryString["BaseModelUrl"] != null ? Request.QueryString["BaseModelUrl"] : string.Empty;

            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            //this is to add sectio specific class to the body tag
            //Generate body classes
            string BodyClass = "page-product-details";

                     
            System.Web.UI.HtmlControls.HtmlControl ctrlBody = (System.Web.UI.HtmlControls.HtmlControl)this.Page.Master.Master.Master.Master.FindControl("body");
            if (ctrlBody != null) ctrlBody.Attributes["class"] = ctrlBody.Attributes["class"] == null ? BodyClass : " " + BodyClass;

            PETrimService trimService = new PETrimService();
            
            
            if (this.ContentPage != null)
            {
                trimID = (int)this.ContentPage.TrimID;
                objTrim = trimService.GetTrimByTrimID(this.TargetID, (int)this.ContentPage.ProductID, trimID, this.PageLanguage.GetCultureStringValue());
            }
            if (objTrim == null) Response.Redirect("~/error/404");// throw new Exception("Url Not Found");

            doPricingandConfiguration();
            FreeFormHtml modelOverview = (this.ContentPage.Elements.ContainsKey("GenericContent_FFH") ? this.ContentPage.Elements["GenericContent_FFH"].DeserializeElementObject() : null) as FreeFormHtml;
            if (modelOverview != null) if (modelOverview.ContentElement.Display) LiteralMiddle1.Text = modelOverview.Html;

            //FreeFormHtml modelOverview2 = (this.ContentPage.Elements.ContainsKey("GenericContent_FFH2") ? this.ContentPage.Elements["GenericContent_FFH2"].DeserializeElementObject() : null) as FreeFormHtml;
            //if (modelOverview2 != null) if (modelOverview2.ContentElement.Display) LiteralMiddle2.Text = modelOverview2.Html;

            FreeFormHtml modelOverviewReadMoreSummary = CMSHelper.getElementThroughContentPointerFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContentPointer_CP4");//this.ContentPage.Elements["GenericContent_FFH2"].DeserializeElementObject() : null) as FreeFormHtml; //no longer used             
            if (modelOverviewReadMoreSummary != null) if (modelOverviewReadMoreSummary.ContentElement.Display) litReadMoreSummary.Text = modelOverviewReadMoreSummary.Html;

            FreeFormHtml modelOverviewReadMoreDetail = CMSHelper.getElementThroughContentPointerFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContentPointer_CP5");
            if (modelOverviewReadMoreDetail != null) if (modelOverviewReadMoreDetail.ContentElement.Display) litReadMoreDetail.Text = modelOverviewReadMoreDetail.Html;

            if (!string.IsNullOrEmpty(litReadMoreDetail.Text) && !string.IsNullOrEmpty(litReadMoreSummary.Text))
                litReadMoreSummary.Text += string.Format(@" <p class=""why-buy-link-more""> <a property=""{0}""  href=""{1}"">{2}</a></p>", "act:more-text", "#advantage-expanded", ResourceManager.GetString("txtReadMore")); //mb30 : new class 

           // ContentPointer DisclimerContentcp = CMSHelper.getElementThroughContentPointerFromCmsPage<ContentPointer>(this.ContentPage, "GenericContentPointer_CP7");//use GenericContent_FFH4 on ModelShared
            FreeFormHtml DisclimerContent = CMSHelper.getElementThroughContentPointerFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContentPointer_CP7");
            if (DisclimerContent != null) if (DisclimerContent.ContentElement.Display) litDisclimerContent.Text = DisclimerContent.Html;
            //if (DisclimerContentcp != null)
            //{
            //    if (DisclimerContentcp.ContentElement.Display)
            //    {
            //        FreeFormHtml DisclimerContent =
            //            CMSHelper.getElementThroughContentPointerFromCmsPage<FreeFormHtml>(this.ContentPage,
            //                                                                               "GenericContentPointer_CP7");
            //        if (DisclimerContent != null)
            //            if (DisclimerContent.ContentElement.Display)
            //                litDisclimerContent.Text = modelOverviewReadMoreDetail.Html;
            //    }
            //}

            GenericContent modelOverview3 = (this.ContentPage.Elements.ContainsKey("GenericContent_GC1") ? this.ContentPage.Elements["GenericContent_GC1"].DeserializeElementObject() : null) as GenericContent;
            if (modelOverview3 != null)  {
                if (modelOverview3.ContentElement.Display)
                {
                    StringBuilder sb = new StringBuilder();
                    try
                    {
                        GenericContentItem gci = modelOverview3.Items[0];
                        sb.Append(@"<div class=""fc feature_box"">");
                        sb.Append(@"    <div class=""fl feature_box_l"">");
                        sb.Append(@"        <div class=""watch_now_copy"">");
                        sb.AppendFormat(@"      <h2 class=""np small"">{0}</h2>",gci.Title);
                        sb.AppendFormat(@"      {0}",gci.Summary);
                        sb.Append(@"            <div class=""divider""></div>");
                        sb.AppendFormat(@"            <div class=""btn""><a href=""{0}"">{1}</a></div>",gci.MoreTextLinkURL,gci.MoreText);
                        sb.Append("             </div>");
                        sb.Append("         </div>");
                        sb.Append(@"    <div class=""fr feature_box_r"">");
                        sb.Append(@"        <div class=""btn_youtube"">");
                        sb.AppendFormat(@"      <a href=""{0}""><img src=""{1}"" alt="""" class=""pf"" /></a>",gci.ImageLinkURL, gci.Image);
                        sb.Append("         </div>");
                        sb.Append("     </div>");
                        sb.Append("</div>");
                    }
                    catch (Exception)
                    {
                    }
                    LiteralMiddle3.Text = sb.ToString();
                }


            }
        }

        private void doPricingandConfiguration()
        {

            HondaCA.Service.Model.TransmissionTypeService tts = new HondaCA.Service.Model.TransmissionTypeService();
            HondaCA.Entity.EntityCollection<HondaCA.Entity.Model.Transmission>  transmissions=  tts.GetTransmissionTypesByTrimID((int)this.ContentPage.TrimID , this.TargetID,
                                             this.PageLanguage.GetCultureStringValue());
            rptPricing.DataSource = transmissions;
            rptPricing.DataBind();
 
        }

        protected void rptPricing_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                HondaCA.Entity.Model.Transmission transmission = (HondaCA.Entity.Model.Transmission)e.Item.DataItem;
                Literal litYourPrice = e.Item.FindControl("litYourPrice") as Literal;
                Literal litDiscount = e.Item.FindControl("litDiscount") as Literal;
                Literal LitMSRP = e.Item.FindControl("LitMSRP") as Literal;                
                HtmlContainerControl divDiscount = e.Item.FindControl("divDiscount") as HtmlContainerControl;
                HtmlContainerControl divMSRP = e.Item.FindControl("divMSRP") as HtmlContainerControl;
                decimal MSRP = 0;

                if (transmission != null)
                {
                  if (!this.IsQuebecPricing)
                  {
                    MSRP = transmission.MSRP; 
                  }
                  else
                  {
                    MSRP = transmission.MSRP + transmission.FreightPDI;
                  }
                    LitMSRP.Text = MSRP > 0
                                       ? string.Format(ResourceManager.GetString("valMSRP4"), MSRP)
                                       : ResourceManager.GetString("txtComingSoon");
                }
                if (transmission.PriceDiscountAmount > 0)
                {
                  decimal discountAmount = PETrimService.getDiscountAmount(MSRP,
                                                                       transmission.PriceDiscountAmount,
                                                                       transmission.PriceDiscountPercentage);
                  decimal discountedPrice = PETrimService.getDiscountedPrice(MSRP,
                                                                             transmission.PriceDiscountAmount,
                                                                             transmission.PriceDiscountPercentage);
                    litYourPrice.Text = string.Format(ResourceManager.GetString("valMSRP3"), discountedPrice);
                    litDiscount.Text = string.Format(ResourceManager.GetString("valMSRP4"), discountAmount);
                    divDiscount.Visible = true;
                    divMSRP.Visible = true;                    
                }
                else
                {
                    litYourPrice.Text = string.Format(ResourceManager.GetString("valMSRP3"), MSRP);
                }

            }
        }
    }
}