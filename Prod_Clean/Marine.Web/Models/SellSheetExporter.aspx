﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SellSheetExporter.aspx.cs" Inherits="Marine.Web.Models.SellSheetExporter" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><%=ModelName%></title>
    <asp:Literal ID="litHeaderLinks" runat="server"></asp:Literal>
    <link href="/_Global/css/engine/sections/sellsheets-print.css" rel="stylesheet" type="text/css" />
</head>

<body class="sellsheets-print">
<div class="sellsheets-content-container">
	<div id="header">
		<div class="logo" runat="server" id="divLogo"><img width="137" height="40" src="<%=ResourceManager.GetString("LOGO_SellSheet")%>" alt="" /></div>
		<div class="h1">
        <%=TrimName %>
			<br/>
			</div>
		<div class="clr"></div>
		</div>
		<div id="hero">
			<ul class="fl vehicle-feaures">
				<asp:Literal ID="litFeatures" runat="server"></asp:Literal>
                
			</ul>
		<div class="fr vehicle-image">
			<asp:Image ID="imgModel" runat="server" BorderWidth="0" Width="250" Height="250" />
		</div>
		<div class="clr"></div>
	</div>
	<div class="clr"></div>
	<%--<div id="specs-bar"><div class="inner"> <%=  string.Format(ResourceManager.GetString("txtSpecifications"), TrimName)%></div></div>--%>
	<div id="content">
        <asp:Repeater ID="RptSpectCategory" runat="server" OnItemDataBound="RepeaterSpecs_ItemDataBound">
            <ItemTemplate>
                <div class="title-bar"><%#Eval("SpecCategoryDescription")%></div>
                <div class="fc specs-tables">                
                    <div class="fl left-table">
				        <table class="fnat">
					        <asp:Literal runat="server" ID="LitLeft"></asp:Literal>
				        </table>
			        </div>
			        <div class="fl right-table">
				        <table class="fnat">
					        <asp:Literal runat="server" ID="LitRight"></asp:Literal>
				        </table>
			        </div>                
                </div>
            </ItemTemplate>
        </asp:Repeater>
		<%--<div class="specs-tables">
        <asp:Repeater ID="RepeaterSpecs" runat="server" onitemdatabound="RepeaterSpecs_ItemDataBound">
        <ItemTemplate>
        <asp:Literal ID ="LiteralSpecItems" runat="server"></asp:Literal>
        </ItemTemplate>
        </asp:Repeater>			
			<div class="clr"></div>
		</div>--%>
        
        <div class="specs" style="margin-top: 10px;">
          <div class="content_section first_section">
             <div class="content_section" style="color: #424242;font-size: 11px;line-height: 16px;margin-bottom: 20px">
                <asp:Literal ID="litDisclaimer" runat="server"></asp:Literal> 
             </div>
             <div class="content_section" runat="server" Visible="False">
                <asp:Literal ID="litWarranty" runat="server"></asp:Literal> 
             </div>
          </div>
      </div>
	</div>
         
	<div id="dynamic-footer" class="footer">
		<%=TrimDisclaimer%>
	</div>
	<div id="static-footer" class="footer">
		<%=ResourceManager.GetString("txtLegal_Footer")%>
	</div>
</div>
</body>
</html>
