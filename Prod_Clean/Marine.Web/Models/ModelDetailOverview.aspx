﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/ModelPage.master" AutoEventWireup="true" CodeBehind="ModelDetailOverview.aspx.cs" Inherits="Marine.Web.Models.ModelDetailOverview" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<%@ Register Src="~/_Global/Controls/RecentlyVisited.ascx" TagName="RecentlyVisited" TagPrefix="uc1" %>
<%@ Register Src="~/_Global/Controls/FacebookLikeButton.ascx" TagPrefix="fb" TagName="LikeButton" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentArea" runat="server">
 <div class="overview">
    <div class="content_section first_section">
        <fb:LikeButton CssClass="fr" ID="FacebookLikeButton" runat="server" Visible="true"></fb:LikeButton>
        <div class="h1"><%=(objTrim!=null ? objTrim.TrimName :"")%> <span><%=ResourceManager.GetString("txtOverview")%></span></div>
        <div>
            <uc:ElementControl ID="ElementControl1" ElementName="GenericContent_FFH" runat="server" />   
            <asp:Literal ID="LiteralMiddle1" runat="server"></asp:Literal> 
        </div>
    </div>
    <div class="content_section configuration_section">
		    <h4><%=ResourceManager.GetString("ConfigurationandPricing")%></h4>
		    <dl>
		         <asp:Repeater runat="server" ID="rptPricing" OnItemDataBound="rptPricing_ItemDataBound">
		             <ItemTemplate>
		                <dd class="<%# (Container.ItemIndex == 0 ? " first" : string.Empty) %>">
				            <div class="top-cap"></div>
                            <div class="fc config-container">
				                <div class="fl product-info">
					                <div class="header"><%#Eval("ModelCode")%></div>
					                <div class="info"><%# Eval("TransmissionSpec")%></div>
				                </div>
				                <div class="fl product-price-container">                                    
					                <div class="fr large-price your-price">
						                <span class="price-heading"><%=ResourceManager.GetString("YourPrice*")%></span>
						                <span class="price"><asp:Literal runat="server" ID="litYourPrice"></asp:Literal></span>
                                    </div>                                    
									<div class="fr small-price discount-price" runat="server" id="divDiscount" visible="false">
										<span class="price-heading"><%=ResourceManager.GetString("Discount") %></span>
										<span class="price"><asp:Literal runat="server" ID="litDiscount"></asp:Literal></span>
									</div>
									<div class="fr small-price msrp" runat="server" id="divMSRP" visible="false">
										<span class="price-heading"><%=ResourceManager.GetString("MSRP*")%></span>
										<del><span class="strike"></span><span class="price"><asp:Literal runat="server" ID="LitMSRP"></asp:Literal></span></del>
									</div>                                    
					                <div class="clr"></div>
				                </div>
                            </div>
				            <div class="bottom-cap"></div>
			            </dd>
	                </ItemTemplate>
	            </asp:Repeater>
		    </dl>
            <div class="price-disclaimer">
                <span>
                   *<% = ( (this.IsQuebecPricing)  ? this.ResourceManager.GetString("MSRPLegalTooltipQuebec") : this.ResourceManager.GetString("MSRPLegalTooltip")  )%>
                </span>
            </div>

		    <p>
		        <div>
                    <uc:ElementControl ID="ElementControl5" ElementName="GenericContentPointer_CP7" runat="server" />   
                    <asp:Literal ID="litDisclimerContent" runat="server"></asp:Literal>             
                </div>
		    </p>
	 </div>
    <div class="content_section mb30">
<%--        <uc:ElementControl ID="ElementControl2" ElementName="GenericContent_FFH2" runat="server" />   
        <asp:Literal ID="LiteralMiddle2" runat="server"></asp:Literal> 
--%>
        <div class="grouped">
            <uc:ElementControl ID="ElementControl2" ElementName="GenericContentPointer_CP4" runat="server" />               
                <asp:Literal ID="litReadMoreSummary" runat="server"></asp:Literal>             
        </div>
        <div >
            <uc:ElementControl ID="ElementControl4" ElementName="GenericContentPointer_CP5" runat="server" />   
            <div id="advantage-expanded" >
                <asp:Literal ID="litReadMoreDetail" runat="server"></asp:Literal> 
                <p class="why-buy-link-less"><a property="act:less-text" href="#advantage-expanded">Collapse</a></p>
            </div>
        </div>

    </div>
    <div class="nb np nm content_section" runat="server" visible="false">
        <uc:ElementControl ID="ElementControl3" ElementName="GenericContent_GC1" runat="server" />  
        <asp:Literal ID="LiteralMiddle3" runat="server"></asp:Literal>  
    </div>
    <uc1:RecentlyVisited ID="RecentlyVisited" runat="server" />
</div>
</asp:Content>

