﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils.CMS;
using System.IO;
//using HondaPE.Service.Model;
//using HondaPE.Entity;
using HondaCA.Entity;
using HondaCA.Common;

using HondaATV.Entity;
using HondaATV.Service.Model;


namespace Marine.Web.Models
{
    public partial class PrintModelSpecs : CmsContentBasePage
    {
        public int TrimID;
        public int ProductID;
        public string Lang;
        protected int SpecCount = 0;
        protected string imgDimension = string.Empty;
        protected string imgPerformance = string.Empty;
                
        public HondaCA.Entity.Model.Trim objTrim;
        public string domain = "http://" + HondaCA.Common.Global.CurrentDomain.ToLower();

        protected override void OnInit(EventArgs e)
        {
            string BaseUrl = string.Empty;
            try
            {
                BaseUrl = Request.QueryString["BaseUrl"];                
            }
            catch (Exception)
            {
                Response.Redirect("~/error/404");
            }
            this.MainUrl = BaseUrl;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            TrimID = Convert.ToInt32(Request.QueryString["TrimID"]);
            ProductID = Convert.ToInt32(Request.QueryString["ProductID"]);
            Lang = Request.QueryString["Lang"];

            this.PageLanguage = Lang == "fr-CA" ? Language.French : Language.English;
            base.InitializeCulture();

            string path = "/_Global/css/";
            string resetCSS = readFile(Server.MapPath(path + "common/reset.css"));            
            string detailCSS = readFile(Server.MapPath(path + "marine/sections/product-details.css"));
            string printCSS = readFile(Server.MapPath(path + "marine/sections/specs-print.css"));
            string ieCSS = readFile(Server.MapPath(path + "common/ie.css"));

            litHeaderLink.Text = "<style type='text/css'>";
            litHeaderLink.Text += resetCSS;
            litHeaderLink.Text += detailCSS;            
            litHeaderLink.Text += printCSS;
            litHeaderLink.Text += "</style>";


            string imageURL = string.Empty;

            PETrimService trimService = new PETrimService();
            objTrim = trimService.GetTrimByTrimID(TargetID, ProductID, TrimID, PageLanguage.GetCultureStringValue());

            LiteralPgeHeading.Text = string.Format(string.Format("{0}", ResourceManager.GetString("txtSpecifications")), objTrim.TrimName);

            //if(string.IsNullOrEmpty(imageURL))
            imageURL = Marine.Web.Code.CommonFunctions.BuildModelImageUrl(objTrim);

            try
            {
                //Image is not display with domain so it is commented.
                //LiteralModelImage.Text = string.Format(@"<img src=""{0}{1}?maxwidth=177"" alt="""" />", domain, HondaATV.Web.Code.CommonFunctions.BuildModelImageUrl(objTrim));
                LiteralModelImage.Text = string.Format(@"<img src=""{0}?maxwidth=177"" alt="""" />", imageURL); 
                //ModelImage.ImageUrl = imageURL+"?Crop=auto&Width=177&Height=177"; string.Format(@"<img src=""{0}?Crop=auto&Width=177&Height=177"" alt="""" />", imageURL);
            }
            catch (Exception)
            {
            }

            HondaATV.Service.Model.SpecsCategoryService scs = new HondaATV.Service.Model.SpecsCategoryService();
            EntityCollection<HondaATV.Entity.SpecsCategory> SpecsCategories = scs.GetSpecsCategoryByTrimId(TrimID, this.PageLanguage, this.TargetID);

            if (SpecsCategories.Count() > 0)
            {
                RepeaterSpecs.DataSource = SpecsCategories;
                RepeaterSpecs.DataBind();
                litDisclaimer.Text = getDisclaimer();
                litWarranty.Text = getWarranty();
            }
            else
                RepeaterSpecs.Visible = false;            
        }

        private string getDisclaimer()
        {
            ContentPointer cp = CMSHelper.getCmsElementFromCmsPage<ContentPointer>(this.ContentPage, "GenericContentPointer_CP8");
            if (cp != null && cp.ContentElement.Display)
            {
                FreeFormHtml ffh = CMSHelper.getElementThroughContentPointerFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContentPointer_CP8");
                return CMSHelperExtention.getFreeFormHtml(ffh);
            }
            return string.Empty;
        }

        private string getWarranty()
        {
            //Warrant Section : should be shared : use ModelShared : GenericContent_FFH4
            ContentPointer cp = CMSHelper.getCmsElementFromCmsPage<ContentPointer>(this.ContentPage, "GenericContentPointer_CP6");
            if (cp != null && cp.ContentElement.Display)
            {
                FreeFormHtml ffh = CMSHelper.getElementThroughContentPointerFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContentPointer_CP6");
               return CMSHelperExtention.getFreeFormHtml(ffh);
            }
            return string.Empty;
        }

        private string readFile(string Path)
        {
            StringBuilder sb = new StringBuilder();
            using (StreamReader file = new StreamReader(Path))
            {
              string line = string.Empty;
              while ((line = file.ReadLine()) != null)
              {
                sb.Append(line.Replace("../img/", domain + "/_GLobal/img/"));
              }
            }
            return sb.ToString();
        }

        protected void RepeaterSpecs_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            Repeater RepCatItem = (Repeater)e.Item.FindControl("RepCatItem");

            HondaATV.Entity.SpecsCategory SpecCat = (HondaATV.Entity.SpecsCategory)e.Item.DataItem;
            HondaATV.Service.Model.SpecsService SpectService = new HondaATV.Service.Model.SpecsService();
            EntityCollection<HondaATV.Entity.Specs> SpectList = SpectService.GetSpecsByTrimIdandCategoryID(TrimID, SpecCat.SpecCategoryID, this.PageLanguage, this.TargetID);
            SpecCount = SpectList.Count;
            if (SpecCount > 0)
            {
                RepCatItem.DataSource = SpectList;
                RepCatItem.DataBind();
            }
            else
            {
                e.Item.Visible = false;
                RepCatItem.Visible = false;
            }
        }
    }
}