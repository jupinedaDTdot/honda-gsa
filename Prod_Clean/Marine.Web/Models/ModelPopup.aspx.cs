﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Marine.Service.Extensions;
using Marine.Web.Code;
using Navantis.Honda.CMS.Demo;
using HondaCA.Common;
using HondaCA.Entity.Model;
using HondaCA.Service.Model;
using HondaCA.Entity;
using HondaATV.Service.Model;
using System.Text;
using Marine.DataAccess.Model;

namespace Marine.Web.Models
{
    public partial class ModelPopup : CmsContentBasePage
    {
        public string features;
        public string categoryURL { get; set; }
        public string trimURL { get; set; }
        public string modelURL { get; set; }
        public string baseURL { get; set; }
        public string imageURL { get; set; }        
        public Trim trim;
        protected bool HasDiscount { get; set; }
        protected bool IsComingSoon { get; set; }        
        protected decimal DiscountAmount;
        protected decimal DiscountedPrice;
        protected decimal MSRP;

        protected override void OnInit(EventArgs e)
        {
            this.IsNotCMSPage = true;
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string lang = Request.QueryString["L"] != null ? Request.QueryString["L"] : "";
            lang = lang.Length > 2 ? lang.Substring(0, 2) : lang;
            if (lang.ToUpper() == Language.French.GetCultureStringValue().Substring(0, 2).ToUpper())
                this.PageLanguage = Language.French;
            else
                this.PageLanguage = Language.English;

            base.InitializeCulture();

                    
            trimURL = Request.QueryString["TrimURL"];
            categoryURL = Request.QueryString["CategoryURL"];
            decimal discount = 0;
            decimal.TryParse(Request.QueryString["Disc"] ?? "", out discount);

            MarineTrimMapper trimService = new MarineTrimMapper();
            EntityCollection<Trim> listTrims = new EntityCollection<Trim>();
            listTrims = trimService.SelectTrimWithBasePrice(categoryURL, this.TargetID, this.PageLanguage.GetCultureStringValue());
            trim = listTrims.FirstOrDefault(t => t.TrimUrl == trimURL);
            
            if (trim != null)
            {
              if (!this.IsQuebecPricing)
                MSRP = trim.MSRP;
              else
                MSRP = trim.MSRP + trim.FreightPDI;

              DiscountAmount = PETrimService.getDiscountAmount(MSRP, trim.PriceDiscountAmount, trim.PriceDiscountPercentage);
              DiscountedPrice = PETrimService.getDiscountedPrice(MSRP, trim.PriceDiscountAmount, trim.PriceDiscountPercentage);
              HasDiscount = DiscountAmount > 0;

              if (trim.Features != null)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (string item in trim.Features.FeatureList)
                    {
                        if (!string.IsNullOrEmpty(item))
                            sb.Append(string.Format(@"<li>{0}</li>", item));
                    }

                    features = sb.ToString();
                }

                baseURL = "/" + categoryURL + "/" + trim.TrimUrl;
                imageURL = CommonFunctions.BuildModelImageUrl(trim);

                if (trim.MSRP <= 0)
                {
                    phCurrentModel.Visible = false;
                    phComingSoon.Visible = true;
                }
            }
        }
    }
}