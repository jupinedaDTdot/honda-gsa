﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Marine.Service.Extensions;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils.CMS;
using Marine.Web.ContentPages;
using System.Text;
using HondaCA.Common;
using HondaCA.Entity;
using HondaCA.Entity.Model;
using HondaCA.Service.Model;
using Marine.Web.Code;
using Marine.Service.Model;
using HondaATV.Service.Model;
using System.Linq;
using Navantis.Honda.CMS.Demo;

namespace Marine.Web.Models
{
    public partial class AllModels : ContentBasePage
    {
      protected CmsContentBasePage basePage;
        protected override void OnInit(EventArgs e)
        {
            string url = Request.QueryString["Url"]?? string.Empty;
            if (!string.IsNullOrEmpty(url))
                Response.Redirect(url);

            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            this.PageLanguage = getLanguageForCMSPage();
            this.InitializeCulture();

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-product-listing");
                this.IsMasterBodyClassUpdate = true;
            }
           
            FreeFormHtml ffh = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH");
            plhTopFFH.Visible = ffh.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plhTopFFH.Visible)
            {
                plhTopFFH.Controls.Add(new LiteralControl(ffh.Html));
            }

            GenericContent gc = CMSHelper.getCmsElementFromCmsPage<GenericContent>(this.ContentPage, "GenericContent_GC1");
            plhTopGC.Visible = gc.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plhTopGC.Visible)
            {
                plhTopGC.Controls.Add(new LiteralControl(setGenericContent(gc)));
            }

            plhModelDetail.Controls.Add(new LiteralControl(setAllCategoryList()));

            CompareWidget.CompareUrl = ResourceManager.GetString("CompareRootFolder");
            CompareWidget.BaseModelFamilyUrl =ResourceManager.GetString("txtMarine");
        }

        private string setAllCategoryList(bool quebec=false)
        {
            StringBuilder sb = new StringBuilder();

            HondaModelService hmServices = new HondaModelService();
            EntityCollection<ModelCategory> modelCategories = hmServices.GetModelCategory(this.PageLanguage, this.TargetID);

            int cnt = 0;
            foreach (ModelCategory mCat in modelCategories)
            {                
                sb.AppendFormat(@"{0}<a name=""{1}""></a><div class=""h1"">{2}</div><p>{3}</p>", cnt == 0 ? "": "<div class='sub_section'>", mCat.ExportKey, mCat.CategoryName, mCat.CategoryDescription.Substring(0,mCat.CategoryDescription.IndexOf("<")));                
                sb.AppendFormat(setAllModelsList(mCat,quebec));
                sb.AppendFormat(@"{0}",cnt++ == 0 ?"" : "</div>");
            }

            return sb.ToString();
        }

        private string setAllModelsList(ModelCategory mCategory,bool quebec=false)
        {
            StringBuilder sb = new StringBuilder();
            MarineTrimService trimService = new MarineTrimService();
            IList<Trim> listTrims = new EntityCollection<Trim>();
            listTrims = trimService.GetTrimWithBasePrice(mCategory.CategoryUrl, this.TargetID, this.PageLanguage.GetCultureStringValue());
            listTrims = listTrims.Select(trim => trim.GetPriceAdjustedTrim(this.TargetID, this.PageLanguage)).ToList();
            
            if (listTrims.Count > 0)
            {
                sb.AppendFormat(@"<div class=""fc product"">");
                sb.AppendFormat(setAllTrims(listTrims, mCategory.CategoryUrl));
                sb.AppendFormat(@"</div>");

                sb.Append(string.Format(@"<div class=""fc price-disclaimer"">"));
                sb.Append(string.Format(@"<span class='fl'>*{0}</span>", this.IsQuebecPricing ? ResourceManager.GetString("MSRPLegalTooltipQuebec") : ResourceManager.GetString("MSRPLegalTooltip")));
                sb.Append(string.Format(@"</div>"));

            }

            return sb.ToString();
        }

        private string setAllTrims(IList<Trim> listTrims, string ModelCategoryUrl,bool quebec=false)
        {
            StringBuilder sb = new StringBuilder();
            int cnt = 0;
            foreach (Trim trim in listTrims)
            {
                string toolTipUrl = string.Empty;
                string modelURL = string.Empty;
                string divCssClass = cnt == 0 ? "fl first product-item" : "fl product-item";                
                toolTipUrl = "/tooltips/" + ModelCategoryUrl + "/" + trim.TrimUrl + "/" + this.PageLanguage.ToString()+"/"+trim.PriceDiscountAmount.ToString();
                modelURL = "/" + ModelCategoryUrl + "/" + trim.TrimUrl;
                sb.AppendFormat(@"<div class=""{0}""><div class=""product-image"" property=""act:popup"" data-position=""{7}"" data-popup-modal=""false"" data-popup-class=""{6}"" 
                                        data-popup-content-url=""{1}"" data-popup-handler=""rollover"" data-id=""{5}"">
                                        <a href=""{2}"" rel=""nofollow""><img src=""{3}?Crop=auto&Width=90&Height=125"" width=""90"" height=""125"" /></a></div>
                                        <h3 class=""price-heading""><a href=""{2}"" rel=""nofollow"">{4}</a></h3>"
                                        , divCssClass
                                        , toolTipUrl
                                        , modelURL
                                        , CommonFunctions.BuildModelImageUrl(trim)
                                        , trim.TrimName
                                        , trim.TrimExportKey + "-" + trim.ModelYearYear
                                        , cnt > 3 ? "toolbox-top-alt toolbox-right" : "toolbox-top-alt toolbox-left"
                                        , cnt > 3 ? "left top" : "right top");
                sb.AppendFormat(getPrice(trim));
                sb.AppendFormat(@"</div><div><!-- IE QUIRK --></div>");
                /*                 
                    <div class=""fc final price"">{5}</div></div>
                    <div><!-- IE QUIRK --></div>                 
                  , string.Format(ResourceManager.GetString("valMSRP2"), "$", trim.MSRP, ResourceManager.GetString("MSRP")));
                 * */
                if (cnt == 5)
                    cnt = 0;
                else
                    cnt++;
            }
            return sb.ToString();
        }

        private string getPrice(Trim trim)
        {
            StringBuilder sb = new StringBuilder();
            PETrimService ts = new PETrimService();

           
            if (ts.IsComingSoonWithoutColor(trim))
            {
                sb.AppendFormat(@"<div class=""fc final price""><strong>{0}</strong><span>{1}</span></div>"
                            , ResourceManager.GetString("txtComingSoonBr")
                            , ResourceManager.GetString("MSRP*")
                        );
            }
            else
            {
              decimal MSRP;
              if (!this.IsQuebecPricing)
              {
                MSRP = trim.MSRP; 
              }
              else
              {
                MSRP = trim.MSRP + trim.FreightPDI;
              }
              decimal discountAmount = trim.GetDiscount(this.IsQuebecPricing);
              decimal discountedPrice = trim.GetFinalPrice(this.IsQuebecPricing);
                if (trim.HasDiscount())
                {
                    sb.AppendFormat(@"<div class=""fc original price"">
								        <del><span class=""strike""></span><strong>{0}</strong></del>
								        <span>{1}</span>
							        </div>
                                    <div class=""fc special price"">
								        <strong>{2}</strong>
								        <span>{3}</span>
							        </div>
							        <div class=""fc final price"">
								        <strong>{4}</strong>
								        <span>{5}</span>
							        </div>",
                             string.Format(ResourceManager.GetString("valMSRP4"), MSRP),
                             ResourceManager.GetString("MSRP*"),
                             string.Format(ResourceManager.GetString("valMSRP4"), discountAmount),
                             ResourceManager.GetString("Discount"),
                             string.Format(ResourceManager.GetString("valMSRP4"), discountedPrice),
                             ResourceManager.GetString("YourPrice*"));


                        //<div class=""fl product-pricing"">
                        //    <div class=""large-price"">
                        //        <span class=""price-heading"">{0}</span>
                        //        <span class=""price"">{1}</span>
                        //    </div>
                            
                        //</div>", ResourceManager.GetString("ManufacturerDiscountedPrice"), string.Format(ResourceManager.GetString("valMSRP3"), DiscountedPrice), ResourceManager.GetString("MSRP"), string.Format(ResourceManager.GetString("valMSRP4"), trim.MSRP));

                }
                else
                {
                    /*
                    sb.AppendFormat(@"<div class=""fl product-pricing""><div class=""large-price""><span class=""price-heading"">{0}</span><span class=""price"">{1}</span></div></div>"
                                    , ResourceManager.GetString("MSRP")
                                    , string.Format(ResourceManager.GetString("valMSRP3"), trim.MSRP)
                                );
                     */                    
                    sb.AppendFormat(@"<div class=""fc final price"">{0}</div>"
                                    , string.Format(ResourceManager.GetString("valMSRP2"), "$", MSRP, ResourceManager.GetString("MSRP*"))
                                    );
                }
            }
            return sb.ToString();
        }

        private string setGenericContent(GenericContent gc)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sb1 = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            StringBuilder sb3 = new StringBuilder();
            if (gc != null && gc.Items != null)
            {
                sb.AppendFormat(@"<table class=""feature_box"">");
                foreach (GenericContentItem item in gc.Items)
                {
                    sb1.AppendFormat(@"<td class=""top_cap""><p class=""heading"">{0}</p></td>",item.Title);
                    sb2.AppendFormat(@"<td class=""center_cap"">{0}</td>", item.Summary);
                    sb3.AppendFormat(@"<td class=""bottom_cap""><div class=""pf btn""><a href=""{0}"">{1}</a></div></td>",item.MoreTextLinkURL,item.MoreText);                    
                }
                sb.AppendFormat(@"<tr>{0}</tr><tr>{1}</tr><tr>{2}</tr>",sb1.ToString(),sb2.ToString(),sb3.ToString());
                sb.AppendFormat(@"</table>");

            }
            return sb.ToString();
        }
    }
}
