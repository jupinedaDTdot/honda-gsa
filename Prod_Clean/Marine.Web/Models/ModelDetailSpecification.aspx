﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/ModelPage.master" AutoEventWireup="true" CodeBehind="ModelDetailSpecification.aspx.cs" Inherits="Marine.Web.Models.ModelDetailSpecification" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentArea" runat="server">
<div class="specs">
  <div class="content_section first_section">
    <div class="fc feature-titles-controls">
        <div class="fl h1"><asp:Literal ID="LiteralModelSpecification" runat="server"></asp:Literal></div>
        <asp:HyperLink ID="HyperLinkDownloadPdf" CssClass="fr download" runat="server"></asp:HyperLink>
        <div class="clr"></div>
        <div class="fc fr accordion-controls">
            <a property="act:collapsible-collapse" class="fl collapse" href="javascript:void(0)"><%=ResourceManager.GetString("txtCollapseAll")%></a>
            <a property="act:collapsible-expand" class="fl expand" href="javascript:void(0)"><%=ResourceManager.GetString("txtExpandAll")%></a>
        </div>
    </div>

    <asp:Repeater  runat="server" ID="rptSpecsCategory" OnItemDataBound="rptSpecsCategory_ItemDataBound" >
      <ItemTemplate>
            <div class="flex_bar"><span><a href="javascript:void(0);"
					            property="act:collapsible"
					            data-collapsible-target="#slidedown_<%#Eval("SpecCategoryID")%>"
					            data-collapsible-active="<%# Container.ItemIndex == 0 ? "true" : "false" %>"
					            ><%#Eval("SpecCategoryDescription")%></a></span>
            </div>
            <div id="slidedown_<%#Eval("SpecCategoryID")%>" class="sub_content">
                <table>
                    <asp:Repeater ID="rptSpecs" runat="server" >
                        <ItemTemplate>
					                <tr <%#(Eval("SpecValue").ToString().Length == 1 && Eval("SpecValue").ToString().StartsWith("-")) ? "class='dn'" : "" %>>
						                <th><%# Eval("SpecLabel")%></th>
						                <td><%# Eval("SpecValue")%></td>
					                </tr>
                         </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </ItemTemplate>   
    </asp:Repeater>
    
    <div class="content_section" id="divDisclaimer" runat="server">
        <uc:ElementControl ID="ElementControl1" ElementName="GenericContentPointer_CP8" runat="server" />  
        <asp:Literal ID="litDisclaimer" runat="server"></asp:Literal> 
    </div>

    <div class="content_section">
        <uc:ElementControl ID="ElementControl3" ElementName="GenericContentPointer_CP6" runat="server" />  
        <asp:Literal ID="litWarranty" runat="server"></asp:Literal> 
    </div>

  </div>
</div>
</asp:Content>