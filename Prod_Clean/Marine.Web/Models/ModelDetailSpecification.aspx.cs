﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using HondaPE.Service.Model;
using HondaCA.Common;
using HondaCA.Entity;
using HondaATV.Entity;
using System.Text;
using HondaCA.WebUtils.CMS;


namespace Marine.Web.Models
{
    public partial class ModelDetailSpecification : CmsContentBasePage
    {
        public string sGroup;
        int modelID;
        public HondaCA.Entity.Model.HondaModel objBaseModel;
        public HondaCA.Entity.Model.Trim objTrim;
        public HondaPE.Entity.Series oSeries;
        bool IsPrintSpecs = false;
        public HondaATV.Service.Model.SpecsService ss;
        public HondaATV.Service.Model.SpecsCategoryService sc;
        public EntityCollection<HondaATV.Entity.SpecsCategory> specCategoryList;
        public EntityCollection<HondaATV.Entity.Specs> TrimSpecs;

        string BaseModelUrl = string.Empty;
        string TrimExportKey = string.Empty;
        int SpecCount = 0;

        protected override void OnInit(EventArgs e)
        {
            string BaseUrl = string.Empty;
            try
            {
                BaseUrl = Request.QueryString["BaseUrl"];
                if (Request.QueryString["Print"] == "1")
                    IsPrintSpecs = true;
            }
            catch (Exception)
            {
                Response.Redirect("~/error/404");
            }
            this.MainUrl = BaseUrl;
            base.OnInit(e);

            //this is to add sectio specific class to the body tag
            //Generate body classes
            string BodyClass = "page-product-details";
          
            System.Web.UI.HtmlControls.HtmlControl ctrlBody = (System.Web.UI.HtmlControls.HtmlControl)this.Page.Master.Master.Master.Master.FindControl("body");
            if (ctrlBody != null) ctrlBody.Attributes["class"] = ctrlBody.Attributes["class"] == null ? BodyClass : " " + BodyClass;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BaseModelUrl = Request.QueryString["BaseModelUrl"];
                TrimExportKey = Request.QueryString["TrimExportKey"];
            }
            catch (Exception)
            {
            }

            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            HondaCA.Service.Model.HondaModelService ms = new HondaCA.Service.Model.HondaModelService();
            if (this.ContentPage != null)
            {
                modelID = (int)this.ContentPage.ProductID;
                objBaseModel = ms.GetModelByModelID(this.TargetID, PageLanguage, modelID);
            }
            else
            {
                objBaseModel = ms.GetModelByUrl(2, PageLanguage, BaseModelUrl);
            }
            if (objBaseModel == null) Response.Redirect("~/error/404");// throw new Exception("Url Not Found");

            ss = new HondaATV.Service.Model.SpecsService();
            sc = new HondaATV.Service.Model.SpecsCategoryService();

            PETrimService trimService = new PETrimService();
            objTrim = trimService.GetTrimByTrimID(this.TargetID, (int)this.ContentPage.ProductID, (int)this.ContentPage.TrimID, this.PageLanguage.GetCultureStringValue());

            LiteralModelSpecification.Text = string.Format(ResourceManager.GetString("txtSpecifications"), objTrim.TrimName);

            DoDisclaimer();
            
            DoWarranty();
            
            HyperLinkDownloadPdf.Text = ResourceManager.GetString("downloadpdf");

            if (IsPrintSpecs)
                PrintSpecs();
            else
                DoSpecs();
        }

        private void DoDisclaimer()
        {
            ContentPointer cp = CMSHelper.getCmsElementFromCmsPage<ContentPointer>(this.ContentPage, ElementControl1.ElementName ?? string.Empty);
            if (cp != null && cp.ContentElement.Display)
            {
                FreeFormHtml ffh = CMSHelper.getElementThroughContentPointerFromCmsPage<FreeFormHtml>(this.ContentPage, ElementControl1.ElementName ?? string.Empty);
                litDisclaimer.Text = CMSHelperExtention.getFreeFormHtml(ffh);
                if (ffh == null) divDisclaimer.Visible = ffh == null && !this.IsStaging ? false : true;
                if (ffh != null) divDisclaimer.Visible = string.IsNullOrEmpty(ffh.Html) && !this.IsStaging ? false : true;
            }
            else
                divDisclaimer.Visible = this.IsStaging ? true : cp.ContentElement.Display ? true : false;
        }

        private void DoWarranty()
        {
            //Warrant Section : should be shared : use ModelShared : GenericContent_FFH4
            ContentPointer cp = CMSHelper.getCmsElementFromCmsPage<ContentPointer>(this.ContentPage,ElementControl3.ElementName ??string.Empty);
            if (cp != null && cp.ContentElement.Display)
            {
                FreeFormHtml ffh = CMSHelper.getElementThroughContentPointerFromCmsPage<FreeFormHtml>(this.ContentPage,ElementControl3.ElementName ??string.Empty);
                litWarranty.Text = CMSHelperExtention.getFreeFormHtml(ffh);
            }
        }

        

        protected void DoSpecs()
        {
            HyperLinkDownloadPdf.Text = ResourceManager.GetString("downloadpdf");

            if (Request.RawUrl.ToLower() == this.MainUrl.ToLower())
            {
                HyperLinkDownloadPdf.NavigateUrl = this.MainUrl + "/print";
            }
            else
            {
                HyperLinkDownloadPdf.NavigateUrl = this.MainUrl + Request.RawUrl.Substring(Request.RawUrl.LastIndexOf("/")) + "/print";
            }

            specCategoryList = sc.GetSpecsCategoryByTrimId(objTrim.TrimID, PageLanguage, this.TargetID);
            if (specCategoryList != null && specCategoryList.Count > 0)
            {
                rptSpecsCategory.DataSource = specCategoryList;
                rptSpecsCategory.DataBind();
            }

            //HondaATV.Service.Model.SpecsService ss = new HondaATV.Service.Model.SpecsService();
            EntityCollection<HondaATV.Entity.Specs> TrimSpecs = ss.GetSpecsByTrimId(objTrim.TrimID, PageLanguage.GetCultureStringValue(), this.TargetID);
        }

        protected void rptSpecsCategory_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            HondaATV.Entity.SpecsCategory cat = (HondaATV.Entity.SpecsCategory)e.Item.DataItem;
            if (cat != null)
            {
                Repeater RepeaterSpecs = (Repeater)e.Item.FindControl("rptSpecs");
                TrimSpecs = ss.GetSpecsByTrimIdandCategoryID(objTrim.TrimID, cat.SpecCategoryID, PageLanguage, TargetID);
                SpecCount = TrimSpecs.Count();
                if (SpecCount > 0)
                {
                    RepeaterSpecs.DataSource = TrimSpecs;
                    RepeaterSpecs.DataBind();
                }
            }

        }

        protected void RepeaterSpecs_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            Specs TrimSpec = (Specs)e.Item.DataItem;
            Literal LiteralSpecItems = (Literal)e.Item.FindControl("LiteralSpecItems");

            StringBuilder sb = new StringBuilder();
            string spectValue = string.Empty;
            spectValue = TrimSpec.SpecValue.Trim().StartsWith("-") ? "" : TrimSpec.SpecValue;
            sb.AppendFormat(@"<tr class=""{0}"">", (e.Item.ItemIndex == 0 ? "first" : (e.Item.ItemIndex == SpecCount - 1 ? "last" : null)));
            sb.AppendFormat(@"<th><a>{0}</a></th>", TrimSpec.SpecLabel);
            sb.AppendFormat(@"<td>{0}</td>", spectValue);
            sb.Append("</tr>");
            LiteralSpecItems.Text = sb.ToString();
        }


        private void PrintSpecs()
        {
            string url = string.Format(Request.RawUrl);
            url = string.Format("/Models/PrintModelSpecs.aspx?TrimID={0}&ProductID={1}&Lang={2}&TargetID={3}&BaseUrl={4}", objTrim.TrimID, this.ContentPage.ProductID, this.PageLanguage.GetCultureStringValue(), this.TargetID, Request.QueryString["BaseUrl"]);
            //Response.Redirect(url);
            ExpertPdf.HtmlToPdf.PdfConverter pdfConverter = new ExpertPdf.HtmlToPdf.PdfConverter();

            string thisPageURL = HttpContext.Current.Request.Url.AbsoluteUri;
            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.PdfPageSize = ExpertPdf.HtmlToPdf.PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.TopMargin = 20;

            //pdfConverter.PdfFooterOptions.FooterHeight = 67; //TODO : If required please uncommente it.
            pdfConverter.PdfFooterOptions.DrawFooterLine = false;

            Navantis.Honda.CMS.Core.Common.HtmlToPDF htmlToPdf = new Navantis.Honda.CMS.Core.Common.HtmlToPDF();
            htmlToPdf.Converter = pdfConverter;

            htmlToPdf.WritePDF(this.Response, url, string.Format("{0}_specification_{1}.pdf", objTrim.TrimUrl, this.PageLanguage.GetCultureStringValue().Substring(0, 2)));
            Response.Write(url);
        }
    }
}