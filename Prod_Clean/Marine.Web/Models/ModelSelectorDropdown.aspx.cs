﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Marine.Service.Extensions;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.Common;
using HondaCA.Entity.Model;
using HondaCA.Service.Model;
using HondaCA.Entity;
using Marine.Web.Code;
using Marine.DataAccess.Model;
using HondaPE.Service.Model;

namespace Marine.Web.Models
{
    public partial class ModelSelectorDropdown : CmsContentBasePage
    {        
        protected string url;
        protected string baseModelFamilyUrl;
        protected int categoryId=0;
        protected override void OnInit(EventArgs e)
        {
            baseModelFamilyUrl = (Request.QueryString["BaseModelUrl"]).ToLower();
            if(Request.QueryString["SeriesID"] != "")
                categoryId = Convert.ToInt32(Request.QueryString["SeriesID"]);

            url = "/" + baseModelFamilyUrl; // + "/shared";
            this.MainUrl = url;
                        
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

           

            //Response.Write("|" + baseModelFamilyUrl + "|"+ this.PageLanguage.ToString());

            //get lang from contentpage 
            Cultures.GetCultureStringValue(this.PageLanguage);

            IElementContent element = this.ContentPage.Elements["GenericContentPointer_CP"].DeserializeElementObject() as IElementContent;
            plmenuFFH.Visible = element.ContentElement.Display ? true : this.IsStaging ? true : false;
            if (plmenuFFH.Visible)
            {
                FreeFormHtml freeFormHtml = getFreeFormHtmlFromContentPointer("GenericContentPointer_CP");
                plmenuFFH.Controls.Add(new LiteralControl(freeFormHtml!=null?freeFormHtml.Html : ""));
            }

            litUsage.Text = setCategoryUsage();

            plhtrimlist.Controls.Add(new LiteralControl(setModelList()));

            

           
            
        }

        private string setCategoryUsage()
        {
            StringBuilder sb = new StringBuilder();
            HondaModelService ModService = new HondaModelService();
            ModelCategory modelCategory = ModService.GetCategoryByCategoryUrl(this.TargetID, this.PageLanguage, baseModelFamilyUrl);

            //litHeading.Text = CommonFunctions.ucFirst(modelCategory.CategoryName) + " " + ResourceManager.GetString("txtMarineOutboards");
            litHeading.Text = ResourceManager.GetString("txt"+modelCategory.CategoryID+"MarineOutboards");
            bool IsUses = false;                        
            string []ArrayDetails = System.Text.RegularExpressions.Regex.Split(modelCategory.CategoryDescription, "\r\n");
            
            if (ArrayDetails.Length > 0)
            {
                int cIndex = 0;
                foreach (string aItem in ArrayDetails)
                {
                    if(cIndex++ == 0) litHeadDescription.Text = aItem;
                    if (aItem.Trim().ToLower().StartsWith("<uses>"))
                    {
                        if (string.IsNullOrEmpty(sb.ToString()))  sb.AppendFormat(@"<p class=""heading"">{0}</p><ul>", ResourceManager.GetString("txtUses"));                        
                        sb.AppendFormat(@"<li>{0}</li>", aItem.Substring(6));
                    }
                    else if (aItem.Trim().ToLower().StartsWith("<utilisations>"))
                    {
                        if (string.IsNullOrEmpty(sb.ToString())) sb.AppendFormat(@"<p class=""heading"">{0}</p><ul>", ResourceManager.GetString("txtUses"));
                        sb.AppendFormat(@"<li>{0}</li>", aItem.Substring(14));
                    }
                }
                sb.AppendFormat(@"{0}",!string.IsNullOrEmpty(sb.ToString()) == true ? "</ul>":"");
            }

            return sb.ToString();
        }

        private string setModelList()
        {
            StringBuilder sb = new StringBuilder();

            MarineTrimMapper trimService = new MarineTrimMapper();
            IList<Trim> listTrims = new EntityCollection<Trim>();
            listTrims = trimService.SelectTrimWithBasePrice(baseModelFamilyUrl, this.TargetID, this.PageLanguage.GetCultureStringValue());
            listTrims = listTrims.Select(trim => trim.GetPriceAdjustedTrim(TargetID, PageLanguage)).ToList();

            int cnt = 0;
            
            foreach (Trim trim in listTrims)
            {
              decimal MSRP;
              if (!this.IsQuebecPricing)
                MSRP = trim.MSRP;
              else
                MSRP = trim.MSRP + trim.FreightPDI;
                
                decimal DiscountAmount = PETrimService.getDiscountAmount(MSRP, trim.PriceDiscountAmount, trim.PriceDiscountPercentage);
                decimal DiscountedPrice = PETrimService.getDiscountedPrice(MSRP, trim.PriceDiscountAmount, trim.PriceDiscountPercentage);

                string firstCssClass = cnt == 0? "fc first product-item" : "fc product-item";
                string modelURL = "/" + baseModelFamilyUrl + "/" + trim.TrimUrl;
                sb.AppendFormat(@"<div class=""{0}""><div class=""fl image""><a href=""{1}"" rel=""nofollow""><img src=""{2}?Crop=auto&Width={3}&Height={4}"" height=""{4}"" width=""{3}"" /></a></div>"
                                    ,firstCssClass
                                    ,modelURL
                                    , CommonFunctions.BuildModelImageUrl(trim)
                                    ,50
                                    ,85);

                sb.AppendFormat(@"<div class=""fl product-copy""><div class=""heading""><a href=""{0}"" rel=""nofollow"">{1}</a></div><p>{2}</p></div>", modelURL, trim.TrimName, trim.TrimDescription);
                sb.AppendFormat(getPrice(DiscountAmount , DiscountedPrice, trim));
                sb.AppendFormat(@"</div>");
            }

            return sb.ToString();
        }

        private string getPrice(decimal DiscountAmount, decimal DiscountedPrice, Trim trim)
        {
            StringBuilder sb = new StringBuilder();
            PETrimService ts = new PETrimService();

            if (ts.IsComingSoonWithoutColor(trim))
            {
                sb.AppendFormat(@"<div class=""fr product-pricing""><div class=""small-price nmt""><span class=""price-heading"">{1}</span><span class=""price coming-soon"">{0}</span></div></div>"
                            , ResourceManager.GetString("txtComingSoonBr")
                            , ResourceManager.GetString("MSRP*")
                        );
            }
            else
            {
              decimal MSRP;
              if (!this.IsQuebecPricing)
                MSRP = trim.MSRP;
              else
                MSRP = trim.MSRP + trim.FreightPDI;
                if (DiscountAmount != 0)
                {
                    sb.AppendFormat(@"<div class=""fr product-pricing"">
                                                        <div class=""large-price"">
                                                            <span class=""price-heading"">{0}</span>
                                                            <span class=""price"">{1}</span>
                                                        </div>
                
                                                        <div class=""small-price"">
                                                            <span class=""price-heading"">{2}</span>
                                                            <del><span class=""strike""></span><span class=""price"">{3}</span></del>
                                                        </div>
                                                    </div>", ResourceManager.GetString("txtManufacturerDiscountedPrice"), string.Format(ResourceManager.GetString("valMSRP3"), DiscountedPrice), ResourceManager.GetString("MSRP*"), string.Format(ResourceManager.GetString("valMSRP4"), MSRP));
                }
                else
                {
                    sb.AppendFormat(@"<div class=""fr product-pricing""><div class=""large-price""><span class=""price-heading"">{0}</span><span class=""price"">{1}</span></div></div>"
                                        , ResourceManager.GetString("MSRP*"), string.Format(ResourceManager.GetString("valMSRP3"), MSRP));
                }
            }
            return sb.ToString();
        }

        

    }

}