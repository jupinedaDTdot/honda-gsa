﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;
using HondaATV.Service.Model;
using HondaCA.Common;
using System.Text;
using Marine.Web.Code;

namespace Marine.Web.Models
{
    //public partial class ModelDetailFeatures : CmsContentBasePage
    //{
    //    public string sGroup;
    //    int modelID;
    //    public HondaCA.Entity.Model.HondaModel objBaseModel;
    //    public HondaCA.Entity.Model.Trim objTrim;

    //    string BaseModelUrl = string.Empty;
    //    string TrimExportKey = string.Empty;

    //    protected override void OnInit(EventArgs e)
    //    {
    //        string BaseUrl = string.Empty;
    //        try
    //        {
    //            BaseUrl = Request.QueryString["BaseUrl"];
    //        }
    //        catch (Exception)
    //        {
    //            Response.Redirect("~/error/404");
    //        }
    //        this.MainUrl = BaseUrl;

    //        base.OnInit(e);

    //        //this is to add sectio specific class to the body tag
    //        System.Web.UI.HtmlControls.HtmlControl ctrlBody = (System.Web.UI.HtmlControls.HtmlControl)this.Page.Master.Master.Master.Master.FindControl("body");
    //        if (ctrlBody != null) ctrlBody.Attributes["class"] = ctrlBody.Attributes["class"] == null ? "page-product-details" : " page-product-details";
    //    }

    //    protected void Page_Load(object sender, EventArgs e)
    //    {
    //        this.PageLanguage = getLanguageForCMSPage();
    //        base.InitializeCulture();

    //        string strfeatures = null;

    //        try
    //        {
    //            BaseModelUrl = Request.QueryString["BaseModelUrl"];
    //            TrimExportKey = Request.QueryString["TrimExportKey"];

    //            PETrimService trimService = new PETrimService();
    //            objTrim = trimService.GetTrimByTrimID(this.TargetID, (int)this.ContentPage.ProductID, (int)this.ContentPage.TrimID, PageLanguage.GetCultureStringValue());

    //            strfeatures = trimService.GetTrimFeatureByTrimID(this.TargetID, objTrim.TrimID,PageLanguage);
    //        }
    //        catch (Exception ex)
    //        {
    //            HondaCA.Common.Error.HttpError(ex);
    //        }
    //        if (objTrim == null) Response.Redirect("~/error/404");

    //        LiteralPageHeader.Text = string.Format(ResourceManager.GetString("txtModelFeatures"), objTrim.TrimName);

    //        //Model Features
    //        if (strfeatures != null)
    //        {
    //            string[] ArrayFeatures = System.Text.RegularExpressions.Regex.Split(strfeatures, "\r\n");

    //            if (ArrayFeatures.Length > 0)
    //            {
    //                StringBuilder sb = new StringBuilder();
    //                int cnt = 0;
    //                for (int i = 0; i < (ArrayFeatures.Length); i++)
    //                {
    //                    if (ArrayFeatures[i].Trim().StartsWith("<<"))
    //                    {                            
    //                        //Start of a new category
    //                        string cat = ArrayFeatures[i].Trim().Substring(2, ArrayFeatures[i].LastIndexOf(">>")-2);
    //                        if(cnt == 0)
    //                            sb.AppendFormat(@"<h2>{0}</h2><dl>", cat);
    //                        else if (cnt >= 1)
    //                            sb.AppendFormat(@"</dl><h2>{0}</h2><dl>", cat);
    //                        cnt++;
    //                    }
    //                    else if (ArrayFeatures[i].Trim().StartsWith("<"))
    //                    {
    //                      //Start of a new category
    //                      string cat = ArrayFeatures[i].Trim().Substring(1, ArrayFeatures[i].LastIndexOf(">") - 1);
    //                      sb.AppendFormat(@"<dt>{0}</dt>", cat);
    //                    }
    //                    else
    //                    {
    //                        sb.AppendFormat(@"<dd>{0}</dd>", ArrayFeatures[i].Trim());
    //                    }
    //                }

    //                if (cnt > 1)
    //                    sb.AppendFormat(@"</dl>");

    //                LiteralFeatures.Text = sb.ToString();
    //            }
    //        }
    //    }
    //}
}