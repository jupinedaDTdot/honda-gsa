﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/ModelPage.master" AutoEventWireup="true" CodeBehind="ModelDetailFeatures.aspx.cs" Inherits="Marine.Web.Models.ModelDetailFeatures" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentArea" runat="server">
<div class="features">
	<div class="fc content_section first_section">
	    <div class="h1"><asp:Literal ID="LiteralPageHeader" runat="server"></asp:Literal></div>
      <asp:Literal ID="LiteralFeatures" runat="server"></asp:Literal>
	</div>
</div>
</asp:Content>
