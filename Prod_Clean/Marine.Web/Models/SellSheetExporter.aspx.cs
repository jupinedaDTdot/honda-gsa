﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Navantis.Honda.CMS.Demo;
using HondaATV.Service.Model;
using HondaATV.Entity;
using HondaCA.Entity;
using HondaCA.WebUtils.CMS;
using HondaPE.DataAccess;
using HondaPE.Entity;
using HondaCA.Common;
using System.Text;
using System.IO;
using Navantis.Honda.CMS.Client.Elements;

namespace Marine.Web.Models
{
    public partial class SellSheetExporter : CmsContentBasePage
    {

        public int TrimID;
        public string Lang;

        int SpecCount = 0;
        protected string ModelName;
        protected string TrimName;
        protected string ModelYear;
        protected string TrimDisclaimer;

        public string domain = "http://" + HondaCA.Common.Global.CurrentDomain.ToLower();

        public HondaCA.Entity.Model.Trim objTrim;

        protected override void OnInit(EventArgs e)
        {
            string BaseUrl = string.Empty;
            try
            {
                BaseUrl = Request.QueryString["BaseUrl"];
            }
            catch (Exception)
            {
                Response.Redirect("~/error/404");
            }
            //this.MainUrl = BaseUrl;
            this.MainUrl = BaseUrl;
           // IsNotCMSPage = true;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            int TrimID = Convert.ToInt32(Request.QueryString["TrimID"]);
            int ProductID = Convert.ToInt32(Request.QueryString["ProductID"]);
            int TargetID = Convert.ToInt32(Request.QueryString["TargetID"]);
            string Lang = Request.QueryString["Lang"];

            this.PageLanguage = Lang == "fr-CA" ? Language.French : Language.English;
            base.InitializeCulture();

            string strCssLogo = this.PageLanguage == Language.English ? "honda-logo" : "honda-logo-french";

            //  divLogo.Attributes.Add("class", strCssLogo);
            string path;
            path = "/_Global/css/";

            string resetCSS = readFile(Server.MapPath(path + "common/reset.css"));
            string printCSS = readFile(Server.MapPath(path + "common/sellsheets-print.css"));
            string ieCSS = readFile(Server.MapPath(path + "common/ie.css"));

            litHeaderLinks.Text = "<style type='text/css'>";
            litHeaderLinks.Text += resetCSS;
            litHeaderLinks.Text += printCSS;
            litHeaderLinks.Text += "</style>";
                        
            PETrimService trimService = new PETrimService();            
            objTrim = trimService.GetTrimByTrimID(TargetID, ProductID, TrimID, PageLanguage.GetCultureStringValue());

            string []URLs = Request.QueryString["BaseUrl"].Split('/');            

            HondaCA.Service.Model.HondaModelService hmService = new HondaCA.Service.Model.HondaModelService();
            HondaCA.Entity.Model.ModelCategory modelCategorie = hmService.GetCategoryByCategoryUrl(TargetID, PageLanguage, URLs[1]);
            
            ModelName = objTrim.BaseModelName;
            FreeFormHtml genContFFH3 = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContent_FFH3");
           // = CMSHelper.getContentPointerElementFromCmsPage(this.ContentPage, "SpecFooter_CP_1").ContentElement.DeserializeElementObject() as FreeFormHtml;
            if (genContFFH3 != null)
            {
                TrimDisclaimer = genContFFH3.Html;
            }
            TrimName = objTrim.TrimName +" "+ (string.IsNullOrEmpty(modelCategorie.CategoryName) ? "" : modelCategorie.CategoryName);
            
            StringBuilder sbfeatures = new StringBuilder();
            foreach (string feature in objTrim.Features.FeatureList)
            {
                if (!string.IsNullOrEmpty(feature.Trim()))
                    sbfeatures.AppendFormat(@"<li>{0}</li>", feature);
            }
            litFeatures.Text = sbfeatures.ToString();
            ModelYear = objTrim.ModelYearYear.ToString();

            imgModel.ImageUrl = string.Format("{0}", Marine.Web.Code.CommonFunctions.BuildModelImageUrl(objTrim));


            HondaATV.Service.Model.SpecsCategoryService specCategoryService = new HondaATV.Service.Model.SpecsCategoryService();
            EntityCollection<HondaATV.Entity.SpecsCategory> spectCategories = specCategoryService.GetSpecsCategoryByTrimId(TrimID, PageLanguage, TargetID);
            if (spectCategories.Count > 0)
            {
                RptSpectCategory.DataSource = spectCategories;
                RptSpectCategory.DataBind();
                litDisclaimer.Text = getDisclaimer();
                //litWarranty.Text = getWarranty();
            }
            else
                RptSpectCategory.Visible = false;

            //SpecsService ss = new SpecsService();
            //EntityCollection<Specs> TrimSpecs = ss.GetSpecsByTrimId(TrimID, PageLanguage.GetCultureStringValue(), TargetID);

            //SpecCount = TrimSpecs.Count();

            //if (SpecCount > 0)
            //{
            //    RepeaterSpecs.DataSource = TrimSpecs;
            //    RepeaterSpecs.DataBind();
            //}
        }

        protected void RepeaterSpecs_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            HondaATV.Entity.SpecsCategory specCategory = (HondaATV.Entity.SpecsCategory)e.Item.DataItem;            
            int trimID = Convert.ToInt32(Request.QueryString["TrimID"]);
            Literal LitLeft = (Literal)e.Item.FindControl("LitLeft");
            Literal LitRight = (Literal)e.Item.FindControl("LitRight");

            HondaATV.Service.Model.SpecsService specService = new HondaATV.Service.Model.SpecsService();
            EntityCollection<HondaATV.Entity.Specs> spectItems = specService.GetSpecsByTrimIdandCategoryID(trimID, specCategory.SpecCategoryID, PageLanguage, TargetID);

            StringBuilder sbLeft = new StringBuilder();
            StringBuilder sbRight = new StringBuilder();

            if (spectItems.Count > 0)
            {
                int cnt = 1;                
                foreach (HondaATV.Entity.Specs spect in spectItems)
                {
                    if (cnt % 2 == 1)
                        sbLeft.AppendFormat(@"<tr {0}><th>{1}</th><td>{2}</td></tr>", (cnt+ 1) % 4 == 0 ? "class='even'" : "", spect.SpecLabel, spect.SpecValue);
                    else
                        sbRight.AppendFormat(@"<tr {0}><th>{1}</th><td>{2}</td></tr>", cnt % 4 == 0 ? "class='even'" : "", spect.SpecLabel, spect.SpecValue);

                    cnt++;
                }
                LitLeft.Text = sbLeft.ToString();
                LitRight.Text = sbRight.ToString();
            }
            else
            {
                e.Item.Visible = false;
            }
       
        }

        private string getDisclaimer()
        {
            ContentPointer cp = CMSHelper.getCmsElementFromCmsPage<ContentPointer>(this.ContentPage, "GenericContentPointer_CP8");
            if (cp != null && cp.ContentElement.Display)
            {
                FreeFormHtml ffh = CMSHelper.getElementThroughContentPointerFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContentPointer_CP8");
                return CMSHelperExtention.getFreeFormHtml(ffh);
            }
            return string.Empty;
        }

        private string getWarranty()
        {
            //Warrant Section : should be shared : use ModelShared : GenericContent_FFH4
            ContentPointer cp = CMSHelper.getCmsElementFromCmsPage<ContentPointer>(this.ContentPage, "GenericContentPointer_CP6");
            if (cp != null && cp.ContentElement.Display)
            {
                FreeFormHtml ffh = CMSHelper.getElementThroughContentPointerFromCmsPage<FreeFormHtml>(this.ContentPage, "GenericContentPointer_CP6");
                return CMSHelperExtention.getFreeFormHtml(ffh);
            }
            return string.Empty;
        }
//        protected void RepeaterSpecs_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
//        {
//            Specs TrimSpec = (Specs)e.Item.DataItem;
//            Literal LiteralSpecItems = (Literal)e.Item.FindControl("LiteralSpecItems");
//            StringBuilder sb = new StringBuilder();
//            if (e.Item.ItemIndex + 1 <= SpecCount / 2|| e.Item.ItemIndex==SpecCount-1)
//            {
//                if (e.Item.ItemIndex == 0)
//                {
//                    sb.AppendFormat(@"<div class=""{0}"">
//				<table class=""fnat"">", "fl left-table");
//                    sb.AppendFormat(@"<tr style=""page-break-inside:avoid;"">
//						<th>{0}</th>
//						<td>{1}</td>
//					</tr>",  TrimSpec.SpecLabel, TrimSpec.SpecValue);
//                }
//                else
//                {
//                    if (e.Item.ItemIndex == (SpecCount / 2) - 1 || e.Item.ItemIndex == SpecCount - 1)
//                    {
//                        if ((e.Item.ItemIndex + 1) % 2 != 0)
//                        {

//                            sb.AppendFormat(@"<tr style=""page-break-inside:avoid;"">
//						<th>{0}</th>
//						<td>{1}</td>
//					</tr>", TrimSpec.SpecLabel, TrimSpec.SpecValue);
//                        }
//                        else
//                        {

//                            sb.AppendFormat(@"<tr class=""even"" style=""page-break-inside:avoid;"">
//						<th>{0}</th>
//						<td>{1}</td>
//					</tr>", TrimSpec.SpecLabel, TrimSpec.SpecValue);
//                        }
//                        sb.Append("</table></div>");
//                    }
//                    else
//                    {
//                        if ((e.Item.ItemIndex + 1) % 2 != 0)
//                        {

//                            sb.AppendFormat(@"<tr style=""page-break-inside:avoid;"">
//						<th>{0}</th>
//						<td>{1}</td>
//					</tr>", TrimSpec.SpecLabel, TrimSpec.SpecValue);
//                        }
//                        else
//                        {

//                            sb.AppendFormat(@"<tr class=""even"" style=""page-break-inside:avoid;"">
//						<th>{0}</th>
//						<td>{1}</td>
//					</tr>", TrimSpec.SpecLabel, TrimSpec.SpecValue);
//                        }
//                    }


//                }




//            }
//            else
//            {
//                if (e.Item.ItemIndex == SpecCount / 2)
//                {
//                    sb.AppendFormat(@"<div class=""{0}"">
//				<table class=""fnat"">", "fr right-table");
//                    sb.AppendFormat(@"<tr style=""page-break-inside:avoid;"">
//						<th>{0}</th>
//						<td>{1}</td>
//					</tr>", TrimSpec.SpecLabel, TrimSpec.SpecValue);
//                }
//                else
//                {
//                    if (e.Item.ItemIndex == SpecCount - 1)
//                    {
//                        if ((e.Item.ItemIndex + 1) % 2 != 0)
//                        {

//                            sb.AppendFormat(@"<tr style=""page-break-inside:avoid;"">
//						<th>{0}</th>
//						<td>{1}</td>
//					</tr>", TrimSpec.SpecLabel, TrimSpec.SpecValue);
//                        }
//                        else
//                        {

//                            sb.AppendFormat(@"<tr class=""even"" style=""page-break-inside:avoid;"">
//						<th>{0}</th>
//						<td>{1}</td>
//					</tr>", TrimSpec.SpecLabel, TrimSpec.SpecValue);
//                        }
//                        sb.Append("</table></div>");
//                    }
//                    else
//                    {
//                        if ((e.Item.ItemIndex + 1) % 2 != 0)
//                        {

//                            sb.AppendFormat(@"<tr style=""page-break-inside:avoid;"">
//						<th>{0}</th>
//						<td>{1}</td>
//					</tr>", TrimSpec.SpecLabel, TrimSpec.SpecValue);
//                        }
//                        else
//                        {

//                            sb.AppendFormat(@"<tr class=""even"" style=""page-break-inside:avoid;"">
//						<th>{0}</th>
//						<td>{1}</td>
//					</tr>", TrimSpec.SpecLabel, TrimSpec.SpecValue);
//                        }
//                    }


//                }

//            }

//            string tablestyle = (e.Item.ItemIndex + 1) % 2 == 0 ? "fl left-table" : "fr right-table";


//            //sb.AppendFormat(@"<th><a>{0}</a></th>", TrimSpec.SpecLabel);
//            //sb.AppendFormat(@"<td>{0}</td>", TrimSpec.SpecValue);
//            //sb.Append("</tr>");
//            LiteralSpecItems.Text += sb.ToString();
//        }

        protected string readFile(string Path)
        {
            StringBuilder sb = new StringBuilder();
            StreamReader file = new StreamReader(Path);
            string line = string.Empty;

            while ((line = file.ReadLine()) != null)
            {
                sb.Append(line.Replace("../img/", domain + "/_Global/img/"));
            }

            file.Close();

            return sb.ToString();
        }
    }
}