﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Configuration;
using HondaCA.Common;

namespace Marine
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            //bool throwError = false;
            //throwError = ((ConfigurationManager.AppSettings["ThrowError"] ?? "false").ToLower() == "false") ? false : true;

            //try
            //{

            //    Exception ex = Server.GetLastError().GetBaseException();
            //    HondaCALogging logger = new HondaCALogging();
            //    logger.LogException(ex.Message, HondaCAEventType.Error);
            //    logger = new HondaCALogging();
            //    logger.LogException("test", HondaCAEventType.Warning);


            //    string rawUrl = Request.RawUrl;
            //    //errorpages/error.aspx?err=404

            //    HttpException objHttpErr = (HttpException)Server.GetLastError();
            //    int errorcode = objHttpErr.GetHttpCode();

            //    if (!throwError)
            //    {
            //        Server.ClearError();

            //        switch (errorcode)
            //        {
            //            case 404:
            //                //404.aspx will try to find page name in OldUrls table
            //                Response.Redirect("/errorpages/404.html");
            //                break;
            //            default:
            //                Response.Redirect("/errorpages/error.html");
            //                break;
            //        }
            //    }

            //}
            //catch (Exception ex)
            //{
            //    if (!throwError)
            //    {
            //        Response.Redirect("/errorpages/error.html");
            //    }
            //    else
            //    { throw; }
            //}

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
