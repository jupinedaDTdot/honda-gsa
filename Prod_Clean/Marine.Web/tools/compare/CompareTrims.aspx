﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true" CodeBehind="CompareTrims.aspx.cs" Inherits="Marine.Web.tools.compare.CompareTrims" %>
<%@ Import Namespace="HondaCA.WebUtils.CMS" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="Stylesheet" type="text/css" href="/_Global/css/sections/compare.css" />
<link rel="Stylesheet" type="text/css" href="/_Global/css/engine/sections/compare.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
<div class="cap_top_default pf"></div>
<div class="container pf">
	<div class="content_container">
		<div class="content_section first_section" id="CompareApp">
			<h1><%=ResourceManager.GetString("txtCompareModel4")%></h1>
            <input type="hidden" id="ModelFamilyUrl" name="ModelFamilyUrl" value="<%= Request.QueryString["ModelFamilyUrl"] ?? "" %>" />
            <input type="hidden" id="TrimIDs" name="TrimIDs" value="<%= preloadedTrimIDs %>" />
			<table class="tbl tbl_compare_result" border="0">
				<thead>
				 	<tr class="first top">
						<th class="first odd name">
							<div><span><%=ResourceManager.GetString("ChooseYourModel")%></span></div>
						</th>
					 	<th class="odd compare" data-index="2">
                                <div class="popup_wrap">

                                    <a href="javascript:void(0)" class="btn secondary delayed"
                                        data-popup-class="model-selector-right"
                                        data-popup-content-url="<% =comparePopupUrl %>"
                                        data-position-my="left top"
                                        data-position-at="left bottom"
                                        data-popup-hanger="true"
                                        data-compare-behavior="add"
                                        property="compare:model-selector"><span><%=ResourceManager.GetString("txtSelectModel")%></span></a>
                                </div>
							<div class="tag-trim-details dn">
								<div class="model">
									<span class="tag-trim-name"></span>
								</div>
							<div class="tag-coming-soon dn">
                                    <span class="price coming-soon"><%= ResourceManager.GetString("txtComingSoon") %></span>
                                    <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                </div>
                                <div class="tag-no-discount">
                                    <span class="price tag-price"></span>
                                    <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                </div>
                                	
                                <div class="tag-has-discount dn">
                                    <div class="fc original price">
                                        <del><span class="strike"></span><strong class="tag-price">$999</strong></del>
                                        <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                    </div>
                                    <div class="fc special price">
                                            <strong class="tag-discount"></strong>
                                            <span class="tag-text-discount"><%=ResourceManager.GetString("Discount")%></span>
                                    </div>
                                    <div class="fc final price">
                                            <strong class="tag-final-price"></strong>
                                            <span class="tag-text-final-price"><%=ResourceManager.GetString("YourPrice*")%></span>
                                    </div>
                                </div>
								<div class="clr"></div>
								<div class="product_image">
									<img class="tag-thumbnail" src="/_Global/img/content/compare/product_image_sample.gif"/>
								</div>
								<div class="change_btn">
                                    <a href="javascript:void(0)" class="btn secondary delayed"
                                        data-popup-class="model-selector-right"
                                        data-position-my="left top"
                                        data-position-at="left bottom"
                                        data-popup-content-url="<% =comparePopupUrl %>"
                                        data-popup-hanger="true"
                                        data-compare-behavior="replace"
                                        property="compare:model-selector"><span><%=ResourceManager.GetString("Change")%></span></a>
								</div>

							</div>
						</th>
						<th class="odd spacer"></th>
						<th class="even compare" data-index="4">
							<div class="top_corners">
								<div class="tl"></div><div class="tr"></div>
								<div class="clr"></div>
							</div>
                            <div class="tag-trim-details dn">

                                <div class="model">
                                    <span class="tag-trim-name"></span>
                                    <a href="javascript:void(0)" class="close"></a>
                                    <div class="clr"></div>
                                </div>
							<div class="tag-coming-soon dn">
                                    <span class="price coming-soon"><%= ResourceManager.GetString("txtComingSoon") %></span>
                                    <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                </div>
                                <div class="tag-no-discount">
                                    <span class="price tag-price"></span>
                                    <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                </div>
                            	
                                <div class="tag-has-discount dn">
                                    <div class="fc original price">
                                        <del><span class="strike"></span><strong class="tag-price">$999</strong></del>
                                        <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                    </div>
                                    <div class="fc special price">
                                            <strong class="tag-discount"></strong>
                                            <span class="tag-text-discount"><%=ResourceManager.GetString("Discount")%></span>
                                    </div>
                                    <div class="fc final price">
                                            <strong class="tag-final-price"></strong>
                                            <span class="tag-text-final-price"><%=ResourceManager.GetString("YourPrice*")%></span>
                                    </div>
                                </div>
                                <div class="clr"></div>
                                <div class="product_image">
                                    <img class="tag-thumbnail" src="/_Global/img/content/compare/product_image_sample.gif"/>
                                </div>
                                <div class="change_btn">
                                    <a href="javascript:void(0)" class="btn secondary delayed"
                                        data-popup-class="model-selector"
                                        data-popup-content-url="<% =comparePopupUrl %>"
                                        data-popup-hanger="true"
                                        data-compare-behavior="replace"
                                        property="compare:model-selector"><span><%=ResourceManager.GetString("Change")%></span></a>
                                </div>
                            </div>

                            <div class="popup_wrap">
                                    <a href="javascript:void(0)" class="btn secondary delayed"
                                    data-popup-class="model-selector"
                                    data-popup-content-url="<% =comparePopupUrl %>"
                                    data-popup-hanger="true"
                                    data-compare-behavior="add"
                                    property="compare:model-selector"><span><%=ResourceManager.GetString("txtSelectModel")%></span></a>
                            </div>
						</th>
						<th class="odd spacer"></th>
						<th class="even compare" data-index="6">
							<div class="top_corners">
								<div class="tl"></div><div class="tr"></div>

								<div class="clr"></div>
							</div>
                            <div class="tag-trim-details dn">
                                <div class="model">
                                    <span class="tag-trim-name"></span>
                                    <a href="javascript:void(0)" class="close"></a>
                                    <div class="clr"></div>
                                </div>
								<div class="tag-coming-soon dn">
                                    <span class="price coming-soon"><%= ResourceManager.GetString("txtComingSoon") %></span>
                                    <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                </div>
                                <div class="tag-no-discount">
                                    <span class="price tag-price"></span>
                                    <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                </div>
	
                                <div class="tag-has-discount dn">
                                    <div class="fc original price">
                                        <del><span class="strike"></span><strong class="tag-price">$999</strong></del>
                                        <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                    </div>
                                    <div class="fc special price">
                                            <strong class="tag-discount"></strong>
                                            <span class="tag-text-discount"><%=ResourceManager.GetString("Discount")%></span>
                                    </div>
                                    <div class="fc final price">
                                            <strong class="tag-final-price"></strong>
                                            <span class="tag-text-final-price"><%=ResourceManager.GetString("YourPrice*")%></span>
                                    </div>
                                </div>
                                <div class="clr"></div>
                                <div class="product_image">
                                    <img class="tag-thumbnail" src="/_Global/img/content/compare/product_image_sample.gif"/>
                                </div>
                                <div class="change_btn">
                                    <a href="javascript:void(0)" class="btn secondary delayed"
                                        data-popup-class="model-selector"
                                        data-popup-content-url="<% =comparePopupUrl %>"
                                        data-popup-hanger="true"
                                        data-compare-behavior="replace"
                                        property="compare:model-selector"><span><%=ResourceManager.GetString("Change")%></span></a>

                                </div>
                            </div>
                            <div class="popup_wrap">
                                    <a href="javascript:void(0)" class="btn secondary delayed"
                                    data-popup-class="model-selector"
                                    data-popup-content-url="<% =comparePopupUrl %>"
                                    data-popup-hanger="true"
                                    data-compare-behavior="add"
                                    property="compare:model-selector"><span><%=ResourceManager.GetString("txtSelectModel")%></span></a>
                            </div>
						</th>
						<!-- FIXME &nbsp; hack to prevent gray area -->
						<th class="odd spacer"></th>
						<th class="last even compare" data-index="8">
							<div class="top_corners">
								<div class="tl"></div><div class="tr"></div>
								<div class="clr"></div>
							</div>
                            <div class="tag-trim-details dn">
                                <div class="model">
                                    <span class="tag-trim-name"></span>
                                    <a href="javascript:void(0)" class="close"></a>
                                    <div class="clr"></div>
                                </div>
								<div class="tag-coming-soon dn">
                                    <span class="price coming-soon"><%= ResourceManager.GetString("txtComingSoon") %></span>
                                    <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                </div>
                                <div class="tag-no-discount">
                                    <span class="price tag-price"></span>
                                    <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                </div>
	
                                <div class="tag-has-discount dn">
                                    <div class="fc original price">
                                        <del><span class="strike"></span><strong class="tag-price">$999</strong></del>
                                        <span class="msrp tag-text-msrp"><%=ResourceManager.GetString("MSRP*")%></span>
                                    </div>
                                    <div class="fc special price">
                                            <strong class="tag-discount"></strong>
                                            <span class="tag-text-discount"><%=ResourceManager.GetString("Discount")%></span>
                                    </div>
                                    <div class="fc final price">
                                            <strong class="tag-final-price"></strong>
                                            <span class="tag-text-final-price"><%=ResourceManager.GetString("YourPrice*")%></span>
                                    </div>
                                </div>
                                <div class="clr"></div>
                                <div class="product_image">
                                    <img class="tag-thumbnail" src="/_Global/img/content/compare/product_image_sample.gif"/>
                                </div>
                                <div class="change_btn">
                                    <a href="javascript:void(0)" class="btn secondary delayed"
                                        data-popup-class="model-selector"
                                        data-popup-content-url="<% =comparePopupUrl %>"
                                        data-popup-hanger="true"
                                        data-compare-behavior="replace"
                                        property="compare:model-selector"><span><%=ResourceManager.GetString("Change")%></span></a>
                                </div>
                            </div>

							<div class="popup_wrap">
                                    <a href="javascript:void(0)" class="btn secondary delayed"
									data-popup-class="model-selector"
                                    data-popup-content-url="<% =comparePopupUrl %>"
									data-popup-hanger="true"
                                    data-compare-behavior="add"
									property="compare:model-selector"><span><%=ResourceManager.GetString("txtSelectModel")%></span></a>
							</div>
						</th>							
					</tr>
				</thead>
				<tbody>
					<tr class="first note">
						<td class="note" colspan="2">
						    <div><span><%=ResourceManager.GetString("comparisonLabel")%></span></div>
                            <div class="price-disclaimer"><span>
                                                              *<% = ( (this.IsQuebecPricing)  ? this.ResourceManager.GetString("MSRPLegalTooltipQuebec") : this.ResourceManager.GetString("MSRPLegalTooltip")  )%>
                                                          </span></div>
                        </td>								
						<td class="odd spacer"><div><span></span></div></td>

						<td class="even"><div><span></span></div></td>
						<td class="odd spacer"><div><span></span></div></td>
						<td class="even"><div><span></span></div></td>
						<td class="odd spacer"><div><span></span></div></td>
						<td class="even"><div><span>&nbsp;</span></div></td>
					</tr>
                            
                    <tr class="tmpl dn">
                        <td class="first odd">
                            <div>

                                <p>
                                    <a class="tag-spec-label" property="act:tooltip"
                                        data-tooltip-class="tooltip-right-alt"
                                        data-position="right bottom"
                                        data-tooltip-content-target=""></a>
                                    <div class="tag-tooltip dn">
                                        Tooltip content
                                    </div>
                                </p>
                            </div>
                        </td>
                        <td class="odd"><div><span></span></div></td>

                        <td class="odd spacer"><div><span></span></div></td>
                        <td class="even"><div><span></span></div></td>
                        <td class="odd spacer"><div><span></span></div></td>
                        <td class="even"><div><span></span></div></td>
                        <td class="odd spacer"><div><span></span></div></td>
                        <td class="even"><div><span>&nbsp;</span></div></td>
                    </tr>
				</tbody>
			</table>
            <asp:PlaceHolder runat="server" ID="DisclaimerPlaceHolder">
            <div class="disclaimer" runat="server" Visible="<%# IsStaging || !string.IsNullOrWhiteSpace(Disclaimer.Html) %>">
                <uc:ElementControl runat="server" Visible="<%# IsStaging %>" ElementName="<%# ContentElementDisclaimer %>" />
                <asp:PlaceHolder runat="server" Visible="<%# Disclaimer.ContentElement.Display %>">
                <div class="disclaimer-content">
                    <%# Disclaimer.Html %>
                </div>
                </asp:PlaceHolder>
            </div>
            </asp:PlaceHolder>
            <div class="error-message-add dn"><%= ResourceManager.GetString("CompareErrorMessage") %></div>
		</div>
	</div>
</div>
<div class="cap_bottom_default pf"></div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="BottomCap" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="includeModelVarJS" runat="server">
<script type="text/javascript" src="/_Global/js/sections/compare.js"></script>
</asp:Content>
