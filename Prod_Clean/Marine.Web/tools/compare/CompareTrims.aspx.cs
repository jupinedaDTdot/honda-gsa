﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using HondaCA.Common;
using Marine.Web.ContentPages;
using HondaCA.WebUtils.CMS;
using Navantis.Honda.CMS.Client.Elements;
using Navantis.Honda.CMS.Demo;

namespace Marine.Web.tools.compare
{
    public partial class CompareTrims : CmsContentBasePage
    {

        public const string ContentElementDisclaimer = "GenericContent_FFH";

        public FreeFormHtml Disclaimer { get; set; }

        protected string comparePopupUrl;
        protected string preloadedTrimIDs;
        protected string ModelFamilyName = string.Empty;
        string langParm = "en";
        protected override void OnInit(EventArgs e)
        {
            if (Request.RawUrl == "/compare" || Request.RawUrl == "/comparer")
            {
                // won't change
                MainUrl = Request.RawUrl;
            }
            else if (Request.RawUrl == "/compare/" || Request.RawUrl == "/comparer/")
            {
                MainUrl = Request.RawUrl.Substring(0, Request.RawUrl.Length - 1);
            }
            else if (Request.RawUrl.StartsWith("/compare/") || Request.RawUrl.StartsWith("/comparer/"))
            {
                var path = Request.RawUrl.Substring(1);
                MainUrl = "/" + path.Substring(0, path.IndexOf('/'));
            }
            else
            {
                MainUrl = "/compare";
            }
            base.OnInit(e);

        }

        protected void prepare()
        {
            langParm = Cultures.GetLanguageString(PageLanguage);
            comparePopupUrl = string.Format("/service/ComparePopUpSelector.aspx?ModelFamilyUrl={0}&amp;L={1}", Request.QueryString["ModelFamilyUrl"], langParm);
            preloadedTrimIDs = Request.QueryString["TrimIDs"] ?? String.Empty;
            ModelFamilyName = getModelFamilyNameByUrl(Request.QueryString["ModelFamilyUrl"]);
        }

        private string getModelFamilyNameByUrl(string url)
        {
            HondaCA.Service.Model.HondaModelService ms = new HondaCA.Service.Model.HondaModelService();
            HondaCA.Entity.EntityCollection<HondaCA.Entity.Model.BaseModelFamily> modelFamilyList = ms.GetModelFamilyByFamilyURL(this.PageLanguage, url);

            if (modelFamilyList.Count > 0) return modelFamilyList[0].ModelFamilyName ?? string.Empty;
            else
                return string.Empty;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();
            prepare();

            var body = (HtmlGenericControl) this.Master.Master.Master.FindControl("body");
            if (body != null)
            {
                body.Attributes.Add("class", "page-compare-results");
            }

            Disclaimer = CMSHelper.getCmsElementFromCmsPage<FreeFormHtml>(this.ContentPage, ContentElementDisclaimer);
            DisclaimerPlaceHolder.DataBind();
        }
    }
}