﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_Global/master/CmsOneColMaster.master" AutoEventWireup="true" CodeBehind="CompareHome.aspx.cs" Inherits="Marine.Web.tools.compare.CompareHome" %>
<%@ Import Namespace="Navantis.Honda.CMS.Client.Elements" %>
<%@ Register Src="~/Cms/Console/ElementControlCompaq.ascx" TagPrefix="uc" TagName="ElementControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/_Global/css/sections/help-me-choose.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="bodyStart" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="feature_area" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="wrapper_seperator" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="ContentArea" runat="server">
  <div class="cap_top_default pf"></div>
		<div class="container pf">
			<div class="content_container">
				<div class="content_section first_section">
					<h1><%=ResourceManager.GetString("CompareTitle")%></h1>
				</div>
				<div class="nb content_section main_section">
                    <uc:ElementControl ID="ElementControl1" runat="server" ElementName="GenericContent_Tout" />
                    <asp:PlaceHolder ID="plModelIst" runat="server"></asp:PlaceHolder>					
				</div>
			</div>
		</div>
		<div class="cap_bottom_default pf"></div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="insertDivatBottom" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="includeModelVarJS" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="cmsMasterIncludeJavaScripatEnd" runat="server">
</asp:Content>