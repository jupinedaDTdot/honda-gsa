﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using Marine.Web.ContentPages;
using Navantis.Honda.CMS.Demo;
using Navantis.Honda.CMS.Client.Elements;


using HondaCA.Entity.Model;


namespace Marine.Web.tools.compare
{
    public partial class CompareHome : ContentBasePage
    {
        protected override void OnInit(EventArgs e)
        {
            this.MainUrl = HttpContext.Current.Request.RawUrl;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PageLanguage = getLanguageForCMSPage();
            base.InitializeCulture();

            HtmlGenericControl body = this.Master.Master.Master.FindControl("body") as HtmlGenericControl;
            if (body != null)
            {
                body.Attributes.Add("class", "page-help-me-choose-landing");
                this.IsMasterBodyClassUpdate = true;
            }
            Tout tout = getTout("GenericContent_Tout");

            if (tout != null)
            {
                plModelIst.Visible = tout.ContentElement.Display ? true : this.IsStaging ? true : false;

                if (plModelIst.Visible)
                {
                    plModelIst.Controls.Add(new LiteralControl(getToutString(tout)));
                }
            }

        }

        protected string getToutString(Tout tout)
        {
            StringBuilder sb = new StringBuilder();
            if (tout != null && tout.Items != null)
            {
                if (tout.ContentElement.Display)
                {
                    int intElementCnt = 1;
                    sb.Append(string.Format(@"<ul class=""fc product_categories"">"));
                    foreach (ToutItem item in tout.Items)
                    {
                        if (intElementCnt > 3)
                        {
                            sb.Append(string.Format(@"</ul><ul class=""fc product_categories"">"));
                            intElementCnt = 1;
                        }
                        string strNM = intElementCnt == 1 ? "nm" : "";
                        sb.Append(string.Format(@"<li class=""fl {0} item""><div class=""image""><a href=""{2}""><img src=""{1}"" alt=""""></a></div><h2 class=""small""><a href=""{2}"">{3}</a></h2></li>", strNM, item.PathReference, item.URL, item.Title));
                        intElementCnt++;
                    }
                    sb.Append(string.Format(@"</ul>"));
                }
            }
            return sb.ToString();
        }
    }
}