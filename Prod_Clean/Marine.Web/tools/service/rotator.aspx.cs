﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HondaCA.Common;
using HondaCA.WebUtils.CMS;
using Marine.Web.ContentPages;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils;
using Navantis.Honda.CMS.Demo;

namespace Marine.Web.tools.service
{
	public partial class ToutRotatorPlaylist : ContentBasePage
	{
		private string _url;
		private string _element;
        private string _isRealtedPage;

		protected override void OnInit(EventArgs e)
		{
      _url = RequestUtils.ExtractQueryStringParameter(Request.QueryString, "url") ?? string.Empty;
      _element = RequestUtils.ExtractQueryStringParameter(Request.QueryString, "element") ?? "Tout";
      _isRealtedPage = RequestUtils.ExtractQueryStringParameter(Request.QueryString, "isRealtedPage") ?? "false";

			MainUrl = _url;

			base.OnInit(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (PageSetting != null)
				PageLanguage = (Language)this.PageSetting.Language;// cms class should pass Value 1 or 2 
			else
				PageLanguage = Language.English;

			base.InitializeCulture();

		    Tout tout;
            if (_isRealtedPage.ToLower() == "true")
            {
                tout = CMSHelper.getCmsElementFromRelatedPage<Tout>(this.RelatedPages, _element);
            }
            else
            {
                tout = (Tout)CMSHelper.getCmsElementFromCmsPage(ContentPage, _element);
            }

			Response.Write(tout.ToRotatorPlaylistString());
			Response.End();
		}
	}
}