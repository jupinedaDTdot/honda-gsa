﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HondaCA.Common;
using HondaCA.WebUtils.CMS;
using Marine.Web.ContentPages;
using Navantis.Honda.CMS.Client.Elements;
using HondaCA.WebUtils;
using Navantis.Honda.CMS.Demo;

namespace Marine.Web.tools.service
{
	public partial class MultiVideoPlaylist : ContentBasePage
	{
		private string _url;
		private string _element;
		private NameValueCollection _query;

		protected override void OnInit(EventArgs e)
		{
      _url = RequestUtils.ExtractQueryStringParameter(Request.QueryString, "url") ?? string.Empty;
      _element = RequestUtils.ExtractQueryStringParameter(Request.QueryString, "element") ?? "Tout";
			MainUrl = _url;
			base.OnInit(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (PageSetting != null)
				PageLanguage = (Language)this.PageSetting.Language;// cms class should pass Value 1 or 2 
			else
				PageLanguage = Language.English;

			base.InitializeCulture();

			Gallery gallery = (Gallery)CMSHelper.getCmsElementFromCmsPage(ContentPage, _element);


			Response.Write(gallery.ToVideoPlaylistString(new PlaylistLabelValues ()
			                                               {
                                                       Replay = ResourceManager.GetString("Replay"),
                                                       Share = ResourceManager.GetString("Share"),
                                                       ShareBoxTitle = ResourceManager.GetString("ShareBoxTitle"),
                                                       ShareBoxSubtitle = ResourceManager.GetString("ShareBoxSubTitle"),
			                                               }));
			Response.End();
		}
	}
}