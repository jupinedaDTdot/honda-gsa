﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pager.ascx.cs" Inherits="Marine.Web.Search.Pager" %>
<%@ Import Namespace="Marine.Web.Search" %>
<div class="pagination">
	<div class="fl prev_next_btns">
	    <asp:HyperLink runat="server" ID="PreviousLink" CssClass="prev" NavigateUrl='<%# PageUrl + PreviousItem.Start %>' />
	</div>
	<ul class="fl page_numbers">
	    <asp:Repeater runat="server" ID="PagesRepeater">
	        <ItemTemplate>
	            <li class="fl <%# ((PageItem)Container.DataItem).First ? "first" : "" %> <%# ((PageItem)Container.DataItem).Last ? "last" : "" %>">
	                <asp:HyperLink runat="server"
	                    Text='<%# ((PageItem)Container.DataItem).Index + 1 %>'
	                    CssClass='<%# ((PageItem)Container.DataItem).Selected ? "selected" : null %>'
	                    NavigateUrl='<%# PageUrl + ((PageItem)Container.DataItem).Start %>'
	                    />
	            </li>
	        </ItemTemplate>
	    </asp:Repeater>
		<li class="clrfix"></li>
	</ul>
	<div class="fl prev_next_btns">
	    <asp:HyperLink runat="server" ID="NextLink" CssClass="next" NavigateUrl='<%# PageUrl + NextItem.Start %>' />
	</div>
	<div class="clrfix"></div>
</div>