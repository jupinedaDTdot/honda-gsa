﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Marine.Web.Search
{
  public class PageItem
  {
    public int Index { get; set; }
    public int Start { get; set; }
    public int End { get; set; }
    public bool First { get; set; }
    public bool Last { get; set; }
    public string Url { get; set; }
    public bool Selected { get; set; }
  }

  public partial class Pager : UserControl
  {
      public Pager()
    {
      PageDisplayLimit = 10;
      PageUrl = string.Empty;
    }

    public int TotalRecords { get; set; }
    public int PageSize { get; set; }
    public int PageIndex { get; set; }
    public string PageUrl { get; set; }
    public int PageDisplayLimit { get; set; }
    public PageItem PreviousItem { get; set; }
    public PageItem NextItem { get; set; }
    public IList<PageItem> PageItems { get; set; }

    public int PageStartRecordCount
    {
      get
      {
        int maxSize = PageIndex * PageSize + 1;

        if (TotalRecords != 0 && TotalRecords < maxSize)
          return TotalRecords;
        else
          return maxSize;
      }
    }

    public int PageEndRecordCount
    {
      get
      {
        int maxSize = (PageIndex + 1) * PageSize;

        if (TotalRecords != 0 && TotalRecords < maxSize)
          return TotalRecords;
        else
          return maxSize;
      }
    }

    private bool HasPrevious
    {
      get
      {
        if (PageIndex > 0)
          return true;

        return false;
      }
    }

    private bool HasNext
    {
      get
      {
        if (PageIndex + 1 < CalculateTotalPages())
          return true;

        return false;
      }
    }

    public int CalculateTotalPages()
    {
      int totalPagesAvailable;

      if (TotalRecords == 0)
        return 0;

      // First calculate the division
      //
      totalPagesAvailable = TotalRecords / PageSize;

      // Now do a mod for any remainder
      //
      if ((TotalRecords % PageSize) > 0)
        totalPagesAvailable++;

      return totalPagesAvailable;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      int totalPages = CalculateTotalPages();

      int lowerBound = (PageIndex - (PageDisplayLimit / 2));
      int upperBound = (PageIndex + (PageDisplayLimit / 2 + PageDisplayLimit % 2));

      if (lowerBound <= 0)
      {
        upperBound = upperBound - lowerBound;
        lowerBound = 0;
      }

      if (upperBound > totalPages)
        upperBound = totalPages;

      PreviousLink.Visible = HasPrevious;
      PreviousItem = CreateItem(PageIndex - 1, null, null);

      //if (lowerBound > 0)
      //  RenderFirst(writer);

      BindPagingLinks(lowerBound, upperBound);

      //if (upperBound < totalPages)
      //  RenderLast(writer);

      NextLink.Visible = HasNext;
      NextItem = CreateItem(PageIndex + 1, null, null);

      DataBind();
    }

    private void BindPagingLinks(int start, int end)
    {
      PageItems = new List<PageItem>();

      for (int i = start; i < end; i++)
      {
        PageItem item = CreateItem(i, start, end);
        PageItems.Add(item);
      }

      PagesRepeater.DataSource = PageItems;
    }

    private PageItem CreateItem(int i, int? start, int? end)
    {
      PageItem item = new PageItem();
      item.First = (i == start);
      item.Last = (i == end - 1);
      item.Selected = (i == PageIndex);
      item.Url = string.Format(PageUrl, i);
      item.Index = i;
      item.Start = (i * PageSize);
      item.End = (((i + 1) * PageSize) - 1);

      return item;
    }
  }
}