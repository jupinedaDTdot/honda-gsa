﻿using System;
using System.Collections.Generic;
using Navantis.Honda.CMS.Demo;
using HondaCA.Common;
using Honda.Google.Common;
using Honda.Google.Common.Contracts;
using Honda.Google.Web.GSA;
using System.Linq;

namespace Marine.Web.Search
{
	public partial class Results : CmsContentBasePage//BasePage
	{
		public IList<Item> PagesSearchItems { get; set; }
		public int TotalResults { get; set; }

		public IList<Item> FeaturedSearchItems { get; set; }

		protected override void OnInit(EventArgs e)
		{
			IsNotCMSPage = true;
			base.OnInit(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			string lang = Request.QueryString["L"] ?? "";
			lang = lang.Length > 2 ? lang.Substring(0, 2) : lang;
			if (lang.ToUpper() == Language.French.GetCultureStringValue().Substring(0, 2).ToUpper())
				this.PageLanguage = Language.French;
			else
				this.PageLanguage = Language.English;
			base.InitializeCulture();

			this.Form.DefaultButton = SearchLinkButton.UniqueID;

			if (!IsPostBack)
			{
				setPage();
			}
		}

		protected void setPage()
		{
			string query = Request.QueryString["q"];
			int start = ConversionHelpers.StringToNullableInt(Request.QueryString["start"]) ?? 0;
			string language = Request.QueryString["L"] == "fr" ? "lang_fr" : "lang_en";

			int perPage = ConversionHelpers.StringToNullableInt(Request.QueryString["perPage"]) ?? 5;

			this.ToggleURL = ResourceManager.GetString("SearchResultsRootFolderToggle") + string.Format("/{0}", System.Web.HttpUtility.UrlPathEncode(query)); //replace with search toggle

			if (!IsPostBack)
			{
				SearchTextBox.Text = query;
			}

			if (!string.IsNullOrEmpty(query))
			{
				WebResult pagesResult = WebSearch(query, start, language, perPage, "Pages");
				PagesSearchItems = pagesResult.Results;

				PagesSearchResultsRepeater.DataSource = PagesSearchItems;
				PagesSearchResultsRepeater.DataBind();

				SearchSummaryText.Text = string.Format("{0} results found for '{1}'.", pagesResult.TotalResults, query);

				Pager.TotalRecords = pagesResult.TotalResults;
				Pager.PageIndex = start / perPage;
				Pager.PageSize = perPage;
				//Pager.PageUrl = ResourceManager.GetString("SearchResultsRootFolderToggle") + string.Format("/{0}?language={1}&perPage={2}&start=", query, language, perPage);
				Pager.PageUrl = ResourceManager.GetString("SearchResultsRootFolder") + string.Format("/{0}?perPage={1}&start=", System.Web.HttpUtility.UrlPathEncode(query), perPage);

				//WebResult featuredResult = WebSearch(query, start, language, 3, "Featured");
				//Always show the first three
				WebResult featuredResult = WebSearch(query, 0, language, 3, "Featured");
				FeaturedSearchItems = featuredResult.Results;

				FeaturedSearchResultsRepeater.DataSource = FeaturedSearchItems;
				FeaturedSearchResultsRepeater.DataBind();
			}
		}

		private WebResult WebSearch(string query, int start, string language, int perPage, string setting)
		{
			GSAClient client = new GSAClient();
			client.Client = GSAClientConfig.GetClient(setting);
			client.Collections = GSAClientConfig.GetCollections(setting);
			client.Query = query;
			client.Language = language;
			client.Start = start;
			client.MaxResults = perPage;
			client.PartialMetaFields = GSAClientConfig.GetPartialMetaFields(setting);
			client.RequiredMetaFields = GSAClientConfig.GetRequiredMetaFields(setting);
			client.Filter = GSAClientConfig.GetFilter(setting);
			client.ServiceUrl = RootUrl(ResolveUrl(GSAClientConfig.GetServiceUrl()));

			return client.WebSearch();
		}

		private string RootUrl(string relativeUrl)
		{
			string root = Request.Url.OriginalString;
			if (root.Contains("127.0.0"))
				root = root.Replace(Request.Url.AbsolutePath, string.Empty);
			else
				root = Request.Url.GetLeftPart(UriPartial.Authority);

			Uri uri = new Uri(new Uri(root, UriKind.Absolute), relativeUrl);
			return uri.ToString();
		}

		public string GetItemCssClass<T>(int index, IList<T> items)
		{
			IList<string> cssClasses = new List<string>();

			if (index == 0)
				cssClasses.Add("first");
			if (index == items.Count - 1)
				cssClasses.Add("last");

			if (cssClasses.Count == 0)
				return string.Empty;

			return string.Join(" ", cssClasses.ToArray());
		}

		protected void SearchLinkButton_Click(object sender, EventArgs e)
		{
			Response.Redirect(ResourceManager.GetString("SearchResultsRootFolder") + "/" + System.Web.HttpUtility.UrlPathEncode(SearchTextBox.Text));
		}
	}
}